BACnet-Core

This is an Molex BACnet Gateway Solution which is migrated to .NET Core 2.0 from .NET Framework 4.5

Pre-Requisites to run
1. Microsoft Visual Studio 2017 with .NET Core framework 2.0
2. The above Source Repository
3. Ubuntu 16.4 Operating System with docker installed
4. Install .NET Core 2.0 to ubuntu usng below ref document
https://dotnet.microsoft.com/download/linux-package-manager/ubuntu16-04/sdk-current

Please follow the below steps to run the BACnet Gateway in ubuntu Env using docker

Steps to run docker & Gateway

1. sudo docker images 
to delete the existing images which are their for gateway (to copy the id)

2. sudo docker rmi -f <above docker image id>
delete the existing docker image

3. sudo docker ps
get the running docker status

4. sudo docker rm -f id from above command

5. sudo docker network ls
to get the network details

6. sudo docker network rm above network id

7. go to  cd Desktop

8. sudo docker build Gateway 
gateway is folder name

9. sudo docker network create -d macvlan  --subnet=192.168.80.0/24 --gateway=192.168.80.1 -o parent=eno1 molex2
to create the macvlan network driver

10. sudo docker run --net molex2  -itd <sudo docker images id> bash

11 sudo docker ps
chekc the docker is running or not.

12. sudo docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' <docker ps id>
to check ip address

13. sudo docker logs <docker ps id>
