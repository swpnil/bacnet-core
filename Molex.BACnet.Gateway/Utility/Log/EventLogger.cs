﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              <EventLogger.cs>
///   Description:        <This file is responsible for application level Event Log handling >
///   Author:             Prasad Joshi                  
///   Date:               05/19/17
///   Notes:              <Notes>
///   Revision History: 
///   Modified By:     Modified Date:    Reviewed By:      Reviewed Date:    Review Comments:
///---------------------------------------------------------------------------------------------
#endregion

using System.Diagnostics;

namespace Molex.BACnet.Gateway.Log
{
    /// <summary>
    /// This file is responsible for application level Event Log handling 
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>26-05-201703:00 PM</TimeStamp>
    public class EventLogger
    {
        #region Public Property

        /// <summary>
        /// Gets or sets the event log event source.
        /// </summary>
        /// <value>
        /// The el event source.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>26-05-201703:01 PM</TimeStamp>
        public static string ELEventSource { get; set; }

        /// <summary>
        /// Gets or sets the event log event log.
        /// </summary>
        /// <value>
        /// The event log event log.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>26-05-201703:03 PM</TimeStamp>
        public static string ELEventLog { get; set; }

        /// <summary>
        /// Gets or sets the event log event.
        /// </summary>
        /// <value>
        /// The event log event.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>26-05-201703:04 PM</TimeStamp>
        public static string ELEvent { get; set; }

        #endregion

        //#region Public Function

        ///// <summary>
        ///// Logs the specified message.
        ///// </summary>
        ///// <param name="message">The Log message.</param>
        ///// <param name="type">The EventLogEntry Type.</param>
        ///// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>26-05-201703:04 PM</TimeStamp>
        //public static void Log(string message, EventLogEntryType type = EventLogEntryType.Information)
        //{
        //    try
        //    {
        //        if (!EventLog.SourceExists(ELEventSource))
        //            EventLog.CreateEventSource(ELEventSource, ELEventLog);

        //        EventLog.WriteEntry(ELEventSource, message, type);
        //    }
        //    catch { }
        //}

        //#endregion

    }
}
