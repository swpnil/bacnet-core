﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              <LoggerErrorEventArgs.cs>
///   Description:        <Description>
///   Author:             <Prasad Joshi>                  
///   Date:               05/08/17
#endregion

using System;

namespace Molex.BACnet.Gateway.Log
{
    /// <summary>
    /// Responsible for handling LoggerError event.
    /// </summary>
    public sealed class LoggerErrorEventArgs : EventArgs
    {
        #region Public Constants

        /// <summary>
        /// constant specifying unknown error.
        /// </summary>
        public const byte UNKNOWN_ERROR = 1;

        /// <summary>
        /// constant specifying security error.
        /// </summary>
        public const byte SECURITY_ERROR = 2;

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or Sets the exception.
        /// </summary>
        public Exception LoggerException { get; private set; }
        /// <summary>
        /// Gets or Sets the error code.
        /// </summary>
        public byte ErrorCode { get; private set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="LoggerErrorEventArgs"/> class.
        /// </summary>
        /// <param name="ex">The exception class</param>
        /// <param name="errorCode">The error code.</param>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>26-05-201703:19 PM</TimeStamp>
        public LoggerErrorEventArgs(Exception ex, byte errorCode)
        {
            this.LoggerException = ex;
            this.ErrorCode = errorCode;
        }

        #endregion
    }

}
