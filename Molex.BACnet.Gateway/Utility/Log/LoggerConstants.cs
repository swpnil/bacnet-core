﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              <LoggerConstants>
///   Description:        <Constant for logger>
///   Author:             <Prasad Joshi>                  
///   Date:               05/18/17
///   Notes:              <Notes>
///   Revision History:
///   v1.0 : Logger Constants for SYSlog format
///   
///   Modified By:Prasad Joshi     Modified Date:   Reviewed By:Mandar Bhong      Reviewed Date:    Review Comments:
///---------------------------------------------------------------------------------------------
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.Utility.Log
{
    /// <summary>
    /// Constant for logger
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>26-05-201704:07 PM</TimeStamp>
    public class LoggerConstants
    {
        /// <summary>
        /// The system log Nil value
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>26-05-201704:08 PM</TimeStamp>
        public const string SYS_LOG_NILVALUE = "-";

        /// <summary>
        /// The system log pri factor
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>26-05-201704:09 PM</TimeStamp>
        public const short SYS_LOG_PRI_FACTOR = 8;

        /// <summary>
        /// The log format system log
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>26-05-201704:09 PM</TimeStamp>
        public const byte LOG_FORMAT_SYS_LOG = 1;

        /// <summary>
        /// The log format default
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>26-05-201704:09 PM</TimeStamp>
        public const byte LOG_FORMAT_DEFAULT = 2;

    }
}
