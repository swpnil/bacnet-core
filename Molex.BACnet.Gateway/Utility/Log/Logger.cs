﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              <Logger>
///   Description:        <Description>
///   Author:             <Mandar Bhong>                  
///   Date:               MM/DD/YY
///   Notes:              <Notes>
///   Revision History: 
///   v1.0: 
///   1.Syslog format supported with existing Common logger format
///   Modified By:Prasad Joshi     Modified Date:05/19/17   Reviewed By:Mandar Bhong      Reviewed Date:    Review Comments:
///---------------------------------------------------------------------------------------------
#endregion

using Molex.BACnet.Gateway.Utility.Log;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;

namespace Molex.BACnet.Gateway.Log
{
    /// <summary>
    /// Responsible to log data.
    /// </summary>
    public sealed class Logger
    {
        public event EventHandler<LoggerErrorEventArgs> Error;
        private static object _syncLock = new object();
        private static volatile object _writeLock = new object();

        static AutoResetEvent autoResetEvent = null;
        static volatile object _syncAutoResetLock = new object();
        ConcurrentQueue<LogData> _logQueue = new ConcurrentQueue<LogData>();
        Thread workerThread;
        bool _isQuit = false;
        DateTime _processStartTime = DateTime.Now;

        Dictionary<int, int> _logFlagCache { get; set; }

        /// <summary>
        /// Log Data when receive exception
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>26-05-201704:20 PM</TimeStamp>
        public static Action<LogData> OnExceptionReceived;

        /// <summary>
        /// Gets or sets a value indicating whether [raise event for first chance exception].
        /// </summary>
        public bool RaiseEventForFirstChanceException { get; set; }
        private int _logType { get; set; }

        /// <summary>
        /// Gets or sets the type of the log.
        /// </summary>
        public int LogType
        {
            get { return _logType; }
            set { _logType = value; }
        }


        #region Singleton Class Implementation

        /// <summary>
        /// Volatile instance of class.
        /// </summary>
        private static volatile Logger _instance;

        /// <summary>
        /// Object for acquiring lock while creating singleton instance
        /// to ensure only one instance is created in multi threading environment.
        /// </summary>
        private static object _syncRoot = new Object();

        /// <summary>
        /// Get's the singleton instance of the class.
        /// </summary>

        public static Logger Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_syncRoot)
                    {
                        if (_instance == null)
                            _instance = new Logger();
                    }
                }

                return _instance;
            }
        }

        #endregion Singleton Class Implementation

        #region Constructor

        /// <summary>
        /// Constructor of Logger class
        /// </summary>
        private Logger()
        {
            autoResetEvent = new AutoResetEvent(false);
            workerThread = new Thread(new ThreadStart(LogQueueMessage));
            workerThread.Priority = ThreadPriority.Lowest;
            workerThread.Start();
            Process currentProcess = Process.GetCurrentProcess();
            _processStartTime = currentProcess.StartTime;

            LogFolderPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);

            _logFlagCache = new Dictionary<int, int>();
        }

        #endregion

        #region Private Members
        uint logFileSize = 1024 * 1024;// 1 MB
        int noOfFilesToBackup = 9;
        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or Sets the boolean value for IsDebugEnabled or not.
        /// </summary>
        public bool IsDebugEnabled { get; set; }

        /// <summary>
        /// Gets or Sets the boolean value for syslog.
        /// </summary>
        public bool IsSyslogFormatDisabled { get; set; }

        public byte LogFormat { get; set; }

        /// <summary>
        /// Gets or Sets the log file folder path.
        /// </summary>
        public string LogFolderPath { get; set; }

        /// <summary>
        /// Gets or Sets the log file size.
        /// </summary>
        public uint LogFileSIZE
        {
            get { return logFileSize; }
            set { logFileSize = value; }
        }

        /// <summary>
        /// Gets or Sets the number of log files to backup.
        /// </summary>
        public int NoOfFilesToBackup
        {
            get { return noOfFilesToBackup; }
            set { noOfFilesToBackup = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is log disabled.
        /// </summary>
        public bool IsLogDisabled { get; set; }

        /// <summary>
        /// Gets or sets the name of the host.
        /// </summary>
        public string HostName { get; set; }

        /// <summary>
        /// Gets or sets the name of the application.
        /// </summary>
        public string AppName { get; set; }

        /// <summary>
        /// Gets or sets the proc identifier.
        /// </summary>
        public string ProcID { get; set; }

        /// <summary>
        /// Gets or sets the MSG identifier.
        /// </summary>
        public string MsgID { get; set; }

        /// <summary>
        /// Gets or sets the MSG identifier text.
        /// </summary>
        public Dictionary<int, string> MsgIDText { get; set; }

        #endregion

        #region Public Constants

        /// <summary>
        /// The delimiter
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>26-05-201704:19 PM</TimeStamp>
        public const string DELIMITER = "}";

        /// <summary>
        /// The log file name
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>26-05-201704:19 PM</TimeStamp>
        public const string LOG_FILE_NAME = "log.txt";

        /// <summary>
        /// The log backup information file
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>26-05-201704:20 PM</TimeStamp>
        public const string LOG_BACKUP_INFO_FILE = "log_info.txt";

        #endregion

        #region Public Methods

        public void SetLogFlag(int key, int value)
        {
            if (!_logFlagCache.ContainsKey(key))
                _logFlagCache.Add(key, value);
        }

        public int GetLogFlag(int key)
        {
            if (!_logFlagCache.ContainsKey(key))
                return -1;

            return _logFlagCache[key];
        }

        /// <summary>
        /// Add log data in a queue.
        /// </summary>
        /// <param name="logData">data to be log</param>
        public void Log(LogData logData, int logType = 0)
        {
            if (_logType != 0 || _logType != logType)
                return;

            EnqueueMessage(logData);
        }

        /// <summary>
        /// Add log data in a queue.
        /// </summary>
        /// <param name="logLevel">The log level.</param>
        /// <param name="message">message to be log</param>
        /// <param name="logType">Type of the log.</param>
        public void Log(LogLevel logLevel, string message, int logType = 0)
        {
            if (_logType != 0 || _logType != logType)
                return;

            LogData logData = new LogData();
            logData.LogLevel = logLevel;
            // logData.LogType = logType;
            logData.MsgID = logType.ToString();
            logData.FacilityValue = FacilityValue.MESSAGES_GENERATED_INTERNALLY_BY_SYSLOGD;
            logData.MoreInfo = message;
            //#if DEBUG
            if (logLevel == LogLevel.ERROR)
                Console.WriteLine("LogLevel :" + logLevel.ToString() + "Message :" + message);
            //#else
            EnqueueMessage(logData);
            //#endif
        }

        /// <summary>
        /// Add log data in a queue.
        /// </summary>
        /// <param name="exceptionMessage">The exception message.</param>
        /// <param name="logLevel">The log level.</param>
        /// <param name="message">exception to be log</param>
        /// <param name="logType">Type of the log.</param>
        public void Log(Exception exceptionMessage, LogLevel logLevel, string message = "", int logType = 0)
        {
            if (_logType != 0 || _logType != logType)
                return;

            LogData logData = new LogData();
            logData.LogLevel = logLevel;
            logData.FacilityValue = FacilityValue.MESSAGES_GENERATED_INTERNALLY_BY_SYSLOGD;
            logData.MsgID = logType.ToString();
            logData.LogException = exceptionMessage;
            logData.MoreInfo = message;

            //#if DEBUG
            if (logLevel == LogLevel.ERROR)
                Console.WriteLine("LogLevel :" + logLevel.ToString() + "Message :" + message);
            //#else
            EnqueueMessage(logData);
            //#endif
        }

        /// <summary>
        /// Generate log text from exception.
        /// </summary>
        /// <param name="ex">exception from which log text is to be created</param>
        /// <returns>It will returns Generated Log Text</returns>
        public string GenerateLogText(Exception ex)
        {
            Assembly caller = Assembly.GetEntryAssembly();
            Process currentProcess = Process.GetCurrentProcess();

            StringBuilder logTextBuilder = new StringBuilder();

            AppendApplicationInfo(logTextBuilder, caller);
            logTextBuilder.AppendLine(DELIMITER);

            AppendExceptionInfo(ex, logTextBuilder);
            Exception exception = ex;
            while (exception.InnerException != null)
            {
                logTextBuilder.AppendLine("Inner Exception Information: ");
                exception = exception.InnerException;
                AppendExceptionInfo(exception, logTextBuilder);
            }

            logTextBuilder.AppendLine(DELIMITER);

            AppendModuleInfo(ex, logTextBuilder, currentProcess);
            logTextBuilder.AppendLine(DELIMITER);

            AppendSystemInfo(logTextBuilder);

            return logTextBuilder.ToString();
        }

        /// <summary>
        ///log file start up
        /// </summary>
        public void LogStartup()
        {
            Assembly caller = Assembly.GetEntryAssembly();
            StringBuilder logTextBuilder = new StringBuilder();
            LogData logData = new LogData();

            logTextBuilder.AppendFormat("----------START----------");
            logTextBuilder.AppendLine();
            logTextBuilder.AppendLine("APPLICATION STARTUP");
            logTextBuilder.AppendFormat("Logger Startup Time: {0}", _processStartTime);
            logTextBuilder.AppendLine();
            logTextBuilder.AppendLine();
            AppendSystemInfo(logTextBuilder);
            logTextBuilder.AppendLine();
            AppendApplicationInfo(logTextBuilder, caller);
            logTextBuilder.AppendLine("----------END----------");
            logTextBuilder.AppendLine();
            WriteToFile(logTextBuilder);

            EnqueueMessage(logData);

        }

        /// <summary>
        /// Writes to file.
        /// </summary>
        /// <param name="logTextBuilder">The log text builder.</param>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>26-05-201703:14 PM</TimeStamp>
        public void WriteToFile(StringBuilder logTextBuilder)
        {
            lock (_writeLock)
            {
                if (this._isQuit) return;
                try
                {
                    string logFileFullPath = Path.Combine(LogFolderPath, LOG_FILE_NAME);
                    FileInfo logFileInfo = new FileInfo(logFileFullPath);
                    bool appendToCurrentLogFile = false;
                    if (!logFileInfo.Exists)
                    {
                        if (!Directory.Exists(LogFolderPath))
                        {
                            Directory.CreateDirectory(LogFolderPath);
                        }
                    }
                    else
                    {
                        if (logFileInfo.Length >= logFileSize)
                        {
                            // save the file with different name
                            string logBackupFileName = string.Format("log_{0}.txt", DateTime.Now.ToString("ddMMyyHHmmss"));
                            string logBackUpFilePath = Path.Combine(LogFolderPath, logBackupFileName);
                            File.Copy(logFileFullPath, logBackUpFilePath, true);
                            string logBackUpInfoFileName = Path.Combine(LogFolderPath, LOG_BACKUP_INFO_FILE);
                            FileInfo backupFileInfo = new FileInfo(logBackUpInfoFileName);

                            List<string> fileParts = new List<string>();
                            if (backupFileInfo.Exists)
                            {
                                using (StreamReader sr = new StreamReader(logBackUpInfoFileName))
                                {
                                    while (sr.Peek() >= 0 && fileParts.Count < noOfFilesToBackup)
                                    {
                                        fileParts.Add(sr.ReadLine());
                                    }

                                    if (fileParts.Count >= noOfFilesToBackup)
                                    {
                                        File.Delete(Path.Combine(LogFolderPath, fileParts[0]));
                                        fileParts.RemoveAt(0);
                                    }
                                }
                            }

                            fileParts.Add(logBackupFileName);

                            using (StreamWriter sw = new StreamWriter(logBackUpInfoFileName))
                            {
                                fileParts.ForEach(filePart => sw.WriteLine(filePart));
                            }
                        }
                        else
                        {
                            appendToCurrentLogFile = true;
                        }
                    }

                    using (StreamWriter sw = new StreamWriter(logFileFullPath, appendToCurrentLogFile))
                    {
                        sw.Write(logTextBuilder.ToString());
                    }
                }

                catch (UnauthorizedAccessException securityException)
                {
                    if (Error != null)
                    {
                        CloseLogger();
                        Error(this, new LoggerErrorEventArgs(securityException, LoggerErrorEventArgs.SECURITY_ERROR));
                    }
                }
                catch (Exception unknownException)
                {
                    if (Error != null)
                    {
                        CloseLogger();
                        Error(this, new LoggerErrorEventArgs(unknownException, LoggerErrorEventArgs.UNKNOWN_ERROR));
                    }
                }
                finally
                {

                }
            }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Enqueue the message
        /// </summary>
        /// <param name="logData">logData to be logged </param>
        void EnqueueMessage(LogData logData)
        {
            if (logData.LogLevel == LogLevel.DEBUG && !IsDebugEnabled)
                return;

            _logQueue.Enqueue(logData);

            lock (_syncAutoResetLock)
            {
                autoResetEvent.Set();
            }
        }

        /// <summary>
        /// Queue log message
        /// </summary>
        void LogQueueMessage()
        {
            while (autoResetEvent.WaitOne())
            {
                if (_isQuit) break;

                lock (_syncAutoResetLock)
                {
                    autoResetEvent.Reset();
                }

                while (_logQueue.Count > 0)
                {
                    LogData req;

                    if (_logQueue.TryDequeue(out req))
                    {
                        if (Logger.Instance.IsLogDisabled)
                            continue;

                        CreateLog(req);
                        if (OnExceptionReceived == null) continue;
                        if (req.LogException == null) continue;

                        if (!req.IsFirstChanceException)
                        {
                            OnExceptionReceived(req);
                        }
                        else if (RaiseEventForFirstChanceException)
                        {
                            OnExceptionReceived(req);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Create log format
        /// </summary>
        /// <param name="logData">logData to be logged</param>
        private void CreateLog(LogData logData)
        {
            if (logData.MoreInfo == null) return;

            switch (Logger.Instance.LogFormat)
            {
                case LoggerConstants.LOG_FORMAT_SYS_LOG:
                    {
                        WriteSysLogFormat(logData);
                    }
                    break;
                case LoggerConstants.LOG_FORMAT_DEFAULT:
                default:
                    {
                        WriteLogFormat(logData);
                    }
                    break;
            }
        }

        /// <summary>
        /// Appends application informarion to log
        /// </summary>
        /// <param name="logTextBuilder">StringBuilder</param>
        /// <param name="caller">Assmbly input</param>
        private void AppendApplicationInfo(StringBuilder logTextBuilder, Assembly caller)
        {
            logTextBuilder.AppendLine("Application Information");
            string productName = string.Empty;
            Version productVersion;

            // Get the product name from the AssemblyProductAttribute.
            //   Usually defined in AssemblyInfo.cs as: [assembly: AssemblyProduct("BACtest")]
            var assemblyProductAttribute = ((AssemblyProductAttribute[])caller.GetCustomAttributes(typeof(AssemblyProductAttribute), false)).SingleOrDefault();
            if (assemblyProductAttribute != null)
                productName = assemblyProductAttribute.Product;

            // Get the product version from the assembly by using its AssemblyName.
            productVersion = new AssemblyName(caller.FullName).Version;

            logTextBuilder.AppendLine("Product Name: " + productName);
            logTextBuilder.AppendLine("Product Version: " + productVersion.ToString());
            logTextBuilder.AppendLine("Assembly Full Name: " + caller.FullName);
            logTextBuilder.AppendLine("Image Runtime Version: " + caller.ImageRuntimeVersion);
            logTextBuilder.AppendLine("Location: " + caller.Location);
        }

        /// <summary>
        /// Appends application exception inofrmation to log
        /// </summary>
        /// <param name="exception">Excpetion</param>
        /// <param name="logTextBuilder">StringBuilder</param>
        private void AppendExceptionInfo(Exception exception, StringBuilder logTextBuilder)
        {
            logTextBuilder.AppendLine("Exception Information");
            logTextBuilder.AppendLine("Exception Type: " + exception.GetType());
            if (!string.IsNullOrEmpty(exception.StackTrace))
                logTextBuilder.AppendLine("Exception Source: " + exception.Source);
            if (exception.TargetSite != null)
                logTextBuilder.AppendLine("Targate Site: " + exception.TargetSite.Name);
            logTextBuilder.AppendLine("Error Message: " + exception.Message);
            if (!string.IsNullOrEmpty(exception.StackTrace))
                logTextBuilder.AppendLine("Stack Trace: " + exception.StackTrace);
        }

        private void AppendModuleInfo(Exception exception, StringBuilder logTextBuilder, Process currentProcess)
        {
            logTextBuilder.AppendLine("Module Information");
            foreach (ProcessModule module in currentProcess.Modules)
            {
                try
                {
                    logTextBuilder.AppendLine(module.FileName + " | " + module.FileVersionInfo.FileVersion + " | " + module.ModuleMemorySize);
                }
                catch (System.IO.FileNotFoundException)
                {
                    logTextBuilder.AppendLine("File Not Found: " + module.ToString());
                }
                catch (Exception)
                {
                    logTextBuilder.AppendLine("Exception: " + module.ToString());
                }
            }
        }

        private void AppendSystemInfo(StringBuilder logTextBuilder)
        {
            logTextBuilder.AppendLine("System Information");
            logTextBuilder.AppendLine("OS Version: " + Environment.OSVersion.ToString());
            logTextBuilder.AppendLine("Processor Count: " + Environment.ProcessorCount.ToString());
            logTextBuilder.AppendLine("Machine Name: " + Environment.MachineName);
            logTextBuilder.AppendLine("User Name: " + Environment.UserName);
            logTextBuilder.AppendLine("Process Working Set: " + Environment.WorkingSet.ToString());
            logTextBuilder.AppendLine("Current Culture: " + CultureInfo.CurrentCulture.Name);
            logTextBuilder.AppendLine(".NET Framework Version: " + Environment.Version.ToString());
            logTextBuilder.AppendLine("Process Start Time: " + _processStartTime.ToString());

        }

        /// <summary>
        /// Write message in default format
        /// </summary>
        /// <param name="logData">LogData to be logged</param>
        private void WriteLogFormat(LogData logData)
        {
            StringBuilder logTextBuilder = new StringBuilder();
            logTextBuilder.AppendFormat("----------START----------");
            logTextBuilder.AppendLine();
            logTextBuilder.AppendFormat("Log Level:{0}{1}DateTime:{2}", logData.LogLevel, System.Environment.NewLine, logData.LogGenerationTime);
            logTextBuilder.AppendLine();
            logTextBuilder.AppendFormat("Process Start Time: {0}", _processStartTime);
            logTextBuilder.AppendLine();

            if (logData.LogException != null)
            {
                AppendExceptionInfo(logData.LogException, logTextBuilder);
                Exception exception = logData.LogException;
                while (exception.InnerException != null)
                {
                    logTextBuilder.AppendLine("Inner Exception Information: ");
                    exception = exception.InnerException;
                    AppendExceptionInfo(exception, logTextBuilder);
                }
            }

            if (logData.MoreInfo != null)
            {
                /* Msg: {0} */
                logTextBuilder.AppendFormat("{0}", logData.MoreInfo);
            }
            logTextBuilder.AppendLine();
            logTextBuilder.AppendLine("----------END----------");
            logTextBuilder.AppendLine();

            WriteToFile(logTextBuilder);

        }

        /// <summary>
        /// Wirte message in a file in syslog format
        /// </summary>
        /// <param name="logData">LogData</param>
        private void WriteSysLogFormat(LogData logData)
        {
            logData.MsgID = (MsgIDText.ContainsKey((logData.LogType)) ? MsgIDText[logData.LogType].ToString() : logData.LogType.ToString());

            StringBuilder logTextBuilder = new StringBuilder();
            logData.Prival = (ushort)((LoggerConstants.SYS_LOG_PRI_FACTOR * (ushort)logData.FacilityValue) + (ushort)logData.LogLevel);
            /* Pri:<{0}>Version:{1} TimeStamp:{2} HostName:{3} AppName:{4} ProcID:{5} MsgID:{6} StucureData:{7} */
            logTextBuilder.AppendFormat("<{0}>{1} {2} {3} {4} {5} {6} {7} ", logData.Prival, logData.Version, logData.LogGenerationTime.ToString("yyyy-MM-ddTHH:mm:ssZ"),
                Logger.Instance.HostName, Logger.Instance.AppName, Logger.Instance.ProcID, logData.MsgID,
                LoggerConstants.SYS_LOG_NILVALUE);

            if (logData.MoreInfo != null)
            {
                /* Msg: {0} */
                logTextBuilder.AppendFormat("{0}", logData.MoreInfo);
            }

            logTextBuilder.AppendLine();
            WriteToFile(logTextBuilder);
        }

        /// <summary>
        /// Close the logger thread
        /// </summary>
        public void CloseLogger()
        {
            // Allowing logger to log pending messages if any.
            Thread.Sleep(1000);

            this._isQuit = true;
            lock (_syncAutoResetLock)
            {
                autoResetEvent.Set();
            }

            if (!workerThread.Join(1000))
            {
                try
                {
                    workerThread.Abort();
                }
                catch
                {

                }
            }
            this._isQuit = false;
        }

        #endregion
    }
}
