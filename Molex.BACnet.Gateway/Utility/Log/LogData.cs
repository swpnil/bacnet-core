﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              <LogData.cs>
///   Description:        <This file holds differnt types of Enums and Log related Data >
///   Author:             <Rupesh Saw>                  
///   Date:               05/08/17
#endregion

using System;

namespace Molex.BACnet.Gateway.Log
{
    /// <summary>
    /// Typs of LogLevel
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>26-05-201703:07 PM</TimeStamp>
    public enum LogLevel
    {
        /// <summary>
        /// ERROR log level.
        /// </summary>
        ERROR = 3,

        /// <summary>
        /// DEBUG log level.
        /// </summary>
        DEBUG = 7
    }

    /// <summary>
    /// This Enum stored Differnt Type of Messgae List
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>26-05-201703:06 PM</TimeStamp>
    public enum FacilityValue
    {
        /// <summary>
        /// kernel messages.
        /// </summary>
        KERNEL_MESSAGES,
        /// <summary>
        /// user-level messages.
        /// </summary>
        USER_LEVEL_MESSAGES,
        /// <summary>
        /// mail system.
        /// </summary>
        MAIL_SYSTEM,
        /// <summary>
        /// system daemons.
        /// </summary>
        SYSTEM_DAEMONS,
        /// <summary>
        /// security/authorization messages.
        /// </summary>
        SECURITY_OR_AUTHORIZATION_MESSAGES1,
        /// <summary>
        /// messages generated internally by syslogd.
        /// </summary>
        MESSAGES_GENERATED_INTERNALLY_BY_SYSLOGD,
        /// <summary>
        /// line printer subsystem.
        /// </summary>
        LINE_PRINTER_SUBSYSTEM,
        /// <summary>
        /// network news subsystem.
        /// </summary>
        NETWORK_NEWS_SUBSYSTEM,
        /// <summary>
        /// UUCP subsystem.
        /// </summary>
        UUCP_SUBSYSTEM,
        /// <summary>
        /// clock daemon.
        /// </summary>
        CLOCK_DAEMON,
        /// <summary>
        /// security/authorization messages.
        /// </summary>
        SECURITY_OR_AUTHORIZATION_MESSAGES2,
        /// <summary>
        /// FTP daemon.
        /// </summary>
        FTP_DAEMON,
        /// <summary>
        /// NTP subsystem.
        /// </summary>
        NTP_SUBSYSTEM,
        /// <summary>
        /// log audit.
        /// </summary>
        LOG_AUDIT,
        /// <summary>
        /// log alert.
        /// </summary>
        LOG_ALERT,
        /// <summary>
        /// clock daemon (note 2).
        /// </summary>
        CLOCK_DAEMON_NOTE2,
        /// <summary>
        /// local use 0  (local0).
        /// </summary>
        LOCAL_USE_0_LOCAL0,
        /// <summary>
        /// local use 1  (local1).
        /// </summary>
        LOCAL_USE_1_LOCAL1,
        /// <summary>
        /// local use 2  (local2).
        /// </summary>
        LOCAL_USE_2_LOCAL2,
        /// <summary>
        /// local use 3  (local3).
        /// </summary>
        LOCAL_USE_3_LOCAL3,
        /// <summary>
        /// local use 4  (local4).
        /// </summary>
        LOCAL_USE_4_LOCAL4,
        /// <summary>
        /// local use 5  (local5).
        /// </summary>
        LOCAL_USE_5_LOCAL5,
        /// <summary>
        /// local use 6  (local6).
        /// </summary>
        LOCAL_USE_6_LOCAL6,
        /// <summary>
        /// local use 7  (local7).
        /// </summary>
        LOCAL_USE_7_LOCAL7
    }



    /// <summary>
    /// Responsible for holding log data.
    /// </summary>
    public class LogData
    {
        /// <summary>
        /// Gets or sets the prival.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>26-05-201703:10 PM</TimeStamp>
        internal ushort Prival { get; set; }

        /// <summary>
        /// Gets or Sets the Version value.
        /// </summary>
        internal short Version { get; set; }

        /// <summary>
        /// Gets or Sets the MessageId.
        /// </summary>
        internal string MsgID { get; set; }

        /// <summary>
        /// Gets or Sets the LogType.
        /// </summary>
        internal int LogType { get; set; }

        /// <summary>
        /// Gets or Sets the log level.
        /// </summary>
        public LogLevel LogLevel { get; set; }

        /// <summary>
        /// Gets or Sets the Facility Value.
        /// </summary>
        internal FacilityValue FacilityValue { get; set; }

        /// <summary>
        /// Gets or Sets the exception.
        /// </summary>
        public Exception LogException { get; set; }

        /// <summary>
        /// Get or set DatabaseName
        /// </summary>
        public string DataBaseName { get; set; }

        /// <summary>
        /// Gets or Sets more information about exception.
        /// </summary>
        public string MoreInfo { get; set; }

        /// <summary>
        /// Gets or Sets the time when exception occured.
        /// </summary>

        public DateTime LogGenerationTime { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is first chance exception.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is first chance exception; otherwise, <c>false</c>.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>15-06-201712:15 PM</TimeStamp>
        public bool IsFirstChanceException { get; set; }

        public LogData()
        {
            LogGenerationTime = DateTime.Now;
            Version = 1;
        }
    }
}
