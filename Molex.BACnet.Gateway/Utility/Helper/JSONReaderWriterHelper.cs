﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              <JSONReaderWriterHelper.cs>
///   Description:        <Description>
///   Author:             Nazneen Zahid                  
///   Date:               05/22/17
///   Notes:              <Notes>
///   Revision History: 
///   Modified By:      Modified Date:    Reviewed By:      Reviewed Date:    Review Comments:
///---------------------------------------------------------------------------------------------
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using System.Runtime.Serialization.Json;
using Molex.BACnet.Gateway.Log;

namespace Molex.BACnet.Gateway.Utility.Helper
{
    /// <summary>
    /// this helper class is used to read data from json file and convert data to Json Format
    /// </summary>
    public static class JSONReaderWriterHelper
    {
        /// <summary>
        /// Reads the json from file.
        /// </summary>
        /// <typeparam name="T">Generic Type</typeparam>
        /// <param name="jsonFileName">Name of the json file.</param>
        /// <returns>Generic Type</returns>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201712:52 PM</TimeStamp>
        public static T ReadJsonFromFile<T>(string jsonFileName)
        {
            try
            {
                var jsonString = File.ReadAllText(jsonFileName);
                T jsonObj = JsonConvert.DeserializeObject<T>(jsonString);
                return jsonObj;
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(LogLevel.DEBUG, "WriteUpdateJsonFile :" + ex.Message);
                return default(T);
            }
        }

        public static bool ValidateJsonFile(string jsonFilePath, string jsonSchemaFilePath, out IList<string> errormessage)
        {
            bool bResult = false;
            try
            {
                var jsonString = File.ReadAllText(jsonFilePath);
                JsonSchema schema = Newtonsoft.Json.Schema.JsonSchema.Parse(File.ReadAllText(jsonSchemaFilePath));
                //JSchema schema = JSchema.Parse(File.ReadAllText(jsonSchemaFilePath));
                JObject jObject = JObject.Parse(jsonString);
                bResult = jObject.IsValid(schema, out errormessage);               
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(LogLevel.DEBUG, "WriteUpdateJsonFile :" + ex.Message);
                errormessage = null;
            }
            return bResult;
        }

        /// <summary>
        /// Reads the json from file for dictinalry.
        /// </summary>
        /// <typeparam name="T">Generic Type</typeparam>
        /// <param name="jsonFileName">Name of the json file.</param>
        /// <returns>Generic Type</returns>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201712:52 PM</TimeStamp>
        public static T ReadJsonFromFileForDictinalry<T>(string jsonFileName)
        {
            try
            {
                var jsonString = File.ReadAllText(jsonFileName);

                DataContractJsonSerializer js = new DataContractJsonSerializer(typeof(T));
                MemoryStream ms = new MemoryStream(System.Text.ASCIIEncoding.ASCII.GetBytes(jsonString));
                T jsonObj = (T)js.ReadObject(ms);
                ms.Close();

                return jsonObj;
            }
            catch (Exception ex)
            {

                return default(T);
            }
        }

        /// <summary>
        /// Writes the update json file.
        /// </summary>
        /// <typeparam name="T">Generic Type</typeparam>
        /// <param name="jsonFileName">Name of the json file.</param>
        /// <param name="dataObject">The data object.</param>
        /// <returns>bool</returns>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201712:52 PM</TimeStamp>
        //public static bool WriteUpdateJsonFile<T>(string jsonFileName, T dataObject)
        //{
        //    try
        //    {
        //        var settings = new DataContractJsonSerializerSettings();
        //        settings.EmitTypeInformation = System.Runtime.Serialization.EmitTypeInformation.Never;

        //        DataContractJsonSerializer dataContractJsonSerializer = new DataContractJsonSerializer(typeof(T), settings);
        //        MemoryStream msObj = new MemoryStream();
        //        dataContractJsonSerializer.WriteObject(msObj, dataObject);
        //        msObj.Position = 0;

        //        StreamReader sr = new StreamReader(msObj);
        //        string json = sr.ReadToEnd();
        //        sr.Close();
        //        msObj.Close();

        //        File.WriteAllText(jsonFileName, json);
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {

        //        return false;
        //    }
        //}

        /// <summary>
        /// Writes the update json file.
        /// </summary>
        /// <typeparam name="T">Generic Type</typeparam>
        /// <param name="jsonFileName">Name of the json file.</param>
        /// <param name="dataObject">The data object.</param>
        /// <returns>bool</returns>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201712:52 PM</TimeStamp>
        public static bool WriteToJsonFile(string jsonFileName, object dataObject)
        {
            try
            {
                var jsonData = JsonConvert.SerializeObject(dataObject);
                System.IO.File.WriteAllText(jsonFileName, jsonData);
                return true;
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(LogLevel.ERROR, "Failed to write the json data to text file");
                return false;
            }
        }


        /// <summary>
        /// Reads the json from file.
        /// </summary>
        /// <typeparam name="T">Generic Type</typeparam>
        /// <param name="jsonFileName">Name of the json file.</param>
        /// <returns>Generic Type</returns>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201712:52 PM</TimeStamp>
        public static T ReadJsonFromString<T>(string jsonString)
        {
            try
            {
                T jsonObj = JsonConvert.DeserializeObject<T>(jsonString);
                return jsonObj;
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "JSONReaderWriterHelper.ReadJsonFromString: Failed to read the json data string => " + jsonString);
                return default(T);
            }
        }
    }
}
