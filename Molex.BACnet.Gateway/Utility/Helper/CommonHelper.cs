﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              <CommonHelper.cs>
///   Description:        <This class provide common helper functions>
///   Author:             <Rupesh Saw>                  
///   Date:               05/09/17
#endregion

using Microsoft.Win32;
using System;
using System.Reflection;

//
namespace Molex.BACnet.Gateway.Helper
{
    /// <summary>
    /// This class provide common helper functions
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>26-05-201702:51 PM</TimeStamp>
    public static class CommonHelper
    {
        /// <summary>
        /// Deeps the clone.
        /// </summary>
        /// <typeparam name="T">Generic Type</typeparam>
        /// <param name="objSource">The object source.</param>
        /// <returns>Return generic type cloned object</returns>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>26-05-201702:58 PM</TimeStamp>
        public static T DeepClone<T>(this T objSource) where T : new()
        {
            //Get the type of source object and create a new instance of that type
            Type typeSource = objSource.GetType();
            object objTarget = Activator.CreateInstance(typeSource);

            //Get all the properties of source object type
            //PropertyInfo[] propertyInfo = typeSource.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            PropertyInfo[] propertyInfo = typeSource.GetProperties();

            //Assign all source property to taget object 's properties
            foreach (PropertyInfo property in propertyInfo)
            {
                //Check whether property can be written to
                if (property.CanWrite)
                {
                    //check whether property type is value type, enum or string type
                    if (property.PropertyType.IsValueType || property.PropertyType.IsEnum || property.PropertyType.Equals(typeof(System.String)))
                    {
                        property.SetValue(objTarget, property.GetValue(objSource, null), null);
                    }
                    //else property type is object/complex types, so need to recursively call this method until the end of the tree is reached
                    else
                    {
                        object objPropertyValue = property.GetValue(objSource, null);
                        if (objPropertyValue == null)
                        {
                            property.SetValue(objTarget, null, null);
                        }
                        else
                        {
                            property.SetValue(objTarget, objPropertyValue.DeepClone(), null);
                        }
                    }
                }
            }
            return (T)objTarget;
        }
    }
}
