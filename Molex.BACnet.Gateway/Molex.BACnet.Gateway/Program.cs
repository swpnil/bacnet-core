﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              <Program.cs>
///   Description:        <Startup location>
///   Author:             Mandar                  
///   Date:               05/08/17
///---------------------------------------------------------------------------------------------
#endregion

using Molex.BACnet.Gateway.ServiceCore.Helpers;
using Molex.BACnet.Gateway.ServiceWebApi.Controllers;
using System;

namespace Molex.BACnet.Gateway
{
    class Program
    {
        internal static GatewayHTTPControllerApi _apiController;

        static void Main(string[] args)
        {
            //start the logger
            AppHelper.StartLogger();

            //start self-host web api.
            string WebAPIAddress = AppHelper.GetIPAddress();
            ////start the web-api
            //while (Molex.BACnet.Gateway.ServiceCore.Repository.AppCoreRepo.Instance.BACnetConfigurationData == null)
            //{
            //    System.Threading.Thread.Sleep(1000);
            //}
            ServiceWebApi.Helper.AppHelper.Init();
            _apiController = new ServiceWebApi.Controllers.GatewayHTTPControllerApi();
            _apiController.Start(WebAPIAddress,
                                 Molex.BACnet.Gateway.ServiceWebApi.Helper.AppHelper.WebApiPortNo);

            //start the bacnet gateway
            AppHelper.Start();
           
        }
    }
}
