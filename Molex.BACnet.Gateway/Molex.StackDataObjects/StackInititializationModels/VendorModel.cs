﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.StackInititializationModels
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------

namespace Molex.StackDataObjects.StackInititializationModels
{
    public class VendorModel
    {
        public ushort VendorID { get; set; }
        public string VendorName { get; set; }
        public string ModelName { get; set; }
        public string FirmwareRevision { get; set; }
        public string SoftwareVersion { get; set; }
        public string Location { get; set; }
        public string DeviceDescription { get; set; }
        public string ObjectDescription { get; set; }
        public string ProfileName { get; set; }
        public string ObjectSerialNumber { get; set; }
        public string SerialNumber { get; set; }
        public string Password { get; set; }
        public uint DeviceID { get; set; }
    }
}
