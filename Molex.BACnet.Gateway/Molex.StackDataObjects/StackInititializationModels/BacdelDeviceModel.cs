﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.StackInititializationModels
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------

namespace Molex.StackDataObjects.StackInititializationModels
{
    public class BacdelDeviceModel
    {
        public uint DAdr { get; set; }
        public uint DeviceID { get; set; }
        public ushort SNet { get; set; }
    }
}
