﻿using Molex.StackDataObjects.Constants;
using System.Collections.Generic;

namespace Molex.StackDataObjects.StackInititializationModels
{
    /// <summary>
    /// To handle register callback for different type like object proparties
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Y"></typeparam>
    public class CallBackModel
    {
        public CallBackModel()
        {
            PropertyIds = new List<BacnetPropertyID>();
        }

        public BacnetObjectType ObjectType { get; set; }
        public List<BacnetPropertyID> PropertyIds { get; set; }
    }
}
