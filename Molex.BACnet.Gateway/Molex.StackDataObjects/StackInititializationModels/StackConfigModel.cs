﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.StackInititializationModels
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using System.Net;

namespace Molex.StackDataObjects.StackInititializationModels
{
    public class StackConfigModel
    {
        public IPAddress IpAddress { get; set; }
        public ushort Port { get; set; }
        public ushort LocalNetworkNumber { get; set; }
        public ushort VirtualNetworkNumber { get; set; }
    }
}
