﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.StackInititializationModels
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------

using System;
using System.Collections.Generic;
namespace Molex.StackDataObjects.StackInititializationModels
{
    public class StackConfigurationModel
    {
        public BacdelDeviceModel BACDelDeviceSelfInfo { get; set; }
        public StackConfigModel StackConfigSelfInfo { get; set; }
        public VendorModel Vendor { get; set; }
    }
}
