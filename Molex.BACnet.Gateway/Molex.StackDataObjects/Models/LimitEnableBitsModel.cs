﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Models
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------

namespace Molex.StackDataObjects.Models
{
    public class LimitEnableBitsModel
    {
        public bool LowLimit { get; set; }
        public bool HighLimit { get; set; }

        public override string ToString()
        {
            return string.Format("Low Limit Enable : {0} , High Limit Enable : {1} ", this.LowLimit, this.HighLimit);
        }
    }
}
