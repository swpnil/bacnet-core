﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Models
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------

namespace Molex.StackDataObjects.Models
{
    public class EventTransitionBitsModel
    {
        public bool ToOffNormal { get; set; }
        public bool ToFault { get; set; }
        public bool ToNormal { get; set; }

        public override string ToString()
        {
            return string.Format("To Off Normal : {0} , To Fault : {1} , To Normal : {2}", this.ToOffNormal, this.ToFault, this.ToNormal);
        }
    }
}
