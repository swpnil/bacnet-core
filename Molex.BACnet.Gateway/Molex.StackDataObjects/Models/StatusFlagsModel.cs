﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Models
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------

namespace Molex.StackDataObjects.Models
{
    public class StatusFlagsModel
    {
        public bool InAlarm { get; set; }
        public bool Fault { get; set; }
        public bool Overridden { get; set; }
        public bool OutOfService { get; set; }

        public override string ToString()
        {
            return InAlarm + ":" + Fault + ":" + Overridden + ":" + OutOfService + "";
        }
    }
}
