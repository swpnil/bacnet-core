﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.APIModels
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.APIModels
{
    public class PropertyValueModel
    {
        public BacnetPropertyID ObjectProperty { get; set; }
        public int PropertyArrayIndex { get; set; }
        public bool IsArrayIndxPresent { get; set; }
        public BacnetDataType DataType { get; set; }
        public byte Priority { get; set; }
        public object PropertyValue { get; set; }
    }
}
