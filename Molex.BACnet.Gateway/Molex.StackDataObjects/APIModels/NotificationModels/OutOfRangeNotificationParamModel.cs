﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.APIModels.NotificationModels
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Models;

namespace Molex.StackDataObjects.APIModels.NotificationModels
{
    public class OutOfRangeNotificationParamModel : BacnetNotificationParamBase
    {
        public float ExceedingValue { get; set; }
        public float ExceededLimit { get; set; }
        public float Deadband { get; set; }
        public StatusFlagsModel StatusFlag { get; set; }
    }
}
