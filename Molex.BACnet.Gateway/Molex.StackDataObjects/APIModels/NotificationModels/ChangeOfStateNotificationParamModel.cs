﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.APIModels.NotificationModels
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Models;

namespace Molex.StackDataObjects.APIModels.NotificationModels
{
    public class ChangeOfStateNotificationParamModel : BacnetNotificationParamBase
    {
        public BacnetPropertyStatesModel BacnetPropertyStatesModel { get; set; }
        public StatusFlagsModel StatusFlags { get; set; }
    }
}
