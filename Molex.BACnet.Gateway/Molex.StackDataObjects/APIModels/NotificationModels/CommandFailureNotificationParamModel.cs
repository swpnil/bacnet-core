﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.APIModels.NotificationModels
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Models;

namespace Molex.StackDataObjects.APIModels.NotificationModels
{
    public class CommandFailureNotificationParamModel : BacnetNotificationParamBase
    {
        public byte Apptag { get; set; }
        public object CommandValue { get; set; }
        public object FeedbackValue { get; set; }
        public StatusFlagsModel StatusFlags { get; set; }
    }
}
