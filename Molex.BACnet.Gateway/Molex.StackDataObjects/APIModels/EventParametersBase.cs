﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.APIModels
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------

using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.APIModels
{
    public class EventParametersBase
    {
        public BacnetEventType EventType { get; set; }
    }
}
