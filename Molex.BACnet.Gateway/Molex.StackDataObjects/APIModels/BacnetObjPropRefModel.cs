﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.APIModels
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.APIModels
{
    public class BacnetObjPropRefModel
    {
        /** Object Instance*/
        public uint ObjectID { get; set; }
        /** Type of Object*/
        public BacnetObjectType ObjectType { get; set; }
        /** Array Index & its flag */
        /* For bool datatype we have to explicitly mention to allocate 1 byte to avoid packing issue.*/
        public bool ArrayIndexPresent { get; set; }
        public uint ArrayIndex { get; set; }
        /** Property Identifier*/
        public BacnetPropertyID PropertyIdentifier { get; set; }
    }
}
