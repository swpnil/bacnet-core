﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.APIModels
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------

namespace Molex.StackDataObjects.APIModels
{
    public class BacnetBitStrModel
    {
        /* Number of unused bits in last byte */
        public byte UnusedBits { get; set; }
        /* Number of used byte in array */
        public byte ByteCnt { get; set; }
        /* bit values */
        public byte[] TransBits { get; set; }
    }
}
