﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.APIModels
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.APIModels
{
    public class ReadRangeRequestModel
    {
        public byte ArrayIndexPresent { get; set; }
        public BacnetObjectType ObjectType { get; set; }
        public uint ObjectId { get; set; }
        public BacnetPropertyID PropertyId { get; set; }
        public uint ArrayIndex { get; set; }
        public BacnetReadRange RangeType { get; set; }
        public ReadRangeParam RangeParam { get; set; }
    }

    public class ReadRangeParam
    {
        public int Count { get; set; }
    }

    public class ReadRangeByPosition : ReadRangeParam
    {
        public uint ReferenceIndex { get; set; }
    }

    public class ReadRangeBySeqNo : ReadRangeParam
    {
        public uint ReferenceIndex { get; set; }
    }

    public class ReadRangeByTime : ReadRangeParam
    {
        public BacnetDateTimeModel dateTime { get; set; }
    }
}

