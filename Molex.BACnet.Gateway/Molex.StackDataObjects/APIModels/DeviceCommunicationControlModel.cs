﻿
using Molex.StackDataObjects.Constants;
namespace Molex.StackDataObjects.APIModels
{
    public class DeviceCommunicationControlModel
    {
        public uint TimeDuration { get; set; }
        public BacnetCommunicationState State { get; set; }
        public string Password { get; set; }
    }
}
