﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.APIModels
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------

namespace Molex.StackDataObjects.APIModels
{
    public class BacnetPropertyStatesModel
    {
        public uint PropState { get; set; }
        public uint AppTag { get; set; }
        public object BacnetPropState { get; set; }
    }
}
