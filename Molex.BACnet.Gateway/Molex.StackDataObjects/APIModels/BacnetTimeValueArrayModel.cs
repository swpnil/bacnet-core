﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.APIModels
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using System.Collections.Generic;

namespace Molex.StackDataObjects.APIModels
{
    public class BacnetTimeValueArrayModel
    {
        public int DayCount { get; set; }
        public List<BacnetTimeValueModel> BacnetTimeValueList { get; set; }
    }
}
