﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.APIModels
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.APIModels
{
    public class ReadPropertyRequestModel
    {
        public byte IsArrayIndexPresent { get; set; }
        public BacnetObjectType ObjectType { get; set; }
        public uint ObjectInstanceNumber { get; set; }
        public BacnetPropertyID ObjectProperty { get; set; }
        public uint ArrayIndex { get; set; }
        public object Tag { get; set; }
    }
}
