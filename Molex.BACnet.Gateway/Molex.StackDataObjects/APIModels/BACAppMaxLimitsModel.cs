﻿
namespace Molex.StackDataObjects.APIModels
{
    public class BACAppMaxLimitsModel
    {
        /* max initiate Q length */
        public uint InitiateQueNodes { get; set; }
        /* max BBMD initiate Q length */
        public uint BBMDInitiateQueNodes { get; set; }
        /* max count for BDT, FDT & register FD entries */
        public uint MaxBdtFdtEntries { get; set; }
        /* max objects that can be created in single device */
        public uint MaxObject { get; set; }
        /* max count for dynamic address binding nodes */
        public uint MaxDynamicAddrBind { get; set; }
        /* max count for COV subscriptions which can be sent by stack  */
        public uint MaxCovSubsTx { get; set; }
        /* max count for properties or data-types */
        public uint MaxStateText { get; set; }
        public uint MaxAFValuesList { get; set; }
        public uint MaxExSchdList { get; set; }
        public uint MaxTimeValueList { get; set; }
        public uint MaxDateList { get; set; }
        public uint MaxDestinationList { get; set; }
        public uint MaxDevObjPropRefList { get; set; }
        public uint MaxEventParaList { get; set; }
        public uint MaxObjIdList { get; set; }
        public uint MaxActiveCovSubs { get; set; }
        /* max count for static address binding nodes */
        public uint MaxStaticAddrBind { get; set; }
        /* max count for network analyzability nodes */
        public uint MaxNwAnalyzability { get; set; }
        /* max b-side callback notification Q length */
        public uint CbNotifyQueNodes { get; set; }
        /* max count for fualt paremeters data-type */
        public uint MaxFaultParaList { get; set; }
        /* max count for COVU subsription list */
        public uint MaxCovUSubs { get; set; }
        /* max count for subordinate list property */
        public uint MaxSubordinateList { get; set; }
    }
}
