﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.APIModels
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------

namespace Molex.StackDataObjects.APIModels
{
    public class BacnetAddressModel
    {
        public byte IPAddressLength { get; set; }
        public byte MacLength { get; set; }
        public ushort NetworkNumber { get; set; }
        public string IPAddress { get; set; }
        public uint Port { get; set; }
        public string MacAddress { get; set; }
    }
}
