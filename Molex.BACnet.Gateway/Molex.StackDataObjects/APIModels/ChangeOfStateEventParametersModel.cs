﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.APIModels
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------

using Molex.StackDataObjects.Constants;
using System.Collections.Generic;
namespace Molex.StackDataObjects.APIModels
{
    public class ChangeOfStateEventParametersModel : EventParametersBase
    {
        public ChangeOfStateEventParametersModel()
        {
            PropertyStates = new List<PropertyStatesModel>();
        }

        public uint TimeDelay { get; set; }
        public List<PropertyStatesModel> PropertyStates { get; set; }
    }

    public class PropertyStatesModel
    {
        public BacnetPropertyStates PropertyState { get; set; }
        public BacnetApplicationTag ApplicationTag { get; set; }
        public object Value { get; set; }
    }
}
