﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.APIModels
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------

namespace Molex.StackDataObjects.APIModels
{
    public class TimeStampModel
    {
        public BacnetTimeModel Time { get; set; }

        public BacnetDateTimeModel DateTime { get; set; }

        public uint SeqNo { get; set; }
    }
}
