﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.StackDataObjects.APIModels
{
    public class TimeSyncModel
    {
        public DateTime DateTimeSync { get; set; }
        public bool IsUTCFormat { get; set; }
    }
}
