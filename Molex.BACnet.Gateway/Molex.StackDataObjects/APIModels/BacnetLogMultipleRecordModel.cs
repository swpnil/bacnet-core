﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.APIModels
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using System.Collections.Generic;

namespace Molex.StackDataObjects.APIModels
{
    public class BacnetLogMultipleRecordModel
    {
        public BacnetDateTimeModel TimeStamp { get; set; }
        public List<BacnetLogDataModel> BacnetLogData { get; set; }
        /** sequence no of log record */
        public uint SequenceNo { get; set; }
    }

    public class BacnetLogDataModel
    {
        public byte TagType { get; set; }
        public object BacnetLogData { get; set; }
    }
}
