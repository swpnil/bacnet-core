﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.APIModels
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.APIModels
{
    public class DeleteObjectModel
    {
        public uint DeviceId { get; set; }
        public BacnetObjectType ObjectType { get; set; }
        public uint ObjectId { get; set; }
    }
}
