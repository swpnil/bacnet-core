﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.APIModels
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using System.Collections.Generic;

namespace Molex.StackDataObjects.APIModels
{
    public class ChangeOfBitstringModel
    {
        public uint Timedelay { get; set; }

        public List<BacnetBitStrModel> BacnetBitStrList { get; set; }

        public ChangeOfBitstringModel()
        {
            BacnetBitStrList = new List<BacnetBitStrModel>();
        }
    }
}
