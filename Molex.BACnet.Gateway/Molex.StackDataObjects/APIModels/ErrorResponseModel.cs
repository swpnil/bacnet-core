﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.APIModels
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.APIModels
{
    public class BacnetErrorResponseModel
    {
        public BacnetErrorClass ErrorClass { get; set; }
        public BacnetErrorCode ErrorCode { get; set; }
        public int FirstFailedNo { get; set; }
    }
}
