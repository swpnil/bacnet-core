﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.APIModels
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------

namespace Molex.StackDataObjects.APIModels
{
    public class BacnetTimeModel
    {
        /* Hour value */
        public byte Hour { get; set; }
        /* Minutes value*/
        public byte Min { get; set; }
        /* Second value */
        public byte Sec { get; set; }
        /* Hundredths value */
        public byte Hundredths { get; set; }
    }
}
