﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.APIModels
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.APIModels
{
    public class ReadPropertyMultipleRequestModel
    {
        public byte ArrayIndexPresent { get; set; }
        public BacnetObjectType ObjectType { get; set; }
        public uint ObjectInstance { get; set; }
        public BacnetPropertyID ObjectProperty { get; set; }
        public uint ArrayIndex { get; set; }
        public object Tag { get; set; }
    }
}
