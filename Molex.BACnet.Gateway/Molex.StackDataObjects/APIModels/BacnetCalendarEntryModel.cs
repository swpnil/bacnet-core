﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.APIModels
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.APIModels
{
    public class BacnetCalendarEntryModel
    {
        public CalendarEntryStatus StatusCalendar { get; set; }
        // Can be of type 'BacnetDateModel'or'BacnetDateRangeModel'or'WeekNDayModel'
        public object BacnetCalenderEntry { get; set; }
    }
}
