﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.APIModels
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------


namespace Molex.StackDataObjects.APIModels
{
    public class ExtPropertyDataModel
    {
        public bool AlarmFlag { get; set; }
        public bool TrendFlag { get; set; }
        public uint EEObjectID { get; set; }
        public uint TLObjectID { get; set; }
    }
}
