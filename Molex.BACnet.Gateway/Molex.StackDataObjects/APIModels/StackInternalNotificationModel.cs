﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.APIModels
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------

using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.APIModels
{
    public class StackInternalNotificationModel
    {
        public uint DeviceId { get; set; }
        public uint ObjectId { get; set; }
        public BacnetObjectType ObjectType { get; set; }
        public BacnetCallbackType CallbackType { get; set; }
        public BacnetCallbackReason CallbackReason { get; set; }
    }
}
