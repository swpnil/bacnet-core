﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.APIModels
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Models;

namespace Molex.StackDataObjects.APIModels
{
    public class BacnetDestinationModel
    {
        public BacnetDestinationModel()
        {
            FromTime = new BacnetTimeModel();
            ToTime = new BacnetTimeModel();
            DaysOfWeek = new DaysOfWeekBitsModel();
            EventTransitions = new EventTransitionBitsModel();
            BacnetRecipient = new BacnetRecipientModel();
        }

        public bool IssueConfirmedNotification { get; set; }
        public uint ProcessID { get; set; }
        public BacnetTimeModel FromTime { get; set; }
        public BacnetTimeModel ToTime { get; set; }
        public DaysOfWeekBitsModel DaysOfWeek { get; set; }
        public EventTransitionBitsModel EventTransitions { get; set; }
        public BacnetRecipientModel BacnetRecipient { get; set; }
    }
}
