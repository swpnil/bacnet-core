﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.APIModels
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.APIModels
{
    public class WeekNDayModel
    {
        /** Month */
        public BacnetMonth Month { get; set; }
        /** Week */
        public BacnetWeekOfMonth WeekOfMonth { get; set; }
        /** Days */
        public BacnetWeekDay WeekNDay { get; set; }
    }
}
