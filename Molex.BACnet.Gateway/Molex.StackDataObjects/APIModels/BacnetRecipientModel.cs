﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.APIModels
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.APIModels
{
    public class BacnetRecipientModel
    {
        public BacnetRecipientModel()
        {
            BacnetAddress = new BacnetAddressModel();
        }

        public DestinationType DestinationType { get; set; }

        public BacnetObjectType ObjectType { get; set; }
        /** Object Instance*/
        public uint ObjId { get; set; }

        public BacnetAddressModel BacnetAddress { get; set; }
    }
}
