﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.APIModels
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using System.Collections.Generic;
using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.APIModels
{
    public class BacnetSpecialEventModel
    {
        public object CalenderReference { get; set; }
        public List<BacnetTimeValueModel> BacnetTimeValue { get; set; }
        public uint EventPriority { get; set; }
        public CalendarEntryStatus StatusCalendar { get; set; }

        public BacnetSpecialEventModel()
        {
            BacnetTimeValue = new List<BacnetTimeValueModel>();
        }
    }
}
