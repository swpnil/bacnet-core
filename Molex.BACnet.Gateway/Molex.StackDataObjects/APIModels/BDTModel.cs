﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.APIModels
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------

namespace Molex.StackDataObjects.APIModels
{
    public class BDTModel
    {
        public string IPAddress { get; set; }
        public ushort Port { get; set; }
        public byte BroadcastMask { get; set; }
    }
}
