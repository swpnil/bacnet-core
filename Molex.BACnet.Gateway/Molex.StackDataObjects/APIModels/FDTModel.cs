﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.APIModels
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------

namespace Molex.StackDataObjects.APIModels
{
    public class FDTModel
    {
        public ushort TimeToLive { get; set; }
        public ushort TimeRemaining { get; set; }
        public string IPAddress { get; set; }
        public ushort Port { get; set; }
    }
}
