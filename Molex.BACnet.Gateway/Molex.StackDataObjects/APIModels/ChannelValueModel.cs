﻿using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.APIModels
{
    public class ChannelValueModel
    {
        public BacnetDataType DataType { get; set; }
        public BacnetApplicationTag TagType { get; set; }
        public object Value { get; set; }
    }
}
