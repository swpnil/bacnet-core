﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.APIModels
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------

namespace Molex.StackDataObjects.APIModels
{
    public class BacnetDateRangeModel
    {
        /** start date*/
        public BacnetDateModel StartDate { get; set; }
        /** end date */
        public BacnetDateModel EndDate { get; set; }

        public BacnetDateRangeModel()
        {
            StartDate = new BacnetDateModel();
            EndDate = new BacnetDateModel();
        }
    }
}
