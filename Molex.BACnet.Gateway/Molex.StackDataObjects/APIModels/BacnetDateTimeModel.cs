﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.APIModels
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------

namespace Molex.StackDataObjects.APIModels
{
    public class BacnetDateTimeModel
    {
        public BacnetTimeModel Time { get; set; }
        public BacnetDateModel Date { get; set; }

        public BacnetDateTimeModel()
        {
            Time = new BacnetTimeModel();
            Date = new BacnetDateModel();
        }
    }
}
