﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.APIModels
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.APIModels
{
    public class BacnetDevObjRefModel
    {
        public bool DeviceIDPresent { get; set; }
        /* Type of Object */
        public BacnetObjectType ObjectType { get; set; }
        /* Device Type */
        public BacnetObjectType DeviceType { get; set; }
        /* Object Instance */
        public uint ObjectID { get; set; }
        /* Device Instance */
        public uint DeviceID { get; set; }
    }
}
