﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.APIModels
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.APIModels.NotificationModels;
using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.APIModels
{
    public class BacnetNotificationParametersModel
    {
        public BacnetEventType EventType { get; set; }
        public bool DecodeFailure { get; set; }
        public BacnetNotificationParamBase BacnetNotificationParameters { get; set; }
    }
}
