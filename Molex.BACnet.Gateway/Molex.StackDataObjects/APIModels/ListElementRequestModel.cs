﻿using Molex.StackDataObjects.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.StackDataObjects.APIModels
{
    public class ListElementRequestModel
    {
        public uint DeviceId { get; set; }

        public uint ObjectId { get; set; }
        
        public BacnetObjectType ObjectType { get; set; }
        
        public BacnetCallbackType CallbackType { get; set; }

        public BacnetCallbackReason CallbackReason { get; set; }

        public BacnetPropertyID PropertyType { get; set; }
    }
}
