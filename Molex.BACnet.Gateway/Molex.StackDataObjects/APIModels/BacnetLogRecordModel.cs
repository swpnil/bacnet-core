﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.APIModels
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Models;
using System.Collections.Generic;

namespace Molex.StackDataObjects.APIModels
{
    public class BacnetLogRecordModel
    {
        public LogRecordModel LogRecord { get; set; }
        public uint SequenceNo { get; set; }
    }

    public class LogRecordModel
    {
        public BacnetDateTimeModel TimeStamp { get; set; }
        public LogDatumModel LogDatum { get; set; }
        public StatusFlagsModel StatusFlag { get; set; }
    }

    public class LogDatumModel
    {
        public byte TagType { get; set; }
        public object LogData { get; set; }
    }

    public class BacnetLogDatum
    {
        public BacnetLogStatusModel BacnetLogStatus { get; set; }
        public bool BooleanVal { get; set; }
        public float RealVal { get; set; }
        public uint EnumVal { get; set; }
        public uint UnsignedIntVal { get; set; }
        public int SignedIntVal { get; set; }
        public double DoubleVal { get; set; }
        public byte[] ByteVal { get; set; }
        public List<BacnetBitStrModel> BacnetBitStr { get; set; }
        public BacnetErrorResponseModel ErrorResponse { get; set; }
        public float TimeChange { get; set; }
        public List<BacnetPropertyValueModel> BacnetPropertyValue { get; set; }
    }

    public class BacnetLogStatusModel
    {
        public byte UnusedBits { get; set; }
        public byte ByteCount { get; set; }
        public object TransactionBits { get; set; }
    }

}


