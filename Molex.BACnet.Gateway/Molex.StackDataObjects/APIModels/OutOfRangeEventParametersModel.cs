﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.APIModels
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------

namespace Molex.StackDataObjects.APIModels
{
    public class OutOfRangeEventParametersModel : EventParametersBase
    {
        public uint TimeDelay { get; set; }
        public float LowLimit { get; set; }
        public float HighLimit { get; set; }
        public float DeadBand { get; set; }
    }
}
