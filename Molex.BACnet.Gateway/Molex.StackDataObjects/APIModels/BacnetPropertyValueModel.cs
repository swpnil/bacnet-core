﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.APIModels
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------

namespace Molex.StackDataObjects.APIModels
{
    public class BacnetPropertyValueModel
    {
        public byte ContextSpecific { get; set; }
        public byte StartTag { get; set; }
        public byte ContextTag { get; set; }
        public byte TagType { get; set; }
        public ValueModel Value { get; set; }
    }

    public class ValueModel
    {
        public bool BooleanVal { get; set; }
        public uint UnsignedIntVal { get; set; }
        public int SignedIntVal { get; set; }
        public float RealVal { get; set; }
        public double DoubleVal { get; set; }
        public string OctetString { get; set; }
        public string CharString { get; set; }
        public string BitString { get; set; }
        public uint EnumVAl { get; set; }
        public BacnetDateModel BacnetDate { get; set; }
        public BacnetTimeModel BacnetTime { get; set; }
        public ObjectIdentifierModel ObjectId { get; set; }
        public object PvVal { get; set; }
    }
}
