﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.APIModels
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Constants;
using System.Collections.Generic;

namespace Molex.StackDataObjects.APIModels
{
    public class CovNotificationModel
    {
        public byte SendSimpleAck { get; set; }
        public byte NotificationType { get; set; }
        public BacnetObjectType MonitoredObjectType { get; set; }
        public uint MonitoredObjectInstance { get; set; }
        public uint DeviceInstance { get; set; }
        public uint ProcessIdentifier { get; set; }
        public uint Lifetime { get; set; }
        public List<PropertyValueModel> COVValue { get; set; }
        public float CovIncrement { get; set; }
        public BacnetAddressModel BacnetDeviceAddress { get; set; }
    }
}
