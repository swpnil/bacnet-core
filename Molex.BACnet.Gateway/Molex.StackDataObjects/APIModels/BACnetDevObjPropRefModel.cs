﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.APIModels
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.APIModels
{
    public class BACnetDevObjPropRefModel
    {
        public BacnetObjectType ObjectType { get; set; }
        public BacnetObjectType DeviceType { get; set; }
        /** Object Instance*/
        public uint ObjId { get; set; }
        /** Device Instance*/
        public uint DeviceInstance { get; set; }
        /** Device Type & its flag */
        public bool DeviceIdPresent { get; set; }
        /** Array Index & its flag */
        public bool ArrIndxPresent { get; set; }
        public uint ArrayIndex { get; set; }
        /** Property Identifier*/
        public BacnetPropertyID PropertyIdentifier { get; set; }
    }
}
