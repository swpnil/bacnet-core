﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.APIModels
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------

using Molex.StackDataObjects.Constants;
namespace Molex.StackDataObjects.APIModels
{
    public class CommandFailureEventParametersModel : EventParametersBase
    {
        public CommandFailureEventParametersModel()
        {
            BacnetObjectPropertyReference = new BACnetDevObjPropRefModel();
        }

        public BacnetApplicationTag ApplicationTag { get; set; }
        public uint TimeDelay { get; set; }
        public BACnetDevObjPropRefModel BacnetObjectPropertyReference { get; set; }
        public object Value { get; set; }
    }
}
