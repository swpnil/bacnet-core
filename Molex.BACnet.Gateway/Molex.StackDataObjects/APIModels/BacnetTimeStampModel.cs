﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.APIModels
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.APIModels
{
    public class BacnetTimeStampModel
    {
        /*Indicates type of time-stamp*/
        public BacnetTimeStampType TimeStampType { get; set; }

        public TimeStampModel TimeStamp { get; set; }
    }
}
