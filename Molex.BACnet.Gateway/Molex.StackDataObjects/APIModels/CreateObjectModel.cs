﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.APIModels
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using System.Collections.Generic;
using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.APIModels
{
    public class CreateObjectModel
    {
        public BacnetObjectType ObjectType { get; set; }
        public uint ObjectId { get; set; }
        public uint DeviceId { get; set; }
        public bool IsObjTypeSpecifier { get; set; }
        public string DeviceObjectID { get; set; }
        public List<PropertyValueModel> PropertyValues { get; set; }
    }
}
