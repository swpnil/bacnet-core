﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.APIModels
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using System.Collections.Generic;

namespace Molex.StackDataObjects.APIModels
{
    public class BacnetTimeValueModel
    {
        public BacnetTimeModel Time { get; set; }
        public List<BacnetValueModel> Values { get; set; }

        public BacnetTimeValueModel()
        {
            Values = new List<BacnetValueModel>();
            Time = new BacnetTimeModel();
        }
    }

    public class BacnetValueModel
    {
        public byte TagType { get; set; }
        public object Value { get; set; }
    }
}
