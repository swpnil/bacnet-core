﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.APIModels
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using System.Collections.Generic;

namespace Molex.StackDataObjects.APIModels
{
    public class ChangeOfStateModel
    {
        public uint Timedelay { get; set; }
        public List<BacnetPropertyStatesModel> BacnetPropertyStatesList { get; set; }

        public ChangeOfStateModel()
        {
            BacnetPropertyStatesList = new List<BacnetPropertyStatesModel>();
        }
    }
}
