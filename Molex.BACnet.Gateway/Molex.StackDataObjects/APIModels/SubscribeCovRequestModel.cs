﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.APIModels
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.APIModels
{
    public class SubscribeCovRequestModel
    {
        public byte Subscribe { get; set; }
        public byte NotificationType { get; set; }
        public BacnetObjectType MonitoredObjectType { get; set; }
        public uint MonitoredObjectInstance { get; set; }
        public uint ProcessIdentifier { get; set; }
        public uint Lifetime { get; set; }
    }
}
