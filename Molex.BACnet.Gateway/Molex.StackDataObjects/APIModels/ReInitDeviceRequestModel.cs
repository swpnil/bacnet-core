﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       UniDEL Pte. Ltd
///   Class:              ReInitDeviceRequestModel
///   Description:        Reinitalize device model for Stack service
///   Author:             Prasad Joshi                  
///   Date:               06/20/17
#endregion

using Molex.StackDataObjects.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.StackDataObjects.APIModels
{
    /// <summary>
    /// This class as Reinitialize device model for callback processor
    /// </summary>
    /// <CreatedBy>prasad.joshi</CreatedBy><TimeStamp>6/20/20171:48 PM</TimeStamp>
    public class ReInitDeviceRequestModel
    {
        /// <summary>
        /// Gets or sets the state of the re initialize.
        /// </summary>
        /// <value>
        /// The state of the re initialize.
        /// </value>
        /// <CreatedBy>prasad.joshi</CreatedBy><TimeStamp>6/20/201712:42 PM</TimeStamp>
        public BacnetReinitializedState ReInitState { get; set; }

        /// <summary>
        /// Gets or sets the device password.
        /// </summary>
        /// <value>
        /// The device password.
        /// </value>
        /// <CreatedBy>prasad.joshi</CreatedBy><TimeStamp>6/20/201712:42 PM</TimeStamp>
        public string DevicePassword { get; set; }
    }
}
