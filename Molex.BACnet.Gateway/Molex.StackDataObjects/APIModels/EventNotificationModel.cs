﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.APIModels
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.APIModels
{
    public class EventNotificationModel
    {
        public byte SendSimpleAck { get; set; }
        public byte IsConfirmedNotification { get; set; }
        public byte Priority { get; set; }
        public byte AckRequired { get; set; }
        public BacnetObjectType InitiatingObjectType { get; set; }
        public uint InitiatingObjectId { get; set; }
        public uint InitiatingDeviceId { get; set; }
        public uint ProcessIdentifier { get; set; }
        public uint NotificationClass { get; set; }
        public BacnetEventType EventType { get; set; }
        public BacnetNotifyType NotifyType { get; set; }
        public BacnetEventState FromState { get; set; }
        public BacnetEventState ToState { get; set; }
        public string CharStringMessage { get; set; }
        public BacnetTimeStampModel TimeStamp { get; set; }
        public object EventValues { get; set; }
        public BacnetAddressModel BacnetDeviceAddress { get; set; }
        public BacnetNotificationParametersModel NotifyParameters { get; set; }
    }
}
