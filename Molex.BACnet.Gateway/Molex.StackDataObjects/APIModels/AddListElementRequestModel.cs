﻿using Molex.StackDataObjects.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.StackDataObjects.APIModels
{
    public class AddListElementRequestModel
    {
        public uint DeviceId { get; set; }
        public byte ArrayIndexPresent { get; set; }
        public BacnetObjectType ObjectType { get; set; }
        public uint ObjectInstance { get; set; }
        public BacnetPropertyID ObjectProperty { get; set; }
        public uint ArrayIndex { get; set; }
        public uint Priority { get; set; }
        public BacnetDataType DataType { get; set; }
        /// <summary>
        /// This value represents the actual property value of object.
        /// In case of propiority array this value may be differ from input value.
        /// </summary>
        public object PropertyValue { get; set; }
        /// <summary>
        /// This value represents the input value for object property.
        /// </summary>
        public object InputValue { get; set; }
    }
}
