﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Request
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.Request
{
    public class ChangeIpAddressPortNoRequest : RequestBase
    {
        public ChangeIpAddressPortNoRequest()
        {
            ReqType = StackConstants.RequestType.BACDEL_Change_IpAddress_PortNo;
        }

        public ushort Port { get; set; }
        public string IPAddress { get; set; }
    }
}
