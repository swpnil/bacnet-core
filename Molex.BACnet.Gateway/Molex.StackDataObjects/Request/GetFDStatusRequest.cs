﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Request
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.Request
{
    public class GetFDStatusRequest : RequestBase
    {
        public GetFDStatusRequest()
        {
            ReqType = StackConstants.RequestType.BACDEL_Get_FD_Status;
        }
    }
}
