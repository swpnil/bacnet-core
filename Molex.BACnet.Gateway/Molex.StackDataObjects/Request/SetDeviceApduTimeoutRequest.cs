﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Request
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.Request
{
    public class SetDeviceApduTimeoutRequest : RequestBase
    {
        public SetDeviceApduTimeoutRequest()
        {
            ReqType = StackConstants.RequestType.BACDEL_Set_Device_Apdu_Timeout;
        }

        public uint DeviceId { get; set; }
        public uint ApduTimeout { get; set; }
        public bool IsAllDevice { get; set; }
    }
}
