﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Request
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------

namespace Molex.StackDataObjects.Request
{
    public class UpdateCovASubscriptionLifeTimeRequest : RequestBase
    {
        public UpdateCovASubscriptionLifeTimeRequest()
        {
            ReqType = Constants.StackConstants.RequestType.BACDEL_Update_COVA_Subscription_LifeTime;
        }

        public uint LifeTime { get; set; }
    }
}
