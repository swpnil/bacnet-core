﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Request
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.Request
{
    public class RegisterFDRequest : RequestBase
    {
        public RegisterFDRequest()
        {
            ReqType = StackConstants.RequestType.BACDEL_BBMD_REQ_REGISTER_FD;
        }

        public string IPAddress { get; set; }
        public ushort Port { get; set; }
        public ushort TimeToLive { get; set; }
    }
}
