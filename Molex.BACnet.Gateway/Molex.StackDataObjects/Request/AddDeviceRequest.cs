﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Request
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.Request
{
    public class AddDeviceRequest : RequestBase
    {
        public AddDeviceRequest()
        {
            ReqType = Constants.StackConstants.RequestType.BACDEL_Add_Device;
        }

        public uint DeviceAdr { get; set; }
        public uint DeviceID { get; set; }
        public ushort SNet { get; set; }
    }
}
