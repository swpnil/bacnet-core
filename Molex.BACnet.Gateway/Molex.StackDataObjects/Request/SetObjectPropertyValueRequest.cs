﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Request
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------

using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.Request
{
    public class SetObjectPropertyValueRequest : RequestBase
    {
        public SetObjectPropertyValueRequest()
        {
            ReqType = Constants.StackConstants.RequestType.BACDEL_Set_Object_Property;
        }

        public uint SourceDeviceId { get; set; }
        public uint ObjectID { get; set; }
        public BacnetObjectType ObjectType { get; set; }
        public BacnetPropertyID ObjectProperty { get; set; }
        public uint ArrayIndex { get; set; }
        public bool IsArrayIndexPresent { get; set; }
        public byte Priority { get; set; }
        public object PropertyValue { get; set; }
        public BacnetDataType DataType { get; set; }
    }
}
