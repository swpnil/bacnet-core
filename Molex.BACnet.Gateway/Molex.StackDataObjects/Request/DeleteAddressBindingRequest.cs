﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Request
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.APIModels;
using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.Request
{
    public class DeleteAddressBindingRequest : RequestBase
    {
        public DeleteAddressBindingRequest()
        {
            ReqType = StackConstants.RequestType.BACDEL_Delete_Dynamic_Device_Address_Binding;
        }

        public int DeviceId { get; set; }
        public BacnetAddressModel DestinationAddress { get; set; }
    }
}
