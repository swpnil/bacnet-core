﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Request
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.Request
{
    public class DisableBBMDRequest : RequestBase
    {
        public DisableBBMDRequest()
        {
            ReqType = StackConstants.RequestType.BACDEL_Disable_BBMD;
        }
    }
}
