﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Request
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------

using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.Request
{
    public class RemoveObjectRequest : RequestBase
    {
        public RemoveObjectRequest()
        {
            ReqType = Constants.StackConstants.RequestType.BACDEL_Delete_Object;
        }

        public uint SourceDeviceId { get; set; }
        public BacnetObjectType ObjectType { get; set; }
        public uint ObjectID { get; set; }
    }
}
