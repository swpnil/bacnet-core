﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Request
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.Request
{
    public class SetDeviceApduRetriesRequest : RequestBase
    {
        public SetDeviceApduRetriesRequest()
        {
            ReqType = StackConstants.RequestType.BACDEL_Set_Device_Apdu_Retries;
        }

        public uint DeviceId { get; set; }
        public uint ApduRetries { get; set; }
        public bool IsAllDevice { get; set; }
    }
}
