﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Request
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.Request
{
    public class ReadRegisterFDListRequest : RequestBase
    {
        public ReadRegisterFDListRequest()
        {
            ReqType = StackConstants.RequestType.BACDEL_BBMD_READ_REG_FD_LIST;
        }
    }
}
