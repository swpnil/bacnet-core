﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Request
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using System;
using System.Collections.Generic;
using Molex.StackDataObjects.APIModels;
using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.Request
{
    public class ReadPropertyMultipleRequest : RequestBase
    {
        public ReadPropertyMultipleRequest()
        {
            ReqType = StackConstants.RequestType.BACDEL_Send_RPM;
        }

        public uint SourceDeviceId { get; set; }
        public uint DestinationDeviceId { get; set; }
        public BacnetAddressModel DestinationAddress { get; set; }
        public bool DestinationTypeFlag { get; set; }
        public List<ReadPropertyMultipleRequestModel> RpmRequest { get; set; }
        public Action<object> onCallBack { get; set; }
    }
}
