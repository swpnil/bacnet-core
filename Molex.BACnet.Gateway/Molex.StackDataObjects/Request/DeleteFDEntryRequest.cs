﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Request
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.Request
{
    public class DeleteFDEntryRequest : RequestBase
    {
        public DeleteFDEntryRequest()
        {
            ReqType = StackConstants.RequestType.BACDEL_BBMD_REQ_DELETE_FDT_ENTRY;
        }

        public string DestinationIPAddress { get; set; }
        public ushort DestinationPort { get; set; }
        public string IPAddress { get; set; }
        public ushort Port { get; set; }
        public byte BroadcastMask { get; set; }
    }
}
