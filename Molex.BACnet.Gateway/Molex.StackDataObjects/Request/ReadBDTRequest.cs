﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Request
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.Request
{
    public class ReadBDTRequest : RequestBase
    {
        public ReadBDTRequest()
        {
            ReqType = StackConstants.RequestType.BACDEL_BBMD_REQ_READ_BDT;
        }

        public string IPAddress { get; set; }
        public ushort Port { get; set; }
    }
}
