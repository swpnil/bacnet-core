﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Request
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.Request
{
    public class ClearDeviceObjectRequest : RequestBase
    {
        public ClearDeviceObjectRequest()
        {
            ReqType = StackConstants.RequestType.BACDEL_Clear_Device_Objects;
        }
    }
}
