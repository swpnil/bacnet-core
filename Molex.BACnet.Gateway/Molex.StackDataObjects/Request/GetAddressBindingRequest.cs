﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Request
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.Request
{
    public class GetAddressBindingRequest : RequestBase
    {
        public GetAddressBindingRequest()
        {
            ReqType = StackConstants.RequestType.BACDEL_Get_Dynamic_Device_Address_Binding;
        }
    }
}
