﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Request
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using System;
using Molex.StackDataObjects.APIModels;
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Response;

namespace Molex.StackDataObjects.Request
{
    public class WhoIsRequest : RequestBase
    {
        public WhoIsRequest()
        {
            ReqType = StackConstants.RequestType.BACDEL_Send_Who_Is;
        }

        public uint SourceDeviceId { get; set; }
        public uint DestinationDeviceId { get; set; }
        public BacnetAddressModel DestinationAddress { get; set; }
        public bool DestinationTypeFlag { get; set; }
        public DestinationType DestinationType { get; set; }
        public bool IsNetworkLayerMessage { get; set; }
        public ushort NetworkNo { get; set; }
        public int LowLimit { get; set; }
        public int HighLimit { get; set; }

        private Action<ResponseResult> ActionCompletedSyncHandler { get; set; }
    }
}
