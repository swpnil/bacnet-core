﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Request
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.Request
{
    public class EnableBBMDRequest : RequestBase
    {
        public EnableBBMDRequest()
        {
            ReqType = StackConstants.RequestType.BACDEL_Enable_BBMD;
        }
    }
}
