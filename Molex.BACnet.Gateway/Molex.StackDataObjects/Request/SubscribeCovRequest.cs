﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Request
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.APIModels;
using Molex.StackDataObjects.Constants;
using System;

namespace Molex.StackDataObjects.Request
{
    public class SubscribeCovRequest : RequestBase
    {
        public SubscribeCovRequest()
        {
            ReqType = StackConstants.RequestType.BACDEL_SEND_SUBSCRIBE_COV;
        }

        public uint SourceDeviceId { get; set; }
        public uint DestinationDeviceId { get; set; }
        public BacnetAddressModel DestinationAddress { get; set; }
        public bool DestinationTypeFlag { get; set; }
        public SubscribeCovRequestModel SubscribeCovReq { get; set; }
        public Action<object> OnCallBack { get; set; }
    }
}
