﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Request
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.Request
{
    public class ChangeDeviceIdRequest : RequestBase
    {
        public ChangeDeviceIdRequest()
        {
            ReqType = StackConstants.RequestType.BACDEL_Change_Device_Id;
        }

        public uint OldDeviceId { get; set; }
        public uint NewDeviceId { get; set; }
        public uint NewDADR { get; set; }
    }
}
