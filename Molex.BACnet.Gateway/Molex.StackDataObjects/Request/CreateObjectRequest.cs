﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Request
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using System;
using Molex.StackDataObjects.APIModels;
using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.Request
{
    public class CreateObjectRequest : RequestBase
    {
        public CreateObjectRequest()
        {
            ReqType = StackConstants.RequestType.BACDEL_Send_Create_Object;
        }

        public uint SourceDeviceId { get; set; }
        public uint DestinationDeviceId { get; set; }
        public BacnetAddressModel DestinationAddress { get; set; }
        public bool DestinationTypeFlag { get; set; }
        public CreateObjectModel CreateObjectData { get; set; }
        public Action<object> OnCallBack { get; set; }
    }
}
