﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Request
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.Request
{
    public class StackDeInit : RequestBase
    {
        public StackDeInit()
        {
            ReqType = StackConstants.RequestType.BACDEL_Stack_Deinit;
        }
    }
}
