﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       UniDEL Pte. Ltd
///   Class:              <SetVendorPasswordDataRequest.cs>
///   Description:        <This class is used for set Vendor Id and Password>
///   Author:             <Rupesh Saw>                  
///   Date:               06/06/17
#endregion

using Molex.StackDataObjects.StackInititializationModels;
namespace Molex.StackDataObjects.Request
{
    /// <summary>
    /// This class is used for set Vendor Id and Password
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>16-06-201705:05 PM</TimeStamp>
    /// <seealso cref="Molex.StackDataObjects.Request.RequestBase" />
    public class SetDevicePasswordRequest : RequestBase
    {
        public SetDevicePasswordRequest()
        {
            ReqType = Constants.StackConstants.RequestType.BACDEL_Set_Device_Password;
        }

        /// <summary>
        /// Gets or sets the vendor identifier.
        /// </summary>
        /// <value>
        /// The vendor identifier.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>16-06-201705:06 PM</TimeStamp>
        public int DeviceId { get; set; }

        /// <summary>
        /// Gets or sets the ba cnet password.
        /// </summary>
        /// <value>
        /// The ba cnet password.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>16-06-201705:06 PM</TimeStamp>
        public string BACnetPassword { get; set; }
    }
}
