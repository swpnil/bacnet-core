﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Request
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------

using Molex.StackDataObjects.APIModels;
namespace Molex.StackDataObjects.Request
{
    public class GetPresentValueFromBACAppRequest : RequestBase
    {
        public GetPresentValueFromBACAppRequest()
        {
            ReqType = Constants.StackConstants.RequestType.BACDEL_Get_PresentValue_From_BACapp;
        }

        public uint SourceDeviceId { get; set; }
        public ExtPropertyDataModel PVTriggerModel { get; set; }
        public BacnetValueModel BacnetValueModel { get; set; }
    }
}
