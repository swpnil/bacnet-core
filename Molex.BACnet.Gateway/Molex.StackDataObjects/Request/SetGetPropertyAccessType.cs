﻿using Molex.StackDataObjects.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.StackDataObjects.Request
{
    public class SetGetPropertyAccessType : RequestBase
    {
        public SetGetPropertyAccessType()
        {
            ReqType = Constants.StackConstants.RequestType.BACDEL_Set_Get_Property_AccessType;
        }

        public uint DeviceId { get; set; }
        public uint ObjectId { get; set; }
        public BacnetObjectType ObjectType { get; set; }
        public BacnetPropertyID PropertyID { get; set; }
        public PropAccessType AccessType { get; set; }
        public bool ReadOrWrite { get; set; }
    }
}
