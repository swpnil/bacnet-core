﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Request
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.Request
{
    public class GetBBMDStatusRequest : RequestBase
    {
        public GetBBMDStatusRequest()
        {
            ReqType = StackConstants.RequestType.BACDEL_Get_BBMD_Status;
        }
    }
}
