﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Request
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using System;
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Response;

namespace Molex.StackDataObjects.Request
{
    public class RequestBase
    {
        private bool IsSendSucceed { get; set; }
        private int NoOfRequests { get; set; }
        private int SendAckReceived { get; set; }
        private int ResponseReceived { get; set; }
        public int PacketId { get; set; }
        public StackConstants.RequestType ReqType { get; set; }
        public Action<ResponseResult> ActionCompletedHandler { get; set; }
        public Object Tag { get; set; }
    }
}
