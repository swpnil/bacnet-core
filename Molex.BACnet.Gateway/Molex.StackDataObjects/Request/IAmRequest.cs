﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Request
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.APIModels;
using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.Request
{
    public class IAmRequest : RequestBase
    {
        public IAmRequest()
        {
            ReqType = StackConstants.RequestType.BACDEL_Send_I_Am;
        }

        public uint SourceDeviceId { get; set; }
        public uint DestinationDeviceId { get; set; }
        public BacnetAddressModel DestinationAddress { get; set; }
        public bool DestinationTypeFlag { get; set; }
        public DestinationType DestinationType { get; set; }
        public bool IsNetworkLayerMessage { get; set; }
    }
}
