﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Request
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using System.Collections.Generic;
using Molex.StackDataObjects.APIModels;
using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.Request
{
    public class WriteBDTRequest : RequestBase
    {
        public WriteBDTRequest()
        {
            ReqType = StackConstants.RequestType.BACDEL_BBMD_REQ_WRITE_BDT;
            BDTEntries = new List<BDTModel>();
        }

        public string IPAddress { get; set; }
        public ushort Port { get; set; }
        public List<BDTModel> BDTEntries { get; set; }
    }
}
