﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Request
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------

using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.Request
{
    public class GetObjectPropertyValueRequest : RequestBase
    {
        public GetObjectPropertyValueRequest()
        {
            ReqType = Constants.StackConstants.RequestType.BACDEL_Get_Object_Property;
        }

        public uint SourceDeviceId { get; set; }
        public uint ObjectID { get; set; }
        public BacnetObjectType ObjectType { get; set; }
        public BacnetPropertyID Property { get; set; }
        public uint ArrayIndex { get; set; }
        public bool IsArrayIndexPresent { get; set; }
    }
}
