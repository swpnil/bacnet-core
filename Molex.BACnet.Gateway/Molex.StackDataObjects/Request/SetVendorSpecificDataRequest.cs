﻿
using Molex.StackDataObjects.StackInititializationModels;
namespace Molex.StackDataObjects.Request
{
    public class SetVendorSpecificDataRequest : RequestBase
    {
        public SetVendorSpecificDataRequest()
        {
            ReqType = Constants.StackConstants.RequestType.BACDEL_Set_Vendor_Specific_data;
        }

        public VendorModel VendorData { get; set; }
    }
}
