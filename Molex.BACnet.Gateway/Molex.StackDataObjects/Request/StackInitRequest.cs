﻿using Molex.StackDataObjects.StackInititializationModels;

namespace Molex.StackDataObjects.Request
{
    public class StackInitRequest : RequestBase
    {
        public StackInitRequest()
        {
            ReqType = Constants.StackConstants.RequestType.BACDEL_Stack_Init;
        }

        public StackConfigModel StackInitConfiguration { get; set; }
    }
}
