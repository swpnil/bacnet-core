﻿
namespace Molex.StackDataObjects.Request
{
    public class CreateDirectoryRequest : RequestBase
    {
        public CreateDirectoryRequest()
        {
            ReqType = Constants.StackConstants.RequestType.BACDEL_Create_Directory;
        }
    }
}
