﻿using Molex.StackDataObjects.APIModels;
using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.Request
{
    public class SetMaxLimitsRequest : RequestBase
    {
        public SetMaxLimitsRequest()
        {
            ReqType = Constants.StackConstants.RequestType.BACDEL_Set_Max_Limits;
        }

        public BACAppMaxLimitsModel BACAppMaxLimits { get; set; }
        public BacnetMaxlimitParameters FirstFailedLimit { get; set; }
        public BacnetMaxlimitParameters LimitType { get; set; }
        public bool SetSpecifiedLimit { get; set; }
    }
}
