﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Constants
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using System.Xml.Serialization;
namespace Molex.StackDataObjects.Constants
{
    public class StackConstants
    {
        public const uint TEST_DEVICE_INSTANCE_NUMBER = 1;

        public enum RequestType
        {
            BACDEL_Add_Device,
            BACDEL_Add_Object,

            //BACDEL_Delete_Device,
            BACDEL_Delete_Object,

            BACDEL_Clear_Device_Objects,
            BACDEL_Stack_Init,
            BACDEL_Stack_Deinit,
            BACDEL_Enable_Socket_Communication,
            BACDEL_Disable_Socket_Communication,
            BACDEL_Set_Character_Encoding,
            BACDEL_Change_IpAddress_PortNo,
            BACDEL_Get_Property_Tag,
            BACDEL_Set_Get_Property_AccessType,
            BACDEL_Init_Device_Properties,
            BACDEL_Get_Property_DataType,
            BACDEL_Get_Object_Property,
            BACDEL_Set_Object_Property,
            BACDEL_Set_Device_Password,
            BACDEL_Set_Device_Apdu_Retries,
            BACDEL_Set_Device_Apdu_Timeout,
            BACDEL_Change_Device_Id,
            BACDEL_Set_Vendor_Specific_data,
            BACDEL_Set_Segmentation_WindowSize,
            BACDEL_Generate_Aside_Request,
            BACDEL_Set_Default_Parameters,
            BACDEL_Generate_Aside_Request_Using_DeviceID,
            BACDEL_Get_Device_ID_Or_IP_Address,
            BACDEL_Get_Router_IP_From_Network_No,
            BACDEL_Get_Sizeof_Object,
            BACDEL_Check_Object_Support,
            BACDEL_App_CallBack_Register,
            BACDEL_App_CallBack_UnRegister,
            BACDEL_Send_I_Have,
            BACDEL_ReInitDevice,
            BACDEL_Send_Who_Is,
            BACDEL_Send_I_Am,
            BACDEL_Send_WPM,
            BACDEL_Send_Who_Has,
            BACDEL_Send_RP,
            BACDEL_Send_WP,
            BACDEL_Send_RPM,
            BACDEL_Send_PT,
            BACDEL_Send_Create_Object,
            BACDEL_Send_Delete_Object,
            BACDEL_Send_RR,
            BACDEL_Get_Dynamic_Device_Address_Binding,
            BACDEL_Delete_Dynamic_Device_Address_Binding,

            BACDEL_Enable_BBMD,
            BACDEL_Disable_BBMD,
            BACDEL_Generate_BBMD_Request,
            BACDEL_Get_BBMD_Status,
            BACDEL_Get_FD_Status,

            BACDEL_BBMD_REQ_READ_BDT = 71,				/* Read BDT */
            BACDEL_BBMD_REQ_READ_FDT = 72,				/* read FDT */
            BACDEL_BBMD_REQ_WRITE_BDT = 73,				/* Write BDT */
            BACDEL_BBMD_REQ_DELETE_FDT_ENTRY = 74,		/* Delete FDT */
            BACDEL_BBMD_REQ_REGISTER_FD = 75,			/* Register as FD */
            BACDEL_BBMD_READ_REG_FD_LIST = 76,			/* Read FD list from stack */
            BACDEL_SEND_SUBSCRIBE_COV = 77,
            BACDEL_Get_PresentValue_From_BACapp,
            BACDEL_Update_COVA_Subscription_LifeTime,
            BACDEL_Create_Directory,
            BACDEL_Set_Max_Limits,
        }

        public const byte DESTINATION_TYPE_BROAD_CAST = 3;
        public const byte DESTINATION_TYPE_UNICAST_CAST = 0;

        public const int REQUEST_INVALID_ERROR = 1;

        public enum StackResponseCode
        {
            Success = 0,
            BACnetError = 1,
            StackError = 2,
            Retry = 253,
            Timeout = 254,
            IntegrationLayerError = 255,
        }

        public const string MAC_ADDRESS_BROAD_CAST = "Broadcast";

        public const int MAX_PROPERTY_VALUE_BYTE_LENGTH = 88;
        public const int MAX_CHARACTERSTRING_BYTE_LENGTH = 80;
        public const int MAX_INTEGRATION_STRUCTURE_SIZE = 304;
    }

    ///<summary>
    ///enum for destination address type in recipient list
    ///Name in Stack Application: DESTINATION_TYPE
    ///</summary>
    public enum DestinationType
    {
        DestinationIsDeviceID = 0,
        DestinationIsIpAddr = 1,
        DestinationIsLocalBroadcast = 2,
        DestinationIsGlobalBroadcast = 3,
        DestinationIsRemoteBroadcast = 4,
        DestinationIsMstpDevice = 5,
        DestinationIsUnicast = 6,
        DestinationIsUnrecognized = 7
    }

    /// <summary>
    /// Name in Stack Application: BACNET_PROPERTY_ID
    /// </summary>
    public enum BacnetPropertyID
    {
        PropAckedTransitions = 0,
        PropAckRequired = 1,
        PropAction = 2,
        PropActionText = 3,
        PropActiveText = 4,
        PropActiveVtSessions = 5,
        PropAlarmValue = 6,
        PropAlarmValues = 7,
        PropAll = 8,
        PropAllWritesSuccessful = 9,
        PropApduSegmentTimesOut = 10,
        PropApduTimesOut = 11,
        PropApplicationSoftwareVersion = 12,
        PropArchive = 13,
        PropBias = 14,
        PropChangeOfStateCount = 15,
        PropChangeOfStateTime = 16,
        PropNotificationClass = 17,
        PropBlank1 = 18,
        PropControlledVariableReference = 19,
        PropControlledVariableUnits = 20,
        PropControlledVariableValue = 21,
        PropCOVIncrement = 22,
        PropDateList = 23,
        PropDayLightSavingsStatus = 24,
        PropDeadBand = 25,
        PropDerivativeConstant = 26,
        PropDerivativeConstantUnits = 27,
        PropDescription = 28,
        PropDescriptionOfHalt = 29,
        PropDeviceAddressBinding = 30,
        PropDeviceType = 31,
        PropEffectivePeriod = 32,
        PropElapsedActiveTime = 33,
        PropErrorLimit = 34,
        PropEventEnable = 35,
        PropEventState = 36,
        PropEventType = 37,
        PropExceptionSchedule = 38,
        PropFaultValues = 39,
        PropFeedbackValue = 40,
        PropFileAccessMethod = 41,
        PropFileSize = 42,
        PropFileType = 43,
        PropFirmwareRevision = 44,
        PropHighLimit = 45,
        PropInactiveText = 46,
        PropInProcess = 47,
        PropInstanceOf = 48,
        PropIntegralConstant = 49,
        PropIntegralConstantUnits = 50,
        PropIssueConfirmedNotifications = 51, ///<summary> removed in version 1 Revision 4 </summary>
        PropLimitEnable = 52,
        PropListOfGroupMembers = 53,
        PropListOfObjectPropertyReferences = 54,
        PropProprietaryGridX = 10001,
        PropProprietaryGridY = 10002,
        //PropProprietaryColorTemperature = 10003,
        PropProprietaryMood = 10004,
        //PropProprietaryColorTemperatureUnit = 10005,
        ///<summary> enumeration value 55 is unassigned </summary>
        //PROPListOfSessionKey = 55,
        PropLocalDate = 56,

        PropLocalTime = 57,
        PropLocation = 58,
        PropLowLimit = 59,
        PropManipulatedVariableReference = 60,
        PropMaximumOutput = 61,
        PropMaxApduLengthAccepted = 62,
        PropMaxInfoFrames = 63,
        PropMaxMaster = 64,
        PropMaxPresValue = 65,
        PropMinimumOffTime = 66,
        PropMinimumOnTime = 67,
        PropMinimumOutput = 68,
        PropMinPresValue = 69,
        PropModelName = 70,
        PropModificationDate = 71,
        PropNotifyType = 72,
        PropNumberOfApduRetries = 73,
        PropNumberOfStates = 74,
        PropObjectIdentifier = 75,
        PropObjectList = 76,
        PropObjectName = 77,
        PropObjectPropertyReference = 78,
        PropObjectType = 79,
        PropOptional = 80,
        PropOutOfService = 81,
        PropOutputUnits = 82,
        PropEventParameters = 83,
        PropPolarity = 84,
        PropPresentValue = 85,
        PropPriority = 86,
        PropPriorityArray = 87,
        PropPriorityForWriting = 88,
        PropProcessIdentifier = 89,
        PropProgramChange = 90,
        PropProgramLocation = 91,
        PropProgramState = 92,
        PropProportionalConstant = 93,
        PropProportionalConstantUnits = 94,
        PropProtocolConformanceClass = 95, ///<summary> deleted in version 1 Revision 2 </summary>
        PropProtocolObjectTypesSupported = 96,
        PropProtocolServicesSupported = 97,
        PropProtocolVersion = 98,
        PropReadOnly = 99,
        PropReasonForHalt = 100,
        PropRecipient = 101, ///<summary> removed in version 1 Revision 4 </summary>
        PropRecipientList = 102,
        PropReliability = 103,
        PropRelinquishDefault = 104,
        PropRequired = 105,
        PropResolution = 106,
        PropSegmentationSupported = 107,
        PropSetPoint = 108,
        PropSetPointReference = 109,
        PropStateText = 110,
        PropStatusFlags = 111,
        PropSystemStatus = 112,
        PropTimeDelay = 113,
        PropTimeOfActiveTimeReset = 114,
        PropTimeOfStateCountReset = 115,
        PropTimeSynchronizationRecipients = 116,
        PropUnits = 117,
        PropUpdateInterval = 118,
        PropUtcOffSet = 119,
        PropVendorIdentifier = 120,
        PropVendorName = 121,
        PropVtClassesSupported = 122,
        PropWeeklySchedule = 123,
        PropAttemptedSamples = 124,
        PropAverageValue = 125,
        PropBufferSize = 126,
        PropClientCOVIncrement = 127,
        PropCOVResubscriptionInterval = 128,
        PropCurrentNotifyTime = 129, ///<summary> removed in version 1 revision 3 </summary>
        PropEventTimestamps = 130,
        PropLogBuffer = 131,
        PropLogDeviceObjectProperty = 132,

        /// <summary>
        /// The Enable property is renamed from log-Enabled in
        ///  Addendum b to ANSI/ASHRAE 135-2004(135b-2)
        /// </summary>
        PropEnable = 133,

        PropLogInterval = 134,
        PropMaximumValue = 135,
        PropMinimumValue = 136,
        PropNotificationThreshold = 137,
        PropPreviousNotifyTime = 138, ///<summary> removed in version 1 revision 3 </summary>
        PropProtocolRevision = 139,
        PropRecordsSinceNotification = 140,
        PropRecordCount = 141,
        PropStartTime = 142,
        PropStopTime = 143,
        PropStopWhenFull = 144,
        PropTotalRecordCount = 145,
        PropValidSamples = 146,
        PropWindowInterval = 147,
        PropWindowSamples = 148,
        PropMaximumValueTimestamp = 149,
        PropMinimumValueTimestamp = 150,
        PropVarianceValue = 151,
        PropActiveCOVSubscriptions = 152,
        PropBackupFailureTimesOut = 153,
        PropConfigurationFiles = 154,
        PropDatabaseRevision = 155,
        PropDirectReading = 156,
        PropLastRestoreTime = 157,
        PropMaintenanceRequired = 158,
        PropMemberOf = 159,
        PropMode = 160,
        PropOperationExpected = 161,
        PropSetting = 162,
        PropSilenced = 163,
        PropTrackingValue = 164,
        PropZoneMembers = 165,
        PropLifeSafetyAlarmValues = 166,
        PropMaxSegmentsAccepted = 167,
        PropProfileName = 168,
        PropAutoSlaveDiscovery = 169,
        PropManualSlaveAddressBinding = 170,
        PropSlaveAddressBinding = 171,
        PropSlaveProxyEnabled = 172,
        PropLastNotifyRecord = 173,
        PropScheduleDefault = 174,
        PropAcceptedModes = 175,
        PropAdjustValue = 176,
        PropCount = 177,
        PropCountBeforeChange = 178,
        PropCountChangeTime = 179,
        PropCOVPeriod = 180,
        PropInputReference = 181,
        PropLimitMonitoringInterval = 182,
        PropLoggingObject = 183,
        PropLoggingRecord = 184,
        PropPrescale = 185,
        PropPulseRate = 186,
        PropScale = 187,
        PropScaleFactor = 188,
        PropUpdateTime = 189,
        PropValueBeforeChange = 190,
        PropValueSet = 191,
        PropValueChangeTime = 192,
        PropAlignIntervals = 193,

        ///<summary> enumeration value 194 is unassigned </summary>
        //PropGroupMemberNames = 194,
        PropIntervalOffSet = 195,

        PropLastRestartReason = 196,

        ///<summary> enumeration values 198-201 are unassigned </summary>
        PropLoggingType = 197,

        //PropMemberStatusFlags = 198,
        //PropNotificationPeriod = 199,
        //PropPreviousNotifyRecord = 200,
        //PropRequestedUpdateInterval = 201,
        PropRestartNotificationRecipients = 202,

        PropTimeOfDeviceRestart = 203,
        PropTimeSynchronizationInterval = 204,
        PropTrigger = 205,
        PropUtcTimeSynchronizationRecipients = 206,

        ///<summary> enumerations 207-211 are used in Addendum d to ANSI/ASHRAE 135-2004 </summary>
        PropNodeSubType = 207,

        PropNodeType = 208,
        PropStructuredObjectList = 209,
        PropSubordinateAnnotations = 210,
        PropSubordinateList = 211,

        ///<summary> enumerations 212-225 are used in Addendum e to ANSI/ASHRAE 135-2004 </summary>
        PropActualShedLevel = 212,

        PropDutyWindow = 213,
        PropExpectedShedLevel = 214,
        PropFullDutyBaseline = 215,

        ///<summary> enumerations 216-217 are used in Addendum i to ANSI/ASHRAE 135-2004 </summary>
        ///<summary> enumeration values 216-217 are unassigned in 135-2010 </summary>
        //PropBlinkPriorityThreshold = 216,
        //PropBlinkTime = 217,
        ///<summary> enumerations 212-225 are used in Addendum e to ANSI/ASHRAE 135-2004 </summary>
        PropRequestedShedLevel = 218,

        PropShedDuration = 219,
        PropShedLevelDescriptions = 220,
        PropShedLevels = 221,
        PropStateDescription = 222,

        ///<summary> enumerations 223-225 are used in Addendum i to ANSI/ASHRAE 135-2004 </summary>
        ///<summary> enumeration values 223-225 are unassigned in 135-2010 </summary>
        //PropFADETime = 223,
        //PropLIGHTINGCommand = 224,
        //PropLIGHTINGCommandPriority = 225,
        ///<summary> enumerations 226-235 are used in Addendum f to ANSI/ASHRAE 135-2004 </summary>
        PropDoorAlarmState = 226,

        PropDoorExtendedPulseTime = 227,
        PropDoorMembers = 228,
        PropDoorOpenTooLongTime = 229,
        PropDoorPulseTime = 230,
        PropDoorStatus = 231,
        PropDoorUnlockDelayTime = 232,
        PropLockStatus = 233,
        PropMaskedAlarmValues = 234,
        PropSecuredStatus = 235,

        ///<summary> enumerations 236-243 are used in Addendum i to ANSI/ASHRAE 135-2004 </summary>
        ///<summary> enumeration values 236-243 are unassigned in 135-2010 </summary>
        //PropOffDelay = 236,
        //PropONDelay = 237,
        //PropPower = 238,
        //PropPowerOnValue = 239,
        //PropProgressValue = 240,
        //PropRampRate = 241,
        //PropStepIncrement = 242,
        //PropSystemFailureValue = 243,
        ///<summary> enumerations 244-311 are used in Addendum j to ANSI/ASHRAE 135-2004 </summary>
        PropAbsenteeLimit = 244,

        PropAccessAlarmEvents = 245,
        PropAccessDoors = 246,
        PropAccessEvent = 247,
        PropAccessEventAuthenticationFactor = 248,
        PropAccessEventCredential = 249,
        PropAccessEventTime = 250,
        PropAccessTransactionEvents = 251,
        PropAccompaniment = 252,
        PropAccompanimentTime = 253,
        PropActivationTime = 254,
        PropActiveAuthenticationPolicy = 255,
        PropAssignedAccessRights = 256,
        PropAuthenticationFactors = 257,
        PropAuthenticationPolicyList = 258,
        PropAuthenticationPolicyNames = 259,
        PropAuthenticationStatus = 260,
        PropAuthorizationMode = 261,
        PropBelongsTo = 262,
        PropCredentialDisable = 263,
        PropCredentialStatus = 264,
        PropCredentials = 265,
        PropCredentialsInZone = 266,
        PropDaysRemaining = 267,
        PropEntryPoints = 268,
        PropExitPoints = 269,
        PropExpiryTime = 270,
        PropExtendedTimeEnable = 271,
        PropFailedAttemptEvents = 272,
        PropFailedAttempts = 273,
        PropFailedAttemptsTime = 274,
        PropLastAccessEvent = 275,
        PropLastAccessPoint = 276,
        PropLastCredentialAdded = 277,
        PropLastCredentialAddedTime = 278,
        PropLastCredentialRemoved = 279,
        PropLastCredentialRemovedTime = 280,
        PropLastUseTime = 281,
        PropLockout = 282,
        PropLockoutRelinquishTime = 283,
        PropMasterExemption = 284, ///<summary>removed in version 1 revision 13</summary>
        PropMaxFailedAttempts = 285,
        PropMembers = 286,
        PropMusterPoint = 287,
        PropNegativeAccessRules = 288,
        PropNumberOfAuthenticationPolicies = 289,
        PropOccupancyCount = 290,
        PropOccupancyCountAdjust = 291,
        PropOccupancyCountEnable = 292,
        PropOccupancyExemption = 293,///<summary>removed in version 1 revision 13</summary>
        PropOccupancyLowerLimit = 294,
        PropOccupancyLowerLimitEnforced = 295,
        PropOccupancyState = 296,
        PropOccupancyUpperLimit = 297,
        PropOccupancyUpperLimitEnforced = 298,
        PropPassbackExemption = 299,///<summary>removed in version 1 revision 13</summary>
        PropPassbackMode = 300,
        PropPassbackTimesOut = 301,
        PropPositiveAccessRules = 302,
        PropReasonForDisable = 303,
        PropSupportedFormats = 304,
        PropSupportedFormatClasses = 305,
        PropThreatAuthority = 306,
        PropThreatLevel = 307,
        PropTraceFlag = 308,
        PropTransactionNotificationClass = 309,
        PropUserExternalIdentifier = 310,
        PropUserInformationReference = 311,

        ///<summary> enumerations 312-313 are used in Addendum k to ANSI/ASHRAE 135-2004 </summary>
        ///<summary> enumerations 314-316 are used in Addendum ? </summary>
        ///<summary> enumeration values 312-316 are unassigned in 135-2010 </summary>
        //PropCharacterSet = 312,
        //PropSTRICTCharacterMode = 313,
        //PropBackupAndRestoreState = 314,
        //PropBackupPreparationTime = 315,
        //PropRestorePreparationTime = 316,
        ///<summary> enumerations 317-323 are used in Addendum j to ANSI/ASHRAE 135-2004 </summary>
        PropUserName = 317,

        PropUserType = 318,
        PropUsesRemaining = 319,
        PropZoneFrom = 320,
        PropZoneTo = 321,
        PropAccessEventTag = 322,
        PropGlobalIdentifier = 323,

        ///<summary> enumerations 324-325 are used in Addendum i to ANSI/ASHRAE 135-2004 </summary>
        ///<summary> enumeration values 324-325 are unassigned in 135-2010 </summary>
        //PropBinaryActiveVALUE = 324,
        //PropBinaryInactiveValue = 325,
        ///<summary> enumeration 326 is used in Addendum j to ANSI/ASHRAE 135-2004 </summary>
        PropVerificationTime = 326,

        ///<summary> enumeration 327-241 is used in Addendum to ANSI/ASHRAE 135-2010 </summary>
        PropBaseDeviceSecurityPolicy = 327,

        PropDistributionKeyRevision = 328,
        PropDoNotHide = 329,
        PropKeySets = 330,
        PropLastKeyServer = 331,
        PropNetworkAccessSecurityPolicies = 332,
        PropPacketReorderTime = 333,
        PropSecurityPduTimesOut = 334,
        PropSecurityTimeWindow = 335,
        PropSupportedSecurityAlgorithms = 336,
        PropUpdateKeySetTimesOut = 337,
        PropBackupAndRestoreState = 338,
        PropBackupPreparationTime = 339,
        PropRestoreCompletionTime = 340,
        PropRestorePreparationTime = 341,

        ///<summary> enumerations 342-344 are defined in Addendum 2008-w </summary>
        PropBitMask = 342,

        PropBitText = 343,
        PropIsUtc = 344,

        ///<summary> enumeration 345-351 is used in Addendum to ANSI/ASHRAE 135-2010 </summary>
        PropGroupMembers = 345,

        PropGroupMemberNames = 346,
        PropMemberStatusFlags = 347,
        PropRequestedUpdateInterval = 348,
        PropCOVUPeriod = 349,
        PropCOVURecipients = 350,
        PropEventMessageTexts = 351,

        #region BACDEL_PR14
        /* enumeration 352-357, 371 & 372 used as per ANSI/ASHRAE Standard 135-2012 */
        PropEventMessageTextsConfig = 352,
        PropEventDetectionEnable = 353,
        PropEventAlgorithmInhibit = 354,
        PropEventAlgorithmInhibitRef = 355,
        PropTimeDelayNormal = 356,
        PropReliabilityEvaluationInhibit = 357,
        PropFaultParameters = 358,
        PropFaultType = 359,
        PropLocalForwardingOnly = 360,
        PropProcessIdentifierFilter = 361,
        PropSubscribedRecipients = 362,
        PropPortFilter = 363,
        PropAuthorizationExemptions = 364,
        PropAllowGroupDelayInhibit = 365,
        PropChannelNumber = 366,
        PropControlGroups = 367,
        PropExecutionDelay = 368,
        PropLastPriority = 369,
        PropWriteStatus = 370,
        PropPropertyList = 371,
        PropSerialNumber = 372,
        PropBlinkWarnEnable = 373,
        PropDefaultFadeTime = 374,
        PropDefaultRampRate = 375,
        PropDefaultStepIncrement = 376,
        PropEgressTime = 377,
        PropInProgress = 378,
        PropInstantaneousPower = 379,
        PropLightingCommand = 380,
        PropLightingCommandDefaultPriority = 381,
        PropMaxActualValue = 382,
        PropMinActualValue = 383,
        PropPower = 384,
        PropTransition = 385,
        PropEgressActive = 386,
        #endregion /* */

        MaxPropSupported,

        ///<summary> Proprietory Properties for Accumulator object </summary>
        PropBacdelProprietaryPulseCount = 512,

        PropBacdelProprietaryPulseFrequency = 513,

        ///<summary> Proprietory property for Program object </summary>
        PropBacdelProprietaryProgramCount = 514,
        /* Proprietory property for Event Enrollment and Trend objects */
        PropBacdelProprietaryExternalValue = 515,
        PropBacdelProprietaryExternalStatusFlags = 516,
        /* Proprietory property for Event Enrollment object */
        PropBacdelProprietaryExternalFeedbackValue = 517,
        ///<summary> The special property identifiers all, optional, and required  </summary>
        ///<summary> are reserved for use in the ReadPropertyConditional and </summary>
        ///<summary> ReadPropertyMultiple services or services not defined in this standard. </summary>
        ///<summary> Enumerated values 0-511 are reserved for definition by ASHRAE.  </summary>
        ///<summary> Enumerated values 512-4194303 may be used by others subject to the  </summary>
        ///<summary> procedures and constraints described in Clause 23.  </summary>

        //#ifdef ProprietaryPROPERTY
        ///<summary> Vendor specific property id's </summary>
        PropBacdelProprietaryCurrentCommanderValue = 40278,

        PropBacdelProprietaryCmdValue = 40279,
        PropBacdelProprietaryCommanderArray = 40281,
        PropBacdelProprietaryCurrentCmdPriority = 40282,
        PropBacdelProprietaryLastCmdTime = 40283,
        PropUninitiazlied = 4194303
        //#endif        
    }

    ///<summary>
    ///Enum for the function Return Types
    ///Name in Stack Application: BACNET_RETURN_TYPE
    ///</summary>
    public enum BacnetReturnType
    {
        BacdelNotAvaliable = -1,
        BacdelSuccess = 0,
        BacdelCONTINUE,
        BacdelError,

        BacdelDeviceNotPresent,
        BacdelTruncatedDataFrame,
        BacdelMallocError,
        BacdelTagError,
        BacdelMaxInstanceError,
        BacdelOutOfRangeError,
        BacdelSemaphoreError,
        BacdelMutexError,
        BacdelThreadError,
        BacdelObjectTypeError,
        BacdelNotProperRangeError,
        BacdelPropertyNotPresent,
        BacdelObjectNotPresent,
        BacdelDuplicateObject,
        BacdelDuplicateObjectName,

        BacdelBbmdNotSupported,
        BacdelBbmdEnabled,
        BacdelBbmdAlreadyEnabled,
        BacdelBbmdDisabled,
        BacdelBbmdAlreadyDisabled,
        BacdelFDEnabled,
        BacdelFDAlreadyEnabled,
        BacdelFDDisabled,
        BacdelFDAlreadyDisabled,

        BacdelHWInterfaceSuccess,
        BacdelHWInterfaceNoMapping,
        BacdelHWInterfaceNoProperty,
        BacdelHWInterfaceNoObject,
        BacdelHWInterfaceNoDevice,
        BacdelHWInterfaceOutOfRangeError,

        BacdelInitiatorSuccess,
        BacdelInitiatorError,
        BacdelInitiatorTimesOut,
        BacdelInitiatorMutexLockFailed,
        BacdelInitiatorSemaphoreFailed,
        BacdelInitiatorNoRequestSpace,
        BacdelInitiatorNoInvokeID,
        BacdelInitiatorNoTokenID,
        BacdelInitiatorMallocError,
        BacdelInitiatorMaxObjectTypeError,
        BacdelInitiatorMaxInstanceError,
        BacdelInitiatorMaxPropertyError,
        BacdelInitiatorBvlcFunctionError,
        BacdelInitiatorNotProperRangeError,
        BacdelInitiatorUnknownServiceChoice,
        BacdelInitiatorQueueFull,
        BacdelInitiatorUnknownObjectType,
        BacdelInitiatorInvalidObjectInstance,
        BacdelInitiatorInvalidProperty,
        BacdelInitiatorChoiceError,
        BacdelInitiatorMaxCharacterLengthError,
        BacdelInitiatorInvalidProcessID,
        BacdelInitiatorInvalidPropertyTag,

        BacdelLocalBufferExceeded,
        BacdelCodePageNotSupported,
        BacdelCharacterSetNotSupported,

        BacdelInvalidLicense,
        BacdelInvalidMacAddress,
        BacdelInvalidIpAddress,
        BacdelInvalidNetworkNo,
        BacdelInvalidPortNumber,
        BacdelSocketInitError,
        BacdelSocketBindError,
        BacdelSocketError,
        BacdelSocketSendError,
        BacdelBIpInitError,
        BacdelTimerError,
        BacdelTimerQError,
        BacdelIpNotAvailable,
        BacdelIpPortNotAvailable,
        BacdelPortNotAvailable,

        BacdelDirectoryCreationFailed,
        BacdelDirectoryPathNotFound,
        BacdelObjectCannotBeDeleted,
        BacdelObjectCannotBeCreated,
        BacdelStackNotInitialized,
        BacdelStackAlreadyInitialized,
        BacdelStackAlreadyDEInitialized,

        BacdelDestinationNotFound,
        BacdelDestinationInvalid,
        BacdelInvalidInputParameter,
        BacdelQueueFull,
        BacdelFileCreationFailed,
        BacdelFileAccessError,
        BacdelRouterIPNotFound,
        BacdelSocketCommunicationDisabled,
        BacdelDccDisabled,

        BacdelInvalidBaudRate,
        BacdelSerialInitError,
        BacdelSerialSendError,
        BacdelSerialRcvError,

        BacdelEntryMessage,
        BacdelExitMessage,
        BacdelInformativeMessage,
        BacdelEncodingFailed,
        BacdelDecodingFailed,

        BacdelDeviceBusy,
        BacdelConfigurationInProgress,
        BacdelOperationalProblem,
        BacdelInternalError,
        BacdelNotConfigured,
        BacdelPasswordError,
        BacdelSecurityError,

        BacdelSegmentationNotSupported,
        BacdelSegmentationError,
        BacdelSegmentationWindowSizeOutOfRange,
        BacdelDynamicCreationNotSupported,
        BacdelDynamicDeletionNotSupported,
        BacdelOutOfResources,
        BacdelLogBufferFull,
        BacdelFileFull,

        BacdelReadAccessDenied,
        BacdelWriteAccessDenied,
        BacdelPropertyIsNotAnArray,
        BacdelPropertyIsNotAList,
        BacdelInvalidArrayIndex,
        BacdelInvalidDataType,
        BacdelInvalidParameterValue,
        BacdelInvalidConfigurationData,
        BacdelDatatypeNotSupported,

        BacdelInconsistentSelectionCriterion,
        BacdelInconsistentParameters,
        BacdelMissingParameter,
        BacdelOptionalFunctionalityNotSupported,
        BacdelRequestDenied,
        BacdelNoSpaceForObject,
        BacdelNoSpaceForProperty,
        BacdelListElementNotFound,
        BacdelUnknownSubscription,

        BacdelBBMDInitiatorQueueFull,
        BacdelReceiveQueueFull,
        BacdelProcessQueueFull,
        BacdelCallbackNotificationQueueFull,
        BacdelPropertyAccessError,
        BacdelServiceExecutionError,
        BacdelSegmentOutOfOrder,
        BacdelMessageQueueError,

        BacdelOther
    }

    /// <summary>
    /// Name in Stack Application: BACNET_OBJECT_TYPE
    /// </summary>
    public enum BacnetObjectType
    {
        ObjectAnalogInput = 0,
        ObjectAnalogOutput = 1,
        ObjectAnalogValue = 2,
        ObjectBinaryInput = 3,
        ObjectBinaryOutput = 4,
        ObjectBinaryValue = 5,
        ObjectCalendar = 6,
        ObjectCommand = 7,
        ObjectDevice = 8,
        ObjectEventEnrollment = 9,
        ObjectFile = 10,
        ObjectGroup = 11,
        ObjectLoop = 12,
        ObjectMultiStateInput = 13,
        ObjectMultiStateOutput = 14,
        ObjectNotificationClass = 15,
        ObjectProgram = 16,
        ObjectSchedule = 17,
        ObjectAveraging = 18,
        ObjectMultiStateValue = 19,
        ObjectTrendlog = 20,
        ObjectLifeSafetyPoint = 21,
        ObjectLifeSafetyZone = 22,
        ObjectAccumulator = 23,
        ObjectPulseConverter = 24,
        ObjectEventLog = 25,
        ObjectGlobalGroup = 26,
        ObjectTrendLogMultiple = 27,
        ObjectLoadControl = 28,
        ObjectStructuredView = 29,
        ObjectAccessDoor = 30,
        ObjectUnassigned = 31,				///<summary> enum value 31 is unused in protocol </summary>
        ObjectAccessCredential = 32,      ///<summary> Addendum 2008-j </summary>
        ObjectAccessPoint = 33,
        ObjectAccessRights = 34,
        ObjectAccessUser = 35,
        ObjectAccessZone = 36,
        ObjectCredentialDataInput = 37,  ///<summary> authentication-factor-input </summary>
        ObjectNetworkSecurity = 38,       ///<summary> Addendum 2008-g </summary>
        ObjectBitStringValue = 39,        ///<summary> Addendum 2008-w </summary>
        ObjectCharacterStringValue = 40,
        ObjectDatePatternValue = 41,
        ObjectDateValue = 42,
        ObjectDateTimePatternValue = 43,
        ObjectDateTimeValue = 44,
        ObjectIntegerValue = 45,
        ObjectLargeAnalogValue = 46,
        ObjectOctetStringValue = 47,
        ObjectPositiveIntegerValue = 48,
        ObjectTimePatternValue = 49,
        ObjectTimeValue = 50,

        #region _PR14
        /* enumerations added in PR14 i.e. ANSI/ASHRAE 2012 manual */
        ObjectNotificationForwarder = 51,
        ObjectAlertEnrollment = 52,
        ObjectChannel = 53,
        ObjectLightingOutput = 54,
        #endregion

        /* Max supported object type */
        MaxAshraeObjectType,

        ///<summary> used for bit string loop </summary>
        ProprietaryBacnetObjectType = 128,

        MaxBacnetObjectType = 1024
        ///<summary> Enumerated values 0-127 are reserved for definition by ASHRAE. </summary>
        ///<summary> Enumerated values 128-1023 may be used by others subject to  </summary>
        ///<summary> the procedures and constraints described in Clause 23. </summary>
    }

    /// <summary>
    /// Name in Stack Application: BACNET_SEGMENTATION
    /// </summary>
    public enum BacnetSegmentation
    {
        SegmentationBoth = 0,
        SegmentationTransmit = 1,
        SegmentationReceive = 2,
        SegmentationNone = 3,
        MaxBacnetSegmentation = 4
    }

    ///<summary>
    ///Bacnet Data Type Enum Values
    ///Name in Stack Application: BACNET_DATA_TYPE
    ///</summary>
    public enum BacnetDataType
    {
        BacnetDTNull = 1,                     ///<summary> null value </summary>
        BacnetDTBoolean,                      ///<summary> boolean </summary>
        BacnetDTEnum,                         ///<summary> enum value for binary objects </summary>
        BacnetDTUnsigned32,                   ///<summary> unsigned 32 </summary>
        BacnetDTUnsigned16,                   ///<summary> unsigned 16 </summary>
        BacnetDTUnsigned,                     ///<summary> unsigned or unsigned 32 </summary>
        BacnetDTInteger,                      ///<summary> signed or integer </summary>
        BacnetDTReal,                         ///<summary> real or float </summary>
        BacnetDTDouble,                       ///<summary> double </summary>
        BacnetDTCharString,                   ///<summary> character string </summary>

        BacnetDTTime,							///<summary> time </summary>
        BacnetDTDate,                         ///<summary> date </summary>
        BacnetDTDateTime,						///<summary> datetime </summary>
        BacnetDTTimestampArray,              ///<summary> timestamp Array </summary>
        BacnetDTCharStringArray,             ///<summary> character string array or list </summary>
        BacnetDTBitString,                    ///<summary> bit string - single byte </summary>
        BacnetDTOctetString,                  ///<summary> octet string </summary>
        BacnetDTUnsignedArray,               ///<summary> unsigned array </summary>
        BacnetDTObjectID,						///<summary> object id </summary>
        BacnetDTObjType,						///<summary> object type property </summary>

        BacnetDTBacnetDevstat,                ///<summary> device status property </summary>
        BacnetDTServicesSupported,			///<summary> service Supported property </summary>
        BacnetDTObjectTypeSupported,		///<summary> object type supported property </summary>
        BacnetDTObjectIDArray,				///<summary> object id array </summary>
        BacnetDTBacnetSeg,                    ///<summary> segmentation supported property </summary>
        BacnetDTBacnetVtClass,                ///<summary> vt class property </summary>
        BacnetDTBacnetVtSess,                 ///<summary> vt session property </summary>
        BacnetDTBacnetSessKey,                ///<summary> sesion key property </summary>
        BacnetDTAddressBinding,               ///<summary> Address binding </summary>
        BacnetDTTimestamp,					///<summary> Timestamp </summary>

        BacnetDTRecipientProcess,			///<summary> recipient process </summary>
        BacnetDTDevObjPropReff,				///<summary> device object property reference </summary>
        BacnetDTCOVSub,						///<summary> cov subscription </summary>
        BacnetDTPriorityArray,				///<summary> priority array </summary>
        BacnetDTBooleanArray,				///<summary> boolean array </summary>
        BacnetDTDestinationList,				///<summary> destination list </summary>
        BacnetDTEventParameters,				///<summary> event parameters </summary>
        BacnetDTNotificationPriority,         ///<summary> priority in NC object </summary>
        BacnetDTRecipientList,               ///<summary> recipient list </summary>
        BacnetDTDateList,						///<summary> date list property </summary>

        BacnetDTDateRange,					///<summary> date range - effective period property </summary>
        BacnetDTDevObjPropReffList,			///<summary> list of object property reference </summary>
        BacnetDTDailyScheduleArray,			///<summary> weekly Schedule property </summary>
        BacnetDTSpecialEventArray,           ///<summary> exception Schedule property </summary>
        BacnetDTSchedulePresentDefault,     ///<summary> schedule Default </summary>
        BacnetDTBackupState,                  ///<summary> backup & restore state property </summary>
        BacnetDTLogBufferTrendlog,           ///<summary> log buffer - log record </summary>
        BacnetDTLogBufferLogStatus,          ///<summary> log buffer - log status </summary>
        BacnetDTLogBufferError,              ///<summary> log buffer - errro </summary>
        BacnetDTLogBufferTimeChange,         ///<summary> log buffer - time change </summary>

        BacnetDTClientCOVIncrement,           ///<summary> client cov increment property </summary>
        BacnetDTObjPropRef,					///<summary> object property reference </summary>
        BacnetDTSetPointRef,					///<summary> setpoint reference </summary>
        BacnetDTEnumNew,                     ///<summary> enum for enum type properties </summary>
        BacnetDTBitStringNew,                ///<summary> bit string - multiple byte </summary>
        BacnetDTPrescale,                     ///<summary> pre scale property </summary>
        BacnetDTScale,                        ///<summary> scale property </summary>
        BacnetDTAccRecord,                    ///<summary> accumulator record </summary>
        BacnetDTBitStringArray,              ///<summary> bit string array </summary>
        BacnetDTActionListArray,                     ///<summary> action command </summary>

        BacnetDTReadAccessSpecsList,			///<summary> read access specification </summary>
        BacnetDTReadAccessResultList,        ///<summary> read access result </summary>
        BacnetDTPropertyAccessResultArray,   ///<summary> property access result </summary>
        BacnetDTDevObjReff,                   ///<summary> device object reference </summary>
        BacnetDTShedLevel,                    ///<summary> Shed level </summary>
        BacnetDTAuthenticationFactor,        ///<summary> authentication factor </summary>
        BacnetDTAUFactorFormatArray,		///<summary> authentication factor format </summary>
        BacnetDTOptionalCharString,          ///<summary> optional character string </summary>
        BacnetDTNwSecurityPolicyArray,     ///<summary> network security policy </summary>
        BacnetDTSecurityKeysetArray,        ///<summary> security ket set </summary>

        BacnetDTAuthenticationPolicyArray,  ///<summary> authentication policy </summary>
        BacnetDTAccessRULEArray,            ///<summary> access rule </summary>
        BacnetDTCredAUFactorArray,         ///<summary> credential authentication factor </summary>
        BacnetDTAssignedAccessRightsArray, ///<summary> assigned access rights </summary>
        BacnetDTLogBufferTLM,                ///<summary> log buffer - log record for trend log multiple </summary>
        BacnetDTLogBufferEL,                 ///<summary> log buffer - log record for event log </summary>
        BacnetDTOptCharStringArray,          ///<summary> Character string object Optional chara string </summary>
        BacnetDTEnumList,					///<summary> for List of enum type properties </summary>
        BacnetDTEventMsgText,				///<summary> for event message text property </summary>
        BacnetDTDevObjReffList,				///<summary> device object reference list </summary>

        BacnetDTDevObjReffArray,		    ///<summary> device object reference array </summary>
        BacnetDTDevObjPropReffArray,		///<summary> array of device object property referrence </summary>
        BacnetDTEmpty,						///<summary> empty value </summary>
        BacnetDTUnsigned8,					///<summary> unsigned 8 </summary>
        BacnetDTUnsignedList,                ///<summary> unsigned list </summary>
        BacnetDTAddressBindingList,			///<summary> List of address binding </summary>
        BacnetDTDailySchedule,				///<summary> single element of daily schedule </summary>
        BacnetDTSecurityKeySet,				///<summary> single element of Key sets </summary>
        BacnetDTSpecialEvent,				///<summary> single element of exception schedule </summary>	/*
        BacnetDTPropertyAccessResult,		///<summary> single element of property access result array </summary>

        BacnetDTAuFactorFormat,				///<summary> single element of authentication factor format</summary>
        BacnetDTNwSecurityPolicy,			///<summary> single element of network security policy </summary>
        BacnetDTAuthenticationPolicy,		///<summary> single element of authentication policy array</summary>/*  */
        BacnetDTAccessRule,		            ///<summary> single element of access rule</summary>
        BacnetDTCredAuFactor,				///<summary> single element of credential au factor</summary>
        BacnetDTAssignedAccessRights,		///<summary> single element of assigned access rights</summary>
        BacnetDTActionList,					///<summary> single element of action list</summary>
        BacnetDTPropertyList,				///<summary> property list </summary>
        BacnetDTLightingCommand,		    ///<summary> lighting command </summary>
        BacnetDTReserved1,					///<summary> reserved-1 data type </summary>

        BacnetDTEventNotifySubsList,		///<summary> list of event notification subscription </summary>
        BacnetDTProcessIdSelection,			///<summary> process id selection </summary>
        BacnetDTPortPermission,				///<summary> port permission </summary>
        BacnetDTPortPermissionArray,		///<summary> array of port permission </summary>
        BacnetDTChannelValue,				///<summary> channel value </summary>
        BacnetDtFaultParameters,            ///<summary> fault parameters </summary>

        ///<summary> Add new values above this comment </summary>
        BacnetDTMax
    }

    /// <summary>
    /// Name in Stack Application: BACNET_ERROR_CLASS
    /// </summary>
    public enum BacnetErrorClass
    {
        ErrorClassDevice = 0,
        ErrorClassObject = 1,
        ErrorClassProperty = 2,
        ErrorClassResources = 3,
        ErrorClassSecurity = 4,
        ErrorClassServices = 5,
        ErrorClassVt = 6,

        ///<summary> Enumerated values 0-63 are reserved for definition by ASHRAE. </summary>
        ///<summary> Enumerated values 64-65535 may be used by others subject to </summary>
        ///<summary> the procedures and constraints described in Clause 23. </summary>
        ErrorClassCommunication = 7,

        MaxBacnetErrorClass = 8,
        FirstProprietaryErrorClass = 64,
        LastProprietaryErrorClass = 65535
    }

    ///<summary> These are sorted in the order given in
    /// Clause 18. ERROR, REJECT AND ABORT CODES
    ///   The Class and Code pairings are required
    ///   to be used in accordance with Clause 18.
    ///   Name in Stack Application: BACNET_ERROR_CODE
    ///   </summary>
    public enum BacnetErrorCode
    {
        ///<summary> valid for all classes </summary>
        ErrorCodeOther = 0,

        ///<summary> Error Class - Device </summary>
        ErrorCodeDeviceBusy = 3,

        ErrorCodeConfigurationInProgress = 2,
        ErrorCodeOperationalProblem = 25,
        ErrorCodeInconsistentConfiguration = 129,
        ErrorCodeInternalError = 131,
        ErrorCodeNotConfigured = 132,

        ///<summary> Error Class - Object </summary>
        ErrorCodeDynamicCreationNotSupported = 4,

        ErrorCodeNoObjectsOfSpecifiedType = 17,
        ErrorCodeObjectDeletionNotPermitted = 23,
        ErrorCodeObjectIdentifierAlreadyExists = 24,
        ErrorCodeReadAccessDenied = 27,
        ErrorCodeUnknownObject = 31,
        ErrorCodeUnsupportedObjectType = 36,
        ErrorCodeOptionalFunctionalityNotSupported = 45,
        ErrorCodeNoAlarmConfigured = 74,
        ErrorCodeLogBufferFull = 75,
        ErrorCodeBusy = 82,
        ErrorCodeFileFull = 128,

        ///<summary> Error Class - Property </summary>
        ErrorCodeInconsistentSelectionCriterion = 8,

        ErrorCodeInvalidDataType = 9,

        ///<summary> ErrorCodeReadAccessDenied = 27, </summary>
        ErrorCodeUnknownProperty = 32,

        ErrorCodeValueOutOfRange = 37,
        ErrorCodeWriteAccessDenied = 40,
        ErrorCodeCharacterSetNotSupported = 41,
        ErrorCodeInvalidArrayINDEX = 42,
        ErrorCodeNotCOVProperty = 44,

        ///<summary> ErrorCode_Optional_Functionality_Not_Supported = 45, </summary>
        ErrorCodeDatatypeNotSupported = 47,

        ErrorCodeDuplicateName = 48,
        ErrorCodeDuplicateObjectID = 49,
        ErrorCodePropertyIsNotAnArray = 50,
        ErrorCodeValueNotInitialized = 72,
        ErrorCodeLoggedValuePURGED = 76,
        ErrorCodeNoPropertySpecified = 77,
        ErrorCodeNotConfiguredForTriggeredLogging = 78,
        ErrorCodeUnknownFileSize = 122,
        ErrorCodeValueTooLong = 134,

        ///<summary> Error Class - Resources </summary>
        ErrorCodeNoSpaceForObject = 18,

        ErrorCodeNoSpaceToAddListElement = 19,
        ErrorCodeNoSpaceToWriteProperty = 20,
        ErrorCodeOutOfMemory = 133,

        ///<summary> Error Class - Security </summary>
        ErrorCodeAuthenticationFailed = 1,

        ErrorCodeIncompatibleSecurityLevels = 6,
        ErrorCodeInvalidOperatorName = 12,
        ErrorCodeKeyGenerationError = 15,
        ErrorCodePasswordFailure = 26,
        ErrorCodeSecurityNotSupported = 28,
        ErrorCodeTimesOut = 30,
        ErrorCodeSuccess = 84,
        ErrorCodeAccessDenied = 85,
        ErrorCodeBadDestinationAddress = 86,
        ErrorCodeBadDestinationDeviceID = 87,
        ErrorCodeBadSignature = 88,
        ErrorCodeBadSourceAddress = 89,
        ErrorCodeBadTimestamp = 90,
        ErrorCodeCannotUseKey = 91,
        ErrorCodeCannotVerifyMessageID = 92,
        ErrorCodeCorrectKeyRevision = 93,
        ErrorCodeDestinationDeviceIDRequired = 94,
        ErrorCodeDuplicateMessage = 95,
        ErrorCodeEncryptionNotConfigured = 96,
        ErrorCodeEncryptionRequired = 97,
        ErrorCodeIncorrectKey = 98,
        ErrorCodeInvalidKeyData = 99,
        ErrorCodeKeyUpdateInProgress = 100,
        ErrorCodeMalFormedMessage = 101,
        ErrorCodeNotKeyServer = 102,
        ErrorCodeSecurityNotConfigured = 103,
        ErrorCodeSourceSecurityRequired = 104,
        ErrorCodeTooManyKey = 105,
        ErrorCodeUnknownAuthenticationType = 106,
        ErrorCodeUnknownKey = 107,
        ErrorCodeUnknownKeyRevision = 108,
        ErrorCodeUnknownSourceMessage = 109,

        ///<summary> Error Class - Services </summary>
        ///<summary> ErrorCodeCharacterSetNotSupported = 41, </summary>
        ErrorCodeCommunicationDisabled = 83,

        ErrorCodeCOVSubscriptionFailed = 43,
        ErrorCodeFileAccessDenied = 5,
        ErrorCodeInconsistentObjectType = 130,
        ErrorCodeInconsistentParameters = 7,
        ErrorCodeInvalidConfigurationData = 46,
        ErrorCodeInvalidEventState = 73,
        ErrorCodeInvalidFileAccessMethod = 10,
        ErrorCodeInvalidFileStartPosition = 11,
        ErrorCodeInvalidParameterDataType = 13,
        ErrorCodeInvalidTimestamp = 14,
        ErrorCodeInvalidTag = 57,
        ErrorCodeMissingRequiredParameter = 16,

        ///<summary> ErrorCodeOptionalFunctionalityNotSupported = 45, </summary>
        ErrorCodeParameterOutOfRange = 80,

        ErrorCodePropertyIsNotAList = 22,

        ///<summary> ErrorCodePropertyIsNotANArray = 50, </summary>
        ///<summary> ErrorCodeValueOutOfRange = 37, </summary>
        ErrorCodeServiceRequestDenied = 29,

        ErrorCodeListElementNotFound = 81,
        ErrorCodeUnknownSubscription = 79,

        ///<summary> Error Class - Vt </summary>
        ErrorCodeUnknownVtClass = 34,

        ErrorCodeUnknownVtSession = 35,
        ErrorCodeNoVtSessionsAvailable = 21,
        ErrorCodeVtSessionAlreadyClosed = 38,
        ErrorCodeVtSessionTerminationFailure = 39,

        ///<summary> Error Class - Communication </summary>
        ErrorCodeReserved1 = 33,

        ErrorCodeAbortBufferOVERFLOW = 51,
        ErrorCodeAbortInvalidApduInThisState = 52,
        ErrorCodeAbortPreemptedByHigherPriorityTASK = 53,
        ErrorCodeAbortSegmentationNotSupported = 54,
        ErrorCodeAbortProprietary = 55,
        ErrorCodeAbortOther = 56,
        ErrorCodeAbortApduTooLong = 123,
        ErrorCodeAbortApplicationExceededReplyTime = 124,
        ErrorCodeAbortOutOfResources = 125,
        ErrorCodeAbortTsmTimesOut = 126,
        ErrorCodeAbortWindowSizeOutOfRange = 127,
        ErrorCodeAbortSecurityError = 136,
        ErrorCodeAbortInsufficientSecurity = 135,

        ErrorCodeRejectBufferOVERFLOW = 59,
        ErrorCodeRejectInconsistentParameters = 60,
        ErrorCodeRejectInvalidParameterDataType = 61,
        ErrorCodeRejectInvalidTag = 62,
        ErrorCodeRejectMissingRequiredParameter = 63,
        ErrorCodeRejectParameterOutOfRange = 64,
        ErrorCodeRejectTooManyArguments = 65,
        ErrorCodeRejectUndefinedEnumeration = 66,
        ErrorCodeRejectUnrecognizedService = 67,
        ErrorCodeRejectProprietary = 68,
        ErrorCodeRejectOther = 69,

        ///<summary> ErrorCodeInvalidTag = 57, </summary>
        ErrorCodeNetworkDown = 58,

        ///<summary> ErrorCodeTimesOut = 30, </summary>
        ErrorCodeUnknownDevice = 70,

        ErrorCodeUnknownRoute = 71,
        ErrorCodeNotRouterToDnet = 110,
        ErrorCodeRouterBusy = 111,
        ErrorCodeUnknownNetworkMessage = 112,
        ErrorCodeMessageTooLong = 113,
        ErrorCodeSecurityError = 114,
        ErrorCodeAddressingError = 115,
        ErrorCodeWriteBDtFailed = 116,
        ErrorCodeReadBDtFailed = 117,
        ErrorCodeRegisterForeignDeviceFailed = 118,
        ErrorCodeReadFDtFailed = 119,
        ErrorCodeDeleteFDtEntryFailed = 120,
        ErrorCodeDistributeBroadcastFailed = 121,

        MaxBacnetErrorCode = 137,

        ///<summary> Enumerated values 0-255 are reserved for definition by ASHRAE. </summary>
        ///<summary> Enumerated values 256-65535 may be used by others subject to </summary>
        ///<summary> the procedures and constraints described in Clause 23. </summary>
        ///<summary> The last enumeration used in this version is 50. </summary>
        FirstProprietaryErrorCode = 256,

        LastProprietaryErrorCode = 65535
    }

    /// <summary>
    /// Name in Stack Application: BACNET_ENGINEERING_UNITS
    /// </summary>
    public enum BacnetEngineeringUnits
    {
        ///<summary> Acceleration </summary>
        UnitsMetersPerSecondPerSecond = 166,

        ///<summary> Area </summary>
        UnitsSquareMeters = 0,

        UnitsSquareCentiMeters = 116,
        UnitsSquareFeet = 1,
        UnitsSquareInches = 115,

        ///<summary> Currency </summary>
        UnitsCurrency1 = 105,

        UnitsCurrency2 = 106,
        UnitsCurrency3 = 107,
        UnitsCurrency4 = 108,
        UnitsCurrency5 = 109,
        UnitsCurrency6 = 110,
        UnitsCurrency7 = 111,
        UnitsCurrency8 = 112,
        UnitsCurrency9 = 113,
        UnitsCurrency10 = 114,

        ///<summary> Electrical </summary>
        UnitsMilliAmperes = 2,

        UnitsAmperes = 3,
        UnitsAmperesPerMeter = 167,
        UnitsAmperesPerSquareMeter = 168,
        UnitsAmpereSquareMeters = 169,
        UnitsDecibles = 199,
        UnitsDeciblesMilliVolt = 200,
        UnitsDeciblesVolt = 201,
        UnitsFarads = 170,
        UnitsHenrys = 171,
        UnitsOhms = 4,
        UnitsOhmMeters = 172,
        UnitsMilliOhms = 145,
        UnitsKiloOhms = 122,
        UnitsMegaOhms = 123,
        UnitsMicroSiemens = 190,
        UnitsMilliSiemens = 202,
        UnitsSiemens = 173,        ///<summary> 1 mho equals 1 Siemens </summary>
        UnitsSiemensPerMeter = 174,
        UnitsTeslas = 175,
        UnitsVolts = 5,
        UnitsMilliVolts = 124,
        UnitsKiloVolts = 6,
        UnitsMegaVolts = 7,
        UnitsVoltAmperes = 8,
        UnitsKiloVoltAmperes = 9,
        UnitsMegaVoltAmperes = 10,
        UnitsVoltAmperesReactive = 11,
        UnitsKiloVoltAmperesReactive = 12,
        UnitsMegaVoltAmperesReactive = 13,
        UnitsVoltsPerDegreeKelvin = 176,
        UnitsVoltsPerMeter = 177,
        UnitsDegreesPhase = 14,
        UnitsPowerFactor = 15,
        UnitsWebers = 178,

        ///<summary> Energy </summary>
        UnitsJoules = 16,

        UnitsKiloJoules = 17,
        UnitsKiloJoulesPerKiloGram = 125,
        UnitsMegaJoules = 126,
        UnitsWattHours = 18,
        UnitsKiloWattHours = 19,
        UnitsMegaWattHours = 146,
        UnitsWattHoursReactive = 203,
        UnitsKiloWattHoursReactive = 204,
        UnitsMegaWattHoursReactive = 205,
        UnitsBtus = 20,
        UnitsKiloBtus = 147,
        UnitsMegaBtus = 148,
        UnitsTherms = 21,
        UnitsTonHours = 22,

        ///<summary> Enthalpy </summary>
        UnitsJoulesPerKiloGramDryAir = 23,

        UnitsKiloJoulesPerKiloGramDryAir = 149,
        UnitsMegaJoulesPerKiloGramDryAir = 150,
        UnitsBtusPerPoundDryAir = 24,
        UnitsBtusPerPound = 117,

        ///<summary> Entropy </summary>
        UnitsJoulesPerDegreeKelvin = 127,

        UnitsKiloJoulesPerDegreeKelvin = 151,
        UnitsMegaJoulesPerDegreeKelvin = 152,
        UnitsJoulesPerKiloGramDegreeKelvin = 128,

        ///<summary> Force </summary>
        UnitsNewton = 153,

        ///<summary> Frequency </summary>
        UnitsCyclesPerHour = 25,

        UnitsCyclesPerMinute = 26,
        UnitsHertz = 27,
        UnitsKiloHertz = 129,
        UnitsMegaHertz = 130,
        UnitsPerHour = 131,

        ///<summary> Humidity </summary>
        UnitsGramsOfWaterPerKiloGramDryAir = 28,

        UnitsPercentRelativeHumidity = 29,

        ///<summary> Length </summary>
        UnitsMicroMeters = 194,

        UnitsMilliMeters = 30,
        UnitsCentiMeters = 118,
        UnitsKiloMeters = 193,
        UnitsMeters = 31,
        UnitsInches = 32,
        UnitsFeet = 33,

        ///<summary> Light </summary>
        UnitsCandelas = 179,

        UnitsCandelasPerSquareMeter = 180,
        UnitsWattsPerSquareFoot = 34,
        UnitsWattsPerSquareMeter = 35,
        UnitsLumens = 36,
        UnitsLuxes = 37,
        UnitsFootCandles = 38,

        ///<summary> Mass </summary>
        UnitsMilliGrams = 196,

        UnitsGrams = 195,
        UnitsKiloGrams = 39,
        UnitsPoundsMass = 40,
        UnitsTons = 41,

        ///<summary> Mass Flow </summary>
        UnitsGramsPerSecond = 154,

        UnitsGramsPerMinute = 155,
        UnitsKiloGramsPerSecond = 42,
        UnitsKiloGramsPerMinute = 43,
        UnitsKiloGramsPerHour = 44,
        UnitsPoundsMassPerSecond = 119,
        UnitsPoundsMassPerMinute = 45,
        UnitsPoundsMassPerHour = 46,
        UnitsTonsPerHour = 156,

        ///<summary> Power </summary>
        UnitsMilliWatts = 132,

        UnitsWatts = 47,
        UnitsKiloWatts = 48,
        UnitsMegaWatts = 49,
        UnitsBtusPerHour = 50,
        UnitsKiloBtusPerHour = 157,
        UnitsHorsePower = 51,
        UnitsTonsRefrigeration = 52,

        ///<summary> Pressure </summary>
        UnitsPascals = 53,

        UnitsHectoPascals = 133,
        UnitsKiloPascals = 54,
        UnitsMilliBars = 134,
        UnitsBars = 55,
        UnitsPoundsForcePerSquareInch = 56,
        UnitsCentiMetersOfWater = 57,
        UnitsInchesOfWater = 58,
        UnitsMilliMetersOfMercury = 59,
        UnitsCentiMetersOfMercury = 60,
        UnitsInchesOfMercury = 61,

        ///<summary> Temperature </summary>
        UnitsDegreesCelsius = 62,

        UnitsDegreesKelvin = 63,
        UnitsDegreesKelvinPerHour = 181,
        UnitsDegreesKelvinPerMinute = 182,
        UnitsDegreesFahrenheit = 64,
        UnitsDegreeDaysCelsius = 65,
        UnitsDegreeDaysFahrenheit = 66,
        UnitsDeltaDegreesFahrenheit = 120,
        UnitsDeltaDegreesKelvin = 121,

        ///<summary> Time </summary>
        UnitsYears = 67,

        UnitsMonths = 68,
        UnitsWeeks = 69,
        UnitsDays = 70,
        UnitsHours = 71,
        UnitsMinutes = 72,
        UnitsSeconds = 73,
        UnitsHundredthsSeconds = 158,
        UnitsMilliSeconds = 159,

        ///<summary> Torque </summary>
        UnitsNewtonMeters = 160,

        ///<summary> Velocity </summary>
        UnitsMilliMetersPerSecond = 161,

        UnitsMilliMetersPerMinute = 162,
        UnitsMetersPerSecond = 74,
        UnitsMetersPerMinute = 163,
        UnitsMetersPerHour = 164,
        UnitsKiloMetersPerHour = 75,
        UnitsFeetPerSecond = 76,
        UnitsFeetPerMinute = 77,
        UnitsMilesPerHour = 78,

        ///<summary> Volume </summary>
        UnitsCubicFeet = 79,

        UnitsCubicMeters = 80,
        UnitsImperialGallons = 81,
        UnitsMilliLiters = 197,
        UnitsLiters = 82,
        UnitsUSGallons = 83,

        ///<summary> Volumetric Flow </summary>
        UnitsCubicFeetPerSecond = 142,

        UnitsCubicFeetPerMinute = 84,
        UnitsCubicFeetPerHour = 191,
        UnitsCubicMetersPerSecond = 85,
        UnitsCubicMetersPerMinute = 165,
        UnitsCubicMetersPerHour = 135,
        UnitsImperialGallonsPerMinute = 86,
        UnitsMilliLitersPerSecond = 198,
        UnitsLitersPerSecond = 87,
        UnitsLitersPerMinute = 88,
        UnitsLitersPerHour = 136,
        UnitsUSGallonsPerMinute = 89,
        UnitsUSGallonsPerHour = 192,

        ///<summary> Other </summary>
        UnitsDegreesAngular = 90,

        UnitsDegreesCelsiusPerHour = 91,
        UnitsDegreesCelsiusPerMinute = 92,
        UnitsDegreesFahrenheitPerHour = 93,
        UnitsDegreesFahrenheitPerMinute = 94,
        UnitsJouleSeconds = 183,
        UnitsKiloGramsPerCubicMeter = 186,
        UnitsKWHoursPerSquareMeter = 137,
        UnitsKWHoursPerSquareFoot = 138,
        UnitsMegaJoulesPerSquareMeter = 139,
        UnitsMegaJoulesPerSquareFoot = 140,
        UnitsNoUnits = 95,
        UnitsNewtonSeconds = 187,
        UnitsNewtonsPerMeter = 188,
        UnitsPartsPerMillion = 96,
        UnitsPartsPerBillion = 97,
        UnitsPercent = 98,
        UnitsPercentObscurationPerFoot = 143,
        UnitsPercentObscurationPerMeter = 144,
        UnitsPercentPerSecond = 99,
        UnitsPerMinute = 100,
        UnitsPerSecond = 101,
        UnitsPsiPerDegreeFahrenheit = 102,
        UnitsRadians = 103,
        UnitsRadiansPerSecond = 184,
        UnitsRevolutionsPerMinute = 104,
        UnitsSquareMetersPerNewton = 185,
        UnitsWattsPerMeterPerDegreeKelvin = 189,
        UnitsWattsPerSquareMeterDegreeKelvin = 141,
        UnitsPerMille = 207,
        UnitsGramsPerGram = 208,
        UnitsKiloGramsPerKiloGram = 209,
        UnitsGramsPerKiloGram = 210,
        UnitsMilliGramsPerGram = 211,
        UnitsMilliGramsPerKiloGram = 212,
        UnitsGramsPerMilliLITER = 213,
        UnitsGramsPerLiter = 214,
        UnitsMilliGramsPerLiter = 215,
        UnitsMicroGramsPerLiter = 216,
        UnitsGramsPerCubicMeter = 217,
        UnitsMilliGramsPerCubicMeter = 218,
        UnitsMicroGramsPerCubicMeter = 219,
        UnitsNanoGramsPerCubicMeter = 220,
        UnitsGramsPerCubicCentiMeter = 221,
        UnitsBecquerels = 222,
        UnitsKiloBecquerels = 223,
        UnitsMegaBecquerels = 224,
        UnitsGray = 225,
        UnitsMilliGray = 226,
        UnitsMicroGray = 227,
        UnitsSieverts = 228,
        UnitsMilliSieverts = 229,
        UnitsMicroSieverts = 230,
        UnitsMicroSievertsPerHour = 231,
        UnitsDeciblesA = 232,
        UnitsNephelometricTurbidityUnit = 233,
        UnitsPH = 234,
        UnitsGramsPerSquareMeter = 235,
        UnitsMinutesPerDegreeKelvin = 236,

        ///<summary> Enumerated values 0-255 are reserved for definition by ASHRAE. </summary>
        ///<summary> Enumerated values 256-65535 may be used by others subject to </summary>
        ///<summary> the procedures and constraints described in Clause 23. </summary>
        ///<summary> The last enumeration used in this version is 189. </summary>
        MaxUnits = 237
    }

    /// <summary>
    /// Name in Stack Application: CALENDAR_ENTRY_STATUS
    /// </summary>
    public enum CalendarEntryStatus
    {
        StatusDate = 0,
        StatusDateRange = 1,
        StatusWeekNDay = 2,
        StatusCalReff = 3
    }

    /// <summary>
    /// Name in Stack Application: BACNET_MONTH
    /// </summary>
    public enum BacnetMonth
    {
        BacnetMonthJanuary = 1,
        BacnetMonthFebruary = 2,
        BacnetMonthMarch = 3,
        BacnetMonthApril = 4,
        BacnetMonthMay = 5,
        BacnetMonthJune = 6,
        BacnetMonthJuly = 7,
        BacnetMonthAugust = 8,
        BacnetMonthSeptember = 9,
        BacnetMonthOctober = 10,
        BacnetMonthNovember = 11,
        BacnetMonthDecember = 12,
        BacnetMonthOddMonth = 13,
        BacnetMonthEvenMonth = 14,
        BacnetMonthAnymonth = 255
    }

    /// <summary>
    /// Name in Stack Application: BACNET_WEEK_OF_MONTH
    /// </summary>
    public enum BacnetWeekOfMonth
    {
        DaysNum1to7 = 1,
        DaysNum8to14 = 2,
        DaysNum15to21 = 3,
        DaysNum22to28 = 4,
        DaysNum29to31 = 5,
        Last7DaysOfMonth = 6,
        AnyWeekOfMonth = 255
    }

    /// <summary>
    /// Name in Stack Application: BACNET_WEEKDAY
    /// </summary>
    public enum BacnetWeekDay
    {
        BacnetWeekdayMonday = 1,
        BacnetWeekdayTuesday = 2,
        BacnetWeekdayWednesday = 3,
        BacnetWeekdayThursday = 4,
        BacnetWeekdayFriday = 5,
        BacnetWeekdaySaturday = 6,
        BacnetWeekdaySunday = 7,
        AnyDayOfWeek = 255
    }

    /// <summary>
    /// /* Read Range service Range types */
    /// Name in Stack Application: BACNET_READ_RANGE
    /// </summary>
    public enum BacnetReadRange
    {
        ReadRangeAll = 0,
        ReadRangeByPosition = 3,
        /* -- context tag 4 is deprecated */
        /* -- context tag 5 is deprecated */
        ReadRangeBySequenceNo = 6,
        ReadRangeByTime = 7
    }

    /// <summary>
    /// Name in Stack Application: BACNET_EVENT_TYPE
    /// </summary>
    public enum BacnetEventType
    {
        EventChangeOfBitString = 0,
        EventChangeOfState = 1,
        EventChangeOfValue = 2,
        EventCommandFailure = 3,
        EventFloatingLimit = 4,
        EventOutOfRange = 5,
        EventComplex = 6, ///<summary> -- complex-event-type  (6), see comment below </summary>
        EventUnassigned = 7, ///<summary> -- context tag 7 is deprecated </summary>
        EventChangeOfLifeSafety = 8,
        EventExtended = 9,
        EventBufferReady = 10,
        EventUnsignedRange = 11,
        EventReserved = 12,///<summary> -- enumeration value 12 is reserved for future addenda </summary>
        EventAccessEvent = 13,
        EventDoubleOutOfRange = 14,
        EventSignedOutOfRange = 15,
        EventUnsignedOutOfRange = 16,
        EventChangeOfCharacterString = 17,
        EventChangeOfStatusFlags = 18,

        EventChangeOfReliability = 19,///<summary>moved to PR14</summary>
        EventNone = 20,
        ///<summary> Add new event type defined by standard here </summary>
        MaxEventType

        ///<summary> Enumerated values 0-63 are reserved for definition by ASHRAE.  </summary>
        ///<summary> Enumerated values 64-65535 may be used by others subject to  </summary>
        ///<summary> the procedures and constraints described in Clause 23.  </summary>
        ///<summary> It is expected that these enumerated values will correspond to  </summary>
        ///<summary> the use of the complex-event-type CHOICE [6] of the  </summary>
    }

    /// <summary>
    /// Name in Stack Application: BACNET_NOTIFY_TYPE
    /// </summary>
    public enum BacnetNotifyType
    {
        NotifyAlarm = 0,
        NotifyEvent = 1,
        NotifyAckNotification = 2,
        MaxNotifyType = 3
    }

    /// <summary>
    /// Name in Stack Application: BACNET_EVENT_STATE
    /// </summary>
    public enum BacnetEventState
    {
        EventStateNormal = 0,
        EventStateFault = 1,
        EventStateOffNormal = 2,
        EventStateHighLimit = 3,
        EventStateLowLimit = 4,
        EventStateLifeSafetyAlarm = 5,
        MaxEventState = 6
    }

    ///<summary>
    ///enum for type of time stamp
    ///Name in Stack Application: BACNET_TIMESTAMP_TYPE
    ///</summary>
    public enum BacnetTimeStampType
    {
        TimestampTypeTime = 0,
        TimestampTypeSequenceNO = 1,
        TimestampTypeDateTime = 2,
        MaxTimestampType
    }

    /// <summary>
    /// Name in Stack Application: BACNET_RELIABILITY
    /// </summary>
    public enum BacnetReliability
    {
        ReliabilityNoFaultDetected = 0,
        ReliabilityNoSensor = 1,
        ReliabilityOverRange = 2,
        ReliabilityUnderRange = 3,
        ReliabilityOpenLoop = 4,
        ReliabilityShortedLoop = 5,
        ReliabilityNoOutput = 6,
        ReliabilityUnreliableOther = 7,
        ReliabilityProcessError = 8,
        ReliabilityMultiStateFault = 9,
        ReliabilityConfigurationError = 10,
        /* -- enumeration value 11 is reserved for a future addendum as per 2010 manual */
        ReliabilityCommunicationFailure = 12,
        ReliabilityMemberFault = 13,

        //ReliabilityTripped = 15,///<summary>moved to PR14</summary>
        ///<summary> Enumerated values 0-63 are reserved for definition by ASHRAE.  </summary>
        ///<summary> Enumerated values 64-65535 may be used by others subject to  </summary>
        ///<summary> the procedures and constraints described in Clause 23. </summary>
        MaxReliability = 14
    }

    /// <summary>
    /// Name in Stack Application: BACNET_LOGGING_TYPE
    /// </summary>
    public enum BacnetLoggingType
    {
        LoggingTypePolled = 0,
        LoggingTypeCOV = 1,
        LoggingTypeTriggered = 2,
        MaxLogingType = 3
    }

    /// <summary>
    /// Name in Stack Application: BACNET_APPLICATION_TAG
    /// </summary>
    public enum BacnetApplicationTag
    {
        BacnetApplicationTagNull = 0,
        BacnetApplicationTagBoolean = 1,
        BacnetApplicationTagUnsignedInt = 2,
        BacnetApplicationTagSignedInt = 3,
        BacnetApplicationTagReal = 4,
        BacnetApplicationTagDouble = 5,
        BacnetApplicationTagOctetString = 6,
        BacnetApplicationTagCharacterString = 7,
        BacnetApplicationTagBitString = 8,
        BacnetApplicationTagEnumerated = 9,
        BacnetApplicationTagDate = 10,
        BacnetApplicationTagTime = 11,
        BacnetApplicationTagObjectID = 12,
        BacnetApplicationTagReserve1 = 13,
        BacnetApplicationTagReserve2 = 14,
        BacnetApplicationTagReserve3 = 15,

        ///<summary> Special Tag Type Definations - Used for properites who have
        ///multiple data types
        ///</summary>
        BacnetApplicationTagDateTime = 16,

        BacnetApplicationTagListUnsign = 17,
        BacnetApplicationTagListChar = 18,
        BacnetApplicationTagPriortyary = 19,
        BacnetApplicationTagTimestamp = 20,
        BacnetApplicationTagActiveCOV = 21,
        BacnetApplicationTagConstructed = 22,
        BacnetApplicationTagAny = 23,

        MaxBacnetApplicationTag, ///<summary> Increment as Special Tags increases </summary>
        TagNotSupported
    }

    /// <summary>
    /// Name in Stack Application: BACNET_DEVICE_STATUS
    /// </summary>
    public enum BacnetDeviceStatus
    {
        StatusOperational = 0,
        StatusOperationalReadOnly = 1,
        StatusDownloadRequired = 2,
        StatusDownloadInProgress = 3,
        StatusNonOperational = 4,
        StatusBackupInProgress = 5,
        MaxDeviceStatus = 6
    }

    /// <summary>
    /// Name in Stack Application: BACNET_BINARY_PV
    /// </summary>
    public enum BacnetBinaryPV
    {
        BinaryInactive = 0,
        MinBinaryPV = 0,  ///<summary> for validating incoming values </summary>
        BinaryActive = 1,
        MAXBinaryPV = 1,  ///<summary> for validating incoming values </summary>
        BinaryNull = 255   ///<summary> our homemade way of storing this info </summary>
    }

    /// <summary>
    /// Name in Stack Application: BACNET_PROGRAM_STATE
    /// </summary>
    public enum BacnetProgramState
    {
        ProgramStateIdle = 0,
        ProgramStateLoading = 1,
        ProgramStateRunning = 2,
        ProgramStateWaiting = 3,
        ProgramStateHalted = 4,
        ProgramStateUnloading = 5,
        MaxProgramState = 6
    }

    /// <summary>
    /// Name in Stack Application: BACNET_PROGRAM_REQUEST
    /// </summary>
    public enum BacnetProgramRequest
    {
        ProgramRequestReady = 0,
        ProgramRequestLoad = 1,
        ProgramRequestRun = 2,
        ProgramRequestHalt = 3,
        ProgramRequestRestart = 4,
        ProgramRequestUnload = 5,
        MaxProgramRequest = 6
    }

    /// <summary>
    /// Name in Stack Application: BACNET_SHED_STATE
    /// </summary>
    public enum BACnetShedState
    {
        BacnetShedInactive = 0,
        BacnetShedRequestPending = 1,
        BacnetShedCompliant = 2,
        BacnetShedNonCompliant = 3,
        MaxShedState
    }

    /// <summary>
    /// enum for callback reason
    /// Name in Stack Application: BACNET_CALLBACK_REASON
    /// </summary>
    public enum BacnetCallbackReason
    {
        CallbackReasonNone = 0,
        CallbackReasonScheduleInternal = 1,
        CallbackReasonScheduleExternal = 2,
        CallbackReasonTrendingInternal = 3,
        CallbackReasonTrendingExternal = 4,
        CallbackReasonInitiatingCovNotification = 5,
        CallbackReasonInitiatingEventNotification = 6,
        CallbackReasonFileDataDeleted = 7,
        CallbackReasonFileDataTruncated = 8,
        CallbackReasonFileDataExpanded = 9,
        CallbackReasonBnrBftTimeout = 10,
        CallbackReasonBnrBptTimeout = 11,
        CallbackReasonBnrRptTimeout = 12,
        CallbackReasonBnrRctTimeout = 13,
        CallbackReasonDccEnabled = 14,
        CallbackReasonDccDisabled = 15,
        CallbackReasonDccDisabledInitiation = 16,
        CallbackReasonApplicationsExceedsTimeout = 17,
        CallbackReasonAsideTsmAborted = 18,
        CallbackReasonBsideTsmAborted = 19,
        CallbackReasonSocketCommunicationEnabled = 20,
        CallbackReasonSocketCommunicationDisabled = 21,
        CallbackReasonTrendingBufferHalfFull = 22,
        CallbackReasonTrendingBufferFull = 23,
        CallbackReasonBDTUpdated = 24,
        CallbackReasonFDTUpdated = 25,
        CallbackReasonNetworkNumberIs = 26,
        CallbackReasonRouterBusyToNetwork = 27,
        CallbackReasonRouterAvailableToNetwork = 28,
        CallbackReasonObjectCreated = 29,
        CallbackReasonObjectDeleted = 30,
        CallbackReasonListElementAdded = 31,
        CallbackReasonListElementDeleted = 32,
        /* add new values above this line */
        MaxCallbackReason
    }

    /// <summary>
    /// enum for type of callback
    /// Name in Stack Application: BACNET_CALLBACK_TYPE
    /// </summary>
    public enum BacnetCallbackType
    {
        CallbackTypeNone = 0,
        CallbackTypeScheduling = 1,
        CallbackTypeTrending = 2,
        CallbackTypeCov = 3,
        CallbackTypeIntrinsicAlarm = 4,
        CallbackTypeAlgorithmicAlarm = 5,
        CallbackTypeFileOperation = 6,
        CallbackTypeBackupRestoreProcedure = 7,
        CallbackTypeDccStatus = 8,
        CallbackTypeAsideTsm = 9,
        CallbackTypeBsideTsm = 10,
        CallbackTypeSocketCommunicationStatus = 11,
        CallbackTypeBBMD = 12,
        CallbackTypeNetworkLayerService = 13,
        CallbackTypeAsideServices = 14,
        CallbackTypeBsideServices = 15,
        /* add new values above this line */
        MaxCallbackType
    }

    /// <summary>
    /// Name in Stack Application: BACNET_PROPERTY_STATES
    /// </summary>
    public enum BacnetPropertyStates
    {
        PropStateBooleanValue = 0,
        PropStateBinaryValue = 1,
        PropStateEventType = 2,
        PropStatePolarity = 3,
        PropStateProgramChange = 4,
        PropStateProgramState = 5,
        PropStateReasonForHalt = 6,
        PropStateReliability = 7,
        PropStateEventState = 8,
        PropStateSystemStatus = 9,
        PropStateUnits = 10,
        PropStateUnsignedValue = 11,
        PropStateLifeSafetyMode = 12,
        PropStateLifeSafetyState = 13,
        PropStateRestartReason = 14,
        PropStateDoorAlarmState = 15,
        PropStateAction = 16,
        PropStateDoorSecuredStatus = 17,
        PropStateDoorStatus = 18,
        PropStateDoorValue = 19,
        PropStateFileAccessMethod = 20,
        PropStateLockStatus = 21,
        PropStateLifeSafetyOperation = 22,
        PropStateMaintenance = 23,
        PropStateNodeType = 24,
        PropStateNotifyType = 25,
        PropStateSecurityLevel = 26,
        PropStateShedState = 27,
        PropStateSilencedState = 28,
        PropStateAccessEvent = 30,
        PropStateZoneOccupancyState = 31,
        PropStateAccessCredDisableReason = 32,
        PropStateAccessCredDisable = 33,
        PropStateAuthenticationStatus = 34,
        PropStateBackupState = 36,
        /* addendum 2010-aa i.e. asni/ashrae 2012 manual */
        PropStateLightingWriteStatus = 37,
        /* addendum 2010-i i.e. asni/ashrae 2012 manual */
        PropStateLightingInProgress = 38,
        PropStateLightingOperation = 39,
        PropStateLightingTransition = 40,
        MaxPropState
    }

    ///<summary>
    ///enumerations for Write Status
    ///Name in Stack Application: BACNET_WRITE_STATUS
    ///</summary>
    public enum BacnetWriteStatus
    {
        BacnetWriteStatusIdle = 0,
        BacnetWriteStatusInProgress = 1,
        BacnetWriteStatusSuccessful = 2,
        BacnetWriteStatusFailed = 3,
        MaxWriteStatus
    }

    ///<summary>
    ///enumerations for Lighting In Progress
    ///Name in Stack Application: BACNET_LIGHTING_IN_PROGRESS
    ///</summary>
    public enum BacnetLightingInProgress
    {
        BacnetLightingInProgressIdle = 0,
        BacnetLightingInProgressFadeActive = 1,
        BacnetLightingInProgressRampActive = 2,
        BacnetLightingInProgressNotControlled = 3,
        BacnetLightingInProgressOther = 4,
        MaxLightingInProgress
    }

    ///<summary>
    ///Enums for callback configurations type
    ///Name in Stack Application: BACNET_CALLBACK_CONFIG_TYPE
    ///</summary>
    public enum BacnetCallbackConfigType
    {
        CallbackConfigNotRequired = 0,
        CallbackConfigRequired = 1,
        CallbackConfigBeforeExecution = 2,
        CallbackConfigAfterExecution = 3,
        /* add new values here */
        MaxCallbackConfigType
    }

    ///<summary>
    ///Enum defining stack's max limits parameters
    ///Name in Stack Application: BACNET_MAXLIMIT_PARAMETERS
    ///</summary>
    public enum BacnetMaxlimitParameters
    {
        BacnetMaxlimitInitiateQNodes = 1,
        BacnetMaxlimitBBMDInitiateQNodes = 2,
        BacnetMaxlimitMaxBdtFdtEntries = 3,
        BacnetMaxlimitMaxObject = 4,
        BacnetMaxlimitMaxDynamicAddBind = 5,
        BacnetMaxlimitMaxCovSubsTx = 6,
        BacnetMaxlimitMaxStateText = 7,
        BacnetMaxlimitMaxAFValuesList = 8,
        BacnetMaxlimitMaxExSchdList = 9,
        BacnetMaxlimitMaxTimeValueList = 10,
        BacnetMaxlimitMaxDateList = 11,
        BacnetMaxlimitMaxDestinationList = 12,
        BacnetMaxlimitMaxDevobjproprefList = 13,
        BacnetMaxlimitMaxEventParamList = 14,
        BacnetMaxlimitMaxObjidList = 15,
        BacnetMaxlimitMaxActiveCovSubs = 16,
        BacnetMaxlimitMaxStaticAddBind = 17,
        BacnetMaxlimitMaxNwAnalizability = 18,
        BacnetMaxlimitCallbackNotifyQNodes = 19,
        BacnetMaxlimitMaxFaultParamList = 20,
        BacnetMaxlimitMaxCovUList = 21,
        BacnetMaxlimitMaxSubordinateList = 22,
        /* add new max limits parameters as applicable */
        MaxBacnetMaxlimitParameters
    }

    ///<summary>
    ///Enumeration for B-Side callbacks or Auto-Responses
    ///Name in Stack Application: BACAPP_CALLBACK_FUN_CHOICE
    ///</summary>
    public enum BacAppCallBackFunChoice
    {
        AppCbReadProperty = 0,
        AppCbReadPropertyMultiple = 1,
        AppCbReadRange = 2,
        AppCbWriteProperty = 3,
        AppCbWritePropertyMultiple = 4,
        AppCbAddListElement = 5,
        AppCbRemoveListElement = 6,
        AppCbCreateObject = 7,
        AppCbDeleteObject = 8,
        AppCbAtomicReadFile = 9,
        AppCbAtomicWriteFile = 10,
        AppCbSubscribeCOV = 11,
        AppCbSubscribeCOVP = 12,
        AppCbCOVNotification = 13,
        AppCbEventNotification = 14,
        AppCbAcknowledgeAlarm = 15,
        AppCbGetAlarmSummary = 16,
        AppCbGetEnrollmentSummary = 17,
        AppCbGetEventInformation = 18,
        AppCbDeviceCommunicationControl = 19,
        AppCbReinitializeDevice = 20,
        AppCbPrivateTransfer = 21,
        AppCbTextMessage = 22,
        AppCbTimeSync = 23,
        AppCbUtcTimeSync = 24,
        AppCbWhoIs = 25,
        AppCbWhoHas = 26,
        AppCbIAm = 27,
        AppCbIHave = 28,
        AppCbVtOpen = 29,
        AppCbVtClose = 30,
        AppCbVtData = 31,

        ///<summary> Add new values here </summary>
        MaxAppCallbackFun
    }

    /// <summary>
    /// Enum defining the reinitialize device states
    /// Name in Stack Application: BACNET_REINITIALIZED_STATE
    /// </summary>
    /// <CreatedBy>prasad.joshi</CreatedBy><TimeStamp>6/20/201711:19 AM</TimeStamp>
    public enum BacnetReinitializedState
    {
        BacnetReinitColdstart = 0,
        BacnetReinitWarmstart = 1,
        BacnetReinitStartBackup = 2,
        BacnetReinitEndBackup = 3,
        BacnetReinitStartRestore = 4,
        BacnetReinitEndRestore = 5,
        BacnetReinitAbortRestore = 6,
        BacnetReinitReinitializedState = 7,
        BacnetReinitIdle = 255
    }

    /// <summary>
    /// Name in Stack Application: BACNET_COMMUNICATION_STATE
    /// </summary>
    public enum BacnetCommunicationState
    {
        CommunicationEnable = 0,
        CommunicationDisable = 1,
        CommunicationDisableInitiation = 2,
        MaxBacnetCommunicationState = 3
    }

    /// <summary>
    /// Name in Stack Application: PROP_ACCESS_TYPE
    /// </summary>
    public enum PropAccessType
    {
        NOT_SUPPORTED = 0,
        READ,
        READ_ONLY,
        READ_WRITE,
        BACNET_DEFAULT,
        COMMANDABLE_PROP,
        ACCESS_DENIED,
        READ_ANYWAY = 51,
        WRITE_ANYWAY = 52,
        IGNORE_ACCESS_TYPE = -1
    }

    /* enum for restart reason */
    public enum BACnetRestartReason
    {
        RESTART_REASON_UNKNOWN = 0,
        RESTART_REASON_COLDSTART = 1,
        RESTART_REASON_WARMSTART = 2,
        RESTART_REASON_DETECTED_POWER_LOST = 3,
        RESTART_REASON_DETECTED_POWERED_OFF = 4,
        RESTART_REASON_HARDWARE_WATCHDOG = 5,
        RESTART_REASON_SOFTWARE_WATCHDOG = 6,
        RESTART_REASON_SUSPENDED = 7,
        MAX_RESTART_REASON = 8
    }
}