﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.StackDataObjects.Constants
{
    public class MolexAppConstants
    {
        //This flag is used to notify stack to set the reliablity if timevalue arrary
        //in Weekly schedule or Exception schedule has time difference of less than 1 minute.
        public const int BACNET_ERROR_SCHEDULE_RELIABILITYFLAG = -1;
    }
}
