﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Response
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.Response
{
    public class GetObjectPropertyValueResponse : ResponseBase
    {
        public object PropertyValue { get; set; }
        public BacnetDataType DataType { get; set; }
    }
}
