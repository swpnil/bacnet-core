﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Response
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.APIModels;
using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.Response
{
    public class ReadRangeResponse : ResponseBase
    {
        public byte ArrayIndexPresent { get; set; }
        public BacnetObjectType ObjectType { get; set; }
        public uint ObjectId { get; set; }
        public BacnetPropertyID PropertyId { get; set; }
        public uint ArrayIndex { get; set; }
        public uint ItemCount { get; set; }
        public uint FirstSeqNo { get; set; }
        public BacnetBitStrModel ResultFlags { get; set; }
        public uint DataLen { get; set; }
        public BacnetDataType DataType { get; set; }
        public object PropertyVal { get; set; }
    }
}
