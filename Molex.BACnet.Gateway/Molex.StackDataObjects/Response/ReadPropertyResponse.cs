﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Response
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------

namespace Molex.StackDataObjects.Response
{
    public class ReadPropertyResponse : ResponseBase
    {
        public object PropertyValue { get; set; }
        public string Type { get; set; }
    }
}
