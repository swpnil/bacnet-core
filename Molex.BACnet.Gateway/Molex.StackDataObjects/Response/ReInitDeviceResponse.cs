﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       UniDEL Pte. Ltd
///   Class:              ReInitDeviceResponse
///   Description:        This class as response model for Reinitialize device service
///   Author:             Prasad Joshi                  
///   Date:               06/20/17
#endregion

using Molex.StackDataObjects.APIModels;
using Molex.StackDataObjects.Response.AutoResponses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.StackDataObjects.Response
{
    /// <summary>
    /// This class as response model for Reinitialize device service
    /// </summary>
    /// <CreatedBy>prasad.joshi</CreatedBy><TimeStamp>6/20/20171:57 PM</TimeStamp>
    /// <seealso cref="Molex.StackDataObjects.Response.AutoResponses.AutoResponse" />
    public class ReInitDeviceResponse : AutoResponse
    {
        /// <summary>
        /// Gets or sets the re initialize device.
        /// </summary>
        /// <value>
        /// The re initialize device.
        /// </value>
        /// <CreatedBy>prasad.joshi</CreatedBy><TimeStamp>6/20/20171:57 PM</TimeStamp>
        public ReInitDeviceRequestModel ReInitializeDevice { get; set; }
    }
}
