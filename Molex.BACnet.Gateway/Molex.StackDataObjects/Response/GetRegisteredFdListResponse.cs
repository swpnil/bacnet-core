﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Response
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using System.Collections.Generic;

namespace Molex.StackDataObjects.Response
{
    public class GetRegisteredFdListResponse : ResponseBase
    {
        public GetRegisteredFdListResponse()
        {
            RegisteredFDList = new List<RegisteredFDListResponseModel>();
        }

        public List<RegisteredFDListResponseModel> RegisteredFDList { get; set; }
    }
}
