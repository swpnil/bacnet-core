﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Response
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using System.Collections.Generic;
using Molex.StackDataObjects.APIModels;

namespace Molex.StackDataObjects.Response
{
    public class ReadFDTResponse : ResponseBase
    {
        public ReadFDTResponse()
        {
            FDTEntries = new List<FDTModel>();
        }

        public List<FDTModel> FDTEntries { get; set; }
    }
}
