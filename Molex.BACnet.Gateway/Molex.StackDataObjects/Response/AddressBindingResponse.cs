﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Response
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using System.Collections.Generic;

namespace Molex.StackDataObjects.Response
{
    public class AddressBindingResponse : ResponseBase
    {
        public AddressBindingResponse()
        {
            AddressBindingResponseData = new List<AddressBindingDataModel>();
        }

        public List<AddressBindingDataModel> AddressBindingResponseData { get; set; }

    }
}
