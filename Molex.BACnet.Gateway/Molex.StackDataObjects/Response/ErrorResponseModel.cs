﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Response
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------

namespace Molex.StackDataObjects.Response
{
    public class ErrorResponseModel
    {
        public ErrorType Type { get; set; }
        public int ErrorClass { get; set; }
        public int ErrorCode { get; set; }
        public int FirstFailedNo { get; set; }
    }

    public enum ErrorType
    {
        Unknown = 1,
        StackError,
        BACnetError,
    }
}
