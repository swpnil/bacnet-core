﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Molex.StackDataObjects.APIModels;

namespace Molex.StackDataObjects.Response.InternalResponses
{
    public class ListElementInternalResponse : InternalResponse
    {
        public ListElementRequestModel ListElementInternalModelData { get; set; }
    }
}
