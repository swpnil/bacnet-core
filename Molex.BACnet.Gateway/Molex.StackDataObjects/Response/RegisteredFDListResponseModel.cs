﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Response
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------

namespace Molex.StackDataObjects.Response
{
    public class RegisteredFDListResponseModel
    {
        public ushort TimeToLive { get; set; }
        public ushort TimeRemaining { get; set; }
        public string IpAddress { get; set; }
        public ushort PortNo { get; set; }
    }
}
