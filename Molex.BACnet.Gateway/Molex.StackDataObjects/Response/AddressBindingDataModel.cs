﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Response
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.APIModels;
using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.Response
{
    public class AddressBindingDataModel
    {
        public uint ObjId { get; set; }
        public BacnetObjectType ObjectType { get; set; }
        public ushort MaxAPDULengthAccepted { get; set; }
        public ushort VendorId { get; set; }
        public BacnetSegmentation SegmentationSupport { get; set; }
        public BacnetAddressModel BACnetDeviceAddress { get; set; }
        public int Next { get; set; }
    }
}
