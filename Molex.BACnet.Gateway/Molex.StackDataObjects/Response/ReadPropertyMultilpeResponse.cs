﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Response
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using System.Collections.Generic;
using Molex.StackDataObjects.APIModels;
using Molex.StackDataObjects.Constants;

namespace Molex.StackDataObjects.Response
{
    public class ReadPropertyMultipleResponse : ResponseBase
    {
        public List<ReadPropertyMultipleDataModel> ReadPropertyMultipleResponseData { get; set; }

        public ReadPropertyMultipleResponse()
        {
            ReadPropertyMultipleResponseData = new List<ReadPropertyMultipleDataModel>();
        }
    }

    public class ReadPropertyMultipleDataModel
    {
        public object PropertyValue { get; set; }
        public string Type { get; set; }
        public bool IsSucceed { get; set; }
        public BacnetErrorClass ErrorClass { get; set; }
        public BacnetErrorCode ErrorCode { get; set; }
        public ReadPropertyMultipleRequestModel ReadPropertyRequest { get; set; }
    }
}
