﻿using Molex.StackDataObjects.APIModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.StackDataObjects.Response.AutoResponses
{
    /// <summary>
    /// This class used for hold model of time sync notification
    /// </summary>
    /// <CreatedBy>prasad.joshi</CreatedBy><TimeStamp>8/28/20172:29 PM</TimeStamp>
    /// <seealso cref="Molex.StackDataObjects.Response.AutoResponses.AutoResponse" />
    public class TimeSyncNotification : AutoResponse
    {
        /// <summary>
        /// Gets or sets the time synchronize response.
        /// </summary>
        /// <value>
        /// The time synchronize response.
        /// </value>
        /// <CreatedBy>prasad.joshi</CreatedBy><TimeStamp>8/28/20172:28 PM</TimeStamp>
        public TimeSyncModel TimeSyncResponse { get; set; }
    }
}
