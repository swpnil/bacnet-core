﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Response.AutoResponses
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------

namespace Molex.StackDataObjects.Response.AutoResponses
{
    public class AutoResponse
    {
        public bool IsSucceed { get; set; }
        public ErrorResponseModel ErrorModel { get; set; }
    }
}
