﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.AutoResponses
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------

using Molex.StackDataObjects.APIModels;
namespace Molex.StackDataObjects.Response.AutoResponses
{
    public class StackInternalNotificationResponse : AutoResponse
    {
        public StackInternalNotificationModel StackInternalNotification { get; set; }
    }
}
