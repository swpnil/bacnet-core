﻿using Molex.StackDataObjects.APIModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.StackDataObjects.Response.AutoResponses
{
    public class RemoveListElementResponse : AutoResponse
    {
        public RemoveListElementRequestModel RemoveListElementResponseData { get; set; }
    }
}
