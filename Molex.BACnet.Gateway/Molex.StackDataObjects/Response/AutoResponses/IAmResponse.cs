﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Response.AutoResponses
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------

namespace Molex.StackDataObjects.Response.AutoResponses
{
    public class IAmResponse : AutoResponse
    {
        public AddressBindingDataModel IAmNotification { get; set; }
    }
}
