﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Response.AutoResponses
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.APIModels;

namespace Molex.StackDataObjects.Response.AutoResponses
{
    public class WritePropertyNotificationResponse : AutoResponse
    {
        public bool IsSuccessSendBack { get; set; }

        public WritePropertyRequestModel WritePropertyCallbackData { get; set; }
    }
}
