﻿using Molex.StackDataObjects.APIModels;
using Molex.StackDataObjects.Response.AutoResponses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Molex.BACnetStackIntegration.Processor
{
    public class AddListElementResponse : AutoResponse
    {
        public AddListElementRequestModel AddListElementResponseData { get; set; }
    }
}
