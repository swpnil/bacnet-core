﻿using Molex.StackDataObjects.APIModels;

namespace Molex.StackDataObjects.Response.AutoResponses
{
    public class DeviceCommunicationControl : AutoResponse
    {
        public bool IsSuccessSendBack { get; set; }

        public DeviceCommunicationControlModel WritePropertyCallbackData { get; set; }
    }
}
