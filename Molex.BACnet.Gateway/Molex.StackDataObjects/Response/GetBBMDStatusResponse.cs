﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Response
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------

namespace Molex.StackDataObjects.Response
{
    public class GetBBMDStatusResponse : ResponseBase
    {
        public bool IsBBMDEnabled { get; set; }
    }
}
