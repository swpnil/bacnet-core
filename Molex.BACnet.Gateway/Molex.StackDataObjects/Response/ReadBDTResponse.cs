﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Response
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using System.Collections.Generic;
using Molex.StackDataObjects.APIModels;

namespace Molex.StackDataObjects.Response
{
    public class ReadBDTResponse : ResponseBase
    {
        public ReadBDTResponse()
        {
            BDTEntries = new List<BDTModel>();
        }

        public List<BDTModel> BDTEntries { get; set; }
    }
}
