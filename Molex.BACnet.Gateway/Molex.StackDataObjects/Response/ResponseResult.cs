﻿///-----------------------------------------------------------------
/// Project: Molex.StackDataObjects 
/// Namespace: Molex.StackDataObjects.Response
/// Class: <ClassName>
/// Description: <Description>
 

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using System;
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Request;

namespace Molex.StackDataObjects.Response
{
    public class ResponseResult
    {
        public bool IsSucceed { get; set; }
        public int TokenId { get; set; }
        public ResponseBase Response { get; set; }
        public int ErrorCode { get; set; }
        public RequestBase Request { get; set; }
        public ErrorResponseModel ErrorModel { get; set; }
        public bool StopProcessing { get; set; }
        public DateTime PacketReceivedTime { get; set; }
        public StackConstants.RequestType ReqType { get; set; }
    }
}
