﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              NetworkInterfaceHelper
///   Description:        It is Responsible To Manage Network Interface.
///   Author:             Anish Nair                  
///   Date:               10/03/19
///---------------------------------------------------------------------------------------------
#endregion

using Molex.BACnet.Configuration.Core.Models;
using Molex.BACnet.Gateway.Log;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;

namespace Molex.BACnet.Configuration.Core.Helper
{
    public class NetworkInterfaceHelper
    {
        /// <summary>
        /// Gets all IP Address for  network interfaces.
        /// </summary>
        /// <returns>return list of IPAddress</returns>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>08-06-201718:48</TimeStamp>"
        public static ResultModel<List<IPAddressModel>> GetAllNetworkInterfaces()
        {
            ResultModel<List<IPAddressModel>> resultModel = new ResultModel<List<IPAddressModel>>();
            try
            {
                List<IPAddressModel> networkInterfacesList = new List<IPAddressModel>();
                NetworkInterface[] networkInterfaces = NetworkInterface.GetAllNetworkInterfaces();
                if (networkInterfaces != null && networkInterfaces.Length > 0)
                {
                    foreach (NetworkInterface item in networkInterfaces)
                    {
                        IPInterfaceProperties ipInterface = item.GetIPProperties();
                        if (ipInterface.UnicastAddresses.Count > 0)
                        {
                            foreach (UnicastIPAddressInformation ip in ipInterface.UnicastAddresses)
                            {
                                if (!IPAddress.IsLoopback(ip.Address) && ip.Address.AddressFamily == AddressFamily.InterNetwork)
                                {
                                    IPAddressModel networkInterfaceModel = new Models.IPAddressModel();
                                    networkInterfaceModel.NetworkInterfaceType = item.NetworkInterfaceType.ToString();
                                    networkInterfaceModel.OperationalStatus = item.OperationalStatus.ToString();
                                    networkInterfaceModel.IPAddress = ip.Address.ToString();
                                    networkInterfaceModel.NetworkInterfaceName = item.Name.ToString();
                                    networkInterfacesList.Add(networkInterfaceModel);
                                }
                            }
                        }
                    }
                }
                resultModel.Result = networkInterfacesList;
                resultModel.IsSucceeded = true;
                Logger.Instance.Log(LogLevel.DEBUG, "GetAllNetworkInterfaces is executed successfully. ");
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "GetAllNetworkInterfaces:Error occured" + ex);
                resultModel.IsSucceeded = false;
                resultModel.Error = new ResponseErrorModel();
                resultModel.Error.ErrorCategory = 1;
                resultModel.Error.ErrorCode = 1;
                resultModel.Error.ErrorMessage = ex.Message;
            }
            return resultModel;

        }
    }
}