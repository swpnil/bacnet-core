﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2019 All Rights Reserved
///   Company Name:       Molex
///   Class:              NetworkInterfaceSettingManager
///   Description:        To Get All IP Address from Network Interface
///   Author:             Anish Nair                  
///   Date:               10/03/19
///---------------------------------------------------------------------------------------------
#endregion

using Molex.BACnet.Configuration.Core.Helper;
using Molex.BACnet.Configuration.Core.Models;
using Molex.BACnet.Configuration.Core.Repository;
using Molex.BACnet.Gateway.Log;
using System;
using System.Collections.Generic;

namespace Molex.BACnet.Configuration.Core.Manager
{
    public class NetworkInterfaceSettingManager
    {
        /// <summary>
        /// Gets all IPAddress for network interfaces.
        /// </summary>
        /// <returns>Returns result model contains either list of IP Address or Error model</returns>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>08-06-201717:54</TimeStamp>"
        public static ResultModel<NetworkInterfaceModel> GetAllNetworkInterfaces()
        {
            ResultModel<NetworkInterfaceModel> resultModel = new ResultModel<NetworkInterfaceModel>();
            ResultModel<List<IPAddressModel>> iPAddressList = new ResultModel<List<IPAddressModel>>();
            try
            {
                iPAddressList = NetworkInterfaceHelper.GetAllNetworkInterfaces();
                if (iPAddressList.IsSucceeded)
                {
                    foreach (var item in iPAddressList.Result)
                    {
                        if (AppWebRepository.Instance.BACnetConfigurationData.IPAddress == item.IPAddress)
                            item.IsSelected = true;
                    }
                }
                //AppWebRepository.Instance.BACnetConfigurationData = Session["BACnetConfigurationData"] as BACnetConfigurationModel;
                resultModel.Result = new NetworkInterfaceModel();
                resultModel.Result.IPAddressList = new List<IPAddressModel>();
                resultModel.Result.IPAddressList = iPAddressList.Result;
                resultModel.Result.UDPPort = AppWebRepository.Instance.BACnetConfigurationData.UDPPort.ToString();
                resultModel.Result.LocalNetworkNumber = AppWebRepository.Instance.BACnetConfigurationData.LocalNetworkNumber.ToString();
                resultModel.Result.VirtualNetworkNumber = AppWebRepository.Instance.BACnetConfigurationData.VirtualNetworkNumber.ToString();
                resultModel.IsSucceeded = true;
                Logger.Instance.Log(LogLevel.DEBUG, "GetAllNetworkInterfaces is executed successfully. ");
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "GetAllNetworkInterfaces:Error occured" + ex);
                resultModel.IsSucceeded = false;
                resultModel.Error = new ResponseErrorModel();
                resultModel.Error.ErrorCategory = 1;
                resultModel.Error.ErrorCode = 1;
                resultModel.Error.ErrorMessage = ex.Message;
            }
            return resultModel;
        }
    }
}