﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2019 All Rights Reserved
///   Company Name:       Molex
///   Class:              ConfigurationSettingManager
///   Description:        To Manage Read/Write Opearions for Json File
///   Author:             Anish Nair                  
///   Date:               10/03/19
///---------------------------------------------------------------------------------------------
#endregion

using Molex.BACnet.Configuration.Core.Models;
using Molex.BACnet.Gateway.Encryption;
using Molex.BACnet.Gateway.Log;
using Molex.BACnet.Gateway.ServiceCore.SingletonCore;
using Molex.BACnet.Gateway.Utility.Helper;
using System;
using System.Collections.Generic;
using System.IO;
using BACnetConfigurationModel = Molex.BACnet.Configuration.Core.Models.BACnetConfigurationModel;

namespace Molex.BACnet.Configuration.Core.Manager
{
    public class ConfigurationSettingManager : SingletonBase<ConfigurationSettingManager>
    {
        /// <summary>
        /// This method is used to read COnfiguration setting from json file.If File does not exist , It creates new json file using default values 
        /// </summary>
        /// <param name="jsonFilePath">It is the file location from where configuration setting file to be read.</param>
        /// <returns>return configuration data to the BCT user</returns>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>08-06-201717:45</TimeStamp>"
        public ResultModel<BACnetConfigurationModel> ReadBACnetConfigurationSetting(string jsonFilePath)
        {

            ResultModel<BACnetConfigurationModel> resultModel = new ResultModel<BACnetConfigurationModel>();
            BACnetConfigurationModel configurationModel = new BACnetConfigurationModel();
            try
            {
                if (File.Exists(jsonFilePath))
                {
                    configurationModel = JSONReaderWriterHelper.ReadJsonFromFile<BACnetConfigurationModel>(jsonFilePath);
                  
                    if (!string.IsNullOrWhiteSpace(configurationModel.UserName))
                        configurationModel.UserName = CryptoHelper.Decrypt3DES(configurationModel.UserName);
                    if (!string.IsNullOrWhiteSpace(configurationModel.Password))
                        configurationModel.Password = CryptoHelper.Decrypt3DES(configurationModel.Password);
                    if (!string.IsNullOrWhiteSpace(configurationModel.BACnetPassword))
                        configurationModel.BACnetPassword = CryptoHelper.Decrypt3DES(configurationModel.BACnetPassword);
                  
                    if (configurationModel != null)
                    {
                        resultModel.IsSucceeded = true;
                        resultModel.Result = configurationModel;
                        Logger.Instance.Log(LogLevel.DEBUG, "ReadBACnetConfigurationSetting is executed successfully. ");
                    }
                    else
                    {
                        Logger.Instance.Log(LogLevel.ERROR, "ReadBACnetConfigurationSetting:Error occured");
                        resultModel.IsSucceeded = false;
                        resultModel.Error = new ResponseErrorModel();
                        resultModel.Error.ErrorCategory = 4;
                        resultModel.Error.ErrorCode = 4;
                        resultModel.Error.ErrorMessage = "File data type is mismatched.";
                    }
                }
                else
                {
                    Logger.Instance.Log(LogLevel.ERROR, "ReadBACnetConfigurationSetting:Error occured");
                    resultModel.IsSucceeded = false;
                    resultModel.Error = new ResponseErrorModel();
                    resultModel.Error.ErrorCategory = 4;
                    resultModel.Error.ErrorCode = 4;
                    resultModel.Error.ErrorMessage = "BACnet configuration file is not found.";
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "ReadBACnetConfigurationSetting:Error occured" + ex);
                resultModel.IsSucceeded = false;
                resultModel.Error = new ResponseErrorModel();
                resultModel.Error.ErrorCategory = 1;
                resultModel.Error.ErrorCode = 1;
                resultModel.Error.ErrorMessage = ex.Message;
            }
            return resultModel;
        }

        /// <summary>
        /// Creates the default configuration setting.
        /// </summary>
        /// <returns>return configuration setting model</returns>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>08-06-201717:48</TimeStamp>"
        private BACnetConfigurationModel CreateDefaultConfigurationSetting()
        {
            BACnetConfigurationModel configurationModel = new BACnetConfigurationModel();
            configurationModel.IPAddress = "172.16.0.134";
            configurationModel.UDPPort = 47808;
            configurationModel.IsBBMD = false;
            //configurationModel.ObjectName = "Molex_BACnet_Gateway";
            //configurationModel.ObjectIdentifier = 632;
            configurationModel.VendorName = "Molex Inc.";
            configurationModel.ModelName = "CoreSync Lighting BACnet Gateway";
            //configurationModel.ApplicationSoftwareVersion = "0.0.1.0";
            //configurationModel.FirmwareRevision = "1.0.0.0";
            //configurationModel.Location = "Pune, India";
            configurationModel.Description = "Unidel";
            configurationModel.APDUTimeout = 6000;
            configurationModel.APDURetries = 3;
            configurationModel.APDUSegmentTimeout = 5000;
            configurationModel.UTCOffset = -800;
            configurationModel.DaylightSavingStatus = true;
            //configurationModel.SerialNumber = "632";
            configurationModel.NotifyType = 0;
            //configurationModel.DebugLogLevel = "5";
            configurationModel.TimeDelay = 0;
            configurationModel.IsDebugLogEnable = true;
            configurationModel.BACnetPassword = "BACnet";
            configurationModel.WriteAccess = true;
            configurationModel.TimeSynchronization = false;
            configurationModel.Scheduling = true;
            configurationModel.ControllerId = 1;

            return configurationModel;
        }

        /// <summary>
        /// Updates the bacnet configuration setting.
        /// </summary>
        /// <param name="jsonFilePath">The json file path.</param>
        /// <param name="configurationModel">The configuration model.</param>
        /// <returns>return result model contains either Configuration setting data or Error message</returns>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>08-06-201717:50</TimeStamp>"
        public ResultModel<BACnetConfigurationModel> UpdateBACnetConfigurationSetting(BACnetConfigurationModel configurationModel, string jsonFilePath)
        {
            bool bResult = false;
            ResultModel<BACnetConfigurationModel> resultModel = new ResultModel<BACnetConfigurationModel>();
            try
            {
                //if (File.Exists(jsonFilePath))
                //{
                    if (!string.IsNullOrWhiteSpace(configurationModel.UserName))
                        configurationModel.UserName = CryptoHelper.Encrypt3DES(configurationModel.UserName);
                    if (!string.IsNullOrWhiteSpace(configurationModel.Password))
                        configurationModel.Password = CryptoHelper.Encrypt3DES(configurationModel.Password);
                    if (!string.IsNullOrWhiteSpace(configurationModel.BACnetPassword))
                        configurationModel.BACnetPassword = CryptoHelper.Encrypt3DES(configurationModel.BACnetPassword);
                  
                    bResult = JSONReaderWriterHelper.WriteToJsonFile(jsonFilePath, configurationModel);
                  
                    if (bResult)
                    {
                        resultModel.IsSucceeded = true;
                        resultModel.Result = configurationModel;
                        Logger.Instance.Log(LogLevel.DEBUG, "UpdateBACnetConfigurationSetting is executed successfully. ");
                    }
                //}
                //else
                //{
                //    resultModel.IsSucceeded = false;
                //    resultModel.Result = configurationModel;
                //    resultModel.Error = new ResponseErrorModel();
                //    resultModel.Error.ErrorCategory = 4;
                //    resultModel.Error.ErrorCode = 4;
                //    Logger.Instance.Log(LogLevel.DEBUG, "UpdateBACnetConfigurationSetting :File not found.");
                //}
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "UpdateBACnetConfigurationSetting:Error occured" + ex);
                resultModel.IsSucceeded = false;
                resultModel.Error = new ResponseErrorModel();
                resultModel.Error.ErrorCategory = 1;
                resultModel.Error.ErrorCode = 1;
                resultModel.Error.ErrorMessage = ex.Message;
            }
            return resultModel;
        }

        /// <summary>
        /// Validates the bacnet configuration setting using json schema.
        /// </summary>
        /// <param name="jsonFilePath">The json file path.</param>
        /// <param name="jsonSchemaFilePath">The json schema file path.</param>
        /// <returns>return result model contains either success or failed.</returns>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>08-06-201717:52</TimeStamp>"
        public static ResultModel<bool> ValidateBACnetConfigurationSetting(string jsonFilePath, string jsonSchemaFilePath)
        {
            ResultModel<bool> resultModel = new ResultModel<bool>();
            //Dictionary<bool, IList<string>> resultData = new Dictionary<bool, IList<string>>();
            //IList<string> errormessage = null;
            try
            {

                //bool bResult = JSONReaderWriterHelper.ValidateJsonFile(jsonFilePath, jsonSchemaFilePath, out errormessage);

                //if (!bResult)
                //{
                //    foreach (var error in errormessage)
                //    {
                //        Logger.Instance.Log(LogLevel.DEBUG, "Error Message while validating Json File : " + error);
                //    }
                //    resultModel.IsSucceeded = false;
                //    return resultModel;
                //}
                //Logger.Instance.Log(LogLevel.DEBUG, "ValidateBACnetConfigurationSetting is executed successfully. ");
                resultModel.IsSucceeded = true;
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "ValidateBACnetConfigurationSetting:Error occured" + ex);
                resultModel.IsSucceeded = false;
                resultModel.Error = new ResponseErrorModel();
                resultModel.Error.ErrorCategory = 1;
                resultModel.Error.ErrorCode = 1;
                resultModel.Error.ErrorMessage = ex.Message;
            }
            return resultModel;

        }
    }
}