﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2019 All Rights Reserved
///   Company Name:       Molex
///   Class:              ServiceControllingManager
///   Description:        To Control Molex Gateway window service 
///   Author:             Anish Nair                  
///   Date:               10/03/19
///---------------------------------------------------------------------------------------------
#endregion

using Molex.BACnet.Configuration.Core.Helper;
using Molex.BACnet.Configuration.Core.Models;
using Molex.BACnet.Gateway.Log;
using Molex.BACnet.Gateway.ServiceCore.Helpers;
using System;

namespace Molex.BACnet.Configuration.Core.Manager
{
    public class ServiceControllingManager
    {
        /// <summary>
        /// Check whether it is installed . if it is installed,to start the gateway service.
        /// </summary>
        /// <param name="serviceName">Name of the service.</param>
        /// <returns>return result model with status or error</returns>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>08-06-201718:07</TimeStamp>"
        public static ResultModel<string> StartGatewayService(string bACnetGWServiceName)
        {
            ResultModel<string> resultModel = new ResultModel<string>();
            try
            {
                if (!AppHelper.IsGatewayRunning)
                    AppHelper.Start();

                resultModel.IsSucceeded = true;
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "StartService:Error occured" + ex);
                resultModel.IsSucceeded = false;
                resultModel.Error = new ResponseErrorModel();
                resultModel.Error.ErrorCategory = 1;
                resultModel.Error.ErrorCode = 1;
                resultModel.Error.ErrorMessage = ex.Message;
            }
            return resultModel;
        }
        /// <summary>
        /// Check whether it is installed . if it is installed,Stops the gateway service.
        /// </summary>
        /// <param name="serviceName">Name of the service.</param>
        /// <returns>return result model with status or error</returns>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>08-06-201718:07</TimeStamp>"
        public static ResultModel<string> StopGatewayService(string bACnetGWServiceName)
        {
            ResultModel<string> resultModel = new ResultModel<string>();
            try
            {
                if (AppHelper.IsGatewayRunning)
                    AppHelper.DeInit();
                resultModel.IsSucceeded = true;
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "StoptService:Error occured" + ex);
                resultModel.IsSucceeded = false;
                resultModel.Error = new ResponseErrorModel();
                resultModel.Error.ErrorCategory = 1;
                resultModel.Error.ErrorCode = 1;
                resultModel.Error.ErrorMessage = ex.Message;
            }
            return resultModel;
        }

        /// <summary>
        /// Check whether it is installed . if it is installed,Get present status of the gateway service .
        /// </summary>
        /// <param name="serviceName">Name of the service.</param>
        /// <returns></returns>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>08-06-201718:09</TimeStamp>"
        public static ResultModel<string> RefreshGatewayServiceStatus(string bACnetGWServiceName)
        {
            ResultModel<string> resultModel = new ResultModel<string>();
            try
            {
                resultModel.IsSucceeded = true;
                if (AppHelper.IsGatewayRunning)
                    resultModel.Result = "Running";
                else
                    resultModel.Result = "Stopped";
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "RefreshGatewayServiceStatus:Error occured" + ex);
                resultModel.IsSucceeded = false;
                resultModel.Error = new ResponseErrorModel();
                resultModel.Error.ErrorCategory = 1;
                resultModel.Error.ErrorCode = 1;
                resultModel.Error.ErrorMessage = ex.Message;
            }
            return resultModel;
        }
    }
}