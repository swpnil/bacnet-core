﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2019 All Rights Reserved
///   Company Name:       Molex
///   Class:              AppWebRepository
///   Description:        Used to save context for BACnet configuration used by Self host Web API
///   Author:             Anish Nair                  
///   Date:               10/03/19
///---------------------------------------------------------------------------------------------
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Molex.BACnet.Configuration.Core.Models;
using Molex.BACnet.Gateway.ServiceCore.SingletonCore;

namespace Molex.BACnet.Configuration.Core.Repository
{
    public class AppWebRepository : SingletonBase<AppWebRepository>
    {
        /// <summary>
        /// Gets or sets the log folder path.
        /// </summary>
        public string LoggerFolderName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is logger disabled.
        /// </summary>
        public bool IsLoggerDisabled { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is syslog format disabled.
        /// </summary>
        public bool IsSyslogFormatDisabled { get; set; }

        /// <summary>
        /// Gets or sets the BACnet configuration data.
        /// </summary>
        public BACnetConfigurationModel BACnetConfigurationData { get; set; }

        /// <summary>
        /// the BCT manual folder contains folder path for Manual .
        /// </summary>
        /// <value>
        /// The BCT manual folder.
        /// </value>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>29-06-201714:42</TimeStamp>"
        public string BCTManualFolder { get; set; }
        /// <summary>
        /// the confiuration folder contains folder path for GW configuration Folder  .
        /// </summary>
        /// <value>
        /// The confiuration folder.
        /// </value>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>29-06-201714:42</TimeStamp>"
        public string ConfiurationFolder { get; set; }

        /// <summary>
        /// Gets or sets the user identifier.
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>29-06-201714:43</TimeStamp>"
        public string UserID { get; set; }
        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>
        /// The password.
        /// </value>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>29-06-201714:43</TimeStamp>"
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the service status.
        /// </summary>
        /// <value>
        /// The service status.
        /// </value>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>29-06-201714:43</TimeStamp>"
        public string ServiceStatus { get; set; }

        public string GatewayProductVersion { get; set; }        

    }
}
