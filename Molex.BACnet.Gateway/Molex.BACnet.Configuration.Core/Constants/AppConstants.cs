﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2019 All Rights Reserved
///   Company Name:       Molex
///   Class:              AppConstants
///   Description:        this model is responsible to manage Constant data 
///   Author:             Anish Nair                  
///   Date:               10/03/19
///---------------------------------------------------------------------------------------------
#endregion


namespace Molex.BACnet.Configuration.Core.Constants
{
    public class AppConstants
    {
        public const string CharacterEncodingTypes = "ANSI X3.4";
        public const string StatusDisable = "Disable";
        public const string StatusEnable = "Enable";
        public const string BACnetCSVFile = "Lighting Schedule Export-Molex.csv";
        public const string LogZipFileName = "Logs.zip";

        /// <summary>
        /// Thisa is used for NotifyType object Property
        /// </summary>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>08-06-201717:25</TimeStamp>"
        public enum NotifyType
        {
            Alarm = 0,
            Event = 1
            //ACKNotification
        }
        /// <summary>
        /// If service is not installed , this status would be displayed to BCT User
        /// </summary>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>08-06-201717:26</TimeStamp>"
        public enum ServiceStatus
        {
            NotAvailable
        }

        /// <summary>
        /// Get window service name 
        /// </summary>
        /// "<CreatedBy>anish.nair</CreatedBy><TimeStamp>07-06-201917:25</TimeStamp>"
        public const string BACnetGWServiceName = "GatewayApplicationService";

        /// <summary>
        /// The BACnet configuration file name
        /// </summary>
        public const string BACnetConfigurationFileName = "BACnetConfig.json";

        /// <summary>
        /// The BACnet Object Mapping file name
        /// </summary>
        public const string BACnetObjectMappingFileName = "BacnetObjectMapping.json";
        /// <summary>
        /// The BACnet Object Mapping file name
        /// </summary>
        public const string DefaultBACnetConfig = "Default_BACnetConfig.json";

        #region Error Constant

        /// <summary>
        /// The error unknown
        /// </summary>
        public const int ERROR_UNKNOWN = -1;

        /// <summary>
        /// The error category dataparsing
        /// </summary>
        public const int ERROR_CATEGORY_DATAPARSING = 1;

        /// <summary>
        /// The error category dataparsing empty json
        /// </summary>
        public const int ERROR_CATEGORY_DATAPARSING_EMPTY_JSON = 1;

        /// <summary>
        /// The error category dataparsing json format error
        /// </summary>
        public const int ERROR_CATEGORY_DATAPARSING_JSON_FORMAT_ERROR = 2;

        /// <summary>
        /// The error category modeldata
        /// </summary>
        public const int ERROR_CATEGORY_MODELDATA = 2;

        /// <summary>
        /// The error category modeldata data not found
        /// </summary>
        public const int ERROR_CATEGORY_MODELDATA_DATA_NOT_FOUND = 2;

        /// <summary>
        /// The error category server
        /// </summary>
        public const int ERROR_CATEGORY_SERVER = 3;

        /// <summary>
        /// The error category server unknown
        /// </summary>
        public const int ERROR_CATEGORY_SERVER_UNKNOWN = -1;

        /// <summary>
        /// The error category server unavaialble
        /// </summary>
        public const int ERROR_CATEGORY_SERVER_UNAVAIALBLE = 3;

        /// <summary>
        /// The error category server authetication failed
        /// </summary>
        public const int ERROR_CATEGORY_SERVER_AUTHETICATION_FAILED = 4;

        /// <summary>
        /// The error category io
        /// </summary>
        public const int ERROR_CATEGORY_IO = 4;

        /// <summary>
        /// The error category io file not found
        /// </summary>
        public const int ERROR_CATEGORY_IO_FILE_NOT_FOUND = 4;

        /// <summary>
        /// The error category modelparsingerror
        /// </summary>
        public const int ERROR_CATEGORY_MODELPARSINGERROR = 5;

        /// <summary>
        /// The error category modelparsingerror model data parsing error
        /// </summary>
        public const int ERROR_CATEGORY_MODELPARSINGERROR_MODEL_DATA_PARSING_ERROR = 5;

        public const int ERROR_CATEGORY_SERVICE = 5;
        public const int ERROR_CATEGORY_SERVICE_NOT_AVAILABLE = 4;        
        #endregion
    }
}
