﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2019 All Rights Reserved
///   Company Name:       Molex
///   Class:              IPAddressValidator
///   Description:        Attribute for IPAddress Validaton.
///   Author:             Anish Nair                  
///   Date:               10/03/19
///---------------------------------------------------------------------------------------------
#endregion

using System;
using System.ComponentModel.DataAnnotations;

namespace Molex.BACnet.Configuration.Core.Models
{
    /// <summary>
    /// Responsible for validating IP Address.
    /// </summary>
    public class IPAddressValidator : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null)
            {

                if (Validate(value))
                {
                    return ValidationResult.Success;
                }
                else
                {
                    return new ValidationResult("Error : Enter valid IP Address.");
                }
            }
            else
            {
                return ValidationResult.Success;
            }
        }
        /// <summary>
        /// Validate the IP Address.
        /// </summary>
        /// <param name="ipAddress">IPAddress to be validated</param>
        public bool Validate(object ipAddress)
        {
            IPSplit splitIp;

            bool result = TryParseSplitIP(ipAddress as string, out splitIp);

            if (result == true)
            {
                return ValidateIPAddress(splitIp);
            }
            else
                return false;
        }

        private bool TryParseSplitIP(string ipAddress, out IPSplit splitIp)
        {
            if (ipAddress == null)
            {
                splitIp = null;
                return false;
            }

            string[] ips = ipAddress.Split(new char[] { '.' }, StringSplitOptions.None);

            if (ips.Length == 4)
            {
                try
                {
                    splitIp = new IPSplit();
                    splitIp.IP1 = Convert.ToInt32(ips[0]);
                    splitIp.IP2 = Convert.ToInt32(ips[1]);
                    splitIp.IP3 = Convert.ToInt32(ips[2]);
                    splitIp.IP4 = Convert.ToInt32(ips[3]);
                    return true;
                }
                catch
                {
                    splitIp = null;
                    return false;
                }
            }
            else
            {
                splitIp = null;
                return false;
            }
        }

        private bool ValidateIPAddress(IPSplit splitIp)
        {
            bool returnValue = false;
            if (splitIp.IP1 >= 0 && splitIp.IP1 <= 255
                && splitIp.IP2 >= 0 && splitIp.IP2 <= 255
                && splitIp.IP3 >= 0 && splitIp.IP3 <= 255
                && splitIp.IP4 >= 0 && splitIp.IP4 <= 255)
            {
                returnValue = true;
                //if (splitIp.IP1 >= 1 && splitIp.IP1 <= 225 && splitIp.IP1 != 127)
                //{
                //    if (splitIp.IP2 == 0 && splitIp.IP3 == 0 && splitIp.IP4 == 0)
                //        return false;
                //    else if (splitIp.IP2 == 255 && splitIp.IP3 == 255 && splitIp.IP4 == 255)
                //        return false;
                //    else
                //        returnValue = true;
                //}
            }

            return returnValue;
        }


    }

    class IPSplit
    {
        public int IP1 { get; set; }
        public int IP2 { get; set; }
        public int IP3 { get; set; }
        public int IP4 { get; set; }
    }
}
