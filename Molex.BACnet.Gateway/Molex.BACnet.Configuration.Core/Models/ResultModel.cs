﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2019 All Rights Reserved
///   Company Name:       Molex
///   Class:              ResultModel
///   Description:        this model is responsible to manage result data 
///   Author:             Anish Nair                  
///   Date:               10/03/19
///---------------------------------------------------------------------------------------------
#endregion

namespace Molex.BACnet.Configuration.Core.Models
{
    public class ResultModel<T>
    {
        public ResponseErrorModel Error { get; set; }
        public bool IsSucceeded { get; set; }
        public T Result { get; set; }
    }
}