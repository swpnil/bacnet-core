﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2019 All Rights Reserved
///   Company Name:       Molex
///   Class:              FDTModel
///   Description:        Model for FDT 
///   Author:             Anish Nair                  
///   Date:               10/03/19
///---------------------------------------------------------------------------------------------
#endregion

namespace Molex.BACnet.Configuration.Core.Models
{
    public class FDTModel
    {
        public ushort TimeToLive { get; set; }
        public ushort TimeRemaining { get; set; }
        public string IPAddress { get; set; }
        public ushort Port { get; set; }
    }
}