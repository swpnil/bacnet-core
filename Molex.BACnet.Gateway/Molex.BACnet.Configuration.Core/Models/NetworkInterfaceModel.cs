﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2019 All Rights Reserved
///   Company Name:       Molex
///   Class:              NetworkInterfaceModel
///   Description:        this model is responsible to manage Network Interface 
///   Author:             Anish Nair                  
///   Date:               10/03/19
///---------------------------------------------------------------------------------------------
#endregion


using Molex.BACnet.Configuration.Core.Helper;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Molex.BACnet.Configuration.Core.Models
{
    public class NetworkInterfaceModel
    {

        public List<IPAddressModel> IPAddressList { get; set; }

        [Required(ErrorMessage = "Error : Enter valid UDP Port Number.")]
        [Display(Name = "UDP Port Number")]
        [Range(0, 65535,
        ErrorMessage = "Error  : Value must be between 0 to 65535.")]
        public string UDPPort { get; set; }

        [Required(ErrorMessage = "Error : Enter valid Virtual Network Number.")]
        [Display(Name = "Virtual Network Number")]
        [Range(1, 65534,
        ErrorMessage = "Error  : Value must be between 1 to 65534.")]
        public string VirtualNetworkNumber { get; set; }

        [Required(ErrorMessage = "Error : Enter valid Local Network Number.")]
        [Display(Name = "Local Network Number")]
        [Range(1, 65534,
        ErrorMessage = "Error  : Value must be between 1 to 65534.")]
        [NotEqualTo("VirtualNetworkNumber", ErrorMessage = "Error : Virtual Network Number and Local Network Number cannot be same. ")]
        public string LocalNetworkNumber { get; set; }

        [Required(ErrorMessage = "Error : Enter valid IP Address.")]
        [Display(Name = "IP Address")]
        public string SelectedIPAddress { get; set; }
    }

    public class IPAddressModel
    {
        public string IPAddress { get; set; }
        public string NetworkInterfaceType { get; set; }
        public string NetworkInterfaceName { get; set; }
        public string OperationalStatus { get; set; }
        public bool IsSelected { get; set; }
    }
}