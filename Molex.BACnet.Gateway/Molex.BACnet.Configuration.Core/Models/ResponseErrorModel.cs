﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2019 All Rights Reserved
///   Company Name:       Molex
///   Class:              NetworkInterfaceModel
///   Description:        this model is responsible to manage Error Handling 
///   Author:             Anish Nair                  
///   Date:               10/03/19
///---------------------------------------------------------------------------------------------
#endregion

namespace Molex.BACnet.Configuration.Core.Models
{
    public class ResponseErrorModel
    {
        public int ErrorCategory { get; set; }
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }

        public override string ToString()
        {
            return string.Format("ErrorCode: {0}, Category: {1}, {2}", 
                                this.ErrorCode, this.ErrorCategory, this.ErrorMessage);
        }

    }
}