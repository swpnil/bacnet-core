﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2019 All Rights Reserved
///   Company Name:       Molex
///   Class:              ObjectAlarmSetting
///   Description:        Model for ObjectAlarmSetting 
///   Author:             Anish Nair                  
///   Date:               10/03/19
///---------------------------------------------------------------------------------------------
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Molex.BACnet.Configuration.Core.Models
{
    public class ObjectAlarmSetting
    {
        public string Name { get; set; }
        public bool ZoneLevel { get; set; }
        public bool Individual { get; set; }
        public bool ZoneValue { get; set; }
        public bool IndividualValue { get; set; }
    }
}