﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2019 All Rights Reserved
///   Company Name:       Molex
///   Class:              BDTModel
///   Description:        Model for BDT 
///   Author:             Anish Nair                  
///   Date:               10/03/19
///---------------------------------------------------------------------------------------------
#endregion

using Molex.BACnet.Configuration.Core.Helper;
using System.ComponentModel.DataAnnotations;

namespace Molex.BACnet.Configuration.Core.Models
{
    public class BDTModel
    {
        [Required(ErrorMessage = "IPAddrees must required")]
        [IPAddressValidator]
        [Display(Name = "IP Address")]
        [RegularExpression(@"^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$", ErrorMessage = "Error : Enter valid IP Address.")]
        public string IPAddress { get; set; }

        [Required(ErrorMessage = "Port number must required")]
        [Display(Name = "UDP Port Number")]
        [Range(0, 65535,
        ErrorMessage = "Error  : Port Value must be between 0 to 65535.")]
        public ushort Port { get; set; }

        [Required(ErrorMessage = "Mask number must required")]
        public byte BroadcastMask { get; set; }

        public bool IsSelfEntry { get; set; }
    }
}