﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2019 All Rights Reserved
///   Company Name:       Molex
///   Class:              BACnetConfigurationModel
///   Description:        this model is responsible to manage data for BACnet Configuration  
///   Author:             Anish Nair                  
///   Date:               10/03/19
///---------------------------------------------------------------------------------------------
#endregion

using Molex.BACnet.Configuration.Core.Constants;
using Molex.BACnet.Gateway.ServiceCore.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Molex.BACnet.Configuration.Core.Models
{
    public class BACnetConfigurationModel
    {
        [Required]
        public uint ControllerId { get; set; }
        ////REQ_BCT_15 - Exposing endpoints to BMS 
        public bool MapIndividualSensors { get; set; }
        public bool MapIndividualFixture { get; set; }
        [Required]
        public string MolexAPIUrl { get; set; }

        [Required]
        public string MolexProjectID { get; set; }

        public string ProjectName { get; set; }

        public bool IsMQTTPublishSelected { get; set; }
        //REQ_BCT_23:Device object properties settings

        #region BACnet device object properties

        [Required]
        public string VendorName { get; set; }

        [Required]
        public string ModelName { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public bool IsTempRequiredInFahrenheit { get; set; }

        [Required]
        [Range(6000, 65000,
        ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public uint APDUTimeout { get; set; }
        [Required]
        [Range(0, 255,
        ErrorMessage = "Value for {0} must be between {1} and {2}.")]

        public uint APDURetries { get; set; }

        [Required]
        [Range(5000, 65000,
        ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public uint APDUSegmentTimeout { get; set; }

        [Required]
        [Range(-780, 780,
        ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int UTCOffset { get; set; }

        [Required]
        public bool DaylightSavingStatus { get; set; }

        #endregion


        //public string Interface { get; set; }//eth0
        [Required]
        public string IPAddress { get; set; }

        [Required]
        [Range(0, 65535,
        ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int UDPPort { get; set; }

        [Required]
        public int LocalNetworkNumber { get; set; }

        [Required]
        public int VirtualNetworkNumber { get; set; }

        //REQ_BCT_24:BBMD/Non-BBMD support
        [Required]
        public bool IsBBMD { get; set; }

        //REQ_BCT_25: Foreign device registration
        [Required]
        public string FDIPAddress { get; set; }

        [Required]
        [Range(0, 65535,
        ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public string FDUDPPort { get; set; }
        [Required]
        public string FDTimeToLive { get; set; }
        [Required]
        public bool FDReRegister { get; set; }

        //REQ_BCT_26:Enable or disable Write access from BMS 
        [Required]
        public bool WriteAccess { get; set; }

        //REQ_BCT_27:Enable or disable Time Synchronization access from BMS 
        [Required]
        public bool TimeSynchronization { get; set; }

        //REQ_BCT_28:Enable or disable Scheduling
        [Required]
        public bool Scheduling { get; set; }

        //REQ_BCT_29: Alarm and event properties settings
        [Required]
        [EnumDataType(typeof(AppConstants.NotifyType))]
        public uint NotifyType { get; set; }

        [Required]
        [Range(0, 4294967295,
        ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public uint TimeDelay { get; set; }

        //REQ_BCT_30:BACnet password setting
        [Required]
        public string BACnetPassword { get; set; }

        //Design-Molex-BACnet-Gateway-1.0.docx : 3.6.5.2	Event Reporting
        [Required]
        public bool IsBuildingAlarm { get; set; }
        [Required]
        public bool IsFloorAlarm { get; set; }

        [Required]
        public Dictionary<string, List<ObjectAlarmSetting>> AlarmNotificationSetting { get; set; }

        [Required]
        public int MonitoringDefaultPriority { get; set; }

        [Required]
        public bool IsDebugLogEnable { get; set; }

        public bool IsBACnetPasswordRequired { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public List<BDTModel> BDTList { get; set; }

        public List<FDTModel> FDTList { get; set; }

        /// <summary>
        /// This Object will persists the Notification Priority for Buildign Level
        /// </summary>
        public NotificationPriority BuildingNotificationPriority { get; set; }

        /// <summary>
        /// This Object will persists the Notification Priority for Zone Level
        /// </summary>
        public NotificationPriority ZoneNotificationPriority { get; set; }
    }
}