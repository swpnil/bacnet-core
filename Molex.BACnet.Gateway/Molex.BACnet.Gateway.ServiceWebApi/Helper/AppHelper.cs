﻿using Molex.BACnet.Configuration.Core.Constants;
using Molex.BACnet.Configuration.Core.Manager;
using Molex.BACnet.Configuration.Core.Models;
using Molex.BACnet.Configuration.Core.Repository;
using Molex.BACnet.Gateway.Encryption;
using Molex.BACnet.Gateway.Log;
using Molex.BACnet.Gateway.ServiceWebApi.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
//using System.Management;
using System.Text;
using System.Threading.Tasks;
using BACnetConfigurationModel = Molex.BACnet.Configuration.Core.Models.BACnetConfigurationModel;

namespace Molex.BACnet.Gateway.ServiceWebApi.Helper
{
    public static class AppHelper
    {
        public static string WebApiIpAddress;
        public static int WebApiPortNo;

        public static void Init()
        {
            Logger.Instance.Log(LogLevel.DEBUG, "WebApi: AppHelper.Init() => ENTER");
            
            AppHelper.WebApiIpAddress = ConfigurationManager.AppSettings["WebApiIpAddress"].Trim();
            AppHelper.WebApiPortNo = Convert.ToInt32(ConfigurationManager.AppSettings["WebApiPortNo"]);

            ReadApplicationConfigurationFile();
            ValidateApplicationSettings();
            //Molex.BACnet.Configuration.Core.Helper.AppHelper.Init();
            ResultModel<BACnetConfigurationModel> resultData = Molex.BACnet.Configuration.Core.Manager.ConfigurationSettingManager.Instance.ReadBACnetConfigurationSetting(Path.Combine(AppWebRepository.Instance.ConfiurationFolder, AppConstants.BACnetConfigurationFileName));
            if (resultData.IsSucceeded)
                Molex.BACnet.Configuration.Core.Repository.AppWebRepository.Instance.BACnetConfigurationData = resultData.Result;

            Logger.Instance.Log(LogLevel.DEBUG, "WebApi: AppHelper.Init() <= EXIT");
        }

        /// <summary>
        /// Reads the application configuration file.
        /// </summary>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>08-06-201718:45</TimeStamp>"
        private static void ReadApplicationConfigurationFile()
        {
            //string ExecutablePath = GetExecutablePath();
            string ApplicationDir = AppDomain.CurrentDomain.BaseDirectory;

            AppWebRepository.Instance.ConfiurationFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["ConfigurationFolderName"]);
            Logger.Instance.Log(LogLevel.DEBUG, string.Format("ReadApplicationConfigurationFile => ConfiurationFolder: {0}. ", AppWebRepository.Instance.ConfiurationFolder));

            //AppWebRepository.Instance.BCTManualFolder = Path.Combine(AppWebRepository.Instance.ConfiurationFolder, ConfigurationManager.AppSettings["BCTManualFolderName"]);
            //Logger.Instance.Log(LogLevel.DEBUG, string.Format("ReadApplicationConfigurationFile => BCTManualFolder: {0}. ", AppWebRepository.Instance.BCTManualFolder));

            //Molex.BACnet.Configuration.Core.Repository.AppWebRepository.Instance.LoggerFolderName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), ConfigurationManager.AppSettings["LoggerFolderName"]);
            //Molex.BACnet.Configuration.Core.Repository.AppWebRepository.Instance.IsSyslogFormatDisabled = Convert.ToBoolean(ConfigurationManager.AppSettings["IsSyslogFormatDisabled"]);
            //Molex.BACnet.Configuration.Core.Repository.AppWebRepository.Instance.IsLoggerDisabled = Convert.ToBoolean(ConfigurationManager.AppSettings["IsLoggerDisabled"]);

            System.Reflection.Assembly caller = System.Reflection.Assembly.GetEntryAssembly();
            var productVersion = new System.Reflection.AssemblyName(caller.FullName).Version;
            AppWebRepository.Instance.GatewayProductVersion = productVersion.ToString();
            Logger.Instance.Log(LogLevel.DEBUG, string.Format("ReadApplicationConfigurationFile => GatewayProductVersion: {0}. ", AppWebRepository.Instance.GatewayProductVersion));

            //AppWebRepository.Instance.UserID = CryptoHelper.Decrypt3DES(ConfigurationManager.AppSettings["UserID"].ToString());
            //AppWebRepository.Instance.Password = CryptoHelper.Decrypt3DES(ConfigurationManager.AppSettings["Password"].ToString());


            Logger.Instance.Log(LogLevel.DEBUG, "ReadApplicationConfigurationFile: Executed. ");
        }

        /// <summary>
        /// Gets the executable path for running windows service 
        /// 
        /// </summary>
        /// <returns></returns>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>28-06-201712:33</TimeStamp>"
        //private static string GetExecutablePath()
        //{
        //    string ServiceName = AppConstants.BACnetGWServiceName;
        //    using (ManagementObject wmiService = new ManagementObject("Win32_Service.Name='" + ServiceName + "'"))
        //    {
        //        try
        //        {
        //            wmiService.Get();
        //            string currentserviceExePath = wmiService["PathName"].ToString();
        //            string executablePath = wmiService["PathName"].ToString();
        //            executablePath = Path.GetDirectoryName(executablePath.Replace('"', ' '));
        //            Logger.Instance.Log(LogLevel.DEBUG, "GetExecutablePath:  " + executablePath);
        //            return executablePath;
        //        }
        //        catch (Exception ex)
        //        {
        //            Logger.Instance.Log(ex, LogLevel.ERROR, "GetExecutablePath:  " + ex.Message);
        //            return string.Empty;
        //        }
        //    }
        //}

        private static bool ValidateApplicationSettings()
        {
            try
            {
                if (string.IsNullOrEmpty(AppWebRepository.Instance.ConfiurationFolder))
                {
                    Logger.Instance.Log(LogLevel.DEBUG, "Configuration Folder does not exist. ");
                    return false;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(LogLevel.ERROR, "validate application settings failed" + ex);
                return false;
            }

            return true;
        }

        #region AlarmEventSetting
        public static AlarmEventSettingViewModel GetAlarmEventSetting(BACnetConfigurationModel bACnetConfigurationData)
        {
            UInt32 key = 0;

            AlarmEventSettingViewModel model = new AlarmEventSettingViewModel();

            if (bACnetConfigurationData.AlarmNotificationSetting == null)
            {
                List<string> zoneTypes = ConfigurationManager.AppSettings["ZoneType"].Split(',').ToList();
                ObjectAlarmSetting zoneObject = null;
                string level = string.Empty;
                foreach (string zone in zoneTypes)
                {
                    level = zone + " Level";
                    model.ZoneBasedObjects.Add(zone, new List<ObjectAlarmSetting>());
                    if (ConfigurationManager.AppSettings.AllKeys.Contains(level) && !string.IsNullOrEmpty(ConfigurationManager.AppSettings[level]))
                    {
                        List<string> ZoneLevelObjects = ConfigurationManager.AppSettings[level].Split(',').ToList();
                        foreach (string name in ZoneLevelObjects)
                        {
                            zoneObject = new ObjectAlarmSetting();
                            zoneObject.Name = name;
                            zoneObject.ZoneLevel = true;
                            zoneObject.Individual = false;
                            model.ZoneBasedObjects[zone].Add(zoneObject);
                        }
                    }

                    level = zone + " Endpoint";
                    if (ConfigurationManager.AppSettings.AllKeys.Contains(level) && !string.IsNullOrEmpty(ConfigurationManager.AppSettings[level]))
                    {
                        List<string> EndpointObjects = ConfigurationManager.AppSettings[level].Split(',').ToList();
                        foreach (string name in EndpointObjects)
                        {
                            zoneObject = new ObjectAlarmSetting();
                            zoneObject.Name = name;
                            zoneObject.ZoneLevel = false;
                            zoneObject.Individual = true;
                            model.ZoneBasedObjects[zone].Add(zoneObject);
                        }
                    }

                    level = zone + " Common";
                    if (ConfigurationManager.AppSettings.AllKeys.Contains(level) && !string.IsNullOrEmpty(ConfigurationManager.AppSettings[level]))
                    {
                        List<string> EndpointObjects = ConfigurationManager.AppSettings[level].Split(',').ToList();
                        foreach (string name in EndpointObjects)
                        {
                            zoneObject = new ObjectAlarmSetting();
                            zoneObject.Name = name;
                            zoneObject.ZoneLevel = true;
                            zoneObject.Individual = true;
                            model.ZoneBasedObjects[zone].Add(zoneObject);
                        }
                    }

                }
            }
            else
            {
                foreach (string objectType in bACnetConfigurationData.AlarmNotificationSetting.Keys)
                {
                    model.ZoneBasedObjects.Add(objectType, new List<ObjectAlarmSetting>());

                    foreach (ObjectAlarmSetting objectAlarmSetting in bACnetConfigurationData.AlarmNotificationSetting[objectType])
                    {
                        model.ZoneBasedObjects[objectType].Add(objectAlarmSetting);
                    }
                }
            }

            key = bACnetConfigurationData.NotifyType;
            //model.SelectedNotifyType = bACnetConfigurationData.NotifyType;
            //model.SelectedTimeDelay = bACnetConfigurationData.TimeDelay.ToString();
            //model.CurrentNotifyType = Enum.GetName(typeof(AppConstants.NotifyType), key);
            //model.CurrentTimeDelay = bACnetConfigurationData.TimeDelay;
            model.NotifyTypeList = GetNotifyType();

            //model.IsBuildingAlarm = bACnetConfigurationData.IsBuildingAlarm;
            //model.IsFloorAlarm = bACnetConfigurationData.IsFloorAlarm;

            //if (model.IsBuildingAlarm)
            //{
            //    model.CurrentBuildingAlarm = AppConstants.StatusEnable;
            //}
            //else
            //{
            //    model.CurrentBuildingAlarm = AppConstants.StatusDisable;
            //}

            //if (model.IsFloorAlarm)
            //{
            //    model.CurrentFloorAlarm = AppConstants.StatusEnable;
            //}
            //else
            //{
            //    model.CurrentFloorAlarm = AppConstants.StatusDisable;
            //}

            return model;
        }

        public static void SetAlarmEventSetting(AlarmEventSettingViewModel model, BACnetConfigurationModel bACnetConfigurationData)
        {
            //bACnetConfigurationData.IsBuildingAlarm = model.IsBuildingAlarm;
            //bACnetConfigurationData.IsFloorAlarm = model.IsFloorAlarm;
            bACnetConfigurationData.AlarmNotificationSetting = new Dictionary<string, List<ObjectAlarmSetting>>();

            foreach (string zoneType in model.ZoneBasedObjects.Keys)
            {
                bACnetConfigurationData.AlarmNotificationSetting.Add(zoneType, new List<ObjectAlarmSetting>());

                foreach (ObjectAlarmSetting objectAlarmSetting in model.ZoneBasedObjects[zoneType])
                {
                    bACnetConfigurationData.AlarmNotificationSetting[zoneType].Add(objectAlarmSetting);
                }
            }

            //model.CurrentNotifyType = Enum.GetName(typeof(AppConstants.NotifyType), model.SelectedNotifyType);
            //model.CurrentTimeDelay = Convert.ToUInt32(model.SelectedTimeDelay);
            //bACnetConfigurationData.TimeDelay = Convert.ToUInt32(model.SelectedTimeDelay);
            //bACnetConfigurationData.NotifyType = model.SelectedNotifyType;
        }


        #region Private Method
        static List<KeyValuePair<string, int>> GetNotifyType()
        {
            var enumData = from AppConstants.NotifyType e in Enum.GetValues(typeof(AppConstants.NotifyType))
                           select new KeyValuePair<string, int>(e.ToString(), (int)e);

            return enumData.ToList();
        }

        #endregion Private Method
        #endregion AlarmEventSetting
    }
}
