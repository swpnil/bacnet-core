﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceWebApi.Models
{
    public class GwApiDataModel
    {
        public bool Status { get; set; }
        public object JsonData { get; set; }
        public string Error { get; set; }
    }
}
