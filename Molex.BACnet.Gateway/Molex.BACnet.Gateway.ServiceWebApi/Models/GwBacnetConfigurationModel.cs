﻿using Molex.BACnet.Configuration.Core.Models;
using Molex.BACnet.Gateway.ServiceCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceWebApi.Models
{
    public class GwBacnetConfigurationModel
    {
        public GwBacnetConfigurationModel()
        {
            this.NetworkInterface = new NetworkInterfaceSettings();
            this.BmsControl = new BmsControlSettings();
            this.Broadcast = new BroadcastSettings();
            this.Communication = new CommunicationSettings();
            this.DeviceObject = new DeviceObjectSettings();
            this.Gateway = new GatewaySettings();
            this.NetworkInterface = new NetworkInterfaceSettings();
            this.Notification = new NotificationSettings();
            this.Security = new SecuritySettings();
        }

        public bool IsDebugLogEnable { get; set; }
        public NetworkInterfaceSettings NetworkInterface { get; set; }
        public GatewaySettings Gateway { get; set; }
        public DeviceObjectSettings DeviceObject { get; set; }
        public NotificationSettings Notification { get; set; }
        public BroadcastSettings Broadcast { get; set; }
        public BmsControlSettings BmsControl { get; set; }
        public CommunicationSettings Communication { get; set; }
        public SecuritySettings Security { get; set; }
    }

    public class NetworkInterfaceSettings
    {
        public NetworkInterfaceSettings()
        {
            this.IPAddressList = new List<IPAddressModel>();
        }

        public List<IPAddressModel> IPAddressList { get; set; }
        public string IPAddress { get; set; }
        public int UDPPort { get; set; }
        public int VirtualNetworkNumber { get; set; }
        public int LocalNetworkNumber { get; set; }
    }

    public class GatewaySettings
    {
        public uint DeviceId { get; set; }
        public string MolexApiUrl { get; set; }
        public string MolexProjectId { get; set; }
        public string MolexProjectName { get; set; }
        public int MonitoringDefaultPriority { get; set; }
    }

    public class DeviceObjectSettings
    {
        public string VendorName { get; set; }
        public string ModelName { get; set; }
        public string Description { get; set; }
        public uint APDUTimeout { get; set; }
        public uint APDURetries { get; set; }
        public uint APDUSegmentTimeout { get; set; }
        public int UTCOffset { get; set; }
        public bool DaylightSavingStatus { get; set; }
    }

    public class NotificationSettings
    {
        public NotificationSettings()
        {
            this.AlarmEventVM = new AlarmEventSettingViewModel();
            //this.AlarmNotificationSetting = new Dictionary<string, List<ObjectAlarmSetting>>();
        }

        public uint TimeDelay { get; set; }
        public uint NotifyType { get; set; }
        public bool IsBuildingAlarm { get; set; }
        public bool IsFloorAlarm { get; set; }
        public bool IsZoneAlarm { get; set; }
        public AlarmEventSettingViewModel AlarmEventVM { get; set; }
        //public Dictionary<string, List<ObjectAlarmSetting>> AlarmNotificationSetting { get; set; }
        public NotificationPriority BuildingNotificationPriority { get; set; }
        public NotificationPriority ZoneNotificationPriority { get; set; }
    }

    public class BroadcastSettings
    {
        public BroadcastSettings()
        {
            this.BDTList = new List<BDTModel>();
            this.FDTList = new List<FDTModel>();
        }

        public bool IsBBMD { get; set; }
        public string FDIPAddress { get; set; }
        public string FDUDPPort { get; set; }
        public string FDTimeToLive { get; set; }
        public bool FDReRegister { get; set; }
        public List<BDTModel> BDTList { get; set; }
        public List<FDTModel> FDTList { get; set; }
    }
    public class BmsControlSettings
    {
        public bool IsBACnetPasswordRequired { get; set; }
        public string BACnetPassword { get; set; }
        public bool WriteAccess { get; set; }
        public bool TimeSynchronization { get; set; }
        public bool Scheduling { get; set; }
        public bool MapIndividualSensors { get; set; }
        public bool MapIndividualFixture { get; set; }
        public string CharacterEncodingTypes { get; set; }
        public bool IsTempRequiredInFahrenheit { get; set; }
    }

    public class CommunicationSettings
    {
        public CommunicationSettings()
        {
            this.AvailableModules = new List<string>();
            this.AvailableModules.Add("HTTP");
            this.AvailableModules.Add("MQTT");
        }

        public List<string> AvailableModules { get; set; }
        public string CurrentModule { get; set; }
    }

    public class SecuritySettings
    {
        public string APIuserID { get; set; }
        public string APIpassword { get; set; }
    }

    public class AlarmEventSettingViewModel
    {
        public AlarmEventSettingViewModel()
        {
            this.ZoneBasedObjects = new Dictionary<string, List<ObjectAlarmSetting>>();
            this.NotifyTypeList = new List<KeyValuePair<string, int>>();
        }

        public List<KeyValuePair<string, int>> NotifyTypeList { get; set; }


        public Dictionary<string, List<ObjectAlarmSetting>> ZoneBasedObjects { get; set; }

    }
}
