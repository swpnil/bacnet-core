﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceWebApi.Models
{
    class GwServiceStatusModel
    {
        public string State { get; set; }
        public string Version { get; set; }
    }
}
