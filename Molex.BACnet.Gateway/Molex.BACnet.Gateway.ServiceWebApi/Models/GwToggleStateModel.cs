﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceWebApi.Models
{
    public class GwToggleStateModel
    {
        public bool State { get; set; }
    }
}
