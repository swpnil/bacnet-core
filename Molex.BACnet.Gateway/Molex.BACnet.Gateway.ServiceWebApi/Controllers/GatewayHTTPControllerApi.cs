﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              GatewayHTTPControllerApi
///   Description:        It is Responsible To Manage Netwrok Interface.
///   Author:             Nazneen Zahid                  
///   Date:               06/06/17
///---------------------------------------------------------------------------------------------
#endregion

using Molex.BACnet.Configuration;
using Molex.BACnet.Configuration.Core.Constants;
using Molex.BACnet.Configuration.Core.Manager;
using Molex.BACnet.Configuration.Core.Models;
using Molex.BACnet.Configuration.Core.Repository;
using Molex.BACnet.Gateway.Log;
using Molex.BACnet.Gateway.ServiceCore.Repository;
using Molex.BACnet.Gateway.ServiceWebApi.Helper;
using Molex.BACnet.Gateway.ServiceWebApi.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Schema;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BACnetConfigurationModel = Molex.BACnet.Configuration.Core.Models.BACnetConfigurationModel;

namespace Molex.BACnet.Gateway.ServiceWebApi.Controllers
{
    public class GatewayHTTPControllerApi
    {
        /// <summary>
        /// Handler Thread will be the count of handlers which are getting created for every HTTP request
        /// </summary>
        private static int HandlerThread = 0;
        public string Map;

        /// <summary>
        /// listener for listening to HTTP requests
        /// </summary>
        private HttpListener listener;

        /// <summary>
        /// Start method for starting the listener & adding the handlers to HTTP request
        /// </summary>
        public void Start(string IPaddress, int portNo)
        {
            try
            {
                Logger.Instance.Log(LogLevel.DEBUG,
                   string.Format("GatewayHTTPControllerApi: rcvd web-api params... {0}:{1} ", IPaddress, portNo));

                HandlerThread = 0;
                //Total Devices will include all the sensors , fixtures , blinds & beacons in the given building 
                //7 leads to subscription types
                //2 is just multiplying factor
                if (HandlerThread < 400)
                    HandlerThread = 400;//if still the value is less than Web API default MaxConCurrentThread.
                listener = new HttpListener();
                listener.Prefixes.Add(string.Format("http://{0}:{1}/Test", IPaddress, portNo) + "/");
                listener.Prefixes.Add(string.Format("http://{0}:{1}/RestoreDefaultConfigurations", IPaddress, portNo) + "/");
                listener.Prefixes.Add(string.Format("http://{0}:{1}/SetDefaultConfigurations", IPaddress, portNo) + "/");
                listener.Prefixes.Add(string.Format("http://{0}:{1}/DownloadObjectMapping", IPaddress, portNo) + "/");
                listener.Prefixes.Add(string.Format("http://{0}:{1}/GetGWServiceStatus", IPaddress, portNo) + "/");
                listener.Prefixes.Add(string.Format("http://{0}:{1}/ToggleGWServiceStatus", IPaddress, portNo) + "/");
                listener.Prefixes.Add(string.Format("http://{0}:{1}/GetBacNetConfiguration", IPaddress, portNo) + "/");
                listener.Prefixes.Add(string.Format("http://{0}:{1}/SaveBacNetConfiguration", IPaddress, portNo) + "/");
                listener.Prefixes.Add(string.Format("http://{0}:{1}/DownloadLogs", IPaddress, portNo) + "/");

                if (listener.IsListening)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "GatewayHTTPControllerApi: Start Exception Received HttpListener can not subscribe");
                    return;
                }

                listener.Start();
                Logger.Instance.Log(LogLevel.DEBUG,
                    string.Format("GatewayHTTPControllerApi: Started listening on... {0}:{1} ", IPaddress, portNo));

                Thread t1 = new Thread(GetData);
                t1.Start();
                //GetData();

            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "GatewayHTTPControllerApi: Start Exception Received");
            }
        }

        private void GetData()
        {
            while (true)
            {
                HttpListenerContext ctx = listener.GetContext();




                ThreadPool.QueueUserWorkItem((_) =>
                {
                    string methodName = string.Empty;
                    HttpListenerRequest httpRequest = ctx.Request;
                    HttpListenerResponse httpResponse = ctx.Response;
                    try
                    {
                        string jsonData = string.Empty;

                        using (Stream networkStream = httpRequest.InputStream)
                        {
                            jsonData = new StreamReader(networkStream).ReadToEnd();
                        }

                        methodName = ctx.Request.Url.Segments[1].Replace("/", "");
                        Logger.Instance.Log(LogLevel.DEBUG,
                                    string.Format("GatewayHTTPControllerApi: GetData for method: {0} ", methodName));

                        string[] strParams = ctx.Request.Url
                                                .Segments
                                                .Skip(2)
                                                .Select(s => s.Replace("/", ""))
                                                .ToArray();

                        if (!this.GetType().GetMethods().Any(mi => mi.GetCustomAttributes(true).Any(attr => ((Mapping)attr).Map == methodName)))
                        {
                            using (Stream stOutput = httpResponse.OutputStream)
                            {
                                // Construct a response.
                                string outputReplyData = string.Empty;
                                httpResponse.StatusCode = 200;
                                GwApiDataModel _GwApiDataModel = new GwApiDataModel();
                                _GwApiDataModel.Status = false;
                                _GwApiDataModel.Error = "API details for '" + methodName + "' not found";
                                _GwApiDataModel.JsonData = "";
                                outputReplyData = Newtonsoft.Json.JsonConvert.SerializeObject(_GwApiDataModel);
                                byte[] bufferreplyData = System.Text.Encoding.UTF8.GetBytes(outputReplyData);
                                // Get a response stream and write the response to it.
                                httpResponse.ContentLength64 = bufferreplyData.Length;
                                stOutput.Write(bufferreplyData, 0, bufferreplyData.Length);
                                bufferreplyData = null;
                            }
                            return;
                        }
                        var method = this.GetType().GetMethods().Where(mi => mi.GetCustomAttributes(true).Any(attr => ((Mapping)attr).Map == methodName)).First();

                        object[] @params = new object[1];

                        if (httpRequest.HttpMethod == "POST")
                        {
                            @params[0] = jsonData;
                        }
                        else
                        {
                            @params = method.GetParameters()
                                                    .Select((p, i) => Convert.ChangeType(strParams[i], p.ParameterType))
                                                    .ToArray();
                        }


                        object ret = method.Invoke(this, @params);
                        string outputData = string.Empty;
                        byte[] buffer;

                        #region Send the response
                        // Construct a response.
                        httpResponse.StatusCode = 200;
                        httpResponse.AppendHeader("Date", DateTime.Now.ToString());
                        httpResponse.AppendHeader("Connection", "close");

                        if (method.Name == "DownloadObjectMapping")
                        {
                            httpResponse.ContentType = "Application/csv";
                            httpResponse.AppendHeader("Content-Disposition", "attachment; filename=" + AppConstants.BACnetCSVFile);

                            outputData = (ret as GwApiDataModel).JsonData.ToString();
                            buffer = System.Text.Encoding.ASCII.GetBytes(outputData);
                        }
                        else if (method.Name == "DownloadLogs")
                        {
                            httpResponse.ContentType = "Application/zip";
                            httpResponse.AppendHeader("Content-Disposition", "attachment; filename=" + AppConstants.LogZipFileName);

                            outputData = (ret as GwApiDataModel).JsonData.ToString();
                            buffer = File.ReadAllBytes(outputData);
                        }
                        else
                        {
                            outputData = JsonConvert.SerializeObject(ret);
                            buffer = System.Text.Encoding.ASCII.GetBytes(outputData);
                        }

                        // Get a response stream and write the response to it.
                        httpResponse.ContentLength64 = buffer.Length;
                        System.IO.Stream output = httpResponse.OutputStream;
                        output.Write(buffer, 0, buffer.Length);
                        output.Close(); // Close the output stream. 

                        #endregion
                    }
                    catch (Exception err)
                    {
                        using (Stream stOutput = httpResponse.OutputStream)
                        {
                            // Construct a response.
                            string outputReplyData = string.Empty;
                            httpResponse.StatusCode = 200;
                            GwApiDataModel _GwApiDataModel = new GwApiDataModel();
                            _GwApiDataModel.Status = false;
                            _GwApiDataModel.Error = "API details for '" + methodName + "' not found" + " " + "Exception Details :" + err.Message.ToString();
                            _GwApiDataModel.JsonData = "";
                            outputReplyData = Newtonsoft.Json.JsonConvert.SerializeObject(_GwApiDataModel);
                            byte[] bufferreplyData = System.Text.Encoding.UTF8.GetBytes(outputReplyData);
                            // Get a response stream and write the response to it.
                            httpResponse.ContentLength64 = bufferreplyData.Length;
                            stOutput.Write(bufferreplyData, 0, bufferreplyData.Length);
                            bufferreplyData = null;
                        }
                    }
                });
            }
        }

        [Mapping("Test")]
        public GwApiDataModel Test()
        {
            GwApiDataModel result = new GwApiDataModel();

            result.Status = true;
            result.JsonData = "WebApi is running";

            return result;
        }

        [Mapping("SetDefaultConfigurations")]
        public GwApiDataModel SetDefaultConfigurations()
        {
            GwApiDataModel result = new GwApiDataModel();

            BACnetConfigurationModel defaultModel = new BACnetConfigurationModel();
            
            ResultModel<BACnetConfigurationModel> resultData = ConfigurationSettingManager.Instance.ReadBACnetConfigurationSetting(Path.Combine(AppWebRepository.Instance.ConfiurationFolder, AppConstants.BACnetConfigurationFileName));
            if (resultData.IsSucceeded)
            {
                //BACnetConfigurationModel model = resultData.Result;

                //defaultModel.UDPPort = model.UDPPort;
                //defaultModel.LocalNetworkNumber = model.LocalNetworkNumber;
                //defaultModel.VirtualNetworkNumber = model.VirtualNetworkNumber;
                //defaultModel.AlarmNotificationSetting = model.AlarmNotificationSetting;
                //defaultModel.VendorName = model.VendorName;
                //defaultModel.ProjectName = model.ProjectName;
                //defaultModel.Description = model.Description;
                //defaultModel.ModelName = model.ModelName;
                //defaultModel.APDURetries = model.APDURetries;
                //defaultModel.APDUTimeout = model.APDUTimeout;
                //defaultModel.APDUSegmentTimeout = model.APDUSegmentTimeout;
                //defaultModel.UTCOffset = model.UTCOffset;
                //defaultModel.DaylightSavingStatus = model.DaylightSavingStatus;
                //defaultModel.UserName = model.UserName;
                //defaultModel.Password = model.Password;
                //defaultModel.ControllerId = model.ControllerId;
                //defaultModel.MolexAPIUrl = model.MolexAPIUrl;
                //defaultModel.MolexProjectID = model.MolexProjectID;
                //defaultModel.MonitoringDefaultPriority = model.MonitoringDefaultPriority;
                //defaultModel.IsBuildingAlarm = model.IsBuildingAlarm;
                //defaultModel.IsFloorAlarm = model.IsFloorAlarm;
                //defaultModel.IPAddress = model.IPAddress;
                //defaultModel.TimeDelay = model.TimeDelay;
                //defaultModel.NotifyType = model.NotifyType;
                //defaultModel.BDTList = model.BDTList;
                //defaultModel.FDTList = model.FDTList;
                //defaultModel.BuildingNotificationPriority = model.BuildingNotificationPriority;
                //defaultModel.ZoneNotificationPriority = model.ZoneNotificationPriority;

                //defaultModel.BACnetPassword = model.BACnetPassword;
                //defaultModel.Scheduling = model.Scheduling;
                //defaultModel.TimeSynchronization = model.TimeSynchronization;
                //defaultModel.WriteAccess = model.WriteAccess;
                //defaultModel.MapIndividualFixture = model.MapIndividualFixture;
                //defaultModel.MapIndividualSensors = model.MapIndividualSensors;
                //defaultModel.IsBACnetPasswordRequired = model.IsBACnetPasswordRequired;

                //defaultModel.IsBBMD = model.IsBBMD;
                //defaultModel.FDIPAddress = model.FDIPAddress;
                //defaultModel.FDReRegister = model.FDReRegister;
                //defaultModel.FDTimeToLive = model.FDTimeToLive;
                //defaultModel.FDUDPPort = model.FDUDPPort;
                //defaultModel.IsDebugLogEnable = model.IsDebugLogEnable;

                ResultModel<BACnetConfigurationModel> resultModel = ConfigurationSettingManager.Instance.UpdateBACnetConfigurationSetting(resultData.Result, Path.Combine(AppWebRepository.Instance.ConfiurationFolder, AppConstants.DefaultBACnetConfig));
                if (!resultModel.IsSucceeded)
                {
                    result.Status = false;
                    result.Error = resultModel.Error.ToString();
                }
                else
                    result.Status = true;
            }
            else
            {
                result.Status = false;
                result.Error = "Unable to read BACnet Config Data";
            }

            return result;
        }

        [Mapping("RestoreDefaultConfigurations")]
        public GwApiDataModel RestoreDefaultConfigurations()
        {
            GwApiDataModel result = new GwApiDataModel();

            ResultModel<BACnetConfigurationModel> resultData = ConfigurationSettingManager.Instance.ReadBACnetConfigurationSetting(Path.Combine(AppWebRepository.Instance.ConfiurationFolder, AppConstants.DefaultBACnetConfig));
            if (resultData.IsSucceeded)
            {
                //BACnetConfigurationModel model = Molex.BACnet.Configuration.Core.Repository.AppWebRepository.Instance.BACnetConfigurationData;

                //model.UDPPort = resultData.Result.UDPPort;
                //model.LocalNetworkNumber = resultData.Result.LocalNetworkNumber;
                //model.VirtualNetworkNumber = resultData.Result.VirtualNetworkNumber;
                //model.IPAddress = resultData.Result.IPAddress;
                //model.VendorName = resultData.Result.VendorName;
                //model.Description = resultData.Result.Description;
                //model.ModelName = resultData.Result.ModelName;
                //model.APDURetries = resultData.Result.APDURetries;
                //model.APDUTimeout = resultData.Result.APDUTimeout;
                //model.APDUSegmentTimeout = resultData.Result.APDUSegmentTimeout;
                //model.UTCOffset = resultData.Result.UTCOffset;
                //model.DaylightSavingStatus = resultData.Result.DaylightSavingStatus;
                //model.ControllerId = resultData.Result.ControllerId;
                //model.MolexAPIUrl = resultData.Result.MolexAPIUrl;
                //model.MolexProjectID = resultData.Result.MolexProjectID;
                //model.AlarmNotificationSetting = resultData.Result.AlarmNotificationSetting;
                //model.MonitoringDefaultPriority = resultData.Result.MonitoringDefaultPriority;
                //model.IsBuildingAlarm = resultData.Result.IsBuildingAlarm;
                //model.IsFloorAlarm = resultData.Result.IsFloorAlarm;
                //model.TimeDelay = resultData.Result.TimeDelay;
                //model.NotifyType = resultData.Result.NotifyType;
                //model.BACnetPassword = resultData.Result.BACnetPassword;
                //model.Scheduling = resultData.Result.Scheduling;
                //model.TimeSynchronization = resultData.Result.TimeSynchronization;
                //model.WriteAccess = resultData.Result.WriteAccess;
                //model.MapIndividualFixture = resultData.Result.MapIndividualFixture;
                //model.MapIndividualSensors = resultData.Result.MapIndividualSensors;
                //model.IsBACnetPasswordRequired = resultData.Result.IsBACnetPasswordRequired;
                //model.FDTList = resultData.Result.FDTList;
                //model.BDTList = resultData.Result.BDTList;
                //model.IsBBMD = resultData.Result.IsBBMD;
                //model.FDIPAddress = resultData.Result.FDIPAddress;
                //model.FDReRegister = resultData.Result.FDReRegister;
                //model.FDTimeToLive = resultData.Result.FDTimeToLive;
                //model.FDUDPPort = resultData.Result.FDUDPPort;
                //model.IsDebugLogEnable = resultData.Result.IsDebugLogEnable;

                ResultModel<BACnetConfigurationModel> resultModel = ConfigurationSettingManager.Instance.UpdateBACnetConfigurationSetting(resultData.Result, Path.Combine(AppWebRepository.Instance.ConfiurationFolder, AppConstants.BACnetConfigurationFileName));
                if (!resultModel.IsSucceeded)
                {
                    result.Status = false;
                    result.Error = resultModel.Error.ToString();
                }
                else
                    result.Status = true;
            }
            else
            {
                result.Status = false;
                result.Error = resultData.Error.ToString();
            }

            return result;
        }

        [Mapping("DownloadObjectMapping")]
        public GwApiDataModel DownloadObjectMapping()
        {
            GwApiDataModel result = new GwApiDataModel();

            string mappingFilePath = Path.Combine(AppWebRepository.Instance.ConfiurationFolder, AppConstants.BACnetCSVFile);
            if (System.IO.File.Exists(mappingFilePath))
            {
                string[] lines = File.ReadAllLines(mappingFilePath);
                StringBuilder builder = new StringBuilder();
                for (int ii = 0; ii < lines.Length; ii++)
                {
                    builder.AppendLine(lines[ii]);
                }
                result.JsonData = builder.ToString();
                result.Status = true;
            }
            else
            {
                result.Status = false;
                result.Error = "File not found";
            }

            return result;
        }

        [Mapping("DownloadLogs")]
        public GwApiDataModel DownloadLogs()
        {
            GwApiDataModel result = new GwApiDataModel();
            //

            string zipPath = Path.Combine(AppWebRepository.Instance.ConfiurationFolder, AppConstants.LogZipFileName);
            if (File.Exists(zipPath))
                File.Delete(zipPath);
            try
            {
                System.IO.Compression.ZipFile.CreateFromDirectory(AppCoreRepo.Instance.LogFolderPath, zipPath);
                result.JsonData = zipPath;
                result.Status = true;
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.Error = ex.Message;
                Logger.Instance.Log(ex, LogLevel.ERROR, "DownloadLogs: => Unable to compress logs folder");
            }

            return result;
        }

        [Mapping("GetGWServiceStatus")]
        public GwApiDataModel GetGWServiceStatus()
        {
            GwApiDataModel result = new GwApiDataModel();

            GwServiceStatusModel statusModel = new GwServiceStatusModel();

            ResultModel<string> resultStatus = ServiceControllingManager.RefreshGatewayServiceStatus(AppConstants.BACnetGWServiceName);
            if (resultStatus.IsSucceeded)
            {
                AppWebRepository.Instance.ServiceStatus = resultStatus.Result;
                statusModel.State = resultStatus.Result;
                statusModel.Version = AppWebRepository.Instance.GatewayProductVersion;  // this.GetType().Assembly.GetName().Version

                result.Status = true;
                result.JsonData = statusModel;
                result.Error = string.Empty;
            }
            else
            {
                result.Status = false;
                result.JsonData = string.Empty;
                result.Error = "Unable to get service status";
            }

            return result;
        }

        [Mapping("ToggleGWServiceStatus")]
        public GwApiDataModel ToggleGWServiceStatus(string jsonData)
        {
            GwApiDataModel result = new GwApiDataModel();
            GwToggleStateModel status = null;

            try
            {
                result = JsonConvert.DeserializeObject<GwApiDataModel>(jsonData);
                status = JsonConvert.DeserializeObject<GwToggleStateModel>(result.JsonData.ToString());
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.Error = "Error parsing POST json data";
                result.JsonData = string.Empty;

                Logger.Instance.Log(ex, LogLevel.ERROR, "ToggleGWServiceStatus: => " + result.Error);
                return result;
            }

            ResultModel<string> resultStatus = null;
            if (status.State)
                resultStatus = ServiceControllingManager.StartGatewayService(AppConstants.BACnetGWServiceName);
            else
                resultStatus = ServiceControllingManager.StopGatewayService(AppConstants.BACnetGWServiceName);

            if (resultStatus.IsSucceeded)
            {
                Molex.BACnet.Configuration.Core.Repository.AppWebRepository.Instance.ServiceStatus = resultStatus.Result;

                result.Status = true;
                result.JsonData = "True";
                result.Error = string.Empty;
            }
            else
            {
                result.Status = false;
                result.JsonData = string.Empty;
                result.Error = "Failed to set service status";
            }

            return result;
        }

        [Mapping("GetBacNetConfiguration")]
        public GwApiDataModel GetBacNetConfiguration()
        {
            GwApiDataModel result = new GwApiDataModel();
            GwBacnetConfigurationModel bacnetCfgModel = new GwBacnetConfigurationModel();

            // BACnetConfigurationModel model =  Molex.BACnet.Configuration.Core.Repository.AppWebRepository.Instance.BACnetConfigurationData;
            ResultModel<BACnetConfigurationModel> resultData = ConfigurationSettingManager.Instance.ReadBACnetConfigurationSetting(Path.Combine(AppWebRepository.Instance.ConfiurationFolder, AppConstants.BACnetConfigurationFileName));
            if (resultData.IsSucceeded)
            {
                bacnetCfgModel.IsDebugLogEnable = resultData.Result.IsDebugLogEnable;

                bacnetCfgModel.NetworkInterface.UDPPort = resultData.Result.UDPPort;
                bacnetCfgModel.NetworkInterface.LocalNetworkNumber = resultData.Result.LocalNetworkNumber;
                bacnetCfgModel.NetworkInterface.VirtualNetworkNumber = resultData.Result.VirtualNetworkNumber;
                bacnetCfgModel.NetworkInterface.IPAddress = resultData.Result.IPAddress;
                bacnetCfgModel.NetworkInterface.IPAddressList = new List<IPAddressModel>();
                var nwInterfaces = NetworkInterfaceSettingManager.GetAllNetworkInterfaces();
                if (nwInterfaces.IsSucceeded && nwInterfaces.Result.IPAddressList != null)
                {
                    bacnetCfgModel.NetworkInterface.IPAddressList.AddRange(nwInterfaces.Result.IPAddressList);
                }

                bacnetCfgModel.DeviceObject.VendorName = resultData.Result.VendorName;
                bacnetCfgModel.DeviceObject.Description = resultData.Result.Description;
                bacnetCfgModel.DeviceObject.ModelName = resultData.Result.ModelName;
                bacnetCfgModel.DeviceObject.APDURetries = resultData.Result.APDURetries;
                bacnetCfgModel.DeviceObject.APDUTimeout = resultData.Result.APDUTimeout;
                bacnetCfgModel.DeviceObject.APDUSegmentTimeout = resultData.Result.APDUSegmentTimeout;
                bacnetCfgModel.DeviceObject.UTCOffset = resultData.Result.UTCOffset;
                bacnetCfgModel.DeviceObject.DaylightSavingStatus = resultData.Result.DaylightSavingStatus;

                bacnetCfgModel.Gateway.DeviceId = resultData.Result.ControllerId;
                bacnetCfgModel.Gateway.MolexApiUrl = resultData.Result.MolexAPIUrl;
                bacnetCfgModel.Gateway.MolexProjectId = resultData.Result.MolexProjectID;
                bacnetCfgModel.Gateway.MolexProjectName = resultData.Result.ProjectName;
                bacnetCfgModel.Gateway.MonitoringDefaultPriority = resultData.Result.MonitoringDefaultPriority;

                //bacnetCfgModel.Notification.AlarmNotificationSetting = resultData.Result.AlarmNotificationSetting;
                bacnetCfgModel.Notification.IsBuildingAlarm = resultData.Result.IsBuildingAlarm;
                bacnetCfgModel.Notification.IsFloorAlarm = resultData.Result.IsFloorAlarm;
                bacnetCfgModel.Notification.TimeDelay = resultData.Result.TimeDelay;
                bacnetCfgModel.Notification.NotifyType = resultData.Result.NotifyType;
                bacnetCfgModel.Notification.AlarmEventVM = AppHelper.GetAlarmEventSetting(resultData.Result);
                bacnetCfgModel.Notification.BuildingNotificationPriority = resultData.Result.BuildingNotificationPriority;
                bacnetCfgModel.Notification.ZoneNotificationPriority = resultData.Result.ZoneNotificationPriority;

                bacnetCfgModel.BmsControl.IsBACnetPasswordRequired = resultData.Result.IsBACnetPasswordRequired;
                bacnetCfgModel.BmsControl.BACnetPassword = resultData.Result.BACnetPassword;
                bacnetCfgModel.BmsControl.Scheduling = resultData.Result.Scheduling;
                bacnetCfgModel.BmsControl.TimeSynchronization = resultData.Result.TimeSynchronization;
                bacnetCfgModel.BmsControl.WriteAccess = resultData.Result.WriteAccess;
                bacnetCfgModel.BmsControl.MapIndividualFixture = resultData.Result.MapIndividualFixture;
                bacnetCfgModel.BmsControl.MapIndividualSensors = resultData.Result.MapIndividualSensors;
                bacnetCfgModel.BmsControl.IsTempRequiredInFahrenheit = resultData.Result.IsTempRequiredInFahrenheit;
                bacnetCfgModel.BmsControl.CharacterEncodingTypes = AppConstants.CharacterEncodingTypes;

                bacnetCfgModel.Broadcast.IsBBMD = resultData.Result.IsBBMD;
                bacnetCfgModel.Broadcast.FDIPAddress = resultData.Result.FDIPAddress;
                bacnetCfgModel.Broadcast.FDReRegister = resultData.Result.FDReRegister;
                bacnetCfgModel.Broadcast.FDTimeToLive = resultData.Result.FDTimeToLive;
                bacnetCfgModel.Broadcast.FDUDPPort = resultData.Result.FDUDPPort;
                bacnetCfgModel.Broadcast.FDTList = resultData.Result.FDTList;  //AppCoreRepo.Instance.BACnetConfigurationData.FDTList; // model.FDTList; // this is required latest FDT entries are updated in model
                bacnetCfgModel.Broadcast.BDTList = resultData.Result.BDTList;

                if (bacnetCfgModel.Broadcast.BDTList == null)
                {
                    bacnetCfgModel.Broadcast.BDTList = new List<BDTModel>();
                    BDTModel bdtModel = new BDTModel();
                    bdtModel.BroadcastMask = 32;
                    bdtModel.IPAddress = resultData.Result.IPAddress;
                    bdtModel.Port = (ushort)resultData.Result.UDPPort;
                    bdtModel.IsSelfEntry = true;
                    bacnetCfgModel.Broadcast.BDTList.Add(bdtModel);
                }

                if (bacnetCfgModel.Broadcast.FDTList == null && AppCoreRepo.Instance.BACnetConfigurationData.FDTList != null)                   
                {
                    bacnetCfgModel.Broadcast.FDTList = new List<FDTModel>();

                    foreach (Molex.StackDataObjects.APIModels.FDTModel item in AppCoreRepo.Instance.BACnetConfigurationData.FDTList)
                    {
                        bacnetCfgModel.Broadcast.FDTList.Add(new FDTModel
                        {
                            IPAddress = item.IPAddress,
                            Port = item.Port,
                            TimeRemaining = item.TimeRemaining,
                            TimeToLive = item.TimeToLive
                        });
                    }
                }


                bacnetCfgModel.Communication.CurrentModule = resultData.Result.IsMQTTPublishSelected == true ? "MQTT" : "HTTP";

                bacnetCfgModel.Security.APIpassword = resultData.Result.Password;
                bacnetCfgModel.Security.APIuserID = resultData.Result.UserName;

                result.Status = true;
                result.JsonData = bacnetCfgModel;
                result.Error = string.Empty;
            }
            else
            {
                result.Status = false;
                result.Error = resultData.Error.ToString();
                result.JsonData = string.Empty;
            }
            return result;
        }

        [Mapping("SaveBacNetConfiguration")]
        public GwApiDataModel SaveBacNetConfiguration(string jsonData)
        {
            GwApiDataModel result = new GwApiDataModel();

            BACnetConfigurationModel bacnetCfgModel = new BACnetConfigurationModel();
            GwBacnetConfigurationModel model = null;
            try
            {
                result = JsonConvert.DeserializeObject<GwApiDataModel>(jsonData);
                model = JsonConvert.DeserializeObject<GwBacnetConfigurationModel>(result.JsonData.ToString());
                //GwBacnetConfigurationModel model = JsonConvert.DeserializeObject<GwBacnetConfigurationModel>(jsonData);
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.Error = "Error parsing POST json data";
                result.JsonData = string.Empty;

                Logger.Instance.Log(ex, LogLevel.ERROR, "SaveBacNetConfiguration: => " + result.Error);
                return result;
            }

            bacnetCfgModel.IsDebugLogEnable = model.IsDebugLogEnable;
            if (model.NetworkInterface.UDPPort <= 0 || model.NetworkInterface.UDPPort > 65535)
            {
                result.Status = false;
                result.Error = "SaveBacNetConfiguration Error Received : Invalid NetworkInterface UDPPort Value : " + model.NetworkInterface.UDPPort.ToString();
                result.JsonData = string.Empty;
                return result;
            }
            bacnetCfgModel.UDPPort = model.NetworkInterface.UDPPort;
            if (model.NetworkInterface.LocalNetworkNumber < 1 || model.NetworkInterface.LocalNetworkNumber > 65534)
            {
                result.Status = false;
                result.Error = "SaveBacNetConfiguration Error Received : Invalid NetworkInterface LocalNetworkNumber Value : " + model.NetworkInterface.LocalNetworkNumber.ToString();
                result.JsonData = string.Empty;
                return result;
            }
            bacnetCfgModel.LocalNetworkNumber = model.NetworkInterface.LocalNetworkNumber;

            if (model.NetworkInterface.VirtualNetworkNumber < 1 || model.NetworkInterface.VirtualNetworkNumber > 65534)
            {
                result.Status = false;
                result.Error = "SaveBacNetConfiguration Error Received : Invalid NetworkInterface VirtualNetworkNumber Value : " + model.NetworkInterface.VirtualNetworkNumber.ToString();
                result.JsonData = string.Empty;
                return result;
            }
            bacnetCfgModel.VirtualNetworkNumber = model.NetworkInterface.VirtualNetworkNumber;

            IPAddress value;
            if (!IPAddress.TryParse(model.NetworkInterface.IPAddress, out value))
            {
                result.Status = false;
                result.Error = "SaveBacNetConfiguration Error Received : Invalid NetworkInterface IPAddress Value : " + model.NetworkInterface.IPAddress.ToString();
                result.JsonData = string.Empty;
                return result;
            }
            bacnetCfgModel.IPAddress = model.NetworkInterface.IPAddress;

            if (string.IsNullOrEmpty(model.DeviceObject.VendorName))
            {
                result.Status = false;
                result.Error = "SaveBacNetConfiguration Error Received : Invalid DeviceObject VendorName Value";
                result.JsonData = string.Empty;
                return result;
            }
            if (model.DeviceObject.VendorName.Trim().Length > 255)
            {
                result.Status = false;
                result.Error = "SaveBacNetConfiguration Error Received : DeviceObject VendorName must contain maximum 255 characters.";
                result.JsonData = string.Empty;
                return result;
            }
            bacnetCfgModel.VendorName = model.DeviceObject.VendorName;

            if (string.IsNullOrEmpty(model.DeviceObject.Description))
            {
                result.Status = false;
                result.Error = "SaveBacNetConfiguration Error Received : Invalid DeviceObject Description Value";
                result.JsonData = string.Empty;
                return result;
            }
            if (model.DeviceObject.Description.Trim().Length > 255)
            {
                result.Status = false;
                result.Error = "SaveBacNetConfiguration Error Received : DeviceObject Description must contain maximum 255 characters.";
                result.JsonData = string.Empty;
                return result;
            }
            bacnetCfgModel.Description = model.DeviceObject.Description;

            if (string.IsNullOrEmpty(model.DeviceObject.ModelName))
            {
                result.Status = false;
                result.Error = "SaveBacNetConfiguration Error Received : Invalid DeviceObject ModelName Value";
                result.JsonData = string.Empty;
                return result;
            }
            if (model.DeviceObject.ModelName.Trim().Length > 255)
            {
                result.Status = false;
                result.Error = "SaveBacNetConfiguration Error Received : DeviceObject ModelName must contain maximum 255 characters.";
                result.JsonData = string.Empty;
                return result;
            }
            bacnetCfgModel.ModelName = model.DeviceObject.ModelName;

            if (model.DeviceObject.APDURetries < 0 || model.DeviceObject.APDURetries > 255)
            {
                result.Status = false;
                result.Error = "SaveBacNetConfiguration Error Received : Invalid DeviceObject APDURetries Value : " + model.DeviceObject.APDURetries.ToString();
                result.JsonData = string.Empty;
                return result;
            }
            bacnetCfgModel.APDURetries = model.DeviceObject.APDURetries;

            if (model.DeviceObject.APDUTimeout < 6000 || model.DeviceObject.APDUTimeout > 65000)
            {
                result.Status = false;
                result.Error = "SaveBacNetConfiguration Error Received : Invalid DeviceObject APDUTimeout Value : " + model.DeviceObject.APDUTimeout.ToString();
                result.JsonData = string.Empty;
                return result;
            }
            bacnetCfgModel.APDUTimeout = model.DeviceObject.APDUTimeout;

            if (model.DeviceObject.APDUSegmentTimeout < 5000 || model.DeviceObject.APDUSegmentTimeout > 65000)
            {
                result.Status = false;
                result.Error = "SaveBacNetConfiguration Error Received : Invalid DeviceObject APDUSegmentTimeout Value : " + model.DeviceObject.APDUSegmentTimeout.ToString();
                result.JsonData = string.Empty;
                return result;
            }
            bacnetCfgModel.APDUSegmentTimeout = model.DeviceObject.APDUSegmentTimeout;

            if (model.DeviceObject.UTCOffset < -780 || model.DeviceObject.UTCOffset > 780)
            {
                result.Status = false;
                result.Error = "SaveBacNetConfiguration Error Received : Invalid DeviceObject UTCOffset Value : " + model.DeviceObject.UTCOffset.ToString();
                result.JsonData = string.Empty;
                return result;
            }
            bacnetCfgModel.UTCOffset = model.DeviceObject.UTCOffset;
            bacnetCfgModel.DaylightSavingStatus = model.DeviceObject.DaylightSavingStatus;

            if (string.IsNullOrEmpty(model.Security.APIuserID) || string.IsNullOrEmpty(model.Security.APIpassword))
            {
                result.Status = false;
                result.Error = "SaveBacNetConfiguration Error Received : Empty API Credentials Value";
                result.JsonData = string.Empty;
                return result;
            }
            bacnetCfgModel.UserName = model.Security.APIuserID;
            bacnetCfgModel.Password = model.Security.APIpassword;

            bacnetCfgModel.ControllerId = model.Gateway.DeviceId;

            if (string.IsNullOrEmpty(model.Gateway.MolexApiUrl))
            {
                result.Status = false;
                result.Error = "SaveBacNetConfiguration Error Received : Empty API URL Value";
                result.JsonData = string.Empty;
                return result;
            }
            bacnetCfgModel.MolexAPIUrl = model.Gateway.MolexApiUrl;

            if (string.IsNullOrEmpty(model.Gateway.MolexProjectId))
            {
                result.Status = false;
                result.Error = "SaveBacNetConfiguration Error Received : Empty API MolexProjectId Value";
                result.JsonData = string.Empty;
                return result;
            }


            if (model.Notification.BuildingNotificationPriority.ToNormal > 255 || model.Notification.ZoneNotificationPriority.ToFault > 255 ||
                model.Notification.BuildingNotificationPriority.ToOffNormal > 255 || model.Notification.ZoneNotificationPriority.ToNormal > 255 ||
                model.Notification.BuildingNotificationPriority.ToFault > 255 || model.Notification.ZoneNotificationPriority.ToOffNormal > 255)
            {
                result.Status = false;
                result.Error = "Invalid Range Of Notification Priority Provided, kindly specify the values in (0-255) range.";
                result.JsonData = string.Empty;
                return result;
            }

            bacnetCfgModel.MolexProjectID = model.Gateway.MolexProjectId;
            bacnetCfgModel.ProjectName = model.Gateway.MolexProjectName;
            bacnetCfgModel.MonitoringDefaultPriority = model.Gateway.MonitoringDefaultPriority;

            bacnetCfgModel.IsBuildingAlarm = model.Notification.IsBuildingAlarm;
            bacnetCfgModel.IsFloorAlarm = model.Notification.IsFloorAlarm;
            bacnetCfgModel.TimeDelay = model.Notification.TimeDelay;
            bacnetCfgModel.NotifyType = model.Notification.NotifyType;
            AppHelper.SetAlarmEventSetting(model.Notification.AlarmEventVM, bacnetCfgModel);
            bacnetCfgModel.BuildingNotificationPriority = model.Notification.BuildingNotificationPriority;
            bacnetCfgModel.ZoneNotificationPriority = model.Notification.ZoneNotificationPriority;
            //bacnetCfgModel.AlarmNotificationSetting = model.AlarmNotificationSetting;

            bacnetCfgModel.BDTList = model.Broadcast.BDTList;
            bacnetCfgModel.FDTList = model.Broadcast.FDTList;

            bacnetCfgModel.IsBACnetPasswordRequired = model.BmsControl.IsBACnetPasswordRequired;
            bacnetCfgModel.BACnetPassword = model.BmsControl.BACnetPassword;
            bacnetCfgModel.Scheduling = model.BmsControl.Scheduling;
            bacnetCfgModel.TimeSynchronization = model.BmsControl.TimeSynchronization;
            bacnetCfgModel.WriteAccess = model.BmsControl.WriteAccess;
            bacnetCfgModel.MapIndividualFixture = model.BmsControl.MapIndividualFixture;
            bacnetCfgModel.MapIndividualSensors = model.BmsControl.MapIndividualSensors;
            bacnetCfgModel.IsTempRequiredInFahrenheit = model.BmsControl.IsTempRequiredInFahrenheit;

            bacnetCfgModel.IsBBMD = model.Broadcast.IsBBMD;
            bacnetCfgModel.FDIPAddress = model.Broadcast.FDIPAddress;
            bacnetCfgModel.FDReRegister = model.Broadcast.FDReRegister;
            bacnetCfgModel.FDTimeToLive = model.Broadcast.FDTimeToLive;
            bacnetCfgModel.FDUDPPort = model.Broadcast.FDUDPPort;

            if (model.Communication.CurrentModule == "MQTT")
                bacnetCfgModel.IsMQTTPublishSelected = true;
            else
                bacnetCfgModel.IsMQTTPublishSelected = false;

            bacnetCfgModel.Password = model.Security.APIpassword;
            bacnetCfgModel.UserName = model.Security.APIuserID;

            ResultModel<BACnetConfigurationModel> resultStatus = ConfigurationSettingManager.Instance.UpdateBACnetConfigurationSetting(bacnetCfgModel, Path.Combine(AppWebRepository.Instance.ConfiurationFolder, AppConstants.BACnetConfigurationFileName));
            if (resultStatus.IsSucceeded)
            {
                Molex.BACnet.Configuration.Core.Repository.AppWebRepository.Instance.BACnetConfigurationData = bacnetCfgModel;
                result.Status = true;
                result.JsonData = "True";
                result.Error = string.Empty;
            }
            else
            {
                result.Status = false;
                result.JsonData = resultStatus.Error.ToString();
                result.Error = string.Empty;
            }

            return result;
        }



        /// <summary>
        /// This method will stop the HTTP listener
        /// </summary>
        public void Stop()
        {
            try
            {
                if (listener == null)
                    return;
                if (listener.IsListening)
                    listener.Stop();
                listener.Close();
                HandlerThread = 0;
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "GatewayHTTPControllerApi:Stop Exception Received");
            }
        }
    }

    class Mapping : Attribute
    {
        public string Map;
        public Mapping(string s)
        {
            Map = s;
        }
    }
}
