﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              <ResultModel.cs>
///   Description:        <This class will provide templete for Result Model including Data, Error Log and success/failure messgge>
///   Author:             Rupesh Saw                  
///   Date:               05/22/17
///---------------------------------------------------------------------------------------------
#endregion



namespace Molex.BACnet.Gateway.ServiceCore.Models
{
    /// <summary>
    /// This class will provide templete for Result Model including Data, Error Log and success/failure messgge
    /// </summary>
    /// <typeparam name="T">Generic Type</typeparam>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:39 PM</TimeStamp>
    public class ResultModel<T>
    {
        #region Public Properties

        /// <summary>
        /// Initializes a new instance of the <see cref="ResultModel{T}"/> class.
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>2/28/20187:31 PM</TimeStamp>
        public ResultModel()
        {
            Error = new ErrorModel();
        }
        /// <summary>
        /// Gets or sets a value indicating whether this instance is success.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is success; otherwise, <c>false</c>.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201706:44 PM</TimeStamp>
        public bool IsSuccess { get; set; }

        /// <summary>
        /// Gets or sets the error.
        /// </summary>
        /// <value>
        /// The error.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201706:45 PM</TimeStamp>
        public ErrorModel Error { get; set; }

        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        /// <value>
        /// The data.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201706:45 PM</TimeStamp>
        public T Data { get; set; }

        #endregion
    }
}
