﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models
{
    /// <summary>
    /// It is used to update space state API
    /// </summary>
    /// <CreatedBy>Nazneen</CreatedBy><TimeStamp>15-06-201712:28 PM</TimeStamp>
    public class AllZoneDataModel
    {
        /// <summary>
        /// Gets or sets the success zone data.
        /// </summary>
        /// <value>
        /// This will hold the List of success Zone Data.
        /// </value>
        /// <CreatedBy>Nazneen</CreatedBy><TimeStamp>15-06-201712:29 PM</TimeStamp>
        public Dictionary<string, string> SuccessZoneData { get; set; }

        /// <summary>
        /// Gets or sets the failed zone data.
        /// </summary>
        /// <value>
        /// This will hold the List of Failed Zone Data.
        /// </value>
        /// <CreatedBy>Nazneen</CreatedBy><TimeStamp>15-06-201712:29 PM</TimeStamp>
        public List<string> FailedZoneData { get; set; }

    }
}
