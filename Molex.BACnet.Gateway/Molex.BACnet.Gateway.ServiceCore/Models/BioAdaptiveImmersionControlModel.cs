﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models
{
    internal class BioDynamicSaturationControlModel : APIUpdateZoneModel
    {
        public string value { get; set; }
        public string property { get; set; }
    }
}
