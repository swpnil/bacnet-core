﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              BACnetObjectDetails
///   Description:        Details regarding BACnetObjectDetails
///   Author:             Rohit Galgali              
///   Date:               5/29/2017
#endregion

using Molex.StackDataObjects.Constants;

namespace Molex.BACnet.Gateway.ServiceCore.Models.BACnet
{
    /// <summary>
    /// Details regarding BACnetObjectDetails
    /// </summary>
    /// <CreatedBy>hp</CreatedBy>
    /// <TimeStamp>5/29/20176:40 PM</TimeStamp>
    public class BACnetObjectDetails
    {
        /// <summary>
        /// Gets or sets the object identifier.
        /// </summary>
        /// <value>
        /// The object identifier.
        /// </value>
        /// <CreatedBy>Rohit Galgali</CreatedBy>
        /// <TimeStamp>5/29/20176:40 PM</TimeStamp>
        public uint ObjectID { get; set; }

        /// <summary>
        /// Gets or sets the name of the object.
        /// </summary>
        /// <value>
        /// The name of the object.
        /// </value>
        /// <CreatedBy>Rohit Galgali</CreatedBy>
        /// <TimeStamp>6/28/20174:05 PM</TimeStamp>
        public string ObjectName { get; set; }

        /// <summary>
        /// Gets or sets the object description.
        /// </summary>
        /// <value>
        /// The object description.
        /// </value>
        /// <CreatedBy>Rohit Galgali</CreatedBy>
        /// <TimeStamp>6/29/20173:02 PM</TimeStamp>
        public string ObjectDescription { get; set; }

        /// <summary>
        /// Gets or sets the present value.
        /// </summary>
        /// <value>
        /// The present value.
        /// </value>
        /// <CreatedBy>Rohit Galgali</CreatedBy>
        /// <TimeStamp>5/29/20176:40 PM</TimeStamp>
        public object PresentValue { get; set; }

        /// <summary>
        /// Gets or sets the reliability.
        /// </summary>
        /// <value>
        /// The reliability.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>7/5/201711:23 AM</TimeStamp>
        public BacnetReliability Reliability { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [out of service].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [out of service]; otherwise, <c>false</c>.
        /// </value>
        /// <CreatedBy>Rohit Galgali</CreatedBy>
        /// <TimeStamp>7/10/20176:08 PM</TimeStamp>
        public bool IsOutOfService { get; set; }

        /// <summary>
        /// Gets or sets the mood.
        /// </summary>
        /// <value>
        /// The mood.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>8/7/20177:37 PM</TimeStamp>
        public string Mood { get; set; }

        /// <summary>
        /// Gets or sets the type of the object.
        /// </summary>
        /// <value>
        /// The type of the object.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>9/14/20178:52 PM</TimeStamp>
        public BacnetObjectType ObjectType { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="BACnetObjectDetails"/> class.
        /// </summary>
        /// <CreatedBy>Rohit Galgali</CreatedBy>
        /// <TimeStamp>5/29/20176:40 PM</TimeStamp>
        public BACnetObjectDetails()
        {
            PresentValue = new object();
        }
    }
}
