﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              CreateObjectResponseModel
///   Description:        BACnet Object Information as response
///   Author:             prasad.joshi                
///   Date:               29/05/17
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models.BACnet
{
    /// <summary>
    /// 
    /// </summary>
    /// <CreatedBy>prasad.joshi</CreatedBy><TimeStamp>5/29/20175:02 PM</TimeStamp>
    public class CreateObjectResultModel
    {
        /// <summary>
        /// Gets or sets the device identifier.
        /// </summary>
        /// <value>
        /// The device identifier.
        /// </value>
        /// <CreatedBy>prasad.joshi</CreatedBy><TimeStamp>5/29/20175:02 PM</TimeStamp>
        public uint DeviceId { get; set; }
        /// <summary>
        /// Gets or sets the object identifier.
        /// </summary>
        /// <value>
        /// The object identifier.
        /// </value>
        /// <CreatedBy>prasad.joshi</CreatedBy><TimeStamp>5/29/20175:02 PM</TimeStamp>
        public uint ObjectId { get; set; }

    }
}
