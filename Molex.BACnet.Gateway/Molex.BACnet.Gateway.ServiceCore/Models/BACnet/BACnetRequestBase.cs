﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              <BACnetRequestBase.cs>
///   Description:        <BACnet Request Base class storing all required properties>
///   Author:             Mandar                  
///   Date:               05/19/17
///   Notes:              <Notes>
///   Revision History: 
///   Modified By:     Modified Date:    Reviewed By:      Reviewed Date:    Review Comments:
///---------------------------------------------------------------------------------------------
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models.BACnet
{
    /// <summary>
    /// BACnet Request Base class storing all required properties
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:31 PM</TimeStamp>
    public class BACnetRequestBase
    {
        #region Public Properties
        /// <summary>
        /// Gets or sets the object identifier.
        /// </summary>
        /// <value>
        /// The object identifier.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>15-06-201712:44 PM</TimeStamp>
        public uint ObjectId { get; set; }

        /// <summary>
        /// Gets or sets the device identifier.
        /// </summary>
        /// <value>
        /// The device identifier.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>15-06-201712:44 PM</TimeStamp>
        public uint DeviceId { get; set; }

        #endregion

    }
}
