﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              PersistDifferenceModel
///   Description:        <Description>
///   Author:             Prasad Joshi                  
///   Date:               MM/DD/YY
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel
{
    /// <summary>
    /// 
    /// </summary>
    /// <CreatedBy>hp</CreatedBy>
    /// <TimeStamp>7/28/201712:56 PM</TimeStamp>
    public class PersistedModels
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PersistedModels"/> class.
        /// </summary>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>8/1/20174:03 PM</TimeStamp>
        public PersistedModels()
        {
            NotificationClasses = new List<NotificationClassPersistedModel>();
            EventEnrollments = new List<EventEnrollmentPersistedModel>();
            TrendLogs = new List<TrendLogPersistedModel>();
            Schedules = new List<SchedulePersistedModel>();
            ObjectDevice = new ObjectDevicePersistedModel();
            Calendars = new List<CalendarPersistedModel>();
        }

        #endregion Constructor

        #region PublicProperties

        /// <summary>
        /// Gets or sets the notification classes.
        /// </summary>
        /// <value>
        /// The notification classes.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>7/28/201712:55 PM</TimeStamp>
        public List<NotificationClassPersistedModel> NotificationClasses { get; set; }


        /// <summary>
        /// Gets or sets the event enrollments.
        /// </summary>
        /// <value>
        /// The event enrollments.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>7/28/201712:55 PM</TimeStamp>
        public List<EventEnrollmentPersistedModel> EventEnrollments { get; set; }


        /// <summary>
        /// Gets or sets the trend logs.
        /// </summary>
        /// <value>
        /// The trend logs.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>7/28/201712:55 PM</TimeStamp>
        public List<TrendLogPersistedModel> TrendLogs { get; set; }


        /// <summary>
        /// Gets or sets the schedules.
        /// </summary>
        /// <value>
        /// The schedules.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>7/28/201712:55 PM</TimeStamp>
        public List<SchedulePersistedModel> Schedules { get; set; }


        /// <summary>
        /// Gets or sets the calendars.
        /// </summary>
        /// <value>
        /// The calendars.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>8/22/201712:19 PM</TimeStamp>
        public List<CalendarPersistedModel> Calendars { get; set; }


        /// <summary>
        /// Gets or sets the object devices.
        /// </summary>
        /// <value>
        /// The object devices.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>7/28/201712:56 PM</TimeStamp>
        public ObjectDevicePersistedModel ObjectDevice { get; set; }

        /// <summary>
        /// Gets or sets the device identifier.
        /// </summary>
        /// <value>
        /// The device identifier.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>8/22/201712:19 PM</TimeStamp>
        public uint DeviceID { get; set; }

        #endregion PublicProperties


    }
}
