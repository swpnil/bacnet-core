﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              <Class Name>
///   Description:        <Description>
///   Author:             Rohit Galgali                 
///   Date:               MM/DD/YY
#endregion

using Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel.DataTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Molex.StackDataObjects.Constants;

namespace Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel.PersistentBase" />
    /// <CreatedBy>hp</CreatedBy>
    /// <TimeStamp>7/31/20172:06 PM</TimeStamp>
    public class SchedulePersistedModel : PersistentBase
    {

        public SchedulePersistedModel()
        {
            WeeklySchedule = new List<TimeValueArrayModel>();
            ExceptionSchedule = new List<SpecialEventModel>();
            ObjectPropertyRefernces = new List<DevObjPropRefModel>();
        }

        /// <summary>
        /// Gets or sets the schedule default.
        /// </summary>
        /// <value>
        /// The schedule default.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>7/31/20173:05 PM</TimeStamp>
        public PersistentValueModel ScheduleDefault { get; set; }

        /// <summary>
        /// Gets or sets the bacnet time value list array.
        /// </summary>
        /// <value>
        /// The bacnet time value list array.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>7/31/20173:05 PM</TimeStamp>
        public List<TimeValueArrayModel> WeeklySchedule { get; set; }

        /// <summary>
        /// Gets or sets the exception schedule.
        /// </summary>
        /// <value>
        /// The exception schedule.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>7/31/20173:05 PM</TimeStamp>
        public List<SpecialEventModel> ExceptionSchedule { get; set; }

        /// <summary>
        /// Gets or sets the effective period.
        /// </summary>
        /// <value>
        /// The effective period.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>7/31/20173:05 PM</TimeStamp>
        public DateRangeModel EffectivePeriod { get; set; }

        /// <summary>
        /// Gets or sets the priority for writing.
        /// </summary>
        /// <value>
        /// The priority for writing.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>7/31/20173:06 PM</TimeStamp>
        public uint PriorityForWriting { get; set; }

        /// <summary>
        /// Gets or sets the object property refernces.
        /// </summary>
        /// <value>
        /// The object property refernces.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>7/31/20173:06 PM</TimeStamp>
        public List<DevObjPropRefModel> ObjectPropertyRefernces { get; set; }
    }
}
