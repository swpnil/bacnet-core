﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Molex.BACnet.Gateway.ServiceCore.Constants;
using Molex.StackDataObjects.APIModels;
using Molex.StackDataObjects.Models;
using Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel.DataTypes;
using Molex.StackDataObjects.Constants;

namespace Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel
{
    /// <summary>
    /// 
    /// </summary>
    /// <CreatedBy>hp</CreatedBy>
    /// <TimeStamp>7/27/20175:28 PM</TimeStamp>
    /// <seealso cref="Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel.PersistentBase" />
    public class EventEnrollmentPersistedModel : PersistentBase
    {
        /// <summary>
        /// Gets or sets the type of the event.
        /// </summary>
        /// <value>
        /// The type of the event.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>8/16/201712:43 PM</TimeStamp>
        public BacnetEventType EventType { get; set; }

        /// <summary>
        /// Gets or sets the event parameter.
        /// </summary>
        /// <value>
        /// The event parameter.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>7/31/201712:58 PM</TimeStamp>
        public OutOfRangeModel OutOfRangeModel { get; set; }


        /// <summary>
        /// Gets or sets the command failure model.
        /// </summary>
        /// <value>
        /// The command failure model.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>8/16/20172:11 PM</TimeStamp>
        public CommandFailureModel CommandFailureModel { get; set; }

        public ChangeOfStateEventModel ChangeOfStateEventModel { get; set; }

        /// <summary>
        /// Gets or sets my property.
        /// </summary>
        /// <value>
        /// My property.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>7/31/201712:58 PM</TimeStamp>
        public DevObjPropRefModel ObjectPropertyReference { get; set; }

        /// <summary>
        /// Gets or sets the notification class.
        /// </summary>
        /// <value>
        /// The notification class.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>7/31/20171:00 PM</TimeStamp>
        public uint NotificationClass { get; set; }

        /// <summary>
        /// Gets or sets the event enable.
        /// </summary>
        /// <value>
        /// The event enable.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>7/31/20171:01 PM</TimeStamp>
        public EventTransitionModel EventEnable { get; set; }

        /// <summary>
        /// Gets or sets my property.
        /// </summary>
        /// <value>
        /// My property.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>7/31/20171:03 PM</TimeStamp>
        public BacnetNotifyType NotificationType { get; set; }
    }
}
