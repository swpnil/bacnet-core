﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel.PersistentBase" />
    /// <CreatedBy>hp</CreatedBy>
    /// <TimeStamp>7/28/201712:42 PM</TimeStamp>
    public class ObjectDevicePersistedModel : PersistentBase
    {

        /// <summary>
        /// Gets or sets the apdu timeout.
        /// </summary>
        /// <value>
        /// The apdu timeout.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>7/28/201712:42 PM</TimeStamp>
        public int? APDUTimeout { get; set; }

        /// <summary>
        /// Gets or sets the number of apdu retries.
        /// </summary>
        /// <value>
        /// The number of apdu retries.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>7/28/201712:42 PM</TimeStamp>
        public int? NumberOfAPDURetries { get; set; }

        /// <summary>
        /// Gets or sets the apdu segment time out.
        /// </summary>
        /// <value>
        /// The apdu segment time out.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>7/28/201712:42 PM</TimeStamp>
        public int? APDUSegmentTimeOut { get; set; }
    }
}
