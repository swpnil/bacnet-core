﻿using Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel.DataTypes;
using Molex.StackDataObjects.APIModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel
{
    /// <summary>Da
    /// 
    /// </summary>
    /// <CreatedBy>hp</CreatedBy>
    /// <TimeStamp>7/27/20176:37 PM</TimeStamp>
    /// <seealso cref="Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel.PersistentBase" />
    public class NotificationClassPersistedModel : PersistentBase
    {
        /// <summary>
        /// Class Constructor
        /// </summary>
        public NotificationClassPersistedModel()
        {
            RecipientList = new List<DestinationModel>();
            NotificationPriority = new List<uint>();
        }
        /// <summary>
        /// Gets or sets the recipient list.
        /// </summary>
        /// <value>
        /// The recipient list.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>7/31/201712:46 PM</TimeStamp>
        public List<DestinationModel> RecipientList { get; set; }

        /// <summary>
        /// Gets or sets the ack required.
        /// </summary>
        /// <value>
        /// The ack required.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>7/31/201712:46 PM</TimeStamp>
        public EventTransitionModel AckRequired { get; set; }

        /// <summary>
        /// Notification Class Customized Priority
        /// </summary>
        public List<uint> NotificationPriority { get; set; }
    }
}
