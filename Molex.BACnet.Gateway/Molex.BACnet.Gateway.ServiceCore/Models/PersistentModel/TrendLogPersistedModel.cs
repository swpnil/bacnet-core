﻿using Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel.DataTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel.PersistentBase" />
    /// <CreatedBy>hp</CreatedBy>
    /// <TimeStamp>7/31/20173:04 PM</TimeStamp>
    public class TrendLogPersistedModel : PersistentBase
    {
        /// <summary>
        /// Gets or sets the start time.
        /// </summary>
        /// <value>
        /// The start time.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>7/31/20173:04 PM</TimeStamp>
        public DateTimeModel StartTime { get; set; }

        /// <summary>
        /// Gets or sets the stop time.
        /// </summary>
        /// <value>
        /// The stop time.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>7/31/20173:04 PM</TimeStamp>
        public DateTimeModel StopTime { get; set; }

        /// <summary>
        /// Gets or sets the log device object.
        /// </summary>
        /// <value>
        /// The log device object.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>7/31/20173:04 PM</TimeStamp>
        public DevObjPropRefModel LogDeviceObject { get; set; }

        /// <summary>
        /// Gets or sets the log interval.
        /// </summary>
        /// <value>
        /// The log interval.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>7/31/20173:05 PM</TimeStamp>
        public uint LogInterval { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [stop when full].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [stop when full]; otherwise, <c>false</c>.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>7/31/20173:05 PM</TimeStamp>
        public bool StopWhenFull { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="TrendLogPersistedModel"/> is enable.
        /// </summary>
        /// <value>
        ///   <c>true</c> if enable; otherwise, <c>false</c>.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>7/31/20173:05 PM</TimeStamp>
        public bool Enable { get; set; }
    }
}
