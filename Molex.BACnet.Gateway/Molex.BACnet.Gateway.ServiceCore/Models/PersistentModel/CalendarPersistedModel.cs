﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel.DataTypes;

namespace Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel.PersistentBase" />
    /// <CreatedBy>hp</CreatedBy>
    /// <TimeStamp>7/27/20175:27 PM</TimeStamp>
    public class CalendarPersistedModel : PersistentBase
    {
        public List<CalendarEntryModel> DateList { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CalendarPersistedModel"/> class.
        /// </summary>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>8/23/20174:23 PM</TimeStamp>
        public CalendarPersistedModel()
        {
            DateList = new List<CalendarEntryModel>();
        }
    }
}
