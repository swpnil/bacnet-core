﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel.DataTypes
{
    public class TimeValueArrayModel
    {
        public int DayCount { get; set; }
        public List<TimeValueModel> TimeValueList { get; set; }
    }
}
