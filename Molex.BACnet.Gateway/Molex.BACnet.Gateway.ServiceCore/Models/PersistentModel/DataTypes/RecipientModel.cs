﻿using Molex.StackDataObjects.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel.DataTypes
{
    public class RecipientModel
    {
        public RecipientModel()
        {
            BacnetAddress = new AddressModel();
        }

        public DestinationType DestinationType { get; set; }

        public BacnetObjectType ObjectType { get; set; }
        /** Object Instance*/
        public uint ObjId { get; set; }

        public AddressModel BacnetAddress { get; set; }
    }
}
