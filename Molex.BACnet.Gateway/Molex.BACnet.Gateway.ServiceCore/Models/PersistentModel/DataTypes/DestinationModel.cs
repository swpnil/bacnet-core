﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel.DataTypes
{
    public class DestinationModel
    {
        public DestinationModel()
        {
            FromTime = new TimeModel();
            ToTime = new TimeModel();
            DaysOfWeek = new DaysOfWeekModel();
            EventTransitions = new EventTransitionModel();
            BacnetRecipient = new RecipientModel();
        }

        public bool IssueConfirmedNotification { get; set; }
        public uint ProcessID { get; set; }
        public TimeModel FromTime { get; set; }
        public TimeModel ToTime { get; set; }
        public DaysOfWeekModel DaysOfWeek { get; set; }
        public EventTransitionModel EventTransitions { get; set; }
        public RecipientModel BacnetRecipient { get; set; }
    }
}
