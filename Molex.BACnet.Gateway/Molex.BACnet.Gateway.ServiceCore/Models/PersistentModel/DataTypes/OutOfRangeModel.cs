﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel.DataTypes
{
    public class OutOfRangeModel
    {
        public uint TimeDelay { get; set; }
        public float LowLimit { get; set; }
        public float HighLimit { get; set; }
        public float DeadBand { get; set; }
    }
}
