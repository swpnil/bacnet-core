﻿using Molex.StackDataObjects.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel.DataTypes
{
    public class CommandFailureModel
    {
        public CommandFailureModel()
        {
            ObjectPropertyReference = new DevObjPropRefModel();
        }

        public BacnetApplicationTag ApplicationTag { get; set; }
        public uint TimeDelay { get; set; }
        public DevObjPropRefModel ObjectPropertyReference { get; set; }
        public object Value { get; set; }
    }
}
