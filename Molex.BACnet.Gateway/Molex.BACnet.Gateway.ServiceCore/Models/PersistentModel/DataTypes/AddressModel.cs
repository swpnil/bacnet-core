﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              AddressModel
///   Description:        <Description>
///   Author:             Rohit Galgali
///   Date:               MM/DD/YY
#endregion
      
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel.DataTypes
{
    public class AddressModel
    {
        public byte IPAddressLength { get; set; }
        public byte MacLength { get; set; }
        public ushort NetworkNumber { get; set; }
        public string IPAddress { get; set; }
        public uint Port { get; set; }
        public string MacAddress { get; set; }
    }
}
