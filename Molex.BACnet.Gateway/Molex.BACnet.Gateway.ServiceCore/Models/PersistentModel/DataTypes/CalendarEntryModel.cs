﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Molex.StackDataObjects.APIModels;
using Molex.StackDataObjects.Constants;

namespace Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel.DataTypes
{
    public class CalendarEntryModel
    {
        public DateModel StatusDate { get; set; }
        public DateRangeModel StatusDateRange { get; set; }
        public WeekNDayModel WeeknDay { get; set; }

        public CalendarEntryStatus StatusCalendar { get; set; }
    }
}
