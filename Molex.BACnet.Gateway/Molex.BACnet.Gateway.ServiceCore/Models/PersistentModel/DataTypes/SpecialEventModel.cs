﻿using Molex.StackDataObjects.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Molex.StackDataObjects.APIModels;

namespace Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel.DataTypes
{
    public class SpecialEventModel
    {
        public DateModel StatusDate { get; set; }
        public DateRangeModel StatusDateRange { get; set; }
        public WeekNDayModel WeeknDay { get; set; }
        public ObjectIdentifierModel CalendarReference { get; set; }
        public List<TimeValueModel> BacnetTimeValue { get; set; }
        public uint EventPriority { get; set; }
        public CalendarEntryStatus StatusCalendar { get; set; }

        public SpecialEventModel()
        {
            BacnetTimeValue = new List<TimeValueModel>();
        }
    }
}
