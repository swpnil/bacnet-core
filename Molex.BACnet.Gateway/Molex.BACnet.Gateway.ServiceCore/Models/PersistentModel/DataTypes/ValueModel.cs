﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel.DataTypes
{
    public class PersistentValueModel
    {
        public byte TagType { get; set; }
        public object Value { get; set; }
    }
}
