﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel.DataTypes
{
    public class DateModel
    {
        public ushort Year { get; set; }
        public byte Month { get; set; }
        public byte Day { get; set; }
        public byte WeekDay { get; set; }
    }
}
