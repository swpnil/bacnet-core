﻿using Molex.StackDataObjects.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel.DataTypes
{
    public class DevObjPropRefModel
    {
        public BacnetObjectType ObjectType { get; set; }
        public BacnetObjectType DeviceType { get; set; }
        /** Object Instance*/
        public uint ObjId { get; set; }
        /** Device Instance*/
        public uint DeviceInstance { get; set; }
        /** Device Type & its flag */
        public bool DeviceIdPresent { get; set; }
        /** Array Index & its flag */
        public bool ArrIndxPresent { get; set; }
        public uint ArrayIndex { get; set; }
        /** Property Identifier*/
        public BacnetPropertyID PropertyIdentifier { get; set; }
    }
}
