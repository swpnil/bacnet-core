﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel.DataTypes
{
    public class DateRangeModel
    {
        /** start date*/
        public DateModel StartDate { get; set; }
        /** end date */
        public DateModel EndDate { get; set; }

        public DateRangeModel()
        {
            StartDate = new DateModel();
            EndDate = new DateModel();
        }
    }
}
