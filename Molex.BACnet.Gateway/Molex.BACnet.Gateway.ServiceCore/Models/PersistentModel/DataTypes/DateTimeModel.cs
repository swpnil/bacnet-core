﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel.DataTypes
{
    public class DateTimeModel
    {
        public TimeModel Time { get; set; }
        public DateModel Date { get; set; }

        public DateTimeModel()
        {
            Time = new TimeModel();
            Date = new DateModel();
        }
    }
}
