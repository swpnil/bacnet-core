﻿using Molex.StackDataObjects.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel.DataTypes
{
    public class ChangeOfStateEventModel
    {
        public ChangeOfStateEventModel()
        {
            PropertyStates = new List<PropertyModel>();
        }

        public uint TimeDelay { get; set; }
        public List<PropertyModel> PropertyStates { get; set; }
    }

    public class PropertyModel
    {
        public BacnetPropertyStates PropertyState { get; set; }
        public BacnetApplicationTag ApplicationTag { get; set; }
        public object Value { get; set; }
    }
}
