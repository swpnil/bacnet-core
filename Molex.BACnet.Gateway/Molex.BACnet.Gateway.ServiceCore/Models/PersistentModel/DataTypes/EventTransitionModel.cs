﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel.DataTypes
{
    public class EventTransitionModel
    {
        public bool ToOffNormal { get; set; }
        public bool ToFault { get; set; }
        public bool ToNormal { get; set; }
    }
}
