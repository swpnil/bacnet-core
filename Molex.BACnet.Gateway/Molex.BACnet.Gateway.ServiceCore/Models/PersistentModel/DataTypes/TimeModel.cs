﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel.DataTypes
{
    public class TimeModel
    {
        /* Hour value */
        public byte Hour { get; set; }
        /* Minutes value*/
        public byte Min { get; set; }
        /* Second value */
        public byte Sec { get; set; }
        /* Hundredths value */
        public byte Hundredths { get; set; }
    }
}
