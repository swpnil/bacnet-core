﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              PersistentBase
///   Description:        This class used for persist common BACnet object properties
///   Author:             Prasad Joshi                 
///   Date:               06/29/17
#endregion

using Molex.BACnet.Gateway.ServiceCore.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel
{
    /// <summary>
    /// The Base class for Persist BACnet Object information
    /// </summary>
    /// <CreatedBy>Rohit Galgali</CreatedBy><TimeStamp>6/29/201711:29 AM</TimeStamp>
    public class PersistentBase
    {
        /// <summary>
        /// Gets or sets the device identifier.
        /// </summary>
        /// <value>
        /// The device identifier.
        /// </value>
        /// <CreatedBy>Rohit Galgali</CreatedBy><TimeStamp>6/29/201711:24 AM</TimeStamp>
        public uint ObjectID { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>8/23/201711:35 AM</TimeStamp>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>8/23/201711:35 AM</TimeStamp>
        public string Description { get; set; }
    }
}
