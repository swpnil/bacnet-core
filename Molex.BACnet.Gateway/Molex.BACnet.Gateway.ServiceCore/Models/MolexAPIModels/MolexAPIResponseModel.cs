﻿
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
namespace Molex.BACnet.Gateway.ServiceCore.Models.APIModels
{
    /// <summary>
    /// To get and hold molex API response
    /// </summary>
    /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/3/201811:31 AM</TimeStamp>
    public class MolexAPIResponseModel
    {
        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="MolexAPIResponseModel"/> is status.
        /// </summary>
        /// <value>
        ///   <c>true</c> if status; otherwise, <c>false</c>.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/3/201811:31 AM</TimeStamp>
        public bool status { get; set; }

        /// <summary>
        /// Gets or sets the result.
        /// </summary>
        /// <value>
        /// The result.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/3/201811:31 AM</TimeStamp>
        public Result result { get; set; }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <value>
        /// The code.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/3/201811:31 AM</TimeStamp>
        public string code { get; set; }
    }

    /// <summary>
    /// To get result data of molex API response
    /// </summary>
    /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/3/201811:31 AM</TimeStamp>
    public class Result
    {
        /// <summary>
        /// Gets or sets the error.
        /// </summary>
        /// <value>
        /// The error.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/3/201811:31 AM</TimeStamp>
        public List<Error> error { get; set; }

        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        /// <value>
        /// The data.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/20189:58 PM</TimeStamp>
        public List<JObject> data { get; set; }
    }

    /// <summary>
    /// To get errors in molex API response
    /// </summary>
    /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/3/201811:31 AM</TimeStamp>
    public class Error
    {
        /// <summary>
        /// Gets or sets the status code.
        /// </summary>
        /// <value>
        /// The status code.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/3/201811:31 AM</TimeStamp>
        public int statusCode { get; set; }

        /// <summary>
        /// Gets or sets the property information.
        /// </summary>
        /// <value>
        /// The property information.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/3/201811:32 AM</TimeStamp>
        public PropertyInfo[] propertyInfo { get; set; }

        /// <summary>
        /// Gets or sets the error MSG.
        /// </summary>
        /// <value>
        /// The error MSG.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/3/201811:32 AM</TimeStamp>
        public string errorMsg { get; set; }
    }

    /// <summary>
    /// To get failed zoneid/subscription id from molex API response
    /// </summary>
    /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/3/201811:32 AM</TimeStamp>
    public class PropertyInfo
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/3/201811:32 AM</TimeStamp>
        public string id { get; set; }


        /// <summary>
        /// Gets or sets the client identifier.
        /// </summary>
        /// <value>
        /// The client identifier.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/201810:01 PM</TimeStamp>
        public string clientID { get; set; }

        /// <summary>
        /// Gets or sets the zone identifier.
        /// </summary>
        /// <value>
        /// The zone identifier.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/3/201811:33 AM</TimeStamp>
        public string zoneId { get; set; }
    }
}
