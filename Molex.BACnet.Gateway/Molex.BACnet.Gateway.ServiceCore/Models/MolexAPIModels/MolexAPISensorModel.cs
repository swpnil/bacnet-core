﻿
namespace Molex.BACnet.Gateway.ServiceCore.Models.MolexAPIModels
{
    public class MolexAPISensorModel
    {
        public string key { get; set; }
        public string name { get; set; }
        public string amBXLocation { get; set; }
        public string gridX { get; set; }
        public string gridY { get; set; }
        public string address { get; set; }
        public string protocol { get; set; }
        public string type { get; set; }
        public string role { get; set; }
        public string id { get; set; }
    }
}
