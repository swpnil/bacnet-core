﻿
namespace Molex.BACnet.Gateway.ServiceCore.Models.MolexAPIModels
{
    /// <summary>
    /// Model to get subscription id and client id on subscription from API response
    /// </summary>
    public class MolexAPISubRespModel
    {
        /// <summary>
        /// string subscription id
        /// </summary>
        public string id { get; set; }
        /// <summary>
        /// string client id
        /// </summary>
        public string clientid { get; set; }
    }
}
