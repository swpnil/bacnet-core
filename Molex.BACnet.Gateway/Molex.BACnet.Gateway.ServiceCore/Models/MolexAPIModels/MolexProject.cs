﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              <MolexProject.cs>
///   Description:        <This file is root for reading data from project file>
///   Author:             <Rupesh Saw>                  
///   Date:               07/10/17
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models.MolexAPIModels
{
    /// <summary>
    /// This file is root for reading data from project file
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>14-07-201701:07 PM</TimeStamp>
    public class MolexProject
    {
        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="MolexProject"/> is status.
        /// </summary>
        /// <value>
        ///   <c>true</c> if status; otherwise, <c>false</c>.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>14-07-201701:07 PM</TimeStamp>
        public bool status { get; set; }

        /// <summary>
        /// Gets or sets the result.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>14-07-201701:07 PM</TimeStamp>
        public MolexResult result { get; set; }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>14-07-201701:07 PM</TimeStamp>
        public int code { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>14-07-201701:07 PM</TimeStamp>
        public string message { get; set; }
    }
}
