﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              <MolexResult.cs>
///   Description:        <This file hold the entire data for all projects details>
///   Author:             <Rupesh Saw>                  
///   Date:               07/11/17
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Molex.BACnet.Gateway.ServiceCore.Models.MolexAPIModels
{
    /// <summary>
    /// This file is root for reading data from project file
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>14-07-201701:04 PM</TimeStamp>
    public class MolexResult
    {
        /// <summary>
        /// Gets or sets the data reading from project file.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>14-07-201701:04 PM</TimeStamp>
        public List<MolexDataModel> data { get; set; }

        /// <summary>
        /// Gets or sets the error.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>14-07-201701:05 PM</TimeStamp>
        public object[] error { get; set; }
    }
}
