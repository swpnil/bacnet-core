﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Molex.BACnet.Gateway.ServiceCore.Models.MolexAPIModels
{
    public class MolexAPILightsceneModel
    {
        public string key { get; set; }
        public string name { get; set; }
        public string slot { get; set; }
        public string location { get; set; }
        public string circadianLink { get; set; }
        public object palette { get; set; }
        public string id { get; set; }
        public string[] type { get; set; }
    }
}
