﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              <MolexAPIFixtureModel.cs>
///   Description:        <This class is used to store the APIFixtureModel Data reading from project file>
///   Author:             <Rupesh Saw>                  
///   Date:               07/07/17
#endregion


namespace Molex.BACnet.Gateway.ServiceCore.Models.MolexAPIModels
{
    /// <summary>
    /// This class is used to store the APIFixtureModel Data reading from project file
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>10-07-201707:26 PM</TimeStamp>
    public class MolexAPIFixtureModel
    {
        /// <summary>
        /// Gets or sets the key.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>10-07-201707:26 PM</TimeStamp>
        public string key { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>10-07-201707:27 PM</TimeStamp>
        public string name { get; set; }

        /// <summary>
        /// Gets or sets the amBX location.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>10-07-201707:27 PM</TimeStamp>
        public string amBXLocation { get; set; }

        /// <summary>
        /// Gets or sets the grid x.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>10-07-201707:27 PM</TimeStamp>
        public string gridX { get; set; }

        /// <summary>
        /// Gets or sets the grid y.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>10-07-201707:27 PM</TimeStamp>
        public string gridY { get; set; }

        /// <summary>
        /// Gets or sets the address.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>10-07-201707:27 PM</TimeStamp>
        public string address { get; set; }

        /// <summary>
        /// Gets or sets the protocol.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>10-07-201707:28 PM</TimeStamp>
        public string protocol { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>10-07-201707:28 PM</TimeStamp>
        public string type { get; set; }

        /// <summary>
        /// Gets or sets the role.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>10-07-201707:28 PM</TimeStamp>
        public string role { get; set; }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>10-07-201707:28 PM</TimeStamp>
        public string id { get; set; }
    }
}
