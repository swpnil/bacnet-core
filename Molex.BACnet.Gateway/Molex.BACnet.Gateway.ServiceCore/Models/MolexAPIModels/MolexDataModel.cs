﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Molex.BACnet.Gateway.ServiceCore.Models.MolexAPIModels
{
    public class MolexDataModel
    {
        /// <summary>
        /// ID Value
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public string id { get; set; }

        /// <summary>
        /// Project Name
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public string name { get; set; }

        /// <summary>
        /// Building ID
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public string buildingID { get; set; }

        /// <summary>
        /// Building Name
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public string buildingName { get; set; }

        /// <summary>
        /// Location
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public string location { get; set; }

        /// <summary>
        /// Longitude
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public string longitude { get; set; }

        /// <summary>
        /// Latitude
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public string latitude { get; set; }

        /// <summary>
        /// Floor Collection
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public List<Floor> floors { get; set; }
    }

    public class Floor
    {
        /// <summary>
        /// Floor ID
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public string id { get; set; }

        /// <summary>
        /// Floor Name
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public string name { get; set; }

        /// <summary>
        /// Zone Collection
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public List<Zone> zones { get; set; }
    }

    public class Zone
    {
        /// <summary>
        /// Zone ID
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public string id { get; set; }

        /// <summary>
        /// Zone Name
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public string name { get; set; }

        /// <summary>
        /// Zone Layer
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public string layer { get; set; }

        /// <summary>
        /// Zone Type
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public string type { get; set; }

        /// <summary>
        /// Zone Location X Co-Ordinates
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public int locX { get; set; }

        /// <summary>
        /// Zone Location Y Co-Ordinates
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public int locY { get; set; }

        /// <summary>
        /// Zone Height
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public int height { get; set; }

        /// <summary>
        /// Zone Width
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public int width { get; set; }
    }


    public class LightSceneData
    {
        /// <summary>
        /// Light Scene ID
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public string id { get; set; }

        /// <summary>
        /// Light Scene Key
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public string key { get; set; }

        /// <summary>
        /// Type List
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public List<string> type { get; set; }
    }

    public class LightSceneResult
    {
        /// <summary>
        /// Data Value
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public List<LightSceneData> data { get; set; }

        /// <summary>
        /// Error 
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public List<object> error { get; set; }
    }

    public class LightScenesRootObject
    {
        /// <summary>
        /// Status
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public bool status { get; set; }

        /// <summary>
        /// Response Result
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public LightSceneResult result { get; set; }

        /// <summary>
        /// Response Code
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public int code { get; set; }

        /// <summary>
        /// Output Message
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public string message { get; set; }
    }
}
