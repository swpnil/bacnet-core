﻿
namespace Molex.BACnet.Gateway.ServiceCore.Models.MolexAPIModels
{
    /// <summary>
    /// To receibe sensor published data from API in GW
    /// </summary>
    public class MolexAPISensorSubRespModel
    {
        /// <summary>
        /// To get or set zone id
        /// </summary>
        public string zoneID { get; set; }

        /// <summary>
        /// To get or set sensor id
        /// </summary>
        public string sensorID { get; set; }

        /// <summary>
        /// To set or get sensor value
        /// </summary>
        public string value { get; set; }

        /// <summary>
        /// To set or get time stamp (sensor get value time)
        /// </summary>
        public string timestamp { get; set; }
    }

    /// <summary>
    /// To receibe sensor published data from API in GW
    /// </summary>
    public class MolexMQTTSensorSubRespModel
    {
        /// <summary>
        /// To get or set zone id
        /// </summary>
        public string zoneID { get; set; }

        /// <summary>
        /// To get or set sensor id
        /// </summary>
        public string ID { get; set; }

        /// <summary>
        /// To set or get sensor value
        /// </summary>
        public string value { get; set; }

        /// <summary>
        /// To set or get time stamp (sensor get value time)
        /// </summary>
        public string timestamp { get; set; }
    }
}
