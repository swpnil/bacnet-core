﻿using Newtonsoft.Json.Linq;

namespace Molex.BACnet.Gateway.ServiceCore.Models.MolexAPIModels
{
    /// <summary>
    /// Generic class to get api publish data
    /// </summary>
    /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/25/20182:16 PM</TimeStamp>
    public class MolexAPIPublishModel
    {
        /// <summary>
        /// Gets or sets the project identifier.
        /// </summary>
        /// <value>
        /// The project identifier.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/25/20182:17 PM</TimeStamp>
        public string projectID { get; set; }

        /// <summary>
        /// Gets or sets the zone identifier.
        /// </summary>
        /// <value>
        /// The zone identifier.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/25/20182:18 PM</TimeStamp>
        public string id { get; set; }

        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        /// <value>
        /// The data.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/25/20182:18 PM</TimeStamp>
        public JToken data { get; set; }

        /// <summary>
        /// Gets or sets the topic.
        /// </summary>
        /// <value>
        /// The topic.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/25/20182:18 PM</TimeStamp>
        public string topic { get; set; }

        /// <summary>
        /// get or set time stamp
        /// </summary>
        public string timestamp { get; set; }
    }
}
