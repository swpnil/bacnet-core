﻿using System.Collections.Generic;

namespace Molex.BACnet.Gateway.ServiceCore.Models.MolexAPIModels
{
    public class MolexAPISensorPresentDataModel
    {
        /// <summary>
        /// Gets or sets the output.
        /// </summary>
        /// <value>
        /// The out put.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>6/9/20174:58 AM</TimeStamp>
        public List<object> OutPut { get; set; }
    }
}
