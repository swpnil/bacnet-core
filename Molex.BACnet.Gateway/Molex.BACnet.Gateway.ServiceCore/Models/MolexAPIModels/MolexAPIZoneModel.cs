﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models.MolexAPIModels
{
    public class MolexAPIZoneModel
    {
        public string key { get; set; }
        public string name { get; set; }
        public string ambx_space { get; set; }
        public string layer { get; set; }
        public string type { get; set; }
        public string default_light_scene { get; set; }
        public string default_mood { get; set; }
        public string default_brightness { get; set; }
        //public MolexAPIDefaultanimatedpaletteModel defaultAnimatedPalette { get; set; }
        //public MolexAPIScheduleModel schedule { get; set; }
        // public MolexAPIActivationModel activation { get; set; }
        // public MolexAPISensorbrainModel sensorBrain { get; set; }
        public Maps maps { get; set; }
        public string[] availableBuildings { get; set; }
        public string[] availableFloors { get; set; }
        public string[] availableAreas { get; set; }
        public LightScene[] availableLightScenes { get; set; }
        public string[] activeSensors { get; set; }
        public string[] activeFixtures { get; set; }
        public string id { get; set; }
    }
    public class LightScene
    {
        public string id { get; set; }
        public string key { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <CreatedBy>Rohit Galgali</CreatedBy>
    /// <TimeStamp>9/27/20179:27 PM</TimeStamp>
    public class Maps
    {
        /// <summary>
        /// Gets or sets the map 1.
        /// </summary>
        /// <value>
        /// The map 1.
        /// </value>
        /// <CreatedBy>Rohit Galgali</CreatedBy>
        /// <TimeStamp>9/27/20179:27 PM</TimeStamp>
        public MapData map_1 { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <CreatedBy>Rohit Galgali</CreatedBy>
    /// <TimeStamp>9/27/20179:28 PM</TimeStamp>
    public class MapData
    {
        public string building { get; set; }
        public string floor { get; set; }
        public int locX { get; set; }
        public int locY { get; set; }
        public int width { get; set; }
        public int height { get; set; }
        public int orientation { get; set; }
    }
}
