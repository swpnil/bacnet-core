﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              <MolexControllerModel.cs>
///   Description:        <This class will holds the Controller Model details>
///   Author:             <Rupesh Saw>                  
///   Date:               07/07/17
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models.MolexAPIModels
{
    /// <summary>
    /// This class will holds the Controller Model details
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>07-07-201704:52 PM</TimeStamp>
    public class MolexAPIControllerModel
    {
        /// <summary>
        /// Gets or sets the key.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>10-07-201707:23 PM</TimeStamp>
        public string key { get; set; }

        /// <summary>
        /// Gets or sets the address.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>10-07-201707:23 PM</TimeStamp>
        public string address { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>10-07-201707:23 PM</TimeStamp>
        public string name { get; set; }

        /// <summary>
        /// Gets or sets the location.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>10-07-201707:24 PM</TimeStamp>
        public string location { get; set; }

        /// <summary>
        /// Gets or sets the configuration.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>10-07-201707:24 PM</TimeStamp>
        public string config { get; set; }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>10-07-201707:24 PM</TimeStamp>
        public string id { get; set; }

        /// <summary>
        /// Gets or sets the light scenes.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>10-07-201707:24 PM</TimeStamp>
        public MolexAPILightsceneModel[] lightScenes { get; set; }

        /// <summary>
        /// Gets or sets the spaces.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>10-07-201707:24 PM</TimeStamp>
        public MolexAPIZoneModel[] spaces { get; set; }

        /// <summary>
        /// Gets or sets the buildings.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>10-07-201707:24 PM</TimeStamp>
        public MolexAPIBuildingModel buildings { get; set; }
    }
}
