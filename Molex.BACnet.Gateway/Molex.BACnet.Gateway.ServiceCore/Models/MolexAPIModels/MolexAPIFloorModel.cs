﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              <MolexAPIFloorModel.cs>
///   Description:        <This will hold floor data information reading form project file>
///   Author:             <Rupesh Saw>                  
///   Date:               12/07/17
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models.MolexAPIModels
{
    /// <summary>
    /// This will hold floor data information reading form project file
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>14-07-201701:00 PM</TimeStamp>
    public class MolexAPIFloorModel
    {
        /// <summary>
        /// Gets or sets the key.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>14-07-201701:01 PM</TimeStamp>
        public string key { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>14-07-201701:01 PM</TimeStamp>
        public string name { get; set; }

        /// <summary>
        /// Gets or sets the floor plan.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>14-07-201701:01 PM</TimeStamp>
        public string floorPlan { get; set; }

        /// <summary>
        /// Gets or sets the grid w.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>14-07-201701:01 PM</TimeStamp>
        public int gridW { get; set; }

        /// <summary>
        /// Gets or sets the grid h.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>14-07-201701:01 PM</TimeStamp>
        public int gridH { get; set; }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>14-07-201701:01 PM</TimeStamp>
        public string id { get; set; }

        /// <summary>
        /// Gets or sets the fixtures.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>14-07-201701:01 PM</TimeStamp>
        public MolexAPIFixtureModel[] fixtures { get; set; }

        /// <summary>
        /// Gets or sets the sensors.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>14-07-201701:01 PM</TimeStamp>
        public MolexAPISensorModel[] sensors { get; set; }
    }
}
