﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              <MolexAPIBuildingModel.cs>
///   Description:        <This class is used for storing APIBuildingModel fetching from project file>
///   Author:             <Rupesh Saw>                  
///   Date:               07/07/17
#endregion


using System.Collections.Generic;
namespace Molex.BACnet.Gateway.ServiceCore.Models.MolexAPIModels
{
    /// <summary>
    /// This class is used for storing APIBuildingModel fetching from project file
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>10-07-201710:53 AM</TimeStamp>
    public class MolexAPIBuildingModel
    {
        /// <summary>
        /// Gets or sets the key.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>10-07-201707:21 PM</TimeStamp>
        public string key { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>10-07-201707:21 PM</TimeStamp>
        public string name { get; set; }

        /// <summary>
        /// Gets or sets the location.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>10-07-201707:22 PM</TimeStamp>
        public string location { get; set; }

        /// <summary>
        /// Gets or sets the utilisation.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>10-07-201707:22 PM</TimeStamp>
        public int utilisation { get; set; }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>10-07-201707:22 PM</TimeStamp>
        public string id { get; set; }

        /// <summary>
        /// Gets or sets the floors.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>10-07-201707:22 PM</TimeStamp>
        public List<Floor> floors { get; set; }

    }

    public class Fixture
    {
        /// <summary>
        /// Fixture Grid X 
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public string gridX { get; set; }

        /// <summary>
        /// Fixture Grid Y
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public string gridY { get; set; }

        /// <summary>
        /// Fixture Location
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public string location { get; set; }

        /// <summary>
        /// Fixture Name
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public string name { get; set; }

        /// <summary>
        /// Fixture Role
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public string role { get; set; }

        /// <summary>
        /// Fixture Type
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public string type { get; set; }

        /// <summary>
        /// Fixture Floor ID
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public string floorID { get; set; }

        /// <summary>
        /// Fixture ID
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public string zoneID { get; set; }

        /// <summary>
        /// Zone ID
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public string id { get; set; }
    }

    public class Sensor
    {
        /// <summary>
        /// Sensor Grid X
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public string gridX { get; set; }

        /// <summary>
        /// Sensor Grid Y
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public string gridY { get; set; }

        /// <summary>
        /// Sensor Location
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public string location { get; set; }

        /// <summary>
        /// Sensor Name
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public string name { get; set; }

        /// <summary>
        /// Sensor Role
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public string role { get; set; }

        /// <summary>
        /// Sensor Type
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public string type { get; set; }

        /// <summary>
        /// Sensor Floor ID
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public string floorID { get; set; }

        /// <summary>
        /// Sensor Zone ID
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public string zoneID { get; set; }

        /// <summary>
        /// Sensor ID
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public string id { get; set; }
    }

    public class Beacon
    {
        /// <summary>
        /// Floor ID
        /// </summary>
        public string floorID { get; set; }

        /// <summary>
        /// Beacon Grid X Location Coordinate
        /// </summary>
        public string gridX { get; set; }

        /// <summary>
        /// Beacon Grid Y Location Coordinate
        /// </summary>
        public string gridY { get; set; }

        /// <summary>
        /// Location
        /// </summary>
        public string location { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// Role
        /// </summary>
        public string role { get; set; }

        /// <summary>
        /// Type
        /// </summary>
        public string type { get; set; }

        /// <summary>
        /// Zone ID
        /// </summary>
        public string zoneID { get; set; }

        /// <summary>
        /// ID
        /// </summary>
        public string id { get; set; }
    }

    public class Blind
    {
        /// <summary>
        /// Floor ID
        /// </summary>
        public string floorID { get; set; }

        /// <summary>
        /// Grid X Coordinate location for Blind
        /// </summary>
        public string gridX { get; set; }

        /// <summary>
        /// Grid Y Coordinate location for Blind
        /// </summary>
        public string gridY { get; set; }

        /// <summary>
        /// Location
        /// </summary>
        public string location { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// Role
        /// </summary>
        public string role { get; set; }

        /// <summary>
        /// Type
        /// </summary>
        public string type { get; set; }

        /// <summary>
        /// ZoneID
        /// </summary>
        public string zoneID { get; set; }

        /// <summary>
        /// ID
        /// </summary>
        public string id { get; set; }
    }

    public class Datum
    {
        public List<Fixture> fixtures { get; set; }
        public List<Beacon> beacons { get; set; }
        public List<Blind> blinds { get; set; }
        public List<Sensor> sensors { get; set; }
    }

    public class Result
    {
        /// <summary>
        /// ResultData
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public List<Datum> data { get; set; }

        /// <summary>
        /// Error
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public List<object> error { get; set; }
    }

    public class ProjectDevicesRootObject
    {
        /// <summary>
        /// Status
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public bool status { get; set; }

        /// <summary>
        /// Result
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public Result result { get; set; }

        /// <summary>
        /// Code
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public int code { get; set; }

        /// <summary>
        /// Message
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public string message { get; set; }
    }

    public class ZoneLightSceneDatum
    {
        /// <summary>
        /// Light Scene Key
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public string key { get; set; }

        /// <summary>
        /// Light Scene Name
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public string name { get; set; }

        /// <summary>
        /// Light Scene Role
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public string role { get; set; }

        /// <summary>
        /// Light Scene Type
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public string type { get; set; }

        /// <summary>
        /// Light Scene ID
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public string id { get; set; }
    }

    public class ZoneLightSceneResult
    {
        /// <summary>
        /// Data
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public List<ZoneLightSceneDatum> data { get; set; }

        /// <summary>
        /// Error
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public List<object> error { get; set; }
    }

    public class ZoneLightSceneRootObject
    {
        /// <summary>
        /// Status
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public bool status { get; set; }

        /// <summary>
        /// Result
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public ZoneLightSceneResult result { get; set; }

        /// <summary>
        /// Code
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public int code { get; set; }

        /// <summary>
        /// Message
        /// </summary>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        public string message { get; set; }
    }
}
