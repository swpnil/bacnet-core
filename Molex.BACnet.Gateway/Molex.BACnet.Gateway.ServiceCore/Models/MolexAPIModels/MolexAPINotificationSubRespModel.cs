﻿
namespace Molex.BACnet.Gateway.ServiceCore.Models.MolexAPIModels
{
    /// <summary>
    /// Model to get published notification data by Molex API
    /// </summary>
    /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/25/20181:14 PM</TimeStamp>
    public class MolexAPINotificationSubRespModel
    {
        /// <summary>
        /// Gets or sets the type of the resource.
        /// </summary>
        /// <value>
        /// The type of the resource.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/25/20181:14 PM</TimeStamp>
        public string resourceType { get; set; }

        /// <summary>
        /// Gets or sets the type of data in packet.
        /// </summary>
        /// <value>
        /// The type of data in packet.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>8/10/20181:14 PM</TimeStamp>
        public string type { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/25/20181:14 PM</TimeStamp>
        public string additionalInfo { get; set; }
    }
}
