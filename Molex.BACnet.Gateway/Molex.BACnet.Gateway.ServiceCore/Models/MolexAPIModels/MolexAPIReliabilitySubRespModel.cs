﻿namespace Molex.BACnet.Gateway.ServiceCore.Models.MolexAPIModels
{
    /// <summary>
    /// Model to catch API published reliability data
    /// </summary>
    /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/20183:17 PM</TimeStamp>
    public class MolexAPIReliabilitySubRespModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MolexAPIReliabilityResponseModel"/> class.
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/20183:17 PM</TimeStamp>
        public MolexAPIReliabilitySubRespModel()
        {
            resources = new MolexAPIResource();
        }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/20183:17 PM</TimeStamp>
        public string id { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/20183:17 PM</TimeStamp>
        public string value { get; set; }

        /// <summary>
        /// Gets or sets the resources.
        /// </summary>
        /// <value>
        /// The resources.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/20183:17 PM</TimeStamp>
        public MolexAPIResource resources { get; set; }

        public class MolexAPIResource
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="MolexAPIResource"/> class.
            /// </summary>
            /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/20183:17 PM</TimeStamp>
            public MolexAPIResource()
            {
                sensor = new MolexAPISensor();
                fixture = new MolexAPIFixture();
                beacon = new MolexAPIBeacon();
                blind = new MolexAPIBlind();
            }

            /// <summary>
            /// Gets or sets the sensor.
            /// </summary>
            /// <value>
            /// The sensor.
            /// </value>
            /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/20183:17 PM</TimeStamp>
            public MolexAPISensor sensor { get; set; }

            /// <summary>
            /// Gets or sets the fixture.
            /// </summary>
            /// <value>
            /// The fixture.
            /// </value>
            /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/20183:18 PM</TimeStamp>
            public MolexAPIFixture fixture { get; set; }


            /// <summary>
            /// Gets or sets the beacon.
            /// </summary>
            /// <value>
            /// The fixture.
            /// </value>
            /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/20183:18 PM</TimeStamp>
            public MolexAPIBeacon beacon{ get; set; }

            /// <summary>
            /// Gets or sets the beacon.
            /// </summary>
            /// <value>
            /// The fixture.
            /// </value>
            /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/20183:18 PM</TimeStamp>
            public MolexAPIBlind blind { get; set; }

            /// <summary>
            /// To get sensor data from API published reliability data
            /// </summary>
            /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/20183:18 PM</TimeStamp>
            public class MolexAPISensor
            {
                /// <summary>
                /// Gets or sets the value.
                /// </summary>
                /// <value>
                /// The value.
                /// </value>
                /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/20183:33 PM</TimeStamp>
                public string value { get; set; }

                /// <summary>
                /// Gets or sets the type of the sensor.
                /// </summary>
                /// <value>
                /// The type of the sensor.
                /// </value>
                /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/20183:33 PM</TimeStamp>
                public object sensorType { get; set; }

                /// <summary>
                /// Initializes a new instance of the <see cref="MolexAPISensor"/> class.
                /// </summary>
                /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/20183:33 PM</TimeStamp>
                public MolexAPISensor()
                {
                    sensorType = new object();
                }
            }

            /// <summary>
            /// To get fixture data from API published reliability data
            /// </summary>
            /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/20183:18 PM</TimeStamp>
            public class MolexAPIFixture
            {
                /// <summary>
                /// Gets or sets the value.
                /// </summary>
                /// <value>
                /// The value.
                /// </value>
                /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/20183:34 PM</TimeStamp>
                public string value { get; set; }

                /// <summary>
                /// Gets or sets the type of the fixture.
                /// </summary>
                /// <value>
                /// The type of the fixture.
                /// </value>
                /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/20183:34 PM</TimeStamp>
                public object fixtureType { get; set; }

                /// <summary>
                /// Initializes a new instance of the <see cref="MolexAPIFixture"/> class.
                /// </summary>
                /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/20183:34 PM</TimeStamp>
                public MolexAPIFixture()
                {
                    fixtureType = new object();
                }
            }

            /// <summary>
            /// To get fixture data from API published reliability data
            /// </summary>
            /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/20183:18 PM</TimeStamp>
            public class MolexAPIBeacon
            {
                /// <summary>
                /// Gets or sets the value.
                /// </summary>
                /// <value>
                /// The value.
                /// </value>
                /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/20183:34 PM</TimeStamp>
                public string value { get; set; }

                /// <summary>
                /// Gets or sets the type of the fixture.
                /// </summary>
                /// <value>
                /// The type of the fixture.
                /// </value>
                /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/20183:34 PM</TimeStamp>
                public object beaconType { get; set; }

                /// <summary>
                /// Initializes a new instance of the <see cref="MolexAPIFixture"/> class.
                /// </summary>
                /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/20183:34 PM</TimeStamp>
                public MolexAPIBeacon()
                {
                    beaconType = new object();
                }
            }

            /// <summary>
            /// To get fixture data from API published reliability data
            /// </summary>
            /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/20183:18 PM</TimeStamp>
            public class MolexAPIBlind
            {
                /// <summary>
                /// Gets or sets the value.
                /// </summary>
                /// <value>
                /// The value.
                /// </value>
                /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/20183:34 PM</TimeStamp>
                public string value { get; set; }

                /// <summary>
                /// Gets or sets the type of the fixture.
                /// </summary>
                /// <value>
                /// The type of the fixture.
                /// </value>
                /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/20183:34 PM</TimeStamp>
                public object blindType { get; set; }

                /// <summary>
                /// Initializes a new instance of the <see cref="MolexAPIFixture"/> class.
                /// </summary>
                /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/20183:34 PM</TimeStamp>
                public MolexAPIBlind()
                {
                    blindType = new object();
                }
            }
        }
    }






}
