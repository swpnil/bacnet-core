﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models
{
    public class ReliabilityDataModel
    {
        public ReliabilityDataModel()
        {
            Resources = new ResourceModel();
        }
        public string ID { get; set; }
        public string Value { get; set; }
        public ResourceModel Resources { get; set; }

        public class ResourceModel
        {
            public ResourceModel()
            {
                SensorModel = new Sensor();
                FixtureModel = new Fixture();
                BeaconModel = new Beacon();
                BlindModel = new Blind();
            }
            public Sensor SensorModel { get; set; }
            public Fixture FixtureModel { get; set; }
            public Beacon BeaconModel { get; set; }
            public Blind BlindModel { get; set; }

            public class Blind
            {
                public string Value { get; set; }
                public object BlindType { get; set; }

                public Blind()
                {
                    BlindType = new object();
                }
            }
            public class Beacon
            {
                public string Value { get; set; }
                public object BeaconType { get; set; }

                public Beacon()
                {
                    BeaconType = new object();
                }
            }
            public class Sensor
            {
                public string Value { get; set; }
                public object SensorType { get; set; }

                public Sensor()
                {
                    SensorType = new object();
                }
            }
            public class Fixture
            {
                public string Value { get; set; }
                public object FixtureType { get; set; }
                public Fixture()
                {
                    FixtureType = new object();
                }
            }
        }
    }
}
