﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models
{
    internal class BlindZoneLevelModel: APIUpdateZoneModel
    {
       [JsonProperty(NullValueHandling=NullValueHandling.Ignore)]
        public All all { get; set; }

       [JsonProperty(NullValueHandling=NullValueHandling.Ignore)]
        public N n { get; set; }

       [JsonProperty(NullValueHandling=NullValueHandling.Ignore)]
        public Ne ne { get; set; }

       [JsonProperty(NullValueHandling=NullValueHandling.Ignore)]
        public E e { get; set; }

       [JsonProperty(NullValueHandling=NullValueHandling.Ignore)]
        public Se se { get; set; }

       [JsonProperty(NullValueHandling=NullValueHandling.Ignore)]
        public S s { get; set; }
        
       [JsonProperty(NullValueHandling=NullValueHandling.Ignore)]
        public Sw sw { get; set; }

       [JsonProperty(NullValueHandling=NullValueHandling.Ignore)]
        public W w { get; set; }

       [JsonProperty(NullValueHandling=NullValueHandling.Ignore)]
        public Nw nw { get; set; }

       [JsonProperty(NullValueHandling=NullValueHandling.Ignore)]
        public C c { get; set; }

        public BlindZoneLevelModel()
        {
            
        }
    }

    public class All
    {
        public int level { get; set; }
    }

    public class N
    {
        public int level { get; set; }
    }

    public class Ne
    {
        public int level { get; set; }
    }

    public class E
    {
        public int level { get; set; }
    }

    public class Se
    {
        public int level { get; set; }
    }

    public class S
    {
        public int level { get; set; }
    }

    public class Sw
    {
        public int level { get; set; }
    }

    public class W
    {
        public int level { get; set; }
    }

    public class Nw
    {
        public int level { get; set; }
    }

    public class C
    {
        public int level { get; set; }
    }

}
