﻿using Molex.StackDataObjects.Response.AutoResponses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models
{
    internal class ScheduleRetryModel
    {
        public bool IsHttpResponseSuccess { get; set; }

        public WritePropertyNotificationResponse WritePropertyAutoResponse { get; set; }
    }
}
