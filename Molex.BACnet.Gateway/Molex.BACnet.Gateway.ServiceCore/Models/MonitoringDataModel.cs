﻿using Newtonsoft.Json.Linq;

namespace Molex.BACnet.Gateway.ServiceCore.Models
{
    /// <summary>
    /// Model for receive pub sub data
    /// </summary>
    /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>3/15/201811:48 AM</TimeStamp>
    internal class MonitoringDataModel
    {
        public string ProjectID { get; set; }
        public string ZoneID { get; set; }
        public JToken Data { get; set; }
        public string Type { get; set; }
        public string NotificationType { get; set; }
        public string Value { get; set; }
    }

}
