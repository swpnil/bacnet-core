﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              <ErrorModel.cs>
///   Description:        <This class will provide templete for Error Model>
///   Author:             Rupesh Saw                  
///   Date:               05/22/17
///---------------------------------------------------------------------------------------------
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models
{
    /// <summary>
    /// This class will provide templete for Error Model
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:37 PM</TimeStamp>
    public class ErrorModel
    {
        #region Public Properties
        /// <summary>
        /// Gets or sets the error code.
        /// </summary>
        /// <value>
        /// The error code.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201706:44 PM</TimeStamp>
        public int ErrorCode { get; set; }

        /// <summary>
        /// Gets or sets the error category.
        /// </summary>
        /// <value>
        /// The error category.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201706:44 PM</TimeStamp>
        public int ErrorCategory { get; set; }

        /// <summary>
        /// Gets or sets the optional message.
        /// </summary>
        /// <value>
        /// The optional message.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201706:44 PM</TimeStamp>
        public string OptionalMessage { get; set; }

        #endregion
    }
}
