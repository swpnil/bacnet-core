﻿
namespace Molex.BACnet.Gateway.ServiceCore.Models
{
    /// <summary>
    /// TimeSyncZoneModel for time control
    /// </summary>
    /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>9/12/20172:06 PM</TimeStamp>
    /// <seealso cref="Molex.BACnet.Gateway.ServiceCore.Models.APIUpdateZoneModel" />
    internal class TimeSyncModel : APIUpdateZoneModel
    {
        /// <summary>
        /// Gets or sets the utctimestamp.
        /// </summary>
        /// <value>
        /// The utctimestamp.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>9/12/20172:06 PM</TimeStamp>
        public string utctimestamp { get; set; }
    }
}
