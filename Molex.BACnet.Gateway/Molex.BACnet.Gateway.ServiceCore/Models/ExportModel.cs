﻿
namespace Molex.BACnet.Gateway.ServiceCore.Models
{
    public class ExportModel
    {
        public string Site { get; set; }
        public string Room { get; set; }
        public string Leaf { get; set; }
        public string EquipmentName { get; set; }
        public string EquipmentFQR { get; set; }
        public string Served { get; set; }
        public string ControllerPart { get; set; }
        public string EngineName { get; set; }
        public string Trunk { get; set; }
        public string ControllerHostName { get; set; }
        public string JCIMACAddress { get; set; }
        public string IPControllerNumber { get; set; }
        public string ZIGBEEPANOffset { get; set; }
        public string Instance { get; set; }
        public string DHCPEnabled { get; set; }
        public string IPAddress { get; set; }
        public string SubnetMask { get; set; }
        public string IPRouter { get; set; }
        public string ETH1 { get; set; }
        public string ETH2 { get; set; }
        public string EquipmentDefinitionName { get; set; }
        public string ControllerTemplateName { get; set; }
        public string Parameter { get; set; }

        //New Parameters 
        public string Parameter1 { get; set; }
        public string Parameter2 { get; set; }
        public string Parameter3 { get; set; }
        public string Lightlevel { get; set; }
        public string Shedinglevel { get; set; }
        public string Light_ON_OFF { get; set; }
        public string Beacon_ON_OFF { get; set; }
        public string ALS { get; set; }
        public string AQ { get; set; }
        public string Presence { get; set; }
        public string ColorTemp { get; set; }
        public string Power { get; set; }
        public string Temperature { get; set; }
        public string Humidity { get; set; }
        public string lightscence { get; set; }
        public string Mood { get; set; }
        public string Target_Lux { get; set; }
        public string Timeout { get; set; }
        public string fadeout { get; set; }
        public string Beaconcolor { get; set; }
    }
}
