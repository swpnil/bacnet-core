﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              <PresRateZoneModel.cs>
///   Description:        <This class will hold PresRate value for zone>
///   Author:             <Rupesh Saw>                  
///   Date:               MM/DD/YY
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models
{
    /// <summary>
    /// This classs hold PresRate value for zone
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>05-07-201712:58 PM</TimeStamp>
    /// <seealso cref="Molex.BACnet.Gateway.ServiceCore.Models.APIUpdateZoneModel" />
    class PresRateZoneModel : APIUpdateZoneModel
    {
        /// <summary>
        /// Gets or sets the pres rate.
        /// </summary>
        /// <value>
        /// The pres rate.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>05-07-201710:22 AM</TimeStamp>
        public uint presrate { get; set; }
    }
}
