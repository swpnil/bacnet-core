﻿using Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models.ObjectMap
{
    internal class MappingData
    {
        public Dictionary<string, EntityObject> CachedObjectDetails { get; set; }
        public Dictionary<string, PersistedModels> UserBACnetObjects{get; set;}
    }
}
