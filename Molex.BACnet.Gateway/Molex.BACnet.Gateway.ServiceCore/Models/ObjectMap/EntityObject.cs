﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              EntityObject
///   Description:        <Description>
///   Author:             Rohit galgali                 
///   Date:               MM/DD/YY
#endregion

using Molex.BACnet.Gateway.ServiceCore.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models.ObjectMap
{
    /// <summary>
    /// This class is used as a model for cached data in Mapping File.
    /// </summary>
    /// <CreatedBy>hp</CreatedBy>
    /// <TimeStamp>6/6/20175:12 PM</TimeStamp>
    public class EntityObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EntityObject"/> class.
        /// </summary>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>6/9/20174:09 PM</TimeStamp>
        public EntityObject()
        {
            BACnetEntityMap = new Dictionary<string, BACnetEntity>();
        }

        /// <summary>
        /// Gets or sets the object map. This dicionary holds mapping of 
        /// BACnet Entity associated with the Entity key. For eg. AV, SV, MSV is associated with Building_1.
        /// </summary>
        /// <value>
        /// The object map.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>6/6/20173:04 PM</TimeStamp>
        public Dictionary<string, BACnetEntity> BACnetEntityMap { get; set; }

        /// <summary>
        /// Gets or sets the type of the entity.
        /// </summary>
        /// <value>
        /// The type of the entity.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>6/6/20174:56 PM</TimeStamp>
        public AppConstants.EntityTypes EntityType { get; set; }

        /// <summary>
        /// Gets or sets the name from design tool.
        /// </summary>
        /// <value>
        /// The name of the dt.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>8/30/20172:31 PM</TimeStamp>
        public string DTName { get; set; }

        /// <summary>
        /// Gets or sets the device identifier.
        /// </summary>
        /// <value>
        /// The device identifier.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>6/6/20174:56 PM</TimeStamp>
        public int DeviceId { get; set; }
    }
}
