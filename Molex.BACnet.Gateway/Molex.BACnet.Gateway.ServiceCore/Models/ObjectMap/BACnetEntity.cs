﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              BACnetEntity
///   Description:        <Description>
///   Author:             Rohit Galgali                
///   Date:               MM/DD/YY
#endregion
      

using Molex.BACnet.Gateway.ServiceCore.Constants;
using Molex.StackDataObjects.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models.ObjectMap
{
    /// <summary>
    /// This class is subset of Entity object used to store the Mapping data.
    /// </summary>
    /// <CreatedBy>hp</CreatedBy>
    /// <TimeStamp>6/6/20175:12 PM</TimeStamp>
    public class BACnetEntity
    {
        #region public properties

        /// <summary>
        /// Gets or sets the object identifier. This Id is associated with AV, MSV, SV
        /// </summary>
        /// <value>
        /// The object identifier.
        /// </value>
        /// <CreatedBy>Rohit Galgali</CreatedBy><TimeStamp>25-05-201706:58 PM</TimeStamp>
        public uint ObjectID { get; set; }

        /// <summary>
        /// Gets or sets the type of the object.
        /// </summary>
        /// <value>
        /// The type of the object. For eg: AV_Brightness etc.
        /// </value>
        /// <CreatedBy>Rohit Galgali</CreatedBy><TimeStamp>25-05-201706:58 PM</TimeStamp>
        public string ObjectType { get; set; }

        /// <summary>
        /// Gets or sets the name of the object.
        /// </summary>
        /// <value>
        /// The name of the object.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>6/28/201711:42 AM</TimeStamp>
        public string ObjectName { get; set; }

        /// <summary>
        /// Gets or sets the object description.
        /// </summary>
        /// <value>
        /// The object description.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>6/28/201711:42 AM</TimeStamp>
        public string ObjectDescription { get; set; }
        
        #endregion
    }
}
