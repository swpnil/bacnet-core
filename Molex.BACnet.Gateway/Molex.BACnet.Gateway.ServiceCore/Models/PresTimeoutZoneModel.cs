﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              <PresTimeoutZoneModel.cs>
///   Description:        <This class hold PresTimeout value for Zone>
///   Author:             <Rupesh Saw>                  
///   Date:               MM/DD/YY
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models
{
    /// <summary>
    /// This class hold PresTimeout value for Zone
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>05-07-201712:59 PM</TimeStamp>
    /// <seealso cref="Molex.BACnet.Gateway.ServiceCore.Models.APIUpdateZoneModel" />
    public class PresTimeoutZoneModel : APIUpdateZoneModel
    {
        /// <summary>
        /// Gets or sets the pres timeout.
        /// </summary>
        /// <value>
        /// The pres timeout.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>05-07-201710:22 AM</TimeStamp>
        public uint prestimeout { get; set; }
    }
}
