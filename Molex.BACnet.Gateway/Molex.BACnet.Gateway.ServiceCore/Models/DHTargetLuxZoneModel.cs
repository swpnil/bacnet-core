﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              <PresentIntergerValueZoneModel.cs>
///   Description:        <This class is used for update presentintergervalue>
///   Author:             <Rupesh Saw>                  
///   Date:               07/04/17
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models
{
    /// <summary>
    /// This class hold DHTargetLux value for Zone
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>05-07-201712:58 PM</TimeStamp>
    /// <seealso cref="Molex.BACnet.Gateway.ServiceCore.Models.APIUpdateZoneModel" />
    public class DHTargetLuxZoneModel : APIUpdateZoneModel
    {
        /// <summary>
        /// Gets or sets the Present Integer Value.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>04-07-201706:56 PM</TimeStamp>
        public uint lux { get; set; }


    }
}
