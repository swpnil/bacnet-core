﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              LightFixtureZoneModel
///   Description:        This class used for update light fixture brightness
///   Author:             Prasad Joshi                  
///   Date:               06/05/17
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models
{
    /// <summary>
    /// This model is used for maintaining brightness value in Zone Level 
    /// </summary>
    /// <CreatedBy>Prasad Joshi</CreatedBy><TimeStamp>15-06-201712:32 PM</TimeStamp>
    /// <seealso cref="Molex.BACnet.Gateway.ServiceCore.Models.APIUpdateZoneModel" />
    internal class LightFixtureZoneModel : APIUpdateZoneModel
    {
        public bool status { get; set; }
        public LightFixtureZoneModelResult result { get; set; }
        public int code { get; set; }
        public string message { get; set; }
    }

    public class LightFixtureZoneModelDatum
    {
        public string bid { get; set; }
        public string fid { get; set; }
        public float value { get; set; }
    }

    public class LightFixtureZoneModelResult
    {
        public List<LightFixtureZoneModelDatum> data { get; set; }
        public List<object> error { get; set; }
    }

}
