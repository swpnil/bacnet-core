﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models
{
    public class SensorStat
    {
        public string total { get; set; }
        public string average { get; set; }
    }
}
