﻿using Molex.BACnet.Gateway.ServiceCore.Constants;
using Molex.BACnet.Gateway.ServiceCore.Repository;
namespace Molex.BACnet.Gateway.ServiceCore.Models.SubsriberModels
{
    /// <summary>
    /// Model for subscription
    /// </summary>
    /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/20181:07 PM</TimeStamp>
    public class SubscriptionModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SubscriptionModel"/> class.
        /// </summary>
        /// <param name="topic">The topic.</param>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/20181:08 PM</TimeStamp>
        public SubscriptionModel(string topic)
        {
            this.topic = topic;
        }

        /// <summary>
        /// Gets or sets the topic.
        /// </summary>
        /// <value>
        /// The topic.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/20181:08 PM</TimeStamp>
        public string topic { get; set; }

        /// <summary>
        /// Gets the type of the resource.
        /// </summary>
        /// <value>
        /// The type of the resource.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/20181:08 PM</TimeStamp>
        public string resourceType { get { return "all"; } }

        /// <summary>
        /// Gets the project identifier.
        /// </summary>
        /// <value>
        /// The project identifier.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/20181:08 PM</TimeStamp>
        public string projectID { get { return AppCoreRepo.Instance.BACnetConfigurationData.MolexProjectID; } }

        /// <summary>
        /// Gets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/20181:08 PM</TimeStamp>
        public string type { get { return "REST"; } }

        /// <summary>
        /// Gets or sets the URL.
        /// </summary>
        /// <value>
        /// The URL.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/20181:08 PM</TimeStamp>
        public string url
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the username.
        /// </summary>
        /// <value>
        /// The username.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/20181:08 PM</TimeStamp>
        public string username { get { return AppCoreRepo.Instance.BACnetConfigurationData.UserName; } }

        /// <summary>
        /// Gets the password.
        /// </summary>
        /// <value>
        /// The password.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/20181:08 PM</TimeStamp>
        public string password { get { return AppCoreRepo.Instance.BACnetConfigurationData.Password; } }
    }
}
