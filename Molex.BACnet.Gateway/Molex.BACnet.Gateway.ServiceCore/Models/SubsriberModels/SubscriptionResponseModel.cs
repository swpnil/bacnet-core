﻿
namespace Molex.BACnet.Gateway.ServiceCore.Models.SubsriberModels
{
    class SubscriptionResponseModel
    {
        /// <summary>
        /// clinetid
        /// </summary>
        public string ClientID { get; set; }

        /// <summary>
        /// subscription id
        /// </summary>
        public string SubscriptionID { get; set; }
    }
}
