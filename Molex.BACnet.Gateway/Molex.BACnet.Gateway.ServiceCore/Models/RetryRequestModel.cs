﻿using Molex.BACnet.Gateway.ServiceCore.Models.Entity;
using Molex.StackDataObjects.APIModels;
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Response.AutoResponses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Molex.BACnet.Gateway.ServiceCore.Models
{
    internal class RetryRequestModel
    {
        public int CurrentCount { get; set; }

        public WritePropertyNotificationResponse WritePropertyAutoResponse { get; set; }
    }
}
