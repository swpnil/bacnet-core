﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              LightSceneZoneModel
///   Description:        This class is used for update lightscene
///   Author:             Prasad Joshi                 
///   Date:               06/05/17
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models
{
    /// <summary>
    /// This model is used for maintaining lightscene value in Zone Level 
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>15-06-201712:34 PM</TimeStamp>
    /// <seealso cref="Molex.BACnet.Gateway.ServiceCore.Models.APIUpdateZoneModel" />
    internal class LightSceneZoneModel : APIUpdateZoneModel
    {

        /// <summary>
        /// Gets or sets the light scene.
        /// </summary>
        /// <value>
        /// The light scene.
        /// </value>
        /// <CreatedBy>prasad.joshi</CreatedBy><TimeStamp>6/2/20179:25 AM</TimeStamp>
        public string lightscene { get; set; }
    }
}
