﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models
{
    public class APIMolexParameterModel
    {
        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        /// <value>
        /// The name of the user.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>8/9/20177:10 PM</TimeStamp>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>
        /// The password.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>8/9/20177:10 PM</TimeStamp>
        public string Password { get; set; }
    }
}
