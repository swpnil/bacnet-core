﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              BACnetConfigurationModel
///   Description:        Entity Model for getting BACnet Configuration Data
///   Author:             Nazneen Zahid                  
///   Date:               05/24/17
///---------------------------------------------------------------------------------------------
#endregion


using System.Collections.Generic;

namespace Molex.BACnet.Gateway.ServiceCore.Models
{
    /// <summary>
    /// This entity model is used to GET or SET BACnet Configuration data
    /// </summary>
    /// <CreatedBy>Nazneen Zahid</CreatedBy><TimeStamp>25-05-201705:36 PM</TimeStamp>
    public class BACnetConfigurationModel
    {
        /// <summary>
        /// Gets or sets the controller identifier.
        /// </summary>
        /// <value>
        /// The controller identifier.
        /// </value>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>29-05-201714:34</TimeStamp>"      
        public uint ControllerId { get; set; }

        /// <summary>
        /// Gets Or Sets the Gateway Communication Module
        /// </summary>
        public bool IsMQTTPublishSelected { get; set; }

        #region BACnet device object properties

        /// <summary>
        /// Gets or sets the name of the vendor.
        /// </summary>
        /// <value>
        /// The name of the vendor.
        /// </value>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>29-05-201714:21</TimeStamp>"
        public string VendorName { get; set; }

        /// <summary>
        /// Gets or sets the vendor identifier.
        /// </summary>
        /// <value>
        /// The vendor identifier.
        /// </value>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>29-05-201714:21</TimeStamp>"
        public uint VendorIdentifier { get; set; }

        /// <summary>
        /// Gets or sets the name of the model.
        /// </summary>
        /// <value>
        /// The name of the model.
        /// </value>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>29-05-201714:22</TimeStamp>"
        public string ModelName { get; set; }

        /// <summary>
        /// Gets or sets the application software version.
        /// </summary>
        /// <value>
        /// The application software version.
        /// </value>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>29-05-201714:22</TimeStamp>"
        //public string ApplicationSoftwareVersion { get; set; }

        /// <summary>
        /// Gets or sets the firmware revision.
        /// </summary>
        /// <value>
        /// The firmware revision.
        /// </value>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>29-05-201714:22</TimeStamp>"
        //public string FirmwareRevision { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>29-05-201714:22</TimeStamp>"
        public string Description { get; set; }

        /// <summary>
        /// Gets or Sets whethere Temp value needs to be converted or not
        /// </summary>
        public bool IsTempRequiredInFahrenheit { get; set; }

        /// <summary>
        /// Gets or sets the apdu timeout.
        /// </summary>
        /// <value>
        /// The apdu timeout.
        /// </value>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>29-05-201714:24</TimeStamp>"
        public uint APDUTimeout { get; set; }


        /// <summary>
        /// Gets or sets the apdu retries.
        /// </summary>
        /// <value>
        /// The apdu retries.
        /// </value>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>29-05-201714:24</TimeStamp>"
        public uint APDURetries { get; set; }

        /// <summary>
        /// Gets or sets the apdu segment timeout.
        /// </summary>
        /// <value>
        /// The apdu segment timeout.
        /// </value>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>29-05-201714:24</TimeStamp>"
        public uint APDUSegmentTimeout { get; set; }

        /// <summary>
        /// Gets or sets the UTC offset.
        /// </summary>
        /// <value>
        /// The UTC offset.
        /// </value>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>29-05-201714:24</TimeStamp>"
        public int UTCOffset { get; set; }

        #endregion


        /// <summary>
        /// Gets or sets the ip address.
        /// </summary>
        /// <value>
        /// The ip address.
        /// </value>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>29-05-201714:24</TimeStamp>"
        public string IPAddress { get; set; }

        /// <summary>
        /// Gets or sets the UDP port.
        /// </summary>
        /// <value>
        /// The UDP port.
        /// </value>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>29-05-201714:24</TimeStamp>"
        public int UDPPort { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is BBMD.
        /// REQ_BCT_24:BBMD/Non-BBMD support
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is BBMD; otherwise, <c>false</c>.
        /// </value>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>29-05-201714:25</TimeStamp>"
        public bool IsBBMD { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether [fdip address].
        /// REQ_BCT_25: Foreign device registration
        /// </summary>
        /// <value>
        ///   <c>true</c> if [fdip address]; otherwise, <c>false</c>.
        /// </value>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>29-05-201714:25</TimeStamp>"
        public string FDIPAddress { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [fd re register].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [fd re register]; otherwise, <c>false</c>.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>7/4/20172:22 PM</TimeStamp>
        public bool FDReRegister { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [fdudp port].
        /// REQ_BCT_25: Foreign device registration
        /// </summary>
        /// <value>
        ///   <c>true</c> if [fdudp port]; otherwise, <c>false</c>.
        /// </value>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>29-05-201714:25</TimeStamp>"
        public string FDUDPPort { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [fd time to live].
        /// REQ_BCT_25: Foreign device registration
        /// </summary>
        /// <value>
        ///   <c>true</c> if [fd time to live]; otherwise, <c>false</c>.
        /// </value>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>29-05-201714:25</TimeStamp>"
        public string FDTimeToLive { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether [write access].
        /// REQ_BCT_26:Enable or disable Write access from BMS 
        /// </summary>
        /// <value>
        ///   <c>true</c> if [write access]; otherwise, <c>false</c>.
        /// </value>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>29-05-201714:25</TimeStamp>"
        public bool WriteAccess { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether [time synchronization].
        /// REQ_BCT_27:Enable or disable Time Synchronization access from BMS 
        /// </summary>
        /// <value>
        ///   <c>true</c> if [time synchronization]; otherwise, <c>false</c>.
        /// </value>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>29-05-201714:25</TimeStamp>"
        public bool TimeSynchronization { get; set; }

        //REQ_BCT_28:Enable or disable Scheduling

        public bool Scheduling { get; set; }


        /// <summary>
        /// Gets or sets the type of the notify.
        /// REQ_BCT_29: Alarm and event properties settings
        /// </summary>
        /// <value>
        /// The type of the notify.
        /// </value>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>29-05-201714:26</TimeStamp>"
        public uint NotifyType { get; set; }

        /// <summary>
        /// Gets or sets the time delay.
        /// REQ_BCT_29: Alarm and event properties settings
        /// </summary>
        /// <value>
        /// The time delay.
        /// </value>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>29-05-201714:26</TimeStamp>"
        public uint TimeDelay { get; set; }


        /// <summary>
        /// Gets or sets the ba cnet password.
        /// REQ_BCT_30:BACnet password setting
        /// </summary>
        /// <value>
        /// The ba cnet password.
        /// </value>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>29-05-201714:26</TimeStamp>"
        public string BACnetPassword { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether this instance is building alarm.
        /// Design-Molex-BACnet-Gateway-1.0.docx : 3.6.5.2	Event Reporting
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is building alarm; otherwise, <c>false</c>.
        /// </value>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>29-05-201714:26</TimeStamp>"
        public bool IsBuildingAlarm { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is floor alarm.
        /// Design-Molex-BACnet-Gateway-1.0.docx : 3.6.5.2	Event Reporting
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is floor alarm; otherwise, <c>false</c>.
        /// </value>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>29-05-201714:26</TimeStamp>"
        public bool IsFloorAlarm { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether [daylight savings status].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [daylight savings status]; otherwise, <c>false</c>.
        /// </value>
        /// <CreatedBy>prasad.joshi</CreatedBy><TimeStamp>6/7/20171:55 PM</TimeStamp>
        public bool DaylightSavingStatus { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [map individual fixtures].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [map individual fixtures]; otherwise, <c>false</c>.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>09-06-201701:47 PM</TimeStamp>
        public bool MapIndividualFixture { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [map individual sensors].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [map individual sensors]; otherwise, <c>false</c>.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>09-06-201701:47 PM</TimeStamp>
        public bool MapIndividualSensors { get; set; }

        /// <summary>
        /// Gets or sets the trasncend server base URL.
        /// </summary>
        /// <value>
        /// The trasncend server base URL.
        /// </value>
        /// <CreatedBy>prasad.joshi</CreatedBy><TimeStamp>6/19/20174:27 PM</TimeStamp>
        public string MolexAPIUrl { get; set; }

        /// <summary>
        /// Gets or sets the priority. This priority will be set by BCT.
        /// This property is used to set the properties while monitoring of the object to the specified priority.
        /// </summary>
        /// <value>
        /// The priority. 
        /// </value>
        /// <CreatedBy>Rohit Galgali</CreatedBy>
        /// <TimeStamp>7/3/20175:01 PM</TimeStamp>
        public int MonitoringDefaultPriority { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether this instance is debug log enable.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is debug log enable; otherwise, <c>false</c>.
        /// </value>
        /// <CreatedBy>Rohit Galgali</CreatedBy>
        /// <TimeStamp>7/4/20172:27 PM</TimeStamp>
        public bool IsDebugLogEnable { get; set; }

        /// <summary>
        /// Gets or sets the molex project identifier.
        /// </summary>
        /// <value>
        /// The molex project identifier.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>21-07-201710:46 AM</TimeStamp>
        public string MolexProjectID { get; set; }
        /// <summary>
        /// Gets or sets the local network number.
        /// </summary>
        /// <value>
        /// The local network number.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>7/31/20174:56 PM</TimeStamp>
        public int LocalNetworkNumber { get; set; }

        /// <summary>
        /// Gets or sets the virtual network number.
        /// </summary>
        /// <value>
        /// The virtual network number.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>7/31/20174:56 PM</TimeStamp>
        public int VirtualNetworkNumber { get; set; }

        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        /// <value>
        /// The name of the user.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>8/9/20175:44 PM</TimeStamp>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>
        /// The password.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>8/9/20175:44 PM</TimeStamp>
        public string Password { get; set; }

        /// <summary>
        /// This Object will persists the Notification Priority for Buildign Level
        /// </summary>
        public NotificationPriority BuildingNotificationPriority { get; set; }

        /// <summary>
        /// This Object will persists the Notification Priority for Zone Level
        /// </summary>
        public NotificationPriority ZoneNotificationPriority { get; set; }

        /// <summary>
        /// Gets or sets the alarm disable object list.
        /// </summary>
        /// <value>
        /// The alarm disable object list.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>2/8/201812:52 PM</TimeStamp>
        public Dictionary<string, List<ZoneObject>> AlarmNotificationSetting { get; set; }

        /// <summary>
        /// Gets or sets the BDT list.
        /// </summary>
        /// <value>
        /// The BDT list.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>4/23/201811:37 AM</TimeStamp>
        public List<StackDataObjects.APIModels.BDTModel> BDTList { get; set; }

        /// <summary>
        /// Gets or sets the FDT list.
        /// </summary>
        /// <value>
        /// The FDT list.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>4/23/201811:37 AM</TimeStamp>
        public List<StackDataObjects.APIModels.FDTModel> FDTList { get; set; }
    }
    /// <summary>
    /// Alarm notification disable object proparties
    /// </summary>
    /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>2/28/20184:45 PM</TimeStamp>
    public class ZoneObject
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>2/28/20184:45 PM</TimeStamp>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [zone level].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [zone level]; otherwise, <c>false</c>.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>2/28/20184:46 PM</TimeStamp>
        public bool ZoneLevel { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="SelectedObject"/> is individual.
        /// </summary>
        /// <value>
        ///   <c>true</c> if individual; otherwise, <c>false</c>.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>2/28/20184:46 PM</TimeStamp>
        public bool Individual { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [zone value].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [zone value]; otherwise, <c>false</c>.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>2/28/20184:46 PM</TimeStamp>
        public bool ZoneValue { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [individual value].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [individual value]; otherwise, <c>false</c>.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>2/28/20184:46 PM</TimeStamp>
        public bool IndividualValue { get; set; }
    }

    public class NotificationPriority
    {
        public int ToOffNormal { get; set; }
        public int ToFault { get; set; }
        public int ToNormal { get; set; }
        public bool OverridePersistantState { get; set; }
    }

}
