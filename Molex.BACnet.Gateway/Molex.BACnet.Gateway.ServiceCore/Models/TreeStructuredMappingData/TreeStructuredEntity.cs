﻿using Molex.BACnet.Gateway.ServiceCore.Constants;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models.TreeStructuredMappingData
{
    internal abstract class TreeStructuredEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TreeStructuredEntity"/> class.
        /// </summary>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>6/9/20174:09 PM</TimeStamp>
        public TreeStructuredEntity()
        {
            BACnetEntityMap = new Dictionary<string, TreeStructuredBACnetEntity>();
        }

        /// <summary>
        /// Gets or sets the object map. This dicionary holds mapping of 
        /// BACnet Entity associated with the Entity key. For eg. AV, SV, MSV is associated with Building_1.
        /// </summary>
        /// <value>
        /// The object map.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>6/6/20173:04 PM</TimeStamp>
        public Dictionary<string, TreeStructuredBACnetEntity> BACnetEntityMap { get; set; }

        /// <summary>
        /// Gets or sets the device identifier.
        /// </summary>
        /// <value>
        /// The device identifier.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>6/6/20174:56 PM</TimeStamp>
        public int DeviceId { get; set; }

    }

    internal class ControllerEntity : TreeStructuredEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TreeStructuredEntity"/> class.
        /// </summary>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>6/9/20174:09 PM</TimeStamp>
        public ControllerEntity()
        {
            Buildings = new Dictionary<string, BuildingEntity>();
        }

        /// <summary>
        /// Gets or sets the childs.
        /// </summary>
        /// <value>
        /// The childs.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>7/13/20176:07 PM</TimeStamp>
        public Dictionary<string, BuildingEntity> Buildings { get; set; }
    }

    internal class BuildingEntity : TreeStructuredEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TreeStructuredEntity"/> class.
        /// </summary>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>6/9/20174:09 PM</TimeStamp>
        public BuildingEntity()
        {
            Floors = new Dictionary<string, FloorEntity>();
        }

        /// <summary>
        /// Gets or sets the childs.
        /// </summary>
        /// <value>
        /// The childs.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>7/13/20176:07 PM</TimeStamp>
        public Dictionary<string, FloorEntity> Floors { get; set; }
    }

    internal class FloorEntity : TreeStructuredEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TreeStructuredEntity"/> class.
        /// </summary>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>6/9/20174:09 PM</TimeStamp>
        public FloorEntity()
        {
            Spaces = new Dictionary<string, SpacesEntity>();
        }

        /// <summary>
        /// Gets or sets the childs.
        /// </summary>
        /// <value>
        /// The childs.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>7/13/20176:07 PM</TimeStamp>
        public Dictionary<string, SpacesEntity> Spaces { get; set; }
    }

    internal class SpacesEntity : TreeStructuredEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TreeStructuredEntity"/> class.
        /// </summary>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>6/9/20174:09 PM</TimeStamp>
        public SpacesEntity()
        {
            Endpoints = new Dictionary<string, EndPointsEntity>();
        }

        /// <summary>
        /// Gets or sets the childs.
        /// </summary>
        /// <value>
        /// The childs.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>7/13/20176:07 PM</TimeStamp>
        public Dictionary<string, EndPointsEntity> Endpoints { get; set; }
    }

    internal class EndPointsEntity : TreeStructuredEntity
    {
        public EndPointsEntity() :base()
        {

        }
    }
}
