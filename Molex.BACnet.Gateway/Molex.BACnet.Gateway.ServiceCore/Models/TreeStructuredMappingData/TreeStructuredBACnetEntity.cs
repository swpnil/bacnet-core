﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models.TreeStructuredMappingData
{
    internal class TreeStructuredBACnetEntity
    {
        #region public properties

        /// <summary>
        /// Gets or sets the object identifier. This Id is associated with AV, MSV, SV
        /// </summary>
        /// <value>
        /// The object identifier.
        /// </value>
        /// <CreatedBy>Rohit Galgali</CreatedBy><TimeStamp>25-05-201706:58 PM</TimeStamp>
        public uint ObjectID { get; set; }

        /// <summary>
        /// Gets or sets the name of the object.
        /// </summary>
        /// <value>
        /// The name of the object.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>6/28/201711:42 AM</TimeStamp>
        public string ObjectName { get; set; }

        /// <summary>
        /// Gets or sets the object description.
        /// </summary>
        /// <value>
        /// The object description.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>6/28/201711:42 AM</TimeStamp>
        public string ObjectDescription { get; set; }

        #endregion
    }
}
