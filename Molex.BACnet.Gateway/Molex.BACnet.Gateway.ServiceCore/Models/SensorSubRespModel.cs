﻿
namespace Molex.BACnet.Gateway.ServiceCore.Models
{
    class SensorSubRespModel
    {
        /// <summary>
        /// To get or set zone id
        /// </summary>
        public string ZoneID { get; set; }

        /// <summary>
        /// To get or set sensor id
        /// </summary>
        public string SensorID { get; set; }

        /// <summary>
        /// To set or get sensor value
        /// </summary>
        public float Value { get; set; }

        /// <summary>
        /// To set or get time stamp (sensor get value time)
        /// </summary>
        public string Timestamp { get; set; }
    }
}
