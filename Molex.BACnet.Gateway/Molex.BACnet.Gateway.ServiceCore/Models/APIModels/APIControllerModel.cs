﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       UniDEL Pte. Ltd
///   Class:              <APIControllerModel.cs>
///   Description:        <This API Model is used to provide skeleton for Reading JSON Data>
///   Author:             Rupesh Saw                  
///   Date:               05/19/17
///---------------------------------------------------------------------------------------------
#endregion

using System.Collections.Generic;

namespace Molex.BACnet.Gateway.ServiceCore.Models.APIModels
{
    /// <summary>
    /// This API Model is used to provide skeleton for Reading JSON Data
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:28 PM</TimeStamp>
    public class APIControllerModel
    {
        #region Public Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="APIControllerModel"/> class.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:28 PM</TimeStamp>
        public APIControllerModel()
        {
            LightScenes = new Dictionary<string, APILightscenesModel>();
            Spaces = new Dictionary<string, APIUser_SpaceModel>();
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>26-05-201703:41 PM</TimeStamp>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the address.
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the location.
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// Gets or sets the configuration.
        /// </summary>
        public string Config { get; set; }

        /// <summary>
        /// Gets or sets the light scenes.
        /// </summary>
        public Dictionary<string, APILightscenesModel> LightScenes { get; set; }

        // public Animatedpalettes animatedPalettes { get; set; }

        /// <summary>
        /// Gets or sets the spaces.
        /// </summary>
        public Dictionary<string, APIUser_SpaceModel> Spaces { get; set; }

        #endregion
    }
}
