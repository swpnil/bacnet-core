﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       UniDEL Pte. Ltd
///   Class:              <APIMolexModel.cs>
///   Description:        <This API Model is used to provide skeleton for Reading JSON Data>
///   Author:             Rupesh Saw                  
///   Date:               05/19/17
///   Notes:              <Notes>
///   Revision History: 
///   Modified By:     Modified Date:    Reviewed By:      Reviewed Date:    Review Comments:
///---------------------------------------------------------------------------------------------
#endregion

using System;
using System.Collections.Generic;

namespace Molex.BACnet.Gateway.ServiceCore.Models.APIModels
{
    /// <summary>
    /// This API Model is used to provide skeleton for Reading JSON Data
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:29 PM</TimeStamp>
    public class APIMolexModel
    {
        #region Public Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="APIMolexModel"/> class.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:29 PM</TimeStamp>
        public APIMolexModel()
        {
            Controllers = new Dictionary<string, APIControllerModel>();
            Buildings = new Dictionary<string, APIBuildingModel>();
        }

        #endregion

        #region Public Properties

        public string Name { get; set; }
        public string Client { get; set; }
        public string Contact { get; set; }
        public string BIMFile { get; set; }
        public string Stage { get; set; }
        public string BaseIPFixtures { get; set; }
        public string BaseIPSensors { get; set; }
        public string BaseIPCurrent { get; set; }
        // public Demospace DemoSpace { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string FormatVersionnumber { get; set; }
        //public List<APIControllerModel> Controllers { get; set; }
        //public List<APIBuildingModel> Buildings { get; set; }        
        /// <summary>
        /// Gets or sets the controllers.
        /// </summary>
        /// <value>
        /// The controllers.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>08-06-201701:12 PM</TimeStamp>
        public Dictionary<string, APIControllerModel> Controllers { get; set; }

        /// <summary>
        /// Gets or sets the buildings.
        /// </summary>
        /// <value>
        /// The buildings.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>08-06-201701:12 PM</TimeStamp>
        public Dictionary<string, APIBuildingModel> Buildings { get; set; }

        #endregion

    }
}
