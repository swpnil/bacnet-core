﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       UniDEL Pte. Ltd
///   Class:              APILightscenesModel.cs
///   Description:        This API Model is used to provide skeleton for Reading JSON Data
///   Author:             Rupesh Saw                  
///   Date:               05/19/17
///   Notes:              <Notes>
///   Revision History: 
///   Modified By:     Modified Date:    Reviewed By:      Reviewed Date:    Review Comments:
///---------------------------------------------------------------------------------------------
#endregion


namespace Molex.BACnet.Gateway.ServiceCore.Models.APIModels
{
    /// <summary>
    /// API for LightSceneModel
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>03-07-201706:35 PM</TimeStamp>
    public class APILightscenesModel
    {
        // public string LightScenesType;        
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>03-07-201706:35 PM</TimeStamp>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>03-07-201706:35 PM</TimeStamp>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the slot.
        /// </summary>
        /// <value>
        /// The slot.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>03-07-201706:35 PM</TimeStamp>
        public int Slot { get; set; }

    }

    //public enum LightscenesType
    //{
    //    Off, Creative, Meeting, Orbit, Speaker, Daylight, Video, Personal, Workinghours, Outofhours, Rr,
    //    Stairs, Elevator, Lobby, Exit, Cafeteria, Aisle, Feature
    //};



}
