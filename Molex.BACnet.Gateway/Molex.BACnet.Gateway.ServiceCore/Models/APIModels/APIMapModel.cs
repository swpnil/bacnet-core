﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       UniDEL Pte. Ltd
///   Class:              APIMapModel.cs
///   Description:        This API Model is used to provide skeleton for Reading JSON Data
///   Author:             Rupesh Saw                  
///   Date:               05/19/17
///   Notes:              <Notes>
///   Revision History: 
///   Modified By:     Modified Date:    Reviewed By:      Reviewed Date:    Review Comments:
///---------------------------------------------------------------------------------------------
#endregion


namespace Molex.BACnet.Gateway.ServiceCore.Models.APIModels
{
    /// <summary>
    /// This API Model is used to provide skeleton for Reading JSON Data
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:29 PM</TimeStamp>
    public class APIMapModel
    {
        #region Public Properties

        public string Id { get; set; }
        public string building { get; set; }
        public string floor { get; set; }
        public int locX { get; set; }
        public int locY { get; set; }
        public int width { get; set; }
        public int height { get; set; }
        public int orientation { get; set; }

        #endregion
    }
}
