﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       UniDEL Pte. Ltd
///   Class:              <APIBuildingModel.cs>
///   Description:        <This API Model is used to provide skeleton for Reading JSON Data>
///   Author:             Rupesh Saw                  
///   Date:               05/19/17
///   Notes:              <Notes>
///   Revision History: 
///   Modified By:     Modified Date:    Reviewed By:      Reviewed Date:    Review Comments:
///---------------------------------------------------------------------------------------------
#endregion

using System.Collections.Generic;

namespace Molex.BACnet.Gateway.ServiceCore.Models.APIModels
{
    /// <summary>
    /// This API Model is used to provide skeleton for Reading JSON Data
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:27 PM</TimeStamp>
    public class APIBuildingModel
    {
        #region Public Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="APIBuildingModel"/> class.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:27 PM</TimeStamp>
        public APIBuildingModel()
        {
            Areas = new Dictionary<string, APIAreaModel>();
            Floors = new Dictionary<string, APIFloorModel>();
        }

        #endregion

        #region Public Property

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201706:57 PM</TimeStamp>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201706:57 PM</TimeStamp>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the location.
        /// </summary>
        /// <value>
        /// The location.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201706:57 PM</TimeStamp>
        public string Location { get; set; }

        /// <summary>
        /// Gets or sets the user data.
        /// </summary>
        /// <value>
        /// The user data.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201706:58 PM</TimeStamp>
        public string UserData { get; set; }

        /// <summary>
        /// Gets or sets the utilisation.
        /// </summary>
        /// <value>
        /// The utilisation.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201706:58 PM</TimeStamp>
        public int Utilisation { get; set; }

        /// <summary>
        /// Gets or sets the areas.
        /// </summary>
        /// <value>
        /// The areas.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201706:58 PM</TimeStamp>
        public Dictionary<string, APIAreaModel> Areas { get; set; }

        /// <summary>
        /// Gets or sets the floors.
        /// </summary>
        /// <value>
        /// The floors.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201706:58 PM</TimeStamp>
        public Dictionary<string, APIFloorModel> Floors { get; set; }

        #endregion
    }
}
