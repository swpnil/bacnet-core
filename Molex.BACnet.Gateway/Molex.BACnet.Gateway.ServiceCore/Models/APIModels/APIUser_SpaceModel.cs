﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       UniDEL Pte. Ltd
///   Class:              <APIUser_SpaceModel.cs>
///   Description:        <This API Model is used to provide skeleton for Reading JSON Data>
///   Author:             Rupesh Saw                  
///   Date:               05/19/17
///   Notes:              <Notes>
///   Revision History: 
///   Modified By:     Modified Date:    Reviewed By:      Reviewed Date:    Review Comments:
///---------------------------------------------------------------------------------------------
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models.APIModels
{
    /// <summary>
    /// This API Model is used to provide skeleton for Reading JSON Data
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:31 PM</TimeStamp>
    public class APIUser_SpaceModel
    {
        #region Public Property

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the am bx space.
        /// </summary>
        public int AmBXSpace { get; set; }

        /// <summary>
        /// Gets or sets the layer.
        /// </summary>
        public string Layer { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the default light scene.
        /// </summary>
        public string DefaultLightScene { get; set; }

        /// <summary>
        /// Gets or sets the default mood.
        /// </summary>
        public string DefaultMood { get; set; }

        /// <summary>
        /// Gets or sets the default brightness.
        /// </summary>
        public int DefaultBrightness { get; set; }
        //public Availablelightscenes1 availableLightScenes { get; set; }
        //public Defaultanimatedpalette1 defaultAnimatedPalette { get; set; }        
        /// <summary>
        /// Gets or sets the active fixtures.
        /// </summary>
        public string[] activeFixtures { get; set; }

        /// <summary>
        /// Gets or sets the active sensors.
        /// </summary>
        public string[] activeSensors { get; set; }
        //  public Schedule schedule { get; set; }
        //   public Activation activation { get; set; }
        //  public Sensorbrain sensorBrain { get; set; }
        // public List<MapModel> maps { get; set; } 

        #endregion
    }
}
