﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       UniDEL Pte. Ltd
///   Class:              <APISensorModel.cs>
///   Description:        <This API Model is used to provide skeleton for Reading JSON Data>
///   Author:             Rupesh Saw                  
///   Date:               05/19/17
///   Notes:              <Notes>
///   Revision History: 
///   Modified By:     Modified Date:    Reviewed By:      Reviewed Date:    Review Comments:
///---------------------------------------------------------------------------------------------
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models.APIModels
{
    /// <summary>
    /// This API Model is used to provide skeleton for Reading JSON Data
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:29 PM</TimeStamp>
    public class APISensorModel
    {
        #region Public Properties

        public string Id { get; set; }
        public string Name { get; set; }
        public string Controller { get; set; }
        public string Space { get; set; }
        public string AmBXLocation { get; set; }
        public uint GridX { get; set; }
        public uint GridY { get; set; }
        public string Address { get; set; }
        public string UserData { get; set; }
        public string Protocol { get; set; }
        public string Type { get; set; }
        public string Role { get; set; }
        public int PollingRate { get; set; }

        #endregion
    }
}
