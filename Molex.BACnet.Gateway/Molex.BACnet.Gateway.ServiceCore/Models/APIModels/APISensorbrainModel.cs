﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       UniDEL Pte. Ltd
///   Class:              <APISensorbrainModel.cs>
///   Description:        <This API Model is used to provide skeleton for Reading JSON Data>
///   Author:             Rupesh Saw                  
///   Date:               05/19/17
///   Notes:              <Notes>
///   Revision History: 
///   Modified By:     Modified Date:    Reviewed By:      Reviewed Date:    Review Comments:
///---------------------------------------------------------------------------------------------
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models.APIModels
{
    /// <summary>
    /// This API Model is used to provide skeleton for Reading JSON Data
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:29 PM</TimeStamp>
    public class APISensorbrainModel
    {
        #region Public Properties

        public APIBehavioursModel Behaviours { get; set; }
        public int BioAdaptivityDef { get; set; }
        public int ImmersionDef { get; set; }
        public int UpdateSensitivity { get; set; }
        public int PresTimeout { get; set; }
        public int PresDelta { get; set; }
        public int PresRate { get; set; }
        public int DHTargetLux { get; set; }
        public int DHRate { get; set; }
        public int DHDiffUp { get; set; }
        public float DHDeltaUp { get; set; }
        public int DHDiffDown { get; set; }
        public float DHDeltaDown { get; set; }
        public APISwatchsModel Swatchs { get; set; }
        public int BasePaletteUpdateEvery { get; set; }
        public int MinimumComponentChangeForUpdate { get; set; }
        public int DHMaxAllowedDeviation { get; set; }
        public float BioadaptivityStepSize { get; set; }
        public float ImmersionStepSize { get; set; }

        #endregion
    }
}
