﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       UniDEL Pte. Ltd
///   Class:              <APIFloorModel.cs>
///   Description:        <This API Model is used to provide skeleton for Reading JSON Data>
///   Author:             Rupesh Saw                  
///   Date:               05/19/17
///   Notes:              <Notes>
///   Revision History: 
///   Modified By:     Modified Date:    Reviewed By:      Reviewed Date:    Review Comments:
///---------------------------------------------------------------------------------------------
#endregion

using System.Collections.Generic;

namespace Molex.BACnet.Gateway.ServiceCore.Models.APIModels
{
    /// <summary>
    /// This API Model is used to provide skeleton for Reading JSON Data
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:28 PM</TimeStamp>
    public class APIFloorModel
    {
        #region Public Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="APIFloorModel"/> class.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:28 PM</TimeStamp>
        public APIFloorModel()
        {
            Fixtures = new Dictionary<string, APIFixtureModel>();
            Sensors = new Dictionary<string, APISensorModel>();
            Zones = new Dictionary<string, APIZoneModel>();
        }

        #endregion


        #region Public Properties

        public string Id { get; set; }
        public string Name { get; set; }
        public string FloorPlan { get; set; }
        public int GridW { get; set; }
        public int GridH { get; set; }
        public Dictionary<string, APIFixtureModel> Fixtures { get; set; }
        public Dictionary<string, APISensorModel> Sensors { get; set; }
        public Dictionary<string, APIZoneModel> Zones { get; set; }

        #endregion
    }
}
