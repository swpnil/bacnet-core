﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       UniDEL Pte. Ltd
///   Class:              <APISwatchPaletteModel.cs>
///   Description:        <This API Model is used to provide skeleton for Reading JSON Data>
///   Author:             Rupesh Saw                  
///   Date:               05/19/17
///   Notes:              <Notes>
///   Revision History: 
///   Modified By:     Modified Date:    Reviewed By:      Reviewed Date:    Review Comments:
///---------------------------------------------------------------------------------------------
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models.APIModels
{
    /// <summary>
    /// SwatchPaletteType
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:30 PM</TimeStamp>
    public enum SwatchPaletteType
    {
        Alert, Focused, Vigilant, Default18, Calm, Relaxed, Cozy
    };
    /// <summary>
    /// This API Model is used to provide skeleton for Reading JSON Data
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:30 PM</TimeStamp>
    public class APISwatchPaletteModel
    {
        #region Public Properties

        public string SwatchType;

        public int[] ww { get; set; }
        public int[] cw { get; set; }
        public int[] pw { get; set; }

        #endregion

        /* public Alert alert { get; set; }
         public Focused focused { get; set; }
         public Vigilant vigilant { get; set; }
         public Default18 _default { get; set; }
         public Calm calm { get; set; }
         public Relaxed relaxed { get; set; }
         public Cozy cozy { get; set; }*/
    }
}
