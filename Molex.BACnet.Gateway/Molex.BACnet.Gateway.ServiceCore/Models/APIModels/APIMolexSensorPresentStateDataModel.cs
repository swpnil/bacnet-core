﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       UniDEL Pte. Ltd
///   Class:              APIMolexSensorPresentStateDataModel.cs
///   Description:        This class is used to deserailize device data of space state to get sensor information
///   Author:             Amol Kulkarni                  
///   Date:               06/07/2017
///   Notes:              <Notes>
///   Revision History: 
///   Modified By:     Modified Date:   Reviewed By:      Reviewed Date:    Review Comments:
///---------------------------------------------------------------------------------------------
#endregion

using System.Collections.Generic;

namespace Molex.BACnet.Gateway.ServiceCore.Models.APIModels
{
    /// <summary>
    /// This API model is used for maintaining PresentStateData (Role and Output) for Sensor
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>15-06-201712:39 PM</TimeStamp>
    public class APIMolexSensorPresentStateDataModel
    {
        /// <summary>
        /// Gets or sets the output.
        /// </summary>
        /// <value>
        /// The out put.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>6/9/20174:58 AM</TimeStamp>
        public List<object> OutPut { get; set; }
    }
}
