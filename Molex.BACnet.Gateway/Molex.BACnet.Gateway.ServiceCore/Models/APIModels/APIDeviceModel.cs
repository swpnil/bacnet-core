﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models.APIModels
{
    public class APIDeviceModel
    {
        /// <summary>
        /// Gets or sets the output.
        /// </summary>
        /// <value>
        /// The out put.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>7/5/201711:22 AM</TimeStamp>
        public List<object> OutPut { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>18-07-201712:46 PM</TimeStamp>
        public string Name { get; set; }
    }
}
