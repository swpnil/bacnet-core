﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       UniDEL Pte. Ltd
///   Class:              <APIAreaModel.cs>
///   Description:        <Molex API Area Model is used to provide skeleton for Reading JSON Data>
///   Author:             Rupesh Saw                  
///   Date:               05/19/17
///   Notes:              <Notes>
///   Revision History: 
///   Modified By:     Modified Date:    Reviewed By:      Reviewed Date:    Review Comments:
///---------------------------------------------------------------------------------------------
#endregion

using System.Collections.Generic;

namespace Molex.BACnet.Gateway.ServiceCore.Models.APIModels
{
    /// <summary>
    /// Molex API Area Model is used to provide skeleton for Reading JSON Data
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:27 PM</TimeStamp>
    public class APIAreaModel
    {

        #region Public Properties
        /// <summary>
        /// Gets or sets the owner.
        /// </summary>
        /// <value>
        /// The owner.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201706:57 PM</TimeStamp>
        public string Owner { get; set; }

        /// <summary>
        /// Gets or sets the spaceref list.
        /// </summary>
        /// <value>
        /// The spaceref list.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201706:57 PM</TimeStamp>
        public Dictionary<string, APISpacerefModel> Spaceref_List { get; set; }

        #endregion
    }
}
