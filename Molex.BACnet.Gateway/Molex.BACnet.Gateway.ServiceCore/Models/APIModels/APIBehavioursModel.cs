﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       UniDEL Pte. Ltd
///   Class:              APIBehavioursModel.cs
///   Description:        This API Model is used to provide skeleton for Reading JSON Data
///   Author:             Rupesh Saw                  
///   Date:               05/19/17
///   Notes:              <Notes>
///   Revision History: 
///   Modified By:     Modified Date:    Reviewed By:      Reviewed Date:    Review Comments:
///---------------------------------------------------------------------------------------------
#endregion


namespace Molex.BACnet.Gateway.ServiceCore.Models.APIModels
{
    /// <summary>
    /// This API Model is used to provide skeleton for Reading JSON Data
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:27 PM</TimeStamp>
    public class APIBehavioursModel
    {
        #region Public Properties
        /// <summary>
        /// Gets or sets the on idle.
        /// </summary>
        /// <value>
        /// The on idle.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201706:57 PM</TimeStamp>
        public string OnIdle { get; set; }

        #endregion
    }
}
