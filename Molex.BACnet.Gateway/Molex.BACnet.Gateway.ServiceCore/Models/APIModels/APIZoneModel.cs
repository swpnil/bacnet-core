﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       UniDEL Pte. Ltd
///   Class:              <APIZoneModel.cs>
///   Description:        <This API Model is used to provide skeleton for Reading JSON Data>
///   Author:             Rupesh Saw                  
///   Date:               05/19/17
///   Notes:              <Notes>
///   Revision History: 
///   Modified By:     Modified Date:    Reviewed By:      Reviewed Date:    Review Comments:
///---------------------------------------------------------------------------------------------
#endregion

using System.Collections.Generic;

namespace Molex.BACnet.Gateway.ServiceCore.Models.APIModels
{
    /// <summary>
    /// This API Model is used to provide skeleton for Reading JSON Data
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:31 PM</TimeStamp>
    public class APIZoneModel
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the am bx space identifier.
        /// </summary>
        public string AmBXSpaceId { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the fixtures.
        /// </summary>
        public Dictionary<string, APIFixtureModel> Fixtures { get; set; }

        /// <summary>
        /// Gets or sets the sensors.
        /// </summary>
        public Dictionary<string, APISensorModel> Sensors { get; set; }

        #endregion


        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="APIZoneModel"/> class.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:31 PM</TimeStamp>
        public APIZoneModel()
        {
            Fixtures = new Dictionary<string, APIFixtureModel>();
            Sensors = new Dictionary<string, APISensorModel>();
        }

        #endregion


    }
}
