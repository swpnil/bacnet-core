﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       UniDEL Pte. Ltd
///   Class:              APIMolexZonePresentStateDataModel
///   Description:        This is a place holder for the Json Data for Space state Data.
///   Author:            Rohit Galgali               
///   Date:               5/26/2017
#endregion

using System;
using System.Collections.Generic;
using Molex.BACnet.Gateway.ServiceCore.Constants;
namespace Molex.BACnet.Gateway.ServiceCore.Models.APIModels
{
    /// <summary>
    /// This is a place holder for the Json Data for Space state Data.
    /// </summary>
    /// <CreatedBy>hp</CreatedBy>
    /// <TimeStamp>5/26/20174:51 PM</TimeStamp>
    public class APIMolexZonePresentStateDataModel
    {
        /// <summary>
        /// Gets or sets brightness.
        /// </summary>
        /// <value>
        /// Brightness.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>5/26/20174:50 PM</TimeStamp>
        public long Brightness { get; set; }

        /// <summary>
        /// Gets or sets the average ambient light.
        /// </summary>
        /// <value>
        /// The average ambient light.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>5/26/20174:51 PM</TimeStamp>
        public float AvgAmbientLight { get; set; }

        /// <summary>
        /// Gets or sets the average color temperature.
        /// </summary>
        /// <value>
        /// The average color temperature.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>5/26/20174:51 PM</TimeStamp>
        public float AvgColorTemperature { get; set; }

        /// <summary>
        /// Gets or sets the average temperature.
        /// </summary>
        /// <value>
        /// The average temperature.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>12/11/201712:13 PM</TimeStamp>
        public float AvgTemperature { get; set; }

        /// <summary>
        /// Gets or sets the average humidity.
        /// </summary>
        /// <value>
        /// The average humidity.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>12/11/201712:13 PM</TimeStamp>
        public float AvgHumidity { get; set; }

        /// <summary>
        /// Gets or sets the average air quality.
        /// </summary>
        /// <value>
        /// The average air quality.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>12/11/201712:13 PM</TimeStamp>
        public float AvgAirQuality { get; set; }

        /// <summary>
        /// Gets or sets the total power.
        /// </summary>
        /// <value>
        /// The total power.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>12/11/201712:13 PM</TimeStamp>
        public float TotalPower { get; set; }


        /// <summary>
        /// Gets or sets the total power.
        /// </summary>
        /// <value>
        /// The total power.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>12/11/201712:13 PM</TimeStamp>
        public int Occupancy { get; set; }


        /// <summary>
        /// Gets or sets the devices.
        /// </summary>
        /// <value>
        /// The devices.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>6/6/20171:42 PM</TimeStamp>
        public Dictionary<string, APIMolexSensorPresentStateDataModel> Sensors { get; set; }

        /// <summary>
        /// Gets or sets the palette.
        /// </summary>
        /// <value>
        /// The palette.
        /// </value>
        /// <CreatedBy>Rohit Galgali</CreatedBy>
        /// <TimeStamp>10/31/20172:17 PM</TimeStamp>
        public string Palette { get; set; }

        /// <summary>
        /// Gets or sets the light scene.
        /// </summary>
        /// <value>
        /// The light scene.
        /// </value>
        /// <CreatedBy>prasad.joshi</CreatedBy><TimeStamp>6/5/20175:02 PM</TimeStamp>
        public string LightScene { get; set; }
        /// <summary>
        /// Gets or sets the dh target lux.
        /// </summary>
        /// <value>
        /// The dh target lux.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>6/13/201710:49 PM</TimeStamp>
        public long DHTargetLux { get; set; }

        /// <summary>
        /// Gets or sets the pres timeout.
        /// </summary>
        /// <value>
        /// The pres timeout.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>20-06-201707:55 AM</TimeStamp>
        public long PresTimeout { get; set; }

        /// <summary>
        /// Gets or sets the pres rate.
        /// </summary>
        /// <value>
        /// The pres rate.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>20-06-201707:55 AM</TimeStamp>
        public long PresRate { get; set; }

        /// <summary>
        /// Gets or sets the mood.
        /// </summary>
        /// <value>
        /// The mood.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>8/7/20177:29 PM</TimeStamp>
        public string Mood { get; set; }

        /// <summary>
        /// Gets or sets the operational status.
        /// </summary>
        /// <value>
        /// The operational status.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>9/15/20174:55 PM</TimeStamp>
        public bool OperationalStatus { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="APIMolexZonePresentStateDataModel"/> is state.
        /// </summary>
        /// <value>
        ///   <c>true</c> if state; otherwise, <c>false</c>.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>11/22/20177:15 PM</TimeStamp>
        public bool State { get; set; }

        #region public
        /// <summary>
        /// Initializes a new instance of the <see cref="APIMolexZonePresentStateDataModel"/> class.
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>9/15/20174:58 PM</TimeStamp>
        public APIMolexZonePresentStateDataModel()
        {
            Sensors = new Dictionary<string, APIMolexSensorPresentStateDataModel>();
        }
        #endregion
    }
}
