﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              BeaconPaletteModel
///   Description:        This class will have properties to update palette value at beacon Zone Level
///   Author:             Amol Kulkarni                  
///   Date:               11/17/17
///---------------------------------------------------------------------------------------------
#endregion
namespace Molex.BACnet.Gateway.ServiceCore.Models
{
    /// <summary>
    /// This class will have properties to update RGB value at beacon zone Level
    /// </summary>
    /// <CreatedBy>Amol.Kulkarni</CreatedBy><TimeStamp>25-05-201706:20 PM</TimeStamp>
    internal class BeaconPaletteModel : APIUpdateZoneModel
    {
        public PaletteModel[] PaletteModelList { get; set; }
    }

    public class PaletteModel
    {
        /// <summary>
        /// Gets or sets the red.
        /// </summary>
        /// <value>
        /// The red.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>11/20/201711:35 AM</TimeStamp>
        public int red { get; set; }
        /// <summary>
        /// Gets or sets the green.
        /// </summary>
        /// <value>
        /// The green.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>11/20/201711:36 AM</TimeStamp>
        public int green { get; set; }
        /// <summary>
        /// Gets or sets the blue.
        /// </summary>
        /// <value>
        /// The blue.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>11/20/201711:36 AM</TimeStamp>
        public int blue { get; set; }
    }
}
