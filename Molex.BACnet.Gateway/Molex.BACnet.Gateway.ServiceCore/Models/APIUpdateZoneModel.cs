﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              APISpaceStateModel
///   Description:        This class will have properties to update Brightness value at Zone Level
///   Author:             Nazneen Zahid                  
///   Date:               05/26/17
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models
{
    /// <summary>
    /// This class will have properties to update Brightness value at Zone Level
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201706:20 PM</TimeStamp>
    public class APIUpdateZoneModel
    {

    }
}
