﻿
using Molex.BACnet.Gateway.ServiceCore.Constants;
using Molex.BACnet.Gateway.ServiceCore.Models.ObjectMap;
using Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel;
using System;
using System.Collections.Generic;
namespace Molex.BACnet.Gateway.ServiceCore.Models
{
    /// <summary>
    /// This class is used to pass different entity info along with configuration data
    /// </summary>
    /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>6/5/201711:18 AM</TimeStamp>
    public class ProcessorConfigurationModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessorConfigurationModel"/> class.
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>6/5/201711:18 AM</TimeStamp>
        public ProcessorConfigurationModel()
        {
            BACnetConfigurationData = new BACnetConfigurationModel();
            EntityBACnetCache = new Dictionary<string, EntityObject>();
            MappedObjectIds = new Dictionary<uint, Dictionary<string, List<uint>>>();
            RestoreStrategy = AppConstants.RestoreIDReclaimStrategy.Incremental;
            //LightScene = new string[AppConstants.LIGHTSCENE_MAX_LIMIT];
            UserBACnetObjects = new Dictionary<string, PersistedModels>();
            ZoneAlarmNotificationSetting = new List<ZoneObject>();
        }

        /// <summary>
        /// Gets or sets the persistent model.
        /// </summary>
        /// <value>
        /// The persistent model.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>8/2/20172:02 PM</TimeStamp>
        //public Dictionary<string, uint> EntityKeyDeviceIDMapping { get; set; }


        /// <summary>
        /// Gets or sets the mapped object ids.
        /// </summary>
        /// <value>
        /// The mapped object ids.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>6/26/20171:25 PM</TimeStamp>
        public Dictionary<uint, Dictionary<string, List<uint>>> MappedObjectIds { get; set; }

        /// <summary>
        /// Gets or sets the bacnet configuration data.
        /// </summary>
        /// <value>
        /// The ba cnet configuration data.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>6/5/20175:29 PM</TimeStamp>
        public BACnetConfigurationModel BACnetConfigurationData { get; set; }

        /// <summary>
        /// Gets or sets the object map. Cached data reading from mapping file
        /// </summary>
        /// <value>
        /// The object map.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>6/9/20175:29 PM</TimeStamp>
        public Dictionary<string, EntityObject> EntityBACnetCache { get; set; }

        /// <summary>
        /// Gets or sets the user ba cnet objects.
        /// </summary>
        /// <value>
        /// The user ba cnet objects.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>8/2/20176:53 PM</TimeStamp>
        public Dictionary<string, PersistedModels> UserBACnetObjects { get; set; }

        /// <summary>
        /// Gets or sets the location from controller.
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// Gets or sets the restore statery.
        /// </summary>
        /// <value>
        /// The restore statery. Incremental or Preserved
        /// </value>
        /// <CreatedBy>Rohit Galgali</CreatedBy>
        /// <TimeStamp>6/27/20173:10 PM</TimeStamp>
        public AppConstants.RestoreIDReclaimStrategy RestoreStrategy { get; set; }

        /// <summary>
        /// Gets or sets the light scene.
        /// </summary>
        /// <value>
        /// The light scene.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>04-07-201710:54 AM</TimeStamp>
        public string[] LightScene { get; set; }

        /// <summary>
        /// Gets or sets the user and general light scenes.
        /// </summary>
        /// <value>
        /// The user and general light scenes.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>11/22/201711:26 AM</TimeStamp>
        public string[] UserAndGeneralLightScenes { get; set; }

        /// <summary>
        /// Gets or sets the beacon light scenes.
        /// </summary>
        /// <value>
        /// The beacon light scenes.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>11/22/201711:26 AM</TimeStamp>
        public string[] BeaconLightScenes { get; set; }

        /// <summary>
        /// Gets or sets the notification class instance number for VRD objects.
        /// </summary>
        /// <value>
        /// The VRD notification instance number.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>8/30/20176:16 PM</TimeStamp>
        public Single VRDNotificationClassInstanceNumber { get; set; }

        /// <summary>
        /// Gets or sets the vd notification instance number for VD objects.
        /// </summary>
        /// <value>
        /// The vd notification instance number.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>8/31/20172:20 PM</TimeStamp>
        public Single VDNotificationClassInstanceNumber { get; set; }

        /// <summary>
        /// Gets or sets the zone alarm notification setting.
        /// </summary>
        /// <value>
        /// The zone alarm notification setting.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>4/12/20188:10 PM</TimeStamp>
        public List<ZoneObject> ZoneAlarmNotificationSetting { get; set; }
    }
}
