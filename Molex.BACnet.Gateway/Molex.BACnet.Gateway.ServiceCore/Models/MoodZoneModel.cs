﻿
namespace Molex.BACnet.Gateway.ServiceCore.Models
{
    /// <summary>
    /// MoodZoneModel for mood control
    /// </summary>
    /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>8/10/201712:13 PM</TimeStamp>
    internal class MoodZoneModel : APIUpdateZoneModel
    {
        /// <summary>
        /// This model is used for maintaining mood value in Zone Level 
        /// </summary>
        /// <value>
        /// The mood.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>8/10/201712:13 PM</TimeStamp>
        public string mood { get; set; }
    }
}
