﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              <ModelBase.cs>
///   Description:        <Base Model class for all Models>
///   Author:             Rupesh Saw                  
///   Date:               MM/DD/YY
///---------------------------------------------------------------------------------------------
#endregion

using System.ComponentModel;

namespace Molex.BACnet.Gateway.ServiceCore.Models
{
    /// <summary>
    /// Base Model class for all Models
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:38 PM</TimeStamp>
    /// <seealso cref="System.ComponentModel.INotifyPropertyChanged" />
    public abstract class ModelBase : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged Members

        /// <summary>
        /// Raised when a property on this object has a new value.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises this object's PropertyChanged event.
        /// </summary>
        /// <param name="propertyName">The property that has a new value.</param>
        protected virtual void NotifyPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                var e = new PropertyChangedEventArgs(propertyName);
                handler(this, e);
            }
        }

        #endregion // INotifyPropertyChanged Members
    }
}
