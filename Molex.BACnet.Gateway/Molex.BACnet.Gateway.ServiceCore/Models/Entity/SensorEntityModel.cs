﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              <SensorEntityModel.cs>
///   Description:        <This class will hold the Sensor related information like identifier,name fetched from Molex API Model>
///   Author:             Rupesh Saw                  
///   Date:               MM/DD/YY
///   Notes:              <Notes>
///---------------------------------------------------------------------------------------------
#endregion


using Molex.BACnet.Gateway.ServiceCore.Constants;
namespace Molex.BACnet.Gateway.ServiceCore.Models.Entity
{
    /// <summary>
    /// This class will hold the Sensor related information like identifier,name fetched from Molex API Model
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>26-05-201703:53 PM</TimeStamp>
    /// <seealso cref="Molex.BACnet.Gateway.ServiceCore.Models.Entity.EntityBaseModel" />
    public class SensorEntityModel : EntityBaseModel
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201707:00 PM</TimeStamp>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the type of the device.
        /// </summary>
        /// <value>
        /// The type of the device.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201707:00 PM</TimeStamp>
        public AppConstants.SensorType SensorType { get; set; }

        /// <summary>
        /// Gets or sets the proprietary grid x.
        /// </summary>
        /// <value>
        /// The proprietary grid x.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201707:00 PM</TimeStamp>
        public uint ProprietaryGridX { get; set; }

        /// <summary>
        /// Gets or sets the proprietary grid y.
        /// </summary>
        /// <value>
        /// The proprietary grid y.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201707:00 PM</TimeStamp>
        public uint ProprietaryGridY { get; set; }

        /// <summary>
        /// Gets or sets the role.
        /// </summary>
        /// <value>
        /// The role.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>21-06-201704:12 PM</TimeStamp>
        public string Role { get; set; }

        /// <summary>
        /// Gets or sets the type of the device.
        /// </summary>
        /// <value>
        /// The type of the device.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>21-06-201705:41 PM</TimeStamp>
        public string DeviceType { get; set; }
        #endregion


        #region Public Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="SensorEntityModel"/> class.
        /// </summary>
        public SensorEntityModel()
        {
            EntityType = Constants.AppConstants.EntityTypes.Sensor;
        }

        #endregion

    }
}
