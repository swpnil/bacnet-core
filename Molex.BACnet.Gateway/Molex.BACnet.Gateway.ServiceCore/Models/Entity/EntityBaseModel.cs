﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              EntityBaseModel.cs
///   Description:        This class will hold the Base Entity common information like identifier,name fetched from Molex API Model
///   Author:             Rupesh Saw                  
///   Date:               05/22/17
///---------------------------------------------------------------------------------------------
#endregion

using Molex.BACnet.Gateway.ServiceCore.Constants;
using Molex.StackDataObjects.Constants;
using System;
using System.Collections.Generic;

namespace Molex.BACnet.Gateway.ServiceCore.Models.Entity
{
    /// <summary>
    /// This class will hold the Base Entity common information like identifier,name fetched from Molex API Model
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:34 PM</TimeStamp>
    public class EntityBaseModel
    {

        #region Public Properties

        /// <summary>
        /// Gets or sets the type of the entity.
        /// </summary>
        /// <value>
        /// The type of the entity.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201706:59 PM</TimeStamp>
        public AppConstants.EntityTypes EntityType { get; set; }

        /// <summary>
        /// Gets or sets the parent.
        /// </summary>
        /// <value>
        /// The parent.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201706:59 PM</TimeStamp>
        public EntityBaseModel Parent { get; set; }

        /// <summary>
        /// Gets or sets the childs.
        /// </summary>
        /// <value>
        /// The childs.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201706:59 PM</TimeStamp>
        public List<EntityBaseModel> Childs { get; set; }

        /// <summary>
        /// Gets or sets the unique identifier.
        /// </summary>
        /// <value>
        /// The unique identifier.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201706:59 PM</TimeStamp>
        public String Guid { get; set; }

        /// <summary>
        /// Gets or sets the name of the entity.
        /// </summary>
        /// <value>
        /// The name of the entity.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201706:59 PM</TimeStamp>
        public string EntityName { get; set; }

        /// <summary>
        /// Gets or sets the object identifier.
        /// </summary>
        /// <value>
        /// The object identifier.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201706:59 PM</TimeStamp>
        public string EntityKey { get; set; }

        /// <summary>
        /// Gets or sets the object identifier.
        /// </summary>
        /// <value>
        /// The object identifier.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201706:59 PM</TimeStamp>
        public string MappingKey { get; set; }

        /// <summary>
        /// Gets or sets the ba cnet data.
        /// </summary>
        /// <value>
        /// The ba cnet data.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201706:59 PM</TimeStamp>
        public BACnetEntityModel BACnetData { get; set; }

        /// <summary>
        /// Get or set for timestamp
        /// </summary>
        public DateTime TimeStamp { get; set; }

        #endregion


        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="EntityBaseModel"/> class.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:34 PM</TimeStamp>
        public EntityBaseModel()
        {
            Childs = new List<EntityBaseModel>();
            BACnetData = new BACnetEntityModel();
        }

        #endregion
    }
}
