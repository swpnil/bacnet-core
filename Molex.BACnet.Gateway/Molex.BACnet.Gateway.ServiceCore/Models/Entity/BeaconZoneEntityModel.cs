﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              <BeaconZoneEntityModel.cs>
///   Description:        <This class will hold the Fixture Entity related information like identifier,name fetched from Molex API Model>
///   Author:             Amol Kulkarni                  
///   Date:               11/17/17
///---------------------------------------------------------------------------------------------
#endregion
namespace Molex.BACnet.Gateway.ServiceCore.Models.Entity
{
    /// <summary>
    /// This class property is used to getting beacon zone palette value(RGB)
    /// </summary>
    /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>11/20/201711:37 AM</TimeStamp>
    public class BeaconZoneEntityModel
    {
        /// <summary>
        /// Gets or sets the red.
        /// </summary>
        /// <value>
        /// The red.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>11/20/201711:38 AM</TimeStamp>
        public int Red { get; set; }
        /// <summary>
        /// Gets or sets the green.
        /// </summary>
        /// <value>
        /// The green.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>11/20/201711:38 AM</TimeStamp>
        public int Green { get; set; }
        /// <summary>
        /// Gets or sets the blue.
        /// </summary>
        /// <value>
        /// The blue.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>11/20/201711:38 AM</TimeStamp>
        public int Blue { get; set; }
    }
}
