﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              ZoneEntityModel.cs
///   Description:        This class will hold the Zone related information like identifier,name fetched from Molex API Model
///   Author:             Rupesh Saw                  
///   Date:               05/22/17
///---------------------------------------------------------------------------------------------
#endregion

using System.Collections.Generic;
using Molex.BACnet.Gateway.ServiceCore.Constants;
using Molex.BACnet.Gateway.ServiceCore.Models.MolexAPIModels;
namespace Molex.BACnet.Gateway.ServiceCore.Models.Entity
{
    /// <summary>
    /// This class will hold the Zone related information like identifier,name fetched from Molex API Model
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:36 PM</TimeStamp>
    /// <seealso cref="Molex.BACnet.Gateway.ServiceCore.Models.Entity.EntityBaseModel" />
    public class ZoneEntityModel : EntityBaseModel
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the proprietary grid x.
        /// </summary>
        /// <value>
        /// The proprietary grid x.
        /// </value>
        /// <CreatedBy>Rohit Galgali</CreatedBy><TimeStamp>25-05-201706:59 PM</TimeStamp>
        public uint ProprietaryGridX { get; set; }

        /// <summary>
        /// Gets or sets the proprietary grid y.
        /// </summary>
        /// <value>
        /// The proprietary grid y.
        /// </value>
        /// <CreatedBy>Rohit galgali</CreatedBy><TimeStamp>25-05-201706:59 PM</TimeStamp>
        public uint ProprietaryGridY { get; set; }

        /// <summary>
        /// Gets or sets the type of the zone.
        /// </summary>
        /// <value>
        /// The type of the zone.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>2/16/201811:48 AM</TimeStamp>
        public AppConstants.ZoneTypes ZoneType { get; set; }


        /// <summary>
        /// Gets or sets the zone identifier.
        /// </summary>
        /// <value>
        /// The zone identifier.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201707:00 PM</TimeStamp>
        public string ZoneId { get; set; }

        /// <summary>
        /// Gets or sets the fixtures.
        /// </summary>
        /// <value>
        /// The fixtures.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201707:00 PM</TimeStamp>
        public List<FixtureEntityModel> Fixtures { get; set; }

        /// <summary>
        /// Gets or sets the sensors.
        /// </summary>
        /// <value>
        /// The sensors.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201707:00 PM</TimeStamp>
        public List<SensorEntityModel> Sensors { get; set; }

        /// <summary>
        /// Beacons
        /// </summary>
        public List<Beacon> Beacons { get; set; }

        /// <summary>
        /// Blinds
        /// </summary>
        public List<Blind> Blinds { get; set; }

        /// <summary>
        /// Gets or sets the light scene.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>20-07-201706:51 PM</TimeStamp>
        public List<string> LightScene { get; set; }

        /// <summary>
        /// Gets or Sets the Beacon Palettes
        /// </summary>
        public List<string> BeaconPalettes { get; set; }

        #endregion

        #region Public Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="ZoneEntityModel"/> class.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>02-06-201712:12 PM</TimeStamp>
        public ZoneEntityModel()
        {
            EntityType = Constants.AppConstants.EntityTypes.Zone;
            Fixtures = new List<FixtureEntityModel>();
            Sensors = new List<SensorEntityModel>();
            Blinds = new List<Blind>();
            Beacons = new List<Beacon>();
            LightScene = new List<string>();
        }

        #endregion


    }
}
