﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              BACnetEntityModel.cs
///   Description:        This class will hold the BACnet information related to floor,Building entity
///   Author:             Mandar Bhong                  
///   Date:               05/19/17
///---------------------------------------------------------------------------------------------
#endregion

using Molex.BACnet.Gateway.ServiceCore.Constants;
using Molex.BACnet.Gateway.ServiceCore.Models.BACnet;
using Molex.StackDataObjects.Constants;
using System.Collections.Generic;

namespace Molex.BACnet.Gateway.ServiceCore.Models.Entity
{
    /// <summary>
    /// Base BACnetEntity Model storing required Attribute mapped with BACnet Stack
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:32 PM</TimeStamp>
    public class BACnetEntityModel
    {
        #region public properties
        /// <summary>
        /// Gets or sets the device identifier. Device Id associated with vd and vr
        /// </summary>
        /// <value>
        /// The device identifier.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201706:58 PM</TimeStamp>
        public int DeviceID { get; set; }


        /// <summary>
        /// Gets or sets the system status.
        /// </summary>
        /// <value>
        /// The system status.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>7/3/201712:31 PM</TimeStamp>
        public BacnetDeviceStatus SystemStatus { get; set; }

        /// <summary>
        /// Gets or sets the object present value. Maintains the dictonary of objecttype as string
        /// versus ObjectDetails
        /// </summary>
        /// <value>
        /// The Zone present value.
        /// </value>
        /// <CreatedBy>Rohit Galgali</CreatedBy>
        /// <TimeStamp>5/26/20172:38 PM</TimeStamp>
        public Dictionary<string, BACnetObjectDetails> ObjectDetails { get; set; }

        /// <summary>
        /// Gets or sets the name from the design tool.
        /// </summary>
        /// <value>
        /// The name of the dt.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>8/31/20173:02 PM</TimeStamp>
        public string  DTName { get; set; }

        #endregion


        #region Public Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="BACnetEntityModel"/> class.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:32 PM</TimeStamp>
        public BACnetEntityModel()
        {
            DeviceID = AppConstants.DEFAULT_DEVICE_ID;
            ObjectDetails = new Dictionary<string, BACnetObjectDetails>();
        }

        #endregion


    }
}
