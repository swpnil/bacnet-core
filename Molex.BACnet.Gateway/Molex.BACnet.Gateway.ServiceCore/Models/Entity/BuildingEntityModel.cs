﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              BuildingEntityModel.cs
///   Description:        This class will hold the Building Entity related information
///   Author:             Rupesh Saw                  
///   Date:               05/19/17
///---------------------------------------------------------------------------------------------
#endregion


using System;
using System.Collections.Generic;

namespace Molex.BACnet.Gateway.ServiceCore.Models.Entity
{
    /// <summary>
    /// This class will hold the Building Entity related information
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:33 PM</TimeStamp>
    /// <seealso cref="Molex.BACnet.Gateway.ServiceCore.Models.Entity.EntityBaseModel" />
    internal class BuildingEntityModel : EntityBaseModel
    {
        #region Public Properties
        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>30-05-201701:45 PM</TimeStamp>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the subordinate list.
        /// </summary>
        /// <value>
        /// The subordinate list.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>30-05-201701:45 PM</TimeStamp>
        public object SubordinateList { get; set; }

        /// <summary>
        /// Gets or sets the subordinate annotations.
        /// </summary>
        /// <value>
        /// The subordinate annotations.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>30-05-201701:45 PM</TimeStamp>
        public string SubordinateAnnotations { get; set; }

        /// <summary>
        /// Gets or sets the location.
        /// </summary>
        /// <value>
        /// The location.
        /// </value>
        public string Location { get; set; }
        #endregion

        #region Public Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="BuildingEntityModel"/> class.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:33 PM</TimeStamp>
        public BuildingEntityModel()
        {
            EntityType = Constants.AppConstants.EntityTypes.Building;
        }

        #endregion
    }
}
