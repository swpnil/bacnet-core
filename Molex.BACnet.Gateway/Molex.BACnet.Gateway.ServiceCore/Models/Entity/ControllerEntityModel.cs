﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              ControllerEntityModel.cs
///   Description:        This class will hold the Controller Entity related information like identifier,name fetched from Molex API Model
///   Author:             Rupesh Saw                  
///   Date:               05/22/17
///---------------------------------------------------------------------------------------------
#endregion

using Molex.BACnet.Gateway.ServiceCore.Models.MolexAPIModels;
using System;
using System.Collections.Generic;
namespace Molex.BACnet.Gateway.ServiceCore.Models.Entity
{
    /// <summary>
    /// This class will hold the Controller Entity related information like identifier,name fetched from Molex API Model
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:33 PM</TimeStamp>
    /// <seealso cref="Molex.BACnet.Gateway.ServiceCore.Models.Entity.EntityBaseModel" />
    public class ControllerEntityModel : EntityBaseModel
    {

        #region Public Properties
        /// <summary>
        /// Gets or sets the location.
        /// </summary>
        /// <value>
        /// The location.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>30-05-201705:07 PM</TimeStamp>
        public string Location { get; set; }

        /// <summary>
        /// Gets or sets the light scenes.
        /// </summary>
        public string[] LightScenes { get; set; }

        /// <summary>
        /// Gets or sets the molex API buildings.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>13-07-201703:21 PM</TimeStamp>
        //public List<MolexAPIBuildingModel> MolexAPIBuildings { get; set; }

        /// <summary>
        /// Gets or sets the molex API zone model.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>13-07-201703:21 PM</TimeStamp>
        //public MolexAPIZoneModel[] MolexAPIZoneModel { get; set; }

        /// <summary>
        /// Gets or sets the molex API lightscene model.
        /// </summary>
        /// <value>
        /// The molex API lightscene model.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>17-07-201704:52 PM</TimeStamp>
        public MolexAPILightsceneModel[] MolexAPILightsceneModel { get; set; }

        #endregion
        #region Public Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ControllerEntityModel"/> class.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:33 PM</TimeStamp>
        public ControllerEntityModel()
        {
            EntityType = Constants.AppConstants.EntityTypes.Controller;
            //MolexAPIBuildings = new List<MolexAPIBuildingModel>();
            LightScenes = new string[Constants.AppConstants.LIGHTSCENE_MAX_LIMIT];
        }

        #endregion

    }
}
