﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              FloorEntityModel.cs
///   Description:        This class will hold the Floor related information like identifier,name fetched from Molex API Model
///   Author:             Rupesh Saw                  
///   Date:               05/22/17
///---------------------------------------------------------------------------------------------
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Models.Entity
{
    /// <summary>
    ///  This class will hold the Floor related information like identifier,name fetched from Molex API Model
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:35 PM</TimeStamp>
    /// <seealso cref="Molex.BACnet.Gateway.ServiceCore.Models.Entity.EntityBaseModel" />
    class FloorEntityModel : EntityBaseModel
    {

        #region Public Properties

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>30-05-201701:45 PM</TimeStamp>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the subordinate list.
        /// </summary>
        /// <value>
        /// The subordinate list.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>30-05-201701:45 PM</TimeStamp>
        public object SubordinateList { get; set; }

        /// <summary>
        /// Gets or sets the subordinate annotations.
        /// </summary>
        /// <value>
        /// The subordinate annotations.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>30-05-201701:45 PM</TimeStamp>
        public string SubordinateAnnotations { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="FloorEntityModel"/> class.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:35 PM</TimeStamp>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:34 PM</TimeStamp>
        public FloorEntityModel()
        {
            EntityType = Constants.AppConstants.EntityTypes.Floor;
        }

        #endregion


    }
}
