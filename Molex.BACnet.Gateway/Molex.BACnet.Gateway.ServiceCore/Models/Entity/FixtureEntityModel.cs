﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              <FixtureEntityModel.cs>
///   Description:        <This class will hold the Fixture Entity related information like identifier,name fetched from Molex API Model>
///   Author:             Rupesh Saw                  
///   Date:               05/22/17
///---------------------------------------------------------------------------------------------
#endregion

using System;
namespace Molex.BACnet.Gateway.ServiceCore.Models.Entity
{
    /// <summary>
    /// This class will hold the Fixture Entity related information like identifier,name fetched from Molex API Model
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:34 PM</TimeStamp>
    /// <seealso cref="Molex.BACnet.Gateway.ServiceCore.Models.Entity.EntityBaseModel" />
    public class FixtureEntityModel : EntityBaseModel
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201706:59 PM</TimeStamp>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the proprietary grid x.
        /// </summary>
        /// <value>
        /// The proprietary grid x.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201706:59 PM</TimeStamp>
        public uint ProprietaryGridX { get; set; }

        /// <summary>
        /// Gets or sets the proprietary grid y.
        /// </summary>
        /// <value>
        /// The proprietary grid y.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201706:59 PM</TimeStamp>
        public uint ProprietaryGridY { get; set; }

        /// <summary>
        /// Gets or sets the user data.
        /// </summary>
        /// <value>
        /// The user data.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>07-06-201710:48 AM</TimeStamp>
        public Single UserData { get; set; }

        #endregion


        #region Public Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="FixtureEntityModel"/> class.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:34 PM</TimeStamp>
        public FixtureEntityModel()
        {
            EntityType = Constants.AppConstants.EntityTypes.Fixture;
        }

        #endregion


    }
}
