﻿using Molex.BACnet.Gateway.Log;
using Molex.BACnet.Gateway.ServiceCore.Constants;
using Molex.BACnet.Gateway.ServiceCore.Managers;
using Molex.BACnet.Gateway.ServiceCore.Repository;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Controllers
{
    /// <summary>
    /// This is an Simple HTTP request handler for handling the data published by API to Gateway
    /// </summary>
    public static class GatewayHTTPController
    {
        /// <summary>
        /// Handler Thread will be the count of handlers which are getting created for every HTTP request
        /// </summary>
        private static int HandlerThread = 0;

        /// <summary>
        /// listener for listening to HTTP requests
        /// </summary>
        private static HttpListener listener;

        /// <summary>
        /// Start method for starting the listener & adding the handlers to HTTP request
        /// </summary>
        public static void Start(string IPaddress)
        {
            try
            {
                string url = string.Format(AppConstants.SUBSCRIPTION_URL, IPaddress) + "/";
                HandlerThread = AppCoreRepo.Instance.TotalDevicesCount * 7 * 2;//Total No of request handlers
                //Total Devices will include all the sensors , fixtures , blinds & beacons in the given building 
                //7 leads to subscription types
                //2 is just multiplying factor
                if (HandlerThread < 400)
                    HandlerThread = 400;//if still the value is less than Web API default MaxConCurrentThread.
                listener = new HttpListener();
                listener.Prefixes.Add(url);

                if (listener.IsListening)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "GatewayHTTPController:Start Exception Received HttpListener can not subscribe");
                    return;
                }

                listener.Start();

                Task.Run(() =>
                {
                    ReceiveIncomingPackages();
                });
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "GatewayHTTPController:Start Exception Received");
            }
        }

        /// <summary>
        /// ReceiveIncomingPackages This Method will receive the incoming packages
        /// </summary>
        private static void ReceiveIncomingPackages()
        {
            while (true)
            {
                try
                {
                    if (HandlerThread == 0)
                        return;
                    if (!listener.IsListening)
                        return;
                    HttpListenerContext context = listener.GetContext();
                    try
                    {
                        #region Read the request
                        using (Stream networkStream = context.Request.InputStream)
                        {
                            Task.Run(() =>
                            {
                                string request = new StreamReader(networkStream).ReadToEnd();
                                if (!string.IsNullOrEmpty(request))
                                {
                                    Task.Run(() =>
                                    {
                                        MonitoringDataManager _MonitoringDataManager = new MonitoringDataManager();
                                        _MonitoringDataManager.Process(request.Trim());
                                        _MonitoringDataManager = null;
                                    });
                                }
                            });
                        }
                        #endregion

                        #region Send the response
                        using (Stream output = context.Response.OutputStream)
                        {
                            // Construct a response.
                            string outputData = string.Empty;
                            outputData = "HTTP/1.1 200 OK" + System.Environment.NewLine;
                            outputData = outputData + "Date: " + DateTime.Now.ToString() + System.Environment.NewLine;
                            outputData = outputData + "Connection: close" + System.Environment.NewLine;
                            outputData = outputData + "Content-Type: text/plain" + System.Environment.NewLine;
                            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(outputData);
                            // Get a response stream and write the response to it.
                            context.Response.ContentLength64 = buffer.Length;
                            output.Write(buffer, 0, buffer.Length);
                            buffer = null;
                        }
                        // You must close the output stream. 
                        #endregion

                        #region Disposing The Stream
                        if (context.Request.InputStream != null)
                        {
                            context.Request.InputStream.Flush();
                            context.Request.InputStream.Close();
                            context.Request.InputStream.Dispose();
                        }
                        if (context.Response.OutputStream != null)
                        {
                            context.Response.OutputStream.Flush();
                            context.Response.OutputStream.Close();
                            context.Response.OutputStream.Dispose();
                        }
                        if (context != null)
                        {
                            context = null;
                        }
                        #endregion
                    }
                    catch (Exception)
                    {
                        //Unknown Package Exception
                        //Logger.Instance.Log(ex, LogLevel.ERROR, "GatewayHTTPController:ProcessRequestHandler Exception Received while sending data to MonitorDataManager");
                    }
                }
                catch (Exception)
                {
                    //Unknown Package Exception
                    //Logger.Instance.Log(ex, LogLevel.ERROR, "GatewayHTTPController:ProcessRequestHandler Exception Received");
                }
            }
        }

        /// <summary>
        /// This method will stop the HTTP listener
        /// </summary>
        public static void Stop()
        {
            try
            {
                if (listener == null)
                    return;
                if (listener.IsListening)
                    listener.Stop();
                listener.Close();
                HandlerThread = 0;
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "GatewayHTTPController:Stop Exception Received");
            }
        }
    }
}
