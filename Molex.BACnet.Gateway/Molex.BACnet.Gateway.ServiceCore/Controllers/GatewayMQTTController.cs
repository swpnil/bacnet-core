﻿using Molex.BACnet.Gateway.Log;
using Molex.BACnet.Gateway.ServiceCore.Constants;
using Molex.BACnet.Gateway.ServiceCore.Helpers;
using Molex.BACnet.Gateway.ServiceCore.Managers;
using Molex.BACnet.Gateway.ServiceCore.Models.Entity;
using Molex.BACnet.Gateway.ServiceCore.Models.MolexAPIModels;
using Molex.BACnet.Gateway.ServiceCore.Repository;
using Molex.StackDataObjects.Constants;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace Molex.BACnet.Gateway.ServiceCore.Controllers
{
    /// <summary>
    /// GatewayMQTTController- responsible for receiving MQTT packages , thier subscription & unsubscription
    /// </summary>
    public static class GatewayMQTTController
    {
        /// <summary>
        /// client MQTT connection client
        /// </summary>
        private static MqttClient client;

        /// <summary>
        /// Retry Count Details
        /// </summary>
        private static int MaxRetryCount = 5;
        private static int RetryCount = 0;

        /// <summary>
        /// client ID
        /// </summary>
        private static string clientId;

        /// <summary>
        /// Will Hold the list of Topics
        /// </summary>
        private static List<string> SubscriptionTopics = null;

        /// <summary>
        /// Class Constructor
        /// </summary>
        static GatewayMQTTController()
        {
            SubscriptionTopics = new List<string>();
            SubscriptionTopics.Add(AppConstants.SubscriptionType.status.ToString());
            SubscriptionTopics.Add(AppConstants.SubscriptionType.notification.ToString());
            SubscriptionTopics.Add(AppConstants.SubscriptionType.sensorData.ToString());
            SubscriptionTopics.Add(AppConstants.SubscriptionType.configurations.ToString());
            SubscriptionTopics.Add(AppConstants.SubscriptionType.properties.ToString());

            SubscriptionTopics.Add("average/minute/CT");
            SubscriptionTopics.Add("average/minute/AL");
            SubscriptionTopics.Add("average/minute/HUM");
            SubscriptionTopics.Add("average/minute/TEMP");
            SubscriptionTopics.Add("average/minute/AQ");
            SubscriptionTopics.Add("floor/power/total");
        }

        /// <summary>
        /// Will Start the MQTT while subscribing
        /// </summary>
        public static void Start()
        {
            try
            {
                RetryCount++;
                string BrokerAddress = AppCoreRepo.Instance.BACnetConfigurationData.MolexAPIUrl;
                string address = BrokerAddress.Replace("/transcend/api/v1/", "").Replace("http://", "");
                address = address.Substring(0, address.LastIndexOf(':'));
                client = new MqttClient(address, 1883, false, MqttSslProtocols.TLSv1_1, null, null);
                client.ConnectionClosed += client_ConnectionClosed;
                // register a callback-function (we have to implement, see below) which is called by the library when a message was received
                client.MqttMsgPublishReceived += client_MqttMsgPublishReceived;
                // use a unique id as client id, each time we start the application
                clientId = Guid.NewGuid().ToString();
                AppCoreRepo.Instance.ClientID = clientId;
                client.Connect(clientId, AppCoreRepo.Instance.BACnetConfigurationData.UserName, AppCoreRepo.Instance.BACnetConfigurationData.Password);
                if (!client.IsConnected)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "GatewayMQTTController:Start client connection failed");
                    return;
                }

                string topic = string.Empty;
                foreach (var item in SubscriptionTopics)
                {
                    if (topic.Contains(AppConstants.SubscriptionType.average.ToString())
                      || topic.Contains(AppConstants.SubscriptionType.power.ToString()))
                        topic = AppCoreRepo.Instance.MolexProjectID + "/" + item;
                    else
                        topic = AppCoreRepo.Instance.MolexProjectID + "/" + item + "/#";
                    // subscribe to the topic with QoS 2
                    client.Subscribe(new string[] { topic }, new byte[] { 2 });   // we need arrays as parameters 
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "GatewayMQTTController:Start Exception Received");
                SetGatewayNonOperational();
            }
        }

        static void client_ConnectionClosed(object sender, EventArgs e)
        {
            try
            {
                if (RetryCount >= MaxRetryCount)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "GatewayMQTTController:client_ConnectionClosed Retry Attempt Ended Cannot RE-Connect");
                    SetGatewayNonOperational();
                    return;
                }
                //Debug.WriteLine("client_ConnectionClosed");             
                Stop();
                //Debug.WriteLine("Stop Done");
                Start();
                //Debug.WriteLine("Start Done");
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "GatewayMQTTController:client_ConnectionClosed Exception Received");
            }
        }

        /// <summary>
        /// This method will set the Gateway VRD & VD's to non operational
        /// </summary>
        private static void SetGatewayNonOperational()
        {
            Logger.Instance.Log(LogLevel.ERROR, "GatewayMQTTController:SetGatewayNonOperational will set the Gateway VRD & VD's to non operational");
            string bacnetObjectKey = CommonHelper.GenerateBACnetObjectMappedID(AppCoreRepo.Instance.BACnetConfigurationData.ControllerId, BacnetObjectType.ObjectDevice, AppCoreRepo.Instance.BACnetConfigurationData.ControllerId);
            EntityBaseModel entityBaseModel = AppCoreRepo.Instance.BACnetObjectEntityLookUp[bacnetObjectKey];
            MonitoredDataStackUpdateHelper.UpdateSystemStatus(entityBaseModel, BacnetDeviceStatus.StatusNonOperational);

            foreach (string zoneID in AppCoreRepo.Instance.ZonesLookUp.Keys)
            {
                entityBaseModel = AppCoreRepo.Instance.ZonesLookUp[zoneID];
                MonitoredDataStackUpdateHelper.UpdateSystemStatus(entityBaseModel, BacnetDeviceStatus.StatusNonOperational);
            }
        }

        /// <summary>
        /// This method will stop the MQTT
        /// </summary>
        public static void Stop()
        {
            try
            {
                if (client == null)
                    return;
                client.MqttMsgPublishReceived -= client_MqttMsgPublishReceived;
                foreach (var item in SubscriptionTopics)
                {
                    string Topic = AppCoreRepo.Instance.MolexProjectID + "/" + item + "/#";
                    // subscribe to the topic with QoS 2
                    client.Unsubscribe(new string[] { Topic });   // we need arrays as parameters 
                }
                if (client.IsConnected)
                    client.Disconnect();
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "GatewayMQTTController:Stop Exception Received");
            }
        }

        // this code runs when a message was received
        private static void client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
        {
            //GC.KeepAlive(client);
            string ReceivedMessage = Encoding.UTF8.GetString(e.Message);
            //Debug.WriteLine(ReceivedMessage);
            Task.Run(() =>
            {
                RetryCount = 0;
                ProcessMQTTMessage(e, ReceivedMessage);
            });
        }

        /// <summary>
        /// This will process MQTT frames & will send it to Queue for Monitoring
        /// </summary>
        /// <param name="e"></param>
        /// <param name="ReceivedMessage"></param>
        private static void ProcessMQTTMessage(MqttMsgPublishEventArgs e, string ReceivedMessage)
        {
            try
            {
                if (string.IsNullOrEmpty(ReceivedMessage))//Check if message is valid
                    return;
                if (e.DupFlag)//Check if it's duplicate
                    return;
                MQTTAPIPublishModel queueItem = JsonConvert.DeserializeObject<MQTTAPIPublishModel>(ReceivedMessage.ToString());

                #region Commenting this Logic
                //The reason behind commenting the below check is might old date data is required at start of the Gateway
                //but RESTAT PROJECT NOTIFICATION should be there only with LATEST DATE so date & time
                //validation is there at PROJECT-NOTIFICATION Section
                //DateTime additionalInfo = Convert.ToDateTime(queueItem.timestamp.ToString());
                //if (additionalInfo.Date != DateTime.Now.Date)
                //    return; 
                #endregion

                foreach (var item in SubscriptionTopics)
                {
                    if (e.Topic.ToUpperInvariant().Contains(item.ToUpperInvariant()))
                    {
                        AppConstants.SubscriptionType subsType;
                        if (Enum.TryParse(item, true, out subsType))
                        {
                            MolexAPIPublishModel _MolexAPIPublishModel = new MolexAPIPublishModel();
                            _MolexAPIPublishModel.topic = subsType.ToString();
                            _MolexAPIPublishModel.timestamp = queueItem.timestamp;
                            _MolexAPIPublishModel.data = queueItem.response;
                            Task.Run(() =>
                            {
                                MonitoringDataManager _MonitoringDataManager = new MonitoringDataManager();
                                _MonitoringDataManager.Process(JsonConvert.SerializeObject(_MolexAPIPublishModel));
                                _MonitoringDataManager = null;
                            });
                        }
                        else
                        {
                            Logger.Instance.Log(LogLevel.ERROR, "GatewayMQTTController:ProcessMQTTMessage Unknown Topic " + item);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "GatewayMQTTController:ProcessMQTTMessage Exception Received");
            }
        }
    }

    /// Generic class to get api publish data
    /// </summary>
    /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/25/20182:16 PM</TimeStamp>
    public class MQTTAPIPublishModel
    {

        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        /// <value>
        /// The data.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/25/20182:18 PM</TimeStamp>
        public JToken response { get; set; }


        /// <summary>
        /// get or set time stamp
        /// </summary>
        public string timestamp { get; set; }
    }
}
