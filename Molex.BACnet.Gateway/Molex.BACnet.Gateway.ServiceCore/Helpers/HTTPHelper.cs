﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              HTTPHelper
///   Description:        To Manage Web API GET PUT operations
///   Author:             Nazneen Zahid                  
///   Date:               05/22/17
///---------------------------------------------------------------------------------------------
#endregion

using Molex.BACnet.Gateway.ServiceCore.Constants;
using Molex.BACnet.Gateway.ServiceCore.Controllers;
using Molex.BACnet.Gateway.ServiceCore.Models;
using Molex.BACnet.Gateway.ServiceCore.Models.SubsriberModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Helpers
{
    /// <summary>
    /// This is Helper Class which can be used across the application for managing GET and PUT Operation on TS
    /// </summary>
    public class HTTPHelper
    {
        /// <summary>
        /// It will use to call HTTP API CoreSync Server and returns JSON data string
        /// </summary>
        /// <param name="url">Project API CoreSync server URL</param>
        /// <param name="apiMolexParameterModel">The apiMolexParameterModel </param>
        /// <returns>returns Json string </returns>
        public static async Task<ResultModel<string>> GetWebAPIAsync(string url, APIMolexParameterModel apiMolexParameterModel)
        {
            ResultModel<string> result = new ResultModel<string>();
            try
            {
                string strJsonString = string.Empty;

                using (var client = SetAuthenticationOnURL(url, apiMolexParameterModel.UserName, apiMolexParameterModel.Password))
                {
                    HttpResponseMessage response = await client.GetAsync(url);
                    GetAPIRespResult(response, url, result);
                }
            }
            catch (HttpRequestException ex)
            {
                Log.Logger.Instance.Log(ex, Log.LogLevel.ERROR);
                Log.Logger.Instance.Log(Log.LogLevel.ERROR, "Unable to reach the requested URL: " + url);
                result.IsSuccess = false;
                result.Error = new ErrorModel();
                result.Error.ErrorCategory = AppConstants.ERROR_CATEGORY_SERVER;
                result.Error.ErrorCode = AppConstants.ERROR_CATEGORY_SERVER_UNAVAILABLE;
                result.Error.OptionalMessage = ex.Message;
            }
            catch (TaskCanceledException ex)
            {
                Log.Logger.Instance.Log(ex, Log.LogLevel.ERROR);
                Log.Logger.Instance.Log(Log.LogLevel.ERROR, "Task canceled(timeout) for the requested URL: " + url);
                result.IsSuccess = false;
                result.Error = new ErrorModel();
                result.Error.ErrorCategory = AppConstants.ERROR_CATEGORY_SERVER;
                result.Error.ErrorCode = AppConstants.ERROR_CATEGORY_SERVER_UNAVAILABLE;
                result.Error.OptionalMessage = ex.Message;
            }
            catch (Exception ex)
            {
                SetRespOnException(result, url, ex);
            }

            return result;
        }


        ///// <summary>
        ///// It will use to update Brightness value at zone Level
        ///// </summary>
        ///// <param name="url">SpaceState API CoreSync server URL </param>
        ///// <param name="aPISpaceStateModel">It contains Version information and Brightness value to update</param>
        ///// <param name="apiMolexParameterModel">It contains credentials of molex  api</param>
        ///// <returns>returns success or failure result</returns>
        //public static async Task<ResultModel<string>> PutWebAPIAsync(string url, object aPISpaceStateModel, APIMolexParameterModel apiMolexParameterModel)
        //{
        //    ResultModel<string> result = new ResultModel<string>();

        //    try
        //    {
        //        using (HttpClient client = SetAuthenticationOnURL(url, apiMolexParameterModel.UserName, apiMolexParameterModel.Password))
        //        {
        //            HttpResponseMessage response = await client.PutAsJsonAsync(url, aPISpaceStateModel);
        //            GetAPIRespResult(response, url, result);
        //            Log.Logger.Instance.Log(Log.LogLevel.DEBUG, "URL:" + url + ", StatusCode:" + response.StatusCode);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        SetRespOnException(result, url, ex);
        //    }

        //    return result;
        //}

        /// <summary>
        /// This is an PUT request performed using httpWebRequest object instead of HttpClient
        /// </summary>
        /// <param name="url"></param>
        /// <param name="data"></param>
        /// <param name="apiMolexParameterModel"></param>
        /// <returns></returns>
        public static ResultModel<string> PutHttpWebRequest(string url, APIUpdateZoneModel data, APIMolexParameterModel apiMolexParameterModel)
        {
            ResultModel<string> result = new ResultModel<string>();
            HttpWebRequest httpWebRequest = SetHTTPPutAuthenticationParameters(url, apiMolexParameterModel);
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = string.Empty;
                if (data as BeaconPaletteModel != null)
                    json = Newtonsoft.Json.JsonConvert.SerializeObject((data as BeaconPaletteModel).PaletteModelList);
                else
                    json = Newtonsoft.Json.JsonConvert.SerializeObject(data);
                streamWriter.Write(json);
            }
            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            GetAPIRespResult(httpResponse, url, result);
            LogHttpPutRequestDetails(result, url, data);
            Log.Logger.Instance.Log(Log.LogLevel.DEBUG, "URL:" + url + ", StatusCode:" + httpResponse.StatusCode);
            return result;
        }

        /// <summary>
        /// This is an PUT request performed using httpWebRequest object instead of HttpClient
        /// </summary>
        /// <param name="url"></param>
        /// <param name="data"></param>
        /// <param name="apiMolexParameterModel"></param>
        /// <returns></returns>
        public static ResultModel<string> CustomHttpWebRequest(string url, string method, APIMolexParameterModel apiMolexParameterModel)
        {
            ResultModel<string> result = new ResultModel<string>();
            HttpWebRequest httpWebRequest = SetCustomHTTPAuthenticationParameters(url, method, apiMolexParameterModel);
            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            GetAPIRespResult(httpResponse, url, result);
            Log.Logger.Instance.Log(Log.LogLevel.DEBUG, "URL:" + url + ", StatusCode:" + httpResponse.StatusCode);
            return result;
        }

        /// <summary>
        /// This method will log the failed http put request details
        /// </summary>
        /// <param name="requestResult"></param>
        /// <param name="url"></param>
        /// <param name="inputModel"></param>
        private static void LogHttpPutRequestDetails(ResultModel<string> requestResult, string url, APIUpdateZoneModel inputModel)
        {
            try
            {
                if (requestResult == null)
                    return;
                if (string.IsNullOrEmpty(url))
                    return;
                if (inputModel == null)
                    return;
                if (!requestResult.IsSuccess)
                {
                    if (requestResult.Error == null)
                        return;
                    string message = "ErrorCode: " + requestResult.Error.ErrorCode.ToString() + System.Environment.NewLine +
                        "ErrorCategory: " + requestResult.Error.ErrorCategory.ToString() + System.Environment.NewLine +
                        "OptionalMessage: " + requestResult.Error.OptionalMessage.ToString() + System.Environment.NewLine +
                        "URL: " + url.ToString() + System.Environment.NewLine +
                        "JSON Request: " + Newtonsoft.Json.JsonConvert.SerializeObject(inputModel);

                    Log.Logger.Instance.Log(Log.LogLevel.ERROR, message);
                }
                else
                {
                    if (string.IsNullOrEmpty(requestResult.Data))
                        return;
                    Molex.BACnet.Gateway.ServiceCore.Models.APIModels.MolexAPIResponseModel _responseData = Newtonsoft.Json.JsonConvert.DeserializeObject<Molex.BACnet.Gateway.ServiceCore.Models.APIModels.MolexAPIResponseModel>(requestResult.Data);
                    if (_responseData == null)
                        return;
                    if (!_responseData.status)
                    {
                        string message = "Response Data Received: " + Newtonsoft.Json.JsonConvert.SerializeObject(_responseData) + System.Environment.NewLine +
                       "URL: " + url.ToString() + System.Environment.NewLine +
                       "JSON Request: " + Newtonsoft.Json.JsonConvert.SerializeObject(inputModel);
                        Log.Logger.Instance.Log(Log.LogLevel.ERROR, message);
                    }
                }
            }
            catch
            {
            }
        }

        /// <summary>
        /// This method will set the authentication Parameters for HTTP web PUT request
        /// </summary>
        /// <param name="url"></param>
        /// <param name="apiMolexParameterModel"></param>
        /// <returns></returns>
        private static HttpWebRequest SetHTTPPutAuthenticationParameters(string url, APIMolexParameterModel apiMolexParameterModel)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.Headers.Clear();
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Accept = "Accept=application/json";
            httpWebRequest.Method = "PUT";
            String username = apiMolexParameterModel.UserName;
            String password = apiMolexParameterModel.Password;
            String encoded = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(username + ":" + password));
            httpWebRequest.Headers.Add("Authorization", "Basic " + encoded);
            return httpWebRequest;
        }

        /// <summary>
        /// This method will set the authentication Parameters for HTTP web PUT request
        /// </summary>
        /// <param name="url"></param>
        /// <param name="apiMolexParameterModel"></param>
        /// <returns></returns>
        private static HttpWebRequest SetCustomHTTPAuthenticationParameters(string url, string method, APIMolexParameterModel apiMolexParameterModel)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.Headers.Clear();
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Accept = "Accept=application/json";
            httpWebRequest.Method = method.ToUpperInvariant();
            String username = apiMolexParameterModel.UserName;
            String password = apiMolexParameterModel.Password;
            String encoded = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(username + ":" + password));
            httpWebRequest.Headers.Add("Authorization", "Basic " + encoded);
            return httpWebRequest;
        }

        /// <summary>
        /// This method is used to fetch Space State Json data for Zone List from the CoreSync Server
        /// </summary>
        /// <param name="zoneId">It is used to fetch data from the SpaceState CoreSync Server</param>
        /// <param name="url">SpaceState API CoreSync server URL  </param>
        /// <param name="apiMolexParameterModel">api authentication parameter</param>
        /// <returns>it returns dictionary object where Key is Zone Id and Value is ResponseModel.ResponseModel contains success or failure result </returns>
        public static async Task<Dictionary<string, ResultModel<string>>> GetWebAPIAsyncByZoneId(string zoneId, string url, APIMolexParameterModel apiMolexParameterModel)
        {
            Dictionary<string, ResultModel<string>> result = new Dictionary<string, ResultModel<string>>();
            ResultModel<string> resultData = new ResultModel<string>();

            try
            {
                string strJsonString = string.Empty;
                using (var client = SetAuthenticationOnURL(url, apiMolexParameterModel.UserName, apiMolexParameterModel.Password))
                {
                    HttpResponseMessage response = await client.GetAsync(url);
                    GetAPIRespResult(response, url, resultData);
                }

                result.Add(zoneId, resultData);
            }
            catch (Exception ex)
            {
                SetRespOnException(resultData, url, ex);
            }
            
            return result;
        }

        /// <summary>
        /// Sets the authentication on URL.
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>13-07-201703:14 PM</TimeStamp>
        public static HttpClient SetAuthenticationOnURL(string url, string username, string password)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Clear();
            string loginData = string.Format("{0}:{1}", username, password);
            var byteArray = Encoding.ASCII.GetBytes(loginData);
            var header = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
            client.DefaultRequestHeaders.Authorization = header;
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            return client;
        }

        /// <summary>
        /// Subscribes the API.
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <param name="apiMolexParameterModel">The API molex parameter model.</param>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>3/13/20188:01 PM</TimeStamp>
        public static ResultModel<string> SubscribeAPI(string url, APIMolexParameterModel apiMolexParameterModel, SubscriptionModel model)
        {
            ResultModel<string> result = new ResultModel<string>();
            HttpWebRequest httpWebRequest = SetHTTPPutAuthenticationParameters(url, apiMolexParameterModel);
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = Newtonsoft.Json.JsonConvert.SerializeObject(model);
                streamWriter.Write(json);
            }
            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            GetAPIRespResult(httpResponse, url, result);
            Log.Logger.Instance.Log(Log.LogLevel.DEBUG, "URL:" + url + ", StatusCode:" + httpResponse.StatusCode);
            return result;
        }

        /// <summary>
        /// Uns the subscribe API.
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <param name="apiMolexParameterModel">The API molex parameter model.</param>
        /// <param name="unsubscribeId">The unsubscribe identifier.</param>
        /// <returns></returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>3/15/20184:38 PM</TimeStamp>
        public static ResultModel<string> UnSubscribeAPI(string url, APIMolexParameterModel apiMolexParameterModel, string subscribeId)
        {
            ResultModel<string> result = new ResultModel<string>();

            try
            {
                url = url + "?subscriptionid=" + subscribeId;
                object emptyModel = new object();
                HttpWebRequest httpWebRequest = SetHTTPPutAuthenticationParameters(url, apiMolexParameterModel);
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = Newtonsoft.Json.JsonConvert.SerializeObject(emptyModel);
                    streamWriter.Write(json);
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                GetAPIRespResult(httpResponse, url, result);
                Log.Logger.Instance.Log(Log.LogLevel.DEBUG, "URL:" + url + ", StatusCode:" + httpResponse.StatusCode);
                return result;

            }
            catch (Exception ex)
            {
                SetRespOnException(result, url, ex);
            }

            return result;
        }

        /// <summary>
        /// Gets the API live status.
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <param name="apiMolexParameterModel">The API molex parameter model.</param>
        /// <param name="clientID">The client identifier.</param>
        /// <returns></returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/201812:56 PM</TimeStamp>
        public static async Task<ResultModel<string>> GetAPILiveStatus(string url, APIMolexParameterModel apiMolexParameterModel, string clientID)
        {
            ResultModel<string> result = new ResultModel<string>();

            try
            {
                string strJsonString = string.Empty;
                url = url + "?clientid=" + clientID;
                using (var client = SetAuthenticationOnURL(url, apiMolexParameterModel.UserName, apiMolexParameterModel.Password))
                {
                    object emptyModel = new object();
                    HttpResponseMessage response = await client.GetAsync(url);
                    GetAPIRespResult(response, url, result);
                }
            }
            catch (Exception ex)
            {
                SetRespOnException(result, url, ex);
            }

            return result;
        }

        #region private method
        /// <summary>
        /// Common function to set exception message on response on API call
        /// </summary>
        /// <param name="result"></param>
        /// <param name="url"></param>
        /// <param name="ex"></param>
        private static void SetRespOnException(ResultModel<string> result, string url, Exception ex)
        {
            string callingFunName = new StackFrame(1).GetMethod().Name;
            result.IsSuccess = false;
            result.Error = new ErrorModel();
            result.Error.ErrorCategory = AppConstants.ERROR_CATEGORY_SERVER;
            result.Error.ErrorCode = AppConstants.ERROR_CATEGORY_SERVER_UNKNOWN;
            result.Error.OptionalMessage = ex.Message;
            Log.Logger.Instance.Log(ex, Log.LogLevel.ERROR, "HTTPHelper" + callingFunName + "fail for url:" + url);
        }

        /// <summary>
        /// To get api rersponse result
        /// </summary>
        /// <param name="response"></param>
        /// <param name="url"></param>
        /// <param name="result"></param>
        private static async void GetAPIRespResult(HttpResponseMessage response, string url, ResultModel<string> result)
        {
            string strJsonString = string.Empty;

            if (response.StatusCode == HttpStatusCode.OK)
            {
                strJsonString = await response.Content.ReadAsStringAsync();
                result.IsSuccess = true;
                result.Data = strJsonString;
            }
            else
            {
                result.IsSuccess = false;
                result.Error = new ErrorModel();
                result.Error.ErrorCategory = AppConstants.ERROR_CATEGORY_SERVER;
                result.Error.ErrorCode = (int)response.StatusCode;
                result.Error.OptionalMessage = "Failed put API with URL:" + url + ", StatusCode:" + response.StatusCode;
            }

            Log.Logger.Instance.Log(Log.LogLevel.DEBUG, "URL:" + url + ", StatusCode:" + response.StatusCode);
        }
        /// <summary>
        /// To get api rersponse result
        /// </summary>
        /// <param name="response"></param>
        /// <param name="url"></param>
        /// <param name="result"></param>
        private static void GetAPIRespResult(HttpWebResponse response, string url, ResultModel<string> result)
        {
            string strJsonString = string.Empty;

            if (response.StatusCode == HttpStatusCode.OK)
            {
                using (var sr = new StreamReader(response.GetResponseStream()))
                {
                    strJsonString = sr.ReadToEnd();
                }
                result.IsSuccess = true;
                result.Data = strJsonString;
            }
            else
            {
                result.IsSuccess = false;
                result.Error = new ErrorModel();
                result.Error.ErrorCategory = AppConstants.ERROR_CATEGORY_SERVER;
                result.Error.ErrorCode = (int)response.StatusCode;
                result.Error.OptionalMessage = "Failed put API with URL:" + url + ", StatusCode:" + response.StatusCode;
            }

            Log.Logger.Instance.Log(Log.LogLevel.DEBUG, "URL:" + url + ", StatusCode:" + response.StatusCode);
        }
        #endregion
    }
}