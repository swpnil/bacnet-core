﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Helpers
{
    /// <summary>
    /// Helper to set date and time
    /// </summary>
    /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>12/29/20176:26 PM</TimeStamp>
    public static class SysDateTimeHelper
    {
        /// <summary>
        /// Sets the system time.
        /// </summary>
        /// <param name="st">The st.</param>
        /// <returns></returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>12/29/20176:27 PM</TimeStamp>
        [DllImport("kernel32.dll", EntryPoint = "SetSystemTime")]
        public static extern bool SetSystemTime(ref SYSTEMTIME st);

        #region PublicMethod

        /// <summary>
        /// Sets the system date time in UTC.
        /// </summary>
        /// <param name="dateTime">The date time.</param>
        /// <returns></returns>
        /// <CreatedBy>prasad.joshi</CreatedBy><TimeStamp>8/28/20176:28 PM</TimeStamp>
        public static bool SetDateTime(DateTime dateTime)
        {

            bool isSuccess = false;

            //Info: Always in UTC
            SYSTEMTIME systime = new SYSTEMTIME();
            systime.wYear = (ushort)dateTime.Year;
            systime.wDay = (ushort)dateTime.Day;
            systime.wMonth = (ushort)dateTime.Month;
            systime.wDayOfWeek = (ushort)dateTime.DayOfWeek;
            systime.wHour = (ushort)dateTime.Hour;
            systime.wMinute = (ushort)dateTime.Minute;
            systime.wSecond = (ushort)dateTime.Second;
            systime.wMilliseconds = (ushort)dateTime.Millisecond;
            isSuccess = SetSystemTime(ref systime);

            return isSuccess;
        }

        #endregion PublicMethod
    }

    #region PublicClass

    [StructLayout(LayoutKind.Sequential)]
    public struct SYSTEMTIME
    {
        public ushort wYear;
        public ushort wMonth;
        public ushort wDayOfWeek;
        public ushort wDay;
        public ushort wHour;
        public ushort wMinute;
        public ushort wSecond;
        public ushort wMilliseconds;
    }

    #endregion PublicClass
}
