﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              <AppHelper.cs>
///   Description:        <This class provides all helper function>
///   Author:             Prasad Joshi                  
///   Date:               MM/DD/YY
///---------------------------------------------------------------------------------------------
#endregion

using Molex.BACnet.Gateway.Encryption;
using Molex.BACnet.Gateway.Log;
using Molex.BACnet.Gateway.ServiceCore.Constants;
using Molex.BACnet.Gateway.ServiceCore.Controllers;
using Molex.BACnet.Gateway.ServiceCore.Managers;
using Molex.BACnet.Gateway.ServiceCore.Models;
using Molex.BACnet.Gateway.ServiceCore.Repository;
using Molex.BACnet.Gateway.Utility.Helper;
using Molex.BACnet.Gateway.Utility.Log;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Helpers
{
    /// <summary>
    /// This class provides all helper functions like Logger, Application settings 
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:05 PM</TimeStamp>
    public static class AppHelper
    {
        #region PrivateVariables
        private static object _lockObject = new object();

        #endregion PrivateVariables

        /// <summary>
        /// Gets or sets the stop service.
        /// </summary>
        /// <value>
        /// The stop service.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201706:54 PM</TimeStamp>
        public static Action StopService { get; set; }

        /// <summary>
        /// Gets or sets the stop service.
        /// </summary>
        /// <value>
        /// The stop service.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201706:54 PM</TimeStamp>
        public static Action RestartService { get; set; }

        /// <summary>
        /// This Boolean Variable will be Set to 'FALSE' if Gateway is Restarted from Notification Call
        /// & it will be again set to 'TRUE'
        /// </summary>
        public static bool IsGatewayStartCompleted { get; set; }


        #region Public Methods

        public static bool IsGatewayRunning { get; set; }

        public static string GetIPAddress()
        {
            string configIP = ConfigurationManager.AppSettings["WebApiIpAddress"].Trim();
            string jsonfilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["ConfigurationFolderName"], AppConstants.BACNET_CONFIGURATION_FILENAME);
            BACnetConfigurationModel bacnetConfigurationData = JSONReaderWriterHelper.ReadJsonFromFile<BACnetConfigurationModel>(jsonfilePath);
            bacnetConfigurationData.IPAddress = configIP;
            JSONReaderWriterHelper.WriteToJsonFile(jsonfilePath, bacnetConfigurationData);
            return configIP;
        }

        public static void Start()
        {
            string IPaddress = GetIPAddress();
            Console.WriteLine("IP address for BACnet Gateway is " + IPaddress);
            Init(IPaddress);
        }

        public static void StartLogger()
        {
            if (!string.IsNullOrEmpty(AppCoreRepo.Instance.LogFolderPath))
                return;
            ReadLoggerStartUpSettings();
            InitLogger();
        }

        /// <summary>
        /// Reads the settings and initializes various modules
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:06 PM</TimeStamp>
        public static void Init(string IPaddress)
        {
            EventLogger.ELEventSource = "Molex-BACnet-Gateway-Service";
            EventLogger.ELEventLog = "Application";

            AppCoreRepo.Instance.Init();

            ExceptionHelper exceptionHelper = new ExceptionHelper();
            exceptionHelper.RegisterExceptionHandlers();

            bool validSuccess = false;
            bool isInitSuccessful = false;
            try
            {
                ReadLoggerStartUpSettings();
                InitLogger();
                ReadApplicationSetting();
                validSuccess = ValidateApplicationSettings();

                if (!validSuccess)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "AppHelper:configuration validation unsuccesfully");
                    DeInit();
                    return;
                }

                if (!ReadBacnetConfigurationFile(IPaddress))
                {
                    Logger.Instance.Log(LogLevel.ERROR, "AppHelper:BACnet configuration validation unsuccesfully");
                    DeInit();
                    return;
                }

                Logger.Instance.IsDebugEnabled = AppCoreRepo.Instance.BACnetConfigurationData.IsDebugLogEnable;
                validSuccess = ValidateBACnetConfigurationSetting();
                AppCoreRepo.Instance.BACnetConfigurationData.VendorIdentifier = 1001;

                if (!validSuccess)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "AppHelper:BACnet configuration validation unsuccesfully");
                    DeInit();
                    return;
                }

                //APIMonitoringManager.Instance.Init();
                PersistenceStorageManager.Instance.Init();
                StartUpManager.Instance.Init();
                ScheduleRetryManager.Instance.Init();
                isInitSuccessful = StartUpManager.Instance.Execute();
                if (!isInitSuccessful)
                {
                    Logger.Instance.Log(LogLevel.DEBUG, "AppHelper:Startup flow failed");
                    DeInit();
                    return;
                }
                HeartBeatManager.Instance.Init();
                MemoryOptimizeManager.GetInstance().StartMemoryOptimization();
                RestartService = Restart;
                AppHelper.IsGatewayRunning = true;
                Logger.Instance.Log(LogLevel.DEBUG, "AppHelper.Init : Service started");
                AppHelper.IsGatewayStartCompleted = true;//Gateway Started Sucessfuly
            }
            catch (Exception ex)
            {
                //Stop Service
                Logger.Instance.Log(ex, LogLevel.ERROR, "AppHelper.Init : Initialization flow failed");
                DeInit();
            }
        }

        private static void Restart()
        {
            ProcessStartInfo info = new ProcessStartInfo();
            info.FileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppConstants.REINITIALIZE_DEVICE_BATCHFILE_PATH);
            info.WindowStyle = ProcessWindowStyle.Hidden;
            Process process = new Process();
            process.StartInfo = info;
            process.Start();
        }
        /// <summary>
        /// Deinitialize the resoruces used in the process
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201702:36 PM</TimeStamp>
        public static void DeInit()
        {
            if (!AppCoreRepo.Instance.BACnetConfigurationData.IsMQTTPublishSelected)
                GatewayHTTPController.Stop();
            else
                GatewayMQTTController.Stop();
            BBMDFDManager.Instance.DeInit();
            UnSubscribeAPI();
            HeartBeatManager.Instance.DeInit();
            StartUpManager.Instance.Deinit();
            PersistenceStorageManager.Instance.DeInit();
            //APIMonitoringManager.Instance.DeInit();
            ScheduleRetryManager.Instance.DeInit();
            Logger.Instance.CloseLogger();
            MemoryOptimizeManager.GetInstance().StopMemoryOptimization();
            IsGatewayRunning = false;
        }

        /// <summary>
       /// This Method will unsubscribe the HTTP subscription for API
        /// </summary>
        private static void UnSubscribeAPI()
        {
            if (!AppCoreRepo.Instance.BACnetConfigurationData.IsMQTTPublishSelected)
            {
                foreach (string unsubscribeId in AppCoreRepo.Instance.SubScriptionIDs)
                {
                    HTTPAPIManager.Instance.UnSubscribeAPI(unsubscribeId);
                }
                Logger.Instance.Log(LogLevel.DEBUG, "API Unsubscribe success");
            }
        }

        public static string GetHostPublicIp()
        {
            try
            {
                System.Net.Sockets.TcpClient tcpClient = new System.Net.Sockets.TcpClient("www.google.com", 80);
                if (tcpClient != null)
                {
                    if (tcpClient.Connected)
                    {
                        string value = ((System.Net.IPEndPoint)(tcpClient.Client.LocalEndPoint)).Address.ToString();
                        tcpClient.Close();
                        tcpClient = null;
                        return value;
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "GetHostPublicIp : Unable to find public host-ip");
            }

            return string.Empty;
        }

        #endregion Public Methods

        #region Private Methods

        /// <summary>
        /// Initializes the logger.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:07 PM</TimeStamp>
        /// <exception cref="System.NotImplementedException"></exception>
        private static void InitLogger()
        {
            string rootLogPath = AppCoreRepo.Instance.LogFolderPath;

            /*if still it returns empty*/
            if (rootLogPath.Trim() == string.Empty)
            {
                //EventLogger.Log("AppHelper.InitLogger : Logger Path is not configured");
                throw new NotImplementedException();
            }

            string logPath = string.Empty;

            Logger.Instance.LogFolderPath = AppCoreRepo.Instance.LogFolderPath;
            Logger.Instance.IsLogDisabled = AppCoreRepo.Instance.IsLoggerDisabled;
            Logger.Instance.IsSyslogFormatDisabled = AppCoreRepo.Instance.IsSyslogFormatDisabled;

            PreapareLogger();

            //Handler attached for BACnet Integration layer logger
            BACnetStackIntegration.Handler.LogHandler.OnLogDataMessage = LogStackIntegration;

            string logFileName = Logger.LOG_FILE_NAME;
            logPath = Path.Combine(rootLogPath, logFileName);
            //EventLogger.Log("AppHelper.InitLogger : Logging Path : " + logPath);
        }

        /// <summary>
        /// Reads the logger setting from application configuration
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:08 PM</TimeStamp>
        private static void ReadLoggerStartUpSettings()
        {
            //AppCoreRepo.Instance.LogFolderPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), ConfigurationManager.AppSettings["LogFilePath"]);
            AppCoreRepo.Instance.LogFolderPath = Path.Combine(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location)
                                                                                       , ConfigurationManager.AppSettings["LogFilePath"]);
            AppCoreRepo.Instance.IsSyslogFormatDisabled = Convert.ToBoolean(ConfigurationManager.AppSettings["IsSyslogFormatDisabled"]);
            AppCoreRepo.Instance.IsLoggerDisabled = Convert.ToBoolean(ConfigurationManager.AppSettings["IsLoggerDisabled"]);
            AppCoreRepo.Instance.LogDeviceId = Convert.ToInt32(ConfigurationManager.AppSettings["LogDeviceId"]);
        }

        /// <summary>
        /// Preapares the logger.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:08 PM</TimeStamp>
        private static void PreapareLogger()
        {
            try
            {
                Logger.Instance.AppName = Process.GetCurrentProcess().ProcessName;
                Logger.Instance.HostName = GetLocalHostName();
                Logger.Instance.ProcID = Convert.ToString(Process.GetCurrentProcess().Id);
                Logger.Instance.MsgIDText = new Dictionary<int, string>();
                Logger.Instance.MsgIDText.Add(0, AppConstants.SYS_LOG_TYPE_APPLICATION);


                //Info: Default(0:All), need to assign application level log type to be write into file
                Logger.Instance.LogType = 0;

                if (Logger.Instance.IsSyslogFormatDisabled)
                    Logger.Instance.LogFormat = (byte)LoggerConstants.LOG_FORMAT_DEFAULT;
                else
                    Logger.Instance.LogFormat = (byte)LoggerConstants.LOG_FORMAT_SYS_LOG;

                Logger.Instance.SetLogFlag(0, Convert.ToInt32(ConfigurationManager.AppSettings["LogTopicPublishedData"]));
                Logger.Instance.SetLogFlag(1, AppCoreRepo.Instance.LogDeviceId);
            }
            catch (Exception e)
            {

            }
        }

        /// <summary>
        /// Gets the name of the local host.
        /// </summary>
        /// <returns>string LocalHostName</returns>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:09 PM</TimeStamp>
        private static string GetLocalHostName()
        {
            try
            {
                // Get the local computer host name.
                return Dns.GetHostName();
            }
            catch (Exception e)
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// This Method will be helpful for comparing object name at Building & floor level
        /// </summary>
        /// <param name="shortName"></param>
        /// <returns></returns>
        public static string GetFullLocationName(string shortName)
        {
            string result = string.Empty;
            switch (shortName.ToUpperInvariant())
            {
                case "C":
                    result = "Center";
                    break;
                case "E":
                    result = "East";
                    break;
                case "N":
                    result = "North";
                    break;
                case "NE":
                    result = "North-East";
                    break;
                case "NW":
                    result = "North-West";
                    break;
                case "S":
                    result = "South";
                    break;
                case "SE":
                    result = "South-East";
                    break;
                case "SW":
                    result = "South-West";
                    break;
                case "W":
                    result = "West";
                    break;
                case "ALL":
                    result = "All";
                    break;
                default:
                    result = "All";
                    break;
            }
            return result;
        }

        /// <summary>
        /// Reads the application setting.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:09 PM</TimeStamp>
        private static void ReadApplicationSetting()
        {
            AppCoreRepo.Instance.ConfigurationFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["ConfigurationFolderName"]);
            AppCoreRepo.Instance.ProjectApiUrlRetryInMinutes = Convert.ToUInt16(ConfigurationManager.AppSettings["ProjectApiUrlRetryInMinutes"]);
            AppCoreRepo.Instance.APIMonitoringTimerIntervalInSeconds = Convert.ToInt32(ConfigurationManager.AppSettings["APIMonitoringTimerIntervalInSeconds"]);
            AppCoreRepo.Instance.ApplicationSoftwareVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            AppCoreRepo.Instance.AvailableMoods = AppConstants.AVAILABLE_MOODS;
            AppCoreRepo.Instance.AvailableZoneStates = AppConstants.AVAILABLE_ZONE_STATES;
            AppCoreRepo.Instance.BACnetPacketDelayTimeMiliSec = Convert.ToUInt32(ConfigurationManager.AppSettings["BACnetPacketDelayTimeMiliSec"].Trim());
            AppCoreRepo.Instance.RemovedLightScenes = ConfigurationManager.AppSettings["RemovedLightScenes"].Trim().Split(',').ToList();
            AppCoreRepo.Instance.HeartBeatTimerIntervalInSeconds = Convert.ToInt32(ConfigurationManager.AppSettings["HeartBeatTimerIntervalInSeconds"]);

            AppCoreRepo.Instance.ResetAttributes = ConfigurationManager.AppSettings["ResetAttributes"].Trim().Split(',').ToArray();
            AppCoreRepo.Instance.AvailableResetAttributeCombos = ConfigurationManager.AppSettings["ResetAttributeCombos"].Trim().Split(',').ToArray();
            AppCoreRepo.Instance.AvailableResetAttributeComboDescriptions = GetResetAttributeComboDescriptions(AppCoreRepo.Instance.AvailableResetAttributeCombos);
        }

        private static string[] GetResetAttributeComboDescriptions(string[] resetAttributeCombos)
        {
            List<string> resetAttributeComboDescriptions = new List<string>();
            System.Text.StringBuilder builder = new System.Text.StringBuilder();

            for (int ii = 0; ii < resetAttributeCombos.Length; ii++)
            {
                string combo = resetAttributeCombos[ii].Replace("+", "");
                int index = 0;
                builder.Clear();
                for (int jj = 0; jj < combo.Count(); jj++)
                {
                    if (int.TryParse(combo[jj].ToString(), out index))
                    {
                        string attribute = AppCoreRepo.Instance.ResetAttributes[index].Trim();
                        attribute = attribute.Substring(0,1).ToUpper() + attribute.Substring(1, attribute.Length - 1);
                        builder.AppendFormat("{0}+", attribute);
                    }
                }
                resetAttributeComboDescriptions.Add(builder.ToString().Trim('+'));
            }

            return resetAttributeComboDescriptions.ToArray();
        }

        /// <summary>
        /// Validates the application settings.
        /// </summary>
        /// <returns>bool</returns>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:09 PM</TimeStamp>
        private static bool ValidateApplicationSettings()
        {
            try
            {
                if (string.IsNullOrEmpty(AppCoreRepo.Instance.ConfigurationFolder))
                {
                    return false;
                }

                if (AppCoreRepo.Instance.ProjectApiUrlRetryInMinutes <= 0 || AppCoreRepo.Instance.ProjectApiUrlRetryInMinutes > 30)
                {
                    Logger.Instance.Log(LogLevel.DEBUG, "AppHelper.ValidateApplicationSettings : Project API URL Retry value set default to 1 Minute");
                    AppCoreRepo.Instance.ProjectApiUrlRetryInMinutes = 1;
                }

                if (AppCoreRepo.Instance.APIMonitoringTimerIntervalInSeconds <= 0 || AppCoreRepo.Instance.APIMonitoringTimerIntervalInSeconds > 300)
                {
                    Logger.Instance.Log(LogLevel.DEBUG, "AppHelper.ValidateApplicationSettings : Space state API URL Retry value set default to 3 seconds");
                    AppCoreRepo.Instance.APIMonitoringTimerIntervalInSeconds = 3;
                }

                if (AppCoreRepo.Instance.BACnetPacketDelayTimeMiliSec < 0 || AppCoreRepo.Instance.BACnetPacketDelayTimeMiliSec > 100)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "AppHelper.ValidateApplicationSettings : Invalid BACnet Packet Delay Time. : " + AppCoreRepo.Instance.BACnetPacketDelayTimeMiliSec);
                    return false;
                }
            }
            catch (Exception e)
            {
                Logger.Instance.Log(LogLevel.ERROR, "AppHelper.ValidateApplicationSettings : validate application settings failed");
                return false;
            }

            return true;
        }

        private static bool ValidateBACnetConfigurationSetting()
        {

            //This step validate BAcnet Configuration Data using Generated schema 
            if (!ValidateBACnetConfigurationData(Path.Combine(AppCoreRepo.Instance.ConfigurationFolder, AppConstants.BACNET_CONFIGURATION_FILENAME), Path.Combine(AppCoreRepo.Instance.ConfigurationFolder, AppConstants.BACnetConfigurationSchemaFileName)).IsSuccess)
            {
                Logger.Instance.Log(LogLevel.ERROR, "StartUpManager: Execute: failed to Validate BACNet configuration file ");
                return false;
            }

            //This step is used to validate IP address 
            if (!ValidateIPAddress(AppCoreRepo.Instance.BACnetConfigurationData.IPAddress).Data)
            {
                Logger.Instance.Log(LogLevel.ERROR, "StartUpManager: Execute: failed to Validate IP Address ");
                return false;
            }

            if (AppCoreRepo.Instance.BACnetConfigurationData.ControllerId > AppConstants.MAX_DEVICE_ID)
            {
                Logger.Instance.Log(LogLevel.ERROR, "StartUpManager: Execute: Controller id : " + AppCoreRepo.Instance.BACnetConfigurationData.ControllerId + " exceeds max limit :" + AppConstants.MAX_DEVICE_ID);
                return false;
            }
            return true;
        }

        private static bool ReadBacnetConfigurationFile(string ipAddress = "")
        {
            //to read BACnet Configuration data from server File repository
            BACnetConfigurationModel bacnetConfigurationData = JSONReaderWriterHelper.ReadJsonFromFile<BACnetConfigurationModel>(Path.Combine(AppCoreRepo.Instance.ConfigurationFolder, AppConstants.BACNET_CONFIGURATION_FILENAME));
            bacnetConfigurationData.BACnetPassword = CryptoHelper.Decrypt3DES(bacnetConfigurationData.BACnetPassword);
            bacnetConfigurationData.UserName = CryptoHelper.Decrypt3DES(bacnetConfigurationData.UserName);
            bacnetConfigurationData.Password = CryptoHelper.Decrypt3DES(bacnetConfigurationData.Password);
            if (!string.IsNullOrWhiteSpace(ipAddress))
                bacnetConfigurationData.IPAddress = ipAddress; 

            if (bacnetConfigurationData == null)
            {
                Logger.Instance.Log(LogLevel.ERROR, "StartUpManager: Execute: failed to read BACNet configuration file ");
                return false;
            }
            else
            {
                AppCoreRepo.Instance.BACnetConfigurationData = bacnetConfigurationData;
            }
            return true;
        }


        /// <summary>
        /// This method is used to validate BACnet configuration data read by configuration file.
        /// It checks for null or empty, range etc validation for configuration file data.
        /// It read json schame for validation ,compares json file with this schema and response the result.
        /// </summary>
        /// <param name="jsonFileName">Name of the json file.</param>
        /// <param name="jsonSchemaFilePath">The json schema file path.</param>
        /// <returns></returns>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>29-05-201714:49</TimeStamp>"
        private static ResultModel<bool> ValidateBACnetConfigurationData(string jsonFileName, string jsonSchemaFilePath)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            Dictionary<bool, IList<string>> resultData = new Dictionary<bool, IList<string>>();
            IList<string> errormessage = null;
            try
            {
                //var jsonString = File.ReadAllText(jsonFileName);
                //JsonSchema schema = JsonSchema.Parse(File.ReadAllText(jsonSchemaFilePath));
                //JObject jObject = JObject.Parse(jsonString);
                //bool bResult = jObject.IsValid(schema, out errormessage);

                //if (!bResult)
                //{
                //    foreach (var error in errormessage)
                //    {
                //        Log.Logger.Instance.Log(Log.LogLevel.ERROR, "AppHelper.ValidateBACnetConfigurationData : Error Messages: " + error);
                //    }
                //    result.IsSuccess = false;
                //    return result;
                //}
                Log.Logger.Instance.Log(Log.LogLevel.DEBUG, "AppHelper.ValidateBACnetConfigurationData : ValidateBACnetConfigurationData is executed successfully. ");
                result.IsSuccess = true;
            }
            catch (Exception ex)
            {
                Log.Logger.Instance.Log(ex, Log.LogLevel.ERROR, "AppHelper.ValidateBACnetConfigurationData : Error occured" + ex);
                result.IsSuccess = false;
                result.Error = new ErrorModel();
                result.Error.ErrorCategory = AppConstants.ERROR_CATEGORY_MODELDATA;
                result.Error.ErrorCode = AppConstants.ERROR_CATEGORY_MODELDATA_DATA_NOT_FOUND;
            }
            return result;

        }
        /// <summary>
        /// This method is used to validate the ip address provided in BACnetCOnfiguration File.
        /// It checks IP address whether exists in Network interface or not.
        /// </summary>
        /// <param name="iPAddress">The ip address provided in BACnetConfiguration File,pass as an input to verify in network interface.</param>
        /// <returns>it returns the result after verifying IP address in network Interface</returns>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>29-05-201711:47</TimeStamp>"
        private static ResultModel<bool> ValidateIPAddress(string iPAddress)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            try
            {
                ResultModel<List<string>> resultData = NetworkInterfaceHelper.GetAllNetworkInterfaces();
                if (resultData.IsSuccess)
                {
                    if (string.IsNullOrEmpty(resultData.Data.FirstOrDefault(item => item == iPAddress)))
                    {
                        Logger.Instance.Log(LogLevel.ERROR, "AppHelper.ValidateIPAddress : failed to Validate IPAddress ");
                        result.Data = false;
                        return result;
                    }
                    Log.Logger.Instance.Log(Log.LogLevel.DEBUG, "AppHelper.ValidateIPAddress : ValidateIPAddress is completed. ");
                    result.Data = true;
                }
            }
            catch (Exception ex)
            {
                Log.Logger.Instance.Log(ex, Log.LogLevel.ERROR, "AppHelper.ValidateIPAddress : Error occured" + ex);
                result.IsSuccess = false;
                result.Error = new ErrorModel();
                result.Error.ErrorCategory = AppConstants.ERROR_CATEGORY_MODELDATA;
                result.Error.ErrorCode = AppConstants.ERROR_CATEGORY_MODELDATA_DATA_NOT_FOUND;
            }
            return result;

        }

        /// <summary>
        /// Logs the stack integration.
        /// </summary>
        /// <param name="isDebug">if set to <c>true</c> [is debug].</param>
        /// <param name="ex">The ex.</param>
        /// <param name="moreInfo">The more information.</param>
        /// <param name="loggenerationTime">The loggeneration time.</param>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/31/20171:35 PM</TimeStamp>
        private static void LogStackIntegration(bool isDebug, Exception ex, string moreInfo, DateTime loggenerationTime)
        {
            LogData data = new LogData();

            if (isDebug)
                data.LogLevel = LogLevel.DEBUG;
            else
                data.LogLevel = LogLevel.ERROR;

            data.MoreInfo = moreInfo;
            //data.LogGenerationTime = loggenerationTime;
            data.LogException = ex;
            Logger.Instance.Log(data);

        }
        #endregion Private Methods
    }
}
