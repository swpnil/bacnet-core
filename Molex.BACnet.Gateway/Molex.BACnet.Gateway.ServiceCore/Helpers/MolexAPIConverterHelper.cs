﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              <MolexAPIConverterHelper.cs>
///   Description:        <This class is used to store all API helper function used for retriving and storing data from project file>
///   Author:             <Rupesh Saw>                  
///   Date:               07/10/17
#endregion


using Molex.BACnet.Gateway.Log;
using Molex.BACnet.Gateway.ServiceCore.AppResources;
using Molex.BACnet.Gateway.ServiceCore.Constants;
using Molex.BACnet.Gateway.ServiceCore.Managers;
using Molex.BACnet.Gateway.ServiceCore.Models;
using Molex.BACnet.Gateway.ServiceCore.Models.Entity;
using Molex.BACnet.Gateway.ServiceCore.Models.MolexAPIModels;
using Molex.BACnet.Gateway.ServiceCore.Models.SubsriberModels;
using Molex.BACnet.Gateway.ServiceCore.Repository;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Molex.BACnet.Gateway.ServiceCore.Helpers
{
    /// <summary>
    /// This class is used to store all API helper function used for retriving and storing data from project file
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>10-07-201710:17 AM</TimeStamp>
    class MolexAPIConverterHelper
    {
        /// <summary>
        /// This function parsing JSON Data into API Model 
        /// </summary>
        /// <param name="jsonData">jsonData</param>
        /// <returns>JSON Data String format</returns>
        public static ResultModel<MolexProject> ParseMolexJSONData(string jsonData)
        {
            Logger.Instance.Log(LogLevel.DEBUG, "MolexAPIConverterHelper.ParseJSONData : Reading JSON Data : Started ");

            ResultModel<MolexProject> result = new ResultModel<MolexProject>();

            if (jsonData != null && jsonData.Length == 0)
            {
                Log.Logger.Instance.Log(Log.LogLevel.DEBUG, "MolexAPIConverterHelper.ParseJSONData : Empty JSON File");

                result.IsSuccess = false;
                result.Error = new ErrorModel();
                result.Error.ErrorCategory = AppConstants.ERROR_CATEGORY_DATAPARSING;
                result.Error.ErrorCode = AppConstants.ERROR_CATEGORY_DATAPARSING_EMPTY_JSON;
                return result;
            }

            try
            {
                MolexProject molexResult = Utility.Helper.JSONReaderWriterHelper.ReadJsonFromString<MolexProject>(jsonData);
                result.IsSuccess = true;
                result.Data = molexResult;

                Logger.Instance.Log(LogLevel.DEBUG, "MolexAPIConverterHelper.ParseJSONData : Reading JSON Data : Completed ");
                return result;
            }
            catch (Exception ex)
            {
                Log.Logger.Instance.Log(ex, Log.LogLevel.ERROR, "MolexAPIConverterHelper.ParseJSONData : JSON Format Error");
                result.IsSuccess = false;
                result.Error = new ErrorModel();
                result.Error.ErrorCategory = AppConstants.ERROR_CATEGORY_DATAPARSING_JSON_FORMAT_ERROR;
                result.Error.ErrorCode = AppConstants.ERROR_CATEGORY_DATAPARSING;
                result.Error.OptionalMessage = ex.Message;
                return result;
            }

        }

        /// <summary>
        ///  Parsing Data from API Model to EnitityModel
        /// </summary>
        /// <param name="molex">APIMolexModel</param>
        /// <returns>Data in APIMolex Model</returns>
        public static ResultModel<EntityBaseModel> ParseEntityModel(MolexProject molex)
        {
            Logger.Instance.Log(LogLevel.DEBUG, "MolexAPIConverterHelper.ParseEntityModel : Parsing APIMolexModel to Entity Model : Started ");

            ResultModel<EntityBaseModel> result = new ResultModel<EntityBaseModel>();
            try
            {
                if (molex != null)
                {
                    if (molex.result == null || molex.result.data == null || molex.result.data.Count == 0)
                    {
                        Logger.Instance.Log(Log.LogLevel.ERROR, "MolexAPIConverterHelper.ParseEntityModel : Result Empty Controller Model.. No Data available");
                        result.IsSuccess = false;
                        result.Error = new ErrorModel();
                        result.Error.ErrorCode = AppConstants.ERROR_CATEGORY_MODELDATA;
                        result.Error.ErrorCategory = AppConstants.ERROR_CATEGORY_MODELDATA_DATA_NOT_FOUND;
                        return result;
                    }
                    Dictionary<string, MolexAPIBuildingModel> buildingMappingDic = new Dictionary<string, MolexAPIBuildingModel>();
                    Dictionary<string, MolexAPIZoneModel> zoneMappingDic = new Dictionary<string, MolexAPIZoneModel>();

                    // Storing building details from controller into buildingMappingDic dictonary
                    MolexAPIBuildingModel _MolexAPIBuildingModel = new MolexAPIBuildingModel();
                    _MolexAPIBuildingModel.id = molex.result.data[0].buildingID;
                    _MolexAPIBuildingModel.key = molex.result.data[0].buildingID;
                    _MolexAPIBuildingModel.location = molex.result.data[0].location;
                    _MolexAPIBuildingModel.name = molex.result.data[0].buildingName;
                    _MolexAPIBuildingModel.utilisation = 0;
                    _MolexAPIBuildingModel.floors = molex.result.data[0].floors;
                    buildingMappingDic.Add(molex.result.data[0].id, _MolexAPIBuildingModel);

                    ControllerEntityModel controller = new ControllerEntityModel();
                    controller.EntityKey = molex.result.data[0].id;
                    controller.EntityName = molex.result.data[0].buildingName;
                    controller.Location = molex.result.data[0].location;
                    controller.MappingKey = molex.result.data[0].id;

                    foreach (MolexAPIBuildingModel currentBuilding in buildingMappingDic.Values)
                    {
                        BuildingEntityModel building = (BuildingEntityModel)GetBuildingModelData(currentBuilding);
                        building.Parent = controller;
                        controller.Childs.Add(building);
                        ResultModel<string> resultJsonData = HTTPAPIManager.Instance.GetProjectDevices(molex.result.data[0].id, building.EntityKey);
                        ProjectDevicesRootObject buildingDevices = Utility.Helper.JSONReaderWriterHelper.ReadJsonFromString<ProjectDevicesRootObject>(resultJsonData.Data);
                        if (IsBuildingDevicesFetched(buildingDevices))
                        {
                            AppCoreRepo.Instance.TotalDevicesCount = buildingDevices.result.data[0].fixtures != null ? buildingDevices.result.data[0].fixtures.Count : 0;
                            AppCoreRepo.Instance.TotalDevicesCount = AppCoreRepo.Instance.TotalDevicesCount + (buildingDevices.result.data[0].sensors != null ? buildingDevices.result.data[0].sensors.Count : 0);
                            AppCoreRepo.Instance.TotalDevicesCount = AppCoreRepo.Instance.TotalDevicesCount + (buildingDevices.result.data[0].blinds != null ? buildingDevices.result.data[0].blinds.Count : 0);
                            AppCoreRepo.Instance.TotalDevicesCount = AppCoreRepo.Instance.TotalDevicesCount + (buildingDevices.result.data[0].beacons != null ? buildingDevices.result.data[0].beacons.Count : 0);
                        }
                        foreach (var currentFloor in currentBuilding.floors)
                        {
                            FloorEntityModel floor = (FloorEntityModel)GetFloorModelData(currentFloor);
                            floor.Parent = building;
                            building.Childs.Add(floor);

                            foreach (Zone molexAPIZoneModel in currentFloor.zones)
                            {
                                if (molexAPIZoneModel.name.ToUpperInvariant() == AppResources.AppResource.DefaultStringResource.ToUpperInvariant() && molexAPIZoneModel.type.ToUpperInvariant() == AppResources.AppResource.DefaultStringResource.ToUpperInvariant())
                                    continue;                             
                                ZoneEntityModel zone = (ZoneEntityModel)GetZoneModelData(molexAPIZoneModel);
                                zone.Parent = floor;
                                floor.Childs.Add(zone);
                                List<Fixture> fixtures = new List<Fixture>();
                                List<Sensor> sensors = new List<Sensor>();
                                List<Blind> blinds = new List<Blind>();
                                List<Beacon> beacons = new List<Beacon>();

                                if (IsBuildingDevicesFetched(buildingDevices))
                                {
                                    if (buildingDevices.result.data[0].fixtures.Any(s => s.zoneID == molexAPIZoneModel.id))
                                        fixtures = buildingDevices.result.data[0].fixtures.Where(s => s.zoneID == molexAPIZoneModel.id).ToList();
                                    if (buildingDevices.result.data[0].sensors.Any(s => s.zoneID == molexAPIZoneModel.id))
                                        sensors = buildingDevices.result.data[0].sensors.Where(s => s.zoneID == molexAPIZoneModel.id).ToList();
                                    if (buildingDevices.result.data[0].blinds.Any(s => s.zoneID == molexAPIZoneModel.id))
                                        blinds = buildingDevices.result.data[0].blinds.Where(s => s.zoneID == molexAPIZoneModel.id).ToList();
                                    if (buildingDevices.result.data[0].beacons.Any(s => s.zoneID == molexAPIZoneModel.id))
                                        beacons = buildingDevices.result.data[0].beacons.Where(s => s.zoneID == molexAPIZoneModel.id).ToList();
                                }
                                foreach (Fixture fixtureID in fixtures)
                                {
                                    FixtureEntityModel fixtureEntityModel = (FixtureEntityModel)GetFixtureModelData(fixtureID);
                                    fixtureEntityModel.Parent = zone;
                                    zone.Fixtures.Add(fixtureEntityModel);
                                }

                                foreach (Sensor sensorID in sensors)
                                {
                                    SensorEntityModel sensorEntityModel = (SensorEntityModel)GetSensorModelData(sensorID);
                                    //To skip unsupported sensor adding in zone ex.switch
                                    if (sensorEntityModel.SensorType == AppConstants.SensorType.NotSupported)
                                    {
                                        continue;
                                    }
                                    sensorEntityModel.Parent = zone;
                                    zone.Sensors.Add(sensorEntityModel);
                                }

                                foreach (Blind item in blinds)
                                {
                                    zone.Blinds.Add(item);
                                }

                                foreach (Beacon item in beacons)
                                {
                                    zone.Beacons.Add(item);
                                }

                                #region Commented Fetch Zone Specific Light Scene
                                //ResultModel<string> zoneResultLightScenes = HTTPAPIManager.Instance.GetZoneLightScenes(molex.result.data[0].id, molexAPIZoneModel.id);
                                //ZoneLightSceneRootObject lightScenes = Utility.Helper.JSONReaderWriterHelper.ReadJsonFromString<ZoneLightSceneRootObject>(zoneResultLightScenes.Data);
                                //foreach (var lightScence in lightScenes.result.data)
                                //{
                                //    string currentLightScene = GetLightSceneModelData(lightScence);
                                //    zone.LightScene.Add(currentLightScene);
                                //} 
                                #endregion
                            }
                        }
                    }

                    Logger.Instance.Log(LogLevel.DEBUG, "MolexAPIConverterHelper.ParseEntityModel : Parsing APIMolexModel to Entity Model : Completed");
                    result.IsSuccess = true;
                    result.Data = controller;
                    return result;
                }
                else
                {
                    Logger.Instance.Log(Log.LogLevel.ERROR, "MolexAPIConverterHelper.ParseEntityModel : Empty Controller Model.. No Data available");
                    result.IsSuccess = false;
                    result.Error = new ErrorModel();
                    result.Error.ErrorCode = AppConstants.ERROR_CATEGORY_MODELDATA;
                    result.Error.ErrorCategory = AppConstants.ERROR_CATEGORY_MODELDATA_DATA_NOT_FOUND;
                    return result;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, Log.LogLevel.ERROR, "MolexAPIConverterHelper.ParseEntityModel : Model Parsing Error ... Mismatch objects ");
                result.IsSuccess = false;
                result.Error = new ErrorModel();
                result.Error.ErrorCode = AppConstants.ERROR_CATEGORY_MODELPARSINGERROR;
                result.Error.ErrorCategory = AppConstants.ERROR_CATEGORY_MODELPARSINGERROR_MODEL_DATA_PARSING_ERROR;
                result.Error.OptionalMessage = ex.Message;
                return result;
            }
        }

        /// <summary>
        /// This Method will verify the Building Devices Existance before adding it to Zones
        /// </summary>
        /// <param name="buildingDevices"></param>
        /// <returns></returns>
        private static bool IsBuildingDevicesFetched(ProjectDevicesRootObject buildingDevices)
        {
            try
            {
                if (buildingDevices == null)
                    return false;
                if (buildingDevices.result == null)
                    return false;
                if (buildingDevices.result.data == null)
                    return false;
                if (buildingDevices.result.data.Count == 0)
                    return false;
                return true;
            }
            catch (Exception ex)
            {
                Log.Logger.Instance.Log(ex, Log.LogLevel.ERROR, "MolexAPIConverterHelper.IsBuildingDevicesFetched : Failed to Validate Building Devices");
                return false;
            }
        }

        /// <summary>
        /// Gets the light scene model data.
        /// </summary>
        /// <param name="lightScence">The light scence.</param>
        /// <param name="controller">The controller.</param>
        /// <returns></returns>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>18-07-201706:37 PM</TimeStamp>
        private static string GetLightSceneModelData(ZoneLightSceneDatum objZoneLight)
        {
            MolexAPILightsceneModel molexAPILightsceneModel = new MolexAPILightsceneModel();
            molexAPILightsceneModel.id = objZoneLight.id;

            string lightSceneEntityModel = objZoneLight.name;
            return lightSceneEntityModel;
        }

        /// <summary>
        /// Parsing Data from APIControllerModel to ControllerEnitityModel
        /// </summary>
        /// <param name="controller">APIControllerModel</param>
        /// <returns>Controller information in Entity Model</returns>
        internal static ControllerEntityModel GetControllerModelData(MolexAPIControllerModel controller)
        {
            ControllerEntityModel controllerModel = new ControllerEntityModel();
            controllerModel.EntityKey = controller.id;
            controllerModel.EntityName = controller.buildings.name;
            controllerModel.Location = controller.location;
            controllerModel.MappingKey = controller.id;
            controllerModel.MolexAPILightsceneModel = controller.lightScenes;
            return controllerModel;
        }

        /// <summary>
        /// Parsing Data from APIBuildingModel to BuildingEnitityModel
        /// </summary>
        /// <param name="building">APIBuildingModel</param>
        /// <returns>Building information in Entity Model</returns>
        internal static EntityBaseModel GetBuildingModelData(MolexAPIBuildingModel building)
        {
            BuildingEntityModel buildingModel = new BuildingEntityModel();
            buildingModel.EntityKey = building.id;
            buildingModel.EntityName = building.name;
            buildingModel.MappingKey = building.id;
            buildingModel.Location = building.location;
            buildingModel.Description = AppResources.AppResource.BuildingDescription;
            return buildingModel;
        }

        /// <summary>
        /// Parsing Data from APIFloorModel to FloorEnitityModel
        /// </summary>
        /// <param name="floor">APIFloorModel</param>
        /// <returns>Floor information in Entity Model</returns>
        internal static EntityBaseModel GetFloorModelData(Floor floor)
        {
            FloorEntityModel floorModel = new FloorEntityModel();
            floorModel.EntityKey = floor.id;
            floorModel.MappingKey = floor.id;
            floorModel.EntityName = floor.name;
            floorModel.Description = AppResources.AppResource.FloorDescription;
            return floorModel;
        }

        /// <summary>
        /// Parsing Data from APIZoneModel to ZoneEnitityModel
        /// </summary>
        /// <param name="zone">APIZoneModel</param>
        /// <returns>Zone information in Entity Model</returns>
        internal static EntityBaseModel GetZoneModelData(Zone zone)
        {
            ZoneEntityModel zoneModel = new ZoneEntityModel();
            zoneModel.EntityKey = zone.id;
            zoneModel.MappingKey = zone.id;
            zoneModel.ZoneId = zone.id;
            zoneModel.EntityName = zone.name;
            zoneModel.ZoneType = GetZoneType(zone.layer, zone.type); //Mapping layer field to Zone type since layer differentiates properly than type in MolexAPIZoneModel
            zoneModel.ProprietaryGridX = (uint)zone.locX;
            zoneModel.ProprietaryGridY = (uint)zone.locY;
            return zoneModel;
        }

        private static AppConstants.ZoneTypes GetZoneType(string zoneLayer, string zoneType)
        {
            switch (zoneLayer.ToUpperInvariant())
            {
                case "SHADING":
                    if (zoneType.ToUpperInvariant() == "SHADES")
                        return AppConstants.ZoneTypes.BlindSpace;
                    else
                        return AppConstants.ZoneTypes.UserSpace;
                case "INFORMATION":
                    return AppConstants.ZoneTypes.BeaconSpace;
                case "USER":
                    return AppConstants.ZoneTypes.UserSpace;
                case "GENERAL":
                    return AppConstants.ZoneTypes.GeneralSpace;
                default:
                    return AppConstants.ZoneTypes.UserSpace;
            }
        }

        /// <summary>
        /// Parsing Data from APIFixtureModel to FixtureEnitityModel
        /// </summary>
        /// <param name="fixture">APIFixtureModel</param>
        /// <returns>Fixture information in Entity Model</returns>
        internal static EntityBaseModel GetFixtureModelData(Fixture fixture)
        {
            FixtureEntityModel fixtureModel = new FixtureEntityModel();
            fixtureModel.EntityKey = fixture.id;
            fixtureModel.EntityName = fixture.name;
            fixtureModel.MappingKey = fixture.id;
            fixtureModel.Description = String.Concat(fixture.role, " ", fixture.name);
            fixtureModel.ProprietaryGridX = Convert.ToUInt32(fixture.gridX);
            fixtureModel.ProprietaryGridY = Convert.ToUInt32(fixture.gridY);
            return fixtureModel;
        }

        /// <summary>
        /// Parsing Data from APISensorModel to SensorEnitityModel
        /// </summary>
        /// <param name="sensor">APISensorModel</param>
        /// <returns>Sensor information in Entity Model</returns>
        internal static EntityBaseModel GetSensorModelData(Sensor sensor)
        {
            SensorEntityModel sensorModel = new SensorEntityModel();
            sensorModel.EntityKey = sensor.id;
            sensorModel.MappingKey = sensor.id;
            sensorModel.EntityName = sensor.name;
            sensorModel.Description = String.Concat(sensor.role, " ", sensor.name);
            sensorModel.SensorType = GetSensorType(sensor.role);
            sensorModel.ProprietaryGridX = Convert.ToUInt32(sensor.gridX);
            sensorModel.ProprietaryGridY = Convert.ToUInt32(sensor.gridY);
            sensorModel.Role = sensor.role;
            sensorModel.DeviceType = sensor.type;
            return sensorModel;
        }
        /// <summary>
        /// Get sensor type by role
        /// </summary>
        /// <param name="role">The role.</param>
        /// <returns>SensorType</returns>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>09-06-201705:10 PM</TimeStamp>
        internal static AppConstants.SensorType GetSensorType(string role)
        {
            AppConstants.SensorType sensorType = AppConstants.SensorType.NotSupported;
            switch (role)
            {
                case "CT":
                    sensorType = AppConstants.SensorType.ColorTemperature;
                    break;
                case "AQ":
                    sensorType = AppConstants.SensorType.AirQuality;
                    break;
                case "PIR":
                    sensorType = AppConstants.SensorType.Presence;
                    break;
                case "PW":
                    sensorType = AppConstants.SensorType.Power;
                    break;
                case "AL":
                    sensorType = AppConstants.SensorType.LuxLevel;
                    break;
                case "HUM":
                    sensorType = AppConstants.SensorType.Humidity;
                    break;
                case "TEMP":
                    sensorType = AppConstants.SensorType.Temperature;
                    break;
                default:
                    Logger.Instance.Log(LogLevel.ERROR, "MolexAPIConverterHelper.GetSensorType: Role Not avilable " + role);
                    break;
            }
            return sensorType;
        }

        /// <summary>
        /// This function is used for fetching APIZoneModel.
        /// </summary>
        /// <param name="floor">The floor.</param>
        /// <param name="controller">The controller.</param>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>13-07-201704:37 PM</TimeStamp>
        internal static ResultModel<List<MolexAPIZoneModel>> GetAPIZoneModel(FloorEntityModel floor, Dictionary<string, MolexAPIZoneModel> zoneMappingDic)
        {
            ResultModel<List<MolexAPIZoneModel>> result = new ResultModel<List<MolexAPIZoneModel>>();
            List<MolexAPIZoneModel> zoneModel = new List<MolexAPIZoneModel>();
            try
            {
                foreach (MolexAPIZoneModel molexAPIZoneModel in zoneMappingDic.Values)
                {
                    if (molexAPIZoneModel.key == "BuildingDefault")
                        continue;
                    if (molexAPIZoneModel.availableFloors.Length != 0 && molexAPIZoneModel.availableFloors.Contains(floor.EntityKey.ToString()))
                    {
                        zoneModel.Add(molexAPIZoneModel);
                    }
                }
                result.Data = zoneModel;
                result.IsSuccess = true;
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, Log.LogLevel.ERROR, "MolexAPIConverterHelper.GetZoneModel : Zone Model Parsing Error ... Mismatch objects ");
                result.IsSuccess = false;
                result.Error = new ErrorModel();
                result.Error.ErrorCode = AppConstants.ERROR_CATEGORY_MODELPARSINGERROR;
                result.Error.ErrorCategory = AppConstants.ERROR_CATEGORY_MODELPARSINGERROR_MODEL_DATA_PARSING_ERROR;
                result.Error.OptionalMessage = ex.Message;
            }

            return result;
        }

        /// <summary>
        /// Gets all light scence.
        /// </summary>
        /// <param name="molex">The molex project model.</param>
        /// <returns></returns>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>18-07-201706:35 PM</TimeStamp>
        internal static ResultModel<List<string>> GetAllLightScence(LightScenesRootObject lightdata)
        {
            ResultModel<List<string>> result = new ResultModel<List<string>>();
            try
            {

                if (lightdata != null)
                {
                    List<string> AllLightScence = new List<string>();
                    foreach (var item in lightdata.result.data)
                    {
                        AllLightScence.Add(item.key);
                    }
                    result.Data = AllLightScence;
                    result.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "MolexAPIConverterHelper.GetAllLightScence: Get Lightscene Failed : ");
                result.IsSuccess = false;
            }
            return result;
        }

        internal static ResultModel<List<string>> GetUserAndGeneralLightScene(LightScenesRootObject lightsceneObject, IEnumerable<string> removedLightScenes)
        {
            ResultModel<List<string>> resultModel = new ResultModel<List<string>>();

            List<string> userAndGeneralLightScene = new List<string>();
            try
            {

                if (lightsceneObject != null)
                {
                    userAndGeneralLightScene = lightsceneObject.result.data.Where(x => x.type.Contains(AppResource.GeneralZoneTypeName) || x.type.Contains(AppResource.UserZoneTypeName) || x.type.Contains("building")).Select(x => x.key).ToList();
                }

                foreach (string removedLightScene in removedLightScenes)
                {
                    userAndGeneralLightScene = userAndGeneralLightScene.Where(x => x != removedLightScene).ToList();
                }

                resultModel.Data = userAndGeneralLightScene;
                resultModel.IsSuccess = true;
            }
            catch (Exception e)
            {
                Logger.Instance.Log(e, LogLevel.ERROR, "MolexAPIConverterHelper.GetAllLightScence: Get Lightscene Failed : ");
                resultModel.IsSuccess = false;
            }
            return resultModel;
        }

        internal static ResultModel<List<string>> GetBeaconLightScene(LightScenesRootObject lightsceneObject, IEnumerable<string> removedLightScenes)
        {
            ResultModel<List<string>> resultModel = new ResultModel<List<string>>();
            List<string> beaconLightScenes = new List<string>();
            try
            {

                if (lightsceneObject != null)
                {
                    beaconLightScenes = lightsceneObject.result.data.Where(x => x.type.Contains("information")).Select(x => x.key).ToList();
                }

                foreach (string removedLightScene in removedLightScenes)
                {
                    beaconLightScenes = beaconLightScenes.Where(x => x != removedLightScene).ToList();
                }

                resultModel.Data = beaconLightScenes;
                resultModel.IsSuccess = true;
            }
            catch (Exception e)
            {
                Logger.Instance.Log(e, LogLevel.ERROR, "MolexAPIConverterHelper.GetAllLightScence: Get Lightscene Failed : ");
                resultModel.IsSuccess = false;
            }
            return resultModel;
        }

        /// <summary>
        /// Parses the reliability data.
        /// </summary>
        /// <param name="jsonData">The json data.</param>
        /// <returns></returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>1/4/20183:45 PM</TimeStamp>
        internal static ResultModel<MolexAPIReliabilitySubRespModel> ParseReliabilityData(string jsonData)
        {
            ResultModel<MolexAPIReliabilitySubRespModel> result = new ResultModel<MolexAPIReliabilitySubRespModel>();
            MolexAPIReliabilitySubRespModel reliabilityData = new MolexAPIReliabilitySubRespModel();
            try
            {
                Logger.Instance.Log(LogLevel.DEBUG, "APIDataConverterHelper.ParseReliabilityData : Reading JSON Data : Started ");

                if (jsonData != null && jsonData.Length == 0)
                {
                    Log.Logger.Instance.Log(Log.LogLevel.ERROR, "MolexAPIConverterHelper.ParseReliabilityData : Empty JSON File");
                    result.IsSuccess = false;
                    result.Error = new ErrorModel();
                    result.Error.ErrorCategory = AppConstants.ERROR_CATEGORY_DATAPARSING;
                    result.Error.ErrorCode = AppConstants.ERROR_CATEGORY_DATAPARSING_EMPTY_JSON;
                    return result;
                }

                reliabilityData = JsonConvert.DeserializeObject<MolexAPIReliabilitySubRespModel>(jsonData);
                result.IsSuccess = true;
                result.Data = reliabilityData;
                return result;
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, Log.LogLevel.ERROR, "MolexAPIConverterHelper.ParseReliabilityData : Model Parsing Error ... Mismatch objects ");
                result.IsSuccess = false;
                result.Error = new ErrorModel();
                result.Error.ErrorCode = AppConstants.ERROR_CATEGORY_MODELPARSINGERROR;
                result.Error.ErrorCategory = AppConstants.ERROR_CATEGORY_MODELPARSINGERROR_MODEL_DATA_PARSING_ERROR;
                result.Error.OptionalMessage = ex.Message;
                return result;
            }
        }

        /// <summary>
        /// Parses the notification json data.
        /// </summary>
        /// <param name="jsonData">The json data.</param>
        /// <returns></returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/201812:48 PM</TimeStamp>
        internal static ResultModel<MolexAPINotificationSubRespModel> ParseNotificationJSONData(string jsonData)
        {
            ResultModel<MolexAPINotificationSubRespModel> result = new ResultModel<MolexAPINotificationSubRespModel>();
            MolexAPINotificationSubRespModel notification = new MolexAPINotificationSubRespModel();

            try
            {
                Logger.Instance.Log(LogLevel.DEBUG, "APIDataConverterHelper.ParseNotificationJSONData : Reading JSON Data : Started ");

                if (jsonData != null && jsonData.Length == 0)
                {
                    Log.Logger.Instance.Log(Log.LogLevel.ERROR, "MolexAPIConverterHelper.ParseNotificationJSONData : Empty JSON File");
                    result.IsSuccess = false;
                    result.Error = new ErrorModel();
                    result.Error.ErrorCategory = AppConstants.ERROR_CATEGORY_DATAPARSING;
                    result.Error.ErrorCode = AppConstants.ERROR_CATEGORY_DATAPARSING_EMPTY_JSON;
                    return result;
                }

                notification = JsonConvert.DeserializeObject<MolexAPINotificationSubRespModel>(jsonData);
                result.IsSuccess = true;
                result.Data = notification;
                return result;

            }
            catch (Exception ex)
            {
                Log.Logger.Instance.Log(Log.LogLevel.ERROR, "MolexAPIDataConverterHelper.ParseNotificationJSONData : JSON Format Error");
                result.IsSuccess = false;
                result.Error = new ErrorModel();
                result.Error.ErrorCategory = AppConstants.ERROR_CATEGORY_DATAPARSING_JSON_FORMAT_ERROR;
                result.Error.ErrorCode = AppConstants.ERROR_CATEGORY_DATAPARSING;
                result.Error.OptionalMessage = ex.Message;
                return result;
            }
        }

        /// <summary>
        /// Parses the molex API notification model to notification model.
        /// </summary>
        /// <param name="apiNotificationRespModel">The molex API notification response model.</param>
        /// <returns></returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/201812:49 PM</TimeStamp>
        internal static NotificationDataModel ParseNotificationRespModel(MolexAPINotificationSubRespModel apiNotificationRespModel)
        {
            NotificationDataModel notificationModel = new NotificationDataModel();

            try
            {
                notificationModel.ResourceType = apiNotificationRespModel.resourceType;
                notificationModel.AdditionalInfo = apiNotificationRespModel.additionalInfo;
                notificationModel.Type = apiNotificationRespModel.type;
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "MolexAPIDataConverterHelper:ParseNotificationRespModel:Json data parsing failed");
            }

            return notificationModel;
        }

        /// <summary>
        /// Parses the molex API reliability data to reliability data model.
        /// </summary>
        /// <param name="apiReliabilityRespModel">The molex API reliability response model.</param>
        /// <returns></returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/201812:49 PM</TimeStamp>
        internal static ReliabilityDataModel ParseReliabilitySubRespModel(MolexAPIReliabilitySubRespModel apiReliabilityRespModel)
        {
            ReliabilityDataModel reliabilityModel = new ReliabilityDataModel();

            try
            {
                reliabilityModel.ID = apiReliabilityRespModel.id;
                reliabilityModel.Value = apiReliabilityRespModel.value;
                reliabilityModel.Resources.FixtureModel.FixtureType = apiReliabilityRespModel.resources.fixture.fixtureType;
                reliabilityModel.Resources.FixtureModel.Value = apiReliabilityRespModel.resources.fixture.value;
                reliabilityModel.Resources.SensorModel.SensorType = apiReliabilityRespModel.resources.sensor.sensorType;
                reliabilityModel.Resources.SensorModel.Value = apiReliabilityRespModel.resources.sensor.value;

                reliabilityModel.Resources.BeaconModel.BeaconType = apiReliabilityRespModel.resources.beacon.beaconType;
                reliabilityModel.Resources.BeaconModel.Value = apiReliabilityRespModel.resources.beacon.value;

                reliabilityModel.Resources.BlindModel.BlindType = apiReliabilityRespModel.resources.blind.blindType;
                reliabilityModel.Resources.BlindModel.Value = apiReliabilityRespModel.resources.blind.value;
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "MolexAPIDataConverterHelper:ParseMolexAPIReliabilityDataToReliabilityDataModel:Json data parsing failed");
            }

            return reliabilityModel;
        }

        /// <summary>
        /// parse api subscription response
        /// </summary>
        /// <param name="jsonData"></param>
        /// <returns></returns>
        internal static SubscriptionResponseModel ParseMolexAPISubRespData(string jsonData)
        {
            SubscriptionResponseModel subscriptionRespModel = new SubscriptionResponseModel();

            try
            {
                MolexAPISubRespModel apiSubscriptionRespModel = JsonConvert.DeserializeObject<MolexAPISubRespModel>(jsonData);
                subscriptionRespModel.ClientID = apiSubscriptionRespModel.clientid;
                subscriptionRespModel.SubscriptionID = apiSubscriptionRespModel.id;
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "MolexAPIDataConverterHelper:ParseMolexAPISubscriptionResponse:Json data parsing failed");
            }

            return subscriptionRespModel;
        }

        /// <summary>
        /// To parse api published sensor data
        /// </summary>
        /// <param name="jsonData"></param>
        /// <returns></returns>
        internal static ResultModel<MolexAPISensorSubRespModel> ParseSensorSubRespData(string jsonData)
        {
            ResultModel<MolexAPISensorSubRespModel> result = new ResultModel<MolexAPISensorSubRespModel>();
            MolexAPISensorSubRespModel sensorResp = new MolexAPISensorSubRespModel();

            try
            {
                if (jsonData != null && jsonData.Length == 0)
                {
                    Log.Logger.Instance.Log(Log.LogLevel.ERROR, "MolexAPIConverterHelper.ParseSensorSubRespData : Empty JSON Data");
                    result.IsSuccess = false;
                    result.Error = new ErrorModel();
                    result.Error.ErrorCategory = AppConstants.ERROR_CATEGORY_DATAPARSING;
                    result.Error.ErrorCode = AppConstants.ERROR_CATEGORY_DATAPARSING_EMPTY_JSON;
                    return result;
                }
                sensorResp = JsonConvert.DeserializeObject<MolexAPISensorSubRespModel>(jsonData);
                result.IsSuccess = true;
                result.Data = sensorResp;

                Logger.Instance.Log(LogLevel.DEBUG, "APIDataConverterHelper.ParseSensorSubRespData : Read JSON Data : " + jsonData);

                return result;

            }
            catch (Exception ex)
            {
                Log.Logger.Instance.Log(Log.LogLevel.ERROR, "MolexAPIDataConverterHelper.ParseSensorSubRespData : JSON Format Error => " + jsonData);
                result.IsSuccess = false;
                result.Error = new ErrorModel();
                result.Error.ErrorCategory = AppConstants.ERROR_CATEGORY_DATAPARSING_JSON_FORMAT_ERROR;
                result.Error.ErrorCode = AppConstants.ERROR_CATEGORY_DATAPARSING;
                result.Error.OptionalMessage = ex.Message;
                return result;
            }
        }

        /// <summary>
        /// To parse apisensor response model to sensor subscription response model.
        /// </summary>
        /// <param name="api"></param>
        /// <returns></returns>
        internal static SensorSubRespModel ParseSensorSubRespModel(MolexAPISensorSubRespModel model)
        {
            SensorSubRespModel sensorSubRespModel = new SensorSubRespModel();

            try
            {
                sensorSubRespModel.SensorID = model.sensorID;
                sensorSubRespModel.Timestamp = model.timestamp;
                sensorSubRespModel.ZoneID = model.zoneID;
                sensorSubRespModel.Value = Convert.ToSingle(model.value);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "MolexAPIDataConverterHelper:ParseSensorSubRespModel:Json data parsing failed");
            }

            return sensorSubRespModel;
        }
    }
}
