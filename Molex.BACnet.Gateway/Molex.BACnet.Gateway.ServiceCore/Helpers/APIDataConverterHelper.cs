﻿//#region File Header

//// Copyright ©  2016 UniDEL Systems Pvt. Ltd.
//// All rights are reserved. Reproduction or transmission in whole or in part, in any form or by any
//// means, electronic, mechanical or otherwise, is prohibited without the prior written consent of the
//// copyright owner.
////
//// Filename          : CommonHelper.cs
//// Author            : Rupesh Saw
//// Description       : This Helper class include multiple function used for retriving data from Molex API and passing to Entity Model API
//// Revision History  : 
//// 
//// Date        Author            Modification
//// 19-05-2017  Rupesh Saw      File Created

//#endregion

//using Molex.BACnet.Gateway.Log;
//using Molex.BACnet.Gateway.ServiceCore.Constants;
//using Molex.BACnet.Gateway.ServiceCore.Models;
//using Molex.BACnet.Gateway.ServiceCore.Models.APIModels;
//using Molex.BACnet.Gateway.ServiceCore.Models.Entity;
//using Molex.StackDataObjects.Constants;
//using Molex.BACnet.Gateway.ServiceCore.Repository;
//using Newtonsoft.Json;
//using Newtonsoft.Json.Linq;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Configuration;

//namespace Molex.BACnet.Gateway.ServiceCore.Helpers
//{
//    /// <summary>
//    /// This class is defined for parsing JSON data to API Models and from API Model to Entity Models so that
//    /// It will be available as per our defined BACnet structure.
//    /// </summary>
//    public class APIDataConverterHelper
//    {
//        /// <summary>
//        /// Parses the zone state json data.
//        /// </summary>
//        /// <param name="jsonData">The json data.</param>
//        /// <param name="mapIndividualFixtures">if set to <c>true</c> [map individual fixtures].</param>
//        /// <param name="mapIndividualSensors">if set to <c>true</c> [map individual sensors].</param>
//        /// <returns></returns>
//        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>7/12/20178:29 PM</TimeStamp>
//        public static ResultModel<APIMolexZonePresentStateDataModel> ParseZoneStateJSONData(string jsonData, bool mapIndividualFixtures, bool mapIndividualSensors)
//        {
//            Logger.Instance.Log(LogLevel.DEBUG, "APIDataConverterHelper.ParseZoneStateJSONData : Reading JSON Data : Started ");
//            ResultModel<APIMolexZonePresentStateDataModel> result = new ResultModel<APIMolexZonePresentStateDataModel>();
//            APIMolexZonePresentStateDataModel zone = new APIMolexZonePresentStateDataModel();

//            if (jsonData != null && jsonData.Length == 0)
//            {
//                Log.Logger.Instance.Log(Log.LogLevel.ERROR, "APIDataConverterHelper.ParseZoneStateJSONData : Empty JSON File");
//                result.IsSuccess = false;
//                result.Error = new ErrorModel();
//                result.Error.ErrorCategory = AppConstants.ERROR_CATEGORY_DATAPARSING;
//                result.Error.ErrorCode = AppConstants.ERROR_CATEGORY_DATAPARSING_EMPTY_JSON;
//                return result;
//            }

//            try
//            {
//                Dictionary<string, dynamic> jsonResult = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(jsonData);
//                // To handle string to long conversion of brightness in default space  fixtures
//                long brighness;
//                bool isBrighness = long.TryParse(Convert.ToString(jsonResult["brightness"]), out brighness);

//                if (isBrighness)
//                {
//                    zone.Brightness = brighness;
//                }

//                zone.AvgAmbientLight = jsonResult["averageAmbientLight"];
//                zone.AvgColorTemperature = jsonResult["averageColourTemperature"];
//                zone.LightScene = jsonResult["lightScene"];
//                zone.DHTargetLux = jsonResult["DHTargetLux"];
//                zone.PresTimeout = jsonResult["presTimeout"];
//                zone.PresRate = jsonResult["presRate"];
//                Dictionary<string, APIDeviceModel> Devices = JsonConvert.DeserializeObject<Dictionary<string, APIDeviceModel>>(jsonResult["devices"].ToString());

//                if (mapIndividualFixtures || mapIndividualSensors)
//                {
//                    foreach (string key in Devices.Keys)
//                    {
//                        if (key.ToString().ToLower().Contains("sensor") && mapIndividualSensors)
//                        {
//                            APIMolexSensorPresentStateDataModel sensorPresentStateDataModel = new APIMolexSensorPresentStateDataModel();
//                            sensorPresentStateDataModel.OutPut = Devices[key].OutPut;
//                            sensorPresentStateDataModel.Status = Devices[key].Status;
//                            zone.Sensors.Add(key, sensorPresentStateDataModel);
//                        }
//                        if (key.ToString().ToLower().Contains("fixture") && mapIndividualFixtures)
//                        {
//                            APIMolexFixturePresentStateDataModel fixturePresentStateDataModel = new APIMolexFixturePresentStateDataModel();
//                            fixturePresentStateDataModel.OutPut = Devices[key].OutPut;
//                            fixturePresentStateDataModel.Status = Devices[key].Status;
//                            zone.Fixtures.Add(key, fixturePresentStateDataModel);
//                        }
//                    }
//                }

//                result.IsSuccess = true;
//                result.Data = zone;
//                return result;
//            }
//            catch (Exception ex)
//            {
//                Log.Logger.Instance.Log(Log.LogLevel.ERROR, "APIDataConverterHelper.ParseZoneStateJSONData : JSON Format Error");
//                result.IsSuccess = false;
//                result.Error = new ErrorModel();
//                result.Error.ErrorCategory = AppConstants.ERROR_CATEGORY_DATAPARSING_JSON_FORMAT_ERROR;
//                result.Error.ErrorCode = AppConstants.ERROR_CATEGORY_DATAPARSING;
//                result.Error.OptionalMessage = ex.Message;
//                return result;
//            }

//        }

//        /// <summary>
//        /// Parses the API molex zone data to zone data model.
//        /// </summary>
//        /// <param name="model">The model.</param>
//        /// <returns>ZonePresentStateDataModel</returns>
//        /// <CreatedBy>Rohit Galgali</CreatedBy>
//        /// <TimeStamp>5/30/201712:13 PM</TimeStamp>
//        //public static ZonePresentStateDataModel ParseAPIMolexZoneDataToZoneDataModel(APIMolexZonePresentStateDataModel model)
//        //{
//        //    ZonePresentStateDataModel zoneModel = new ZonePresentStateDataModel();

//        //    try
//        //    {
//        //        zoneModel.Brightness = model.Brightness;
//        //        zoneModel.LightScene = model.LightScene;
//        //        zoneModel.DHTargetLux = model.DHTargetLux;
//        //        zoneModel.PresTimeout = model.PresTimeout;
//        //        zoneModel.PresRate = model.PresRate;
//        //        zoneModel.Mood = model.Mood;
//        //        zoneModel.OperationalStatus = model.OperationalStatus;
//        //        zoneModel.Palette = Convert.ToInt32(model.Palette);
//        //        foreach (string key in model.Sensors.Keys)
//        //        {
//        //            SensorPresentStateDataModel sensorModel = new SensorPresentStateDataModel();

//        //            if (model.Sensors[key].OutPut.Count > 1)
//        //            {
//        //                sensorModel.IsReadSuccess = true;
//        //                sensorModel.PresentValue = Convert.ToSingle(model.Sensors[key].OutPut[1]);
//        //            }
//        //            else
//        //            {
//        //                sensorModel.IsReadSuccess = false;
//        //            }

//        //            if (AppCoreRepo.Instance.UnreliableStatusList.Contains(model.Sensors[key].Status.ToLower()))
//        //            {
//        //                sensorModel.Reliability = BacnetReliability.ReliabilityUnreliableOther;
//        //            }
//        //            else
//        //            {
//        //                sensorModel.Reliability = BacnetReliability.ReliabilityNoFaultDetected;
//        //            }

//        //            zoneModel.Sensors.Add(key, sensorModel);

//        //        }

//        //        foreach (string key in model.Fixtures.Keys)
//        //        {
//        //            FixturePresentStateDataModel fixtureModel = new FixturePresentStateDataModel();

//        //            if (AppCoreRepo.Instance.UnreliableStatusList.Contains(model.Fixtures[key].Status.ToLower()))
//        //            {
//        //                fixtureModel.Reliability = BacnetReliability.ReliabilityUnreliableOther;
//        //            }
//        //            else
//        //            {
//        //                fixtureModel.Reliability = BacnetReliability.ReliabilityNoFaultDetected;
//        //            }

//        //            zoneModel.Fixtures.Add(key, fixtureModel);
//        //        }
//        //    }
//        //    catch (Exception ex)
//        //    {
//        //        Logger.Instance.Log(ex, LogLevel.ERROR, "APIDataConverterHelper:ParseAPIMolexZoneDataToZoneDataModel:Json data parsing failed");
//        //    }

//        //    return zoneModel;
//        //}

//        /// <summary>
//        /// This function parsing JSON Data into API Model 
//        /// </summary>
//        /// <param name="jsonData">jsonData</param>
//        /// <returns>JSON Data String format</returns>
//        public static ResultModel<APIMolexModel> ParseJSONData(string jsonData)
//        {
//            Logger.Instance.Log(LogLevel.DEBUG, "APIDataConverterHelper.ParseJSONData : Reading JSON Data : Started ");

//            ResultModel<APIMolexModel> result = new ResultModel<APIMolexModel>();
//            APIMolexModel project = new APIMolexModel();

//            if (jsonData != null && jsonData.Length == 0)
//            {
//                Log.Logger.Instance.Log(Log.LogLevel.ERROR, "APIDataConverterHelper.ParseJSONData : Empty JSON File");
//                result.IsSuccess = false;
//                result.Error = new ErrorModel();
//                result.Error.ErrorCategory = AppConstants.ERROR_CATEGORY_DATAPARSING;
//                result.Error.ErrorCode = AppConstants.ERROR_CATEGORY_DATAPARSING_EMPTY_JSON;
//                return result;
//            }

//            try
//            {
//                var jsonResult = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(jsonData);
//                JObject obj = jsonResult["Molex"];
//                project.Name = obj["name"].ToString();
//                project.Client = obj["client"].ToString();
//                project.Contact = obj["contact"].ToString();
//                project.BIMFile = obj["BIMFile"].ToString();
//                project.Stage = obj["stage"].ToString();
//                project.BaseIPFixtures = obj["baseIPFixtures"].ToString();
//                project.LastModifiedDate = Convert.ToDateTime(obj["lastModifiedDate"].ToString());
//                project.FormatVersionnumber = obj["formatVersionnumber"].ToString();
//                string strbuildings = obj["buildings"].ToString();
//                JObject jsonbuildings = JObject.Parse(strbuildings);
//                string strdemoSpace = obj["demoSpace"].ToString();
//                JObject jsondemoSpace = JObject.Parse(strbuildings);
//                string strcontrollers = obj["controllers"].ToString();
//                JObject jsoncontrollers = JObject.Parse(strcontrollers);
//                APIControllerModel controller = new APIControllerModel();
//                #region Fetching Controller Data

//                foreach (var _controller in jsoncontrollers)
//                {
//                    string contrl = jsoncontrollers[_controller.Key].ToString();
//                    JObject jsonResultController = JObject.Parse(contrl);
//                    controller.Id = _controller.Key.ToString();
//                    controller.Name = jsonResultController["name"].ToString();
//                    controller.Address = jsonResultController["address"].ToString();
//                    controller.Location = jsonResultController["location"].ToString();
//                    controller.Config = jsonResultController["config"].ToString();
//                    project.Controllers.Add(_controller.Key, controller);
//                    string _spaces = jsonResultController["spaces"].ToString();
//                    JObject _allSpace = JObject.Parse(_spaces);
//                    string _lightscenes = jsonResultController["lightScenes"].ToString();
//                    JObject _alllightscenes = JObject.Parse(_lightscenes);

//                    foreach (var currentSpace in _allSpace)
//                    {
//                        APIUser_SpaceModel space = new APIUser_SpaceModel();
//                        string curspace = _allSpace[currentSpace.Key].ToString();
//                        JObject jsonResultSpace = JObject.Parse(curspace);
//                        space.Id = currentSpace.Key.ToString();
//                        space.Name = jsonResultSpace["name"].ToString();
//                        space.Type = jsonResultSpace["type"].ToString();
//                        space.Layer = jsonResultSpace["layer"].ToString();
//                        space.DefaultMood = jsonResultSpace["defaultMood"].ToString();
//                        space.DefaultLightScene = jsonResultSpace["defaultLightScene"].ToString();
//                        space.AmBXSpace = Convert.ToInt32(jsonResultSpace["amBXSpace"].ToString());
//                        space.DefaultBrightness = Convert.ToInt32(jsonResultSpace["defaultBrightness"].ToString());
//                        controller.Spaces.Add(currentSpace.Key, space);
//                    }
//                    int i = 0;
//                    foreach (var currentlightscenes in _alllightscenes)
//                    {
//                        APILightscenesModel apiLightscenesModel = new APILightscenesModel();
//                        string currapiLightscenesModel = _alllightscenes[currentlightscenes.Key].ToString();
//                        JObject jsonResultlightscene = JObject.Parse(currapiLightscenesModel);
//                        apiLightscenesModel.Id = currentlightscenes.Key.ToString();
//                        apiLightscenesModel.Name = jsonResultlightscene["name"].ToString();
//                        apiLightscenesModel.Slot = Convert.ToInt32(jsonResultlightscene["slot"].ToString());
//                        controller.LightScenes.Add(currentlightscenes.Key, apiLightscenesModel);

//                        AppCoreRepo.Instance.AllLightScene[i] = apiLightscenesModel.Id;
//                        i++;
//                    }
//                }
//                #endregion Fetching Controller Data

//                #region Fetching Building Data

//                foreach (var _Building in jsonbuildings)
//                {
//                    APIBuildingModel model = new APIBuildingModel();
//                    string building = jsonbuildings[_Building.Key].ToString();
//                    project.Buildings.Add(_Building.Key, model);
//                    JObject jsonResultBuilding = JObject.Parse(building);
//                    model.Id = _Building.Key.ToString();
//                    model.Name = jsonResultBuilding["name"].ToString();
//                    model.Location = jsonResultBuilding["location"].ToString();
//                    model.UserData = jsonResultBuilding["userData"].ToString();
//                    model.Utilisation = Int32.Parse(jsonResultBuilding["utilisation"].ToString());
//                    string _floor = jsonResultBuilding["floors"].ToString();
//                    JObject _allFloor = JObject.Parse(_floor);

//                    foreach (var currentfloor in _allFloor)
//                    {
//                        APIFloorModel floor = new APIFloorModel();
//                        string curFloor = _allFloor[currentfloor.Key].ToString();
//                        model.Floors.Add(currentfloor.Key, floor);
//                        JObject jsonResultFloor = JObject.Parse(curFloor);
//                        floor.Id = currentfloor.Key.ToString();
//                        floor.Name = jsonResultFloor["name"].ToString();
//                        floor.FloorPlan = jsonResultFloor["floorPlan"].ToString();
//                        floor.GridH = Convert.ToInt32(jsonResultFloor["gridH"].ToString());
//                        floor.GridW = Convert.ToInt32(jsonResultFloor["gridW"].ToString());
//                        string fixtures = jsonResultFloor["fixtures"].ToString();
//                        JObject jsonResultFixtures = JObject.Parse(fixtures);

//                        foreach (var currentFixture in jsonResultFixtures)
//                        {
//                            APIFixtureModel fixture = new APIFixtureModel();
//                            string curFixture = jsonResultFixtures[currentFixture.Key].ToString();
//                            JObject jsonResultFixture = JObject.Parse(curFixture);
//                            floor.Fixtures.Add(currentFixture.Key, fixture);
//                            fixture.Id = currentFixture.Key.ToString();
//                            fixture.Name = jsonResultFixture["name"].ToString();
//                            fixture.Role = jsonResultFixture["role"].ToString();
//                            fixture.Protocol = jsonResultFixture["protocol"].ToString();
//                            fixture.Type = jsonResultFixture["type"].ToString();
//                            fixture.Address = jsonResultFixture["address"].ToString();
//                            fixture.Space = jsonResultFixture["space"].ToString();
//                            fixture.GridX = Convert.ToUInt32(jsonResultFixture["gridX"].ToString());
//                            fixture.GridY = Convert.ToUInt32(jsonResultFixture["gridY"].ToString());

//                            if (!floor.Zones.ContainsKey(fixture.Space))
//                            {
//                                APIZoneModel zone = new APIZoneModel();
//                                zone.Id = controller.Spaces[fixture.Space].Id;
//                                zone.AmBXSpaceId = controller.Spaces[fixture.Space].AmBXSpace.ToString();
//                                zone.Name = controller.Spaces[fixture.Space].Name;
//                                floor.Zones.Add(fixture.Space, zone);
//                            }

//                            floor.Zones[fixture.Space].Fixtures.Add(fixture.Id, fixture);
//                        }

//                        string Sensors = jsonResultFloor["sensors"].ToString();
//                        JObject jsonResultSensors = JObject.Parse(Sensors);

//                        foreach (var currentSensor in jsonResultSensors)
//                        {
//                            APISensorModel sensor = new APISensorModel();
//                            string curSensor = jsonResultSensors[currentSensor.Key].ToString();
//                            JObject jsonResultSensor = JObject.Parse(curSensor);

//                            floor.Sensors.Add(currentSensor.Key, sensor);

//                            sensor.Id = currentSensor.Key.ToString();
//                            sensor.Name = jsonResultSensor["name"].ToString();
//                            sensor.Role = jsonResultSensor["role"].ToString();
//                            sensor.Type = jsonResultSensor["type"].ToString();
//                            sensor.UserData = jsonResultSensor["userData"].ToString();
//                            sensor.Address = jsonResultSensor["address"].ToString();
//                            sensor.Controller = jsonResultSensor["controller"].ToString();
//                            sensor.AmBXLocation = jsonResultSensor["amBXLocation"].ToString();
//                            sensor.Protocol = jsonResultSensor["protocol"].ToString();
//                            sensor.Space = jsonResultSensor["space"].ToString();
//                            sensor.PollingRate = Convert.ToInt32(jsonResultSensor["pollingRate"].ToString());
//                            sensor.GridX = Convert.ToUInt32(jsonResultSensor["gridX"].ToString());
//                            sensor.GridY = Convert.ToUInt32(jsonResultSensor["gridY"].ToString());

//                            if (!floor.Zones.ContainsKey(sensor.Space))
//                            {
//                                APIZoneModel zone = new APIZoneModel();
//                                zone.Id = controller.Spaces[sensor.Space].Id;
//                                zone.AmBXSpaceId = controller.Spaces[sensor.Space].AmBXSpace.ToString();
//                                zone.Name = controller.Spaces[sensor.Space].Name;
//                                floor.Zones.Add(sensor.Space, zone);
//                            }

//                            floor.Zones[sensor.Space].Sensors.Add(sensor.Id, sensor);
//                        }
//                    }
//                }

//                #endregion Building Data
//                Logger.Instance.Log(LogLevel.DEBUG, "APIDataConverterHelper.ParseJSONData : Reading JSON Data : Completed ");

//                result.IsSuccess = true;
//                result.Data = project;
//                return result;
//            }
//            catch (Exception ex)
//            {
//                Log.Logger.Instance.Log(ex, Log.LogLevel.ERROR, "APIDataConverterHelper.ParseJSONData : JSON Format Error");
//                //ResponseResultModel result = new Models.Error.ResponseResultModel();
//                result.IsSuccess = false;
//                result.Error = new ErrorModel();
//                result.Error.ErrorCategory = AppConstants.ERROR_CATEGORY_DATAPARSING_JSON_FORMAT_ERROR;
//                result.Error.ErrorCode = AppConstants.ERROR_CATEGORY_DATAPARSING;
//                result.Error.OptionalMessage = ex.Message;
//                return result;
//            }

//        }

//        /// <summary>
//        ///  Parsing Data from API Model to EnitityModel
//        /// </summary>
//        /// <param name="molex">APIMolexModel</param>
//        /// <returns>Data in APIMolex Model</returns>
//        public static ResultModel<EntityBaseModel> ParseEntityModel(APIMolexModel molex)
//        {
//            Logger.Instance.Log(LogLevel.DEBUG, "APIDataConverterHelper.ParseEntityModel : Parsing APIMolexModel to Entity Model : Started ");

//            ResultModel<EntityBaseModel> result = new ResultModel<EntityBaseModel>();

//            try
//            {
//                ControllerEntityModel controller = new ControllerEntityModel();

//                controller = GetControllerModelData(molex.Controllers.First().Value);
//                controller.EntityKey = molex.Controllers.First().Key;

//                if (controller != null)
//                {
//                    foreach (var currentBuilding in molex.Buildings.Values)
//                    {
//                        BuildingEntityModel building = (BuildingEntityModel)GetBuildingModelData(currentBuilding);
//                        building.Parent = controller;
//                        controller.Childs.Add(building);

//                        foreach (var currentFloor in currentBuilding.Floors.Values)
//                        {
//                            FloorEntityModel floor = (FloorEntityModel)GetFloorModelData(currentFloor);
//                            floor.Parent = building;
//                            building.Childs.Add(floor);

//                            foreach (var currentZone in currentFloor.Zones.Values)
//                            {
//                                if (currentZone.AmBXSpaceId == AppConstants.BUILDING_DEFAULT_ZONE_ID)
//                                {
//                                    continue;
//                                }

//                                ZoneEntityModel zone = (ZoneEntityModel)GetZoneModelData(currentZone);
//                                zone.Parent = floor;
//                                floor.Childs.Add(zone);

//                                foreach (string key in currentZone.Fixtures.Keys)
//                                {
//                                    FixtureEntityModel fixture = (FixtureEntityModel)GetFixtureModelData(currentFloor.Fixtures[key]);
//                                    fixture.Parent = zone;
//                                    zone.Fixtures.Add(fixture);
//                                }

//                                foreach (string key in currentZone.Sensors.Keys)
//                                {
//                                    SensorEntityModel sensor = (SensorEntityModel)GetSensorModelData(currentFloor.Sensors[key]);
//                                    sensor.Parent = zone;
//                                    zone.Sensors.Add(sensor);
//                                }
//                            }
//                        }
//                    }

//                    Logger.Instance.Log(LogLevel.DEBUG, "APIDataConverterHelper.ParseEntityModel : Parsing APIMolexModel to Entity Model : Completed ");
//                    result.IsSuccess = true;
//                    result.Data = controller;
//                    return result;
//                }
//                else
//                {
//                    Logger.Instance.Log(Log.LogLevel.ERROR, "APIDataConverterHelper.ParseEntityModel : Empty Controller Model.. No Data available");
//                    result.IsSuccess = false;
//                    result.Error.ErrorCode = AppConstants.ERROR_CATEGORY_MODELDATA;
//                    result.Error.ErrorCategory = AppConstants.ERROR_CATEGORY_MODELDATA_DATA_NOT_FOUND;
//                    return result;
//                }
//            }
//            catch (Exception ex)
//            {
//                Logger.Instance.Log(ex, Log.LogLevel.ERROR, "APIDataConverterHelper.ParseEntityModel : Model Parsing Error ... Mismatch objects ");
//                result.IsSuccess = false;
//                result.Error.ErrorCode = AppConstants.ERROR_CATEGORY_MODELPARSINGERROR;
//                result.Error.ErrorCategory = AppConstants.ERROR_CATEGORY_MODELPARSINGERROR_MODEL_DATA_PARSING_ERROR;
//                result.Error.OptionalMessage = ex.Message;
//                return result;
//            }
//        }

//        /// <summary>
//        /// Parsing Data from APIControllerModel to ControllerEnitityModel
//        /// </summary>
//        /// <param name="controller">APIControllerModel</param>
//        /// <returns>Controller information in Entity Model</returns>
//        internal static ControllerEntityModel GetControllerModelData(APIControllerModel controller)
//        {
//            ControllerEntityModel controllerModel = new ControllerEntityModel();
//            controllerModel.EntityName = controller.Name;
//            controllerModel.Location = controller.Location;
//            return controllerModel;
//        }

//        /// <summary>
//        /// Parsing Data from APIBuildingModel to BuildingEnitityModel
//        /// </summary>
//        /// <param name="building">APIBuildingModel</param>
//        /// <returns>Building information in Entity Model</returns>
//        internal static EntityBaseModel GetBuildingModelData(APIBuildingModel building)
//        {
//            BuildingEntityModel buildingModel = new BuildingEntityModel();
//            buildingModel.EntityKey = building.Id;
//            buildingModel.EntityName = building.Name;
//            buildingModel.Location = building.Location;
//            buildingModel.Description = AppResources.AppResource.BuildingDescription;
//            return buildingModel;

//        }

//        /// <summary>
//        /// Parsing Data from APIFloorModel to FloorEnitityModel
//        /// </summary>
//        /// <param name="floor">APIFloorModel</param>
//        /// <returns>Floor information in Entity Model</returns>
//        internal static EntityBaseModel GetFloorModelData(APIFloorModel floor)
//        {
//            FloorEntityModel floorModel = new FloorEntityModel();
//            floorModel.EntityKey = floor.Id;
//            floorModel.EntityName = floor.Name;
//            floorModel.Description = AppResources.AppResource.FloorDescription;
//            return floorModel;
//        }

//        /// <summary>
//        /// Parsing Data from APIZoneModel to ZoneEnitityModel
//        /// </summary>
//        /// <param name="zone">APIZoneModel</param>
//        /// <returns>Zone information in Entity Model</returns>
//        internal static EntityBaseModel GetZoneModelData(APIZoneModel zone)
//        {
//            ZoneEntityModel zoneModel = new ZoneEntityModel();
//            zoneModel.EntityKey = zone.Id;
//            zoneModel.ZoneId = zone.AmBXSpaceId;
//            zoneModel.EntityName = zone.Name;
//            return zoneModel;
//        }

//        /// <summary>
//        /// Parsing Data from APIFixtureModel to FixtureEnitityModel
//        /// </summary>
//        /// <param name="fixture">APIFixtureModel</param>
//        /// <returns>Fixture information in Entity Model</returns>
//        internal static EntityBaseModel GetFixtureModelData(APIFixtureModel fixture)
//        {
//            FixtureEntityModel fixtureModel = new FixtureEntityModel();
//            fixtureModel.EntityKey = fixture.Id;
//            fixtureModel.EntityName = fixture.Name;
//            fixtureModel.Description = String.Concat(fixture.Role, " ", fixture.Name);
//            fixtureModel.ProprietaryGridX = fixture.GridX;
//            fixtureModel.ProprietaryGridY = fixture.GridY;
//            fixtureModel.UserData = Convert.ToSingle(fixture.UserData);
//            return fixtureModel;
//        }

//        /// <summary>
//        /// Parsing Data from APISensorModel to SensorEnitityModel
//        /// </summary>
//        /// <param name="sensor">APISensorModel</param>
//        /// <returns>Sensor information in Entity Model</returns>
//        internal static EntityBaseModel GetSensorModelData(APISensorModel sensor)
//        {
//            SensorEntityModel sensorModel = new SensorEntityModel();
//            sensorModel.EntityKey = sensor.Id;
//            sensorModel.EntityName = sensor.Name;
//            sensorModel.Description = String.Concat(sensor.Role, " ", sensor.Name);
//            sensorModel.SensorType = GetSensorType(sensor.Role);
//            sensorModel.ProprietaryGridX = sensor.GridX;
//            sensorModel.ProprietaryGridY = sensor.GridY;
//            sensorModel.Role = sensor.Role;
//            sensorModel.DeviceType = sensor.Type;
//            return sensorModel;
//        }

//        /// <summary>
//        /// Get sensor type by role
//        /// </summary>
//        /// <param name="role">The role.</param>
//        /// <returns>SensorType</returns>
//        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>09-06-201705:10 PM</TimeStamp>
//        internal static AppConstants.SensorType GetSensorType(string role)
//        {
//            AppConstants.SensorType sensorType = AppConstants.SensorType.NotSupported;
//            switch (role)
//            {
//                case "CT":
//                    sensorType = AppConstants.SensorType.ColorTemperature;
//                    break;
//                case "AQ":
//                    sensorType = AppConstants.SensorType.AirQuality;
//                    break;
//                case "PIR":
//                    sensorType = AppConstants.SensorType.Presense;
//                    break;
//                case "PW":
//                    sensorType = AppConstants.SensorType.Power;
//                    break;
//                case "AL":
//                    sensorType = AppConstants.SensorType.LuxLevel;
//                    break;
//                default:
//                    Logger.Instance.Log(LogLevel.ERROR, "APIDataConverterHelper.GetSensorType: Role Not avilable " + role);
//                    break;
//            }
//            return sensorType;
//        }
//    }
//}
