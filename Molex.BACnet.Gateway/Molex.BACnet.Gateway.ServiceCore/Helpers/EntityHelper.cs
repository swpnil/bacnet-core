﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              EntityHelper.cs
///   Description:        This class is used for providing helper functions for Entity Model.
///   Author:             Rupesh Saw                  
///   Date:               05/19/17
///---------------------------------------------------------------------------------------------
#endregion

using Molex.BACnet.Gateway.Log;
using Molex.BACnet.Gateway.ServiceCore.AppResources;
using Molex.BACnet.Gateway.ServiceCore.Constants;
using Molex.BACnet.Gateway.ServiceCore.Models;
using Molex.BACnet.Gateway.ServiceCore.Models.Entity;
using Molex.BACnet.Gateway.ServiceCore.Repository;
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Response.AutoResponses;
using System;
using System.Collections.Generic;

namespace Molex.BACnet.Gateway.ServiceCore.Helpers
{
    /// <summary>
    /// This class is used for providing helper functions for Entity Model
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:14 PM</TimeStamp>
    public static class EntityHelper
    {
        /// <summary>
        /// Gets the zone data.
        /// </summary>
        /// <param name="controller">The controller.</param>
        /// <returns>Dictionary containing BACnetObjectId as Key and EntityModel as Value</returns>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201711:52 AM</TimeStamp>
        public static ResultModel<Dictionary<string, EntityBaseModel>> GetZoneData(EntityBaseModel controller)
        {
            Logger.Instance.Log(LogLevel.DEBUG, "GetZoneData: Reading zone level data from ControllerEntityModel - Started ");
            ResultModel<Dictionary<string, EntityBaseModel>> result = new ResultModel<Dictionary<string, EntityBaseModel>>();

            try
            {
                Dictionary<string, EntityBaseModel> zonesdata = new Dictionary<string, EntityBaseModel>();
                EntityBaseModel buildings = controller.Childs.Find(x => x.EntityType == Constants.AppConstants.EntityTypes.Building);
                List<EntityBaseModel> floors = buildings.Childs.FindAll(x => x.EntityType == AppConstants.EntityTypes.Floor);

                foreach (FloorEntityModel item in floors)
                {
                    List<EntityBaseModel> zones = item.Childs.FindAll(x => x.EntityType == AppConstants.EntityTypes.Zone);
                    foreach (ZoneEntityModel zone in zones)
                    {
                        zonesdata.Add(zone.EntityKey, zone);
                    }
                }

                Logger.Instance.Log(LogLevel.DEBUG, "GetZoneData: Reading zone level data from ControllerEntityModel - Completed ");

                result.IsSuccess = true;
                result.Data = zonesdata;
                return result;
            }
            catch (Exception ex)
            {
                result.Error = new ErrorModel();
                result.Error.ErrorCategory = AppConstants.ERROR_CATEGORY_MODELDATA;
                result.Error.ErrorCode = AppConstants.ERROR_CATEGORY_MODELDATA_DATA_NOT_FOUND;
                result.Error.OptionalMessage = ex.Message;
                return result;
            }
        }

        /// <summary>
        /// This method is used to get device ID for parent entity if BACnet Device is not set for current entity
        /// </summary>
        /// <param name="entityBaseModel">This model contains parent child Entity data</param>
        /// <returns>It returns Device ID for Parent Entity</returns>
        public static ResultModel<int> GetDeviceId(EntityBaseModel entityBaseModel)
        {
            //Logger.Instance.Log(LogLevel.DEBUG, "GetDeviceId: Reading DeviceId - Started ");

            ResultModel<int> result = new ResultModel<int>();
            try
            {
                if (entityBaseModel.BACnetData.DeviceID != AppConstants.DEFAULT_DEVICE_ID)
                {
                    result.Data = entityBaseModel.BACnetData.DeviceID;
                    result.IsSuccess = true;
                    return result;
                }

                if (entityBaseModel.Parent == null)
                {
                    result.IsSuccess = false;
                    return result;
                }

                while (entityBaseModel.Parent != null)
                {
                    if (entityBaseModel.Parent.BACnetData.DeviceID != AppConstants.DEFAULT_DEVICE_ID)
                    {
                        result.Data = entityBaseModel.Parent.BACnetData.DeviceID;
                        result.IsSuccess = true;
                        break;
                    }
                    entityBaseModel = entityBaseModel.Parent;
                };

            }
            catch (Exception ex)
            {
                result.Error = new ErrorModel();
                result.IsSuccess = false;
                result.Error.ErrorCategory = AppConstants.ERROR_CATEGORY_MODELDATA;
                result.Error.ErrorCode = AppConstants.ERROR_CATEGORY_MODELDATA_DATA_NOT_FOUND;
                result.Error.OptionalMessage = ex.Message;
            }

            //Logger.Instance.Log(LogLevel.DEBUG, "GetDeviceId: Reading DeviceId - Completed ");

            return result;

        }

        /// <summary>
        /// Gets all entity look up.
        /// </summary>
        /// <param name="controller">The controller.</param>
        /// <returns></returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>7/4/20172:16 PM</TimeStamp>
        public static ResultModel<Dictionary<string, EntityBaseModel>> GetAllEntityLookUp(EntityBaseModel controller)
        {
            ResultModel<Dictionary<string, EntityBaseModel>> result = new ResultModel<Dictionary<string, EntityBaseModel>>();

            try
            {
                result.Data = new Dictionary<string, EntityBaseModel>();
                result.Data.Add(controller.EntityKey, controller);

                foreach (EntityBaseModel buildingBaseModel in controller.Childs)
                {
                    result.Data.Add(buildingBaseModel.EntityKey, buildingBaseModel);

                    foreach (EntityBaseModel floorBaseModel in buildingBaseModel.Childs)
                    {
                        result.Data.Add(floorBaseModel.EntityKey, floorBaseModel);

                        foreach (ZoneEntityModel zoneBaseModel in floorBaseModel.Childs)
                        {
                            result.Data.Add(zoneBaseModel.EntityKey, zoneBaseModel);

                            foreach (FixtureEntityModel fixtureItem in zoneBaseModel.Fixtures)
                            {
                                result.Data.Add(fixtureItem.EntityKey, fixtureItem);
                            }

                            foreach (SensorEntityModel sensorItem in zoneBaseModel.Sensors)
                            {
                                result.Data.Add(sensorItem.EntityKey, sensorItem);
                            }
                        }
                    }
                }

                result.IsSuccess = true;
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR);
                result.Error = new ErrorModel();
                result.Error.ErrorCategory = AppConstants.ERROR_CATEGORY_MODELDATA;
                result.Error.ErrorCode = AppConstants.ERROR_CATEGORY_MODELDATA_DATA_NOT_FOUND;
                result.Error.OptionalMessage = ex.Message;
                result.IsSuccess = false;
            }

            return result;
        }

        /// <summary>
        /// Gets the type of the sensor.
        /// </summary>
        /// <param name="sensorType">Type of the sensor.</param>
        /// <returns>BacnetObjectType</returns>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>08-06-201712:53 PM</TimeStamp>
        public static BacnetObjectType GetBACnetObjectTypeBySensorType(AppConstants.SensorType sensorType)
        {
            switch (sensorType)
            {
                case AppConstants.SensorType.NotSupported:
                    break;

                case AppConstants.SensorType.ColorTemperature:
                case AppConstants.SensorType.AirQuality:
                case AppConstants.SensorType.Humidity:
                case AppConstants.SensorType.Temperature:
                case AppConstants.SensorType.Power:
                case AppConstants.SensorType.LuxLevel:
                    return BacnetObjectType.ObjectAnalogInput;

                case AppConstants.SensorType.Presence:
                    return BacnetObjectType.ObjectBinaryInput;

                default:
                    Logger.Instance.Log(LogLevel.ERROR, "SensorProcessor.GetSensorType:Sensor type not found");
                    return 0;
            }
            return 0;
        }

        /// <summary>
        /// Gets the key for object details dictonary.
        /// </summary>
        /// <param name="objectType">Type of the object.</param>
        /// <param name="lightingObjectproperty">The lighting objectproperty.</param>
        /// <returns></returns>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>6/19/201712:44 PM</TimeStamp>
        public static string GetObjectDetailsKey(BacnetObjectType objectType, string lightingObjectproperty)
        {
            if (string.IsNullOrEmpty(lightingObjectproperty))
                return objectType.ToString();
            else
                return string.Format("{0}_{1}", objectType.ToString(), lightingObjectproperty);
        }

        /// <summary>
        /// Gets the controller entity model.
        /// </summary>
        /// <param name="entityBaseModel">The entity base model.</param>
        /// <returns></returns>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>03-07-201705:25 PM</TimeStamp>
        public static ControllerEntityModel GetControllerEntityModel(EntityBaseModel entityBaseModel)
        {
            while (entityBaseModel.EntityType != AppConstants.EntityTypes.Controller)
            {
                entityBaseModel = entityBaseModel.Parent;
            }
            return entityBaseModel as ControllerEntityModel;
        }

        /// <summary>
        /// Gets the entity model from device identifier.
        /// </summary>
        /// <param name="deviceID">The device identifier.</param>
        /// <returns></returns>
        /// <CreatedBy>Rohit Galgali</CreatedBy>
        /// <TimeStamp>8/18/20176:49 PM</TimeStamp>
        public static EntityBaseModel GetEntityModelFromDeviceID(uint deviceID)
        {
            if (AppCoreRepo.Instance.ControllerEntityModel.BACnetData.DeviceID == deviceID)
            {
                return AppCoreRepo.Instance.ControllerEntityModel;
            }
            else
            {
                foreach (KeyValuePair<string, EntityBaseModel> item in AppCoreRepo.Instance.ZonesLookUp)
                {
                    if (item.Value.BACnetData.DeviceID == deviceID)
                    {
                        return item.Value;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Gets the object key.
        /// </summary>
        /// <param name="writePropertyNotificationResponse">The write property notification response.</param>
        /// <param name="entityBaseModel">The entity base model.</param>
        /// <returns></returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>10/6/20176:36 PM</TimeStamp>
        public static string GetObjectKey(WritePropertyNotificationResponse writePropertyNotificationResponse, EntityBaseModel entityBaseModel)
        {
            string objectKey = string.Empty;

            switch (writePropertyNotificationResponse.WritePropertyCallbackData.ObjectType)
            {
                case BacnetObjectType.ObjectStructuredView:
                case BacnetObjectType.ObjectDevice:
                    objectKey = EntityHelper.GetObjectDetailsKey(writePropertyNotificationResponse.WritePropertyCallbackData.ObjectType, AppConstants.LIGHTING_OBJECTDEVICE_OBJECT);
                    break;
                case BacnetObjectType.ObjectAnalogValue:
                    if (entityBaseModel as ZoneEntityModel != null)
                    {
                        if ((entityBaseModel as ZoneEntityModel).ZoneType == AppConstants.ZoneTypes.BlindSpace)
                        {
                            if (entityBaseModel.BACnetData.ObjectDetails == null)
                                return string.Empty;
                            foreach (var item in entityBaseModel.BACnetData.ObjectDetails)
                            {
                                if (item.Value.ObjectID == writePropertyNotificationResponse.WritePropertyCallbackData.ObjectInstance
                                    && item.Value.ObjectType == writePropertyNotificationResponse.WritePropertyCallbackData.ObjectType)
                                    return item.Key;
                            }
                        }
                        if ((entityBaseModel as ZoneEntityModel).ZoneType == AppConstants.ZoneTypes.UserSpace ||
                            (entityBaseModel as ZoneEntityModel).ZoneType == AppConstants.ZoneTypes.GeneralSpace)
                        {
                            if (entityBaseModel.BACnetData.ObjectDetails == null)
                                return string.Empty;
                            foreach (var item in entityBaseModel.BACnetData.ObjectDetails)
                            {
                                if (item.Value.ObjectID == writePropertyNotificationResponse.WritePropertyCallbackData.ObjectInstance
                                    && item.Value.ObjectType == writePropertyNotificationResponse.WritePropertyCallbackData.ObjectType)
                                    return item.Key;
                            }
                        }
                    }
                    if (entityBaseModel as FloorEntityModel != null || entityBaseModel as BuildingEntityModel != null)
                    {
                        if (entityBaseModel.BACnetData.ObjectDetails == null)
                            return string.Empty;
                        foreach (var item in entityBaseModel.BACnetData.ObjectDetails)
                        {
                            if (item.Value.ObjectID == writePropertyNotificationResponse.WritePropertyCallbackData.ObjectInstance
                                && item.Value.ObjectType == writePropertyNotificationResponse.WritePropertyCallbackData.ObjectType)
                                return item.Key;
                        }
                    }
                    objectKey = EntityHelper.GetObjectDetailsKey(writePropertyNotificationResponse.WritePropertyCallbackData.ObjectType, AppConstants.LIGHTING_BRIGHTENESS_OBJECT);
                    break;
                case BacnetObjectType.ObjectMultiStateValue:
                    objectKey = EntityHelper.GetObjectDetailsKey(writePropertyNotificationResponse.WritePropertyCallbackData.ObjectType, AppConstants.LIGHTING_LIGHTSCENE_OBJECT);
                    break;
                case BacnetObjectType.ObjectNotificationClass:
                    objectKey = EntityHelper.GetObjectDetailsKey(writePropertyNotificationResponse.WritePropertyCallbackData.ObjectType, AppConstants.LIGHTING_NOTIFICATIONCLASS_OBJECT);
                    break;
                case BacnetObjectType.ObjectAnalogInput:
                case BacnetObjectType.ObjectBinaryInput:
                    objectKey = EntityHelper.GetObjectDetailsKey(writePropertyNotificationResponse.WritePropertyCallbackData.ObjectType, AppConstants.LIGHTING_SENSOR_OBJECT);
                    break;
                case BacnetObjectType.ObjectBinaryValue:
                    string type = string.Empty;
                    if (entityBaseModel is BuildingEntityModel)
                        type = AppConstants.BUILDING_ONOFF_OBJECT;
                    else if (entityBaseModel is FloorEntityModel)
                        type = AppConstants.FLOOR_ONOFF_OBJECT;
                    else
                        type = AppConstants.ZONE_ONOFF_OBJECT;
                    objectKey = EntityHelper.GetObjectDetailsKey(writePropertyNotificationResponse.WritePropertyCallbackData.ObjectType, type);
                    break;
                case BacnetObjectType.ObjectPositiveIntegerValue:
                    {
                        ZoneEntityModel zoneEntityModel = entityBaseModel as ZoneEntityModel;

                        switch (zoneEntityModel.ZoneType)
                        {
                            case AppConstants.ZoneTypes.BeaconSpace:
                                if (entityBaseModel.BACnetData.ObjectDetails == null)
                                    return string.Empty;
                                foreach (var item in entityBaseModel.BACnetData.ObjectDetails)
                                {
                                    if (item.Value.ObjectID == writePropertyNotificationResponse.WritePropertyCallbackData.ObjectInstance
                                        && item.Value.ObjectType == writePropertyNotificationResponse.WritePropertyCallbackData.ObjectType)
                                        return item.Key;
                                }
                                break;
                            case AppConstants.ZoneTypes.UserSpace:
                            case AppConstants.ZoneTypes.GeneralSpace:
                                string dhTargetLux = EntityHelper.GetObjectDetailsKey(BacnetObjectType.ObjectPositiveIntegerValue, AppConstants.LIGHTING_DHTARGETLUX_OBJECT);
                                string presRate = EntityHelper.GetObjectDetailsKey(BacnetObjectType.ObjectPositiveIntegerValue, AppConstants.LIGHTING_PRESRATE_OBJECT);
                                string presTimeout = EntityHelper.GetObjectDetailsKey(BacnetObjectType.ObjectPositiveIntegerValue, AppConstants.LIGHTING_PRESTIMEOUT_OBJECT);

                                if (entityBaseModel.BACnetData.ObjectDetails.ContainsKey(dhTargetLux)
                                    && entityBaseModel.BACnetData.ObjectDetails[dhTargetLux].ObjectID == writePropertyNotificationResponse.WritePropertyCallbackData.ObjectInstance)
                                {
                                    objectKey = EntityHelper.GetObjectDetailsKey(writePropertyNotificationResponse.WritePropertyCallbackData.ObjectType, AppConstants.LIGHTING_DHTARGETLUX_OBJECT);
                                }
                                else if (entityBaseModel.BACnetData.ObjectDetails.ContainsKey(presRate)
                                     && entityBaseModel.BACnetData.ObjectDetails[presRate].ObjectID == writePropertyNotificationResponse.WritePropertyCallbackData.ObjectInstance)
                                {
                                    objectKey = EntityHelper.GetObjectDetailsKey(writePropertyNotificationResponse.WritePropertyCallbackData.ObjectType, AppConstants.LIGHTING_PRESRATE_OBJECT);
                                }
                                else if (entityBaseModel.BACnetData.ObjectDetails.ContainsKey(presTimeout)
                                     && entityBaseModel.BACnetData.ObjectDetails[presTimeout].ObjectID == writePropertyNotificationResponse.WritePropertyCallbackData.ObjectInstance)
                                {
                                    objectKey = EntityHelper.GetObjectDetailsKey(writePropertyNotificationResponse.WritePropertyCallbackData.ObjectType, AppConstants.LIGHTING_PRESTIMEOUT_OBJECT);
                                }
                                break;
                        }

                    }
                    break;
            }
            return objectKey;
        }

        /// <summary>
        /// Generates the analog object units.
        /// </summary>
        /// <param name="sensorType">The property value.</param>
        /// <returns>BacnetEngineeringUnits</returns>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>08-06-201712:12 PM</TimeStamp>
        public static BacnetEngineeringUnits GenerateAnalogObjectUnits(AppConstants.SensorType sensorType)
        {
            switch (sensorType)
            {
                case AppConstants.SensorType.ColorTemperature:
                    return BacnetEngineeringUnits.UnitsDegreesKelvin;
                case AppConstants.SensorType.Temperature:
                    if (!AppCoreRepo.Instance.BACnetConfigurationData.IsTempRequiredInFahrenheit)
                        return BacnetEngineeringUnits.UnitsDegreesCelsius;
                    else
                        return BacnetEngineeringUnits.UnitsDegreesFahrenheit;
                case AppConstants.SensorType.AirQuality:
                    return BacnetEngineeringUnits.UnitsPartsPerMillion;
                case AppConstants.SensorType.LuxLevel:
                    return BacnetEngineeringUnits.UnitsLuxes;
                case AppConstants.SensorType.Power:
                    return BacnetEngineeringUnits.UnitsWatts;
                case AppConstants.SensorType.Humidity:
                    return BacnetEngineeringUnits.UnitsPercentRelativeHumidity;
                case AppConstants.SensorType.Others:
                    return BacnetEngineeringUnits.UnitsPercent;
                default:
                    return BacnetEngineeringUnits.UnitsNoUnits;
            }

        }

        /// <summary>
        /// Gets the sensor minimum value.
        /// </summary>
        /// <param name="sensorType">Type of the sensor.</param>
        /// <returns>Single</returns>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>06-06-201705:49 PM</TimeStamp>
        public static Single GetSensorMinValue(AppConstants.SensorType sensorType)
        {
            switch (sensorType)
            {
                case AppConstants.SensorType.NotSupported:
                    return 10;
                case AppConstants.SensorType.Power:
                case AppConstants.SensorType.ColorTemperature:
                case AppConstants.SensorType.LuxLevel:
                case AppConstants.SensorType.Humidity:
                    return 0;
                case AppConstants.SensorType.AirQuality:
                    return 400;//Changes done for BAC-274
                case AppConstants.SensorType.Presence:
                    return 10;
                case AppConstants.SensorType.Temperature:
                    return -40;
                default:
                    Logger.Instance.Log(LogLevel.DEBUG, "SensorProcessor.GetSensorMinValue:Sensor Minimum value not found");
                    return 0;
            }
        }

        /// <summary>
        /// Gets the sensor maximum value.
        /// </summary>
        /// <param name="sensorType">Type of the sensor.</param>
        /// <returns>Single</returns>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>06-06-201705:49 PM</TimeStamp>
        public static Single GetSensorMaxValue(AppConstants.SensorType sensorType)
        {
            switch (sensorType)
            {
                case AppConstants.SensorType.NotSupported:
                    return 1000;
                case AppConstants.SensorType.ColorTemperature:
                    return 8000;
                case AppConstants.SensorType.AirQuality:
                    return 2100;//Changes done for BAC-274
                case AppConstants.SensorType.Presence:
                    return 1000;
                case AppConstants.SensorType.Power:
                    return 60;
                case AppConstants.SensorType.LuxLevel:
                    return 65535;
                case AppConstants.SensorType.Temperature:
                    if (!AppCoreRepo.Instance.BACnetConfigurationData.IsTempRequiredInFahrenheit)
                        return 120;
                    else
                        return 248;
                case AppConstants.SensorType.Humidity:
                    return 100;
                default:
                    Logger.Instance.Log(LogLevel.DEBUG, "ZoneProcessor.GetSensorMaxValue:Sensor Maximum value not found");
                    return 0;

            }
        }

        /// <summary>
        /// Gets the sensor high limit.
        /// </summary>
        /// <param name="sensorType">Type of the sensor.</param>
        /// <returns></returns>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>21-06-201703:23 PM</TimeStamp>
        public static Single GetSensorHighLimit(AppConstants.SensorType sensorType)
        {
            switch (sensorType)
            {
                case AppConstants.SensorType.NotSupported:
                    return 0;
                case AppConstants.SensorType.ColorTemperature:
                    return 6000;
                case AppConstants.SensorType.Power:
                    return 52;
                case AppConstants.SensorType.LuxLevel:
                    return 5000;
                case AppConstants.SensorType.Humidity:
                    return 60;
                case AppConstants.SensorType.AirQuality:
                    return 2100;//Changes done for BAC-274
                case AppConstants.SensorType.Temperature:
                    if (!AppCoreRepo.Instance.BACnetConfigurationData.IsTempRequiredInFahrenheit)
                        return 28;
                    else
                        return 82.4f;
                default:
                    Logger.Instance.Log(LogLevel.DEBUG, "SensorProcessor.GetSensorHighLimit:Sensor High Limit value not found");
                    return 0;
            }
        }

        /// <summary>
        /// Gets the sensor low limit.
        /// </summary>
        /// <param name="sensorType">Type of the sensor.</param>
        /// <returns></returns>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>21-06-201703:23 PM</TimeStamp>
        public static Single GetSensorLowLimit(AppConstants.SensorType sensorType)
        {
            switch (sensorType)
            {
                case AppConstants.SensorType.ColorTemperature:
                case AppConstants.SensorType.LuxLevel:
                case AppConstants.SensorType.Power:
                case AppConstants.SensorType.NotSupported:
                    return 0;
                case AppConstants.SensorType.Humidity:
                    return 40;
                case AppConstants.SensorType.AirQuality:
                    return 400;
                case AppConstants.SensorType.Temperature:
                    if (!AppCoreRepo.Instance.BACnetConfigurationData.IsTempRequiredInFahrenheit)
                        return 18;
                    else
                        return 64.4f;
                default:
                    Logger.Instance.Log(LogLevel.DEBUG, "SensorProcessor.GetSensorLowLimit :Sensor Low Limit value not found");
                    return 0;
            }
        }


        /// <summary>
        /// Gets the sensor deadband.
        /// </summary>
        /// <param name="sensorType">Type of the sensor.</param>
        /// <returns></returns>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>21-06-201703:24 PM</TimeStamp>
        public static Single GetSensorDeadband(AppConstants.SensorType sensorType)
        {
            switch (sensorType)
            {
                case AppConstants.SensorType.Humidity:
                    return 5;
                case AppConstants.SensorType.Temperature:
                case AppConstants.SensorType.ColorTemperature:
                case AppConstants.SensorType.LuxLevel:
                case AppConstants.SensorType.Power:
                case AppConstants.SensorType.AirQuality:
                    return 0;
                default:
                    Logger.Instance.Log(LogLevel.DEBUG, "SensorProcessor.GetSensorDeadband :Sensor Deadband value not found");
                    return 0;
            }
        }

        /// <summary>
        /// Gets the cov increment.
        /// </summary>
        /// <param name="sensorType">Type of the sensor.</param>
        /// <returns></returns>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>21-06-201703:24 PM</TimeStamp>
        public static Single GetCOVIncrement(AppConstants.SensorType sensorType)
        {
            switch (sensorType)
            {
                case AppConstants.SensorType.Humidity:
                    return 0.5f;
                case AppConstants.SensorType.Temperature:
                case AppConstants.SensorType.Power:
                    return 0.1f;
                case AppConstants.SensorType.ColorTemperature:
                case AppConstants.SensorType.LuxLevel:
                case AppConstants.SensorType.AirQuality:
                    return 5;
                default:
                    Logger.Instance.Log(LogLevel.DEBUG, "SensorProcessor.GetCOVIncrement :Sensor COV Increment value not found");
                    return 0;
            }
        }

        internal static float ConvertCelsiusToFahrenheit(float celsius)
        {
            float fahrenheit = ((celsius * 9 / 5) + 32);
            return fahrenheit;
        }
    }
}
