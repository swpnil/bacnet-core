﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              ObjectMappingHelperForTreeStructure
///   Description:        <Description>
///   Author:             Rohit Galgali              
///   Date:               07/28/17
#endregion

using Molex.BACnet.Gateway.ServiceCore.Models.Entity;
using Molex.BACnet.Gateway.ServiceCore.Models.TreeStructuredMappingData;
using Molex.BACnet.Gateway.Utility.Helper;
using System.Collections.Generic;
using System.Linq;

namespace Molex.BACnet.Gateway.ServiceCore.Helpers
{
    /// <summary>
    /// ObjectMappingHelper is used to map Entity objects for Object Mapping
    /// </summary>
    /// <CreatedBy>Rohit Galgali</CreatedBy>
    /// <TimeStamp>6/9/20174:01 PM</TimeStamp>
    internal class ObjectMappingHelperForTreeStructure
    {


        /// <summary>
        /// Fills the data model structure.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="models">The models.</param>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>3/2/201810:31 AM</TimeStamp>
        internal static void FillDataModelStructure(EntityBaseModel model, List<EntityBaseModel> models)
        {

            foreach (EntityBaseModel entityBaseModel in model.Childs)
            {
                switch (entityBaseModel.EntityType)
                {
                    case Constants.AppConstants.EntityTypes.Building:
                        BuildingEntityModel buildingEntityModel = entityBaseModel as BuildingEntityModel;
                        models.Add(buildingEntityModel.Parent);
                        FillDataModelStructure(entityBaseModel, models);
                        break;
                    case Constants.AppConstants.EntityTypes.Zone:
                        models.Add(entityBaseModel);
                        FillDataModelStructure(entityBaseModel, models);
                        break;
                    default:
                        FillDataModelStructure(entityBaseModel, models);
                        break;
                }

            }
        }



        /// <summary>
        /// Writes the mapped data to file.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="cachedObjectDetails">The cached object details.</param>
        /// <CreatedBy>Rohit galgali</CreatedBy>
        /// <TimeStamp>6/14/20175:48 PM</TimeStamp>
        internal static void WriteMappedDataToFile(string fileName, Dictionary<string, ControllerEntity> cachedObjectDetails)
        {
            JSONReaderWriterHelper.WriteToJsonFile(fileName, cachedObjectDetails);
        }

        #region Private

        /// <summary>
        /// Maps the entities.
        /// </summary>
        /// <param name="cachedObjectDetails">The cached object details.</param>
        /// <param name="model">The model.</param>
        /// <CreatedBy>Rohit Galgali</CreatedBy>
        /// <TimeStamp>7/18/201712:47 PM</TimeStamp>
        private static void MapControllers(Dictionary<string, ControllerEntity> cachedObjectDetails, EntityBaseModel model)
        {
            cachedObjectDetails.Add(model.MappingKey, GetControllerEntity(model));//Mapping Controller data to file.

            foreach (var entityModel in model.Childs)
            {
                MapBuildings(cachedObjectDetails.Where(x => x.Key == model.MappingKey).FirstOrDefault().Value.Buildings, entityModel);
            }
        }

        /// <summary>
        /// Maps the buildings.
        /// </summary>
        /// <param name="buildingObjectDetails">The building object details.</param>
        /// <param name="model">The model.</param>
        /// <CreatedBy>Rohit Galgali</CreatedBy>
        /// <TimeStamp>7/28/201712:09 PM</TimeStamp>
        private static void MapBuildings(Dictionary<string, BuildingEntity> buildingObjectDetails, EntityBaseModel model)
        {
            buildingObjectDetails.Add(model.MappingKey, GetBuildingEntity(model));
            foreach (var entityModel in model.Childs)
            {
                MapFloors(buildingObjectDetails.Where(x => x.Key == model.MappingKey).FirstOrDefault().Value.Floors, entityModel);
            }
        }

        /// <summary>
        /// Maps the floors.
        /// </summary>
        /// <param name="floorObjectDetails">The floor object details.</param>
        /// <param name="model">The model.</param>
        /// <CreatedBy>Rohit Galgali</CreatedBy>
        /// <TimeStamp>7/28/201712:08 PM</TimeStamp>
        private static void MapFloors(Dictionary<string, FloorEntity> floorObjectDetails, EntityBaseModel model)
        {
            floorObjectDetails.Add(model.MappingKey, GetFloorEntity(model));
            foreach (var entityModel in model.Childs)
            {
                MapZones(floorObjectDetails.Where(x => x.Key == model.MappingKey).FirstOrDefault().Value.Spaces, entityModel);
            }
        }

        /// <summary>
        /// Maps the zones.
        /// </summary>
        /// <param name="zoneObjectDetails">The zone object details.</param>
        /// <param name="model">The model.</param>
        /// <CreatedBy>Rohit Galgali</CreatedBy>
        /// <TimeStamp>7/28/201712:08 PM</TimeStamp>
        private static void MapZones(Dictionary<string, SpacesEntity> zoneObjectDetails, EntityBaseModel model)
        {
            zoneObjectDetails.Add(model.MappingKey, GetZoneEntity(model));
            ZoneEntityModel zoneModel = (ZoneEntityModel)model;
            foreach (var fixtures in zoneModel.Fixtures)
            {
                MapEndPoints(zoneObjectDetails.Where(x => x.Key == model.MappingKey).FirstOrDefault().Value.Endpoints, fixtures);
            }

            foreach (var sensors in zoneModel.Sensors)
            {
                MapEndPoints(zoneObjectDetails.Where(x => x.Key == model.MappingKey).FirstOrDefault().Value.Endpoints, sensors);
            }

        }

        /// <summary>
        /// Mapfixtureses the specified cached object details.
        /// </summary>
        /// <param name="cachedObjectDetails">The cached object details.</param>
        /// <param name="entityModel">The entity model.</param>
        /// <CreatedBy>Rohit Galgali</CreatedBy>
        /// <TimeStamp>7/18/201712:47 PM</TimeStamp>
        private static void MapEndPoints(Dictionary<string, EndPointsEntity> cachedObjectDetails, EntityBaseModel entityModel)
        {
            cachedObjectDetails.Add(entityModel.MappingKey, GetEndPointsEntity(entityModel));
        }

        /// <summary>
        /// Gets the entity object.
        /// </summary>
        /// <param name="entityBaseModel">The entity base model.</param>
        /// <returns></returns>
        /// <CreatedBy>Rohit Galgali</CreatedBy>
        /// <TimeStamp>6/8/20171:43 PM</TimeStamp>
        private static ControllerEntity GetControllerEntity(EntityBaseModel entityBaseModel)
        {
            ControllerEntity obj = new ControllerEntity();

            obj.DeviceId = EntityHelper.GetDeviceId(entityBaseModel).Data;
            foreach (var item in entityBaseModel.BACnetData.ObjectDetails)
            {
                obj.BACnetEntityMap.Add(item.Key.ToString(), GetObjectDetails(item));
            }
            return obj;
        }

        /// <summary>
        /// Gets the entity object.
        /// </summary>
        /// <param name="entityBaseModel">The entity base model.</param>
        /// <returns></returns>
        /// <CreatedBy>Rohit Galgali</CreatedBy>
        /// <TimeStamp>6/8/20171:43 PM</TimeStamp>
        private static BuildingEntity GetBuildingEntity(EntityBaseModel entityBaseModel)
        {
            BuildingEntity obj = new BuildingEntity();

            obj.DeviceId = EntityHelper.GetDeviceId(entityBaseModel).Data;
            foreach (var item in entityBaseModel.BACnetData.ObjectDetails)
            {
                obj.BACnetEntityMap.Add(item.Key.ToString(), GetObjectDetails(item));
            }
            return obj;
        }

        /// <summary>
        /// Gets the entity object.
        /// </summary>
        /// <param name="entityBaseModel">The entity base model.</param>
        /// <returns></returns>
        /// <CreatedBy>Rohit Galgali</CreatedBy>
        /// <TimeStamp>6/8/20171:43 PM</TimeStamp>
        private static FloorEntity GetFloorEntity(EntityBaseModel entityBaseModel)
        {
            FloorEntity obj = new FloorEntity();

            obj.DeviceId = EntityHelper.GetDeviceId(entityBaseModel).Data;
            foreach (var item in entityBaseModel.BACnetData.ObjectDetails)
            {
                obj.BACnetEntityMap.Add(item.Key.ToString(), GetObjectDetails(item));
            }
            return obj;
        }

        /// <summary>
        /// Gets the entity object.
        /// </summary>
        /// <param name="entityBaseModel">The entity base model.</param>
        /// <returns></returns>
        /// <CreatedBy>Rohit Galgali</CreatedBy>
        /// <TimeStamp>6/8/20171:43 PM</TimeStamp>
        private static SpacesEntity GetZoneEntity(EntityBaseModel entityBaseModel)
        {
            SpacesEntity obj = new SpacesEntity();

            obj.DeviceId = EntityHelper.GetDeviceId(entityBaseModel).Data;
            foreach (var item in entityBaseModel.BACnetData.ObjectDetails)
            {
                obj.BACnetEntityMap.Add(item.Key.ToString(), GetObjectDetails(item));
            }
            return obj;
        }

        /// <summary>
        /// Gets the entity object.
        /// </summary>
        /// <param name="entityBaseModel">The entity base model.</param>
        /// <returns></returns>
        /// <CreatedBy>Rohit Galgali</CreatedBy>
        /// <TimeStamp>6/8/20171:43 PM</TimeStamp>
        private static EndPointsEntity GetEndPointsEntity(EntityBaseModel entityBaseModel)
        {
            EndPointsEntity obj = new EndPointsEntity();

            obj.DeviceId = EntityHelper.GetDeviceId(entityBaseModel).Data;
            foreach (var item in entityBaseModel.BACnetData.ObjectDetails)
            {
                obj.BACnetEntityMap.Add(item.Key.ToString(), GetObjectDetails(item));
            }
            return obj;
        }

        /// <summary>
        /// Gets the object details.
        /// </summary>
        /// <param name="bACnetPair">The item.</param>
        /// <returns></returns>
        /// <CreatedBy>Rohit Galgali</CreatedBy>
        /// <TimeStamp>6/9/201711:01 AM</TimeStamp>
        private static TreeStructuredBACnetEntity GetObjectDetails(KeyValuePair<string, Models.BACnet.BACnetObjectDetails> bACnetPair)
        {
            TreeStructuredBACnetEntity obj = new TreeStructuredBACnetEntity();
            obj.ObjectID = bACnetPair.Value.ObjectID;
            obj.ObjectName = bACnetPair.Value.ObjectName;
            obj.ObjectDescription = bACnetPair.Value.ObjectDescription;
            return obj;
        }
        #endregion
    }
}