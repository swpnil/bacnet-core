﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              MonitoredDataStackUpdateHelper
///   Description:        This class is used as helper for monitoring BACnet Data stack
///   Author:             Prasad Joshi                 
///   Date:               06/05/17
#endregion

using Molex.BACnet.Gateway.Log;
using Molex.BACnet.Gateway.ServiceCore.AppResources;
using Molex.BACnet.Gateway.ServiceCore.Constants;
using Molex.BACnet.Gateway.ServiceCore.Managers;
using Molex.BACnet.Gateway.ServiceCore.Models;
using Molex.BACnet.Gateway.ServiceCore.Models.Entity;
using Molex.BACnet.Gateway.ServiceCore.Models.MolexAPIModels;
using Molex.BACnet.Gateway.ServiceCore.Repository;
using Molex.StackDataObjects.Constants;
using System;
using System.Linq;
using System.Collections.Generic;

namespace Molex.BACnet.Gateway.ServiceCore.Helpers
{
    /// <summary>
    /// This class is used as helper for monitoring BACnet Data stack
    /// </summary>
    /// <CreatedBy>Prasad Joshi</CreatedBy><TimeStamp>06-07-201701:11 PM</TimeStamp>
    internal static class MonitoredDataStackUpdateHelper
    {
        /// <summary>
        /// Updates the brightness.
        /// </summary>
        /// <param name="brightness">The brightness.</param>
        /// <param name="zoneLookupModel">The zone lookup model.</param>
        /// <CreatedBy>prasad.joshi</CreatedBy>
        /// <TimeStamp>6/7/20176:07 PM</TimeStamp>
        internal static void UpdateBrightness(double brightness, EntityBaseModel zoneLookupModel)
        {
            string key = EntityHelper.GetObjectDetailsKey(BacnetObjectType.ObjectAnalogValue, AppConstants.LIGHTING_BRIGHTENESS_OBJECT);
            if (!zoneLookupModel.BACnetData.ObjectDetails.ContainsKey(key))
                return;
            //If WP is inprocess for object then skip monitoring (to fix BAC-18 issue)
            if (AppCoreRepo.Instance.BACnetObjectsInWPProcess.Contains(zoneLookupModel.BACnetData.ObjectDetails[key].ObjectID))
            {
                return;
            }

            ResultModel<int> deviceId = EntityHelper.GetDeviceId(zoneLookupModel);

            if (!deviceId.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.ERROR, "Property update: Property update for Analog value failed, Device Id not found.");
                return;
            }

            if (zoneLookupModel.BACnetData.ObjectDetails[key].IsOutOfService)
            {
                Logger.Instance.Log(LogLevel.DEBUG, "MonitoredDataStackUpdateHelper.UpdateBrightness : OutofService is true skipping update for DeviceID ");
                return;
            }

            if (zoneLookupModel.BACnetData.ObjectDetails[key].PresentValue.Equals(brightness))
            {
                return;
            }

            bool result = StackManager.Instance.WriteProperty((uint)deviceId.Data,
                StackDataObjects.Constants.BacnetObjectType.ObjectAnalogValue, (uint)zoneLookupModel.BACnetData.ObjectDetails[key].ObjectID, brightness,
                StackDataObjects.Constants.BacnetPropertyID.PropPresentValue, StackDataObjects.Constants.BacnetDataType.BacnetDTReal);

            if (!result)
            {
                Logger.Instance.Log(LogLevel.ERROR, "Property update: Property update for Analog value failed");
                return;
            }

            UpdatePriorityBrightness(ref brightness, zoneLookupModel, key, deviceId.Data);

            zoneLookupModel.BACnetData.ObjectDetails[key].PresentValue = brightness;

            List<FixtureEntityModel> fixtures = ((ZoneEntityModel)zoneLookupModel).Fixtures;
            UpdateZoneFixturePresentValue(deviceId.Data, fixtures, key, brightness);
        }

        /// <summary>
        /// Updates the BioDynamic Control.
        /// </summary>
        /// <param name="brightness">The brightness.</param>
        /// <param name="zoneLookupModel">The zone lookup model.</param>
        /// <CreatedBy>prasad.joshi</CreatedBy>
        /// <TimeStamp>6/7/20176:07 PM</TimeStamp>
        internal static void UpdateBioDynamicControl(double brightness, EntityBaseModel zoneLookupModel)
        {
            string key = EntityHelper.GetObjectDetailsKey(BacnetObjectType.ObjectAnalogValue, AppResource.BioDynamicControlName);
            if (!zoneLookupModel.BACnetData.ObjectDetails.ContainsKey(key))
                return;
            //If WP is inprocess for object then skip monitoring (to fix BAC-18 issue)
            if (AppCoreRepo.Instance.BACnetObjectsInWPProcess.Contains(zoneLookupModel.BACnetData.ObjectDetails[key].ObjectID))
            {
                return;
            }

            ResultModel<int> deviceId = EntityHelper.GetDeviceId(zoneLookupModel);

            if (!deviceId.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.ERROR, "Property update: Property update for Analog value failed, Device Id not found.");
                return;
            }

            if (zoneLookupModel.BACnetData.ObjectDetails[key].IsOutOfService)
            {
                Logger.Instance.Log(LogLevel.DEBUG, "MonitoredDataStackUpdateHelper.UpdateBrightness : OutofService is true skipping update for DeviceID ");
                return;
            }

            if (zoneLookupModel.BACnetData.ObjectDetails[key].PresentValue.Equals(brightness))
            {
                return;
            }

            bool result = StackManager.Instance.WriteProperty((uint)deviceId.Data,
                StackDataObjects.Constants.BacnetObjectType.ObjectAnalogValue, (uint)zoneLookupModel.BACnetData.ObjectDetails[key].ObjectID, brightness,
                StackDataObjects.Constants.BacnetPropertyID.PropPresentValue, StackDataObjects.Constants.BacnetDataType.BacnetDTReal);

            if (!result)
            {
                Logger.Instance.Log(LogLevel.ERROR, "Property update: Property update for Analog value failed");
                return;
            }
        }

        /// <summary>
        /// Updates the Saturation Control.
        /// </summary>
        /// <param name="brightness">The brightness.</param>
        /// <param name="zoneLookupModel">The zone lookup model.</param>
        /// <CreatedBy>prasad.joshi</CreatedBy>
        /// <TimeStamp>6/7/20176:07 PM</TimeStamp>
        internal static void UpdateSaturationControl(double brightness, EntityBaseModel zoneLookupModel)
        {
            string key = EntityHelper.GetObjectDetailsKey(BacnetObjectType.ObjectAnalogValue, AppResource.SaturationControlName);
            if (!zoneLookupModel.BACnetData.ObjectDetails.ContainsKey(key))
                return;
            //If WP is inprocess for object then skip monitoring (to fix BAC-18 issue)
            if (AppCoreRepo.Instance.BACnetObjectsInWPProcess.Contains(zoneLookupModel.BACnetData.ObjectDetails[key].ObjectID))
            {
                return;
            }

            ResultModel<int> deviceId = EntityHelper.GetDeviceId(zoneLookupModel);

            if (!deviceId.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.ERROR, "Property update: Property update for Analog value failed, Device Id not found.");
                return;
            }

            if (zoneLookupModel.BACnetData.ObjectDetails[key].IsOutOfService)
            {
                Logger.Instance.Log(LogLevel.DEBUG, "MonitoredDataStackUpdateHelper.UpdateBrightness : OutofService is true skipping update for DeviceID ");
                return;
            }

            if (zoneLookupModel.BACnetData.ObjectDetails[key].PresentValue.Equals(brightness))
            {
                return;
            }

            bool result = StackManager.Instance.WriteProperty((uint)deviceId.Data,
                StackDataObjects.Constants.BacnetObjectType.ObjectAnalogValue, (uint)zoneLookupModel.BACnetData.ObjectDetails[key].ObjectID, brightness,
                StackDataObjects.Constants.BacnetPropertyID.PropPresentValue, StackDataObjects.Constants.BacnetDataType.BacnetDTReal);

            if (!result)
            {
                Logger.Instance.Log(LogLevel.ERROR, "Property update: Property update for Analog value failed");
                return;
            }
        }

        /// <summary>
        /// Updates the Blind Level for Zone
        /// </summary>
        /// <param name="brightness">The brightness.</param>
        /// <param name="zoneLookupModel">The zone lookup model.</param>
        /// <CreatedBy>Swpnil.Vyas</CreatedBy>
        /// <TimeStamp>12-09-2018</TimeStamp>
        internal static void UpdateBlindLevel(BlindZoneLevelModel blindValue, EntityBaseModel zoneLookupModel)
        {
            UpdateZoneBlindDirectionLevel(blindValue.all.level, zoneLookupModel, AppResource.AnalogValueBlindObjectName);
            //UpdateZoneBlindDirectionLevel(blindValue.n.level, zoneLookupModel, AppResource.AnalogValueBlindObjectName + "_" + "N");
            //UpdateZoneBlindDirectionLevel(blindValue.c.level, zoneLookupModel, AppResource.AnalogValueBlindObjectName + "_" + "C");
            //UpdateZoneBlindDirectionLevel(blindValue.e.level, zoneLookupModel, AppResource.AnalogValueBlindObjectName + "_" + "E");
            //UpdateZoneBlindDirectionLevel(blindValue.w.level, zoneLookupModel, AppResource.AnalogValueBlindObjectName + "_" + "W");
            //UpdateZoneBlindDirectionLevel(blindValue.s.level, zoneLookupModel, AppResource.AnalogValueBlindObjectName + "_" + "S");
            //UpdateZoneBlindDirectionLevel(blindValue.ne.level, zoneLookupModel, AppResource.AnalogValueBlindObjectName + "_" + "NE");
            //UpdateZoneBlindDirectionLevel(blindValue.nw.level, zoneLookupModel, AppResource.AnalogValueBlindObjectName + "_" + "NW");
            //UpdateZoneBlindDirectionLevel(blindValue.se.level, zoneLookupModel, AppResource.AnalogValueBlindObjectName + "_" + "SE");
            //UpdateZoneBlindDirectionLevel(blindValue.sw.level, zoneLookupModel, AppResource.AnalogValueBlindObjectName + "_" + "SW");
            UpdateZoneBlindDirectionLevel(blindValue.n.level, zoneLookupModel, AppResource.AnalogValueBlindObjectName + "_" + "North");
            UpdateZoneBlindDirectionLevel(blindValue.c.level, zoneLookupModel, AppResource.AnalogValueBlindObjectName + "_" + "Center");
            UpdateZoneBlindDirectionLevel(blindValue.e.level, zoneLookupModel, AppResource.AnalogValueBlindObjectName + "_" + "East");
            UpdateZoneBlindDirectionLevel(blindValue.w.level, zoneLookupModel, AppResource.AnalogValueBlindObjectName + "_" + "West");
            UpdateZoneBlindDirectionLevel(blindValue.s.level, zoneLookupModel, AppResource.AnalogValueBlindObjectName + "_" + "South");
            UpdateZoneBlindDirectionLevel(blindValue.ne.level, zoneLookupModel, AppResource.AnalogValueBlindObjectName + "_" + "North-East");
            UpdateZoneBlindDirectionLevel(blindValue.nw.level, zoneLookupModel, AppResource.AnalogValueBlindObjectName + "_" + "North-West");
            UpdateZoneBlindDirectionLevel(blindValue.se.level, zoneLookupModel, AppResource.AnalogValueBlindObjectName + "_" + "South-East");
            UpdateZoneBlindDirectionLevel(blindValue.sw.level, zoneLookupModel, AppResource.AnalogValueBlindObjectName + "_" + "South-West");
        }

        private static void UpdateZoneBlindDirectionLevel(int blindLevel, EntityBaseModel zoneLookupModel, string name)
        {
            string key = EntityHelper.GetObjectDetailsKey(BacnetObjectType.ObjectAnalogValue, name);
            if (!zoneLookupModel.BACnetData.ObjectDetails.ContainsKey(key))
                return;
            //If WP is inprocess for object then skip monitoring (to fix BAC-18 issue)
            if (AppCoreRepo.Instance.BACnetObjectsInWPProcess.Contains(zoneLookupModel.BACnetData.ObjectDetails[key].ObjectID))
            {
                return;
            }

            ResultModel<int> deviceId = EntityHelper.GetDeviceId(zoneLookupModel);

            if (!deviceId.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.ERROR, "Property update: Property update for Analog value failed, Device Id not found.");
                return;
            }

            if (zoneLookupModel.BACnetData.ObjectDetails[key].IsOutOfService)
            {
                Logger.Instance.Log(LogLevel.DEBUG, "MonitoredDataStackUpdateHelper.UpdateBlindLevel : OutofService is true skipping update for DeviceID ");
                return;
            }

            if (zoneLookupModel.BACnetData.ObjectDetails[key].PresentValue.Equals(blindLevel))
            {
                return;
            }


            bool result = StackManager.Instance.WriteProperty((uint)deviceId.Data,
                StackDataObjects.Constants.BacnetObjectType.ObjectAnalogValue, (uint)zoneLookupModel.BACnetData.ObjectDetails[key].ObjectID, blindLevel,
                StackDataObjects.Constants.BacnetPropertyID.PropPresentValue, StackDataObjects.Constants.BacnetDataType.BacnetDTReal);

            if (!result)
            {
                Logger.Instance.Log(LogLevel.ERROR, "Property update: Property update for Analog value failed");
                return;
            }
            double brightness = blindLevel;
            UpdatePriorityBrightness(ref brightness, zoneLookupModel, key, deviceId.Data);

            zoneLookupModel.BACnetData.ObjectDetails[key].PresentValue = brightness;

            List<FixtureEntityModel> fixtures = ((ZoneEntityModel)zoneLookupModel).Fixtures;
            UpdateZoneFixturePresentValue(deviceId.Data, fixtures, key, brightness);
        }

        /// <summary>
        /// Updates the light scene.
        /// </summary>
        /// <param name="lightScene">The light Scene.</param>
        /// <param name="zoneLookupModel">The zone lookup model.</param>
        /// <CreatedBy>prasad.joshi</CreatedBy><TimeStamp>6/7/20176:07 PM</TimeStamp>
        internal static void UpdateLightScene(string lightScene, EntityBaseModel zoneLookupModel, bool isValueApplied = false)
        {
            string key = EntityHelper.GetObjectDetailsKey(BacnetObjectType.ObjectMultiStateValue, AppConstants.LIGHTING_LIGHTSCENE_OBJECT);

            if (!zoneLookupModel.BACnetData.ObjectDetails.ContainsKey(key))
                return;
            //If WP is inprocess for object then skip monitoring (to fix BAC-18 issue)
            if (AppCoreRepo.Instance.BACnetObjectsInWPProcess.Contains(zoneLookupModel.BACnetData.ObjectDetails[key].ObjectID))
            {
                return;
            }

            ResultModel<int> deviceId = EntityHelper.GetDeviceId(zoneLookupModel);

            if (!deviceId.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.ERROR, "Property update: Property update for Multi state value failed. Device id not found");
                return;
            }

            if (zoneLookupModel.BACnetData.ObjectDetails[key].IsOutOfService)
            {
                Logger.Instance.Log(LogLevel.DEBUG, "MonitoredDataStackUpdateHelper.UpdateLightScene : OutofService is true skipping update for DeviceID ");
                return;
            }

            if (zoneLookupModel.BACnetData.ObjectDetails[key].PresentValue.Equals(lightScene) && !isValueApplied)
            {
                return;
            }
            int index = 0;
            switch (((ZoneEntityModel)zoneLookupModel).ZoneType)
            {
                case AppConstants.ZoneTypes.BeaconSpace:
                    {
                        index = Array.IndexOf(((ZoneEntityModel)zoneLookupModel).LightScene.ToArray(), lightScene);
                        if (index == -1)
                        {
                            Logger.Instance.Log(LogLevel.ERROR, "MonitoredDataStackUpdateHelper.UpdateLightScene: No Matching LightScene found.. Name: " + lightScene);
                            return;
                        }
                    }
                    break;
                case AppConstants.ZoneTypes.UserSpace:
                case AppConstants.ZoneTypes.GeneralSpace:
                default:
                    {
                        index = Array.IndexOf(((ZoneEntityModel)zoneLookupModel).LightScene.ToArray(), lightScene);
                        if (index == -1)
                        {
                            Logger.Instance.Log(LogLevel.ERROR, "MonitoredDataStackUpdateHelper.UpdateLightScene: No Matching LightScene found.. Name: " + lightScene);
                            return;
                        }
                    }
                    break;
            }


            bool result = StackManager.Instance.WriteProperty((uint)deviceId.Data, StackDataObjects.Constants.BacnetObjectType.ObjectMultiStateValue, (uint)zoneLookupModel.BACnetData.ObjectDetails[key].ObjectID,
                index + 1, StackDataObjects.Constants.BacnetPropertyID.PropPresentValue, StackDataObjects.Constants.BacnetDataType.BacnetDTReal);

            if (!result)
            {
                Logger.Instance.Log(LogLevel.ERROR, "MonitoredDataStackUpdateHelper.UpdateLightScene: Property update: Property update for Multi state value failed for " + lightScene);
                return;
            }
            UpdateBeaconPalettes(zoneLookupModel as ZoneEntityModel, lightScene);
            UpdatedPriorityLightScene(ref lightScene, zoneLookupModel, key, deviceId, index);
            zoneLookupModel.BACnetData.ObjectDetails[key].PresentValue = lightScene;
        }

        /// <summary>
        /// Here this method will set the Beacon Palettes
        /// </summary>
        /// <param name="zoneLookupModel"></param>
        /// <param name="lightSceneSelected"></param>
        private static void UpdateBeaconPalettes(ZoneEntityModel zoneLookupModel, string lightSceneSelected)
        {
            try
            {
                if (string.IsNullOrEmpty(lightSceneSelected))
                    return;
                if (zoneLookupModel == null)
                    return;
                if (zoneLookupModel.ZoneType != AppConstants.ZoneTypes.BeaconSpace)
                    return;
                List<string> palettesList = GetPalettesList(lightSceneSelected);
                if (palettesList == null || palettesList.Count == 0)
                    Logger.Instance.Log(LogLevel.ERROR, "MonitoredDataStackUpdateHelper.UpdateBeaconPalettes Empty Palettes found for light scene " + lightSceneSelected);
                uint deviceID = (uint)zoneLookupModel.BACnetData.DeviceID;
                if (!zoneLookupModel.BACnetData.ObjectDetails.ContainsKey(BacnetObjectType.ObjectMultiStateValue.ToString() + "_BeaconPalettes"))
                    return;
                uint objectID = zoneLookupModel.BACnetData.ObjectDetails[BacnetObjectType.ObjectMultiStateValue.ToString() + "_BeaconPalettes"].ObjectID;
                bool isSuccess = StackManager.Instance.WriteProperty(deviceID, BacnetObjectType.ObjectMultiStateValue, objectID, palettesList.ToArray(), BacnetPropertyID.PropStateText, BacnetDataType.BacnetDTCharStringArray);
                if (!isSuccess)
                    Logger.Instance.Log(LogLevel.ERROR, "MonitoredDataStackUpdateHelper.UpdateZoneStateText StackManager.Instance.WriteProperty failed");
                else
                    zoneLookupModel.BeaconPalettes = palettesList;
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "MonitoredDataStackUpdateHelper.UpdateBeaconPalettes " + ex.Message.ToString());
            }
        }

        /// <summary>
        /// This method will return the applicable list of palettes based on light scene
        /// </summary>
        /// <param name="lightScene"></param>
        /// <returns></returns>
        private static List<string> GetPalettesList(string lightScene)
        {
            try
            {
                List<string> result = new List<string>();
                switch (lightScene.ToUpperInvariant().Trim())
                {
                    case "ONECOLOUR":
                        result.Add("default");
                        result.Add("fire");
                        result.Add("ok");
                        break;
                    case "CYCLE":
                    case "CHASE":
                        result.Add("default");
                        result.Add("rainbow");
                        result.Add("cool");
                        result.Add("hot");
                        break;
                    case "TWOCOLOUR":
                    case "SLOWFLASH":
                    case "FASTFLASH":
                    case "SLOWPULSE":
                    case "FASTPULSE":
                        result.Add("default");
                        result.Add("two");
                        result.Add("cool");
                        result.Add("hot");
                        break;
                    case "THREECOLOUR":
                        result.Add("default");
                        result.Add("three");
                        result.Add("cool");
                        result.Add("hot");
                        break;
                    default:
                        break;
                }
                return result;
            }
            catch
            {
                return new List<string>();
            }
        } 

        /// <summary>
        /// Updates the sensor present value.
        /// </summary>
        /// <param name="sensorPresentData">The sensor present data.</param>
        /// <param name="sensorEntityModel">The sensor entity model.</param>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>6/8/20174:36 AM</TimeStamp>
        internal static void UpdateSensorPresentValue(float presentValue, SensorEntityModel sensorEntityModel)
        {
            BacnetObjectType bacnetObjectType = EntityHelper.GetBACnetObjectTypeBySensorType(sensorEntityModel.SensorType);

            string key = EntityHelper.GetObjectDetailsKey(bacnetObjectType, AppConstants.LIGHTING_SENSOR_OBJECT);

            if (!sensorEntityModel.BACnetData.ObjectDetails.ContainsKey(key))
            {
                //When MapIndividual sensor is false
                return;
            }

            ResultModel<int> deviceId = EntityHelper.GetDeviceId(sensorEntityModel);

            if (!deviceId.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.ERROR, "Property update: Property update for Analog Input - Failed, Device ID not found");
                return;
            }

            if (sensorEntityModel.BACnetData.ObjectDetails[key].IsOutOfService)
            {
                Logger.Instance.Log(LogLevel.DEBUG, "MonitoredDataStackUpdateHelper.UpdateSensorPresentValue : OutofService is true skipping update for Sensor PV");
                return;
            }
            switch (sensorEntityModel.SensorType)
            {
                case AppConstants.SensorType.Temperature:
                    if (AppCoreRepo.Instance.BACnetConfigurationData.IsTempRequiredInFahrenheit)
                    {
                        float temp = presentValue;
                        presentValue = ((temp * 9 / 5) + 32);
                    }
                    break;
            }
            if (!sensorEntityModel.BACnetData.ObjectDetails[key].PresentValue.Equals(presentValue))
            {
                bool result = StackManager.Instance.WriteProperty((uint)deviceId.Data,
                              bacnetObjectType, sensorEntityModel.BACnetData.ObjectDetails[key].ObjectID,
                              presentValue, StackDataObjects.Constants.BacnetPropertyID.PropPresentValue);

                if (!result)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "Property update: Property update for Analog Input - Failed");
                    return;
                }
                sensorEntityModel.BACnetData.ObjectDetails[key].PresentValue = presentValue;
            }
        }

        /// <summary>
        /// Updates the fixtures present value. Brightness is updated.
        /// </summary>
        /// <param name="deviceId">The device identifier.</param>
        /// <param name="fixtures">The fixtures.</param>
        /// <param name="key">The key.</param>
        /// <param name="brightness">The brightness.</param>
        /// <CreatedBy>Rohit Galgali</CreatedBy>
        /// <TimeStamp>6/21/20174:11 PM</TimeStamp>
        private static void UpdateZoneFixturePresentValue(int deviceId, IEnumerable<FixtureEntityModel> fixtures, string key, double brightness)
        {
            foreach (FixtureEntityModel fixture in fixtures)
            {
                if (!fixture.BACnetData.ObjectDetails.ContainsKey(key))
                {
                    //When MapIndividual fixture is false
                    continue;
                }
                bool result = StackManager.Instance.WriteProperty((uint)deviceId,
                        StackDataObjects.Constants.BacnetObjectType.ObjectAnalogValue, (uint)fixture.BACnetData.ObjectDetails[key].ObjectID, brightness,
                        StackDataObjects.Constants.BacnetPropertyID.PropPresentValue, StackDataObjects.Constants.BacnetDataType.BacnetDTReal);

                if (!result)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "Property update: Property update for Analog value failed at Fixture level");
                    return;
                }

                fixture.BACnetData.ObjectDetails[key].PresentValue = brightness;
            }
        }

        internal static void UpdateZoneStateText(ZoneEntityModel zoneLookupModel, MolexAPIPublishModel queueItem)
        {
            try
            {
                if (zoneLookupModel == null)
                    return;
                Dictionary<string, string> lightsceneObject = Utility.Helper.JSONReaderWriterHelper.ReadJsonFromString<Dictionary<string, string>>(queueItem.data["value"].ToString());
                if (lightsceneObject == null)
                    return;
                List<string> avaliableLightScenes = new List<string>();
                zoneLookupModel.LightScene.Clear();
                foreach (var item in lightsceneObject.Keys)
                {
                    avaliableLightScenes.Add(item);
                    zoneLookupModel.LightScene.Add(item);
                }
                uint deviceID = (uint)((Molex.BACnet.Gateway.ServiceCore.Models.Entity.EntityBaseModel)(zoneLookupModel)).BACnetData.DeviceID;
                if (!((Molex.BACnet.Gateway.ServiceCore.Models.Entity.EntityBaseModel)(zoneLookupModel)).BACnetData.ObjectDetails.ContainsKey(BacnetObjectType.ObjectMultiStateValue.ToString() + "_LightScene"))
                    return;
                uint objectID = ((Molex.BACnet.Gateway.ServiceCore.Models.Entity.EntityBaseModel)(zoneLookupModel)).BACnetData.ObjectDetails[BacnetObjectType.ObjectMultiStateValue.ToString() + "_LightScene"].ObjectID;
                bool isSuccess = StackManager.Instance.WriteProperty(deviceID, BacnetObjectType.ObjectMultiStateValue, objectID, avaliableLightScenes.ToArray(), BacnetPropertyID.PropStateText, BacnetDataType.BacnetDTCharStringArray);
                if (!isSuccess)
                    Logger.Instance.Log(LogLevel.ERROR, "MonitoredDataStackUpdateHelper.UpdateZoneStateText StackManager.Instance.WriteProperty failed");

                bool isPropRelinquishDefaultSuccess = StackManager.Instance.WriteProperty(deviceID, BacnetObjectType.ObjectMultiStateValue, objectID, 1, BacnetPropertyID.PropRelinquishDefault, BacnetDataType.BacnetDTUnsigned);
                if (!isPropRelinquishDefaultSuccess)
                    Logger.Instance.Log(LogLevel.ERROR, "MonitoredDataStackUpdateHelper.UpdateZoneStateText StackManager.Instance.WriteProperty failed for RelinquishDefault");

                //Now Store the All Light Scenes in Persistent Manager to get the updated details
                PersistenceStorageManager.Instance.Store(EntityHelper.GetControllerEntityModel(zoneLookupModel), AppCoreRepo.Instance.PersistentModelCache);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "MonitoredDataStackUpdateHelper.UpdateZoneStateText " + ex.Message.ToString());
            }
        }

        /// <summary>
        /// Updates the piv present value.
        /// </summary>
        /// <param name="zonePresentStateDataModel">The zone present state data model.</param>
        /// <param name="zoneLookupModel">The zone lookup model.</param>
        /// <param name="pivType">Type of the piv.</param>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>6/22/20173:24 PM</TimeStamp>
        internal static void UpdatePIVBeaconPalattes(BeaconPaletteModel paletteModel, EntityBaseModel zoneLookupModel)
        {
            string key = EntityHelper.GetObjectDetailsKey(BacnetObjectType.ObjectPositiveIntegerValue, AppResource.BeaconColor + " 1");
            if (!zoneLookupModel.BACnetData.ObjectDetails.ContainsKey(key))
                return;
            string pallete = string.Empty;
            if (paletteModel.PaletteModelList.Length > 0)
            {
                pallete = paletteModel.PaletteModelList[0].red.ToString("000") + paletteModel.PaletteModelList[0].green.ToString("000") + paletteModel.PaletteModelList[0].blue.ToString("000");
                UpdateIndividualPalettes(zoneLookupModel, key, Convert.ToInt64(pallete));
            }
            else
            {
                pallete = "000000000";
                UpdateIndividualPalettes(zoneLookupModel, key, Convert.ToInt64(pallete));
            }

            key = EntityHelper.GetObjectDetailsKey(BacnetObjectType.ObjectPositiveIntegerValue, AppResource.BeaconColor+" 2");
            if (!zoneLookupModel.BACnetData.ObjectDetails.ContainsKey(key))
                return;
            pallete = string.Empty;
            if (paletteModel.PaletteModelList.Length > 1)
            {
                pallete = paletteModel.PaletteModelList[1].red.ToString("000") + paletteModel.PaletteModelList[1].green.ToString("000") + paletteModel.PaletteModelList[1].blue.ToString("000");
                UpdateIndividualPalettes(zoneLookupModel, key, Convert.ToInt64(pallete));
            }
            else
            {
                pallete = "000000000";
                UpdateIndividualPalettes(zoneLookupModel, key, Convert.ToInt64(pallete));
            }

            key = EntityHelper.GetObjectDetailsKey(BacnetObjectType.ObjectPositiveIntegerValue, AppResource.BeaconColor + " 3");
            if (!zoneLookupModel.BACnetData.ObjectDetails.ContainsKey(key))
                return;
            pallete = string.Empty;
            if (paletteModel.PaletteModelList.Length > 2)
            {
                pallete = paletteModel.PaletteModelList[2].red.ToString("000") + paletteModel.PaletteModelList[2].green.ToString("000") + paletteModel.PaletteModelList[2].blue.ToString("000");
                UpdateIndividualPalettes(zoneLookupModel, key, Convert.ToInt64(pallete));
            }
            else
            {
                pallete = "000000000";
                UpdateIndividualPalettes(zoneLookupModel, key, Convert.ToInt64(pallete));
            }

            key = EntityHelper.GetObjectDetailsKey(BacnetObjectType.ObjectPositiveIntegerValue, AppResource.BeaconColor + " 4");
            if (!zoneLookupModel.BACnetData.ObjectDetails.ContainsKey(key))
                return;
            pallete = string.Empty;
            if (paletteModel.PaletteModelList.Length > 3)
            {
                pallete = paletteModel.PaletteModelList[3].red.ToString("000") + paletteModel.PaletteModelList[3].green.ToString("000") + paletteModel.PaletteModelList[3].blue.ToString("000");
                UpdateIndividualPalettes(zoneLookupModel, key, Convert.ToInt64(pallete));
            }
            else
            {
                pallete = "000000000";
                UpdateIndividualPalettes(zoneLookupModel, key, Convert.ToInt64(pallete));
            }

            key = EntityHelper.GetObjectDetailsKey(BacnetObjectType.ObjectPositiveIntegerValue, AppResource.BeaconColor + " 5");
            if (!zoneLookupModel.BACnetData.ObjectDetails.ContainsKey(key))
                return;
            pallete = string.Empty;
            if (paletteModel.PaletteModelList.Length > 4)
            {
                pallete = paletteModel.PaletteModelList[4].red.ToString("000") + paletteModel.PaletteModelList[4].green.ToString("000") + paletteModel.PaletteModelList[4].blue.ToString("000");
                UpdateIndividualPalettes(zoneLookupModel, key, Convert.ToInt64(pallete));
            }
            else
            {
                pallete = "000000000";
                UpdateIndividualPalettes(zoneLookupModel, key, Convert.ToInt64(pallete));
            }
        }

        /// <summary>
        /// This Method will update the Palettes for individual Palettes
        /// </summary>
        /// <param name="zoneLookupModel"></param>
        /// <param name="key"></param>
        /// <param name="presentvalue"></param>
        private static void UpdateIndividualPalettes(EntityBaseModel zoneLookupModel, string key , long presentvalue)
        {
            //If WP is inprocess for object then skip monitoring (to fix BAC-18 issue)
            if (AppCoreRepo.Instance.BACnetObjectsInWPProcess.Contains(zoneLookupModel.BACnetData.ObjectDetails[key].ObjectID))
                return;

            ResultModel<int> deviceId = EntityHelper.GetDeviceId(zoneLookupModel);
            if (!deviceId.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.ERROR, "Failed to get Device ID for UpdatePIVPresentValue for Entity Key" + zoneLookupModel.EntityKey);
                return;
            }

            if (zoneLookupModel.BACnetData.ObjectDetails[key].IsOutOfService)
            {
                Logger.Instance.Log(LogLevel.DEBUG, "MonitoredDataStackUpdateHelper.UpdatePIVPresentValue : OutofService is true skipping update for DeviceID ");
                return;
            }

            if (zoneLookupModel.BACnetData.ObjectDetails[key].PresentValue.Equals(presentvalue))
                return;

            bool result = StackManager.Instance.WriteProperty((uint)deviceId.Data,
                StackDataObjects.Constants.BacnetObjectType.ObjectPositiveIntegerValue, (uint)zoneLookupModel.BACnetData.ObjectDetails[key].ObjectID, presentvalue,
                StackDataObjects.Constants.BacnetPropertyID.PropPresentValue, StackDataObjects.Constants.BacnetDataType.BacnetDTUnsigned);

            if (!result)
            {
                Logger.Instance.Log(LogLevel.ERROR, "MonitoredDataStackUpdateHelper.UpdatePIVPresentValue : Property update for positive integer failed " + key);
                return;
            }
            zoneLookupModel.BACnetData.ObjectDetails[key].PresentValue = presentvalue;
        }

        /// <summary>
        /// Updates the piv present value.
        /// </summary>
        /// <param name="zonePresentStateDataModel">The zone present state data model.</param>
        /// <param name="zoneLookupModel">The zone lookup model.</param>
        /// <param name="pivType">Type of the piv.</param>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>6/22/20173:24 PM</TimeStamp>
        internal static void UpdatePIVPresentValue(long presentValue, EntityBaseModel zoneLookupModel, AppConstants.PositiveIntegerValueType pivType)
        {
            string key = EntityHelper.GetObjectDetailsKey(BacnetObjectType.ObjectPositiveIntegerValue, pivType.ToString());
            if (!zoneLookupModel.BACnetData.ObjectDetails.ContainsKey(key))
                return;
            //If WP is inprocess for object then skip monitoring (to fix BAC-18 issue)
            if (AppCoreRepo.Instance.BACnetObjectsInWPProcess.Contains(zoneLookupModel.BACnetData.ObjectDetails[key].ObjectID))
            {
                return;
            }

            ResultModel<int> deviceId = EntityHelper.GetDeviceId(zoneLookupModel);
            switch (pivType)
            {
                case AppConstants.PositiveIntegerValueType.OccupancyFadeOutTime:
                    presentValue = presentValue / AppConstants.MILLISECONDS_TO_SECOND;
                    break;
                case AppConstants.PositiveIntegerValueType.OccupancyTimeOut:
                    presentValue = presentValue / AppConstants.MILLISECONDS_TO_SECOND;
                    break;
                default:
                    break;
            }
            if (!deviceId.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.ERROR, "Failed to get Device ID for UpdatePIVPresentValue for Entity Key" + zoneLookupModel.EntityKey);
                return;
            }

            if (zoneLookupModel.BACnetData.ObjectDetails[key].IsOutOfService)
            {
                Logger.Instance.Log(LogLevel.DEBUG, "MonitoredDataStackUpdateHelper.UpdatePIVPresentValue : OutofService is true skipping update for DeviceID ");
                return;
            }

            if (zoneLookupModel.BACnetData.ObjectDetails[key].PresentValue.Equals(presentValue))
            {
                return;
            }

            bool result = StackManager.Instance.WriteProperty((uint)deviceId.Data,
                StackDataObjects.Constants.BacnetObjectType.ObjectPositiveIntegerValue, (uint)zoneLookupModel.BACnetData.ObjectDetails[key].ObjectID, presentValue,
                StackDataObjects.Constants.BacnetPropertyID.PropPresentValue, StackDataObjects.Constants.BacnetDataType.BacnetDTUnsigned);

            if (!result)
            {
                Logger.Instance.Log(LogLevel.ERROR, "MonitoredDataStackUpdateHelper.UpdatePIVPresentValue : Property update for positive integer failed " + pivType);
                return;
            }
            UpdatePriorityPIVPresentValue(ref presentValue, zoneLookupModel, key, deviceId.Data, pivType);
            zoneLookupModel.BACnetData.ObjectDetails[key].PresentValue = presentValue;

            //TODO: Update lighting network with DHTarget/pressTime value 
        }
        /// <summary>
        /// Updates the system status.
        /// </summary>
        /// <param name="entityBaseModel">The bacnet object key.</param>
        /// <param name="bacnetDeviceStatus">The bacnet device status.</param>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>7/5/20171:58 PM</TimeStamp>
        internal static void UpdateSystemStatus(EntityBaseModel entityBaseModel, BacnetDeviceStatus bacnetDeviceStatus)
        {
            ResultModel<int> deviceId = EntityHelper.GetDeviceId(entityBaseModel);

            if (!deviceId.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.ERROR, "Failed to get Device ID for UpdatePIVPresentValue for Entity Key" + entityBaseModel.EntityKey);
                return;
            }
            if (entityBaseModel.BACnetData.SystemStatus != bacnetDeviceStatus)
            {
                bool result = StackManager.Instance.WriteProperty((uint)deviceId.Data, BacnetObjectType.ObjectDevice, (uint)deviceId.Data, bacnetDeviceStatus, BacnetPropertyID.PropSystemStatus);

                if (!result)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "MonitoredDataStackUpdateHelper.UpdateSystemStatus : System status update failed for controller device");
                    return;
                }

                entityBaseModel.BACnetData.SystemStatus = bacnetDeviceStatus;
            }
        }

        /// <summary>
        /// Gets the integer property value from Response result.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        /// <CreatedBy>Rohit Galgali</CreatedBy>
        /// <TimeStamp>7/3/20175:11 PM</TimeStamp>
        private static dynamic GetPropertyValue<T>(StackDataObjects.Response.ResponseResult value)
        {
            dynamic brightness = Convert.ChangeType(((Molex.StackDataObjects.Response.GetObjectPropertyValueResponse)(value.Response)).PropertyValue, typeof(T));
            return brightness;
        }

        internal static void UpdateMsvPresentValue(string presentValue, EntityBaseModel zoneLookupModel, string msvObjectType)
        {
            string key = EntityHelper.GetObjectDetailsKey(BacnetObjectType.ObjectMultiStateValue, msvObjectType);
            int stateValue = -1;

            if (msvObjectType == AppConstants.RESET_ATTRIBUTES_AFTER_TIMEOUT)
            {
                stateValue = ConvertApiResetAttributeToMsvStateValue(presentValue);
                if (stateValue == -1)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "ConvertApiResetAttributeToMsvStateValue '" + presentValue + "' => '" + stateValue.ToString() + "'");
                    return;
                }
            }

            ResultModel<int> deviceId = EntityHelper.GetDeviceId(zoneLookupModel);
            if (!deviceId.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.ERROR, "Failed to get Device ID for UpdateMsvPresentValue for Entity Key" + zoneLookupModel.EntityKey);
                return;
            }

            if (zoneLookupModel.BACnetData.ObjectDetails[key].PresentValue.Equals(stateValue))
            {
                return;
            }

            bool result = StackManager.Instance.WriteProperty((uint)deviceId.Data,
               StackDataObjects.Constants.BacnetObjectType.ObjectMultiStateValue, (uint)zoneLookupModel.BACnetData.ObjectDetails[key].ObjectID, stateValue,
               StackDataObjects.Constants.BacnetPropertyID.PropPresentValue, StackDataObjects.Constants.BacnetDataType.BacnetDTReal);

            if (!result)
            {
                Logger.Instance.Log(LogLevel.ERROR, "MonitoredDataStackUpdateHelper.UpdateMsvPresentValue : Failed to update Type: " + msvObjectType + " with value: " + presentValue);
                return;
            }

            zoneLookupModel.BACnetData.ObjectDetails[key].PresentValue = stateValue;
        }

        private static int ConvertApiResetAttributeToMsvStateValue(string resetAttributeString)
        {
            int stateValue = -1;
            List<string> rcvdResetCombo = new List<string>();

            resetAttributeString = resetAttributeString.Trim('[', ']');

            if (string.IsNullOrWhiteSpace(resetAttributeString))
            {
                rcvdResetCombo.Add("0");
            }
            else
            {
                string[] resetAttributes = resetAttributeString.Split(' ',',').ToArray();
                for (int ii = 0; ii < resetAttributes.Length; ii++)
                {
                    for (int jj = 0; jj < AppCoreRepo.Instance.ResetAttributes.Length; jj++)
                    {
                        if (resetAttributes[ii].Trim().Contains(AppCoreRepo.Instance.ResetAttributes[jj].Trim()))
                        {
                            rcvdResetCombo.Add((jj).ToString());
                            break;
                        }
                    }
                }
            }

            if (rcvdResetCombo.Count > 0)
            {
                for (int ii = 0; ii < AppCoreRepo.Instance.AvailableResetAttributeCombos.Length; ii++)
                {
                    string[] combo = AppCoreRepo.Instance.AvailableResetAttributeCombos[ii].Split('+');
                    if (rcvdResetCombo.Count != combo.Length)
                        continue;
                    if (rcvdResetCombo.Intersect(combo).Count() == combo.Length)
                    {
                        stateValue = (ii + 1);
                        break;
                    }
                }
            }

            return stateValue;
        }

        /// <summary>
        /// Update the brightness with the priority.
        /// </summary>
        /// <param name="brightness">brightness</param>
        /// <param name="entityLookupModel">entityLookupModel</param>
        /// <param name="key">key</param>
        /// <param name="deviceId">deviceId</param>
        private static void UpdatePriorityBrightness(ref double brightness, EntityBaseModel entityLookupModel, string key, int deviceId)
        {
            Molex.StackDataObjects.Response.ResponseResult value = StackManager.Instance.GetObjectPropertyValue((uint)deviceId,
                                                                                                                (uint)entityLookupModel.BACnetData.ObjectDetails[key].ObjectID,
                                                                                                                BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropPresentValue);

            double updatedBrightness = 0;
            if (!value.IsSucceed)
            {
                Logger.Instance.Log(LogLevel.ERROR, "Failed to Get Object Property value from stack for brightness property.");
                return;
            }

            updatedBrightness = GetPropertyValue<double>(value);

            //This condition notifies that Higher priority PV is set already and we are trying to set a lower priority PV
            //if there is a difference then the value is sent to update
            if (updatedBrightness == brightness)
            {
                return;
            }

            ResultModel<string> lightingNetworkPropertyUpdate = new ResultModel<string>();
            brightness = updatedBrightness;
            lightingNetworkPropertyUpdate = HTTPAPIManager.Instance.UpdateZoneBrightness(((ZoneEntityModel)entityLookupModel), Convert.ToInt32(brightness));

            if (!lightingNetworkPropertyUpdate.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.ERROR, "MonitoredDataStackUpdateHelper:UpdatePriorityBrightness: " + lightingNetworkPropertyUpdate.Error.OptionalMessage);
            }
        }

        /// <summary>
        /// Update the Light scene with Priority.
        /// </summary>
        /// <param name="lightScene">lightScene</param>
        /// <param name="zoneLookupModel">zoneLookupModel</param>
        /// <param name="key">key</param>
        /// <param name="deviceId">deviceId</param>
        /// <param name="index">index</param>
        private static void UpdatedPriorityLightScene(ref string lightScene, EntityBaseModel zoneLookupModel, string key, ResultModel<int> deviceId, int index)
        {
            try
            {
                Molex.StackDataObjects.Response.ResponseResult value = StackManager.Instance.GetObjectPropertyValue((uint)deviceId.Data, (uint)zoneLookupModel.BACnetData.ObjectDetails[key].ObjectID, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropPresentValue);
                int updatedLightScene = 0;
                if (!value.IsSucceed)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "Failed to Get Object Property value from stack for Light scene.");
                }

                updatedLightScene = GetPropertyValue<int>(value);

                if (updatedLightScene == (index + 1))
                {
                    return;
                }

                index = updatedLightScene;
                //updatedLightScene recieved is 1 based array and stateText is 0 based. Hence -1.
                lightScene = ((ZoneEntityModel)zoneLookupModel).LightScene[index - 1];

                ResultModel<string> lightingNetworkPropertyUpdate = new ResultModel<string>();
                lightingNetworkPropertyUpdate = HTTPAPIManager.Instance.UpdateLightScene(((ZoneEntityModel)zoneLookupModel), lightScene);

                if (!lightingNetworkPropertyUpdate.IsSuccess)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "MonitoredDataStackUpdateHelper:UpdatedPriorityLightScene:" + lightingNetworkPropertyUpdate.Error.OptionalMessage);
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "MonitoredDataStackUpdateHelper:UpdatedPriorityLightScene:" + ex.Message.ToString());
            }
        }

        /// <summary>
        /// Update the DHTargetLux with the priority.
        /// </summary>
        /// <param name="presentValue">presentValue</param>
        /// <param name="entityLookupModel">entityLookupModel</param>
        /// <param name="key">key</param>
        /// <param name="deviceId">deviceId</param>
        /// <param name="pivType">pivType</param>
        private static void UpdatePriorityPIVPresentValue(ref long presentValue, EntityBaseModel entityLookupModel, string key, int deviceId, AppConstants.PositiveIntegerValueType pivType)
        {
            Molex.StackDataObjects.Response.ResponseResult value = StackManager.Instance.GetObjectPropertyValue((uint)deviceId, (uint)entityLookupModel.BACnetData.ObjectDetails[key].ObjectID, BacnetObjectType.ObjectPositiveIntegerValue, BacnetPropertyID.PropPresentValue);

            int updatedPresentValue = 0;
            if (!value.IsSucceed)
            {
                Logger.Instance.Log(LogLevel.ERROR, "Failed to Get Object Property value from stack for brightness property.");
                return;
            }

            updatedPresentValue = GetPropertyValue<int>(value);

            //This condition notifies that Higher priority PV is set already and we are trying to set a lower priority PV
            //if there is a difference then the value is sent to update
            if (updatedPresentValue != presentValue)
            {
                ResultModel<string> lightingNetworkPropertyUpdate = new ResultModel<string>();
                presentValue = updatedPresentValue;

                switch (pivType)
                {
                    case AppConstants.PositiveIntegerValueType.DHTargetLux:
                        lightingNetworkPropertyUpdate = HTTPAPIManager.Instance.UpdateDHTargetData(((ZoneEntityModel)entityLookupModel), Convert.ToUInt32(presentValue));
                        break;
                    case AppConstants.PositiveIntegerValueType.OccupancyFadeOutTime:
                        lightingNetworkPropertyUpdate = HTTPAPIManager.Instance.UpdatepresRate(((ZoneEntityModel)entityLookupModel), Convert.ToUInt32(presentValue) * AppConstants.PRESENT_RATE_RATIO);
                        break;
                    case AppConstants.PositiveIntegerValueType.OccupancyTimeOut:
                        lightingNetworkPropertyUpdate = HTTPAPIManager.Instance.UpdatepresTimeout(((ZoneEntityModel)entityLookupModel), Convert.ToUInt32(presentValue) * AppConstants.MILLISECONDS_TO_SECOND);
                        break;
                }

                if (!lightingNetworkPropertyUpdate.IsSuccess)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "MonitoredDataStackUpdatedHelper:UpdatePriorityPIVPresentValue: " + lightingNetworkPropertyUpdate.Error.OptionalMessage);
                }
            }
        }

        /// <summary>
        /// Updates the reliability.
        /// </summary>
        /// <param name="bacnetReliability">The bacnet reliability.</param>
        /// <param name="entityBaseModel">The entity base model.</param>
        /// <param name="bacnetObjectType">Type of the bacnet object.</param>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>7/5/201711:20 AM</TimeStamp>
        internal static void UpdateReliability(BacnetReliability bacnetReliability, EntityBaseModel entityBaseModel, string objectKey, string timestamp)
        {
            DateTime ValueUpdatedOn = Convert.ToDateTime(timestamp);
            ResultModel<int> deviceId = EntityHelper.GetDeviceId(entityBaseModel);
            if (!entityBaseModel.BACnetData.ObjectDetails.ContainsKey(objectKey))
                return;
            if (entityBaseModel.BACnetData.ObjectDetails[objectKey].Reliability != bacnetReliability && ValueUpdatedOn.Subtract(entityBaseModel.TimeStamp).TotalMilliseconds >= 0)
            {
                bool result = StackManager.Instance.WriteProperty((uint)deviceId.Data, entityBaseModel.BACnetData.ObjectDetails[objectKey].ObjectType, entityBaseModel.BACnetData.ObjectDetails[objectKey].ObjectID, bacnetReliability, BacnetPropertyID.PropReliability);

                if (!result)
                {
                    return;
                }
                entityBaseModel.TimeStamp = ValueUpdatedOn;
                entityBaseModel.BACnetData.ObjectDetails[objectKey].Reliability = bacnetReliability;
            }
        }

        /// <summary>
        /// Updates the mood.
        /// </summary>
        /// <param name="mood">The mood.</param>
        /// <param name="entityBaseModel">The entity base model.</param>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>8/8/20176:30 PM</TimeStamp>
        internal static void UpdateBeaconPalettes(string palettes, EntityBaseModel zoneLookupModel)
        {
            string key = EntityHelper.GetObjectDetailsKey(BacnetObjectType.ObjectMultiStateValue, AppConstants.PALETTES_OBJECT);
            if (!zoneLookupModel.BACnetData.ObjectDetails.ContainsKey(key))
                return;
            //If WP(write property request) is inprocess for object then skip monitoring
            if (AppCoreRepo.Instance.BACnetObjectsInWPProcess.Contains(zoneLookupModel.BACnetData.ObjectDetails[key].ObjectID))
            {
                return;
            }
            ResultModel<int> deviceId = EntityHelper.GetDeviceId(zoneLookupModel);

            if (zoneLookupModel.BACnetData.ObjectDetails[key].IsOutOfService)
            {
                Logger.Instance.Log(LogLevel.DEBUG, "MonitoredDataStackUpdateHelper.UpdateMood : OutofService is true skipping update for DeviceID ");
                return;
            }

            if (zoneLookupModel.BACnetData.ObjectDetails[key].PresentValue != palettes)
            {
                int index = Array.IndexOf((zoneLookupModel as ZoneEntityModel).BeaconPalettes.ToArray(), palettes);

                if (index == -1)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "MonitoredDataStackUpdateHelper.UpdateMood: No Matching Mood found.. Name: " + palettes);
                    return;
                }

                bool result = StackManager.Instance.WriteProperty((uint)deviceId.Data, BacnetObjectType.ObjectMultiStateValue, zoneLookupModel.BACnetData.ObjectDetails[key].ObjectID, index + 1, BacnetPropertyID.PropPresentValue);

                if (!result)
                {
                    return;
                }

                zoneLookupModel.BACnetData.ObjectDetails[key].PresentValue = palettes;
            }
        }

        /// <summary>
        /// Updates the mood.
        /// </summary>
        /// <param name="mood">The mood.</param>
        /// <param name="entityBaseModel">The entity base model.</param>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>8/8/20176:30 PM</TimeStamp>
        internal static void UpdateMood(string mood, EntityBaseModel zoneLookupModel)
        {
            string key = EntityHelper.GetObjectDetailsKey(BacnetObjectType.ObjectMultiStateValue, AppConstants.LIGHTING_MOOD_OBJECT);
            if (!zoneLookupModel.BACnetData.ObjectDetails.ContainsKey(key))
                return;
            //If WP(write property request) is inprocess for object then skip monitoring
            if (AppCoreRepo.Instance.BACnetObjectsInWPProcess.Contains(zoneLookupModel.BACnetData.ObjectDetails[key].ObjectID))
            {
                return;
            }
            ResultModel<int> deviceId = EntityHelper.GetDeviceId(zoneLookupModel);

            if (zoneLookupModel.BACnetData.ObjectDetails[key].IsOutOfService)
            {
                Logger.Instance.Log(LogLevel.DEBUG, "MonitoredDataStackUpdateHelper.UpdateMood : OutofService is true skipping update for DeviceID ");
                return;
            }

            if (zoneLookupModel.BACnetData.ObjectDetails[key].Mood != mood)
            {
                int index = Array.IndexOf(AppCoreRepo.Instance.AvailableMoods, mood);

                if (index == -1)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "MonitoredDataStackUpdateHelper.UpdateMood: No Matching Mood found.. Name: " + mood);
                    return;
                }

                bool result = StackManager.Instance.WriteProperty((uint)deviceId.Data, BacnetObjectType.ObjectMultiStateValue, zoneLookupModel.BACnetData.ObjectDetails[key].ObjectID, index + 1, BacnetPropertyID.PropPresentValue);

                if (!result)
                {
                    return;
                }

                zoneLookupModel.BACnetData.ObjectDetails[key].Mood = mood;
            }
        }

        /// <summary>
        /// This Method will update the Building Level Occupancy State
        /// </summary>
        /// <param name="zoneModel"></param>
        /// <param name="key"></param>
        internal static void UpdateBuildingOccupancyState(EntityBaseModel floorModel, string key)
        {
            try
            {
                string floorkey = EntityHelper.GetObjectDetailsKey(BacnetObjectType.ObjectBinaryInput, AppConstants.BUILDING_OCCUPANCY_OBJECT);
                if (AppCoreRepo.Instance.ControllerEntityModel == null)
                    return;
                if (AppCoreRepo.Instance.ControllerEntityModel.Childs == null)
                    return;
                if (AppCoreRepo.Instance.ControllerEntityModel.Childs.Count == 0)
                    return;
                foreach (var buildingItem in AppCoreRepo.Instance.ControllerEntityModel.Childs)
                {
                    if (buildingItem == null)
                        continue;
                    if (buildingItem.Childs == null)
                        continue;
                    if (buildingItem.Childs.Any(s => s.EntityKey == floorModel.EntityKey))
                    {
                        int state = 0;
                        foreach (var zoneItem in buildingItem.Childs)
                        {
                            if (zoneItem == null)
                                continue;
                            if (zoneItem.BACnetData == null)
                                continue;
                            if (zoneItem.BACnetData.ObjectDetails == null)
                                continue;
                            if (!zoneItem.BACnetData.ObjectDetails.ContainsKey(key))
                                continue;
                            if (zoneItem.BACnetData.ObjectDetails[key].PresentValue == null)
                                continue;
                            int value;
                            int.TryParse(zoneItem.BACnetData.ObjectDetails[key].PresentValue.ToString(), out value);
                            if (value != 0)
                            {
                                state = 1;

                                bool result = StackManager.Instance.WriteProperty(AppCoreRepo.Instance.BACnetConfigurationData.ControllerId, StackDataObjects.Constants.BacnetObjectType.ObjectBinaryInput, (uint)buildingItem.BACnetData.ObjectDetails[floorkey].ObjectID, state, StackDataObjects.Constants.BacnetPropertyID.PropPresentValue, StackDataObjects.Constants.BacnetDataType.BacnetDTEnum);
                                if (!result)
                                {
                                    Logger.Instance.Log(LogLevel.ERROR, "MonitoredDataStackUpdateHelper.UpdateLightScene: Property update: Property update for Multi state value failed for " + state);
                                    return;
                                }

                                buildingItem.BACnetData.ObjectDetails[floorkey].PresentValue = state;
                                return;
                            }
                        }
                        if (state == 0)
                        {
                            bool result = StackManager.Instance.WriteProperty(AppCoreRepo.Instance.BACnetConfigurationData.ControllerId, StackDataObjects.Constants.BacnetObjectType.ObjectBinaryInput, (uint)buildingItem.BACnetData.ObjectDetails[floorkey].ObjectID, state, StackDataObjects.Constants.BacnetPropertyID.PropPresentValue, StackDataObjects.Constants.BacnetDataType.BacnetDTEnum);
                            if (!result)
                            {
                                Logger.Instance.Log(LogLevel.ERROR, "MonitoredDataStackUpdateHelper.UpdateLightScene: Property update: Property update for Multi state value failed for " + state);
                                return;
                            }

                            buildingItem.BACnetData.ObjectDetails[floorkey].PresentValue = state;
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// This Method will update the floor Level Occupancy
        /// </summary>
        /// <param name="state"></param>
        /// <param name="zoneModel"></param>
        internal static void UpdateFloorOccupancyState(EntityBaseModel zoneModel, string key)
        {
            string floorkey = EntityHelper.GetObjectDetailsKey(BacnetObjectType.ObjectBinaryInput, AppConstants.FLOOR_OCCUPANCY_OBJECT);
            if (AppCoreRepo.Instance.ControllerEntityModel == null)
                return;
            if (AppCoreRepo.Instance.ControllerEntityModel.Childs == null)
                return;
            if (AppCoreRepo.Instance.ControllerEntityModel.Childs.Count == 0)
                return;
            foreach (var buildingItem in AppCoreRepo.Instance.ControllerEntityModel.Childs)
            {
                if (buildingItem == null)
                    continue;
                if (buildingItem.Childs == null)
                    continue;
                foreach (var floorItem in buildingItem.Childs)
                {
                    if (floorItem == null)
                        continue;
                    if (floorItem.Childs == null)
                        continue;
                    if (floorItem.Childs.Any(s => s.EntityKey == zoneModel.EntityKey))
                    {
                        int state = 0;
                        foreach (var zoneItem in floorItem.Childs)
                        {
                            if (zoneItem == null)
                                continue;
                            if (zoneItem.BACnetData == null)
                                continue;
                            if (zoneItem.BACnetData.ObjectDetails == null)
                                continue;
                            if (!zoneItem.BACnetData.ObjectDetails.ContainsKey(key))
                                continue;
                            if (zoneItem.BACnetData.ObjectDetails[key].PresentValue == null)
                                continue;
                            int value;
                            int.TryParse(zoneItem.BACnetData.ObjectDetails[key].PresentValue.ToString(),out value);
                            if(value != 0)
                            {
                                state = 1;

                                bool result = StackManager.Instance.WriteProperty(AppCoreRepo.Instance.BACnetConfigurationData.ControllerId, StackDataObjects.Constants.BacnetObjectType.ObjectBinaryInput, (uint)floorItem.BACnetData.ObjectDetails[floorkey].ObjectID, state, StackDataObjects.Constants.BacnetPropertyID.PropPresentValue, StackDataObjects.Constants.BacnetDataType.BacnetDTEnum);
                                if (!result)
                                {
                                    Logger.Instance.Log(LogLevel.ERROR, "MonitoredDataStackUpdateHelper.UpdateLightScene: Property update: Property update for Multi state value failed for " + state);
                                    return;
                                }

                                floorItem.BACnetData.ObjectDetails[floorkey].PresentValue = state;
                                UpdateBuildingOccupancyState(floorItem, floorkey);
                                return;
                            }
                        }
                        if(state == 0)
                        {
                            bool result = StackManager.Instance.WriteProperty(AppCoreRepo.Instance.BACnetConfigurationData.ControllerId, StackDataObjects.Constants.BacnetObjectType.ObjectBinaryInput, (uint)floorItem.BACnetData.ObjectDetails[floorkey].ObjectID, state, StackDataObjects.Constants.BacnetPropertyID.PropPresentValue, StackDataObjects.Constants.BacnetDataType.BacnetDTEnum);
                            if (!result)
                            {
                                Logger.Instance.Log(LogLevel.ERROR, "MonitoredDataStackUpdateHelper.UpdateLightScene: Property update: Property update for Multi state value failed for " + state);
                                return;
                            }

                            floorItem.BACnetData.ObjectDetails[floorkey].PresentValue = state;
                            UpdateBuildingOccupancyState(floorItem, floorkey);
                            return;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Updates the state of the on off.
        /// </summary>
        /// <param name="state">if set to <c>true</c> [state].</param>
        /// <param name="zoneLookupModel">The zone lookup model.</param>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>11/22/20178:15 PM</TimeStamp>
        internal static void UpdateZoneOccupancyState(int state, EntityBaseModel zoneLookupModel)
        {
            string key = EntityHelper.GetObjectDetailsKey(BacnetObjectType.ObjectBinaryInput, AppConstants.ZONE_OCCUPANCY_OBJECT);
            if (!zoneLookupModel.BACnetData.ObjectDetails.ContainsKey(key))
                return;
            //If WP(write property request) is inprocess for object then skip monitoring(To resolve BAC-85 issue).
            if (AppCoreRepo.Instance.BACnetObjectsInWPProcess.Contains(zoneLookupModel.BACnetData.ObjectDetails[key].ObjectID))
            {
                return;
            }

            ResultModel<int> deviceId = EntityHelper.GetDeviceId(zoneLookupModel);

            if (!deviceId.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.ERROR, "Property update: Property update for Multi state value failed. Device id not found");
                return;
            }

            if (zoneLookupModel.BACnetData.ObjectDetails[key].IsOutOfService)
            {
                Logger.Instance.Log(LogLevel.DEBUG, "MonitoredDataStackUpdateHelper.UpdateLightScene : OutofService is true skipping update for DeviceID ");
                return;
            }

            if (zoneLookupModel.BACnetData.ObjectDetails[key].PresentValue.Equals(state))
            {
                return;
            }

            bool result = StackManager.Instance.WriteProperty((uint)deviceId.Data, StackDataObjects.Constants.BacnetObjectType.ObjectBinaryInput, (uint)zoneLookupModel.BACnetData.ObjectDetails[key].ObjectID,
                state, StackDataObjects.Constants.BacnetPropertyID.PropPresentValue, StackDataObjects.Constants.BacnetDataType.BacnetDTEnum);

            if (!result)
            {
                Logger.Instance.Log(LogLevel.ERROR, "MonitoredDataStackUpdateHelper.UpdateLightScene: Property update: Property update for Multi state value failed for " + state);
                return;
            }

            zoneLookupModel.BACnetData.ObjectDetails[key].PresentValue = state;
            UpdateFloorOccupancyState(zoneLookupModel, key);
        }

        /// <summary>
        /// Updates the state of the on off.
        /// </summary>
        /// <param name="state">if set to <c>true</c> [state].</param>
        /// <param name="zoneLookupModel">The zone lookup model.</param>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>11/22/20178:15 PM</TimeStamp>
        internal static void UpdateZoneState(int state, EntityBaseModel zoneLookupModel)
        {
            string key = EntityHelper.GetObjectDetailsKey(BacnetObjectType.ObjectBinaryValue, AppConstants.ZONE_ONOFF_OBJECT);

            if (!zoneLookupModel.BACnetData.ObjectDetails.ContainsKey(key))
                return;
            //If WP(write property request) is inprocess for object then skip monitoring(To resolve BAC-85 issue).
            if (AppCoreRepo.Instance.BACnetObjectsInWPProcess.Contains(zoneLookupModel.BACnetData.ObjectDetails[key].ObjectID))
            {
                return;
            }

            ResultModel<int> deviceId = EntityHelper.GetDeviceId(zoneLookupModel);

            if (!deviceId.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.ERROR, "Property update: Property update for Binary value failed. Device id not found");
                return;
            }

            if (zoneLookupModel.BACnetData.ObjectDetails[key].IsOutOfService)
            {
                Logger.Instance.Log(LogLevel.DEBUG, "MonitoredDataStackUpdateHelper.UpdateZoneState : OutofService is true skipping update for DeviceID ");
                return;
            }

            if (zoneLookupModel.BACnetData.ObjectDetails[key].PresentValue.Equals(state))
            {
                return;
            }

            bool result = StackManager.Instance.WriteProperty((uint)deviceId.Data, StackDataObjects.Constants.BacnetObjectType.ObjectBinaryValue, (uint)zoneLookupModel.BACnetData.ObjectDetails[key].ObjectID,
                state, StackDataObjects.Constants.BacnetPropertyID.PropPresentValue, StackDataObjects.Constants.BacnetDataType.BacnetDTReal);

            if (!result)
            {
                return;
            }

            zoneLookupModel.BACnetData.ObjectDetails[key].PresentValue = state;
        }

        internal static void UpdateAverageSensorValues(float presentValue, EntityBaseModel entityBaseModel, AppConstants.SensorType sensorType)
        {
            BacnetObjectType objectType = EntityHelper.GetBACnetObjectTypeBySensorType(sensorType);
            string key = EntityHelper.GetObjectDetailsKey(objectType, sensorType.ToString());

            if (!entityBaseModel.BACnetData.ObjectDetails.ContainsKey(key))
            {
                return;
            }

            //In case of sensor disable from FM while monitoring BACnet Gateway set the value as -99 for
            //disabled sensor while parsing the molex API published data based on sensor type avaibility in device statistics field of space state
            if (presentValue == -99)
            {
                return;
            }

            ResultModel<int> deviceId = EntityHelper.GetDeviceId(entityBaseModel);

            if (!deviceId.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.ERROR, "Failed to get Device ID for UpdateAverageSensorValues for Entity Key" + entityBaseModel.EntityKey);
                return;
            }

            if (entityBaseModel.BACnetData.ObjectDetails[key].IsOutOfService)
            {
                Logger.Instance.Log(LogLevel.DEBUG, "MonitoredDataStackUpdateHelper.UpdateAverageSensorValues : OutofService is true skipping update for DeviceID ");
                return;
            }

            if (entityBaseModel.BACnetData.ObjectDetails[key].PresentValue.Equals(presentValue))
            {
                return;
            }

            switch (sensorType)
            {
                case AppConstants.SensorType.Presence:
                    //if IsOccupied has values other than 0 & 1 then this condition.
                    presentValue = presentValue == 0 ? 0 : 1;
                    break;
                case AppConstants.SensorType.Temperature:
                    if (AppCoreRepo.Instance.BACnetConfigurationData.IsTempRequiredInFahrenheit)
                    {
                        presentValue = EntityHelper.ConvertCelsiusToFahrenheit(presentValue);
                    }
                    break;
            }

            bool result = StackManager.Instance.WriteProperty((uint)deviceId.Data,
                objectType, (uint)entityBaseModel.BACnetData.ObjectDetails[key].ObjectID, presentValue,
                StackDataObjects.Constants.BacnetPropertyID.PropPresentValue, BacnetDataType.BacnetDTReal);

            if (!result)
            {
                return;
            }
            entityBaseModel.BACnetData.ObjectDetails[key].PresentValue = presentValue;
        }
    }
}