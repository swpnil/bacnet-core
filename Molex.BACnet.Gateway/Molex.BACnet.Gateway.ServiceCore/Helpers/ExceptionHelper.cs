﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              <ExceptionHelper.cs>
///   Description:        <This class providing functions for ExceptionHelpers>
///   Author:             Prasad Joshi                  
///   Date:               05/15/17
///---------------------------------------------------------------------------------------------
#endregion

using Molex.BACnet.Gateway.Log;
using System;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Helpers
{
    /// <summary>
    /// This class providing functions for ExceptionHelpers
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:14 PM</TimeStamp>
    public class ExceptionHelper
    {
        /// <summary>
        /// Registers the exception handlers.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:14 PM</TimeStamp>
        public void RegisterExceptionHandlers()
        {
            InitLogger();
            // This event is raised every time when an exception occurs even if exception is handled and can be used to log the exception.
            AppDomain.CurrentDomain.FirstChanceException += new EventHandler<System.Runtime.ExceptionServices.FirstChanceExceptionEventArgs>(CurrentDomain_FirstChanceException);

            // This event is raised every time an exception occur and is not handled.(UI thread or any other thread initialized).
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

            // This event is raised every time an unhandled exception occurs on UI thread.
            //App.Current.DispatcherUnhandledException += new System.Windows.Threading.DispatcherUnhandledExceptionEventHandler(App_DispatcherUnhandledException);

            // This event is raised by Task.
            TaskScheduler.UnobservedTaskException += new EventHandler<UnobservedTaskExceptionEventArgs>(TaskScheduler_UnobservedTaskException);

            // Register this event only if any winforms components are used.
            //System.Windows.Forms.Application.ThreadException += WinFormApplication_ThreadException;
        }

        /// <summary>
        /// Initializes the logger.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:14 PM</TimeStamp>
        public void InitLogger()
        {
            // Initialize logger            
            Logger.Instance.Error += new EventHandler<LoggerErrorEventArgs>(Logger_Error);
        }

        /// <summary>
        /// Handles the Error event of the Logger control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="LoggerErrorEventArgs"/> instance containing the event data.</param>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:14 PM</TimeStamp>
        void Logger_Error(object sender, LoggerErrorEventArgs e)
        {
            // TODO: Handle logger error here, need to display a message to user based on the type of error.
            try
            {
                //EventLogger.Log("Logger error occured. Error Code:" + e.ErrorCode + ". Error Msg: " + Convert.ToString(e.LoggerException.Message));
            }
            catch (Exception ex)
            {
                //EventLogger.Log("Unhandled exception occured in Logger_Error.");
            }
        }

        /// <summary>
        /// Handles the UnhandledException event of the CurrentDomain control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="UnhandledExceptionEventArgs"/> instance containing the event data.</param>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:15 PM</TimeStamp>
        void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            HandleUnhandledException(e.ExceptionObject as Exception);
        }

        /// <summary>
        /// Handles the UnobservedTaskException event of the TaskScheduler control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="UnobservedTaskExceptionEventArgs"/> instance containing the event data.</param>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:15 PM</TimeStamp>
        void TaskScheduler_UnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs e)
        {
            HandleUnhandledException(e.Exception);
            e.SetObserved();
        }

        /// <summary>
        /// Handles the FirstChanceException event of the CurrentDomain control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Runtime.ExceptionServices.FirstChanceExceptionEventArgs"/> instance containing the event data.</param>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:15 PM</TimeStamp>
        void CurrentDomain_FirstChanceException(object sender, System.Runtime.ExceptionServices.FirstChanceExceptionEventArgs e)
        {
            // Deregister First Chance Exception as if any error occurs while logging than also this event will fire and will be a
            // recursive call.

            try
            {
                AppDomain.CurrentDomain.FirstChanceException -= CurrentDomain_FirstChanceException;
                LogData logData = new LogData();
                logData.IsFirstChanceException = true;
                logData.LogException = e.Exception;
                Logger.Instance.Log(logData);
            }
            catch
            {
            }
            finally
            {
                AppDomain.CurrentDomain.FirstChanceException += new EventHandler<System.Runtime.ExceptionServices.FirstChanceExceptionEventArgs>(CurrentDomain_FirstChanceException);
            }
        }

        /// <summary>
        /// Handles the unhandled exception.
        /// </summary>
        /// <param name="ex">The exception</param>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:15 PM</TimeStamp>
        private void HandleUnhandledException(Exception ex)
        {
            try
            {
                AppDomain.CurrentDomain.FirstChanceException -= CurrentDomain_FirstChanceException;
            }
            catch
            {
            }
            finally
            {
                AppDomain.CurrentDomain.FirstChanceException += new EventHandler<System.Runtime.ExceptionServices.FirstChanceExceptionEventArgs>(CurrentDomain_FirstChanceException);
            }
        }


    }
}
