﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              <CommonHelper.cs>
///   Description:        <This file provides common functions used in multiple classes across Application>
///   Author:             Mandar Bhong                  
///   Date:               05/19/17
///   Notes:              <Notes>
///   Revision History: 
///   Modified By:     Modified Date:    Reviewed By:      Reviewed Date:    Review Comments:
///   Rupesh Saw       05/25/17          Mandar            05/25/17           CreateUniqueObjectName function- Added
///---------------------------------------------------------------------------------------------
#endregion

using Molex.BACnet.Gateway.ServiceCore.Constants;
using Molex.BACnet.Gateway.ServiceCore.Models;
using Molex.BACnet.Gateway.ServiceCore.Models.Entity;
using Molex.StackDataObjects.Constants;
using System;
using System.Text;


namespace Molex.BACnet.Gateway.ServiceCore.Helpers
{
    /// <summary>
    /// This file provides common functions used in multiple classes across Application
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:10 PM</TimeStamp>
    public static class CommonHelper
    {

        /// <summary>
        /// This method will convert MAC Address for Device
        /// </summary>
        /// <param name="instanceNo">Instance number of Device</param>
        /// <param name="length">MAC length</param>
        /// <returns>string MAC Address</returns>
        public static string GenerateMAC(uint instanceNo, out int length)
        {
            string newMac = string.Empty;
            string mac = Convert.ToUInt32(instanceNo).ToString("X");
            length = 6;

            for (int i = 0; i < 12 - mac.Length; i++)
                newMac += "0";

            newMac += mac;

            return newMac;
        }

        /// <summary>
        /// Generate BACnet Object Mapped ID
        /// </summary>
        /// <param name="deviceID">deviceID</param>
        /// <param name="bacnetObjectType">bacnetObjectType</param>
        /// <param name="objectID">objectID</param>
        /// <returns>string BACnetObjectMappedId</returns>
        public static string GenerateBACnetObjectMappedID(uint deviceID, BacnetObjectType bacnetObjectType, uint objectID)
        {
            string mappedObjectID = string.Empty;

            mappedObjectID = deviceID + AppConstants.SPLIT_UP_STRING + bacnetObjectType.ToString() + AppConstants.SPLIT_UP_STRING + objectID;
            return mappedObjectID;
        }

        /// <summary>
        /// Generate BACnet Object Mapped ID
        /// </summary>
        /// <param name="deviceID">deviceID</param>
        /// <param name="bacnetObjectType">bacnetObjectType</param>
        /// <param name="propertyValue">The property value.</param>
        /// <param name="objectID">objectID</param>
        /// <returns>
        /// string BACnetObjectMappedId
        /// </returns>
        public static string GenerateScheduleObjectMappedID(uint deviceID, BacnetObjectType bacnetObjectType, BacnetPropertyID propertyValue, uint objectID)
        {
            string mappedObjectID = string.Empty;

            mappedObjectID = deviceID + AppConstants.SPLIT_UP_STRING + bacnetObjectType.ToString() + AppConstants.SPLIT_UP_STRING + (int)propertyValue + AppConstants.SPLIT_UP_STRING + objectID;
            return mappedObjectID;
        }


        /// <summary>
        /// Prepare Error log message based on ErrorModel data
        /// </summary>
        /// <param name="errorModel">ErrorModel</param>
        /// <param name="msg">Message</param>
        /// <returns>string Log Message</returns>
        public static string PrepareErrorLogMessage(ErrorModel errorModel, String msg)
        {
            StringBuilder sBuilder = new StringBuilder();
            sBuilder.Append("Category: " + errorModel.ErrorCategory + ", Code: " + errorModel.ErrorCode + ", Optional Message: " + errorModel.OptionalMessage);
            sBuilder.Append(", Message: " + msg);
            return sBuilder.ToString();


        }

        /// <summary>
        /// Gets the description.
        /// </summary>
        /// <param name="objectType">Type of the object.</param>
        /// <returns></returns>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>9/11/20172:41 PM</TimeStamp>
        public static string GetDescription(BacnetObjectType objectType)
        {
            return string.Format("This Object Represents {0}", GetObjectName(objectType));
        }

        /// <summary>
        /// Gets the name of the object.
        /// </summary>
        /// <param name="objectType">Type of the object.</param>
        /// <returns></returns>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>9/11/20172:41 PM</TimeStamp>
        /// <exception cref="System.NotSupportedException"></exception>
        public static string GetObjectName(BacnetObjectType objectType)
        {
            switch (objectType)
            {
                case BacnetObjectType.ObjectCalendar:
                    return "Calendar";

                case BacnetObjectType.ObjectEventEnrollment:
                    return "Event Enrollment";

                case BacnetObjectType.ObjectNotificationClass:
                    return "Notification Class";

                case BacnetObjectType.ObjectSchedule:
                    return "Schedule";

                case BacnetObjectType.ObjectTrendlog:
                    return "Trend Log";

                default:
                    throw new NotSupportedException(string.Format("Object Type: {0} Not Supported", objectType));
            }
        }

        /// <summary>
        /// Gets the RGB values.
        /// </summary>
        /// <param name="propertyValue">The property value.</param>
        /// <returns></returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy>
        /// <TimeStamp>11/20/201711:47 AM</TimeStamp>
        public static BeaconZoneEntityModel GetRGBValues(uint propertyValue)
        {
            BeaconZoneEntityModel beaconColorModel = new BeaconZoneEntityModel();
            string value = propertyValue.ToString("000000000");
            beaconColorModel.Red = Convert.ToInt16(value.Substring(0, 3));
            beaconColorModel.Green = Convert.ToInt16(value.Substring(3, 3));
            beaconColorModel.Blue = Convert.ToInt16(value.Substring(6, 3));
            return beaconColorModel;
        }
    }
}
