﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              DestinationTypeConverter
///   Description:        <Description>
///   Author:            Rohit Galgali                  
///   Date:               MM/DD/YY
#endregion
      
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel.DataTypes;
using Molex.StackDataObjects.APIModels;

namespace Molex.BACnet.Gateway.ServiceCore.Helpers.PersistentDataConverters
{
    /// <summary>
    /// 
    /// </summary>
    /// <CreatedBy>hp</CreatedBy>
    /// <TimeStamp>8/23/20174:13 PM</TimeStamp>
    internal static class DestinationTypeConverter
    {
        /// <summary>
        /// Converts to bacnet destination model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>8/23/20174:13 PM</TimeStamp>
        public static BacnetDestinationModel ConvertToBacnetDestinationModel(DestinationModel model)
        {
            BacnetDestinationModel bacnetModel = new BacnetDestinationModel();
            bacnetModel.BacnetRecipient = new BacnetRecipientModel()
            {
                BacnetAddress = GetBacnetAddressModel(model.BacnetRecipient.BacnetAddress),
                DestinationType = model.BacnetRecipient.DestinationType,
                ObjectType = model.BacnetRecipient.ObjectType,
                ObjId = model.BacnetRecipient.ObjId
            };
            bacnetModel.ProcessID = model.ProcessID;
            bacnetModel.IssueConfirmedNotification = model.IssueConfirmedNotification;
            bacnetModel.FromTime = new BacnetTimeModel()
            {
                Hour = model.FromTime.Hour,
                Hundredths = model.FromTime.Hundredths,
                Min = model.FromTime.Min,
                Sec = model.FromTime.Sec
            };
            bacnetModel.ToTime = new BacnetTimeModel()
            {
                Hour = model.ToTime.Hour,
                Hundredths = model.ToTime.Hundredths,
                Min = model.ToTime.Min,
                Sec = model.ToTime.Sec
            };
            bacnetModel.DaysOfWeek = new StackDataObjects.Models.DaysOfWeekBitsModel()
            {
                Monday = model.DaysOfWeek.Monday,
                Tuesday = model.DaysOfWeek.Tuesday,
                Wednesday = model.DaysOfWeek.Wednesday,
                Thursday = model.DaysOfWeek.Thursday,
                Friday = model.DaysOfWeek.Friday,
                Saturday = model.DaysOfWeek.Saturday,
                Sunday = model.DaysOfWeek.Sunday,
            };
            bacnetModel.EventTransitions = new StackDataObjects.Models.EventTransitionBitsModel() 
            {
                ToFault = model.EventTransitions.ToFault,
                ToOffNormal = model.EventTransitions.ToOffNormal,
                ToNormal = model.EventTransitions.ToNormal,
            };
            return bacnetModel;
        }

        /// <summary>
        /// Converts to destination model.
        /// </summary>
        /// <param name="bacnetModel">The bacnet model.</param>
        /// <returns></returns>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>8/23/20174:14 PM</TimeStamp>
        public static DestinationModel ConvertToDestinationModel(BacnetDestinationModel bacnetModel)
        {
            DestinationModel model = new DestinationModel();
            model.IssueConfirmedNotification = bacnetModel.IssueConfirmedNotification;
            model.BacnetRecipient = new RecipientModel()
            {
                BacnetAddress = GetAddressModel(bacnetModel.BacnetRecipient.BacnetAddress),
                DestinationType = bacnetModel.BacnetRecipient.DestinationType == StackDataObjects.Constants.DestinationType.DestinationIsDeviceID ? bacnetModel.BacnetRecipient.DestinationType : StackDataObjects.Constants.DestinationType.DestinationIsIpAddr,
                ObjectType = bacnetModel.BacnetRecipient.ObjectType,
                ObjId = bacnetModel.BacnetRecipient.ObjId
            };
            model.ProcessID = bacnetModel.ProcessID;
            model.FromTime = new TimeModel()
            {
                Hour = bacnetModel.FromTime.Hour,
                Hundredths = bacnetModel.FromTime.Hundredths,
                Min = bacnetModel.FromTime.Min,
                Sec = bacnetModel.FromTime.Sec
            };
            model.ToTime = new TimeModel()
            {
                Hour = bacnetModel.ToTime.Hour,
                Hundredths = bacnetModel.ToTime.Hundredths,
                Min = bacnetModel.ToTime.Min,
                Sec = bacnetModel.ToTime.Sec
            };
            model.DaysOfWeek = new DaysOfWeekModel()
            {
                Monday = bacnetModel.DaysOfWeek.Monday,
                Tuesday = bacnetModel.DaysOfWeek.Tuesday,
                Wednesday = bacnetModel.DaysOfWeek.Wednesday,
                Thursday = bacnetModel.DaysOfWeek.Thursday,
                Friday = bacnetModel.DaysOfWeek.Friday,
                Saturday = bacnetModel.DaysOfWeek.Saturday,
                Sunday = bacnetModel.DaysOfWeek.Sunday,
            };
            model.EventTransitions = new EventTransitionModel()
            {
                ToFault = bacnetModel.EventTransitions.ToFault,
                ToOffNormal = bacnetModel.EventTransitions.ToOffNormal,
                ToNormal = bacnetModel.EventTransitions.ToNormal,
            };
            return model;
        }

        private static BacnetAddressModel GetBacnetAddressModel(AddressModel addressModel)
        {
            return new BacnetAddressModel()
            {
                IPAddress = addressModel.IPAddress,
                IPAddressLength = addressModel.IPAddressLength,
                MacAddress = addressModel.MacAddress,
                MacLength = addressModel.MacLength,
                NetworkNumber = addressModel.NetworkNumber,
                Port = addressModel.Port
            };
        }

        private static AddressModel GetAddressModel(BacnetAddressModel addressModel)
        {
            return new AddressModel()
            {
                IPAddress = addressModel.IPAddress,
                IPAddressLength = addressModel.IPAddressLength,
                MacAddress = addressModel.MacAddress,
                MacLength = addressModel.MacLength,
                NetworkNumber = addressModel.NetworkNumber,
                Port = addressModel.Port
            };
        }
    }
}
