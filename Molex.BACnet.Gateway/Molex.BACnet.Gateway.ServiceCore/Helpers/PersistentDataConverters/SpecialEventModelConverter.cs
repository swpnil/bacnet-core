﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              SpecialEventModelConverter
///   Description:        <Description>
///   Author:             Rohit Galgali             
///   Date:               MM/DD/YY
#endregion
      
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Molex.StackDataObjects.APIModels;
using Newtonsoft.Json;
using Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel.DataTypes;

namespace Molex.BACnet.Gateway.ServiceCore.Helpers.PersistentDataConverters
{
    internal static class SpecialEventModelConverter
    {
        public static BacnetSpecialEventModel ConvertBacnetSpecialEventModel(SpecialEventModel model)
        {
            BacnetSpecialEventModel bacnetModel = new BacnetSpecialEventModel();
            foreach (var timeModel in model.BacnetTimeValue)
            {
                bacnetModel.BacnetTimeValue.Add(new BacnetTimeValueModel()
                {
                    Time = TimeModelConverter.ConvertToBacnetTimeModel(timeModel.Time),
                    Values = GetBacnetValueModels(timeModel.Values)
                });
            }
            switch (model.StatusCalendar)
            {
                case Molex.StackDataObjects.Constants.CalendarEntryStatus.StatusDate:
                    bacnetModel.CalenderReference = DateModelConverter.ConvertToBacnetDateModel(model.StatusDate);
                    break;
                case Molex.StackDataObjects.Constants.CalendarEntryStatus.StatusDateRange:
                    bacnetModel.CalenderReference = DateRangeConverter.ConvertToBacnetDateRangeModel(model.StatusDateRange);
                    break;
                case Molex.StackDataObjects.Constants.CalendarEntryStatus.StatusWeekNDay:
                    bacnetModel.CalenderReference = model.WeeknDay;
                    break;
                case Molex.StackDataObjects.Constants.CalendarEntryStatus.StatusCalReff:
                    bacnetModel.CalenderReference = model.CalendarReference;
                    break;
                default:
                    break;
            }
            
            bacnetModel.EventPriority = model.EventPriority;
            bacnetModel.StatusCalendar = model.StatusCalendar;

            return bacnetModel;
        }

        public static SpecialEventModel ConvertToSpecialEventModel(BacnetSpecialEventModel bacnetModel)
        {
            SpecialEventModel model = new SpecialEventModel();
            foreach (var timeModel in bacnetModel.BacnetTimeValue)
            {
                model.BacnetTimeValue.Add(new TimeValueModel()
                {
                    Time = TimeModelConverter.ConvertToTimeModel(timeModel.Time),
                    Values = GetValueModels(timeModel.Values)
                });
            }

            switch (bacnetModel.StatusCalendar)
            {
                case Molex.StackDataObjects.Constants.CalendarEntryStatus.StatusDate:
                    model.StatusDate = DateModelConverter.ConvertToDateModel((BacnetDateModel)bacnetModel.CalenderReference);
                    break;
                case Molex.StackDataObjects.Constants.CalendarEntryStatus.StatusDateRange:
                    model.StatusDateRange = DateRangeConverter.ConvertToDateRangeModel((BacnetDateRangeModel)bacnetModel.CalenderReference);
                    break;
                case Molex.StackDataObjects.Constants.CalendarEntryStatus.StatusWeekNDay:
                    model.WeeknDay = (WeekNDayModel)bacnetModel.CalenderReference;
                    break;
                case Molex.StackDataObjects.Constants.CalendarEntryStatus.StatusCalReff:
                    model.CalendarReference = (ObjectIdentifierModel)bacnetModel.CalenderReference;
                    break;
                default:
                    break;
            }
            
            model.EventPriority = bacnetModel.EventPriority;
            model.StatusCalendar = bacnetModel.StatusCalendar;

            return model;
        }

        private static List<BacnetValueModel> GetBacnetValueModels(List<PersistentValueModel> valueModels)
        {
            List<BacnetValueModel> bacnetValueModels = new List<BacnetValueModel>();
            foreach (var valueModel in valueModels)
            {
                bacnetValueModels.Add(ValueModelConverter.ConvertToBacnetValueModel(valueModel));
            }
            return bacnetValueModels;
        }

        private static List<PersistentValueModel> GetValueModels(List<BacnetValueModel> bacnetValueModels)
        {
            List<PersistentValueModel> valueModels = new List<PersistentValueModel>();
            foreach (var valueModel in bacnetValueModels)
            {
                valueModels.Add(ValueModelConverter.ConvertToValueModel(valueModel));
            }
            return valueModels;
        }
    }
}
