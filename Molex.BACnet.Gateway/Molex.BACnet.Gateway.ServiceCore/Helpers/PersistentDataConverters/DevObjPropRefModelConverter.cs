﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              DevObjPropRefModelConverter
///   Description:        <Description>
///   Author:             Rohit Galgali                  
///   Date:               MM/DD/YY
#endregion
      
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel.DataTypes;
using Molex.StackDataObjects.APIModels;

namespace Molex.BACnet.Gateway.ServiceCore.Helpers.PersistentDataConverters
{
    /// <summary>
    /// 
    /// </summary>
    /// <CreatedBy>hp</CreatedBy>
    /// <TimeStamp>8/23/20174:14 PM</TimeStamp>
    internal static class DevObjPropRefModelConverter
    {
        /// <summary>
        /// Converts to bacnet dev object property reference model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>8/23/20174:14 PM</TimeStamp>
        public static BACnetDevObjPropRefModel ConvertToBacnetDevObjPropRefModel(DevObjPropRefModel model)
        {
            if (model == null)
                return null;
            BACnetDevObjPropRefModel bacnetModel = new BACnetDevObjPropRefModel();
            bacnetModel.ArrayIndex = model.ArrayIndex;
            bacnetModel.ArrIndxPresent = model.ArrIndxPresent;
            bacnetModel.DeviceIdPresent = model.DeviceIdPresent;
            bacnetModel.DeviceInstance = model.DeviceInstance;
            bacnetModel.DeviceType = model.DeviceType;
            bacnetModel.ObjectType = model.ObjectType;
            bacnetModel.ObjId = model.ObjId;
            bacnetModel.PropertyIdentifier = model.PropertyIdentifier;

            return bacnetModel;
        }

        /// <summary>
        /// Converts to dev object property reference model.
        /// </summary>
        /// <param name="bacnetModel">The bacnet model.</param>
        /// <returns></returns>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>8/23/20174:14 PM</TimeStamp>
        public static DevObjPropRefModel ConvertToDevObjPropRefModel(BACnetDevObjPropRefModel bacnetModel)
        {
            if (bacnetModel == null)
                return null;
            DevObjPropRefModel model = new DevObjPropRefModel();
            model.ArrayIndex = bacnetModel.ArrayIndex;
            model.ArrIndxPresent = bacnetModel.ArrIndxPresent;
            model.DeviceIdPresent = bacnetModel.DeviceIdPresent;
            model.DeviceInstance = bacnetModel.DeviceInstance;
            model.DeviceType = bacnetModel.DeviceType;
            model.ObjectType = bacnetModel.ObjectType;
            model.ObjId = bacnetModel.ObjId;
            model.PropertyIdentifier = bacnetModel.PropertyIdentifier;

            return model;
        }
    }
}
