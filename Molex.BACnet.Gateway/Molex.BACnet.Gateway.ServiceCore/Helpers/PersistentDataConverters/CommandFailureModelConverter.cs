﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              <Class Name>
///   Description:        <Description>
///   Author:             Rohit Galgali               
///   Date:               MM/DD/YY
#endregion
      
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel.DataTypes;
using Molex.StackDataObjects.APIModels;

namespace Molex.BACnet.Gateway.ServiceCore.Helpers.PersistentDataConverters
{
    /// <summary>
    /// 
    /// </summary>
    /// <CreatedBy>hp</CreatedBy>
    /// <TimeStamp>8/23/20174:11 PM</TimeStamp>
    internal class CommandFailureModelConverter
    {
        /// <summary>
        /// Converts to bacnet command failure model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>8/23/20174:11 PM</TimeStamp>
        public static CommandFailureEventParametersModel ConvertToBacnetCommandFailureModel(CommandFailureModel model)
        {
            CommandFailureEventParametersModel bacnetModel = new CommandFailureEventParametersModel();
            bacnetModel.ApplicationTag = model.ApplicationTag;
            bacnetModel.BacnetObjectPropertyReference = DevObjPropRefModelConverter.ConvertToBacnetDevObjPropRefModel(model.ObjectPropertyReference);
            bacnetModel.EventType = StackDataObjects.Constants.BacnetEventType.EventCommandFailure;
            bacnetModel.TimeDelay = model.TimeDelay;
            bacnetModel.Value = model.Value;
            return bacnetModel;
        }

        /// <summary>
        /// Converts to command failure model.
        /// </summary>
        /// <param name="bacnetModel">The bacnet model.</param>
        /// <returns></returns>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>8/23/20174:11 PM</TimeStamp>
        public static CommandFailureModel ConvertToCommandFailureModel(CommandFailureEventParametersModel bacnetModel)
        {
            CommandFailureModel model = new CommandFailureModel();
            model.ApplicationTag = bacnetModel.ApplicationTag;
            model.ObjectPropertyReference = DevObjPropRefModelConverter.ConvertToDevObjPropRefModel(bacnetModel.BacnetObjectPropertyReference);
            model.TimeDelay = bacnetModel.TimeDelay;
            model.Value = bacnetModel.Value;
            return model;
        }

    }
}
