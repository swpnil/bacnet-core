﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              TimeValueArrayModelConverter
///   Description:        <Description>
///   Author:             Rohit Galgali               
///   Date:               MM/DD/YY
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Molex.StackDataObjects.APIModels;
using Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel.DataTypes;

namespace Molex.BACnet.Gateway.ServiceCore.Helpers.PersistentDataConverters
{
    internal static class TimeValueArrayModelConverter
    {
        public static BacnetTimeValueArrayModel ConvertToBacnetTimeValueArrayModel(TimeValueArrayModel model)
        {
            BacnetTimeValueArrayModel bacnetModel = new BacnetTimeValueArrayModel();
            bacnetModel.DayCount = model.DayCount;
            bacnetModel.BacnetTimeValueList = GetBacnetTimeValueList(model.TimeValueList);

            return bacnetModel;
        }

        public static TimeValueArrayModel ConvertToTimeValueArrayModel(BacnetTimeValueArrayModel bacnetModel)
        {
            TimeValueArrayModel model = new TimeValueArrayModel();
            model.DayCount = bacnetModel.DayCount;
            model.TimeValueList = GetTimeValueList(bacnetModel.BacnetTimeValueList);

            return model;
        }

        private static List<TimeValueModel> GetTimeValueList(List<BacnetTimeValueModel> bacnetTimeValueModels)
        {
            List<TimeValueModel> timeModels = new List<TimeValueModel>();
            foreach (var item in bacnetTimeValueModels)
            {
                timeModels.Add(new TimeValueModel()
                {
                    Time = TimeModelConverter.ConvertToTimeModel(item.Time),
                    Values = GetValues(item.Values)
                });
            }
            return timeModels;
        }

        private static List<PersistentValueModel> GetValues(List<BacnetValueModel> bacnetValueModels)
        {
            List<PersistentValueModel> valueModels = new List<PersistentValueModel>();
            foreach (var valueModel in bacnetValueModels)
            {
                valueModels.Add(ValueModelConverter.ConvertToValueModel(valueModel));
            }
            return valueModels;
        }

        private static List<BacnetTimeValueModel> GetBacnetTimeValueList(List<TimeValueModel> timeValueModels)
        {
            List<BacnetTimeValueModel> bacnetTimeModels = new List<BacnetTimeValueModel>();
            foreach (var item in timeValueModels)
            {
                bacnetTimeModels.Add(new BacnetTimeValueModel()
                {
                    Time = TimeModelConverter.ConvertToBacnetTimeModel(item.Time),
                    Values = GetBacnetValues(item.Values)
                });
            }
            return bacnetTimeModels;
        }

        private static List<BacnetValueModel> GetBacnetValues(List<PersistentValueModel> valueModels)
        {
            List<BacnetValueModel> bacnetValueModels = new List<BacnetValueModel>();
            foreach (var valueModel in valueModels)
            {
                bacnetValueModels.Add(ValueModelConverter.ConvertToBacnetValueModel(valueModel));
            }
            return bacnetValueModels;
        }
    }
}
