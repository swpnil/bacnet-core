﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              CalendarEntryModelConverter
///   Description:        Converter helper class for conversion between Bacnet Calendar model and Gateway Calendar model
///   Author:             Rohit Galgali                  
///   Date:               MM/DD/YY
#endregion
      
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel.DataTypes;
using Molex.StackDataObjects.APIModels;

namespace Molex.BACnet.Gateway.ServiceCore.Helpers.PersistentDataConverters
{
    /// <summary>
    /// Converts calendar entry model to and fro
    /// </summary>
    /// <CreatedBy>Rohit Galgali</CreatedBy>
    /// <TimeStamp>8/23/20174:06 PM</TimeStamp>
    internal class CalendarEntryModelConverter
    {
        /// <summary>
        /// Converts to bacnet calendar entry model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        /// <CreatedBy>Rohit Galgali</CreatedBy>
        /// <TimeStamp>8/23/20174:06 PM</TimeStamp>
        public static BacnetCalendarEntryModel ConvertToBacnetCalendarEntryModel(CalendarEntryModel model)
        {
            BacnetCalendarEntryModel bacnetModel = new BacnetCalendarEntryModel();
            bacnetModel.StatusCalendar = model.StatusCalendar;

            switch (model.StatusCalendar)
            {
                case Molex.StackDataObjects.Constants.CalendarEntryStatus.StatusDate:
                    bacnetModel.BacnetCalenderEntry = DateModelConverter.ConvertToBacnetDateModel(model.StatusDate);
                    break;
                case Molex.StackDataObjects.Constants.CalendarEntryStatus.StatusDateRange:
                    bacnetModel.BacnetCalenderEntry = DateRangeConverter.ConvertToBacnetDateRangeModel(model.StatusDateRange);
                    break;
                case Molex.StackDataObjects.Constants.CalendarEntryStatus.StatusWeekNDay:
                    bacnetModel.BacnetCalenderEntry = model.WeeknDay;
                    break;
                default:
                    break;
            }

            return bacnetModel;
        }

        /// <summary>
        /// Converts to calendar entry model.
        /// </summary>
        /// <param name="bacnetModel">The bacnet model.</param>
        /// <returns></returns>
        /// <CreatedBy>Rohit Galgali</CreatedBy>
        /// <TimeStamp>8/23/20174:06 PM</TimeStamp>
        public static CalendarEntryModel ConvertToCalendarEntryModel(BacnetCalendarEntryModel bacnetModel)
        {
            CalendarEntryModel model = new CalendarEntryModel();
            model.StatusCalendar = bacnetModel.StatusCalendar;
            switch (bacnetModel.StatusCalendar)
            {
                case Molex.StackDataObjects.Constants.CalendarEntryStatus.StatusDate:
                    model.StatusDate = DateModelConverter.ConvertToDateModel((BacnetDateModel)bacnetModel.BacnetCalenderEntry);
                    break;
                case Molex.StackDataObjects.Constants.CalendarEntryStatus.StatusDateRange:
                    model.StatusDateRange = DateRangeConverter.ConvertToDateRangeModel((BacnetDateRangeModel)bacnetModel.BacnetCalenderEntry);
                    break;
                case Molex.StackDataObjects.Constants.CalendarEntryStatus.StatusWeekNDay:
                    model.WeeknDay = (WeekNDayModel)bacnetModel.BacnetCalenderEntry;
                    break;
                default:
                    break;
            }
            return model;
        }
    }
}
