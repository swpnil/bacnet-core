﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              TimeModelConverter
///   Description:        <Description>
///   Author:             Rohit Galgali                 
///   Date:               MM/DD/YY
#endregion
      
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Molex.StackDataObjects.APIModels;
using Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel.DataTypes;

namespace Molex.BACnet.Gateway.ServiceCore.Helpers.PersistentDataConverters
{
    internal static class TimeModelConverter
    {
        public static BacnetTimeModel ConvertToBacnetTimeModel(TimeModel model)
        {
            BacnetTimeModel bacnetModel = new BacnetTimeModel();
            bacnetModel.Hour = model.Hour;
            bacnetModel.Hundredths = model.Hundredths;
            bacnetModel.Min = model.Min;
            bacnetModel.Sec = model.Sec;

            return bacnetModel;
        }

        public static TimeModel ConvertToTimeModel(BacnetTimeModel bacnetModel)
        {
            TimeModel model = new TimeModel();
            model.Hour = bacnetModel.Hour;
            model.Hundredths = bacnetModel.Hundredths;
            model.Min = bacnetModel.Min;
            model.Sec = bacnetModel.Sec;

            return model;
        }
    }
}
