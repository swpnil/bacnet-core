﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              ChangeOfStateConverter
///   Description:        <Description>
///   Author:             Rohit Galgali                  
///   Date:               MM/DD/YY
#endregion
      
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel.DataTypes;
using Molex.StackDataObjects.APIModels;

namespace Molex.BACnet.Gateway.ServiceCore.Helpers.PersistentDataConverters
{
    /// <summary>
    /// Converts ChangeOfState data Type to and fro
    /// </summary>
    /// <CreatedBy>Rohit Galgali</CreatedBy>
    /// <TimeStamp>8/23/20174:07 PM</TimeStamp>
    internal class ChangeOfStateConverter
    {
        /// <summary>
        /// Converts to bacnet change of state model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>8/23/20174:08 PM</TimeStamp>
        public static ChangeOfStateEventParametersModel ConvertToBacnetChangeOfStateModel(ChangeOfStateEventModel model)
        {
            ChangeOfStateEventParametersModel bacnetModel = new ChangeOfStateEventParametersModel();
            bacnetModel.EventType = StackDataObjects.Constants.BacnetEventType.EventChangeOfState;
            bacnetModel.PropertyStates = GetAllBacnetPropertyStates(model.PropertyStates);
            bacnetModel.TimeDelay = model.TimeDelay;
            return bacnetModel;
        }

        /// <summary>
        /// Converts to change of state model.
        /// </summary>
        /// <param name="bacnetModel">The bacnet model.</param>
        /// <returns></returns>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>8/23/20174:08 PM</TimeStamp>
        public static ChangeOfStateEventModel ConvertToChangeOfStateModel(ChangeOfStateEventParametersModel bacnetModel)
        {
            ChangeOfStateEventModel model = new ChangeOfStateEventModel();
            model.PropertyStates = GetAllPropertyStates(bacnetModel.PropertyStates);
            model.TimeDelay = bacnetModel.TimeDelay;
            return model;
        }

        private static List<PropertyModel> GetAllPropertyStates(List<PropertyStatesModel> bacnetPropertyModels)
        {
            List<PropertyModel> propertyModels = new List<PropertyModel>();

            foreach (var item in bacnetPropertyModels)
            {
                propertyModels.Add(new PropertyModel()
                {
                    ApplicationTag = item.ApplicationTag,
                    PropertyState = item.PropertyState,
                    Value = item.Value
                });
            }
            return propertyModels;
        }

        private static List<PropertyStatesModel> GetAllBacnetPropertyStates(List<PropertyModel> propertyModels)
        {
            List<PropertyStatesModel> bacnetPropertyModels = new List<PropertyStatesModel>();

            foreach (var item in propertyModels)
            {
                bacnetPropertyModels.Add(new PropertyStatesModel()
                {
                    ApplicationTag = item.ApplicationTag,
                    PropertyState = item.PropertyState,
                    Value = item.Value
                });
            }
            return bacnetPropertyModels;
        }
    }
}
