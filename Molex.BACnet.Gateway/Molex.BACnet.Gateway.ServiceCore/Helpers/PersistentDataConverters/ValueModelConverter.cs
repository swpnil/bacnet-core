﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              ValueModelConverter
///   Description:        <Description>
///   Author:             Rohit Galgali                
///   Date:               MM/DD/YY
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel.DataTypes;


namespace Molex.BACnet.Gateway.ServiceCore.Helpers.PersistentDataConverters
{
    internal static class ValueModelConverter
    {
        public static Molex.StackDataObjects.APIModels.BacnetValueModel ConvertToBacnetValueModel(PersistentValueModel model)
        {
            if (model == null)
                return null;
            Molex.StackDataObjects.APIModels.BacnetValueModel bacnetModel = new Molex.StackDataObjects.APIModels.BacnetValueModel()
            {
                TagType = model.TagType,
                Value = model.Value
            };
            return bacnetModel;
        }

        public static PersistentValueModel ConvertToValueModel(Molex.StackDataObjects.APIModels.BacnetValueModel bacnetModel)
        {
            //This is a patch. Stack has a bug in their code which needs to be addressed.
            //When user sets null, stack should send proper valumodel with tag type as BACNET_APPLICATION_TAG_NULL
            if(bacnetModel == null)
            {
              return new PersistentValueModel()
                {
                    TagType = 0,
                    Value = null
                };
            }
            PersistentValueModel model = new PersistentValueModel()
            {
                TagType = bacnetModel.TagType,
                Value = bacnetModel.Value
            };
            return model;
        }
    }
}
