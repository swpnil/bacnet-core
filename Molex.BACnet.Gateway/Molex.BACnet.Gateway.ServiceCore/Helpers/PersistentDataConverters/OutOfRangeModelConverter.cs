﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              OutOfRangeModelConverter
///   Description:        <Description>
///   Author:             Rohit Galgali             
///   Date:               MM/DD/YY
#endregion
      
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Molex.StackDataObjects.APIModels;
using Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel.DataTypes;
using Molex.StackDataObjects.Constants;

namespace Molex.BACnet.Gateway.ServiceCore.Helpers.PersistentDataConverters
{
    internal class OutOfRangeModelConverter
    {
        public static OutOfRangeEventParametersModel ConvertToBacnetOutOfRangeModel(OutOfRangeModel model)
        {
            OutOfRangeEventParametersModel bacnetModel = new OutOfRangeEventParametersModel();

            bacnetModel.DeadBand = model.DeadBand;
            bacnetModel.EventType = BacnetEventType.EventOutOfRange;
            bacnetModel.HighLimit = model.HighLimit;
            bacnetModel.LowLimit = model.LowLimit;
            bacnetModel.TimeDelay= model.TimeDelay;

            return bacnetModel;
        }

        public static OutOfRangeModel ConvertToOutOfRangeModel(OutOfRangeEventParametersModel bacnetModel)
        {
            OutOfRangeModel model = new OutOfRangeModel();
            model.DeadBand = bacnetModel.DeadBand;
            model.HighLimit = bacnetModel.HighLimit;
            model.LowLimit = bacnetModel.LowLimit;
            model.TimeDelay = bacnetModel.TimeDelay;

            return model;
        }
    }
}
