﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              EventTransitionModelConverter
///   Description:        <Description>
///   Author:             Rohit Galgali                 
///   Date:               MM/DD/YY
#endregion
      
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel.DataTypes;
using Molex.StackDataObjects.Models;


namespace Molex.BACnet.Gateway.ServiceCore.Helpers.PersistentDataConverters
{
    /// <summary>
    /// 
    /// </summary>
    /// <CreatedBy>hp</CreatedBy>
    /// <TimeStamp>8/23/20174:15 PM</TimeStamp>
    internal static class EventTransitionModelConverter
    {
        /// <summary>
        /// Converts to event transition model.
        /// </summary>
        /// <param name="bacnetModel">The bacnet model.</param>
        /// <returns></returns>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>8/23/20174:15 PM</TimeStamp>
        internal static EventTransitionModel ConvertToEventTransitionModel(EventTransitionBitsModel bacnetModel)
        {
            if (bacnetModel == null)
                return null;
            EventTransitionModel model = new EventTransitionModel()
            {
                ToFault = bacnetModel.ToFault,
                ToNormal = bacnetModel.ToNormal,
                ToOffNormal = bacnetModel.ToOffNormal
            };
            return model;
        }

        /// <summary>
        /// Converts to bacnet event transition model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>8/23/20174:15 PM</TimeStamp>
        internal static EventTransitionBitsModel ConvertToBacnetEventTransitionModel(EventTransitionModel model)
        {
            if (model == null)
                return null;
            EventTransitionBitsModel bacnetModel = new EventTransitionBitsModel()
            {
                ToFault = model.ToFault,
                ToNormal = model.ToNormal,
                ToOffNormal = model.ToOffNormal
            };
            return bacnetModel;
        }
    }
}
