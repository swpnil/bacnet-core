﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              DateRangeConverter
///   Description:        <Description>
///   Author:             Rohit Galgali                 
///   Date:               MM/DD/YY
#endregion
      
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel.DataTypes;
using Molex.StackDataObjects.APIModels;

namespace Molex.BACnet.Gateway.ServiceCore.Helpers.PersistentDataConverters
{
    /// <summary>
    /// This Converter converts DateRangeModel in Gateway to BacnetDateRangeModel and vice versa.
    /// </summary>
    /// <CreatedBy>Rohit Galgali</CreatedBy>
    /// <TimeStamp>8/8/201712:04 PM</TimeStamp>
    internal static class DateRangeConverter
    {
        /// <summary>
        /// Converts to date range model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>8/8/201712:14 PM</TimeStamp>
        public static DateRangeModel ConvertToDateRangeModel(BacnetDateRangeModel model)
        {
            //BacnetDateRangeModel rangeModel = (BacnetDateRangeModel)model;
            Models.PersistentModel.DataTypes.DateModel startDate = new Models.PersistentModel.DataTypes.DateModel()
            {
                Day = model.StartDate.Day,
                Month = model.StartDate.Month,
                WeekDay = model.StartDate.WeekDay,
                Year = model.StartDate.Year
            };
            Models.PersistentModel.DataTypes.DateModel endDate = new Models.PersistentModel.DataTypes.DateModel()
            {
                Day = model.EndDate.Day,
                Month = model.EndDate.Month,
                WeekDay = model.EndDate.WeekDay,
                Year = model.EndDate.Year
            };

            return new Models.PersistentModel.DataTypes.DateRangeModel() { StartDate = startDate, EndDate = endDate };
        }

        /// <summary>
        /// Converts to bacnet date range model.
        /// </summary>
        /// <param name="dateRangeModel">The date range model.</param>
        /// <returns></returns>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>8/8/201712:14 PM</TimeStamp>
        public static BacnetDateRangeModel ConvertToBacnetDateRangeModel(Models.PersistentModel.DataTypes.DateRangeModel dateRangeModel)
        {
            if(dateRangeModel == null)
                return null;
            
            BacnetDateModel startModel = new BacnetDateModel()
            {
                Day = dateRangeModel.StartDate.Day,
                Month = dateRangeModel.StartDate.Month,
                WeekDay = dateRangeModel.StartDate.WeekDay,
                Year = dateRangeModel.StartDate.Year
            };
            BacnetDateModel endModel = new BacnetDateModel()
            {
                Day = dateRangeModel.EndDate.Day,
                Month = dateRangeModel.EndDate.Month,
                WeekDay = dateRangeModel.EndDate.WeekDay,
                Year = dateRangeModel.EndDate.Year
            };
            BacnetDateRangeModel model = new BacnetDateRangeModel() { StartDate = startModel, EndDate = endModel };
            return model;
        }
    }
}
