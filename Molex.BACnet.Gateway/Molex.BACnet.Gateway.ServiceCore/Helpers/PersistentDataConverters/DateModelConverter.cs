﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              DateModelConverter
///   Description:        <Description>
///   Author:             Rohit Galgali                  
///   Date:               MM/DD/YY
#endregion
      
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Molex.StackDataObjects.APIModels;
using Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel.DataTypes;


namespace Molex.BACnet.Gateway.ServiceCore.Helpers.PersistentDataConverters
{
    /// <summary>
    /// 
    /// </summary>
    /// <CreatedBy>hp</CreatedBy>
    /// <TimeStamp>8/23/20174:12 PM</TimeStamp>
    internal static class DateModelConverter
    {
        /// <summary>
        /// Converts to bacnet date model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>8/23/20174:12 PM</TimeStamp>
        public static BacnetDateModel ConvertToBacnetDateModel(DateModel model)
        {
            BacnetDateModel bacnetModel = new BacnetDateModel();
            bacnetModel.Day = model.Day;
            bacnetModel.Month = model.Month;
            bacnetModel.WeekDay = model.WeekDay;
            bacnetModel.Year = model.Year;

            return bacnetModel;
        }

        /// <summary>
        /// Converts to date model.
        /// </summary>
        /// <param name="bacnetModel">The bacnet model.</param>
        /// <returns></returns>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>8/23/20174:12 PM</TimeStamp>
        public static DateModel ConvertToDateModel(BacnetDateModel bacnetModel)
        {
            DateModel model = new DateModel();
            model.Day = bacnetModel.Day;
            model.Month = bacnetModel.Month;
            model.WeekDay = bacnetModel.WeekDay;
            model.Year = bacnetModel.Year;

            return model;
        }
    }
}
