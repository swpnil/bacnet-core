﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              DateTimeModelConverter
///   Description:        <Description>
///   Author:             Rohit Galgali                 
///   Date:               MM/DD/YY
#endregion
      

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Molex.StackDataObjects.APIModels;
using Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel.DataTypes;

namespace Molex.BACnet.Gateway.ServiceCore.Helpers.PersistentDataConverters
{
    /// <summary>
    /// 
    /// </summary>
    /// <CreatedBy>hp</CreatedBy>
    /// <TimeStamp>8/23/20174:13 PM</TimeStamp>
    internal static class DateTimeModelConverter
    {
        /// <summary>
        /// Converts to bacnet date time model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>8/23/20174:13 PM</TimeStamp>
        public static BacnetDateTimeModel ConvertToBacnetDateTimeModel(DateTimeModel model)
        {
            if (model == null)
                return null;
            BacnetDateTimeModel bacnetModel = new BacnetDateTimeModel();
            bacnetModel.Date = DateModelConverter.ConvertToBacnetDateModel(model.Date);
            bacnetModel.Time = TimeModelConverter.ConvertToBacnetTimeModel(model.Time);
            return bacnetModel;
        }

        /// <summary>
        /// Converts to date time model.
        /// </summary>
        /// <param name="bacnetModel">The bacnet model.</param>
        /// <returns></returns>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>8/23/20174:13 PM</TimeStamp>
        public static DateTimeModel ConvertToDateTimeModel(BacnetDateTimeModel bacnetModel)
        {
            if (bacnetModel == null)
                return null;
            DateTimeModel model = new DateTimeModel();
            model.Date = DateModelConverter.ConvertToDateModel(bacnetModel.Date);
            model.Time = TimeModelConverter.ConvertToTimeModel(bacnetModel.Time);
            return model;
        }
    }
}
