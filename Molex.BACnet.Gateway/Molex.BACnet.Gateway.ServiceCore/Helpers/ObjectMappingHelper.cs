﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              ObjectMappingHelper
///   Description:        Helper class to get and write data to mapping file.
///   Author:             Rohit Galgali                
///   Date:               MM/DD/YY
#endregion

using Molex.BACnet.Gateway.Log;
using Molex.BACnet.Gateway.ServiceCore.Constants;
using Molex.BACnet.Gateway.ServiceCore.Models;
using Molex.BACnet.Gateway.ServiceCore.Models.Entity;
using Molex.BACnet.Gateway.ServiceCore.Models.ObjectMap;
using Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel;
using Molex.BACnet.Gateway.ServiceCore.Repository;
using Molex.BACnet.Gateway.Utility.Helper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Molex.BACnet.Gateway.ServiceCore.Helpers
{
    /// <summary>
    /// ObjectMappingHelper is used to map Entity objects for Object Mapping
    /// </summary>
    /// <CreatedBy>hp</CreatedBy>
    /// <TimeStamp>6/9/20174:01 PM</TimeStamp>
    public class ObjectMappingHelper
    {
        #region Helper Methods exposed

        /// <summary>
        /// Updates the object mapping data from file.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <param name="mappingData">The cached object details.</param>
        /// <returns>boolen</returns>
        /// <CreatedBy>Rohit</CreatedBy><TimeStamp>15-06-201712:25 PM</TimeStamp>
        internal static bool GetObjectMappingDataFromFile(string filePath, out MappingData mappingData)
        {
            mappingData = new MappingData();
            mappingData.CachedObjectDetails = new Dictionary<string, EntityObject>();
            if (!File.Exists(filePath))
            {
                Logger.Instance.Log(LogLevel.DEBUG, "Mapping Data file does not exist.");
                return false;
            }
            else
            {
                mappingData = JSONReaderWriterHelper.ReadJsonFromFile<MappingData>(filePath);
                if (mappingData.CachedObjectDetails == null || mappingData.CachedObjectDetails.Count == 0)
                {
                    Logger.Instance.Log(LogLevel.DEBUG, "Mapping Data file is blank or the format is broken.");
                    return false;
                }
                //this ensures that ObjectMapping is filled and can be used for further refernces.
                return true;
            }
        }

        /// <summary>
        /// Writes the mapped data to file.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="cachedObjectDetails">The cached object details.</param>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>6/14/20175:48 PM</TimeStamp>
        internal static void WriteMappedDataToFile(string fileName, MappingData cachedObjectDetails)
        {
            JSONReaderWriterHelper.WriteToJsonFile(fileName, cachedObjectDetails);
        }

        /// <summary>
        /// Maps the data. gets the model entites and generates an dictonary
        /// to hold the mapping
        /// </summary>
        /// <param name="model">The EntityBaseModel.</param>
        /// <param name="cachedObjectDetails">The entity ba cnet cache.</param>
        /// <CreatedBy>Rohit galgali</CreatedBy>
        /// <TimeStamp>6/8/20171:43 PM</TimeStamp>
        internal static Dictionary<string, EntityObject> MapEntityToObjectData(EntityBaseModel model)
        {
            Dictionary<string, EntityObject> cachedObjectDetails = new Dictionary<string, EntityObject>();
            cachedObjectDetails.Add(model.MappingKey, GetEntityObject(model));//Mapping Controller data to file.

            MapEntityTillZoneLevel(model, cachedObjectDetails);
            MapFixturesLevel(model, cachedObjectDetails);
            MapSensors(model, cachedObjectDetails);
            return cachedObjectDetails;
        }

        /// <summary>
        /// Gets the device identifier from mapping data.
        /// </summary>
        /// <param name="entityBaseModel">The entity base model.</param>
        /// <param name="cachedObjectDetails">The processor configuration.</param>
        /// <returns></returns>
        /// <CreatedBy>Rohit Galgali</CreatedBy>
        /// <TimeStamp>6/12/20174:59 PM</TimeStamp>
        internal static ResultModel<int> GetDeviceIdFromMappingData(EntityBaseModel entityBaseModel, Dictionary<string, EntityObject> cachedObjectDetails)
        {
            Logger.Instance.Log(LogLevel.DEBUG, "GetDeviceIdFromMappingData: Reading DeviceId from Mapping file - Started ");
            ResultModel<int> result = new ResultModel<int>();

            try
            {
                result.Data = cachedObjectDetails[entityBaseModel.MappingKey].DeviceId;
                result.IsSuccess = true;
                return result;
            }
            catch (Exception e)
            {
                Logger.Instance.Log(LogLevel.DEBUG, "GetDeviceIdFromMappingData: DeviceId in Mapping file not found.");
                result.IsSuccess = false;
                result.Error = new ErrorModel();
                return result;
            }
        }

        /// <summary>
        /// Gets the object identifier from mapping data.
        /// </summary>
        /// <param name="processorConfiguration">The processor configuration.</param>
        /// <param name="deviceID">The device identifier.</param>
        /// <param name="mappingKey">The entity key.</param>
        /// <param name="objectKey">The object key.</param>
        /// <returns></returns>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>6/14/20173:01 PM</TimeStamp>
        internal static ResultModel<uint> GetObjectIdFromMappingData(ProcessorConfigurationModel processorConfiguration, string mappingKey, string objectKey)
        {
            ResultModel<uint> result = new ResultModel<uint>();
            try
            {
                string key = objectKey;
                if (!processorConfiguration.EntityBACnetCache.ContainsKey(mappingKey))
                    return result;
                if (!processorConfiguration.EntityBACnetCache[mappingKey].BACnetEntityMap.ContainsKey(key))
                    return result;
                result.Data = processorConfiguration.EntityBACnetCache[mappingKey].BACnetEntityMap[key].ObjectID;
                result.IsSuccess = true;
                return result;
            }
            catch (Exception e)
            {
                result.Error = new ErrorModel();
                result.IsSuccess = false;
                return result;
            }
        }


        /// <summary>
        /// Gets the dictionary of device object identifier mapped in the cache file.
        /// </summary>
        /// <param name="entityBACnetCache">The entity ba cnet cache.</param>
        /// <returns></returns>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>6/27/20177:41 PM</TimeStamp>
        internal static Dictionary<uint, Dictionary<string, List<uint>>> GetDeviceObjectIDMapping(Dictionary<string, EntityObject> entityBACnetCache)
        {
            Dictionary<uint, Dictionary<string, List<uint>>> deviceIDMap = new Dictionary<uint, Dictionary<string, List<uint>>>();

            foreach (EntityObject entityObject in entityBACnetCache.Values)
            {
                uint deviceID = (uint)entityObject.DeviceId;

                if (entityObject.EntityType == Constants.AppConstants.EntityTypes.Zone || entityObject.EntityType == Constants.AppConstants.EntityTypes.Controller)
                {
                    deviceIDMap[deviceID] = new Dictionary<string, List<uint>>();
                }

                foreach (string objectKey in entityObject.BACnetEntityMap.Keys)
                {
                    if (!deviceIDMap[deviceID].ContainsKey(objectKey))
                    {
                        deviceIDMap[deviceID][objectKey] = new List<uint>();
                    }

                    deviceIDMap[deviceID][objectKey].Add(entityObject.BACnetEntityMap[objectKey].ObjectID);
                }

            }
            return deviceIDMap;
        }

        /// <summary>
        /// Gets the restor identifier strategy. This method decides which strategy to use for retriving
        /// device id while creating device.
        /// </summary>
        /// <param name="cachedObjectDetails">The cached object details.</param>
        /// <param name="newControllerID">The new controller identifier.</param>
        /// <returns></returns>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>6/27/20172:45 PM</TimeStamp>
        internal static AppConstants.RestoreIDReclaimStrategy GetRestorIDStatery(Dictionary<string, EntityObject> cachedObjectDetails, uint newControllerID)
        {
            if (newControllerID != cachedObjectDetails.FirstOrDefault().Value.DeviceId)
            {
                return AppConstants.RestoreIDReclaimStrategy.Incremental;
            }
            return AppConstants.RestoreIDReclaimStrategy.Preserve;
        }

        /// <summary>
        /// This method will return EntityKey vs Device ID from the Persisted Models dictonary
        /// </summary>
        /// <param name="persistedModels">The dictionary.</param>
        /// <returns></returns>
        /// <CreatedBy>Rohit Galgali</CreatedBy>
        /// <TimeStamp>8/2/20177:15 PM</TimeStamp>
        internal static Dictionary<string, uint> GetPersistenceEntityKeyDeviceIDMapping(Dictionary<string, Models.PersistentModel.PersistedModels> persistedModels)
        {
            Dictionary<string, uint> entityKeyDeviceIDMapping = new Dictionary<string, uint>();
            foreach (var item in persistedModels)
            {
                entityKeyDeviceIDMapping[item.Key] = item.Value.DeviceID;
            }

            return entityKeyDeviceIDMapping;
        }

        /// <summary>
        /// This method will return updated Persisted Models.
        /// If any zones are deleted in DT and we persist their properties then it will be removed,
        /// and the models will get updated.
        /// </summary>
        /// <param name="entityKeyDeviceIDMapping">The dictionary.</param>
        /// <param name="persistedEntityKeyDeviceIDMapping">The persisted models.</param>
        /// <returns></returns>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>8/2/20177:15 PM</TimeStamp>
        internal static Dictionary<string, Models.PersistentModel.PersistedModels> GetUpdatePersistentModels(Dictionary<string, uint> entityKeyDeviceIDMapping, Dictionary<string, PersistedModels> persistedEntityKeyDeviceIDMapping)
        {
            Dictionary<string, Models.PersistentModel.PersistedModels> values = new Dictionary<string, Models.PersistentModel.PersistedModels>(persistedEntityKeyDeviceIDMapping);
            foreach (var item in values)
            {
                if (entityKeyDeviceIDMapping.ContainsKey(item.Key))
                {
                    if (entityKeyDeviceIDMapping[item.Key] != item.Value.DeviceID)
                        persistedEntityKeyDeviceIDMapping[item.Key].DeviceID = entityKeyDeviceIDMapping[item.Key];
                }
                else
                {
                    persistedEntityKeyDeviceIDMapping.Remove(item.Key);
                }
            }
            return persistedEntityKeyDeviceIDMapping;
        }


        #endregion

        #region Private modules

        /// <summary>
        /// Maps the sensors.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="cachedObjectDetails">The cached object details.</param>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>6/22/20175:23 PM</TimeStamp>
        private static void MapSensors(EntityBaseModel model, Dictionary<string, EntityObject> cachedObjectDetails)
        {
            foreach (var item in model.Childs)
            {
                if (item.Childs.Count == 0)
                {
                    if (item is ZoneEntityModel)
                    {
                        var zoneModel = item as ZoneEntityModel;
                        foreach (var sensor in zoneModel.Sensors)
                        {
                            if (sensor.BACnetData.ObjectDetails.Count == 0) continue;
                            cachedObjectDetails.Add(sensor.MappingKey, GetSensorDetails(sensor));
                        }
                    }
                }
                MapSensors(item, cachedObjectDetails);
            }
        }

        /// <summary>
        /// Gets the sensor details.
        /// </summary>
        /// <param name="sensor">The sensor.</param>
        /// <returns></returns>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>6/22/20175:23 PM</TimeStamp>
        private static EntityObject GetSensorDetails(SensorEntityModel sensor)
        {
            EntityObject entityObject = new EntityObject();
            entityObject.DeviceId = EntityHelper.GetDeviceId(sensor).Data;
            entityObject.EntityType = sensor.EntityType;
            entityObject.DTName = sensor.BACnetData.DTName;
            foreach (var item in sensor.BACnetData.ObjectDetails)
            {
                entityObject.BACnetEntityMap.Add(item.Key.ToString(), GetObjectDetails(item));
            }
            return entityObject;
        }

        /// <summary>
        /// Maps the fixtures level.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="cachedObjectDetails">The cached object details.</param>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>6/14/20175:46 PM</TimeStamp>
        private static void MapFixturesLevel(EntityBaseModel model, Dictionary<string, EntityObject> cachedObjectDetails)
        {
            foreach (var item in model.Childs)
            {
                if (item.Childs.Count == 0)
                {
                    if (item is ZoneEntityModel)
                    {
                        var zoneModel = item as ZoneEntityModel;
                        foreach (var fixture in zoneModel.Fixtures)
                        {
                            if (fixture.BACnetData.ObjectDetails.Count == 0) continue;
                            cachedObjectDetails.Add(fixture.MappingKey, GetFixtureDetails(fixture));
                        }
                    }
                }
                MapFixturesLevel(item, cachedObjectDetails);
            }
        }

        /// <summary>
        /// Gets the fixture details.
        /// </summary>
        /// <param name="fixtureModel">The fixture model.</param>
        /// <returns></returns>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>6/14/20175:46 PM</TimeStamp>
        private static EntityObject GetFixtureDetails(FixtureEntityModel fixtureModel)
        {
            EntityObject entityObject = new EntityObject();
            entityObject.DeviceId = EntityHelper.GetDeviceId(fixtureModel).Data;
            entityObject.EntityType = fixtureModel.EntityType;
            entityObject.DTName = fixtureModel.BACnetData.DTName;
            foreach (var item in fixtureModel.BACnetData.ObjectDetails)
            {
                entityObject.BACnetEntityMap.Add(item.Key.ToString(), GetObjectDetails(item));
            }
            return entityObject;
        }

        /// <summary>
        /// Maps the entity till zone level.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="EntityBACnetCache">The entity ba cnet cache.</param>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>6/14/20175:46 PM</TimeStamp>
        private static void MapEntityTillZoneLevel(EntityBaseModel model, Dictionary<string, EntityObject> EntityBACnetCache)
        {
            foreach (var item in model.Childs)
            {
                EntityBACnetCache.Add(item.MappingKey, GetEntityObject(item));
                MapEntityTillZoneLevel(item, EntityBACnetCache);
            }
        }

        /// <summary>
        /// Gets the entity object.
        /// </summary>
        /// <param name="entityBaseModel">The entity base model.</param>
        /// <returns></returns>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>6/8/20171:43 PM</TimeStamp>
        private static EntityObject GetEntityObject(EntityBaseModel entityBaseModel)
        {
            EntityObject obj = new EntityObject();

            obj.DeviceId = EntityHelper.GetDeviceId(entityBaseModel).Data;
            obj.EntityType = entityBaseModel.EntityType;
            obj.DTName = entityBaseModel.BACnetData.DTName;
            foreach (var item in entityBaseModel.BACnetData.ObjectDetails)
            {
                obj.BACnetEntityMap.Add(item.Key.ToString(), GetObjectDetails(item));
            }
            return obj;
        }

        /// <summary>
        /// Gets the object details.
        /// </summary>
        /// <param name="bACnetPair">The item.</param>
        /// <returns></returns>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>6/9/201711:01 AM</TimeStamp>
        private static BACnetEntity GetObjectDetails(KeyValuePair<string, Models.BACnet.BACnetObjectDetails> bACnetPair)
        {
            BACnetEntity obj = new BACnetEntity();
            obj.ObjectID = bACnetPair.Value.ObjectID;
            obj.ObjectType = bACnetPair.Key;
            obj.ObjectName = bACnetPair.Value.ObjectName;
            obj.ObjectDescription = bACnetPair.Value.ObjectDescription;
            return obj;
        }

        #endregion

    }
}
