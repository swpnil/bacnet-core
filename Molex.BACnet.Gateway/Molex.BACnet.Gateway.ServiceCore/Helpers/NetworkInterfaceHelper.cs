﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              NetworkInterfaceHelper
///   Description:        To Manage Netwrok Interface
///   Author:             Nazneen Zahid                  
///   Date:               05/25/17
///---------------------------------------------------------------------------------------------
#endregion


using Molex.BACnet.Gateway.ServiceCore.Constants;
using Molex.BACnet.Gateway.ServiceCore.Models;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;

namespace Molex.BACnet.Gateway.ServiceCore.Helpers
{
    /// <summary>
    /// This class is used to Manage Network Interface 
    /// </summary>
    /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>29-05-201714:51</TimeStamp>"
    public class NetworkInterfaceHelper
    {
        /// <summary>
        /// Gets all IP address for network interfaces.
        /// </summary>
        /// <returns>It returns list of IP Address for network interfaces </returns>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>25-05-201717:36</TimeStamp>"
        public static ResultModel<List<string>> GetAllNetworkInterfaces()
        {
            ResultModel<List<string>> result = new ResultModel<List<string>>();
            List<string> ipAddrList = new List<string>();
            try
            {
                NetworkInterface[] networkInterfaces = NetworkInterface.GetAllNetworkInterfaces();
                if (networkInterfaces != null && networkInterfaces.Length > 0)
                {
                    foreach (NetworkInterface item in networkInterfaces)
                    {
                        IPInterfaceProperties ipInterface = item.GetIPProperties();
                        if (ipInterface.UnicastAddresses.Count > 0)
                        {
                            foreach (UnicastIPAddressInformation ip in ipInterface.UnicastAddresses)
                            {
                                if (!IPAddress.IsLoopback(ip.Address) && ip.Address.AddressFamily == AddressFamily.InterNetwork)
                                {
                                    ipAddrList.Add(ip.Address.ToString());
                                }
                            }
                        }
                    }
                }
                Log.Logger.Instance.Log(Log.LogLevel.DEBUG, "GetAllNetworkInterfaces is completed. ");
                result.Data = ipAddrList;
                result.IsSuccess = true;

            }
            catch (Exception ex)
            {
                Log.Logger.Instance.Log(ex, Log.LogLevel.ERROR, "GetAllNetworkInterfaces:Error occured");
                result.IsSuccess = false;
                result.Error = new ErrorModel();
                result.Error.ErrorCategory = AppConstants.ERROR_CATEGORY_SERVER;
                result.Error.ErrorCode = AppConstants.ERROR_CATEGORY_SERVER_UNAVAILABLE;
            }
            return result;

        }
    }
}
