﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       UniDEL Pte. Ltd
///   Class:              <SingletonBase.cs>
///   Description:        <This class is providing Singleton Queue process to be used by diffrent class's>
///   Author:             Sonal Choudhary               
///   Date:               05/01/17
///---------------------------------------------------------------------------------------------
#endregion

using Molex.BACnet.Gateway.ServiceCore.Managers;
using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.SingletonCore
{
    /// <summary>
    /// This class is providing Singleton Queue process to be used by diffrent class's
    /// </summary>
    /// <typeparam name="T">Generic Type class</typeparam>
    /// <typeparam name="T1">Generic Type sub class</typeparam>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:50 PM</TimeStamp>
    /// <seealso cref="Molex.BACnet.Gateway.ServiceCore.SingletonCore.SingletonBase{T}" />
    internal class SingletonQueueBase<T, T1> : SingletonBase<T>
        where T1 : class
        where T : class
    {

        #region Private Veriabls

        static AutoResetEvent _autoResetEvent = null;
        static volatile object _syncAutoResetLock = new object();
        ConcurrentQueue<T1> _queue = new ConcurrentQueue<T1>();
        Thread _workerThread;
        bool _isQuit = false;

        #endregion

        #region Internal Properties
        /// <summary>
        /// Gets the queue count.
        /// </summary>
        internal int QueueCount
        {
            get
            {
                return _queue.Count;
            }
        }

        /// <summary>
        /// Gets or sets the name of the thread.
        /// </summary>
        public string ThreadName { get; protected set; }

        #endregion

        #region Internal Methods

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:50 PM</TimeStamp>
        internal void Init()
        {
            if (_workerThread != null)
            {
                //_workerThread.Abort();
                _workerThread = null;
                _queue = new ConcurrentQueue<T1>();
                _isQuit = false;
                ConstructorInit();
            }
            _workerThread.Start();

            PreInit();
        }

        /// <summary>
        /// Deinitialize this instance.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:50 PM</TimeStamp>
        internal void DeInit()
        {
            //TODO: Handle exception
            PreDeInit();
            _isQuit = true;
            lock (_syncAutoResetLock)
            {
                _autoResetEvent.Set();
            }
        }

        #endregion

        #region Override Methods

        /// <summary>
        /// Constructors the initialize.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:51 PM</TimeStamp>
        protected override void ConstructorInit()
        {
            _autoResetEvent = new AutoResetEvent(false);
            _workerThread = new Thread(new ThreadStart(ProcessQueueItems));
            _workerThread.Priority = ThreadPriority.Normal;
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Enques the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:51 PM</TimeStamp>
        protected void Enque(T1 item)
        {
            _queue.Enqueue(item);

            lock (_syncAutoResetLock)
            {
                _autoResetEvent.Set();
            }
        }

        /// <summary>
        /// Processes the specified queue item.
        /// </summary>
        /// <param name="queueItem">The queue item.</param>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:51 PM</TimeStamp>
        public virtual void Process(T1 queueItem)
        {

        }

        /// <summary>
        /// Pres the de initialize.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:51 PM</TimeStamp>
        protected virtual void PreDeInit()
        {

        }

        /// <summary>
        /// Pres the initialize.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:51 PM</TimeStamp>
        protected virtual void PreInit()
        {

        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Processes the queue items.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:51 PM</TimeStamp>
        void ProcessQueueItems()
        {
            while (_autoResetEvent.WaitOne())
            {
                if (_isQuit) break;

                lock (_syncAutoResetLock)
                {
                    _autoResetEvent.Reset();
                }

                while (_queue.Count > 0)
                {
                    T1 queueItem;
                    try
                    {
                        if (_queue.TryDequeue(out queueItem))
                        {
                            if (queueItem.GetType().Name == typeof(MappingModel).Name)
                            {
                                Process(queueItem);
                            }
                            else
                            {
                                Process(queueItem);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Logger.Instance.Log(ex, Log.LogLevel.ERROR, "SingletonQueueBase.ProcessQueueItems()");
                    }
                }
            }
        }

        #endregion

    }
}
