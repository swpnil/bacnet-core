﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       UniDEL Pte. Ltd
///   Class:              <SingletonBase.cs>
///   Description:        <This class is providing Singleton process to be used by diffrent class's>
///   Author:             Sonal Choudhary               
///   Date:               05/01/17
///---------------------------------------------------------------------------------------------
#endregion

using System;

namespace Molex.BACnet.Gateway.ServiceCore.SingletonCore
{
    /// <summary>
    /// This class is providing Singleton process to be used by diffrent class's
    /// </summary>
    /// <typeparam name="T">Generic Type</typeparam>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:49 PM</TimeStamp>
    public abstract class SingletonBase<T> where T : class
    {
        private static object lockingObject = new object();
        private static T singleTonObject;

        /// <summary>
        /// Initializes a new instance of the <see cref="SingletonBase{T}"/> class.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>26-05-201704:02 PM</TimeStamp>
        protected SingletonBase()
        {
            ConstructorInit();
        }

        /// <summary>
        /// Constructors the initialize.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>26-05-201704:02 PM</TimeStamp>
        protected virtual void ConstructorInit()
        {

        }

        /// <summary>
        /// Gets the instance.
        /// </summary>
        public static T Instance
        {
            get
            {
                if (singleTonObject == null)
                {
                    lock (lockingObject)
                    {
                        if (singleTonObject == null)
                        {
                            singleTonObject = Activator.CreateInstance(typeof(T), true) as T;
                        }
                    }
                }
                return singleTonObject;
            }
        }

    }
}
