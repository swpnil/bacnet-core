﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Molex.BACnet.Gateway.ServiceCore.AppResources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class AppResource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal AppResource() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Molex.BACnet.Gateway.ServiceCore.AppResources.AppResource", typeof(AppResource).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Zone Average {0}.
        /// </summary>
        internal static string AnalogInputObjectRepresent {
            get {
                return ResourceManager.GetString("AnalogInputObjectRepresent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Zone Total Power Usage.
        /// </summary>
        internal static string AnalogInputPowerObjectRepresent {
            get {
                return ResourceManager.GetString("AnalogInputPowerObjectRepresent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Building BioDynamic.
        /// </summary>
        internal static string AnalogValueBioDynamicRepresentBuilding {
            get {
                return ResourceManager.GetString("AnalogValueBioDynamicRepresentBuilding", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Floor BioDynamic.
        /// </summary>
        internal static string AnalogValueBioDynamicRepresentFloor {
            get {
                return ResourceManager.GetString("AnalogValueBioDynamicRepresentFloor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Zone BioDynamic.
        /// </summary>
        internal static string AnalogValueBioDynamicRepresentZone {
            get {
                return ResourceManager.GetString("AnalogValueBioDynamicRepresentZone", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Shading Level.
        /// </summary>
        internal static string AnalogValueBlindLocationObjectSubName {
            get {
                return ResourceManager.GetString("AnalogValueBlindLocationObjectSubName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Master Shading Level.
        /// </summary>
        internal static string AnalogValueBlindObjectName {
            get {
                return ResourceManager.GetString("AnalogValueBlindObjectName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Building Saturation.
        /// </summary>
        internal static string AnalogValueSaturationRepresentBuilding {
            get {
                return ResourceManager.GetString("AnalogValueSaturationRepresentBuilding", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Floor Saturation.
        /// </summary>
        internal static string AnalogValueSaturationRepresentFloor {
            get {
                return ResourceManager.GetString("AnalogValueSaturationRepresentFloor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Zone Saturation.
        /// </summary>
        internal static string AnalogValueSaturationRepresentZone {
            get {
                return ResourceManager.GetString("AnalogValueSaturationRepresentZone", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Blind Building Shading Level.
        /// </summary>
        internal static string AnalogViewRepresentBlindBuilding {
            get {
                return ResourceManager.GetString("AnalogViewRepresentBlindBuilding", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Blind Floor Shading Level.
        /// </summary>
        internal static string AnalogViewRepresentBlindFloor {
            get {
                return ResourceManager.GetString("AnalogViewRepresentBlindFloor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Blind Zone Shading Level.
        /// </summary>
        internal static string AnalogViewRepresentBlindZone {
            get {
                return ResourceManager.GetString("AnalogViewRepresentBlindZone", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Building Light Level.
        /// </summary>
        internal static string AnalogViewRepresentBuilding {
            get {
                return ResourceManager.GetString("AnalogViewRepresentBuilding", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Floor Light Level.
        /// </summary>
        internal static string AnalogViewRepresentFloor {
            get {
                return ResourceManager.GetString("AnalogViewRepresentFloor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Zone Light Level.
        /// </summary>
        internal static string AnalogViewRepresentZone {
            get {
                return ResourceManager.GetString("AnalogViewRepresentZone", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to  Average {0}.
        /// </summary>
        internal static string Average {
            get {
                return ResourceManager.GetString("Average", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Beacon Light Level.
        /// </summary>
        internal static string BeaconBrightness {
            get {
                return ResourceManager.GetString("BeaconBrightness", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to RGB Color.
        /// </summary>
        internal static string BeaconColor {
            get {
                return ResourceManager.GetString("BeaconColor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Beacon Light Level.
        /// </summary>
        internal static string BeaconLightLevel {
            get {
                return ResourceManager.GetString("BeaconLightLevel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Beacon Light Scene.
        /// </summary>
        internal static string BeaconLightScene {
            get {
                return ResourceManager.GetString("BeaconLightScene", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Beacon Notification Class.
        /// </summary>
        internal static string BeaconNotification {
            get {
                return ResourceManager.GetString("BeaconNotification", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Zone Beacon RGB color setting.
        /// </summary>
        internal static string BeaconPositiveIntergerValueRepresent {
            get {
                return ResourceManager.GetString("BeaconPositiveIntergerValueRepresent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Beacon On Off.
        /// </summary>
        internal static string BeaconState {
            get {
                return ResourceManager.GetString("BeaconState", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to beacon.
        /// </summary>
        internal static string BeaconZoneTypeName {
            get {
                return ResourceManager.GetString("BeaconZoneTypeName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Zone Occupancy.
        /// </summary>
        internal static string BinaryInputObjectRepresent {
            get {
                return ResourceManager.GetString("BinaryInputObjectRepresent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Zone Light On Off.
        /// </summary>
        internal static string BinaryValueRepresent {
            get {
                return ResourceManager.GetString("BinaryValueRepresent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to BioDynamic.
        /// </summary>
        internal static string BioDynamicControlName {
            get {
                return ResourceManager.GetString("BioDynamicControlName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Blind Notification Class.
        /// </summary>
        internal static string BlindNotification {
            get {
                return ResourceManager.GetString("BlindNotification", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CoreSync BACnet/IP Structured View Object.
        /// </summary>
        internal static string BuildingDescription {
            get {
                return ResourceManager.GetString("BuildingDescription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to configuration.
        /// </summary>
        internal static string ConfigurationType {
            get {
                return ResourceManager.GetString("ConfigurationType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to DEFAULT.
        /// </summary>
        internal static string DefaultStringResource {
            get {
                return ResourceManager.GetString("DefaultStringResource", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 3000.
        /// </summary>
        internal static string FixtureColorTemperature {
            get {
                return ResourceManager.GetString("FixtureColorTemperature", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CoreSync BACnet/IP Structured View Object.
        /// </summary>
        internal static string FloorDescription {
            get {
                return ResourceManager.GetString("FloorDescription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to General Light Level.
        /// </summary>
        internal static string GeneralLightLevel {
            get {
                return ResourceManager.GetString("GeneralLightLevel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to general.
        /// </summary>
        internal static string GeneralZoneTypeName {
            get {
                return ResourceManager.GetString("GeneralZoneTypeName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Light Scene.
        /// </summary>
        internal static string LightScene {
            get {
                return ResourceManager.GetString("LightScene", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Zone Mood.
        /// </summary>
        internal static string MoodMultiStateValueRepresentZone {
            get {
                return ResourceManager.GetString("MoodMultiStateValueRepresentZone", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Building Light Scene.
        /// </summary>
        internal static string MultiStateValueRepresentBuilding {
            get {
                return ResourceManager.GetString("MultiStateValueRepresentBuilding", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Floor Light Scene.
        /// </summary>
        internal static string MultiStateValueRepresentFloor {
            get {
                return ResourceManager.GetString("MultiStateValueRepresentFloor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Zone Light Scene.
        /// </summary>
        internal static string MultiStateValueRepresentZone {
            get {
                return ResourceManager.GetString("MultiStateValueRepresentZone", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Notification Class.
        /// </summary>
        internal static string NotificationClassRepresent {
            get {
                return ResourceManager.GetString("NotificationClassRepresent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Target Lux Level.
        /// </summary>
        internal static string PIV1 {
            get {
                return ResourceManager.GetString("PIV1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Occupancy TimeOut.
        /// </summary>
        internal static string PIV2 {
            get {
                return ResourceManager.GetString("PIV2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Occupancy FadeOut.
        /// </summary>
        internal static string PIV3 {
            get {
                return ResourceManager.GetString("PIV3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Beacon Color.
        /// </summary>
        internal static string PIV4 {
            get {
                return ResourceManager.GetString("PIV4", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Zone Target Lux level.
        /// </summary>
        internal static string PIVDHTargetRepresent {
            get {
                return ResourceManager.GetString("PIVDHTargetRepresent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Zone Fade out.
        /// </summary>
        internal static string PIVPresRateRepresent {
            get {
                return ResourceManager.GetString("PIVPresRateRepresent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Zone Time out.
        /// </summary>
        internal static string PIVPresTimeOutRepresent {
            get {
                return ResourceManager.GetString("PIVPresTimeOutRepresent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to B-AAC.
        /// </summary>
        internal static string ProfileName {
            get {
                return ResourceManager.GetString("ProfileName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to project.
        /// </summary>
        internal static string ProjectResourceType {
            get {
                return ResourceManager.GetString("ProjectResourceType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Saturation.
        /// </summary>
        internal static string SaturationControlName {
            get {
                return ResourceManager.GetString("SaturationControlName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Occupancy.
        /// </summary>
        internal static string Status {
            get {
                return ResourceManager.GetString("Status", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Structure.
        /// </summary>
        internal static string Structure {
            get {
                return ResourceManager.GetString("Structure", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Total {0} Usage.
        /// </summary>
        internal static string Total {
            get {
                return ResourceManager.GetString("Total", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User Light Level.
        /// </summary>
        internal static string UserLightLevel {
            get {
                return ResourceManager.GetString("UserLightLevel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to user.
        /// </summary>
        internal static string UserZoneTypeName {
            get {
                return ResourceManager.GetString("UserZoneTypeName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Molex BACnet Gateway.
        /// </summary>
        internal static string VendorObjectDescription {
            get {
                return ResourceManager.GetString("VendorObjectDescription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Light Level.
        /// </summary>
        internal static string ZoneBrightness {
            get {
                return ResourceManager.GetString("ZoneBrightness", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Notification Class.
        /// </summary>
        internal static string ZoneNotification {
            get {
                return ResourceManager.GetString("ZoneNotification", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Lights On Off.
        /// </summary>
        internal static string ZoneState {
            get {
                return ResourceManager.GetString("ZoneState", resourceCulture);
            }
        }
    }
}
