﻿using Molex.BACnet.Gateway.Log;
using Molex.BACnet.Gateway.ServiceCore.AppResources;
using Molex.BACnet.Gateway.ServiceCore.Constants;
using Molex.BACnet.Gateway.ServiceCore.Helpers;
using Molex.BACnet.Gateway.ServiceCore.Models;
using Molex.BACnet.Gateway.ServiceCore.Models.Entity;
using Molex.BACnet.Gateway.ServiceCore.Repository;
using Molex.StackDataObjects.APIModels;
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Models;
using Molex.StackDataObjects.Response.AutoResponses;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Managers
{
    public class SCScheduleManager
    {
        private static SCScheduleManager _SCScheduleManager;
        private List<SCScheduleModel> SCScheduleList;
        private ProcessorConfigurationModel processorConfiguration;

        /// <summary>
        /// Singleton Class Implementation
        /// </summary>
        /// <returns></returns>
        public static SCScheduleManager GetInstance()
        {
            if (_SCScheduleManager == null)
            {
                _SCScheduleManager = new SCScheduleManager();
                _SCScheduleManager.SCScheduleList = new List<SCScheduleModel>();
            }
            return _SCScheduleManager;
        }

        public void GetAllSchedules(ProcessorConfigurationModel pConfig)
        {
            try
            {
                processorConfiguration = pConfig;
                if (AppCoreRepo.Instance.ZonesLookUp == null)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "Exception Recevied at GetAllSchedules: Empty ZonesLookUp Recevied ");
                    return;
                }
                if (AppCoreRepo.Instance.ZonesLookUp.Count == 0)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "Exception Recevied at GetAllSchedules: Empty ZonesLookUp Recevied ");
                    return;
                }
                foreach (var item in AppCoreRepo.Instance.ZonesLookUp)
                {
                    if (item.Value == null)
                        continue;
                    string url = AppCoreRepo.Instance.BACnetConfigurationData.MolexAPIUrl + "schedule?type=zone&id=" + item.Value.EntityKey + "&projectid=" + AppCoreRepo.Instance.BACnetConfigurationData.MolexProjectID;
                    APIMolexParameterModel _apiMolexParameterModel = new APIMolexParameterModel();
                    _apiMolexParameterModel.UserName = AppCoreRepo.Instance.BACnetConfigurationData.UserName;
                    _apiMolexParameterModel.Password = AppCoreRepo.Instance.BACnetConfigurationData.Password;
                    //{{IP}}{{basic}}/schedule?type=zone&id={{zone3}}&projectid={{project_id}} 
                    ResultModel<string> result = HTTPHelper.CustomHttpWebRequest(url, "GET", _apiMolexParameterModel);
                    if (result != null)
                    {
                        try
                        {
                            SCScheduleRootObject _SCScheduleRootObject = Newtonsoft.Json.JsonConvert.DeserializeObject<SCScheduleRootObject>(result.Data);
                            if (_SCScheduleRootObject != null)
                                CreateScheduleObjectList(item.Value as ZoneEntityModel, _SCScheduleRootObject);
                        }
                        catch (Exception ex)
                        {
                            Logger.Instance.Log(ex, LogLevel.DEBUG, "Exception Recevied at GetAllSchedules JSON Parsing:");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "Exception Recevied at GetAllSchedules:");
            }
        }

        private void CreateScheduleObjectList(ZoneEntityModel ZoneModel, SCScheduleRootObject objSchedule)
        {
            SCScheduleModel _SCScheduleModel = null;
            if (objSchedule == null)
                return;
            if (objSchedule.result == null)
                return;
            if (objSchedule.result.data == null)
                return;
            if (objSchedule.result.data.Count == 0)
                return;
            if (!SCScheduleList.Any(s => s.ZoneID == ZoneModel.EntityKey))
                _SCScheduleModel = new SCScheduleModel();
            else
                _SCScheduleModel = SCScheduleList.Where(s => s.ZoneID == ZoneModel.EntityKey).First();
            if (_SCScheduleModel.SCScheduleRoot == null)
                _SCScheduleModel.SCScheduleRoot = new List<SCScheduleResponse>();
            if (_SCScheduleModel.ObjectCollection == null)
                _SCScheduleModel.ObjectCollection = new List<SCObjectModel>();
            foreach (var item in objSchedule.result.data)
            {
                if (item.isSuccess)
                {
                    foreach (var scheduleItem in item.response)
                    {
                        _SCScheduleModel.SCScheduleRoot.Add(scheduleItem);
                    }
                }
            }
            switch (ZoneModel.ZoneType)
            {
                case AppConstants.ZoneTypes.UserSpace:
                    {
                        int objectID = 15251;
                        _SCScheduleModel.ObjectCollection.Add(new SCObjectModel(ZoneModel.BACnetData.DeviceID, objectID, ScheduleObjectType.Light_Scene));
                        objectID++;
                        _SCScheduleModel.ObjectCollection.Add(new SCObjectModel(ZoneModel.BACnetData.DeviceID, objectID, ScheduleObjectType.Mood));
                        objectID++;
                        _SCScheduleModel.ObjectCollection.Add(new SCObjectModel(ZoneModel.BACnetData.DeviceID, objectID, ScheduleObjectType.Brightness));
                        objectID++;
                        _SCScheduleModel.ObjectCollection.Add(new SCObjectModel(ZoneModel.BACnetData.DeviceID, objectID, ScheduleObjectType.BioDynamic));
                        objectID++;
                        _SCScheduleModel.ObjectCollection.Add(new SCObjectModel(ZoneModel.BACnetData.DeviceID, objectID, ScheduleObjectType.Saturation));
                        objectID++;
                        _SCScheduleModel.ObjectCollection.Add(new SCObjectModel(ZoneModel.BACnetData.DeviceID, objectID, ScheduleObjectType.Target_Lux));
                        objectID++;
                        _SCScheduleModel.ObjectCollection.Add(new SCObjectModel(ZoneModel.BACnetData.DeviceID, objectID, ScheduleObjectType.Min_Brightness));
                        objectID++;
                        _SCScheduleModel.ObjectCollection.Add(new SCObjectModel(ZoneModel.BACnetData.DeviceID, objectID, ScheduleObjectType.Delta_Up));
                        objectID++;
                        _SCScheduleModel.ObjectCollection.Add(new SCObjectModel(ZoneModel.BACnetData.DeviceID, objectID, ScheduleObjectType.Delta_Down));
                        objectID++;
                        _SCScheduleModel.ObjectCollection.Add(new SCObjectModel(ZoneModel.BACnetData.DeviceID, objectID, ScheduleObjectType.Avaliable_Light_Scenes));
                        objectID++;
                        _SCScheduleModel.ObjectCollection.Add(new SCObjectModel(ZoneModel.BACnetData.DeviceID, objectID, ScheduleObjectType.Occupancy_TimeOut));
                        objectID++;
                        _SCScheduleModel.ObjectCollection.Add(new SCObjectModel(ZoneModel.BACnetData.DeviceID, objectID, ScheduleObjectType.Occupancy_FadeOut));
                        objectID++;
                        _SCScheduleModel.ObjectCollection.Add(new SCObjectModel(ZoneModel.BACnetData.DeviceID, objectID, ScheduleObjectType.Zone_On_Off));
                    }
                    break;
                case AppConstants.ZoneTypes.GeneralSpace:
                    {
                        int objectID = 15251;
                        _SCScheduleModel.ObjectCollection.Add(new SCObjectModel(ZoneModel.BACnetData.DeviceID, objectID, ScheduleObjectType.Light_Scene));
                        objectID++;
                        _SCScheduleModel.ObjectCollection.Add(new SCObjectModel(ZoneModel.BACnetData.DeviceID, objectID, ScheduleObjectType.Mood));
                        objectID++;
                        _SCScheduleModel.ObjectCollection.Add(new SCObjectModel(ZoneModel.BACnetData.DeviceID, objectID, ScheduleObjectType.Brightness));
                        objectID++;
                        _SCScheduleModel.ObjectCollection.Add(new SCObjectModel(ZoneModel.BACnetData.DeviceID, objectID, ScheduleObjectType.BioDynamic));
                        objectID++;
                        _SCScheduleModel.ObjectCollection.Add(new SCObjectModel(ZoneModel.BACnetData.DeviceID, objectID, ScheduleObjectType.Saturation));
                        objectID++;
                        _SCScheduleModel.ObjectCollection.Add(new SCObjectModel(ZoneModel.BACnetData.DeviceID, objectID, ScheduleObjectType.Target_Lux));
                        objectID++;
                        _SCScheduleModel.ObjectCollection.Add(new SCObjectModel(ZoneModel.BACnetData.DeviceID, objectID, ScheduleObjectType.Min_Brightness));
                        objectID++;
                        _SCScheduleModel.ObjectCollection.Add(new SCObjectModel(ZoneModel.BACnetData.DeviceID, objectID, ScheduleObjectType.Delta_Up));
                        objectID++;
                        _SCScheduleModel.ObjectCollection.Add(new SCObjectModel(ZoneModel.BACnetData.DeviceID, objectID, ScheduleObjectType.Delta_Down));
                        objectID++;
                        _SCScheduleModel.ObjectCollection.Add(new SCObjectModel(ZoneModel.BACnetData.DeviceID, objectID, ScheduleObjectType.Avaliable_Light_Scenes));
                        objectID++;
                        _SCScheduleModel.ObjectCollection.Add(new SCObjectModel(ZoneModel.BACnetData.DeviceID, objectID, ScheduleObjectType.Occupancy_TimeOut));
                        objectID++;
                        _SCScheduleModel.ObjectCollection.Add(new SCObjectModel(ZoneModel.BACnetData.DeviceID, objectID, ScheduleObjectType.Occupancy_FadeOut));
                        objectID++;
                        _SCScheduleModel.ObjectCollection.Add(new SCObjectModel(ZoneModel.BACnetData.DeviceID, objectID, ScheduleObjectType.Zone_On_Off));
                    }
                    break;
                case AppConstants.ZoneTypes.BeaconSpace:
                    {
                        int objectID = 15251;
                        _SCScheduleModel.ObjectCollection.Add(new SCObjectModel(ZoneModel.BACnetData.DeviceID, objectID, ScheduleObjectType.Light_Scene));
                        objectID++;
                        _SCScheduleModel.ObjectCollection.Add(new SCObjectModel(ZoneModel.BACnetData.DeviceID, objectID, ScheduleObjectType.Palettes));
                        objectID++;
                        _SCScheduleModel.ObjectCollection.Add(new SCObjectModel(ZoneModel.BACnetData.DeviceID, objectID, ScheduleObjectType.Brightness));
                        objectID++;
                        _SCScheduleModel.ObjectCollection.Add(new SCObjectModel(ZoneModel.BACnetData.DeviceID, objectID, ScheduleObjectType.Avaliable_Light_Scenes));
                        //_SCScheduleModel.ObjectCollection.Add(new SCObjectModel(ZoneModel.BACnetData.DeviceID, objectID++, ScheduleObjectType.Zone_On_Off));
                    }
                    break;
                case AppConstants.ZoneTypes.BlindSpace:
                    {
                        int objectID = 15251;
                        foreach (var item in _SCScheduleModel.SCScheduleRoot)
                        {
                            _SCScheduleModel.ObjectCollection.Add(new SCObjectModel(ZoneModel.BACnetData.DeviceID, objectID, ScheduleObjectType.Shading_Level));
                            objectID++;
                        }
                    }
                    break;
                default:
                    break;
            }
            foreach (var item in _SCScheduleModel.ObjectCollection)
            {
                CreateScheduleInstance(item.DeviceID, item.ObjectID, _SCScheduleModel.SCScheduleRoot, ZoneModel, item.ScheduleObjectTypeProvided);
            }
        }

        private BacnetValueModel GetScheduleDefaultValue()
        {
            BacnetValueModel _BacnetValueModel = new BacnetValueModel();
            _BacnetValueModel.TagType = (byte)BacnetApplicationTag.BacnetApplicationTagNull;
            return _BacnetValueModel;
        }

        /// <summary>
        /// This Method will create the Schedule Object for specific Zone
        /// </summary>
        /// <param name="deviceID"></param>
        /// <param name="objectID"></param>
        /// <param name="scheduleName"></param>
        /// <param name="scheduleDescription"></param>
        /// <returns></returns>
        private bool CreateScheduleInstance(int deviceID, int objectID, List<SCScheduleResponse> modelList, ZoneEntityModel zoneModel, ScheduleObjectType SCScheduleType)
        {
            try
            {
                if (modelList == null)
                    return false;
                string scheduleName = string.Empty;
                #region Set Schedule Name
                switch (zoneModel.ZoneType)
                {
                    case AppConstants.ZoneTypes.UserSpace:
                        scheduleName = "User ";
                        break;
                    case AppConstants.ZoneTypes.BeaconSpace:
                        scheduleName = "Beacon ";
                        break;
                    case AppConstants.ZoneTypes.BlindSpace:
                        scheduleName = "Blind ";
                        break;
                    case AppConstants.ZoneTypes.GeneralSpace:
                        scheduleName = "General ";
                        break;
                    default:
                        break;
                }
                switch (SCScheduleType)
                {
                    case ScheduleObjectType.Light_Scene:
                        scheduleName = scheduleName + "Light Scene Schedule";
                        break;
                    case ScheduleObjectType.Mood:
                        scheduleName = scheduleName + "Mood Schedule";
                        break;
                    case ScheduleObjectType.Palettes:
                        scheduleName = scheduleName + "Palettes Schedule";
                        break;
                    case ScheduleObjectType.Brightness:
                        scheduleName = scheduleName + "Brightness Schedule";
                        break;
                    case ScheduleObjectType.BioDynamic:
                        scheduleName = scheduleName + "BioDynamic Schedule";
                        break;
                    case ScheduleObjectType.Saturation:
                        scheduleName = scheduleName + "Saturation Schedule";
                        break;
                    case ScheduleObjectType.Target_Lux:
                        scheduleName = scheduleName + "Target Lux Schedule";
                        break;
                    case ScheduleObjectType.Min_Brightness:
                        scheduleName = scheduleName + "Min Brightness Schedule";
                        break;
                    case ScheduleObjectType.Delta_Up:
                        scheduleName = scheduleName + "Delta Up Schedule";
                        break;
                    case ScheduleObjectType.Delta_Down:
                        scheduleName = scheduleName + "Delta Down Schedule";
                        break;
                    case ScheduleObjectType.Avaliable_Light_Scenes:
                        scheduleName = scheduleName + "Avaliable Light Scenes Schedule";
                        break;
                    case ScheduleObjectType.Occupancy_TimeOut:
                        scheduleName = scheduleName + "Occupancy TimeOut Schedule";
                        break;
                    case ScheduleObjectType.Occupancy_FadeOut:
                        scheduleName = scheduleName + "Occupancy FadeOut Schedule";
                        break;
                    case ScheduleObjectType.Zone_On_Off:
                        scheduleName = scheduleName + "Zone On/Off Schedule";
                        break;
                    case ScheduleObjectType.Shading_Level:
                        scheduleName = scheduleName + "Master Shading Level Schedule";
                        break;
                    default:
                        break;
                } 
                #endregion
                bool result = StackManager.Instance.CreateObject((uint)deviceID, BacnetObjectType.ObjectSchedule, (uint)objectID, scheduleName);
                if (!result)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "Exception Recevied at CreateScheduleInstance: Create Object failed");
                    return false;
                }
                bool isSuccess = StackManager.Instance.WriteProperty((uint)deviceID, BacnetObjectType.ObjectSchedule, (uint)objectID, (byte)AppCoreRepo.Instance.BACnetConfigurationData.MonitoringDefaultPriority, BacnetPropertyID.PropPriorityForWriting);
                if (!isSuccess)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "Exception Recevied at CreateScheduleInstance: WP MonitoringDefaultPriority Failed");
                    return false;
                }
                isSuccess = StackManager.Instance.WriteProperty((uint)deviceID, BacnetObjectType.ObjectSchedule, (uint)objectID, scheduleName, BacnetPropertyID.PropDescription);
                if (!isSuccess)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "Exception Recevied at CreateScheduleInstance: WP Schedule Description Failed");
                    return false;
                }
                SetPropertyAccessType((uint)deviceID, (uint)objectID, BacnetObjectType.ObjectSchedule, BacnetPropertyID.PropExceptionSchedule, PropAccessType.NOT_SUPPORTED, true);

                #region Need to check Path
                //isSuccess = StackManager.Instance.WriteProperty((uint)deviceID, BacnetObjectType.ObjectSchedule, (uint)objectID, GetScheduleDefaultValue(), BacnetPropertyID.PropPresentValue);
                //if (!isSuccess)
                //{
                //    Logger.Instance.Log(LogLevel.ERROR, "Exception Recevied at CreateScheduleInstance: WP Schedule Present Value Failed");
                //    return false;
                //}

                //isSuccess = StackManager.Instance.WriteProperty((uint)deviceID, BacnetObjectType.ObjectSchedule, (uint)objectID, GetScheduleDefaultValue(), BacnetPropertyID.PropScheduleDefault);
                //if (!isSuccess)
                //{
                //    Logger.Instance.Log(LogLevel.ERROR, "Exception Recevied at CreateScheduleInstance: WP Schedule Schedule Default Failed");
                //    return false;
                //}

                //int notificationclassObjectID = 0;//ErrorCodeUnknownProperty                                                                                                                                       
                //notificationclassObjectID = (int)zoneModel.BACnetData.ObjectDetails.Where(s => s.Value.ObjectType == BacnetObjectType.ObjectNotificationClass).First().Value.ObjectID;
                //isSuccess = StackManager.Instance.WriteProperty((uint)deviceID, BacnetObjectType.ObjectSchedule, (uint)objectID, notificationclassObjectID, BacnetPropertyID.PropNotificationClass);
                //if (!isSuccess)
                //{
                //    Logger.Instance.Log(LogLevel.ERROR, "Exception Recevied at CreateScheduleInstance: WP Schedule Notification Class Failed");
                //    return false;
                //} 

                //BacnetDateRangeModel effectiveModel = new BacnetDateRangeModel();
                //effectiveModel.StartDate.Day = 255;
                //effectiveModel.StartDate.Month = 255;
                //effectiveModel.StartDate.Year = 255;
                //effectiveModel.EndDate.Day = 255;
                //effectiveModel.EndDate.Month = 255;
                //effectiveModel.EndDate.Year = 255;
                //isSuccess = StackManager.Instance.WriteProperty((uint)deviceID, BacnetObjectType.ObjectSchedule, (uint)objectID, effectiveModel, BacnetPropertyID.PropEffectivePeriod);
                //if (!isSuccess)
                //{
                //    Logger.Instance.Log(LogLevel.ERROR, "Exception Recevied at CreateScheduleInstance: WP Schedule EffectivePeriod Failed");
                //    return false;
                //}

                //if (processorConfiguration.BACnetConfigurationData.AlarmNotificationSetting != null)//Error Code Unknown Property
                //{
                //    processorConfiguration.ZoneAlarmNotificationSetting = processorConfiguration.BACnetConfigurationData.AlarmNotificationSetting["User Zone"];
                //}
                //EventTransitionBitsModel eventTransitionBitsModel = GetEventTransitionBit(processorConfiguration.ZoneAlarmNotificationSetting, "Schedule");
                //isSuccess = StackManager.Instance.WriteProperty((uint)deviceID, BacnetObjectType.ObjectSchedule, (uint)objectID, eventTransitionBitsModel, BacnetPropertyID.PropEventEnable);
                //if (!isSuccess)
                //{
                //    Logger.Instance.Log(LogLevel.ERROR, "Exception Recevied at CreateScheduleInstance: WP Schedule EventEnable Failed");
                //    return false;
                //} 
                #endregion

                //weekly Schedule
                List<BacnetTimeValueArrayModel> bacnetTimeValues = new List<BacnetTimeValueArrayModel>();

                for (int i = 1; i <= 7; i++)
                {
                    BacnetTimeValueArrayModel _BacnetTimeValueArrayModel = new BacnetTimeValueArrayModel();
                    _BacnetTimeValueArrayModel.DayCount = i;
                    _BacnetTimeValueArrayModel.BacnetTimeValueList = new List<BacnetTimeValueModel>();
                    bacnetTimeValues.Add(_BacnetTimeValueArrayModel);
                }
                foreach (var timeItem in bacnetTimeValues)
                {
                    foreach (var modelobject in modelList)
                    {
                        BacnetTimeValueModel _BacnetTimeValueModel = new BacnetTimeValueModel();
                        _BacnetTimeValueModel.Time.Hour = Convert.ToByte(modelobject.actionAt.Split(':')[0]);
                        _BacnetTimeValueModel.Time.Min = Convert.ToByte(modelobject.actionAt.Split(':')[1]);
                        _BacnetTimeValueModel.Time.Sec = Convert.ToByte(modelobject.actionAt.Split(':')[2]);
                        _BacnetTimeValueModel.Time.Hundredths = 0;
                        _BacnetTimeValueModel.Values = new List<BacnetValueModel>();
                        #region Switch
                        switch (SCScheduleType)
                        {
                            case ScheduleObjectType.Light_Scene:
                                {
                                    try
                                    {
                                        BacnetValueModel _BacnetValueModel = new BacnetValueModel();
                                        _BacnetValueModel.TagType = (byte)BacnetApplicationTag.BacnetApplicationTagUnsignedInt;
                                        JObject lightScenes = modelobject.availableLightScenes;
                                        List<string> lightsce = new List<string>();
                                        foreach (var item in lightScenes)
                                        {
                                            lightsce.Add(item.Key);
                                        }
                                        _BacnetValueModel.Value = (1 - lightsce.IndexOf(modelobject.lightScene)).ToString();
                                        _BacnetTimeValueModel.Values.Add(_BacnetValueModel);
                                    }
                                    catch (Exception ex)
                                    {
                                        Logger.Instance.Log(ex, LogLevel.ERROR, "Exception Recevied at CreateScheduleInstance: Failed to Update Value for LightScene Object");
                                    }
                                }
                                break;
                            case ScheduleObjectType.Mood:
                                {
                                    try
                                    {
                                        BacnetValueModel _BacnetValueModel = new BacnetValueModel();
                                        _BacnetValueModel.TagType = (byte)BacnetApplicationTag.BacnetApplicationTagUnsignedInt;
                                        _BacnetValueModel.Value = Math.Abs(1 + AppCoreRepo.Instance.AvailableMoods.ToList().IndexOf(modelobject.mood)).ToString();
                                        _BacnetTimeValueModel.Values.Add(_BacnetValueModel);
                                    }
                                    catch (Exception ex)
                                    {
                                        Logger.Instance.Log(ex, LogLevel.ERROR, "Exception Recevied at CreateScheduleInstance: Failed to Update Value for Mood Object");
                                    }
                                }
                                break;
                            case ScheduleObjectType.Palettes:
                                {
                                    try
                                    {
                                        BacnetValueModel _BacnetValueModel = new BacnetValueModel();
                                        _BacnetValueModel.TagType = (byte)BacnetApplicationTag.BacnetApplicationTagUnsignedInt;
                                        _BacnetTimeValueModel.Values.Add(_BacnetValueModel);
                                        //List<string> palettesList = MonitoredDataStackUpdateHelper.GetPalettesList(modelobject.lightScene);
                                        //if (palettesList != null && palettesList.Count > 0 && palettesList.Contains(modelobject.palette))
                                        //    _BacnetValueModel.Value = Math.Abs(1 + palettesList.ToList().IndexOf(modelobject.palette)).ToString();
                                        //_BacnetTimeValueModel.Values.Add(_BacnetValueModel);
                                    }
                                    catch (Exception ex)
                                    {
                                        Logger.Instance.Log(ex, LogLevel.ERROR, "Exception Recevied at CreateScheduleInstance: Failed to Update Value for Mood Object");
                                    }
                                }
                                break;
                            case ScheduleObjectType.Brightness:
                                {
                                    try
                                    {
                                        BacnetValueModel _BacnetValueModel = new BacnetValueModel();
                                        _BacnetValueModel.TagType = (byte)BacnetApplicationTag.BacnetApplicationTagReal;
                                        _BacnetValueModel.Value = modelobject.brightness;
                                        _BacnetTimeValueModel.Values.Add(_BacnetValueModel);
                                    }
                                    catch (Exception ex)
                                    {
                                        Logger.Instance.Log(ex, LogLevel.ERROR, "Exception Recevied at CreateScheduleInstance: Failed to Update Value for Brightness Object");
                                    }
                                }
                                break;
                            case ScheduleObjectType.BioDynamic:
                                {
                                    try
                                    {
                                        BacnetValueModel _BacnetValueModel = new BacnetValueModel();
                                        _BacnetValueModel.TagType = (byte)BacnetApplicationTag.BacnetApplicationTagReal;
                                        _BacnetValueModel.Value = modelobject.biodynamic;
                                        _BacnetTimeValueModel.Values.Add(_BacnetValueModel);
                                    }
                                    catch (Exception ex)
                                    {
                                        Logger.Instance.Log(ex, LogLevel.ERROR, "Exception Recevied at CreateScheduleInstance: Failed to Update Value for biodynamic Object");
                                    }
                                }
                                break;
                            case ScheduleObjectType.Saturation:
                                {
                                    try
                                    {
                                        BacnetValueModel _BacnetValueModel = new BacnetValueModel();
                                        _BacnetValueModel.TagType = (byte)BacnetApplicationTag.BacnetApplicationTagReal;
                                        _BacnetValueModel.Value = modelobject.saturation;
                                        _BacnetTimeValueModel.Values.Add(_BacnetValueModel);
                                    }
                                    catch (Exception ex)
                                    {
                                        Logger.Instance.Log(ex, LogLevel.ERROR, "Exception Recevied at CreateScheduleInstance: Failed to Update Value for saturation Object");
                                    }
                                }
                                break;
                            case ScheduleObjectType.Target_Lux:
                                {
                                    try
                                    {
                                        BacnetValueModel _BacnetValueModel = new BacnetValueModel();
                                        _BacnetValueModel.TagType = (byte)BacnetApplicationTag.BacnetApplicationTagUnsignedInt;
                                        _BacnetValueModel.Value = modelobject.daylightHarvesting["luxAimAt"].Value.ToString();
                                        _BacnetTimeValueModel.Values.Add(_BacnetValueModel);
                                    }
                                    catch (Exception ex)
                                    {
                                        Logger.Instance.Log(ex, LogLevel.ERROR, "Exception Recevied at CreateScheduleInstance: Failed to Update Value for saturation Object");
                                    }
                                }
                                break;
                            case ScheduleObjectType.Min_Brightness:
                                {
                                    try
                                    {
                                        BacnetValueModel _BacnetValueModel = new BacnetValueModel();
                                        _BacnetValueModel.TagType = (byte)BacnetApplicationTag.BacnetApplicationTagReal;
                                        _BacnetValueModel.Value = modelobject.daylightHarvesting["minimumBrightness"].Value.ToString();
                                        _BacnetTimeValueModel.Values.Add(_BacnetValueModel);
                                    }
                                    catch (Exception ex)
                                    {
                                        Logger.Instance.Log(ex, LogLevel.ERROR, "Exception Recevied at CreateScheduleInstance: Failed to Update Value for saturation Object");
                                    }
                                }
                                break;
                            case ScheduleObjectType.Delta_Up:
                                {
                                    try
                                    {
                                        BacnetValueModel _BacnetValueModel = new BacnetValueModel();
                                        _BacnetValueModel.TagType = (byte)BacnetApplicationTag.BacnetApplicationTagUnsignedInt;
                                        double value = 0;
                                        double.TryParse(modelobject.daylightHarvesting["deltaUp"].Value.ToString(), out value);
                                        int setValue = 0;
                                        if (value == 0.4)
                                            setValue = 3;
                                        else if (value == 0.7)
                                            setValue = 2;
                                        else if (value == 1.0)
                                            setValue = 1;
                                        _BacnetValueModel.Value = setValue;
                                        _BacnetTimeValueModel.Values.Add(_BacnetValueModel);
                                    }
                                    catch (Exception ex)
                                    {
                                        Logger.Instance.Log(ex, LogLevel.ERROR, "Exception Recevied at CreateScheduleInstance: Failed to Update Value for saturation Object");
                                    }
                                }
                                break;
                            case ScheduleObjectType.Delta_Down:
                                {
                                    try
                                    {
                                        BacnetValueModel _BacnetValueModel = new BacnetValueModel();
                                        _BacnetValueModel.TagType = (byte)BacnetApplicationTag.BacnetApplicationTagUnsignedInt;
                                        double value = 0;
                                        double.TryParse(modelobject.daylightHarvesting["deltaDown"].Value.ToString(), out value);
                                        int setValue = 0;
                                        if (value == 0.4)
                                            setValue = 3;
                                        else if (value == 0.7)
                                            setValue = 2;
                                        else if (value == 1.0)
                                            setValue = 1;
                                        _BacnetValueModel.Value = setValue;
                                        _BacnetTimeValueModel.Values.Add(_BacnetValueModel);
                                    }
                                    catch (Exception ex)
                                    {
                                        Logger.Instance.Log(ex, LogLevel.ERROR, "Exception Recevied at CreateScheduleInstance: Failed to Update Value for saturation Object");
                                    }
                                }
                                break;
                            case ScheduleObjectType.Avaliable_Light_Scenes:
                                {
                                    try
                                    {
                                        BacnetValueModel _BacnetValueModel = new BacnetValueModel();
                                        _BacnetValueModel.TagType = (byte)BacnetApplicationTag.BacnetApplicationTagCharacterString;
                                        JObject lightScenes = modelobject.availableLightScenes;
                                        List<string> lightsce = new List<string>();
                                        foreach (var item in lightScenes)
                                        {
                                            lightsce.Add(item.Key);
                                        }
                                        _BacnetValueModel.Value = lightsce;
                                        _BacnetTimeValueModel.Values.Add(_BacnetValueModel);
                                    }
                                    catch (Exception ex)
                                    {
                                        Logger.Instance.Log(ex, LogLevel.ERROR, "Exception Recevied at CreateScheduleInstance: Failed to Update Value for saturation Object");
                                    }
                                }
                                break;
                            case ScheduleObjectType.Occupancy_TimeOut:
                                {
                                    try
                                    {
                                        BacnetValueModel _BacnetValueModel = new BacnetValueModel();
                                        _BacnetValueModel.TagType = (byte)BacnetApplicationTag.BacnetApplicationTagUnsignedInt;
                                        if (modelobject.occupancySetting as OccupancySetting != null)
                                            _BacnetValueModel.Value = (modelobject.occupancySetting as OccupancySetting).occupancyTimeOut;
                                        _BacnetTimeValueModel.Values.Add(_BacnetValueModel);
                                    }
                                    catch (Exception ex)
                                    {
                                        Logger.Instance.Log(ex, LogLevel.ERROR, "Exception Recevied at CreateScheduleInstance: Failed to Update Value for saturation Object");
                                    }
                                }
                                break;
                            case ScheduleObjectType.Occupancy_FadeOut:
                                {
                                    try
                                    {
                                        BacnetValueModel _BacnetValueModel = new BacnetValueModel();
                                        _BacnetValueModel.TagType = (byte)BacnetApplicationTag.BacnetApplicationTagUnsignedInt;
                                        if (modelobject.occupancySetting as OccupancySetting != null)
                                            _BacnetValueModel.Value = (modelobject.occupancySetting as OccupancySetting).opacityTimeOut;
                                        _BacnetTimeValueModel.Values.Add(_BacnetValueModel);
                                    }
                                    catch (Exception ex)
                                    {
                                        Logger.Instance.Log(ex, LogLevel.ERROR, "Exception Recevied at CreateScheduleInstance: Failed to Update Value for saturation Object");
                                    }
                                }
                                break;
                            case ScheduleObjectType.Zone_On_Off:
                                {
                                    try
                                    {
                                        BacnetValueModel _BacnetValueModel = new BacnetValueModel();
                                        _BacnetValueModel.TagType = (byte)BacnetApplicationTag.BacnetApplicationTagUnsignedInt;
                                        if (modelobject.occupancySetting as OccupancySetting != null)
                                            _BacnetValueModel.Value = (modelobject.occupancySetting as OccupancySetting).opacityValue;
                                        _BacnetTimeValueModel.Values.Add(_BacnetValueModel);
                                    }
                                    catch (Exception ex)
                                    {
                                        Logger.Instance.Log(ex, LogLevel.ERROR, "Exception Recevied at CreateScheduleInstance: Failed to Update Value for saturation Object");
                                    }
                                }
                                break;
                            case ScheduleObjectType.Shading_Level:
                                {
                                    try
                                    {
                                        BacnetValueModel _BacnetValueModel = new BacnetValueModel();
                                        _BacnetValueModel.TagType = (byte)BacnetApplicationTag.BacnetApplicationTagUnsignedInt;
                                        _BacnetValueModel.Value = modelobject.globalSetTo;// (1 - zoneModel.LightScene.IndexOf(modelobject.result.data[0].response[0].lightScene)).ToString();
                                        _BacnetTimeValueModel.Values.Add(_BacnetValueModel);
                                    }
                                    catch (Exception ex)
                                    {
                                        Logger.Instance.Log(ex, LogLevel.ERROR, "Exception Recevied at CreateScheduleInstance: Failed to Update Value for LightScene Object");
                                    }
                                }
                                break;
                            default:
                                break;
                        }
                        #endregion
                        timeItem.BacnetTimeValueList.Add(_BacnetTimeValueModel);
                    }
                }


                isSuccess = StackManager.Instance.WriteProperty((uint)deviceID, BacnetObjectType.ObjectSchedule, (uint)objectID, bacnetTimeValues, BacnetPropertyID.PropWeeklySchedule);
                if (!isSuccess)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "Exception Recevied at CreateScheduleInstance: WP Schedule WeeklySchedule Failed");
                    return false;
                }


                //ListOfObjectPropertyRef
                List<BACnetDevObjPropRefModel> bacnetDevObjPropRef = new List<BACnetDevObjPropRefModel>();

                #region List Of Object Property Ref Mapping
                switch (SCScheduleType)
                {
                    case ScheduleObjectType.Light_Scene:
                        {
                            try
                            {
                                BACnetDevObjPropRefModel _BACnetDevObjPropRefModel = new BACnetDevObjPropRefModel();
                                _BACnetDevObjPropRefModel.DeviceIdPresent = false;
                                _BACnetDevObjPropRefModel.ObjectType = BacnetObjectType.ObjectMultiStateValue;
                                _BACnetDevObjPropRefModel.ObjId = zoneModel.BACnetData.ObjectDetails[BacnetObjectType.ObjectMultiStateValue + "_" + AppConstants.LIGHTING_LIGHTSCENE_OBJECT].ObjectID;
                                _BACnetDevObjPropRefModel.ArrIndxPresent = false;
                                _BACnetDevObjPropRefModel.PropertyIdentifier = BacnetPropertyID.PropPresentValue;
                                bacnetDevObjPropRef.Add(_BACnetDevObjPropRefModel);
                            }
                            catch (Exception ex)
                            {
                                Logger.Instance.Log(ex, LogLevel.ERROR, "Exception Recevied at CreateScheduleInstance: Failed to Update Object Ref for Light_Scene Object");
                                StackManager.Instance.RemoveObject((uint)deviceID, BacnetObjectType.ObjectSchedule, (uint)objectID);
                            }
                        }
                        break;
                    case ScheduleObjectType.Mood:
                        {
                            try
                            {
                                if (zoneModel.BACnetData.ObjectDetails.ContainsKey(BacnetObjectType.ObjectMultiStateValue + "_" + AppConstants.LIGHTING_MOOD_OBJECT))
                                {
                                    BACnetDevObjPropRefModel _BACnetDevObjPropRefModel = new BACnetDevObjPropRefModel();
                                    _BACnetDevObjPropRefModel.DeviceIdPresent = false;
                                    _BACnetDevObjPropRefModel.ObjectType = BacnetObjectType.ObjectMultiStateValue;
                                    _BACnetDevObjPropRefModel.ObjId = zoneModel.BACnetData.ObjectDetails[BacnetObjectType.ObjectMultiStateValue + "_" + AppConstants.LIGHTING_MOOD_OBJECT].ObjectID;
                                    _BACnetDevObjPropRefModel.ArrIndxPresent = false;
                                    _BACnetDevObjPropRefModel.PropertyIdentifier = BacnetPropertyID.PropPresentValue;
                                    bacnetDevObjPropRef.Add(_BACnetDevObjPropRefModel);
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.Instance.Log(ex, LogLevel.ERROR, "Exception Recevied at CreateScheduleInstance: Failed to Update Object Ref for Mood Object");
                                StackManager.Instance.RemoveObject((uint)deviceID, BacnetObjectType.ObjectSchedule, (uint)objectID);
                            }
                        }
                        break;
                    case ScheduleObjectType.Palettes:
                        {
                            try
                            {
                                if (zoneModel.BACnetData.ObjectDetails.ContainsKey("ObjectMultiStateValue_BeaconPalettes"))
                                {
                                    BACnetDevObjPropRefModel _BACnetDevObjPropRefModel = new BACnetDevObjPropRefModel();
                                    _BACnetDevObjPropRefModel.DeviceIdPresent = false;
                                    _BACnetDevObjPropRefModel.ObjectType = BacnetObjectType.ObjectMultiStateValue;
                                    _BACnetDevObjPropRefModel.ObjId = zoneModel.BACnetData.ObjectDetails["ObjectMultiStateValue_BeaconPalettes"].ObjectID;
                                    _BACnetDevObjPropRefModel.ArrIndxPresent = false;
                                    _BACnetDevObjPropRefModel.PropertyIdentifier = BacnetPropertyID.PropPresentValue;
                                    bacnetDevObjPropRef.Add(_BACnetDevObjPropRefModel);
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.Instance.Log(ex, LogLevel.ERROR, "Exception Recevied at CreateScheduleInstance: Failed to Update Object Ref for Mood Object");
                                StackManager.Instance.RemoveObject((uint)deviceID, BacnetObjectType.ObjectSchedule, (uint)objectID);
                            }
                        }
                        break;
                    case ScheduleObjectType.Brightness:
                        {
                            try
                            {
                                BACnetDevObjPropRefModel _BACnetDevObjPropRefModel = new BACnetDevObjPropRefModel();
                                _BACnetDevObjPropRefModel.DeviceIdPresent = false;
                                _BACnetDevObjPropRefModel.ObjectType = BacnetObjectType.ObjectAnalogValue;
                                _BACnetDevObjPropRefModel.ObjId = zoneModel.BACnetData.ObjectDetails[BacnetObjectType.ObjectAnalogValue + "_" + AppConstants.LIGHTING_BRIGHTENESS_OBJECT].ObjectID;
                                _BACnetDevObjPropRefModel.ArrIndxPresent = false;
                                _BACnetDevObjPropRefModel.PropertyIdentifier = BacnetPropertyID.PropPresentValue;
                                bacnetDevObjPropRef.Add(_BACnetDevObjPropRefModel);
                            }
                            catch (Exception ex)
                            {
                                Logger.Instance.Log(ex, LogLevel.ERROR, "Exception Recevied at CreateScheduleInstance: Failed to Update Object Ref for Brightness Object");
                                StackManager.Instance.RemoveObject((uint)deviceID, BacnetObjectType.ObjectSchedule, (uint)objectID);
                            }
                        }
                        break;
                    case ScheduleObjectType.BioDynamic:
                        {
                            try
                            {
                                BACnetDevObjPropRefModel _BACnetDevObjPropRefModel = new BACnetDevObjPropRefModel();
                                _BACnetDevObjPropRefModel.DeviceIdPresent = false;
                                _BACnetDevObjPropRefModel.ObjectType = BacnetObjectType.ObjectAnalogValue;
                                _BACnetDevObjPropRefModel.ObjId = zoneModel.BACnetData.ObjectDetails[BacnetObjectType.ObjectAnalogValue + "_" + AppResource.BioDynamicControlName].ObjectID;
                                _BACnetDevObjPropRefModel.ArrIndxPresent = false;
                                _BACnetDevObjPropRefModel.PropertyIdentifier = BacnetPropertyID.PropPresentValue;
                                bacnetDevObjPropRef.Add(_BACnetDevObjPropRefModel);
                            }
                            catch (Exception ex)
                            {
                                Logger.Instance.Log(ex, LogLevel.ERROR, "Exception Recevied at CreateScheduleInstance: Failed to Update Object Ref for BioDynamic Object");
                                StackManager.Instance.RemoveObject((uint)deviceID, BacnetObjectType.ObjectSchedule, (uint)objectID);
                            }
                        }
                        break;
                    case ScheduleObjectType.Saturation:
                        {
                            try
                            {
                                BACnetDevObjPropRefModel _BACnetDevObjPropRefModel = new BACnetDevObjPropRefModel();
                                _BACnetDevObjPropRefModel.DeviceIdPresent = false;
                                _BACnetDevObjPropRefModel.ObjectType = BacnetObjectType.ObjectAnalogValue;
                                _BACnetDevObjPropRefModel.ObjId = zoneModel.BACnetData.ObjectDetails[BacnetObjectType.ObjectAnalogValue + "_" + AppResource.SaturationControlName].ObjectID;
                                _BACnetDevObjPropRefModel.ArrIndxPresent = false;
                                _BACnetDevObjPropRefModel.PropertyIdentifier = BacnetPropertyID.PropPresentValue;
                                bacnetDevObjPropRef.Add(_BACnetDevObjPropRefModel);
                            }
                            catch (Exception ex)
                            {
                                Logger.Instance.Log(ex, LogLevel.ERROR, "Exception Recevied at CreateScheduleInstance: Failed to Update Object Ref for Saturation Object");
                                StackManager.Instance.RemoveObject((uint)deviceID, BacnetObjectType.ObjectSchedule, (uint)objectID);
                            }
                        }
                        break;
                    case ScheduleObjectType.Target_Lux:
                        {
                            try
                            {
                                BACnetDevObjPropRefModel _BACnetDevObjPropRefModel = new BACnetDevObjPropRefModel();
                                _BACnetDevObjPropRefModel.DeviceIdPresent = false;
                                _BACnetDevObjPropRefModel.ObjectType = BacnetObjectType.ObjectPositiveIntegerValue;
                                _BACnetDevObjPropRefModel.ObjId = zoneModel.BACnetData.ObjectDetails["ObjectPositiveIntegerValue_DHTargetLux"].ObjectID;
                                _BACnetDevObjPropRefModel.ArrIndxPresent = false;
                                _BACnetDevObjPropRefModel.PropertyIdentifier = BacnetPropertyID.PropPresentValue;
                                bacnetDevObjPropRef.Add(_BACnetDevObjPropRefModel);
                            }
                            catch (Exception ex)
                            {
                                Logger.Instance.Log(ex, LogLevel.ERROR, "Exception Recevied at CreateScheduleInstance: Failed to Update Object Ref for Saturation Object");
                                StackManager.Instance.RemoveObject((uint)deviceID, BacnetObjectType.ObjectSchedule, (uint)objectID);
                            }
                        }
                        break;
                    case ScheduleObjectType.Min_Brightness:
                        {
                            try
                            {
                                BACnetDevObjPropRefModel _BACnetDevObjPropRefModel = new BACnetDevObjPropRefModel();
                                _BACnetDevObjPropRefModel.DeviceIdPresent = false;
                                _BACnetDevObjPropRefModel.ObjectType = BacnetObjectType.ObjectAnalogValue;
                                //_BACnetDevObjPropRefModel.ObjId = zoneModel.BACnetData.ObjectDetails[BacnetObjectType.ObjectAnalogValue + "_" + AppConstants.LIGHTING_MIN_BRIGHTNESS_OBJECT].ObjectID;
                                _BACnetDevObjPropRefModel.ArrIndxPresent = false;
                                _BACnetDevObjPropRefModel.PropertyIdentifier = BacnetPropertyID.PropPresentValue;
                                bacnetDevObjPropRef.Add(_BACnetDevObjPropRefModel);
                            }
                            catch (Exception ex)
                            {
                                Logger.Instance.Log(ex, LogLevel.ERROR, "Exception Recevied at CreateScheduleInstance: Failed to Update Object Ref for Saturation Object");
                                StackManager.Instance.RemoveObject((uint)deviceID, BacnetObjectType.ObjectSchedule, (uint)objectID);
                            }
                        }
                        break;
                    case ScheduleObjectType.Delta_Up:
                        {
                            try
                            {
                                //if (zoneModel.BACnetData.ObjectDetails.ContainsKey(BacnetObjectType.ObjectMultiStateValue + "_" + AppConstants.LIGHTING_DELTA_UP_OBJECT))
                                //{
                                //    BACnetDevObjPropRefModel _BACnetDevObjPropRefModel = new BACnetDevObjPropRefModel();
                                //    _BACnetDevObjPropRefModel.DeviceIdPresent = false;
                                //    _BACnetDevObjPropRefModel.ObjectType = BacnetObjectType.ObjectMultiStateValue;
                                //    _BACnetDevObjPropRefModel.ObjId = zoneModel.BACnetData.ObjectDetails[BacnetObjectType.ObjectMultiStateValue + "_" + AppConstants.LIGHTING_DELTA_UP_OBJECT].ObjectID;
                                //    _BACnetDevObjPropRefModel.ArrIndxPresent = false;
                                //    _BACnetDevObjPropRefModel.PropertyIdentifier = BacnetPropertyID.PropPresentValue;
                                //    bacnetDevObjPropRef.Add(_BACnetDevObjPropRefModel);
                                //}
                            }
                            catch (Exception ex)
                            {
                                Logger.Instance.Log(ex, LogLevel.ERROR, "Exception Recevied at CreateScheduleInstance: Failed to Update Object Ref for Mood Object");
                                StackManager.Instance.RemoveObject((uint)deviceID, BacnetObjectType.ObjectSchedule, (uint)objectID);
                            }
                        }
                        break;
                    case ScheduleObjectType.Delta_Down:
                        {
                            try
                            {
                                //if (zoneModel.BACnetData.ObjectDetails.ContainsKey(BacnetObjectType.ObjectMultiStateValue + "_" + AppConstants.LIGHTING_DELTA_DOWN_OBJECT))
                                //{
                                //    BACnetDevObjPropRefModel _BACnetDevObjPropRefModel = new BACnetDevObjPropRefModel();
                                //    _BACnetDevObjPropRefModel.DeviceIdPresent = false;
                                //    _BACnetDevObjPropRefModel.ObjectType = BacnetObjectType.ObjectMultiStateValue;
                                //    _BACnetDevObjPropRefModel.ObjId = zoneModel.BACnetData.ObjectDetails[BacnetObjectType.ObjectMultiStateValue + "_" + AppConstants.LIGHTING_DELTA_DOWN_OBJECT].ObjectID;
                                //    _BACnetDevObjPropRefModel.ArrIndxPresent = false;
                                //    _BACnetDevObjPropRefModel.PropertyIdentifier = BacnetPropertyID.PropPresentValue;
                                //    bacnetDevObjPropRef.Add(_BACnetDevObjPropRefModel);
                                //}
                            }
                            catch (Exception ex)
                            {
                                Logger.Instance.Log(ex, LogLevel.ERROR, "Exception Recevied at CreateScheduleInstance: Failed to Update Object Ref for Mood Object");
                                StackManager.Instance.RemoveObject((uint)deviceID, BacnetObjectType.ObjectSchedule, (uint)objectID);
                            }
                        }
                        break;
                    case ScheduleObjectType.Avaliable_Light_Scenes:
                        {
                            try
                            {
                                BACnetDevObjPropRefModel _BACnetDevObjPropRefModel = new BACnetDevObjPropRefModel();
                                _BACnetDevObjPropRefModel.DeviceIdPresent = false;
                                _BACnetDevObjPropRefModel.ObjectType = BacnetObjectType.ObjectMultiStateValue;
                                _BACnetDevObjPropRefModel.ObjId = zoneModel.BACnetData.ObjectDetails["ObjectMultiStateValue_LightScene"].ObjectID;
                                _BACnetDevObjPropRefModel.ArrIndxPresent = false;
                                _BACnetDevObjPropRefModel.PropertyIdentifier = BacnetPropertyID.PropStateText;
                                bacnetDevObjPropRef.Add(_BACnetDevObjPropRefModel);
                            }
                            catch (Exception ex)
                            {
                                Logger.Instance.Log(ex, LogLevel.ERROR, "Exception Recevied at CreateScheduleInstance: Failed to Update Object Ref for Saturation Object");
                                StackManager.Instance.RemoveObject((uint)deviceID, BacnetObjectType.ObjectSchedule, (uint)objectID);
                            }
                        }
                        break;
                    case ScheduleObjectType.Occupancy_TimeOut:
                        {
                            try
                            {
                                BACnetDevObjPropRefModel _BACnetDevObjPropRefModel = new BACnetDevObjPropRefModel();
                                _BACnetDevObjPropRefModel.DeviceIdPresent = false;
                                _BACnetDevObjPropRefModel.ObjectType = BacnetObjectType.ObjectPositiveIntegerValue;
                                _BACnetDevObjPropRefModel.ObjId = zoneModel.BACnetData.ObjectDetails["ObjectPositiveIntegerValue_OccupancyTimeOut"].ObjectID;
                                _BACnetDevObjPropRefModel.ArrIndxPresent = false;
                                _BACnetDevObjPropRefModel.PropertyIdentifier = BacnetPropertyID.PropPresentValue;
                                bacnetDevObjPropRef.Add(_BACnetDevObjPropRefModel);
                            }
                            catch (Exception ex)
                            {
                                Logger.Instance.Log(ex, LogLevel.ERROR, "Exception Recevied at CreateScheduleInstance: Failed to Update Object Ref for Saturation Object");
                                StackManager.Instance.RemoveObject((uint)deviceID, BacnetObjectType.ObjectSchedule, (uint)objectID);
                            }
                        }
                        break;
                    case ScheduleObjectType.Occupancy_FadeOut:
                        {
                            try
                            {
                                BACnetDevObjPropRefModel _BACnetDevObjPropRefModel = new BACnetDevObjPropRefModel();
                                _BACnetDevObjPropRefModel.DeviceIdPresent = false;
                                _BACnetDevObjPropRefModel.ObjectType = BacnetObjectType.ObjectPositiveIntegerValue;
                                _BACnetDevObjPropRefModel.ObjId = zoneModel.BACnetData.ObjectDetails["ObjectPositiveIntegerValue_OccupancyFadeOutTime"].ObjectID;
                                _BACnetDevObjPropRefModel.ArrIndxPresent = false;
                                _BACnetDevObjPropRefModel.PropertyIdentifier = BacnetPropertyID.PropPresentValue;
                                bacnetDevObjPropRef.Add(_BACnetDevObjPropRefModel);
                            }
                            catch (Exception ex)
                            {
                                Logger.Instance.Log(ex, LogLevel.ERROR, "Exception Recevied at CreateScheduleInstance: Failed to Update Object Ref for Saturation Object");
                                StackManager.Instance.RemoveObject((uint)deviceID, BacnetObjectType.ObjectSchedule, (uint)objectID);
                            }
                        }
                        break;
                    case ScheduleObjectType.Zone_On_Off:
                        {
                            try
                            {
                                BACnetDevObjPropRefModel _BACnetDevObjPropRefModel = new BACnetDevObjPropRefModel();
                                _BACnetDevObjPropRefModel.DeviceIdPresent = false;
                                _BACnetDevObjPropRefModel.ObjectType = BacnetObjectType.ObjectBinaryValue;
                                _BACnetDevObjPropRefModel.ObjId = zoneModel.BACnetData.ObjectDetails["ObjectBinaryValue_ZoneState"].ObjectID;
                                _BACnetDevObjPropRefModel.ArrIndxPresent = false;
                                _BACnetDevObjPropRefModel.PropertyIdentifier = BacnetPropertyID.PropPresentValue;
                                bacnetDevObjPropRef.Add(_BACnetDevObjPropRefModel);
                            }
                            catch (Exception ex)
                            {
                                Logger.Instance.Log(ex, LogLevel.ERROR, "Exception Recevied at CreateScheduleInstance: Failed to Update Object Ref for Saturation Object");
                                StackManager.Instance.RemoveObject((uint)deviceID, BacnetObjectType.ObjectSchedule, (uint)objectID);
                            }
                        }
                        break;
                    case ScheduleObjectType.Shading_Level:
                        {
                            try
                            {
                                if (zoneModel.BACnetData.ObjectDetails.ContainsKey("ObjectAnalogValue_Master Shading Level"))
                                {
                                    BACnetDevObjPropRefModel _BACnetDevObjPropRefModel = new BACnetDevObjPropRefModel();
                                    _BACnetDevObjPropRefModel.DeviceIdPresent = false;
                                    _BACnetDevObjPropRefModel.ObjectType = BacnetObjectType.ObjectMultiStateValue;
                                    _BACnetDevObjPropRefModel.ObjId = zoneModel.BACnetData.ObjectDetails["ObjectAnalogValue_Master Shading Level"].ObjectID;
                                    _BACnetDevObjPropRefModel.ArrIndxPresent = false;
                                    _BACnetDevObjPropRefModel.PropertyIdentifier = BacnetPropertyID.PropPresentValue;
                                    bacnetDevObjPropRef.Add(_BACnetDevObjPropRefModel);
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.Instance.Log(ex, LogLevel.ERROR, "Exception Recevied at CreateScheduleInstance: Failed to Update Object Ref for Mood Object");
                                StackManager.Instance.RemoveObject((uint)deviceID, BacnetObjectType.ObjectSchedule, (uint)objectID);
                            }
                        }
                        break;
                    default:
                        break;
                } 
                #endregion

                isSuccess = StackManager.Instance.WriteProperty((uint)deviceID, BacnetObjectType.ObjectSchedule, (uint)objectID, bacnetDevObjPropRef, BacnetPropertyID.PropListOfObjectPropertyReferences);
                if (!isSuccess)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "Exception Recevied at CreateScheduleInstance: WP Schedule WeeklySchedule Failed");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "Exception Recevied at CreateScheduleInstance:");
                return false;
            }
        }

        /// <summary>
        /// Gets the event transition bit.
        /// </summary>
        /// <returns></returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy>
        /// <TimeStamp>4/11/20186:36 PM</TimeStamp>
        private EventTransitionBitsModel GetEventTransitionBit(List<ZoneObject> AlarmNotificationSetting, string entity)
        {
            EventTransitionBitsModel eventTransitionBitsModel = new EventTransitionBitsModel();
            if (AlarmNotificationSetting.Any(x => x.Name == entity && x.ZoneValue))
            {
                eventTransitionBitsModel.ToFault = true;
                eventTransitionBitsModel.ToNormal = true;
                eventTransitionBitsModel.ToOffNormal = true;
            }
            else
            {
                eventTransitionBitsModel.ToFault = false;
                eventTransitionBitsModel.ToNormal = false;
                eventTransitionBitsModel.ToOffNormal = false;
            }

            return eventTransitionBitsModel;
        }

        /// <summary>
        /// Sets the type of the property access.
        /// </summary>
        /// <param name="deviceId">The device identifier.</param>
        /// <param name="objectId">The object identifier.</param>
        /// <param name="bacnetObjectType">Type of the bacnet object.</param>
        /// <param name="bacnetPropertyID">The bacnet property identifier.</param>
        /// <param name="accessType">Type of the access.</param>
        /// <param name="isReadOrWrite">if set to <c>true</c> [is read or write].</param>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>12/27/201712:42 PM</TimeStamp>
        protected void SetPropertyAccessType(uint deviceId, uint objectId, BacnetObjectType bacnetObjectType, BacnetPropertyID bacnetPropertyID, PropAccessType accessType, bool isReadOrWrite)
        {
            StackManager.SetPropertyAccessType(deviceId, objectId, bacnetObjectType, bacnetPropertyID, accessType, isReadOrWrite);
        }
    
        /// <summary>
        /// This Method will update the Schedule Object Reliability Property
        /// </summary>
        /// <param name="value"></param>
        /// <param name="zoneModel"></param>
        public void SetScheduleReliability(string value , EntityBaseModel zoneModel)
        {
            try
            {
                BacnetReliability bacnetReliability = BacnetReliability.ReliabilityNoFaultDetected;
                AppConstants.ReliabilityStatus reliabilityStatus;

                if (Enum.TryParse(value, true, out reliabilityStatus))
                {
                    switch (reliabilityStatus)
                    {
                        case AppConstants.ReliabilityStatus.Good:
                            bacnetReliability = BacnetReliability.ReliabilityNoFaultDetected;
                            break;
                        case AppConstants.ReliabilityStatus.Warning:
                        case AppConstants.ReliabilityStatus.Critical:
                            bacnetReliability = BacnetReliability.ReliabilityUnreliableOther;
                            break;
                        case AppConstants.ReliabilityStatus.NoSensor:
                            bacnetReliability = BacnetReliability.ReliabilityNoSensor;
                            break;
                        default:
                            bacnetReliability = BacnetReliability.ReliabilityConfigurationError;
                            return;
                    }
                }
                if (zoneModel == null)
                    return;
                if (SCScheduleList == null)
                    return;
                foreach (var item in SCScheduleList)
                {
                    if (item == null)
                        continue;
                    if(item.ZoneID == zoneModel.EntityKey)
                    {
                        foreach (var bacnetItem in item.ObjectCollection)
                        {
                            bool isSuccess = StackManager.Instance.WriteProperty((uint)bacnetItem.DeviceID, BacnetObjectType.ObjectSchedule, (uint)bacnetItem.ObjectID, bacnetReliability, BacnetPropertyID.PropReliability);
                            if (!isSuccess)
                            {
                                Logger.Instance.Log(LogLevel.ERROR, "Exception Recevied at CreateScheduleInstance: WP Schedule Reliability Failed");
                                return;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
               Logger.Instance.Log(ex, LogLevel.DEBUG, "Exception Recevied at SetScheduleReliability:");
            }
        }

        /// <summary>
        /// This Method will handle the BACnet Schedule object creation
        /// </summary>
        /// <returns></returns>
        public bool HandleScheduleObjectCreation(CreateObjectNotification createObjectNotificationResponse)
        {
            return false;
        }

        /// <summary>
        /// This Method will handle the BACnet Schedule Object deletion
        /// </summary>
        /// <returns></returns>
        public bool HandleScheduleObjectDeletion(DeleteObjectNotification deleteObjectNotification)
        {
            return false;
        }
    }

    public enum ScheduleObjectType
    {
        Light_Scene,
        Mood,
        Palettes,
        Brightness,
        BioDynamic,
        Saturation,
        Target_Lux,
        Min_Brightness,
        Delta_Up,
        Delta_Down,
        Avaliable_Light_Scenes,
        Occupancy_TimeOut,
        Occupancy_FadeOut,
        Zone_On_Off,
        Shading_Level
    }

    #region SCSchedule Support Class
    public class SCScheduleModel
    {
        public List<SCScheduleResponse> SCScheduleRoot { get; set; }
        public string ZoneID { get; set; }
        public AppConstants.ZoneTypes ZoneType { get; set; }
        public List<SCObjectModel> ObjectCollection { get; set; }
    }

    public class SCObjectModel
    {
        public int DeviceID { get; set; }
        public int ObjectID { get; set; }
        public ScheduleObjectType ScheduleObjectTypeProvided { get; set; }

        public SCObjectModel(int dID, int oID, ScheduleObjectType type)
        {
            DeviceID = dID;
            ObjectID = oID;
            ScheduleObjectTypeProvided = type;
        }
    }

    public class DaylightHarvesting
    {
        public int luxAimAt { get; set; }
        public int minimumBrightness { get; set; }
        public int deltaUp { get; set; }
        public double deltaDown { get; set; }
    }

    public class AvailableLightScenes
    {
        public string personal { get; set; }
        public string speaker { get; set; }
        public string daylight { get; set; }
        public string orbit { get; set; }
        public string meeting { get; set; }
        public string creative { get; set; }
        public string video { get; set; }
        public string slowFlash { get; set; }
        public string fastFlash { get; set; }
        public string slowPulse { get; set; }
        public string fastPulse { get; set; }
    }

    public class OccupancySetting
    {
        public string onIdle { get; set; }
        public string onOccupancy { get; set; }
        public int occupancyTimeOut { get; set; }
        public int opacityValue { get; set; }
        public int opacityTimeOut { get; set; }
    }

    public class SCScheduleResponse
    {
        public string buildingId { get; set; }
        public string floorId { get; set; }
        public string projectId { get; set; }
        public string zoneId { get; set; }
        public string scheduleName { get; set; }
        public string layer { get; set; }
        public string actionAt { get; set; }
        public string lightScene { get; set; }
        public string mood { get; set; }
        public string palette { get; set; }
        public int brightness { get; set; }
        public int biodynamic { get; set; }
        public int saturation { get; set; }
        public dynamic daylightHarvesting { get; set; }
        public dynamic availableLightScenes { get; set; }
        public dynamic occupancySetting { get; set; }
        public bool enabled { get; set; }
        public int globalSetTo { get; set; }
    }

    public class SCScheduleDatum
    {
        public bool isSuccess { get; set; }
        public List<SCScheduleResponse> response { get; set; }
    }

    public class SCScheduleResult
    {
        public List<SCScheduleDatum> data { get; set; }
        public List<object> error { get; set; }
    }

    public class SCScheduleRootObject
    {
        public bool status { get; set; }
        public SCScheduleResult result { get; set; }
        public int code { get; set; }
        public string message { get; set; }
    }
    #endregion
}
