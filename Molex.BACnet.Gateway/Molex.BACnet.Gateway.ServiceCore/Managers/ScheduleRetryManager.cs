﻿using Molex.BACnet.Gateway.Log;
using Molex.BACnet.Gateway.ServiceCore.BACnetHandlers;
using Molex.BACnet.Gateway.ServiceCore.Constants;
using Molex.BACnet.Gateway.ServiceCore.Helpers;
using Molex.BACnet.Gateway.ServiceCore.Models;
using Molex.BACnet.Gateway.ServiceCore.Repository;
using Molex.BACnet.Gateway.ServiceCore.SingletonCore;
using Molex.BACnetStackIntegration.StackManagedApi;
using Molex.StackDataObjects.APIModels;
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Response.AutoResponses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Molex.BACnet.Gateway.ServiceCore.Managers
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Molex.BACnet.Gateway.ServiceCore.SingletonCore.SingletonQueueBase{Molex.BACnet.Gateway.ServiceCore.Managers.ScheduleRetryManager,Molex.StackDataObjects.APIModels.WritePropertyRequestModel}" />
    /// <CreatedBy>hp</CreatedBy>
    /// <TimeStamp>9/15/20174:29 PM</TimeStamp>
    internal class ScheduleRetryManager : SingletonQueueBase<ScheduleRetryManager, ScheduleRetryModel>
    {
        #region Private Fields

        private Timer _timer;
        private bool _isOperationInProgress;
        private static readonly object _syncLock = new object();
        #endregion

        Dictionary<string, RetryRequestModel> _pendingResponses;

        protected override void PreInit()
        {
            _timer.Enabled = true;
            base.PreInit();
        }

        protected override void ConstructorInit()
        {
            _pendingResponses = new Dictionary<string, RetryRequestModel>(); //new List<RetryRequestModel>();
            if (_timer != null)
            {
                _timer.Elapsed -= ElapsedTime;
                _timer.Dispose();
            }
            _timer = new Timer();
            // timer of five second is interval
            _timer.Interval = 5000;
            _timer.Elapsed += ElapsedTime;
            base.ConstructorInit();

        }

        protected override void PreDeInit()
        {
            if (_timer != null)
            {
                _timer.Stop();
                _timer.Close();
                _timer = null;
            }

            Logger.Instance.Log(LogLevel.DEBUG, "ScheduleRetryManager.DeInit, De-Initialization Called");
            base.PreDeInit();
        }

        public override void Process(ScheduleRetryModel queueItem)
        {
            lock (_syncLock)
            {
                string key = GetRetryKey(queueItem.WritePropertyAutoResponse);
                if (queueItem.IsHttpResponseSuccess)
                {
                    _pendingResponses.Remove(key);
                }
                else
                {
                    RetryRequestModel model = new RetryRequestModel();
                    model.WritePropertyAutoResponse = queueItem.WritePropertyAutoResponse;
                    _pendingResponses[key] = model;
                }
            }
        }

        /// <summary>
        /// Occurs after the specified interval in timer is elapsed.
        /// </summary>
        /// <CreatedBy>Rohit Galgali</CreatedBy>
        /// <TimeStamp>5/26/201710:51 AM</TimeStamp>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ElapsedTime(object sender, ElapsedEventArgs e)
        {
            if (_isOperationInProgress)
            {
                return;
            }

            _isOperationInProgress = true;

            lock (_syncLock)
            {
                List<string> removeItems = new List<string>();

                foreach (RetryRequestModel model in _pendingResponses.Values)
                {
                    if (IsRetryStop(model.CurrentCount))
                    {
                        Logger.Instance.Log(LogLevel.DEBUG, "Retry stop at:" + model.CurrentCount * 5 + "Seconds");
                        continue;
                    }

                    if (IsEventOccurred(model.CurrentCount))
                    {
                        model.WritePropertyAutoResponse.WritePropertyCallbackData.BacnetAddress.IPAddress = "127001"; //Setting dummy ip address to bypass the retry mechanism.
                        model.WritePropertyAutoResponse.IsSucceed = true;
                        WritePropertyHandler.OnWriteProperty(model.WritePropertyAutoResponse);

                        Logger.Instance.Log(LogLevel.DEBUG, "Retry at:" + model.CurrentCount * 5 + "Seconds");
                        ResultModel<string> isSuccess = new ResultModel<string>();

                        if (model.WritePropertyAutoResponse.IsSucceed)
                        {
                            WritePropertyNotificationResponse writePropertyNotificationResponse = model.WritePropertyAutoResponse as WritePropertyNotificationResponse;

                            StackManager.Instance.WriteProperty((uint)writePropertyNotificationResponse.WritePropertyCallbackData.DeviceId,
                                                                 writePropertyNotificationResponse.WritePropertyCallbackData.ObjectType,
                                                                 writePropertyNotificationResponse.WritePropertyCallbackData.ObjectInstance,
                                                                 writePropertyNotificationResponse.WritePropertyCallbackData.PropertyValue,
                                                                 writePropertyNotificationResponse.WritePropertyCallbackData.ObjectProperty,
                                                                 writePropertyNotificationResponse.WritePropertyCallbackData.ArrayIndex,
                                                                 writePropertyNotificationResponse.WritePropertyCallbackData.Priority,
                                                                 writePropertyNotificationResponse.WritePropertyCallbackData.ArrayIndexPresent,
                                                                 writePropertyNotificationResponse.WritePropertyCallbackData.DataType);

                            removeItems.Add(GetRetryKey(writePropertyNotificationResponse));
                            Logger.Instance.Log(LogLevel.DEBUG, "Retry succeded.");
                        }
                        else
                        {
                            model.CurrentCount++;
                            Logger.Instance.Log(LogLevel.DEBUG, "Retry failed, trying for next retry.");
                        }
                    }
                    else
                    {
                        model.CurrentCount++;
                    }
                }

                if (removeItems.Count > 0)
                {
                    foreach (string key in removeItems)
                    {
                        _pendingResponses.Remove(key);
                    }
                }
            }
            _isOperationInProgress = false;
        }

        private bool IsEventOccurred(int currentCount)
        {
            switch (currentCount)
            {
                case 0:
                case 1:
                case 3:
                case 4:
                case 6:
                case 7:
                case 8:
                    return false;
                case 2:
                case 5:
                case 9:
                    return true;
                case 10:
                    break;
            }

            return false;
        }

        private bool IsRetryStop(int currentCount)
        {
            if (currentCount > 9)
                return true;
            else
                return false;
        }

        internal void ProcessRetry(WritePropertyNotificationResponse writePropertyRequestModel, bool isHttpResponseSuccess)
        {
            ScheduleRetryModel model = new ScheduleRetryModel() { WritePropertyAutoResponse = writePropertyRequestModel, IsHttpResponseSuccess = isHttpResponseSuccess };
            base.Enque(model);
        }

        private string GetRetryKey(WritePropertyNotificationResponse model)
        {
            return string.Format("{0}_{1}_{2}", model.WritePropertyCallbackData.DeviceId, model.WritePropertyCallbackData.ObjectType, model.WritePropertyCallbackData.ObjectInstance);
        }
    }
}
