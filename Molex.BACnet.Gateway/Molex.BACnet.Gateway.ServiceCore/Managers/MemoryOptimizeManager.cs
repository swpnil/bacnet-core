﻿using Molex.BACnet.Gateway.Log;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Timers;

namespace Molex.BACnet.Gateway.ServiceCore.Managers
{
    /// <summary>
    /// This Class will optimize Memory Consumption for BACnet Gateway Service
    /// </summary>
    public class MemoryOptimizeManager
    {
        #region Private Members
        private static MemoryOptimizeManager _MemoryOptimizeManager;
        private static Timer mTimer = null;
        private static int ProcessCheckInterval = 2000;//Timer Interval
        #endregion

        /// <summary>
        /// Singleton Class Implementation
        /// </summary>
        /// <returns></returns>
        public static MemoryOptimizeManager GetInstance()
        {
            if (_MemoryOptimizeManager == null)
            {
                _MemoryOptimizeManager = new MemoryOptimizeManager();
            }
            return _MemoryOptimizeManager;
        }

        /// <summary>
        /// This Method will start the memory optimization
        /// </summary>
        public void StartMemoryOptimization()
        {
            if (mTimer == null)
            {
                mTimer = new Timer();
                mTimer.Interval = ProcessCheckInterval;
            }
            mTimer.Elapsed -= mTimer_Elapsed;
            mTimer.Elapsed += mTimer_Elapsed;
            mTimer.Stop();
            mTimer.Start();
        }

        private void mTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                mTimer.Stop();
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "Exception Received in MemoryOptimizeManager : mTimer_Elapsed ");
            }
            finally
            {
                mTimer.Start();
            }
        }

        /// <summary>
        /// This method will stop memory optimization
        /// </summary>
        public void StopMemoryOptimization()
        {
            if (mTimer != null)
            {
                mTimer.Stop();
                mTimer.Elapsed -= mTimer_Elapsed;
                mTimer.Dispose();
                mTimer = null;
            }
        }
    }
}
