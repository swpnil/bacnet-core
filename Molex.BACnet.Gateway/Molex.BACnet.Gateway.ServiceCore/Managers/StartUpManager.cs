﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              <StartUpManager>
///   Description:        <start up required initialization>
///   Author:             <Prasad Joshi>                  
///   Date:               05/18/17
///   Notes:              <Notes>
///   Revision History: 
///   Modified By:Prasad Joshi     Modified Date:   Reviewed By:      Reviewed Date:    Review Comments:
///---------------------------------------------------------------------------------------------
#endregion

using Molex.BACnet.Gateway.Log;
using Molex.BACnet.Gateway.ServiceCore.Constants;
using Molex.BACnet.Gateway.ServiceCore.Helpers;
using Molex.BACnet.Gateway.ServiceCore.Models;
using Molex.BACnet.Gateway.ServiceCore.Models.Entity;
using Molex.BACnet.Gateway.ServiceCore.Models.MolexAPIModels;
using Molex.BACnet.Gateway.ServiceCore.Models.ObjectMap;
using Molex.BACnet.Gateway.ServiceCore.Models.SubsriberModels;
using Molex.BACnet.Gateway.ServiceCore.Processor;
using Molex.BACnet.Gateway.ServiceCore.Repository;
using Molex.BACnet.Gateway.ServiceCore.SingletonCore;
using Molex.StackDataObjects.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading;
using Molex.BACnet.Gateway.ServiceCore.Controllers;
using System.Net.Sockets;

namespace Molex.BACnet.Gateway.ServiceCore.Managers
{
    /// <summary>
    /// start up required initialization
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:26 PM</TimeStamp>
    internal class StartUpManager : SingletonBase<StartUpManager>
    {
        #region Variable
        bool _isInitCompleted;
        DateTime _projectAPIFetchedTime;
        DateTime _projectLightScenesFetchedTime;
        int RetryCount = 0;
        int MaxRetryCount = 5;
        #endregion

        #region PublicMethods

        /// <summary>
        /// Initialize this instance
        /// </summary>
        public void Init()
        {
            _projectAPIFetchedTime = DateTime.Now;
            _projectLightScenesFetchedTime = DateTime.Now;
            _isInitCompleted = true;
        }

        /// <summary>
        /// DeInitialize Instances
        /// </summary>
        public void Deinit()
        {
            if (!_isInitCompleted) return;

            Logger.Instance.Log(LogLevel.DEBUG, "StartUpManager:Deinit: StackManager De-Initialization Called");
            StackManager.Instance.DeInit();
            Logger.Instance.Log(LogLevel.DEBUG, "StartUpManager:Deinit: HTTPAPIManager De-Initialization Called");
            HTTPAPIManager.Instance.DeInit();
        }

        /// <summary>
        /// This Method will validate whther the Current Project ID & Persistent Details 
        /// are matching or not otherwise it will delete that Persistent Files manually
        /// </summary>
        /// <param name="projectID"></param>
        private void ValidatePersistantDetailsModel()
        {
            if (AppCoreRepo.Instance == null)
                return;
            if (AppCoreRepo.Instance.BACnetConfigurationData == null)
                return;
            string projectID = AppCoreRepo.Instance.BACnetConfigurationData.MolexProjectID;
            if (string.IsNullOrEmpty(projectID))
                return;
            MappingData mappingData;
            bool isObjectMappingAvailable = ObjectMappingHelper.GetObjectMappingDataFromFile(Path.Combine(AppCoreRepo.Instance.ConfigurationFolder, AppConstants.BACNET_OBEJCT_FILENAME), out mappingData);
            if (mappingData == null)
                return;
            if (mappingData.CachedObjectDetails == null)
                return;
            if (mappingData.CachedObjectDetails.Any(s => s.Key == projectID))
            {
                EntityObject buildingObject = mappingData.CachedObjectDetails.Where(s => s.Key == projectID).First().Value;
                if (buildingObject != null && buildingObject.DeviceId != AppCoreRepo.Instance.BACnetConfigurationData.ControllerId)
                {
                    //delete the old Configuration as Project ID is not there
                    FileInfo configFile = new FileInfo(Path.Combine(AppCoreRepo.Instance.ConfigurationFolder, AppConstants.BACNET_OBEJCT_FILENAME));
                    if (configFile != null && configFile.Exists)
                        configFile.Delete();
                }
            }
            else
            {
                //delete the old Configuration as Project ID is not there
                FileInfo configFile = new FileInfo(Path.Combine(AppCoreRepo.Instance.ConfigurationFolder, AppConstants.BACNET_OBEJCT_FILENAME));
                if (configFile != null && configFile.Exists)
                    configFile.Delete();
            }
        }

        /// <summary>
        /// Execute Required initialization flow
        /// </summary>
        /// <returns>bool</returns>
        public bool Execute()
        {
            Logger.Instance.Log(LogLevel.DEBUG, "Execute: StartUp Manager Execution : Started");
            try
            {
                ValidatePersistantDetailsModel();
                ProcessorConfigurationModel processorConfiguration = new ProcessorConfigurationModel();

                MappingData mappingData;
                bool isObjectMappingAvailable = ObjectMappingHelper.GetObjectMappingDataFromFile(Path.Combine(AppCoreRepo.Instance.ConfigurationFolder, AppConstants.BACNET_OBEJCT_FILENAME), out mappingData);

                if (isObjectMappingAvailable)
                {
                    processorConfiguration.RestoreStrategy = ObjectMappingHelper.GetRestorIDStatery(mappingData.CachedObjectDetails, AppCoreRepo.Instance.BACnetConfigurationData.ControllerId);
                    processorConfiguration.EntityBACnetCache = mappingData.CachedObjectDetails;
                    processorConfiguration.UserBACnetObjects = mappingData.UserBACnetObjects;
                    processorConfiguration.MappedObjectIds = ObjectMappingHelper.GetDeviceObjectIDMapping(mappingData.CachedObjectDetails);
                }

                processorConfiguration.BACnetConfigurationData = AppCoreRepo.Instance.BACnetConfigurationData;

                ObjectIdentifierManager.Instance.SetInitailDeviceId = AppCoreRepo.Instance.BACnetConfigurationData.ControllerId;

                //Integration layer required intialization
                if (!StackManager.Instance.Init(AppCoreRepo.Instance.BACnetConfigurationData.IsDebugLogEnable, AppCoreRepo.Instance.LogDeviceId))
                {
                    Logger.Instance.Log(LogLevel.ERROR, "StartUpManager: Execute: failed to Integration Layer initialization");
                    return false;
                }
                //Vendor specific configuring stack
                ResponseResult responseConfigureStack = StackManager.Instance.ConfigureStack();
                if (!responseConfigureStack.IsSucceed)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "StartUpManager: Execute: failed to set Vendor specific data");
                    return responseConfigureStack.IsSucceed;
                }

                //Initialize HTTPAPI manager with Molex Project and Space state URL
                HTTPAPIManager.Instance.Init(AppCoreRepo.Instance.BACnetConfigurationData.MolexAPIUrl, AppCoreRepo.Instance.BACnetConfigurationData.UserName, AppCoreRepo.Instance.BACnetConfigurationData.Password, AppCoreRepo.Instance.BACnetConfigurationData.MolexProjectID);

                //fetch project API data
                ResultModel<string> resultJsonData = GetProjectAPIData();
                if (!resultJsonData.IsSuccess)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "StartUpManager:GetProjectAPIData: Project API request failed");
                    return resultJsonData.IsSuccess;
                }

                //Storing JSON Data in APIMolexModel  
                ResultModel<MolexProject> apiMolexModel = MolexAPIConverterHelper.ParseMolexJSONData(resultJsonData.Data);
                if (!apiMolexModel.IsSuccess)
                {
                    Logger.Instance.Log(LogLevel.ERROR, CommonHelper.PrepareErrorLogMessage(apiMolexModel.Error, "StartUpManager:Execute:Parsing Project API data failed"));
                    return apiMolexModel.IsSuccess;
                }

                ResultModel<string> resultlightsceneJsonData = GetAllProjectLightScenes();
                LightScenesRootObject lightsceneObject = Utility.Helper.JSONReaderWriterHelper.ReadJsonFromString<LightScenesRootObject>(resultlightsceneJsonData.Data);

                ResultModel<List<string>> resultLightscene = MolexAPIConverterHelper.GetAllLightScence(lightsceneObject);
                if (!resultLightscene.IsSuccess)
                {
                    Logger.Instance.Log(LogLevel.ERROR, CommonHelper.PrepareErrorLogMessage(resultLightscene.Error, "StartUpManager:Execute:Parsing Lightscene data failed"));
                    return resultLightscene.IsSuccess;
                }
                //Storing all LightScene from project File
                AppCoreRepo.Instance.AllLightScene = resultLightscene.Data.ToArray();

                processorConfiguration.LightScene = AppCoreRepo.Instance.AllLightScene;


                ResultModel<List<string>> userLightSceneResultModel = MolexAPIConverterHelper.GetUserAndGeneralLightScene(lightsceneObject, AppCoreRepo.Instance.RemovedLightScenes);
                if (!userLightSceneResultModel.IsSuccess)
                {
                    Logger.Instance.Log(LogLevel.ERROR, CommonHelper.PrepareErrorLogMessage(resultLightscene.Error, "StartUpManager:Execute:Parsing User Lightscene data failed"));
                    return userLightSceneResultModel.IsSuccess;
                }
                AppCoreRepo.Instance.UserAndGeneralLightScene = userLightSceneResultModel.Data.ToArray();

                processorConfiguration.UserAndGeneralLightScenes = AppCoreRepo.Instance.UserAndGeneralLightScene;

                ResultModel<List<string>> beaconLightSceneResultModel = MolexAPIConverterHelper.GetBeaconLightScene(lightsceneObject, AppCoreRepo.Instance.RemovedLightScenes);
                if (!beaconLightSceneResultModel.IsSuccess)
                {
                    Logger.Instance.Log(LogLevel.ERROR, CommonHelper.PrepareErrorLogMessage(resultLightscene.Error, "StartUpManager:Execute:Parsing Beacon Lightscene data failed"));
                    return beaconLightSceneResultModel.IsSuccess;
                }

                AppCoreRepo.Instance.BeaconLightScene = beaconLightSceneResultModel.Data.ToArray();

                processorConfiguration.BeaconLightScenes = AppCoreRepo.Instance.BeaconLightScene;

                //Storing the ProjectID from project file
                AppCoreRepo.Instance.MolexProjectID = processorConfiguration.BACnetConfigurationData.MolexProjectID;

                Logger.Instance.Log(LogLevel.DEBUG, "Molex Project ID for communication: " + AppCoreRepo.Instance.MolexProjectID);


                //Parsing API Molex model Data into Entity Model
                ResultModel<EntityBaseModel> entityBaseModel = MolexAPIConverterHelper.ParseEntityModel(apiMolexModel.Data);
                if (!entityBaseModel.IsSuccess)
                {
                    Logger.Instance.Log(LogLevel.ERROR, CommonHelper.PrepareErrorLogMessage(entityBaseModel.Error, "StartUpManager:Execute:Parsing project API to entity model failed"));
                    return entityBaseModel.IsSuccess;
                }

                //Storing Zone data in Dictonary<key,Value> format  
                ResultModel<Dictionary<string, EntityBaseModel>> zoneData = EntityHelper.GetZoneData(entityBaseModel.Data);
                if (!zoneData.IsSuccess)
                {
                    Logger.Instance.Log(LogLevel.ERROR, CommonHelper.PrepareErrorLogMessage(zoneData.Error, "StartUpManager:Execute:Parsing zone entity model failed"));
                    return zoneData.IsSuccess;
                }

                AppCoreRepo.Instance.ZonesLookUp = zoneData.Data;
                //Storing All entity data in Dictonary<key,Value> format  
                //key:Entity Key , value:EntityBaseModel
                ResultModel<Dictionary<string, EntityBaseModel>> allEntityLookUp = EntityHelper.GetAllEntityLookUp(entityBaseModel.Data);
                if (!allEntityLookUp.IsSuccess)
                {
                    Logger.Instance.Log(LogLevel.ERROR, CommonHelper.PrepareErrorLogMessage(allEntityLookUp.Error, "StartUpManager:Execute:Parsing all entity model failed"));
                    return allEntityLookUp.IsSuccess;
                }

                AppCoreRepo.Instance.AllEntityLookUp = allEntityLookUp.Data;

                //Set virtual network number in stack before create device
                bool IsSuccess = StackManager.SetLocalNetworkNumber((uint)AppCoreRepo.Instance.BACnetConfigurationData.LocalNetworkNumber);
                if (!IsSuccess)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "StartUpManager:SetVirtualNetworkNumber:Fail to set Local Network Number");
                    return IsSuccess;
                }

                //Set local network number in stack before create device

                IsSuccess = StackManager.SetVirtualNetworkNumber((uint)AppCoreRepo.Instance.BACnetConfigurationData.VirtualNetworkNumber);
                if (!IsSuccess)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "StartUpManager:SetLocalNetworkNumber:Fail to set Virtual Network Number");
                    return IsSuccess;
                }

                AppCoreRepo.Instance.ControllerEntityModel = EntityHelper.GetControllerEntityModel(entityBaseModel.Data);

                //Create BACnet Object for all Entity Model in BACnet Stack
                bool result = (new ControllerProcessor()).ProcessEntity(entityBaseModel.Data, processorConfiguration);

                if (!result)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "StartUpManager:Execute:Create BACnet objects failed");
                    return result;
                }

                //TODO: make provison for multiple controller device object creation

                //BACnet Stack initialization
                ResponseResult responseResult = StackManager.Instance.StackInit();

                if (!responseResult.IsSucceed)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "StartUpManager:Execute:BACnet Stack initialization failed");
                    return responseResult.IsSucceed;
                }

                //On post init register BACnet packet time delay and BBMD/FD 
                result = (new PostStackInitProcessor()).Process(processorConfiguration, AppCoreRepo.Instance.BACnetPacketDelayTimeMiliSec);

                if (!result)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "StartUpManager:PostStackInitProcessor:BACnet register delay/BBMD functionality failed");
                    return result;
                }

                AppCoreRepo.Instance.PersistentModelCache = processorConfiguration.UserBACnetObjects;
                PersistenceStorageManager.Instance.Store(entityBaseModel.Data, AppCoreRepo.Instance.PersistentModelCache);
                //SCScheduleManager.GetInstance().GetAllSchedules(processorConfiguration);
                if (!AppCoreRepo.Instance.BACnetConfigurationData.IsMQTTPublishSelected)
                {
                    string LocalEndPointAddress = GetLocalEndPointAddress();
                    GatewayHTTPController.Start(LocalEndPointAddress);
                    APISubscription(LocalEndPointAddress);
                }
                else
                {
                    GatewayMQTTController.Start();
                }
            }
            catch (Exception e)
            {
                Logger.Instance.Log(e, LogLevel.ERROR, "StartupManager:Execute:Initialization execute failed");
                return false;
            }

            Logger.Instance.Log(LogLevel.DEBUG, "Execute: StartUp Manager Execution : Completed");
            return true;
        }

        #endregion PublicMethods

        #region PrivateMethods
        /// <summary>
        /// This Method will return the LocalEndPoint Address
        /// </summary>
        /// <returns></returns>
        public string GetLocalEndPointAddress()
        {
            try
            {
                string BrokerAddress = AppCoreRepo.Instance.BACnetConfigurationData.MolexAPIUrl;
                string address = BrokerAddress.Replace("/transcend/api/v1/", "").Replace("http://", "");
                string[] data = address.Split(':');
                if (data.Length != 2)
                    return AppCoreRepo.Instance.BACnetConfigurationData.IPAddress;
                TcpClient tcpClient = new TcpClient(data[0], Convert.ToInt32(data[1]));
                if (tcpClient != null)
                {
                    if (tcpClient.Connected)
                    {
                        string value = ((System.Net.IPEndPoint)(tcpClient.Client.LocalEndPoint)).Address.ToString();
                        tcpClient.Close();
                        tcpClient = null;
                        return value;
                    }
                    else
                        return AppCoreRepo.Instance.BACnetConfigurationData.IPAddress;
                }
                else
                    return AppCoreRepo.Instance.BACnetConfigurationData.IPAddress;
            }
            catch
            {
                return AppCoreRepo.Instance.BACnetConfigurationData.IPAddress;
            }
        }

        /// <summary>
        /// APIs the subscription.
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/201811:57 AM</TimeStamp>
        public void APISubscription(string IPAddress)
        {
            SubscriptionModel model;
            RetryCount = 0;//Re-Setting the Retry Count
            //Subscription to get reliability status of all devices
            model = new SubscriptionModel(AppConstants.SubscriptionType.status.ToString());
            model.url = string.Format(AppConstants.SUBSCRIPTION_URL, IPAddress);
            DoSubscription(model);

            RetryCount = 0;//Re-Setting the Retry Count
            //Subscription to get notification from API
            model = new SubscriptionModel(AppConstants.SubscriptionType.notification.ToString());
            model.url = string.Format(AppConstants.SUBSCRIPTION_URL, IPAddress);
            DoSubscription(model);

            RetryCount = 0;//Re-Setting the Retry Count
            ////Subscription to get individual sensor readings in all zones
            model = new SubscriptionModel(AppConstants.SubscriptionType.sensorData.ToString());
            model.url = string.Format(AppConstants.SUBSCRIPTION_URL, IPAddress);
            DoSubscription(model);

            RetryCount = 0;//Re-Setting the Retry Count
            //Subscription to get daylight harvesting data of zone
            model = new SubscriptionModel(AppConstants.SubscriptionType.configurations.ToString());
            model.url = string.Format(AppConstants.SUBSCRIPTION_URL, IPAddress);
            DoSubscription(model);

            RetryCount = 0;//Re-Setting the Retry Count
            //Subscription to get zone level objects proparties value
            model = new SubscriptionModel(AppConstants.SubscriptionType.properties.ToString());
            model.url = string.Format(AppConstants.SUBSCRIPTION_URL, IPAddress);
            DoSubscription(model);

            RetryCount = 0;//Re-Setting the Retry Count
            //Subscription to get Floor level sensor average values
            model = new SubscriptionModel("floor/" + AppConstants.SubscriptionType.average.ToString() + "/minute");
            model.url = string.Format(AppConstants.SUBSCRIPTION_URL, IPAddress);
            DoSubscription(model);

            RetryCount = 0;//Re-Setting the Retry Count
            //Subscription to get Floor level occupancy value
            model = new SubscriptionModel("floor/" + AppConstants.SubscriptionType.occupancy.ToString() + "/minute");
            model.url = string.Format(AppConstants.SUBSCRIPTION_URL, IPAddress);
            DoSubscription(model);

            RetryCount = 0;//Re-Setting the Retry Count
            //Subscription to get Floor level power usage value
            model = new SubscriptionModel("floor/" + AppConstants.SubscriptionType.power.ToString() + "/total");
            model.url = string.Format(AppConstants.SUBSCRIPTION_URL, IPAddress);
            DoSubscription(model);
        }

        /// <summary>
        /// To persist client id and subscription id 
        /// </summary>
        /// <param name="jsonData"></param>
        private void DoSubscription(SubscriptionModel model)
        {
            ResultModel<string> subscriptionResponse = HTTPAPIManager.Instance.SubscribeAPI(model);
            if (!subscriptionResponse.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.ERROR, "StartupManager:DoSubscription:" + subscriptionResponse.Error.OptionalMessage + "for topic:" + model.topic);
                //1045 error code return in case of already subscribe
                if (subscriptionResponse.Error.ErrorCode != AppConstants.API_ALREADY_SUBSCRIBE_ERROR_CODE)
                {
                    return;
                }
                else if (subscriptionResponse.Error.ErrorCode == AppConstants.API_ALREADY_SUBSCRIBE_ERROR_CODE)
                {
                    //TODO : This is temp. fix & later their will be HTTPBroker changes in API To Handle the same. 
                    //after that we will remove the same.
                    SubscriptionResponseModel subModel = MolexAPIConverterHelper.ParseMolexAPISubRespData(subscriptionResponse.Data);
                    HTTPAPIManager.Instance.UnSubscribeAPI(subModel.SubscriptionID);
                    if (RetryCount >= MaxRetryCount)
                    {
                        Logger.Instance.Log(LogLevel.ERROR, "StartUpManager:DoSubscription() subscribtion retry failed for subscribtion type :" + model.topic);
                        return;
                    }
                    RetryCount++;
                    DoSubscription(model);
                }
            }
            else
            {
                SubscriptionResponseModel subscriptionResponseModel = MolexAPIConverterHelper.ParseMolexAPISubRespData(subscriptionResponse.Data);
                //To preserv subscription id in application which will be use at the time of unsubscription
                AppCoreRepo.Instance.SubScriptionIDs.Add(subscriptionResponseModel.SubscriptionID);
                //To preserv client id in application which will be use for api alive status call
                AppCoreRepo.Instance.ClientID = subscriptionResponseModel.ClientID;
            }
        }

        /// <summary>
        /// Get project data from molex API
        /// </summary>
        /// <returns></returns>
        private ResultModel<string> GetProjectAPIData()
        {
            ResultModel<string> resultJsonData = null;
            _projectAPIFetchedTime = _projectAPIFetchedTime.AddMinutes(AppCoreRepo.Instance.ProjectApiUrlRetryInMinutes);
            resultJsonData = HTTPAPIManager.Instance.GetProjectData();
            if (resultJsonData.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.DEBUG, "StartUpManager:GetProjectAPIData:Porject API request served successfully");
            }
            else
            {
                if (resultJsonData.Error.ErrorCode == AppConstants.ERROR_CATEGORY_SERVER_UNAVAILABLE)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "StartUpManager:GetProjectAPIData:" + resultJsonData.Error.OptionalMessage);
                }
                else
                {
                    Logger.Instance.Log(LogLevel.ERROR, "StartUpManager:GetProjectAPIData:" + resultJsonData.Error.OptionalMessage);
                }
            }
            return resultJsonData;
        }

        //private ResultModel<string> GetProjectAPIData()
        //{
        //    ResultModel<string> resultJsonData = null;
        //    while (true)
        //    {
        //        if (_projectAPIFetchedTime <= DateTime.Now)
        //        {
        //            _projectAPIFetchedTime = _projectAPIFetchedTime.AddMinutes(AppCoreRepo.Instance.ProjectApiUrlRetryInMinutes);
        //            resultJsonData = HTTPAPIManager.Instance.GetProjectData();
        //            if (resultJsonData.IsSuccess)
        //            {
        //                Logger.Instance.Log(LogLevel.DEBUG, "StartUpManager:GetProjectAPIData:Porject API request served successfully");
        //                break;
        //            }
        //            else
        //            {
        //                if (resultJsonData.Error.ErrorCode == AppConstants.ERROR_CATEGORY_SERVER_UNAVAILABLE)
        //                {
        //                    Logger.Instance.Log(LogLevel.ERROR, "StartUpManager:GetProjectAPIData:" + resultJsonData.Error.OptionalMessage);
        //                    continue;
        //                }
        //                else
        //                {
        //                    Logger.Instance.Log(LogLevel.ERROR, "StartUpManager:GetProjectAPIData:" + resultJsonData.Error.OptionalMessage);
        //                    break;
        //                }
        //            }
        //        }

        //        Thread.Sleep(1000);
        //    }
        //    return resultJsonData;
        //}


        /// <summary>
        /// Get project data from molex API
        /// </summary>
        /// <returns></returns>
        private ResultModel<string> GetAllProjectLightScenes()
        {
            ResultModel<string> resultJsonData = null;
            while (true)
            {
                if (_projectLightScenesFetchedTime <= DateTime.Now)
                {
                    _projectLightScenesFetchedTime = _projectLightScenesFetchedTime.AddMinutes(AppCoreRepo.Instance.ProjectApiUrlRetryInMinutes);
                    resultJsonData = HTTPAPIManager.Instance.GetProjectLightScenes();
                    if (resultJsonData.IsSuccess)
                    {
                        Logger.Instance.Log(LogLevel.DEBUG, "StartUpManager:GetProjectAPIData:Porject API request served successfully");
                        break;
                    }
                    else
                    {
                        if (resultJsonData.Error.ErrorCode == AppConstants.ERROR_CATEGORY_SERVER_UNAVAILABLE)
                        {
                            Logger.Instance.Log(LogLevel.ERROR, "StartUpManager:GetProjectAPIData:" + resultJsonData.Error.OptionalMessage);
                            continue;
                        }
                        else
                        {
                            Logger.Instance.Log(LogLevel.ERROR, "StartUpManager:GetProjectAPIData:" + resultJsonData.Error.OptionalMessage);
                            break;
                        }
                    }
                }

                Thread.Sleep(1000);
            }
            return resultJsonData;
        }
        #endregion PrivateMethods
    }
}
