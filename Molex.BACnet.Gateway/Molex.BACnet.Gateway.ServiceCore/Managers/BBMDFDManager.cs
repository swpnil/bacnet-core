﻿#region File Header
// Copyright ©  2017 UniDEL Systems Pvt. Ltd.
// All rights are reserved. Reproduction or transmission in whole or in part, in any form or by any
// means, electronic, mechanical or otherwise, is prohibited without the prior written consent of the
// copyright owner.
//
// Filename          : FDManager.cs
// Author            : Amol Kulkarni
// Description       : This class will hold the functionality for BBMD and Foreign device registration
// Revision History  : 
// 
// Date        Author            Modification
// 28-08-2017  Amol Kulkarni      File Created
#endregion
using Molex.BACnet.Gateway.Encryption;
using Molex.BACnet.Gateway.Log;
using Molex.BACnet.Gateway.ServiceCore.Constants;
using Molex.BACnet.Gateway.ServiceCore.Models;
using Molex.BACnet.Gateway.ServiceCore.Repository;
using Molex.BACnet.Gateway.ServiceCore.SingletonCore;
using Molex.BACnet.Gateway.Utility.Helper;
using Molex.BACnetStackIntegration.StackManagedApi;
using Molex.StackDataObjects.APIModels;
using Molex.StackDataObjects.Response;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace Molex.BACnet.Gateway.ServiceCore.Managers
{
    /// <summary>
    /// BBMD and FD register Manager
    /// </summary>
    /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>8/28/20176:45 PM</TimeStamp>
    /// <seealso cref="Molex.BACnet.Gateway.ServiceCore.SingletonCore.SingletonBase{Molex.BACnet.Gateway.ServiceCore.Managers.FDManager}" />
    public class BBMDFDManager : SingletonBase<BBMDFDManager>
    {
        #region PrivateField
        private bool _isOperationInProgress;
        private System.Timers.Timer _timer;
        #endregion PrivateField

        #region PublicMethod
        /// <summary>
        /// Gets the BBMD status.
        /// </summary>
        /// <returns></returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>8/28/20176:45 PM</TimeStamp>
        public ResponseResult GetBBMDStatus()
        {
            ResponseResult result = StackManagedApi.GetBBMDStatus();

            if (!result.IsSucceed)
            {
                Logger.Instance.Log(LogLevel.ERROR, "StackManager.GetBBMDStatus: Get BBMD Status failed");
            }
            return result;
        }

        /// <summary>
        /// Enables the BBMD.
        /// </summary>
        /// <returns></returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>8/28/20176:45 PM</TimeStamp>
        public ResponseResult EnableBBMD()
        {
            ResponseResult result = StackManagedApi.EnableBBMD();
            if (!result.IsSucceed)
            {
                Logger.Instance.Log(LogLevel.ERROR, "StackManager.EnableBBMD: Enable BBMD failed");
            }
            return result;
        }

        /// <summary>
        /// Writes the BDT.
        /// </summary>
        /// <param name="ipAddress">The ip address.</param>
        /// <param name="port">The port.</param>
        /// <param name="bdtList">The BDT list.</param>
        /// <returns></returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/18/20183:26 PM</TimeStamp>
        public bool WriteBDT(string ipAddress, int port, List<StackDataObjects.APIModels.BDTModel> bdtList)
        {
            bool result = true;
            //To write BCT configured BDT entry in BDT table
            if (bdtList != null && bdtList.Count > 0)
            {
                result = StackManagedApi.WriteBDT(ipAddress, (ushort)port, bdtList, WriteBDTCallback);

                if (!result)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "BBMDFDManager.WriteBDT: Write BDTList entries in BDT table failed");
                }
            }
            return result;
        }


        /// <summary>
        /// Disables the BBMD.
        /// </summary>
        /// <returns></returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>8/28/20176:45 PM</TimeStamp>
        public ResponseResult DisableBBMD()
        {
            ResponseResult result = StackManagedApi.DisableBBMD();

            if (!result.IsSucceed)
            {
                Logger.Instance.Log(LogLevel.ERROR, "StackManager.EnableBBMD: Enable BBMD failed");
            }
            return result;
        }

        /// <summary>
        /// Gets the fd status.
        /// </summary>
        /// <returns></returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>8/28/20176:45 PM</TimeStamp>
        public ResponseResult GetFDStatus()
        {
            ResponseResult result = StackManagedApi.GetFDStatus();

            if (!result.IsSucceed)
            {
                Logger.Instance.Log(LogLevel.ERROR, "StackManager.GetFDStatus: Get FDStatus failed");
            }
            return result;
        }

        /// <summary>
        /// Registers the fd.
        /// </summary>
        /// <returns></returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>8/28/20176:45 PM</TimeStamp>
        public bool RegisterFD()
        {
            bool isRegister = true;
            IPAddress result = null;
            //Re Fetch the FDT details because this method might can be initiated at Gateway Start or at Windows 
            //Service custom command so in case of Custom command if the details are changed then we need to Re-Fetch the FDT details
            AppCoreRepo.Instance.BACnetConfigurationData.FDIPAddress = JSONReaderWriterHelper.ReadJsonFromFile<BACnetConfigurationModel>(Path.Combine(AppCoreRepo.Instance.ConfigurationFolder, AppConstants.BACNET_CONFIGURATION_FILENAME)).FDIPAddress;
            AppCoreRepo.Instance.BACnetConfigurationData.FDUDPPort = JSONReaderWriterHelper.ReadJsonFromFile<BACnetConfigurationModel>(Path.Combine(AppCoreRepo.Instance.ConfigurationFolder, AppConstants.BACNET_CONFIGURATION_FILENAME)).FDUDPPort;
            AppCoreRepo.Instance.BACnetConfigurationData.FDTimeToLive = JSONReaderWriterHelper.ReadJsonFromFile<BACnetConfigurationModel>(Path.Combine(AppCoreRepo.Instance.ConfigurationFolder, AppConstants.BACNET_CONFIGURATION_FILENAME)).FDTimeToLive;

            string FDIPAddress = AppCoreRepo.Instance.BACnetConfigurationData.FDIPAddress;
            string FDPort = AppCoreRepo.Instance.BACnetConfigurationData.FDUDPPort;
            string TimeToLive = AppCoreRepo.Instance.BACnetConfigurationData.FDTimeToLive;

            if (!string.IsNullOrEmpty(FDIPAddress) &&
            IPAddress.TryParse(FDIPAddress, out result)
            && !string.IsNullOrEmpty(FDPort) && !string.IsNullOrEmpty(TimeToLive))
            {
                int FDUDPPort = Convert.ToInt32(FDPort);
                int FDTimeToLive = Convert.ToInt32(TimeToLive);
                isRegister = StackManagedApi.RegisterFD(FDIPAddress, (ushort)FDUDPPort, (ushort)FDTimeToLive, FDCallback);
            }

            return isRegister;
        }

        /// <summary>
        /// De-init to stop FDReRegister timer
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>9/15/20172:04 PM</TimeStamp>
        public void DeInit()
        {
            DeleteFDEnty();
            Logger.Instance.Log(LogLevel.DEBUG, "FDManager.DeInit,De-Initialization Called to stop FDReRegister timer");
        }

        /// <summary>
        /// To delete FD entry on GW stop or on BCT setting change
        /// </summary>
        public void DeleteFDEnty()
        {
            //Code for delete FD entry from BBMD device of GW on service stop(BAC-122)
            if (!AppCoreRepo.Instance.BACnetConfigurationData.IsBBMD)
            {
                bool isSuccess = true;
                IPAddress result = null;
                string FDIPAddress = AppCoreRepo.Instance.BACnetConfigurationData.FDIPAddress;
                string FDPort = AppCoreRepo.Instance.BACnetConfigurationData.FDUDPPort;
                string TimeToLive = AppCoreRepo.Instance.BACnetConfigurationData.FDTimeToLive;

                if (!string.IsNullOrEmpty(FDIPAddress) &&
                        IPAddress.TryParse(FDIPAddress, out result)
                        && !string.IsNullOrEmpty(FDPort) && !string.IsNullOrEmpty(TimeToLive))
                {
                    Stop();
                    isSuccess = StackManagedApi.DeleteFDT(FDIPAddress, (ushort)(Convert.ToInt32(FDPort)), AppCoreRepo.Instance.BACnetConfigurationData.IPAddress, (ushort)(Convert.ToInt32(AppCoreRepo.Instance.BACnetConfigurationData.UDPPort)), FDDeleteEntryCallback);
                }

                if (!isSuccess)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "FDManager.FD entry deletion failure");
                }
            }
        }

        /// <summary>
        /// Gets the BDT list.
        /// </summary>
        /// <param name="port">The port.</param>
        /// <param name="iPAddress">The i p address.</param>
        /// <returns></returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>4/5/20182:50 PM</TimeStamp>
        public bool GetBDTList(int port, string iPAddress)
        {
            bool isSucess = StackManagedApi.ReadBDT((ushort)port, iPAddress, ReadBDTCallback);
            return isSucess;
        }

        /// <summary>
        /// To stop re-registration as FD device
        /// </summary>
        public void StopReRegistration()
        {
            Stop();
            if (!RegisterFD())
            {
                Logger.Instance.Log(LogLevel.ERROR, "StackManager.GetFDStatus: ReregisterFD failed");
            }
        }

        /// <summary>
        /// To start FD re-registration
        /// </summary>
        public void StartReRegistration()
        {
            if (RegisterFD())
            {
                Start();
            }
        }
        #endregion PublicMethod

        #region PrivateMethod
        /// <summary>
        /// Fds the re register.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Timers.ElapsedEventArgs"/> instance containing the event data.</param>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>8/28/20176:45 PM</TimeStamp>
        private void FDReRegister(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (_isOperationInProgress)
            {
                return;
            }

            _isOperationInProgress = true;

            if (!BBMDFDManager.Instance.RegisterFD())
            {
                Logger.Instance.Log(LogLevel.ERROR, "StackManager.GetFDStatus: ReregisterFD failed");
            }

            _isOperationInProgress = false;
        }

        /// <summary>
        /// Fds the callback.
        /// </summary>
        /// <param name="response">The response.</param>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>8/28/20176:46 PM</TimeStamp>
        private void FDCallback(ResponseResult response)
        {
            if (!response.IsSucceed)
            {
                Logger.Instance.Log(LogLevel.ERROR, "FDManager:Register FD failed");
                return;
            }

            if (!AppCoreRepo.Instance.BACnetConfigurationData.FDReRegister) return;

            if (_timer != null) return;

            Start();
        }

        /// <summary>
        /// Stops this instance.
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>9/15/20172:04 PM</TimeStamp>
        private void Stop()
        {
            if (_timer != null)
            {
                _timer.Stop();
                _timer.Close();
            }
        }

        /// <summary>
        /// Start ths re-registration of FD device
        /// </summary>
        private void Start()
        {
            //If timer is not null then what we need to update here the timetolive & interval too
            if (_timer != null)
            {
                //befroe starting the timer dispose it if its already working
                try
                {
                    _timer.Stop();
                    _timer.Elapsed -= FDReRegister;
                    _timer.Dispose();
                    _timer = null;
                }
                catch
                { }
            }
            _isOperationInProgress = false;
            int timeToLive = Convert.ToInt32(AppCoreRepo.Instance.BACnetConfigurationData.FDTimeToLive);
            timeToLive = timeToLive == 0 ? 1 : timeToLive;
            _timer = new System.Timers.Timer();
            _timer.Interval = timeToLive * 1000;
            _timer.Elapsed -= FDReRegister;
            _timer.Elapsed += FDReRegister;
            _timer.Start();
        }


        /// <summary>
        /// Read BDT entries function
        /// </summary>
        /// <param name="response"></param>
        private void ReadBDTCallback(ResponseResult response)
        {
            if (!response.IsSucceed)
            {
                Logger.Instance.Log(LogLevel.ERROR, "FDManager:Read BDT list failed");
                return;
            }

            string jsonFilePath = Path.Combine(AppCoreRepo.Instance.ConfigurationFolder, AppConstants.BACNET_CONFIGURATION_FILENAME);
            List<BDTModel> BDTEntries = ((Molex.StackDataObjects.Response.ReadBDTResponse)(response.Response)).BDTEntries;
            if (BDTEntries.Count > 0 && File.Exists(jsonFilePath))
            {
                AppCoreRepo.Instance.BACnetConfigurationData.BDTList = BDTEntries;
                AppCoreRepo.Instance.BACnetConfigurationData.BACnetPassword = CryptoHelper.Encrypt3DES(AppCoreRepo.Instance.BACnetConfigurationData.BACnetPassword);
                AppCoreRepo.Instance.BACnetConfigurationData.UserName = CryptoHelper.Encrypt3DES(AppCoreRepo.Instance.BACnetConfigurationData.UserName);
                AppCoreRepo.Instance.BACnetConfigurationData.Password = CryptoHelper.Encrypt3DES(AppCoreRepo.Instance.BACnetConfigurationData.Password);
                JSONReaderWriterHelper.WriteToJsonFile(jsonFilePath, AppCoreRepo.Instance.BACnetConfigurationData);
            }
        }

        /// <summary>
        /// callback function on write BDT
        /// </summary>
        /// <param name="response"></param>
        private void WriteBDTCallback(ResponseResult response)
        {
            if (!response.IsSucceed)
            {
                Logger.Instance.Log(LogLevel.ERROR, "BBMDFDManager:Write BDT entry failed");
                return;
            }

        }

        /// <summary>
        /// Delete fd entry callback
        /// </summary>
        /// <param name="response"></param>
        private void FDDeleteEntryCallback(ResponseResult response)
        {
            if (!response.IsSucceed)
            {
                Logger.Instance.Log(LogLevel.ERROR, "FDManager:Delete FD entry failed");
            }
        }
        #endregion PrivateMethod
    }
}
