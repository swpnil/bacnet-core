﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              NameDescriptionPersistenceStorageManager
///   Description:        SingletonQueueBase, used to write name and description to objects in mapping file.
///   Author:             Rohit Galgali                
///   Date:               07/07/17
#endregion

using CsvHelper;
using Molex.BACnet.Gateway.Log;
using Molex.BACnet.Gateway.ServiceCore.Constants;
using Molex.BACnet.Gateway.ServiceCore.Helpers;
using Molex.BACnet.Gateway.ServiceCore.Models;
using Molex.BACnet.Gateway.ServiceCore.Models.BACnet;
using Molex.BACnet.Gateway.ServiceCore.Models.Entity;
using Molex.BACnet.Gateway.ServiceCore.Models.ObjectMap;
using Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel;
using Molex.BACnet.Gateway.ServiceCore.Repository;
using Molex.BACnet.Gateway.ServiceCore.SingletonCore;
using Molex.StackDataObjects.Constants;
using System;
using System.Collections.Generic;
using System.IO;

namespace Molex.BACnet.Gateway.ServiceCore.Managers
{
    /// <summary>
    /// This Class queues the entity model and writes them to mapping file.
    /// </summary>
    /// <CreatedBy>Rohit Galgali</CreatedBy>
    /// <TimeStamp>7/7/20174:08 PM</TimeStamp>
    /// <seealso cref="Molex.BACnet.Gateway.ServiceCore.SingletonCore.SingletonQueueBase{Molex.BACnet.Gateway.ServiceCore.Managers.NameDescriptionPersistenceStorageManager,Molex.BACnet.Gateway.ServiceCore.Models.Entity.EntityBaseModel}" />
    internal class PersistenceStorageManager : SingletonQueueBase<PersistenceStorageManager, MappingModel>
    {
        private PersistenceStorageManager()
        {
            base.ThreadName = "PersistenceStorageManagerThread";
        }

        /// <summary>
        /// Stores the specified model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="persistentModel">The persistent model.</param>
        /// <CreatedBy>Rohit Galgali</CreatedBy>
        /// <TimeStamp>7/7/20174:09 PM</TimeStamp>
        internal void Store(EntityBaseModel model, Dictionary<string, PersistedModels> persistentModel)
        {
            MappingModel mapmodel = new MappingModel() { EntityModel = model, PersistentModel = persistentModel };
            base.Enque(mapmodel);
        }

        /// <summary>
        /// Processes the specified queue item.
        /// </summary>
        /// <param name="queueItem">The queue item.</param>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>7/7/20174:09 PM</TimeStamp>
        /// <CreatedBy>Rohit Galgali</CreatedBy><TimeStamp>25-05-201705:51 PM</TimeStamp>
        public override void Process(MappingModel queueItem)
        {
            try
            {
                MappingData mappingData = new MappingData();
                Dictionary<string, EntityObject> cachedData = ObjectMappingHelper.MapEntityToObjectData(queueItem.EntityModel);
                Dictionary<string, PersistedModels> persitentModels = queueItem.PersistentModel;
                mappingData.CachedObjectDetails = cachedData;
                mappingData.UserBACnetObjects = persitentModels;
                ObjectMappingHelper.WriteMappedDataToFile(Path.Combine(AppCoreRepo.Instance.ConfigurationFolder, AppConstants.BACNET_OBEJCT_FILENAME), mappingData);
                List<EntityBaseModel> dataModels = new List<EntityBaseModel>();
                ObjectMappingHelperForTreeStructure.FillDataModelStructure(queueItem.EntityModel, dataModels);
                string path = Path.Combine(AppCoreRepo.Instance.ConfigurationFolder, AppConstants.BACNET_CSVSTRUCTURED_OBJECT_FILENAME);
                List<ExportModel> lightingObjectList = new List<ExportModel>();


                lightingObjectList.Add(new ExportModel
                {
                    Site = "Space Information",
                    EquipmentName = "All Items / Equipment Tree Information",
                    EngineName = "Network Information (MSTP and IP)",
                    EquipmentDefinitionName = "Definitions and Templates",
                    Parameter = "Parameters"
                });
                lightingObjectList.Add(new ExportModel
                {
                    Site = "Site/Building/Floor(Required)",
                    Room = "Room Number (Optional)",
                    Leaf = "Leaf Space (e.g. Room) (Required)",
                    EquipmentName = "Device and Equipiment Name (Required)",
                    EquipmentFQR = "Device and Equipment FQR Reference (Required)",
                    Served = "Served By Equipment Name (Required)",
                    ControllerPart = "Controller Part # (Optional)",
                    EngineName = "Engine Name(Required)",
                    Trunk = "Trunk Name (Required)",
                    ControllerHostName = "Controller Host Name (Future)",
                    JCIMACAddress = "JCI MAC  Address",
                    IPControllerNumber = "IP Controller Number",
                    ZIGBEEPANOffset = "ZIGBEE PAN Offset",
                    Instance = "Instance # (BACoid)",
                    DHCPEnabled = "DHCP Enabled",
                    IPAddress = "IP Address",
                    SubnetMask = "Subnet Mask",
                    IPRouter = "IP Router",
                    ETH1 = "ETH-1 (Optional)",
                    ETH2 = "ETH-2 (Optional)",
                    EquipmentDefinitionName = "Equipment Definition Name (Optional)",
                    ControllerTemplateName = "Controller Template Name (Required)",
                    Lightlevel = "Light level",
                    Shedinglevel = "Sheding level",
                    Light_ON_OFF = "Light ON/OFF",
                    Beacon_ON_OFF = "Beacon ON/OFF",
                    ALS = "ALS",
                    AQ = "AQ",
                    Presence = "Presence",
                    ColorTemp = "Color Temp",
                    Power = "Power",
                    Temperature = "Temperature",
                    Humidity = "Humidity",
                    lightscence = "light scence",
                    Mood = "Mood",
                    Target_Lux = "Target Lux",
                    Timeout = "Timeout",
                    fadeout = "fadeout",
                    Beaconcolor = "Beacon color"
                });
                lightingObjectList.Add(new ExportModel
                {
                    EquipmentName = "Attribute ID"
                });
                lightingObjectList.Add(new ExportModel
                {
                    EquipmentName = "Attribute Type"
                });

                foreach (EntityBaseModel item in dataModels)
                {
                    string zoneType = string.Empty;
                    string LightLevelvalue = "yes";
                    string Shedinglevelvalue = "no";
                    string Light_ON_OFFvalue = "yes";
                    string Beacon_ON_OFFvalue = "no";
                    string ALSvalue = "no";
                    string AQvalue = "yes";
                    string Presencevalue = "yes";
                    string ColorTempvalue = "yes";
                    string Powervalue = "yes";
                    string Temperaturevalue = "yes";
                    string Humidityvalue = "yes";
                    string lightscencevalue = "yes";
                    string Moodvalue = "yes";
                    string Target_Luxvalue = "yes";
                    string Timeoutvalue = "yes";
                    string fadeoutvalue = "yes";
                    string Beaconcolorvalue = "yes";
                    ALSvalue = isObjectExists(item.BACnetData.ObjectDetails, "Average LuxLevel") ? "yes" : "no";
                    AQvalue = isObjectExists(item.BACnetData.ObjectDetails, "Average AirQuality") ? "yes" : "no";
                    Presencevalue = isObjectExists(item.BACnetData.ObjectDetails, "Occupancy") ? "yes" : "no";
                    ColorTempvalue = isObjectExists(item.BACnetData.ObjectDetails, " Average ColorTemperature") ? "yes" : "no";
                    Powervalue = isObjectExists(item.BACnetData.ObjectDetails, "Total PowerUsage") ? "yes" : "no";
                    Temperaturevalue = isObjectExists(item.BACnetData.ObjectDetails, "Average Temperature") ? "yes" : "no";
                    Humidityvalue = isObjectExists(item.BACnetData.ObjectDetails, "Average Humidity") ? "yes" : "no";
                    Moodvalue = isObjectExists(item.BACnetData.ObjectDetails, "Mood") ? "yes" : "no";
                    Target_Luxvalue = isObjectExists(item.BACnetData.ObjectDetails, "Target Lux Level") ? "yes" : "no";
                    Timeoutvalue = isObjectExists(item.BACnetData.ObjectDetails, "Occupancy TimeOut") ? "yes" : "no";
                    fadeoutvalue = isObjectExists(item.BACnetData.ObjectDetails, "Occupancy FadeOut") ? "yes" : "no";
                    Beaconcolorvalue = isObjectExists(item.BACnetData.ObjectDetails, "Beacon Color") ? "yes" : "no";
                    switch (item.EntityType)
                    {
                        case AppConstants.EntityTypes.Zone:
                            switch (((ZoneEntityModel)item).ZoneType)
                            {
                                case AppConstants.ZoneTypes.BeaconSpace:
                                    {
                                        zoneType = "Information";
                                        Light_ON_OFFvalue = "no";
                                        Beacon_ON_OFFvalue = isObjectExists(item.BACnetData.ObjectDetails, "beacon on Off") ? "yes" : "no";
                                        #region Light Scene
                                        if (((Molex.BACnet.Gateway.ServiceCore.Models.Entity.ZoneEntityModel)(item)) != null)
                                        {
                                            if (((Molex.BACnet.Gateway.ServiceCore.Models.Entity.ZoneEntityModel)(item)).LightScene != null)
                                            {
                                                if (((Molex.BACnet.Gateway.ServiceCore.Models.Entity.ZoneEntityModel)(item)).LightScene.Count != 0)
                                                {
                                                    lightscencevalue = string.Empty;
                                                    foreach (var lightScene in ((Molex.BACnet.Gateway.ServiceCore.Models.Entity.ZoneEntityModel)(item)).LightScene)
                                                    {
                                                        if (string.IsNullOrEmpty(lightscencevalue))
                                                            lightscencevalue = lightScene;
                                                        else
                                                            lightscencevalue = lightscencevalue + "," + lightScene;
                                                    }
                                                }
                                            }
                                        } 
                                        #endregion
                                    }
                                    break;
                                case AppConstants.ZoneTypes.UserSpace:
                                    {
                                        zoneType = "Lights (user)";
                                        Light_ON_OFFvalue = isObjectExists(item.BACnetData.ObjectDetails, "Lights on Off") ? "yes" : "no";
                                        #region Light Scene
                                        if (((Molex.BACnet.Gateway.ServiceCore.Models.Entity.ZoneEntityModel)(item)) != null)
                                        {
                                            if (((Molex.BACnet.Gateway.ServiceCore.Models.Entity.ZoneEntityModel)(item)).LightScene != null)
                                            {
                                                if (((Molex.BACnet.Gateway.ServiceCore.Models.Entity.ZoneEntityModel)(item)).LightScene.Count != 0)
                                                {
                                                    lightscencevalue = string.Empty;
                                                    foreach (var lightScene in ((Molex.BACnet.Gateway.ServiceCore.Models.Entity.ZoneEntityModel)(item)).LightScene)
                                                    {
                                                        if (string.IsNullOrEmpty(lightscencevalue))
                                                            lightscencevalue = lightScene;
                                                        else
                                                            lightscencevalue = lightscencevalue + "," + lightScene;
                                                    }
                                                }
                                            }
                                        } 
                                        #endregion
                                    }
                                    break;
                                case AppConstants.ZoneTypes.GeneralSpace:
                                    {
                                        zoneType = "Lights (general)";
                                        Light_ON_OFFvalue = isObjectExists(item.BACnetData.ObjectDetails, "Lights on Off") ? "yes" : "no";
                                        #region Light Scene
                                        if (((Molex.BACnet.Gateway.ServiceCore.Models.Entity.ZoneEntityModel)(item)) != null)
                                        {
                                            if (((Molex.BACnet.Gateway.ServiceCore.Models.Entity.ZoneEntityModel)(item)).LightScene != null)
                                            {
                                                if (((Molex.BACnet.Gateway.ServiceCore.Models.Entity.ZoneEntityModel)(item)).LightScene.Count != 0)
                                                {
                                                    lightscencevalue = string.Empty;
                                                    foreach (var lightScene in ((Molex.BACnet.Gateway.ServiceCore.Models.Entity.ZoneEntityModel)(item)).LightScene)
                                                    {
                                                        if (string.IsNullOrEmpty(lightscencevalue))
                                                            lightscencevalue = lightScene;
                                                        else
                                                            lightscencevalue = lightscencevalue + "," + lightScene;
                                                    }
                                                }
                                            }
                                        } 
                                        #endregion
                                    }
                                    break;
                                case AppConstants.ZoneTypes.BlindSpace:
                                    {
                                        Light_ON_OFFvalue = "no";
                                        LightLevelvalue = "no";
                                        Shedinglevelvalue = string.Empty;
                                        foreach (var objItem in (item as ZoneEntityModel).BACnetData.ObjectDetails)
                                        {
                                            string objectKey = objItem.Key.Substring(objItem.Key.LastIndexOf('_') + 1);
                                            string value = GetFullMotorName(objectKey);
                                            if (string.IsNullOrEmpty(Shedinglevelvalue))
                                                Shedinglevelvalue = value;
                                            else
                                                Shedinglevelvalue = Shedinglevelvalue + "," + value;

                                        }
                                        zoneType = "Shades";
                                        lightscencevalue = "no";
                                    }
                                    break;
                            }
                            break;
                        case AppConstants.EntityTypes.Building:
                        case AppConstants.EntityTypes.Controller:
                            LightLevelvalue = string.Empty;
                            Shedinglevelvalue = string.Empty;
                            Light_ON_OFFvalue = string.Empty;
                            Beacon_ON_OFFvalue = string.Empty;
                            ALSvalue = string.Empty;
                            AQvalue = string.Empty;
                            Presencevalue = string.Empty;
                            ColorTempvalue = string.Empty;
                            Powervalue = string.Empty;
                            Temperaturevalue = string.Empty;
                            Humidityvalue = string.Empty;
                            lightscencevalue = string.Empty;
                            Moodvalue = string.Empty;
                            Target_Luxvalue = string.Empty;
                            Timeoutvalue = string.Empty;
                            fadeoutvalue = string.Empty;
                            Beaconcolorvalue = string.Empty;
                            break;
                    }
                    lightingObjectList.Add(new ExportModel
                   {
                       EquipmentName = item.BACnetData.ObjectDetails[BacnetObjectType.ObjectDevice.ToString()].ObjectName,
                       EquipmentFQR = item.BACnetData.ObjectDetails[BacnetObjectType.ObjectDevice.ToString()].ObjectName,
                       Instance = Convert.ToString(item.BACnetData.ObjectDetails[BacnetObjectType.ObjectDevice.ToString()].ObjectID),
                       ControllerTemplateName = zoneType == string.Empty ? "" : zoneType,
                       Lightlevel = LightLevelvalue,
                       Shedinglevel = Shedinglevelvalue,
                       Light_ON_OFF = Light_ON_OFFvalue,
                       Beacon_ON_OFF = Beacon_ON_OFFvalue,
                       ALS = ALSvalue,
                       AQ = AQvalue,
                       Presence = Presencevalue,
                       ColorTemp = ColorTempvalue,
                       Power = Powervalue,
                       Temperature = Temperaturevalue,
                       Humidity = Humidityvalue,
                       lightscence = lightscencevalue,
                       Mood = Moodvalue,
                       Target_Lux = Target_Luxvalue,
                       Timeout = Timeoutvalue,
                       fadeout = fadeoutvalue,
                       Beaconcolor = Beaconcolorvalue
                   });


                }
                using (StreamWriter sw = new StreamWriter(path))
                using (CsvWriter cw = new CsvWriter(sw))
                {
                    foreach (ExportModel emp in lightingObjectList)
                    {

                        cw.WriteRecord<ExportModel>(emp);
                        cw.NextRecord();
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "PersistenceStorageManager:Process, Error occured while storing Entity model data.");
            }
        }

        private bool isObjectExists(Dictionary<string, BACnetObjectDetails> objectDetails, string objectName)
        {
            bool result = false;
            if (objectDetails == null)
                return result;
            foreach (var item in objectDetails)
            {
                if (item.Value == null)
                    continue;
                if (item.Value.ObjectName.ToUpperInvariant().Trim() == objectName.ToUpperInvariant().Trim())
                    return result = true;
            }
            return result;
        }

        /// <summary>
        /// This Method will be helpful for comparing object name at Building & floor level
        /// </summary>
        /// <param name="shortName"></param>
        /// <returns></returns>
        public string GetFullMotorName(string shortName)
        {
            string result = string.Empty;
            switch (shortName.ToUpperInvariant().Trim())
            {
                case "CENTER":
                    result = "Center motor";
                    break;
                case "EAST":
                    result = "East motor";
                    break;
                case "NORTH":
                    result = "North motor";
                    break;
                case "NORTH-EAST":
                    result = "North-East motor";
                    break;
                case "NORTH-WEST":
                    result = "North-West motor";
                    break;
                case "SOUTH":
                    result = "South motor";
                    break;
                case "SOUTH-EAST":
                    result = "South-East motor";
                    break;
                case "SOUTH-WEST":
                    result = "South-West motor";
                    break;
                case "WEST":
                    result = "West motor";
                    break;
                case "ALL":
                    result = "motor";
                    break;
                case "MASTER SHADING LEVEL":
                    result = "motor";
                    break;
                default:
                    result = string.Empty;
                    break;
            }
            return result;
        }
    }

    internal class MappingModel
    {
        public EntityBaseModel EntityModel { get; set; }
        public Dictionary<string, PersistedModels> PersistentModel { get; set; }
    }
}
