﻿#region File Header
// Copyright ©  2017 UniDEL Systems Pvt. Ltd.
// All rights are reserved. Reproduction or transmission in whole or in part, in any form or by any
// means, electronic, mechanical or otherwise, is prohibited without the prior written consent of the
// copyright owner.
//
// Filename          : ObjectIdentifierManager.cs
// Author            : Amol Kulkarni
// Description       : This class will hold the functionality for create and get objectid/deviceid
// Revision History  : 
// 
// Date        Author            Modification
// 15-05-2017  Amol Kulkarni      File Created
#endregion

using Molex.BACnet.Gateway.Log;
using Molex.BACnet.Gateway.ServiceCore.AppResources;
using Molex.BACnet.Gateway.ServiceCore.Constants;
using Molex.BACnet.Gateway.ServiceCore.Helpers;
using Molex.BACnet.Gateway.ServiceCore.Models.Entity;
using Molex.BACnet.Gateway.ServiceCore.SingletonCore;
using Molex.StackDataObjects.Constants;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Molex.BACnet.Gateway.ServiceCore.Managers
{

    /// <summary>
    /// This class will hold the functionality for create and get objectid/deviceid
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:19 PM</TimeStamp>
    /// <seealso cref="Molex.BACnet.Gateway.ServiceCore.SingletonCore.SingletonBase{Molex.BACnet.Gateway.ServiceCore.Managers.ObjectIdentifierManager}" />
    internal class ObjectIdentifierManager : SingletonBase<ObjectIdentifierManager>
    {
        /// <summary>
        /// To keep high and low range entity wise
        /// </summary>
        /// /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>26-06-201705:19 PM</TimeStamp>
        private class EntityMinMaxLimit
        {
            public int MinId { get; set; }
            public int MaxId { get; set; }
            public bool IsSet { get; set; }
        }

        //To cache the object min,max id limit
        Dictionary<uint, Dictionary<AppConstants.EntityTypes, Dictionary<string, EntityMinMaxLimit>>> _objectMinMaxIDLimit;
        #region Constructor and Singleton Implementation
        volatile static object _syncObjectIDs;
        //To mantain hirarchey of device id and object ids under that device
        Dictionary<uint, Dictionary<AppConstants.EntityTypes, Dictionary<string, List<int>>>> _objectIDs;
        //To initailize deviceid by manually
        uint _setInitialDeviceId;
        //ID ranges for different objects
        const int _buildingMinObjectId = 1;
        const int _buildingMaxObjectId = 1000;
        const int _floorMinObjectId = 1001;
        const int _floorMaxObjectId = 10000;
        const int _aIsensorMinObjectId = 40001;
        const int _aIsensorMaxObjectId = 55000;
        const int _bIsensorMinObjectId = 55001;
        const int _bIsensorMaxObjectId = 70000;
        const int _fixtureMinObjectId = 70001;
        const int _fixtureMaxObjectId = 100000;
        const int _trendLogMinObjectId = 14501;
        const int _trendLogMaxObjectId = 15250;
        const int _scheduleMinObjectId = 15251;
        const int _scheduleMaxObjectId = 16000;
        const int _notificationMinObjectId = 16001;
        const int _notificationMaxObjectId = 16750;
        const int _eventEnrollementMinObjectId = 16751;
        const int _eventEnrollementMaxObjectId = 17500;
        const int _calenderMinObjectId = 17501;
        const int _calenderMaxObjectId = 18250;
        const int _msvMinObjectId = 10751;
        const int _msvMaxObjectId = 11500;
        const int _avMinObjectId = 10001;
        const int _avMaxObjectId = 10750;
        const int _bvMinObjectId = 11501;
        const int _bvMaxObjectId = 12250;
        const int _pivUserSpaceMinObjectId = 12251;
        const int _pivUserSpaceMaxObjectId = 12625;
        const int _pivBeaconSpaceMinObjectId = 12626;
        const int _pivBeaconSpaceMaxObjectId = 13000;
        const int _avBioControlMinObjectId = 13001;
        const int _avBioControlMaxObjectId = 13500;
        const int _avImControlMinObjectId = 13501;
        const int _avImControlMaxObjectId = 14000;
        const int _avBlindAllMinObjectId = 14001;
        const int _avBlindAllMaxObjectId = 14500;
        /// <summary>
        /// Gets or sets the set initail device identifier.
        /// </summary>
        /// <value>
        /// The set initail device identifier.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201706:56 PM</TimeStamp>
        public uint SetInitailDeviceId
        {
            get { return _setInitialDeviceId; }
            set
            {
                _setInitialDeviceId = value;
                _syncObjectIDs = new object();
                if (_objectIDs != null)
                    _objectIDs.Clear();
                if (_objectMinMaxIDLimit != null)
                    _objectMinMaxIDLimit.Clear();
            }
        }

        /// <summary>
        /// Prevents a default instance of the <see cref="ObjectIdentifierManager"/> class from being created.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201706:56 PM</TimeStamp>
        private ObjectIdentifierManager()
        {
            _syncObjectIDs = new object();
            _objectIDs = new Dictionary<uint, Dictionary<AppConstants.EntityTypes, Dictionary<string, List<int>>>>();
            _objectMinMaxIDLimit = new Dictionary<uint, Dictionary<AppConstants.EntityTypes, Dictionary<string, EntityMinMaxLimit>>>();
        }
        #endregion Constructor and Singleton Implementation

        #region Internal Methods
        /// <summary>
        /// Gets the object identifier.
        /// </summary>
        /// <param name="deviceId">The device identifier.</param>
        /// <param name="objectType">Type of the object.</param>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="objectId">object Id.</param>
        /// <returns></returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>25-05-2017 05:00 PM</TimeStamp>
        internal int GetObjectId(uint deviceId, BacnetObjectType objectType, AppConstants.EntityTypes entityType, int objectId = 0, bool isKeySpecificID = false , string objectFindKey = "")
        {
            try
            {
                lock (_syncObjectIDs)
                {
                    string key = isKeySpecificID == false ? objectType.ToString() : objectType.ToString() + "_" + objectFindKey;
                    if ((entityType == AppConstants.EntityTypes.Floor || entityType == AppConstants.EntityTypes.Building) && isKeySpecificID)
                        key = objectType.ToString();
                    int currentID = 1;

                    if (!_objectIDs.ContainsKey(deviceId))
                    {
                        Logger.Instance.Log(LogLevel.ERROR, "ObjectIdentifier: GetObjectId : Device id not found-" + deviceId);
                        return -1;
                    }
                    if (!_objectMinMaxIDLimit.ContainsKey(deviceId))
                    {
                        _objectMinMaxIDLimit.Add(deviceId, new Dictionary<AppConstants.EntityTypes, Dictionary<string, EntityMinMaxLimit>>());
                    }
                    if (objectId != 0)
                    {
                        objectId = objectId + 1;
                        
                        if (_objectIDs[deviceId][entityType][key].Contains(objectId))
                        {
                            objectId = GetObjectId(deviceId, objectType, entityType, objectId);
                        }

                        return objectId;
                    }

                    if (!_objectIDs[deviceId].ContainsKey(entityType))
                    {
                        _objectIDs[deviceId].Add(entityType, new Dictionary<string, List<int>>());

                        if (!_objectMinMaxIDLimit[deviceId].ContainsKey(entityType))
                        {
                            _objectMinMaxIDLimit[deviceId].Add(entityType, new Dictionary<string, EntityMinMaxLimit>());
                        }
                    }

                    if (!_objectIDs[deviceId][entityType].ContainsKey(key))
                    {
                        if (!_objectMinMaxIDLimit[deviceId][entityType].ContainsKey(key))
                        {
                            _objectMinMaxIDLimit[deviceId][entityType].Add(key, new EntityMinMaxLimit());
                        }
                        SetMinMaxObjectId(entityType, objectType, deviceId, isKeySpecificID, objectFindKey);
                        currentID = _objectMinMaxIDLimit[deviceId][entityType][key].MinId;
                        _objectIDs[deviceId][entityType].Add(key, new List<int>());
                    }
                    else
                    {
                        if (_objectIDs[deviceId][entityType][key].Count == 0)
                        {
                            currentID = _objectMinMaxIDLimit[deviceId][entityType][key].MinId;
                            return currentID;
                        }
                        currentID = _objectIDs[deviceId][entityType][key].Max();
                        currentID = currentID + 1;

                        //To check missing id
                        if (currentID > _objectMinMaxIDLimit[deviceId][entityType][key].MaxId)
                        {
                            currentID = Enumerable.Range(_objectMinMaxIDLimit[deviceId][entityType][key].MinId, _objectMinMaxIDLimit[deviceId][entityType][key].MaxId).Except(_objectIDs[deviceId][entityType][key]).FirstOrDefault();

                            if (_objectIDs[deviceId][entityType][key].Contains(currentID))
                            {
                                Logger.Instance.Log(LogLevel.DEBUG, "ObjectIdentifier:Return duplicate objectid because all the ids in range are allocated for type " + entityType);
                            }
                        }

                        if (currentID == int.MaxValue)
                        {
                            Logger.Instance.Log(LogLevel.ERROR, "ObjectIdentifier:Object id exceeds the integer range");
                        }
                    }

                    return currentID;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(LogLevel.ERROR, "ObjectIdentifier: GetObjectId" + ex.Message);
                return 0;
            }
        }

        /// <summary>
        /// To get incremental device id by one against initialized  deviceid
        /// </summary>
        /// <param name="deviceId"></param>
        /// <returns></returns>
        internal uint GetDeviceId(uint deviceId = 0)
        {
            lock (_syncObjectIDs)
            {
                if (deviceId != 0)
                {
                    _setInitialDeviceId = deviceId;
                }

                _setInitialDeviceId = _setInitialDeviceId + 1;

                if (_setInitialDeviceId > AppConstants.MAX_DEVICE_ID)
                {
                    _setInitialDeviceId = 1;
                }

                if (_objectIDs.ContainsKey(_setInitialDeviceId))
                {
                    GetDeviceId();
                }

                return _setInitialDeviceId;
            }
        }

        /// <summary>
        /// Adds the object identifier.
        /// </summary>
        /// <param name="deviceId">The device identifier.</param>
        /// <param name="objectType">Type of the object.</param>
        /// <param name="objectId">The object identifier.</param>
        /// <param name="entityType">Type of the entity.</param>
        /// <returns></returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>25-05-201705:21 PM</TimeStamp>
        /// <exception cref="System.Exception">ObjectID Already Exist</exception>
        internal bool AddObjectId(uint deviceId, BacnetObjectType objectType, int objectId, AppConstants.EntityTypes entityType, bool isKeySpecificID = false, string objectFindKey = "")
        {
            try
            {
                lock (_syncObjectIDs)
                {
                    string key = isKeySpecificID == false ? objectType.ToString() : objectType.ToString() + "_" + objectFindKey;
                    if ((entityType == AppConstants.EntityTypes.Floor || entityType == AppConstants.EntityTypes.Building) && isKeySpecificID)
                        key = objectType.ToString();
                    if (!_objectIDs.ContainsKey(deviceId))
                    {
                        Logger.Instance.Log(LogLevel.ERROR, "ObjectIdentifier: AddObjectId : Device id not found-" + deviceId);
                        return false;
                    }

                    if (!_objectMinMaxIDLimit.ContainsKey(deviceId))
                    {
                        _objectMinMaxIDLimit.Add(deviceId, new Dictionary<AppConstants.EntityTypes, Dictionary<string, EntityMinMaxLimit>>());
                    }
                    //To check Entity Type already exist
                    if (!_objectIDs[deviceId].ContainsKey(entityType))
                    {
                        _objectIDs[deviceId].Add(entityType, new Dictionary<string, List<int>>());

                        if (!_objectMinMaxIDLimit[deviceId].ContainsKey(entityType))
                        {
                            _objectMinMaxIDLimit[deviceId].Add(entityType, new Dictionary<string, EntityMinMaxLimit>());
                        }
                    }

                    //To check Object Type already exist
                    if (!_objectIDs[deviceId][entityType].ContainsKey(key))
                    {
                        if (!_objectMinMaxIDLimit[deviceId][entityType].ContainsKey(key))
                        {
                            _objectMinMaxIDLimit[deviceId][entityType].Add(key, new EntityMinMaxLimit());
                        }
                        SetMinMaxObjectId(entityType, objectType, deviceId, isKeySpecificID, objectFindKey);
                        _objectIDs[deviceId][entityType].Add(key, new List<int>());
                    }

                    //To check Object ID already exist
                    if (!_objectIDs[deviceId][entityType][key].Contains(objectId))
                    {
                        _objectIDs[deviceId][entityType][key].Add(objectId);
                    }
                    else
                    {
                        Logger.Instance.Log(LogLevel.ERROR, "ObjectIdentifier: AddObjectId : Object id already exist");
                        return false;
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(LogLevel.ERROR, "ObjectIdentifier: AddObjectId:" + ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Removes the object identifier.
        /// </summary>
        /// <param name="deviceId">The device identifier.</param>
        /// <param name="objectType">Type of the object.</param>
        /// <param name="objectId">The object identifier.</param>
        /// <param name="entityType">Type of the entity.</param>
        /// <returns></returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>10/6/20175:35 PM</TimeStamp>
        internal bool RemoveObjectId(uint deviceId, BacnetObjectType objectType, int objectId, AppConstants.EntityTypes entityType, bool isKeySpecificID = false , string objectFindKey = "")
        {
            try
            {
                lock (_syncObjectIDs)
                {
                    string key = isKeySpecificID == false ? objectType.ToString() : objectType.ToString() + "_" + objectFindKey;
                    if (!_objectIDs.ContainsKey(deviceId))
                    {
                        Logger.Instance.Log(LogLevel.ERROR, "ObjectIdentifier: RemoveObjectId : Device id not found: " + deviceId);
                        return false;
                    }

                    //To check Entity Type already exist
                    if (!_objectIDs[deviceId].ContainsKey(entityType))
                    {
                        Logger.Instance.Log(LogLevel.ERROR, "ObjectIdentifier: RemoveObjectId : entity type not exist: " + entityType);
                        return false;
                    }

                    //To check Object Type already exist
                    if (!_objectIDs[deviceId][entityType].ContainsKey(key))
                    {
                        Logger.Instance.Log(LogLevel.ERROR, "ObjectIdentifier: RemoveObjectId : Object type not exist: " + objectType);
                        return false;
                    }

                    //To check Object ID already exist
                    if (_objectIDs[deviceId][entityType][key].Contains(objectId))
                    {
                        _objectIDs[deviceId][entityType][key].Remove(objectId);
                    }
                    else
                    {
                        Logger.Instance.Log(LogLevel.ERROR, "ObjectIdentifier: RemoveObjectId : Object id not exist: " + objectId);
                        return false;
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(LogLevel.ERROR, "ObjectIdentifier: RemoveObjectId:" + ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Adds the device identifier.
        /// </summary>
        /// <param name="deviceId">The device identifier.</param>
        /// <returns>bool</returns>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:21 PM</TimeStamp>
        internal bool AddDeviceId(uint deviceId)
        {
            try
            {
                lock (_syncObjectIDs)
                {
                    if (deviceId > AppConstants.MAX_DEVICE_ID)
                    {
                        return false;
                    }
                    if (!_objectIDs.ContainsKey(deviceId))
                    {
                        _objectIDs.Add(deviceId, new Dictionary<AppConstants.EntityTypes, Dictionary<string, List<int>>>());
                    }
                    else
                    {
                        Logger.Instance.Log(LogLevel.ERROR, "ObjectIdentifier: AddDeviceId : Device id already exist");
                        return false;
                    }

                    _setInitialDeviceId = deviceId;
                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(LogLevel.ERROR, "ObjectIdentifier: AddDeviceId:" + ex.Message);
                return false;
            }
        }

        #endregion Internal Methods

        #region Private Methods

        /// <summary>
        /// Sets the minimum maximum object identifier.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="objectType">Type of the object.</param>
        /// <param name="deviceId">The device identifier.</param>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/2/20182:51 PM</TimeStamp>
        private void SetMinMaxObjectId(AppConstants.EntityTypes entityType, BacnetObjectType objectType, uint deviceId, bool isKeySpecificID = false , string objectFindKey = "")
        {
            try
            {
                EntityMinMaxLimit ObjectMinMaxLimit;
                string key = isKeySpecificID == false ? objectType.ToString() : objectType.ToString() + "_" + objectFindKey;
                switch (entityType)
                {
                    case AppConstants.EntityTypes.Controller:
                    case AppConstants.EntityTypes.Building:
                        ObjectMinMaxLimit = _objectMinMaxIDLimit[deviceId][entityType][key];
                        if (!ObjectMinMaxLimit.IsSet)
                        {
                            ObjectMinMaxLimit.MaxId = _buildingMaxObjectId;
                            ObjectMinMaxLimit.MinId = _buildingMinObjectId;
                            ObjectMinMaxLimit.IsSet = true;
                        }
                        break;
                    case AppConstants.EntityTypes.Floor:
                        ObjectMinMaxLimit = _objectMinMaxIDLimit[deviceId][entityType][key];
                        if (!ObjectMinMaxLimit.IsSet)
                        {
                            ObjectMinMaxLimit.MaxId = _floorMaxObjectId;
                            ObjectMinMaxLimit.MinId = _floorMinObjectId;
                            ObjectMinMaxLimit.IsSet = true;
                        }
                        break;
                    case AppConstants.EntityTypes.Zone:
                        ObjectMinMaxLimit = _objectMinMaxIDLimit[deviceId][entityType][key];
                        switch (objectType)
                        {
                            case BacnetObjectType.ObjectTrendlog:
                                if (!ObjectMinMaxLimit.IsSet)
                                {
                                    ObjectMinMaxLimit.MinId = _trendLogMinObjectId;
                                    ObjectMinMaxLimit.MaxId = _trendLogMaxObjectId;
                                    ObjectMinMaxLimit.IsSet = true;
                                }
                                break;
                            case BacnetObjectType.ObjectSchedule:
                                if (!ObjectMinMaxLimit.IsSet)
                                {
                                    ObjectMinMaxLimit.MinId = _scheduleMinObjectId;
                                    ObjectMinMaxLimit.MaxId = _scheduleMaxObjectId;
                                    ObjectMinMaxLimit.IsSet = true;
                                }
                                break;
                            case BacnetObjectType.ObjectNotificationClass:
                                if (!ObjectMinMaxLimit.IsSet)
                                {
                                    ObjectMinMaxLimit.MinId = _notificationMinObjectId;
                                    ObjectMinMaxLimit.MaxId = _notificationMaxObjectId;
                                    ObjectMinMaxLimit.IsSet = true;
                                }
                                break;
                            case BacnetObjectType.ObjectEventEnrollment:
                                if (!ObjectMinMaxLimit.IsSet)
                                {
                                    ObjectMinMaxLimit.MinId = _eventEnrollementMinObjectId;
                                    ObjectMinMaxLimit.MaxId = _eventEnrollementMaxObjectId;
                                    ObjectMinMaxLimit.IsSet = true;
                                }
                                break;
                            case BacnetObjectType.ObjectCalendar:
                                if (!ObjectMinMaxLimit.IsSet)
                                {
                                    ObjectMinMaxLimit.MinId = _calenderMinObjectId;
                                    ObjectMinMaxLimit.MaxId = _calenderMaxObjectId;
                                    ObjectMinMaxLimit.IsSet = true;
                                }
                                break;
                            case BacnetObjectType.ObjectMultiStateValue:
                                if (!ObjectMinMaxLimit.IsSet)
                                {
                                    ObjectMinMaxLimit.MinId = _msvMinObjectId;
                                    ObjectMinMaxLimit.MaxId = _msvMaxObjectId;
                                    ObjectMinMaxLimit.IsSet = true;
                                }
                                break;
                            case BacnetObjectType.ObjectAnalogValue:
                                if (!ObjectMinMaxLimit.IsSet)
                                {
                                    if (isKeySpecificID)
                                    {
                                        if (objectFindKey.ToUpperInvariant() == AppResource.BioDynamicControlName.ToUpperInvariant())
                                        {
                                            ObjectMinMaxLimit.MinId = _avBioControlMinObjectId;
                                            ObjectMinMaxLimit.MaxId = _avBioControlMaxObjectId;
                                        }
                                        else if (objectFindKey.ToUpperInvariant() == AppResource.SaturationControlName.ToUpperInvariant())
                                        {
                                            ObjectMinMaxLimit.MinId = _avImControlMinObjectId;
                                            ObjectMinMaxLimit.MaxId = _avImControlMaxObjectId;
                                        }
                                        else if (objectFindKey.ToUpperInvariant() == AppResource.AnalogValueBlindObjectName.ToUpperInvariant())
                                        {
                                            ObjectMinMaxLimit.MinId = _avBlindAllMinObjectId;
                                            ObjectMinMaxLimit.MaxId = _avBlindAllMaxObjectId;
                                        }
                                    }
                                    else
                                    {
                                        ObjectMinMaxLimit.MinId = _avMinObjectId;
                                        ObjectMinMaxLimit.MaxId = _avMaxObjectId;
                                    }
                                    ObjectMinMaxLimit.IsSet = true;
                                }
                                break;
                            case BacnetObjectType.ObjectBinaryValue:
                                if (!ObjectMinMaxLimit.IsSet)
                                {
                                    ObjectMinMaxLimit.MinId = _bvMinObjectId;
                                    ObjectMinMaxLimit.MaxId = _bvMaxObjectId;
                                    ObjectMinMaxLimit.IsSet = true;
                                }
                                break;
                            case BacnetObjectType.ObjectPositiveIntegerValue:
                                ZoneEntityModel zoneEntityModel = EntityHelper.GetEntityModelFromDeviceID(deviceId) as ZoneEntityModel;
                                switch (zoneEntityModel.ZoneType)
                                {
                                    case AppConstants.ZoneTypes.UserSpace:
                                    case AppConstants.ZoneTypes.GeneralSpace:
                                        if (!ObjectMinMaxLimit.IsSet)
                                        {
                                            ObjectMinMaxLimit.MinId = _pivUserSpaceMinObjectId;
                                            ObjectMinMaxLimit.MaxId = _pivUserSpaceMaxObjectId;
                                            ObjectMinMaxLimit.IsSet = true;
                                        }
                                        break;
                                    case AppConstants.ZoneTypes.BeaconSpace:
                                        if (!ObjectMinMaxLimit.IsSet)
                                        {
                                            ObjectMinMaxLimit.MinId = _pivBeaconSpaceMinObjectId;
                                            ObjectMinMaxLimit.MaxId = _pivBeaconSpaceMaxObjectId;
                                            ObjectMinMaxLimit.IsSet = true;
                                        }
                                        break;

                                }
                                break;
                        }

                        break;

                    case AppConstants.EntityTypes.Fixture:
                        ObjectMinMaxLimit = _objectMinMaxIDLimit[deviceId][entityType][key];

                        if (!ObjectMinMaxLimit.IsSet)
                        {
                            ObjectMinMaxLimit.MaxId = _fixtureMaxObjectId;
                            ObjectMinMaxLimit.MinId = _fixtureMinObjectId;
                            ObjectMinMaxLimit.IsSet = true;
                        }
                        break;
                    case AppConstants.EntityTypes.Sensor:
                        ObjectMinMaxLimit = _objectMinMaxIDLimit[deviceId][entityType][key];

                        switch (objectType)
                        {
                            case BacnetObjectType.ObjectBinaryInput:
                                if (!ObjectMinMaxLimit.IsSet)
                                {
                                    ObjectMinMaxLimit.MaxId = _bIsensorMaxObjectId;
                                    ObjectMinMaxLimit.MinId = _bIsensorMinObjectId;
                                    ObjectMinMaxLimit.IsSet = true;
                                }
                                break;
                            case BacnetObjectType.ObjectAnalogInput:
                                if (!ObjectMinMaxLimit.IsSet)
                                {
                                    ObjectMinMaxLimit.MaxId = _aIsensorMaxObjectId;
                                    ObjectMinMaxLimit.MinId = _aIsensorMinObjectId;
                                    ObjectMinMaxLimit.IsSet = true;
                                }
                                break;
                            default:
                                break;
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(LogLevel.ERROR, "ObjectIdentifier: SetMinMaxObjectId:" + ex.Message);
            }
        }
        #endregion Private Methods

    }
}
