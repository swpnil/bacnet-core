﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              DeleteCallbackManager
///   Description:        Call back manager to delete objects in stack and maintain the same in the mapping file.
///   Author:             Rohit Galgali               
///   Date:               MM/DD/YY
#endregion


using Molex.BACnet.Gateway.Log;
using Molex.BACnet.Gateway.ServiceCore.Constants;
using Molex.BACnet.Gateway.ServiceCore.Helpers;
using Molex.BACnet.Gateway.ServiceCore.Models.Entity;
using Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel;
using Molex.BACnet.Gateway.ServiceCore.Repository;
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Response;
using System;
using System.Linq;

namespace Molex.BACnet.Gateway.ServiceCore.Managers
{
    internal class DeleteCallbackManager
    {
        internal static void DeleteFeatureObjects(StackDataObjects.Response.AutoResponses.DeleteObjectNotification deleteObjectNotification)
        {
            deleteObjectNotification.IsSucceed = true;
            EntityBaseModel entityBaseModel = EntityHelper.GetEntityModelFromDeviceID(deleteObjectNotification.DeleteObjectResponse.DeviceId);

            if (entityBaseModel == null)
            {
                return;
            }

            PersistedModels model;
            if (AppCoreRepo.Instance.PersistentModelCache.ContainsKey(entityBaseModel.MappingKey))
            {
                model = AppCoreRepo.Instance.PersistentModelCache[entityBaseModel.MappingKey];
            }
            else
            {
                if (VerifyNotADefaultNotificationClass(entityBaseModel, deleteObjectNotification))
                {
                    Logger.Instance.Log(LogLevel.DEBUG, "Cannot delete object. Default Notification class: " + deleteObjectNotification.DeleteObjectResponse.DeviceId);
                    deleteObjectNotification.ErrorModel = new ErrorResponseModel();
                    deleteObjectNotification.ErrorModel.Type = ErrorType.StackError;
                    deleteObjectNotification.IsSucceed = false;
                    deleteObjectNotification.ErrorModel.ErrorCode = (int)StackDataObjects.Constants.BacnetErrorCode.ErrorCodeObjectDeletionNotPermitted;
                    deleteObjectNotification.ErrorModel.ErrorClass = (int)StackDataObjects.Constants.BacnetErrorClass.ErrorClassObject;
                    return;
                }
                Logger.Instance.Log(LogLevel.DEBUG, "Cannot delete object. Object not found for device id: " + deleteObjectNotification.DeleteObjectResponse.DeviceId);
                deleteObjectNotification.ErrorModel = new ErrorResponseModel();
                deleteObjectNotification.ErrorModel.Type = ErrorType.StackError;
                deleteObjectNotification.IsSucceed = false;
                deleteObjectNotification.ErrorModel.ErrorCode = (int)StackDataObjects.Constants.BacnetErrorCode.ErrorCodeNoObjectsOfSpecifiedType;
                deleteObjectNotification.ErrorModel.ErrorClass = (int)StackDataObjects.Constants.BacnetErrorClass.ErrorClassObject;
                return;
            }

            bool isSuccess = RemoveFromPersistentModel(deleteObjectNotification, model, entityBaseModel);

            if (!isSuccess)
                return;

            isSuccess = StackManager.Instance.RemoveObject((uint)deleteObjectNotification.DeleteObjectResponse.DeviceId, deleteObjectNotification.DeleteObjectResponse.ObjectType, (uint)deleteObjectNotification.DeleteObjectResponse.ObjectId);

            if (!isSuccess)
            {
                Logger.Instance.Log(LogLevel.ERROR, "DeleteCallbackManager.DeleteFeatureObjects, Failed to delete object for object type:" + deleteObjectNotification.DeleteObjectResponse.ObjectType.ToString());
                return;
            }

            bool isRemoveObjectIdSuccess = ObjectIdentifierManager.Instance.RemoveObjectId((uint)deleteObjectNotification.DeleteObjectResponse.DeviceId, deleteObjectNotification.DeleteObjectResponse.ObjectType, (int)deleteObjectNotification.DeleteObjectResponse.ObjectId, entityBaseModel.EntityType);

            if (!isRemoveObjectIdSuccess)
            {
                Logger.Instance.Log(LogLevel.ERROR, "DeleteCallbackManager.DeleteFeatureObjects,Failed to remove created dynamic object id from object identifier list with id: " + deleteObjectNotification.DeleteObjectResponse.ObjectId + ",entity type: " + entityBaseModel.EntityType + ",object type: " + deleteObjectNotification.DeleteObjectResponse.ObjectType + ",device id: " + deleteObjectNotification.DeleteObjectResponse.DeviceId);
                return;
            }


            if (AppCoreRepo.Instance.PersistentModelCache.ContainsKey(entityBaseModel.MappingKey))
            {
                AppCoreRepo.Instance.PersistentModelCache[entityBaseModel.MappingKey] = model;
            }
            else
            {
                AppCoreRepo.Instance.PersistentModelCache.Add(entityBaseModel.MappingKey, model);
            }

            PersistenceStorageManager.Instance.Store(EntityHelper.GetControllerEntityModel(entityBaseModel), AppCoreRepo.Instance.PersistentModelCache);

        }

        private static bool RemoveFromPersistentModel(StackDataObjects.Response.AutoResponses.DeleteObjectNotification deleteObjectNotification, PersistedModels model, EntityBaseModel entityBaseModel)
        {
            try
            {
                switch (deleteObjectNotification.DeleteObjectResponse.ObjectType)
                {
                    case BacnetObjectType.ObjectCalendar:
                        {
                            var itemToRemove = model.Calendars.Single(r => r.ObjectID == deleteObjectNotification.DeleteObjectResponse.ObjectId);
                            model.Calendars.Remove(itemToRemove);
                        }
                        break;

                    case BacnetObjectType.ObjectEventEnrollment:
                        {
                            var itemToRemove = model.EventEnrollments.Single(r => r.ObjectID == deleteObjectNotification.DeleteObjectResponse.ObjectId);
                            model.EventEnrollments.Remove(itemToRemove);
                        }
                        break;

                    case BacnetObjectType.ObjectNotificationClass:
                        {
                            if (deleteObjectNotification.DeleteObjectResponse.ObjectType == BacnetObjectType.ObjectNotificationClass)
                            {
                                if (VerifyNotADefaultNotificationClass(entityBaseModel, deleteObjectNotification))
                                {
                                    deleteObjectNotification.ErrorModel = new ErrorResponseModel();
                                    deleteObjectNotification.ErrorModel.Type = ErrorType.StackError;
                                    deleteObjectNotification.IsSucceed = false;
                                    deleteObjectNotification.ErrorModel.ErrorCode = (int)StackDataObjects.Constants.BacnetErrorCode.ErrorCodeObjectDeletionNotPermitted;
                                    deleteObjectNotification.ErrorModel.ErrorClass = (int)StackDataObjects.Constants.BacnetErrorClass.ErrorClassObject;
                                    return false;
                                }
                            }
                            var itemToRemove = model.NotificationClasses.Single(r => r.ObjectID == deleteObjectNotification.DeleteObjectResponse.ObjectId);
                            model.NotificationClasses.Remove(itemToRemove);
                        }
                        break;

                    case BacnetObjectType.ObjectSchedule:
                        {
                            var itemToRemove = model.Schedules.Single(r => r.ObjectID == deleteObjectNotification.DeleteObjectResponse.ObjectId);
                            model.Schedules.Remove(itemToRemove);
                        }
                        break;

                    case BacnetObjectType.ObjectTrendlog:
                        {
                            var itemToRemove = model.TrendLogs.Single(r => r.ObjectID == deleteObjectNotification.DeleteObjectResponse.ObjectId);
                            model.TrendLogs.Remove(itemToRemove);
                        }
                        break;
                    default:
                        return false;
                }
                return true;
            }
            catch (Exception)
            {
                Logger.Instance.Log(LogLevel.ERROR, "DeleteCallbackManager.RemoveFromPersistentModel, error occured for removing object type: " + deleteObjectNotification.DeleteObjectResponse.ObjectType);
                return false;
            }
        }

        private static bool VerifyNotADefaultNotificationClass(EntityBaseModel entityBaseModel, StackDataObjects.Response.AutoResponses.DeleteObjectNotification deleteObjectNotification)
        {
            string key = EntityHelper.GetObjectDetailsKey(deleteObjectNotification.DeleteObjectResponse.ObjectType, AppConstants.LIGHTING_NOTIFICATIONCLASS_OBJECT);
            if (entityBaseModel.BACnetData.ObjectDetails[key].ObjectID == deleteObjectNotification.DeleteObjectResponse.ObjectId)
            {
                return true;
            }
            return false;
        }
    }
}
