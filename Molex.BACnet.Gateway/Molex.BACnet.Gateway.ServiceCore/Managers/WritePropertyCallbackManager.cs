﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              WritePropertyCallbackManager
///   Description:        Manages callback from BACnet stack
///   Author:             Prasad Joshi                 
///   Date:               06/13/17
#endregion

using Molex.BACnet.Gateway.Log;
using Molex.BACnet.Gateway.ServiceCore.AppResources;
using Molex.BACnet.Gateway.ServiceCore.Constants;
using Molex.BACnet.Gateway.ServiceCore.Helpers;
using Molex.BACnet.Gateway.ServiceCore.Helpers.PersistentDataConverters;
using Molex.BACnet.Gateway.ServiceCore.Models;
using Molex.BACnet.Gateway.ServiceCore.Models.BACnet;
using Molex.BACnet.Gateway.ServiceCore.Models.Entity;
using Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel;
using Molex.BACnet.Gateway.ServiceCore.Repository;
using Molex.StackDataObjects.APIModels;
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Models;
using Molex.StackDataObjects.Response;
using Molex.StackDataObjects.Response.AutoResponses;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Molex.BACnet.Gateway.ServiceCore.Managers
{
    /// <summary>
    /// Manages callback from BACnet stack
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>06-07-201712:35 PM</TimeStamp>
    public class WritePropertyCallbackManager
    {
        private const uint MinutesValueInSeconds = 60;

        #region PublicMethods

        /// <summary>
        /// To handle Write Property action at Building Level from BMS
        /// </summary>
        /// <param name="key">The key is used to identify the object type .</param>
        /// <param name="response">The response object is used as action delegate for write property.</param>
        public bool WritePropertyBuildingLevel(string key, AutoResponse response)
        {
            bool IsSucceed = false;
            WritePropertyNotificationResponse writePropertyNotificationResponse = response as WritePropertyNotificationResponse;
            BuildingEntityModel buildingEntityModel = (BuildingEntityModel)AppCoreRepo.Instance.BACnetObjectEntityLookUp[key];
            BacnetObjectType bacnetObjectType;
            string Objectkey = string.Empty;
            bacnetObjectType = BacnetObjectType.ObjectAnalogValue;

            string ObjKey = EntityHelper.GetObjectKey(writePropertyNotificationResponse, AppCoreRepo.Instance.BACnetObjectEntityLookUp[key]);

            if (buildingEntityModel.BACnetData.ObjectDetails[ObjKey].IsOutOfService)
            {
                return true;
            }
            if (buildingEntityModel.Parent.BACnetData.SystemStatus == BacnetDeviceStatus.StatusNonOperational)
            {
                writePropertyNotificationResponse.ErrorModel = new ErrorResponseModel();
                writePropertyNotificationResponse.ErrorModel.ErrorClass = (int)StackDataObjects.Constants.BacnetErrorClass.ErrorClassDevice;
                writePropertyNotificationResponse.ErrorModel.ErrorCode = (int)StackDataObjects.Constants.BacnetErrorCode.ErrorCodeOperationalProblem;
                writePropertyNotificationResponse.IsSucceed = false;
                return false;
            }

            switch (writePropertyNotificationResponse.WritePropertyCallbackData.ObjectType)
            {
                case BacnetObjectType.ObjectAnalogValue:
                    {
                        //string brightnesskey = EntityHelper.GetObjectDetailsKey(BacnetObjectType.ObjectAnalogValue, AppConstants.LIGHTING_BRIGHTENESS_OBJECT);

                        //if (brightnesskey == null)
                        //{
                        //    Logger.Instance.Log(LogLevel.ERROR, "WritePropertyCallbackManager.WritePropertyBuildingLevel: Analog value Present value updation failed: " + writePropertyNotificationResponse.WritePropertyCallbackData.DeviceId);
                        //}

                        Objectkey = ObjKey;
                        bacnetObjectType = BacnetObjectType.ObjectAnalogValue;
                        break;
                    }
                case BacnetObjectType.ObjectBinaryValue:
                    {
                        Objectkey = ObjKey;
                        bacnetObjectType = BacnetObjectType.ObjectBinaryValue;
                        break;
                    }
                case BacnetObjectType.ObjectMultiStateValue:
                    {
                        string lightscene = EntityHelper.GetObjectDetailsKey(BacnetObjectType.ObjectMultiStateValue, AppConstants.LIGHTING_LIGHTSCENE_OBJECT);

                        if (lightscene == null)
                        {
                            Logger.Instance.Log(LogLevel.ERROR, "WritePropertyCallbackManager.WritePropertyBuildingLevel: Multistate value Present value updation failed: " + writePropertyNotificationResponse.WritePropertyCallbackData.DeviceId);
                        }

                        Objectkey = lightscene;
                        bacnetObjectType = BacnetObjectType.ObjectMultiStateValue;
                        break;
                    }
                default:
                    {
                        Logger.Instance.Log(LogLevel.ERROR, "WritePropertyCallbackManager.WritePropertyBuildingLevel: Present value updation failed: " + writePropertyNotificationResponse.WritePropertyCallbackData.DeviceId);
                        break;
                    }
            }

            if (String.IsNullOrEmpty(Objectkey))
            {
                Logger.Instance.Log(LogLevel.ERROR, "WritePropertyCallbackManager.WritePropertyBuildingLevel : " + writePropertyNotificationResponse.WritePropertyCallbackData.ObjectType +
                    "Type Present value validation failed object " + writePropertyNotificationResponse.WritePropertyCallbackData.ObjectInstance + " For device Id " + writePropertyNotificationResponse.WritePropertyCallbackData.DeviceId);
                response.ErrorModel = new ErrorResponseModel();
                response.ErrorModel.Type = ErrorType.StackError;
                response.IsSucceed = false;
                response.ErrorModel.ErrorCode = (int)StackDataObjects.Constants.BacnetErrorCode.ErrorCodeOther;
                response.ErrorModel.ErrorClass = (int)StackDataObjects.Constants.BacnetErrorClass.ErrorClassObject;
                return false;
            }
            //API call to pass present value change from BMS to CoreSync server
            ResultModel<string> result = WriteObjectTypeProperty(writePropertyNotificationResponse.WritePropertyCallbackData.ObjectType,
                                  buildingEntityModel, writePropertyNotificationResponse.WritePropertyCallbackData.PropertyValue, writePropertyNotificationResponse.WritePropertyCallbackData.ObjectInstance, writePropertyNotificationResponse.WritePropertyCallbackData.ObjectProperty);


            //IsSucceed = result.IsSuccess;
            List<string> failZoneIds = new List<string>();

            if (!result.IsSuccess)
            {
                try
                {
                    if (result.Error != null && result.Error.ErrorCategory == AppConstants.ERROR_CATEGORY_MODELDATA)
                    {
                        failZoneIds = GetFailedZoneIDs(result.Data);
                    }
                    else
                    {
                        Logger.Instance.Log(LogLevel.ERROR, result.Error.OptionalMessage);
                        return result.IsSuccess;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Instance.Log(ex, LogLevel.ERROR, "WritePropertyCallbackManager.WritePropertyBuildingLevel :GetFailedzoneIDs from molex api result  failed");
                    return false;
                }
            }


            bool isAllFloorSucceed = true;
            BacnetDataType dataType;

            if (writePropertyNotificationResponse.WritePropertyCallbackData.InputValue == null)
                dataType = BacnetDataType.BacnetDTNull;
            else
                dataType = writePropertyNotificationResponse.WritePropertyCallbackData.DataType;

            foreach (FloorEntityModel floorEntityModel in buildingEntityModel.Childs)
            {
                bool isAllZoneSucceed = true;

                foreach (ZoneEntityModel zoneEntityModel in floorEntityModel.Childs)
                {
                    if (failZoneIds.Contains(zoneEntityModel.EntityKey))
                    {
                        Logger.Instance.Log(LogLevel.ERROR, "WritePropertyCallbackManager.WritePropertyBuildingLevel :Fail to update present value property of object: " + writePropertyNotificationResponse.WritePropertyCallbackData.ObjectType + "under zone: " + zoneEntityModel.EntityName);
                        isAllZoneSucceed = false;
                    }
                    else
                    {
                        string zoneEntityObjectKey = string.Empty;
                        zoneEntityObjectKey = Objectkey;
                        if (writePropertyNotificationResponse.WritePropertyCallbackData.ObjectType == BacnetObjectType.ObjectAnalogValue)
                        {
                            if (Objectkey == bacnetObjectType.ToString() + "_" + buildingEntityModel.EntityType + " " + AppResource.UserLightLevel)
                            {
                                if (zoneEntityModel.ZoneType != AppConstants.ZoneTypes.UserSpace)
                                    continue;
                                zoneEntityObjectKey = EntityHelper.GetObjectDetailsKey(BacnetObjectType.ObjectAnalogValue, AppConstants.LIGHTING_BRIGHTENESS_OBJECT);
                            }
                            else if (Objectkey == bacnetObjectType.ToString() + "_" + buildingEntityModel.EntityType + " " + AppResource.GeneralLightLevel)
                            {
                                if (zoneEntityModel.ZoneType != AppConstants.ZoneTypes.GeneralSpace)
                                    continue;
                                zoneEntityObjectKey = EntityHelper.GetObjectDetailsKey(BacnetObjectType.ObjectAnalogValue, AppConstants.LIGHTING_BRIGHTENESS_OBJECT);
                            }
                            else if (Objectkey == bacnetObjectType.ToString() + "_" + buildingEntityModel.EntityType + " " + AppResource.BeaconLightLevel)
                            {
                                if (zoneEntityModel.ZoneType != AppConstants.ZoneTypes.BeaconSpace)
                                    continue;
                                zoneEntityObjectKey = EntityHelper.GetObjectDetailsKey(BacnetObjectType.ObjectAnalogValue, AppConstants.LIGHTING_BRIGHTENESS_OBJECT);
                            }
                        }
                        ResultModel<int> zonedeviceId = EntityHelper.GetDeviceId(zoneEntityModel);
                        if (!zoneEntityModel.BACnetData.ObjectDetails.ContainsKey(zoneEntityObjectKey))
                            continue;
                        object value = writePropertyNotificationResponse.WritePropertyCallbackData.InputValue;
                        if (bacnetObjectType == BacnetObjectType.ObjectMultiStateValue)
                        {
                            value = Convert.ToInt32(GetLightSceneIndexParameter(zoneEntityModel, Convert.ToInt32(value.ToString()), AppConstants.EntityTypes.Zone));
                        }
                        IsSucceed = StackManager.Instance.WriteProperty((uint)zonedeviceId.Data, bacnetObjectType,
                                   zoneEntityModel.BACnetData.ObjectDetails[zoneEntityObjectKey].ObjectID, value,
                                    writePropertyNotificationResponse.WritePropertyCallbackData.ObjectProperty, writePropertyNotificationResponse.WritePropertyCallbackData.ArrayIndex,
                                    writePropertyNotificationResponse.WritePropertyCallbackData.Priority, writePropertyNotificationResponse.WritePropertyCallbackData.ArrayIndexPresent,
                                    dataType);
                    }
                }

                if (isAllZoneSucceed)
                {
                    ResultModel<int> floordeviceId = EntityHelper.GetDeviceId(floorEntityModel);
                    string floorEntityObjectKey = string.Empty;
                    floorEntityObjectKey = Objectkey;
                    if (writePropertyNotificationResponse.WritePropertyCallbackData.ObjectType == BacnetObjectType.ObjectAnalogValue)
                    {
                        if (Objectkey == bacnetObjectType.ToString() + "_" + buildingEntityModel.EntityType + " " + AppResource.UserLightLevel)
                            floorEntityObjectKey = bacnetObjectType.ToString() + "_" + floorEntityModel.EntityType + " " + AppResource.UserLightLevel;
                        else if (Objectkey == bacnetObjectType.ToString() + "_" + buildingEntityModel.EntityType + " " + AppResource.GeneralLightLevel)
                            floorEntityObjectKey = bacnetObjectType.ToString() + "_" + floorEntityModel.EntityType + " " + AppResource.GeneralLightLevel;
                        else if (Objectkey == bacnetObjectType.ToString() + "_" + buildingEntityModel.EntityType + " " + AppResource.BeaconLightLevel)
                            floorEntityObjectKey = bacnetObjectType.ToString() + "_" + floorEntityModel.EntityType + " " + AppResource.BeaconLightLevel;
                    }
                    if (!floorEntityModel.BACnetData.ObjectDetails.ContainsKey(floorEntityObjectKey))
                        continue;
                    IsSucceed = StackManager.Instance.WriteProperty((uint)floordeviceId.Data, bacnetObjectType,
                                floorEntityModel.BACnetData.ObjectDetails[floorEntityObjectKey].ObjectID, writePropertyNotificationResponse.WritePropertyCallbackData.InputValue,
                                 writePropertyNotificationResponse.WritePropertyCallbackData.ObjectProperty, writePropertyNotificationResponse.WritePropertyCallbackData.ArrayIndex,
                                 writePropertyNotificationResponse.WritePropertyCallbackData.Priority, writePropertyNotificationResponse.WritePropertyCallbackData.ArrayIndexPresent,
                                  dataType);
                }
                else
                {
                    isAllFloorSucceed = false;

                    if (result.Error.ErrorCode == AppConstants.ERROR_LIGHTSCENE_NOT_SUPPORTED)
                    {
                        writePropertyNotificationResponse.ErrorModel = new ErrorResponseModel();
                        writePropertyNotificationResponse.ErrorModel.ErrorClass = (int)StackDataObjects.Constants.BacnetErrorClass.ErrorClassProperty;
                        writePropertyNotificationResponse.ErrorModel.ErrorCode = (int)StackDataObjects.Constants.BacnetErrorCode.ErrorCodeValueOutOfRange;
                        writePropertyNotificationResponse.IsSucceed = false;
                    }
                }
            }

            IsSucceed = isAllFloorSucceed;

            return IsSucceed;
        }

        /// <summary>
        /// To handle Write Property action at Floor Level from BMS
        /// </summary>
        /// <param name="key">The key is used to identify the object type.</param>
        /// <param name="response">The response object is used as action delegate for write property.</param>
        public bool WritePropertyFloorLevel(string key, AutoResponse response)
        {
            BacnetObjectType bacnetObjectType;
            string Objectkey = string.Empty;
            bacnetObjectType = BacnetObjectType.ObjectAnalogValue;
            WritePropertyNotificationResponse writePropertyNotificationResponse = response as WritePropertyNotificationResponse;
            FloorEntityModel floorEntityModel = (FloorEntityModel)AppCoreRepo.Instance.BACnetObjectEntityLookUp[key];

            string objectkey = EntityHelper.GetObjectKey(writePropertyNotificationResponse, AppCoreRepo.Instance.BACnetObjectEntityLookUp[key]);

            if (floorEntityModel.BACnetData.ObjectDetails[objectkey].IsOutOfService)
            {
                return true;
            }
            if (floorEntityModel.Parent.Parent.BACnetData.SystemStatus == BacnetDeviceStatus.StatusNonOperational)
            {
                writePropertyNotificationResponse.ErrorModel = new ErrorResponseModel();
                writePropertyNotificationResponse.ErrorModel.ErrorClass = (int)StackDataObjects.Constants.BacnetErrorClass.ErrorClassDevice;
                writePropertyNotificationResponse.ErrorModel.ErrorCode = (int)StackDataObjects.Constants.BacnetErrorCode.ErrorCodeOperationalProblem;
                writePropertyNotificationResponse.IsSucceed = false;
                return false;
            }

            switch (writePropertyNotificationResponse.WritePropertyCallbackData.ObjectType)
            {
                case BacnetObjectType.ObjectAnalogValue:
                    {
                        //string brightnesskey = EntityHelper.GetObjectDetailsKey(BacnetObjectType.ObjectAnalogValue, AppConstants.LIGHTING_BRIGHTENESS_OBJECT);

                        //if (brightnesskey == null)
                        //{
                        //    Logger.Instance.Log(LogLevel.ERROR, "WritePropertyCallbackManager.WritePropertyFloorLevel: Analog value Present value updation failed: " + writePropertyNotificationResponse.WritePropertyCallbackData.DeviceId);
                        //}

                        Objectkey = objectkey;
                        bacnetObjectType = BacnetObjectType.ObjectAnalogValue;
                        break;
                    }
                case BacnetObjectType.ObjectMultiStateValue:
                    {
                        string lightscene = EntityHelper.GetObjectDetailsKey(BacnetObjectType.ObjectMultiStateValue, AppConstants.LIGHTING_LIGHTSCENE_OBJECT);

                        if (lightscene == null)
                        {
                            Logger.Instance.Log(LogLevel.ERROR, "WritePropertyCallbackManager.WritePropertyFloorLevel: Multistate value Present value updation failed: " + writePropertyNotificationResponse.WritePropertyCallbackData.DeviceId);
                        }

                        Objectkey = lightscene;
                        bacnetObjectType = BacnetObjectType.ObjectMultiStateValue;
                        break;
                    }
                default:
                    {
                        Logger.Instance.Log(LogLevel.ERROR, "WritePropertyCallbackManager.WritePropertyFloorLevel: Present value updation failed: " + writePropertyNotificationResponse.WritePropertyCallbackData.DeviceId);
                        break;
                    }
            }

            //API call to pass present value changed value from BMS to CoreSync server
            ResultModel<string> result = WriteObjectTypeProperty(writePropertyNotificationResponse.WritePropertyCallbackData.ObjectType,
                                      floorEntityModel, writePropertyNotificationResponse.WritePropertyCallbackData.PropertyValue, writePropertyNotificationResponse.WritePropertyCallbackData.ObjectInstance, writePropertyNotificationResponse.WritePropertyCallbackData.ObjectProperty);


            List<string> failZoneIds = new List<string>();

            if (!result.IsSuccess)
            {
                try
                {
                    if (result.Error != null && result.Error.ErrorCategory == AppConstants.ERROR_CATEGORY_MODELDATA)
                    {
                        failZoneIds = GetFailedZoneIDs(result.Data);
                    }
                    else
                    {
                        Logger.Instance.Log(LogLevel.ERROR, result.Error.OptionalMessage);
                        return result.IsSuccess;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Instance.Log(ex, LogLevel.ERROR, "WritePropertyCallbackManager.WritePropertyFloorLevel :GetFailedzoneIDs from molex api result  failed");
                    return false;
                }
            }


            bool isAllZoneSucceed = true;
            BacnetDataType dataType;

            if (writePropertyNotificationResponse.WritePropertyCallbackData.InputValue == null)
                dataType = BacnetDataType.BacnetDTNull;
            else
                dataType = writePropertyNotificationResponse.WritePropertyCallbackData.DataType;

            foreach (ZoneEntityModel zoneEntityModel in floorEntityModel.Childs)
            {
                if (failZoneIds.Contains(zoneEntityModel.EntityKey))
                {
                    Logger.Instance.Log(LogLevel.ERROR, "WritePropertyCallbackManager.WritePropertyFloorLevel :Fail to update present value property of object: " + writePropertyNotificationResponse.WritePropertyCallbackData.ObjectType + "under zone: " + zoneEntityModel.EntityName);
                    isAllZoneSucceed = false;
                }
                else
                {
                    ResultModel<int> zonedeviceId = EntityHelper.GetDeviceId(zoneEntityModel);
                    string zoneEntityObjectKey = string.Empty;
                    zoneEntityObjectKey = Objectkey;
                    if (writePropertyNotificationResponse.WritePropertyCallbackData.ObjectType == BacnetObjectType.ObjectAnalogValue)
                    {
                        if (Objectkey == bacnetObjectType.ToString() + "_" + floorEntityModel.EntityType + " " + AppResource.UserLightLevel)
                        {
                            if (zoneEntityModel.ZoneType != AppConstants.ZoneTypes.UserSpace)
                                continue;
                            zoneEntityObjectKey = EntityHelper.GetObjectDetailsKey(BacnetObjectType.ObjectAnalogValue, AppConstants.LIGHTING_BRIGHTENESS_OBJECT);
                        }
                        else if (Objectkey == bacnetObjectType.ToString() + "_" + floorEntityModel.EntityType + " " + AppResource.GeneralLightLevel)
                        {
                            if (zoneEntityModel.ZoneType != AppConstants.ZoneTypes.GeneralSpace)
                                continue;
                            zoneEntityObjectKey = EntityHelper.GetObjectDetailsKey(BacnetObjectType.ObjectAnalogValue, AppConstants.LIGHTING_BRIGHTENESS_OBJECT);
                        }
                        else if (Objectkey == bacnetObjectType.ToString() + "_" + floorEntityModel.EntityType + " " + AppResource.BeaconLightLevel)
                        {
                            if (zoneEntityModel.ZoneType != AppConstants.ZoneTypes.BeaconSpace)
                                continue;
                            zoneEntityObjectKey = EntityHelper.GetObjectDetailsKey(BacnetObjectType.ObjectAnalogValue, AppConstants.LIGHTING_BRIGHTENESS_OBJECT);
                        }
                    }
                    if (!zoneEntityModel.BACnetData.ObjectDetails.ContainsKey(zoneEntityObjectKey))
                        continue;
                    object value = writePropertyNotificationResponse.WritePropertyCallbackData.InputValue;
                    if (bacnetObjectType == BacnetObjectType.ObjectMultiStateValue)
                    {
                        value = Convert.ToInt32(GetLightSceneIndexParameter(zoneEntityModel, Convert.ToInt32(value.ToString()), AppConstants.EntityTypes.Zone));
                    }
                    bool IsSucceed = StackManager.Instance.WriteProperty((uint)zonedeviceId.Data, bacnetObjectType,
                               zoneEntityModel.BACnetData.ObjectDetails[zoneEntityObjectKey].ObjectID, value,
                                writePropertyNotificationResponse.WritePropertyCallbackData.ObjectProperty, writePropertyNotificationResponse.WritePropertyCallbackData.ArrayIndex,
                                writePropertyNotificationResponse.WritePropertyCallbackData.Priority, writePropertyNotificationResponse.WritePropertyCallbackData.ArrayIndexPresent,
                                dataType);
                }

            }

            if (!isAllZoneSucceed)
            {
                if (result.Error.ErrorCode == AppConstants.ERROR_LIGHTSCENE_NOT_SUPPORTED)
                {
                    writePropertyNotificationResponse.ErrorModel = new ErrorResponseModel();
                    writePropertyNotificationResponse.ErrorModel.ErrorClass = (int)StackDataObjects.Constants.BacnetErrorClass.ErrorClassProperty;
                    writePropertyNotificationResponse.ErrorModel.ErrorCode = (int)StackDataObjects.Constants.BacnetErrorCode.ErrorCodeValueOutOfRange;
                    writePropertyNotificationResponse.IsSucceed = false;
                }
            }

            return isAllZoneSucceed;
        }

        /// <summary>
        /// To handle Write Property action at Zone Level from BMS
        /// </summary>
        /// <param name="key">The key is used to identify the object type.</param>
        /// <param name="response">The response object is used as action delegate for write property.</param>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>30-05-201714:36</TimeStamp>"
        public bool WritePropertyZoneLevel(string key, AutoResponse response)
        {
            WritePropertyNotificationResponse writePropertyNotificationResponse = response as WritePropertyNotificationResponse;
            EntityBaseModel entityModel = AppCoreRepo.Instance.BACnetObjectEntityLookUp[key];

            string objectkey = EntityHelper.GetObjectKey(writePropertyNotificationResponse, AppCoreRepo.Instance.BACnetObjectEntityLookUp[key]);

            if (entityModel.BACnetData.ObjectDetails[objectkey].IsOutOfService)
            {
                return true;
            }

            if (writePropertyNotificationResponse.WritePropertyCallbackData.InputValue == null)
            {
                entityModel.BACnetData.ObjectDetails[objectkey].PresentValue = AppConstants.ANALOG_VALUE_OBJECT_UNKNOWN_PRESENT_VALUE;
            }
            if (entityModel.BACnetData.SystemStatus == BacnetDeviceStatus.StatusNonOperational)
            {
                writePropertyNotificationResponse.ErrorModel = new ErrorResponseModel();
                writePropertyNotificationResponse.ErrorModel.ErrorClass = (int)StackDataObjects.Constants.BacnetErrorClass.ErrorClassDevice;
                writePropertyNotificationResponse.ErrorModel.ErrorCode = (int)StackDataObjects.Constants.BacnetErrorCode.ErrorCodeOperationalProblem;
                writePropertyNotificationResponse.IsSucceed = false;
                return false;
            }

            uint objectID = writePropertyNotificationResponse.WritePropertyCallbackData.ObjectInstance;

            //API call to pass present value changed value from BMS to CoreSync server
            ResultModel<string> result = WriteObjectTypeProperty(writePropertyNotificationResponse.WritePropertyCallbackData.ObjectType,
                                           ((ZoneEntityModel)entityModel), writePropertyNotificationResponse.WritePropertyCallbackData.PropertyValue, objectID, writePropertyNotificationResponse.WritePropertyCallbackData.ObjectProperty);

            if (IsSchedulingCallback(writePropertyNotificationResponse.WritePropertyCallbackData.BacnetAddress.IPAddress))
            {
                ScheduleRetryManager.Instance.ProcessRetry(writePropertyNotificationResponse, result.IsSuccess);
            }

            if (!result.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.ERROR, "WritePropertyCallbackManager.WritePropertyZoneLevel:Error occurred while write property at zone-" + ((ZoneEntityModel)entityModel).EntityName + "- Error :" + result.Error.OptionalMessage);

                if (result.Error.ErrorCode == AppConstants.ERROR_LIGHTSCENE_NOT_SUPPORTED)
                {
                    writePropertyNotificationResponse.ErrorModel = new ErrorResponseModel();
                    writePropertyNotificationResponse.ErrorModel.ErrorClass = (int)StackDataObjects.Constants.BacnetErrorClass.ErrorClassProperty;
                    writePropertyNotificationResponse.ErrorModel.ErrorCode = (int)StackDataObjects.Constants.BacnetErrorCode.ErrorCodeValueOutOfRange;
                    writePropertyNotificationResponse.IsSucceed = false;
                }
                return false;
            }
            return true;
        }

        private bool IsSchedulingCallback(string ipAddress)
        {
            long addr;
            long.TryParse(ipAddress, out addr);
            if (addr != 0)
            {
                return false;
            }
            return true;
        }

        private int GetLightSceneIndexParameter(EntityBaseModel model, int InputValue, AppConstants.EntityTypes requestedEntityType)
        {
            if (model == null)
                return 0;
            switch (requestedEntityType)
            {
                case AppConstants.EntityTypes.Zone:
                    string value = AppCoreRepo.Instance.AllLightScene.GetValue(--InputValue).ToString();
                    ZoneEntityModel zoneModel = model as ZoneEntityModel;
                    if (zoneModel.LightScene.Contains(value))
                        return zoneModel.LightScene.IndexOf(value) + 1;
                    break;
                default:
                    break;
            }
            return 0;
        }

        /// <summary>
        /// Writes the object type property.
        /// </summary>
        /// <param name="bacnetObjectType">Type of the bacnet object.</param>
        /// <param name="model">The model.</param>
        /// <param name="bacnetPropertyID">The bacnetPropertyID</param>
        /// <param name="objectID">The objectID</param>
        /// <param name="propertyValue">The propertyValue</param>
        /// <returns>The result model</returns>
        /// <CreatedBy>prasad.joshi</CreatedBy><TimeStamp>6/2/201710:42 AM</TimeStamp>
        private ResultModel<string> WriteObjectTypeProperty(BacnetObjectType bacnetObjectType, EntityBaseModel model, object propertyValue, uint objectID = 0, BacnetPropertyID? bacnetPropertyID = null)
        {
            ResultModel<string> result = null;
            try
            {
                switch (bacnetObjectType)
                {
                    case BacnetObjectType.ObjectAnalogValue:
                        {
                            if (model as ZoneEntityModel != null)
                            {
                                if (((ZoneEntityModel)(model)).ZoneType == AppConstants.ZoneTypes.BlindSpace)
                                    result = HTTPAPIManager.Instance.UpdateZoneBlindLevel(model, Convert.ToInt32(propertyValue), objectID);
                                else
                                {
                                    if (model.BACnetData.ObjectDetails.Any(s => s.Value.ObjectID == objectID && s.Value.ObjectType == bacnetObjectType))
                                    {
                                        BACnetObjectDetails bacnetObj = model.BACnetData.ObjectDetails.Where(s => s.Value.ObjectID == objectID && s.Value.ObjectType == bacnetObjectType).First().Value;
                                        if (bacnetObj.ObjectName == AppConstants.LIGHTING_BRIGHTENESS_OBJECT)
                                            result = HTTPAPIManager.Instance.UpdateZoneBrightness(model, Convert.ToInt32(propertyValue), objectID);
                                        else if (bacnetObj.ObjectName == AppResource.BioDynamicControlName)
                                            result = HTTPAPIManager.Instance.UpdateZoneControlLevel(model, "biodynamic", Convert.ToInt32(propertyValue));
                                        else if (bacnetObj.ObjectName == AppResource.SaturationControlName)
                                            result = HTTPAPIManager.Instance.UpdateZoneControlLevel(model, "saturation", Convert.ToInt32(propertyValue));
                                        else
                                            result = HTTPAPIManager.Instance.UpdateZoneBrightness(model, Convert.ToInt32(propertyValue), objectID);
                                    }
                                    else
                                        result = HTTPAPIManager.Instance.UpdateZoneBrightness(model, Convert.ToInt32(propertyValue));
                                }
                            }
                            else
                            {
                                if (model.BACnetData.ObjectDetails.Any(s => s.Value.ObjectID == objectID && s.Value.ObjectType == bacnetObjectType))
                                {
                                    BACnetObjectDetails bacnetObj = model.BACnetData.ObjectDetails.Where(s => s.Value.ObjectID == objectID && s.Value.ObjectType == bacnetObjectType).First().Value;
                                    if (bacnetObj.ObjectName == model.EntityName + " " + AppResource.UserLightLevel)
                                        result = HTTPAPIManager.Instance.UpdateZoneBrightness(model, Convert.ToInt32(propertyValue), 0, AppConstants.ZoneTypes.UserSpace);
                                    else if (bacnetObj.ObjectName == model.EntityName + " " + AppResource.GeneralLightLevel)
                                        result = HTTPAPIManager.Instance.UpdateZoneBrightness(model, Convert.ToInt32(propertyValue), 0, AppConstants.ZoneTypes.GeneralSpace);
                                    else if (bacnetObj.ObjectName == model.EntityName + " " + AppResource.BeaconLightLevel)
                                        result = HTTPAPIManager.Instance.UpdateZoneBrightness(model, Convert.ToInt32(propertyValue), 0, AppConstants.ZoneTypes.BeaconSpace);
                                    else if (bacnetObj.ObjectName == model.EntityName + " " + AppResource.BioDynamicControlName)
                                        result = HTTPAPIManager.Instance.UpdateZoneControlLevel(model, "biodynamic", Convert.ToInt32(propertyValue));
                                    else if (bacnetObj.ObjectName == model.EntityName + " " + AppResource.SaturationControlName)
                                        result = HTTPAPIManager.Instance.UpdateZoneControlLevel(model, "saturation", Convert.ToInt32(propertyValue));
                                    else
                                        result = HTTPAPIManager.Instance.UpdateZoneBlindLevel(model, Convert.ToInt32(propertyValue), objectID);
                                }
                                else
                                    result = HTTPAPIManager.Instance.UpdateZoneBrightness(model, Convert.ToInt32(propertyValue));
                            }
                            //result = HTTPAPIManager.Instance.UpdateZoneBrightness(model, Convert.ToInt32(propertyValue));
                            Logger.Instance.Log(LogLevel.DEBUG, "Brightness is set for model name : " + model.EntityName + ", result: " + result.IsSuccess);
                            return result;
                        }

                    case BacnetObjectType.ObjectMultiStateValue:
                        {
                            BACnetObjectDetails bacnetObj = model.BACnetData.ObjectDetails.Where(s => s.Value.ObjectID == objectID && s.Value.ObjectType == bacnetObjectType).First().Value;
                            if (bacnetObj.ObjectName == AppConstants.LIGHTING_MOOD_OBJECT)
                            {
                                switch (bacnetPropertyID)
                                {
                                    case BacnetPropertyID.PropRelinquishDefault:
                                    case BacnetPropertyID.PropPresentValue:
                                        int indexOfMood = Convert.ToInt16(propertyValue);
                                        string mood = AppCoreRepo.Instance.AvailableMoods.GetValue(--indexOfMood).ToString();
                                        result = HTTPAPIManager.Instance.UpdateMood(model, mood);
                                        Logger.Instance.Log(LogLevel.DEBUG, "Lightscene is set for model name : " + model.EntityName + ", result: " + result.IsSuccess);
                                        return result;
                                    default:
                                        Logger.Instance.Log(LogLevel.ERROR, "WriteObjectTypeProperty: BACnet property type " + bacnetPropertyID + " not found of object multistate value");
                                        return result;
                                }
                            }
                            else if (bacnetObj.ObjectName == AppConstants.RESET_ATTRIBUTES_AFTER_TIMEOUT)
                            {
                                int indexOfResetAttrib = Convert.ToInt16(propertyValue);
                                result = HTTPAPIManager.Instance.UpdateResetAttributeAfterTimeOut(model, indexOfResetAttrib);
                                Logger.Instance.Log(LogLevel.DEBUG, AppConstants.RESET_ATTRIBUTES_AFTER_TIMEOUT + " is set for model name : " + model.EntityName + ", result: " + result.IsSuccess);
                                return result;
                            }
                            else if (bacnetObj.ObjectName == "Beacon Palettes")
                            {
                                switch (bacnetPropertyID)
                                {
                                    case BacnetPropertyID.PropRelinquishDefault:
                                    case BacnetPropertyID.PropPresentValue:
                                        int indexOfMood = Convert.ToInt16(propertyValue);
                                        string mood = GetBeaconPalette(model, --indexOfMood);
                                        result = HTTPAPIManager.Instance.UpdateMood(model, mood);
                                        Logger.Instance.Log(LogLevel.DEBUG, "Lightscene is set for model name : " + model.EntityName + ", result: " + result.IsSuccess);
                                        return result;
                                    default:
                                        Logger.Instance.Log(LogLevel.ERROR, "WriteObjectTypeProperty: BACnet property type " + bacnetPropertyID + " not found of object multistate value");
                                        return result;
                                }
                            }
                            else
                            {
                                switch (bacnetPropertyID)
                                {
                                    case BacnetPropertyID.PropRelinquishDefault:
                                    case BacnetPropertyID.PropPresentValue:
                                        int indexOfLightScene = Convert.ToInt16(propertyValue);
                                        string lightscene = GetLightScene(model, indexOfLightScene); //AppCoreRepo.Instance.AllLightScene.GetValue(--indexOfLightScene).ToString();
                                        result = HTTPAPIManager.Instance.UpdateLightScene(model, lightscene);
                                        Logger.Instance.Log(LogLevel.DEBUG, "Lightscene is set for model name : " + model.EntityName + ", result: " + result.IsSuccess);
                                        return result;
                                    default:
                                        Logger.Instance.Log(LogLevel.ERROR, "WriteObjectTypeProperty: BACnet property type " + bacnetPropertyID + " not found of object multistate value");
                                        return result;
                                }
                            }

                        }
                    case BacnetObjectType.ObjectPositiveIntegerValue:
                        {
                            EntityBaseModel entityBaseModel = AppCoreRepo.Instance.ZonesLookUp[model.EntityKey];
                            ZoneEntityModel zoneEntityModel = entityBaseModel as ZoneEntityModel;
                            uint presentValue;

                            switch (zoneEntityModel.ZoneType)
                            {
                                case AppConstants.ZoneTypes.BeaconSpace:
                                    presentValue = Convert.ToUInt32(propertyValue);
                                    BeaconZoneEntityModel beaconColorModel = CommonHelper.GetRGBValues(presentValue);
                                    result = HTTPAPIManager.Instance.UpdateBeaconColor(entityBaseModel, beaconColorModel,objectID);
                                    break;
                                case AppConstants.ZoneTypes.UserSpace:
                                case AppConstants.ZoneTypes.GeneralSpace:
                                    string dhTargetLux = EntityHelper.GetObjectDetailsKey(BacnetObjectType.ObjectPositiveIntegerValue, AppConstants.LIGHTING_DHTARGETLUX_OBJECT);
                                    string presRate = EntityHelper.GetObjectDetailsKey(BacnetObjectType.ObjectPositiveIntegerValue, AppConstants.LIGHTING_PRESRATE_OBJECT);
                                    string presTimeout = EntityHelper.GetObjectDetailsKey(BacnetObjectType.ObjectPositiveIntegerValue, AppConstants.LIGHTING_PRESTIMEOUT_OBJECT);

                                    if (entityBaseModel.BACnetData.ObjectDetails.ContainsKey(dhTargetLux)
                                        && entityBaseModel.BACnetData.ObjectDetails[dhTargetLux].ObjectID == objectID)
                                    {
                                        presentValue = Convert.ToUInt32(propertyValue);
                                        result = HTTPAPIManager.Instance.UpdateDHTargetData(model, presentValue);
                                    }
                                    if (entityBaseModel.BACnetData.ObjectDetails.ContainsKey(presRate)
                                         && entityBaseModel.BACnetData.ObjectDetails[presRate].ObjectID == objectID)
                                    {
                                        presentValue = Convert.ToUInt32(propertyValue) * AppConstants.PRESENT_RATE_RATIO;
                                        result = HTTPAPIManager.Instance.UpdatepresRate(model, presentValue);
                                    }
                                    if (entityBaseModel.BACnetData.ObjectDetails.ContainsKey(presTimeout)
                                         && entityBaseModel.BACnetData.ObjectDetails[presTimeout].ObjectID == objectID)
                                    {
                                        presentValue = Convert.ToUInt32(propertyValue) * AppConstants.MILLISECONDS_TO_SECOND;
                                        result = HTTPAPIManager.Instance.UpdatepresTimeout(model, presentValue);
                                    }
                                    break;

                            }

                            Logger.Instance.Log(LogLevel.DEBUG, "PresentIntegerValue is set for zone id : " + model.EntityKey + ", result: " + result.IsSuccess);
                            return result;
                        }
                    case BacnetObjectType.ObjectBinaryValue:
                        {
                            int indexOfState = Convert.ToInt32(propertyValue);
                            string state = AppCoreRepo.Instance.AvailableZoneStates.GetValue(indexOfState).ToString();

                            if (model.EntityType == AppConstants.EntityTypes.Building)
                            {                                
                                result = HTTPAPIManager.Instance.UpdateOnOffControlLevel(model, "state", state);
                                if (result.IsSuccess)
                                {
                                    MonitoringDataManager _MonitoringDataManager = new MonitoringDataManager();
                                    foreach (var floor in model.Childs)
                                    {
                                        _MonitoringDataManager.UpdateFloorOnOffControl(floor.EntityKey, indexOfState);
                                    }
                                    _MonitoringDataManager = null;

                                }
                                return result;
                            }
                            else if (model.EntityType == AppConstants.EntityTypes.Floor)
                            {
                                result = HTTPAPIManager.Instance.UpdateOnOffControlLevel(model, "state", state);
                                return result;
                            }
                            else
                            {
                                result = HTTPAPIManager.Instance.UpdateZoneState(model, state);
                                Logger.Instance.Log(LogLevel.DEBUG, "Zone state is set for model name : " + model.EntityName + ", result: " + result.IsSuccess);
                                return result;
                            }
                        }
                    default:
                        {
                            result = new ResultModel<string>();
                            result.IsSuccess = false;
                            result.Error = new ErrorModel();
                            result.Error.ErrorCategory = AppConstants.ERROR_CATEGORY_SERVER;
                            result.Error.ErrorCode = AppConstants.ERROR_CATEGORY_SERVER_UNAVAILABLE;
                            Logger.Instance.Log(LogLevel.ERROR, "ObjectPropertyWritter: BACnet Object type " + bacnetObjectType + " not found");
                            return result;
                        }
                }
            }
            catch (Exception e)
            {
                result = new ResultModel<string>();
                result.IsSuccess = false;
                result.Error = new ErrorModel();
                result.Error.ErrorCategory = AppConstants.ERROR_CATEGORY_SERVER;
                result.Error.ErrorCode = AppConstants.ERROR_CATEGORY_SERVER_UNAVAILABLE;
                Logger.Instance.Log(e, LogLevel.ERROR, "ObjectPropertyWritter: BACnet Object type " + bacnetObjectType + " failed");
                return result;
            }

        }

        private string GetLightScene(EntityBaseModel model, int indexOfLightScene)
        {
            if (model is ZoneEntityModel)
            {
                ZoneEntityModel zoneModel = model as ZoneEntityModel;
                return (zoneModel as ZoneEntityModel).LightScene.ToArray().GetValue(--indexOfLightScene).ToString();
            }
            else
            {
                return AppCoreRepo.Instance.AllLightScene.GetValue(--indexOfLightScene).ToString();
            }
        }

        private string GetBeaconPalette(EntityBaseModel model, int indexOfLightScene)
        {
            try
            {
                if (model is ZoneEntityModel)
                {
                    ZoneEntityModel zoneModel = model as ZoneEntityModel;
                    return (zoneModel as ZoneEntityModel).BeaconPalettes.ToArray().GetValue(indexOfLightScene).ToString();
                }
                else
                {
                    return AppCoreRepo.Instance.AllLightScene.GetValue(--indexOfLightScene).ToString();
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "WritePropertyCallbackManager:GetBeaconPalettesList: Error occured");
                return string.Empty;
            }
        }

        /// <summary>
        /// Updates the name description.
        /// </summary>
        /// <param name="writePropertyNotificationResponse">The write property notification response.</param>
        /// <CreatedBy>Rohit Galgali</CreatedBy>
        /// <TimeStamp>7/7/20173:23 PM</TimeStamp>
        public void UpdateNameDescription(WritePropertyNotificationResponse writePropertyNotificationResponse)
        {
            //TODO: Need to discussion to handle situation for write to file exception and notify to statck or write file with stack thread
            try
            {
                EntityBaseModel entityBaseModel = null;
                switch (writePropertyNotificationResponse.WritePropertyCallbackData.ObjectType)
                {
                    case BacnetObjectType.ObjectCalendar:
                    case BacnetObjectType.ObjectEventEnrollment:
                    case BacnetObjectType.ObjectNotificationClass:
                    case BacnetObjectType.ObjectSchedule:
                    case BacnetObjectType.ObjectTrendlog:
                        {
                            entityBaseModel = UpdateFeatureObjectsNameAndDescription(writePropertyNotificationResponse);
                        }
                        break;
                    default:
                        {
                            entityBaseModel = UpdateObjectNameDescription(writePropertyNotificationResponse);
                        }
                        break;
                }

                if (entityBaseModel == null)
                    return;

                PersistenceStorageManager.Instance.Store(EntityHelper.GetControllerEntityModel(entityBaseModel), AppCoreRepo.Instance.PersistentModelCache);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "WritePropertyCallbackManager:UpdateNameDescription: Error occured");
            }

        }

        private EntityBaseModel UpdateFeatureObjectsNameAndDescription(WritePropertyNotificationResponse writePropertyNotificationResponse)
        {
            EntityBaseModel entityBaseModel = EntityHelper.GetEntityModelFromDeviceID(writePropertyNotificationResponse.WritePropertyCallbackData.DeviceId);

            PersistentBase persistedModel = GetPersistedModel(writePropertyNotificationResponse, entityBaseModel);

            string propertyValue = Convert.ToString(writePropertyNotificationResponse.WritePropertyCallbackData.PropertyValue);

            switch (writePropertyNotificationResponse.WritePropertyCallbackData.ObjectProperty)
            {
                case BacnetPropertyID.PropDescription:
                    persistedModel.Description = propertyValue;
                    break;
                case BacnetPropertyID.PropObjectName:
                    persistedModel.Name = propertyValue;
                    break;
            }

            return entityBaseModel;
        }

        private static EntityBaseModel UpdateObjectNameDescription(WritePropertyNotificationResponse writePropertyNotificationResponse)
        {
            string objectKey = string.Empty;

            string key = CommonHelper.GenerateBACnetObjectMappedID(writePropertyNotificationResponse.WritePropertyCallbackData.DeviceId,
                                                                   writePropertyNotificationResponse.WritePropertyCallbackData.ObjectType,
                                                                   writePropertyNotificationResponse.WritePropertyCallbackData.ObjectInstance);
            EntityBaseModel entityBaseModel = AppCoreRepo.Instance.BACnetObjectEntityLookUp[key];

            string propertyValue = Convert.ToString(writePropertyNotificationResponse.WritePropertyCallbackData.PropertyValue);

            objectKey = EntityHelper.GetObjectKey(writePropertyNotificationResponse, entityBaseModel);

            switch (writePropertyNotificationResponse.WritePropertyCallbackData.ObjectProperty)
            {
                case BacnetPropertyID.PropDescription:
                    entityBaseModel.BACnetData.ObjectDetails[objectKey].ObjectDescription = propertyValue;
                    break;
                case BacnetPropertyID.PropObjectName:
                    entityBaseModel.BACnetData.ObjectDetails[objectKey].ObjectName = propertyValue;
                    break;
            }
            return entityBaseModel;
        }

        private PersistentBase GetPersistedModel(WritePropertyNotificationResponse writePropertyNotificationResponse, EntityBaseModel entityBaseModel)
        {
            PersistentBase model = null;
            switch (writePropertyNotificationResponse.WritePropertyCallbackData.ObjectType)
            {
                case BacnetObjectType.ObjectCalendar:
                    model = AppCoreRepo.Instance.PersistentModelCache[entityBaseModel.MappingKey].Calendars.Where(x => x.ObjectID == writePropertyNotificationResponse.WritePropertyCallbackData.ObjectInstance).FirstOrDefault();
                    break;

                case BacnetObjectType.ObjectEventEnrollment:
                    model = AppCoreRepo.Instance.PersistentModelCache[entityBaseModel.MappingKey].EventEnrollments.Where(x => x.ObjectID == writePropertyNotificationResponse.WritePropertyCallbackData.ObjectInstance).FirstOrDefault();
                    break;

                case BacnetObjectType.ObjectSchedule:
                    model = AppCoreRepo.Instance.PersistentModelCache[entityBaseModel.MappingKey].Schedules.Where(x => x.ObjectID == writePropertyNotificationResponse.WritePropertyCallbackData.ObjectInstance).FirstOrDefault();
                    break;

                case BacnetObjectType.ObjectTrendlog:
                    model = AppCoreRepo.Instance.PersistentModelCache[entityBaseModel.MappingKey].TrendLogs.Where(x => x.ObjectID == writePropertyNotificationResponse.WritePropertyCallbackData.ObjectInstance).FirstOrDefault();
                    break;

                case BacnetObjectType.ObjectNotificationClass:
                    {
                        //This logic is implemented since one Notification class is created for each VRD and VD.
                        // To handle changes for this particular object null checks and key checks are included.
                        if (AppCoreRepo.Instance.PersistentModelCache.ContainsKey(entityBaseModel.MappingKey))
                        {
                            model = AppCoreRepo.Instance.PersistentModelCache[entityBaseModel.MappingKey].NotificationClasses.Where(x => x.ObjectID == writePropertyNotificationResponse.WritePropertyCallbackData.ObjectInstance).FirstOrDefault();
                        }
                        else
                        {
                            AppCoreRepo.Instance.PersistentModelCache[entityBaseModel.MappingKey] = new PersistedModels();
                        }

                        if (model == null)
                        {
                            NotificationClassPersistedModel notificationObject = new NotificationClassPersistedModel();
                            notificationObject.ObjectID = writePropertyNotificationResponse.WritePropertyCallbackData.ObjectInstance;
                            AppCoreRepo.Instance.PersistentModelCache[entityBaseModel.MappingKey].NotificationClasses.Add(notificationObject);
                            model = AppCoreRepo.Instance.PersistentModelCache[entityBaseModel.MappingKey].NotificationClasses.Where(x => x.ObjectID == writePropertyNotificationResponse.WritePropertyCallbackData.ObjectInstance).FirstOrDefault();
                        }
                    }
                    break;
            }

            return model;
        }

        /// <summary>
        /// Updates the out of service value. Call back of outofservice value is handled
        /// </summary>
        /// <param name="writePropertyNotificationResponse">The write property notification response.</param>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>7/12/20174:26 PM</TimeStamp>
        public void UpdateOutOfServiceValue(WritePropertyNotificationResponse writePropertyNotificationResponse)
        {
            string key = CommonHelper.GenerateBACnetObjectMappedID(writePropertyNotificationResponse.WritePropertyCallbackData.DeviceId,
                                                                   writePropertyNotificationResponse.WritePropertyCallbackData.ObjectType,
                                                                   writePropertyNotificationResponse.WritePropertyCallbackData.ObjectInstance);
            EntityBaseModel entityBaseModel = AppCoreRepo.Instance.BACnetObjectEntityLookUp[key];

            bool propertyValue = Convert.ToBoolean(writePropertyNotificationResponse.WritePropertyCallbackData.PropertyValue);

            string objectKey = EntityHelper.GetObjectKey(writePropertyNotificationResponse, entityBaseModel);

            if (entityBaseModel.BACnetData.ObjectDetails.ContainsKey(objectKey))
            {
                entityBaseModel.BACnetData.ObjectDetails[objectKey].IsOutOfService = propertyValue;

                if (propertyValue == false) // Setting present value as unknow due to which through monitoring flow it will be updated to stack
                {
                    entityBaseModel.BACnetData.ObjectDetails[objectKey].PresentValue = AppConstants.ANALOG_VALUE_OBJECT_UNKNOWN_PRESENT_VALUE;
                    entityBaseModel.BACnetData.ObjectDetails[objectKey].Reliability = BacnetReliability.MaxReliability;
                }
            }
            else
            {
                Logger.Instance.Log(LogLevel.DEBUG, "WritePropertyCallbackManager.UpdateOutOfService Unknown key setter for OutOfService property");
            }
        }

        #endregion PublicMethods

        #region Update Object Properties

        internal void WriteAPDUValues(WritePropertyNotificationResponse writePropertyNotificationResponse)
        {
            EntityBaseModel entityBaseModel = EntityHelper.GetEntityModelFromDeviceID(writePropertyNotificationResponse.WritePropertyCallbackData.DeviceId);

            string objectKey = EntityHelper.GetObjectKey(writePropertyNotificationResponse, entityBaseModel);

            if (entityBaseModel.BACnetData.ObjectDetails.ContainsKey(objectKey))
            {
                PersistedModels model;
                if (AppCoreRepo.Instance.PersistentModelCache.ContainsKey(entityBaseModel.MappingKey))
                {
                    model = AppCoreRepo.Instance.PersistentModelCache[entityBaseModel.MappingKey];
                }
                else
                {
                    model = new PersistedModels();
                    model.DeviceID = (uint)entityBaseModel.BACnetData.DeviceID;

                    AppCoreRepo.Instance.PersistentModelCache.Add(entityBaseModel.MappingKey, model);
                }

                switch (writePropertyNotificationResponse.WritePropertyCallbackData.ObjectProperty)
                {
                    case BacnetPropertyID.PropNumberOfApduRetries:
                        model.ObjectDevice.NumberOfAPDURetries = Convert.ToInt16(writePropertyNotificationResponse.WritePropertyCallbackData.PropertyValue);
                        break;
                    case BacnetPropertyID.PropApduTimesOut:
                        model.ObjectDevice.APDUTimeout = Convert.ToInt16(writePropertyNotificationResponse.WritePropertyCallbackData.PropertyValue);
                        break;
                    case BacnetPropertyID.PropApduSegmentTimesOut:
                        model.ObjectDevice.APDUSegmentTimeOut = Convert.ToInt16(writePropertyNotificationResponse.WritePropertyCallbackData.PropertyValue);
                        break;
                }

                PersistenceStorageManager.Instance.Store(EntityHelper.GetControllerEntityModel(entityBaseModel), AppCoreRepo.Instance.PersistentModelCache);
            }

            else
            {
                Logger.Instance.Log(LogLevel.DEBUG, "WritePropertyCallbackManager.WriteAPDUValues Unknown key setter for APDU properties");
            }
        }


        internal void WriteSchduleObjectProperties(WritePropertyNotificationResponse writePropertyNotificationResponse)
        {
            EntityBaseModel model = EntityHelper.GetEntityModelFromDeviceID(writePropertyNotificationResponse.WritePropertyCallbackData.DeviceId);
            if (model == null)
                return;

            SchedulePersistedModel schedulePersistentModel = (SchedulePersistedModel)GetPersistedModel(writePropertyNotificationResponse, model);

            switch (writePropertyNotificationResponse.WritePropertyCallbackData.ObjectProperty)
            {
                case BacnetPropertyID.PropEffectivePeriod:
                    {
                        schedulePersistentModel.EffectivePeriod = DateRangeConverter.ConvertToDateRangeModel((BacnetDateRangeModel)writePropertyNotificationResponse.WritePropertyCallbackData.PropertyValue);
                    }
                    break;
                case BacnetPropertyID.PropPriorityForWriting:
                    {
                        schedulePersistentModel.PriorityForWriting = (uint)writePropertyNotificationResponse.WritePropertyCallbackData.PropertyValue;
                    }
                    break;
                case BacnetPropertyID.PropListOfObjectPropertyReferences:
                    {
                        List<BACnetDevObjPropRefModel> bacnetDevObjPropReferences = (List<BACnetDevObjPropRefModel>)writePropertyNotificationResponse.WritePropertyCallbackData.PropertyValue;

                        schedulePersistentModel.ObjectPropertyRefernces.Clear();

                        if (bacnetDevObjPropReferences == null)
                        {
                            AppCoreRepo.Instance.ScheduledObjects.Remove(writePropertyNotificationResponse.WritePropertyCallbackData.ObjectInstance);
                            break;
                        }

                        if (!VerifyForZoneLevelObjects(bacnetDevObjPropReferences, model, writePropertyNotificationResponse.WritePropertyCallbackData.ObjectInstance))
                        {
                            writePropertyNotificationResponse.ErrorModel = new ErrorResponseModel();
                            writePropertyNotificationResponse.ErrorModel.ErrorClass = (int)BacnetErrorClass.ErrorClassProperty;
                            writePropertyNotificationResponse.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeValueOutOfRange;
                            writePropertyNotificationResponse.IsSucceed = false;
                            return;
                        }

                        foreach (var bacnetDevObjRef in bacnetDevObjPropReferences)
                        {
                            schedulePersistentModel.ObjectPropertyRefernces.Add(DevObjPropRefModelConverter.ConvertToDevObjPropRefModel((BACnetDevObjPropRefModel)bacnetDevObjRef));
                            if (!AppCoreRepo.Instance.ScheduledObjects.ContainsKey(writePropertyNotificationResponse.WritePropertyCallbackData.ObjectInstance))
                            {
                                AppCoreRepo.Instance.ScheduledObjects.Add(writePropertyNotificationResponse.WritePropertyCallbackData.ObjectInstance, CommonHelper.GenerateScheduleObjectMappedID((uint)model.BACnetData.DeviceID, ((BACnetDevObjPropRefModel)bacnetDevObjRef).ObjectType, ((BACnetDevObjPropRefModel)bacnetDevObjRef).PropertyIdentifier, ((BACnetDevObjPropRefModel)bacnetDevObjRef).ObjId));
                            }
                        }
                    }
                    break;
                case BacnetPropertyID.PropWeeklySchedule:
                    {
                        IEnumerable bacnetTimeValueArrayModel = (IEnumerable)writePropertyNotificationResponse.WritePropertyCallbackData.PropertyValue;
                        schedulePersistentModel.WeeklySchedule.Clear();

                        foreach (var bacnetTimeValueModel in bacnetTimeValueArrayModel)
                        {
                            if (!VerifyTimeValueListModel(((BacnetTimeValueArrayModel)bacnetTimeValueModel).BacnetTimeValueList))
                            {
                                writePropertyNotificationResponse.ErrorModel = new ErrorResponseModel();
                                writePropertyNotificationResponse.ErrorModel.ErrorClass = (int)BacnetErrorClass.ErrorClassProperty;
                                writePropertyNotificationResponse.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeDatatypeNotSupported;
                                writePropertyNotificationResponse.IsSucceed = false;
                                return;
                            }
                            schedulePersistentModel.WeeklySchedule.Add(TimeValueArrayModelConverter.ConvertToTimeValueArrayModel((BacnetTimeValueArrayModel)bacnetTimeValueModel));
                        }
                    }
                    break;
                case BacnetPropertyID.PropExceptionSchedule:
                    {
                        IEnumerable bacnetExceptionScheduleModel = (IEnumerable)writePropertyNotificationResponse.WritePropertyCallbackData.PropertyValue;
                        schedulePersistentModel.ExceptionSchedule.Clear();

                        foreach (var bacnetExceptionSchedule in bacnetExceptionScheduleModel)
                        {
                            if (!VerifyTimeValueListModel(((BacnetSpecialEventModel)bacnetExceptionSchedule).BacnetTimeValue))
                            {
                                writePropertyNotificationResponse.ErrorModel = new ErrorResponseModel();
                                writePropertyNotificationResponse.ErrorModel.ErrorClass = (int)BacnetErrorClass.ErrorClassProperty;
                                writePropertyNotificationResponse.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeDatatypeNotSupported;
                                writePropertyNotificationResponse.IsSucceed = false;
                                return;
                            }
                            schedulePersistentModel.ExceptionSchedule.Add(SpecialEventModelConverter.ConvertToSpecialEventModel((BacnetSpecialEventModel)bacnetExceptionSchedule));
                        }
                    }
                    break;
                case BacnetPropertyID.PropScheduleDefault:
                    {
                        if (!VerifyDataTypeOfValueModel((BacnetValueModel)writePropertyNotificationResponse.WritePropertyCallbackData.PropertyValue))
                        {
                            writePropertyNotificationResponse.ErrorModel = new ErrorResponseModel();
                            writePropertyNotificationResponse.ErrorModel.ErrorClass = (int)BacnetErrorClass.ErrorClassProperty;
                            writePropertyNotificationResponse.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeDatatypeNotSupported;
                            writePropertyNotificationResponse.IsSucceed = false;
                            return;
                        }
                        schedulePersistentModel.ScheduleDefault = ValueModelConverter.ConvertToValueModel((BacnetValueModel)writePropertyNotificationResponse.WritePropertyCallbackData.PropertyValue);
                    }
                    break;
            }

            PersistenceStorageManager.Instance.Store(EntityHelper.GetControllerEntityModel(model), AppCoreRepo.Instance.PersistentModelCache);
        }

        private bool VerifyTimeValueListModel(List<BacnetTimeValueModel> bacnetTimeValueModels)
        {
            foreach (var timeValueModel in bacnetTimeValueModels)
            {
                foreach (var valueModel in timeValueModel.Values)
                {
                    if (!VerifyDataTypeOfValueModel(valueModel))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        private bool VerifyDataTypeOfValueModel(BacnetValueModel bacnetValueModel)
        {
            //This is a patch. Stack has a bug in their code which needs to be addressed.
            //When user sets null, stack should send proper valumodel with tag type as BACNET_APPLICATION_TAG_NULL
            if (bacnetValueModel == null)
            {
                return true;
            }
            BacnetApplicationTag tagValue = (BacnetApplicationTag)bacnetValueModel.TagType;

            if (tagValue != BacnetApplicationTag.BacnetApplicationTagReal &&
               tagValue != BacnetApplicationTag.BacnetApplicationTagUnsignedInt &&
               tagValue != BacnetApplicationTag.BacnetApplicationTagNull)
            {
                return false;
            }

            return true;
        }

        private bool IsValidTimeArray(List<BacnetTimeValueModel> timeList)
        {
            for (int index = 1; index < timeList.Count; index++)
            {
                for (int counter = 0; counter <= index - 1; counter++)
                {
                    if (timeList[index].Time.Hour == timeList[counter].Time.Hour && timeList[index].Time.Hundredths == timeList[counter].Time.Hundredths)
                    {
                        uint differenceInTime = (uint)(((timeList[index].Time.Min * MinutesValueInSeconds) + timeList[index].Time.Sec) - ((timeList[counter].Time.Min * MinutesValueInSeconds) + timeList[counter].Time.Sec));

                        if (differenceInTime < MinutesValueInSeconds)
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        private bool VerifyForZoneLevelObjects(List<BACnetDevObjPropRefModel> listOfObjectFromCallBack, EntityBaseModel model, uint scheduleObjectId)
        {
            //User has set null or empty value. Which is considered as valid scenario
            if (listOfObjectFromCallBack == null)
                return true;

            //Verify list has only one element in it
            if (listOfObjectFromCallBack.Count > 1)
            {
                return false;
            }

            //Get the callback value from the list. Since only one schedule is restricted getting value from 0 index.
            BACnetDevObjPropRefModel objectPropertyReference = listOfObjectFromCallBack[0];

            //Verify if object is for AV, MSV, PIV only
            if (objectPropertyReference.ObjectType != BacnetObjectType.ObjectAnalogValue &&
                objectPropertyReference.ObjectType != BacnetObjectType.ObjectMultiStateValue &&
                objectPropertyReference.ObjectType != BacnetObjectType.ObjectPositiveIntegerValue)
            {
                return false;
            }

            //Verify if any property other than present value or propriotery value(Mood) is added by user.
            if (objectPropertyReference.PropertyIdentifier != BacnetPropertyID.PropPresentValue)
            {
                return false;
            }

            //Verify only zone level objects are scheduled
            if (!model.BACnetData.ObjectDetails.Values.Where(x => x.ObjectType == objectPropertyReference.ObjectType).Any(x => x.ObjectID == objectPropertyReference.ObjId))
            {
                return false;
            }

            //Check across all the schedule objects within the device for duplicate entries.
            foreach (var scheduleObject in AppCoreRepo.Instance.ScheduledObjects)
            {
                if (scheduleObject.Key == scheduleObjectId)
                {
                    continue;
                }

                string objectKey = CommonHelper.GenerateScheduleObjectMappedID((uint)model.BACnetData.DeviceID, objectPropertyReference.ObjectType, objectPropertyReference.PropertyIdentifier, objectPropertyReference.ObjId);

                if (scheduleObject.Value == objectKey)
                {
                    return false;
                }
            }

            return true;
        }

        internal void WriteNotificationClassProperties(WritePropertyNotificationResponse writePropertyNotificationResponse)
        {
            EntityBaseModel model = EntityHelper.GetEntityModelFromDeviceID(writePropertyNotificationResponse.WritePropertyCallbackData.DeviceId);
            if (model == null)
                return;

            NotificationClassPersistedModel notificationPersistentModel = (NotificationClassPersistedModel)GetPersistedModel(writePropertyNotificationResponse, model);

            switch (writePropertyNotificationResponse.WritePropertyCallbackData.ObjectProperty)
            {
                case BacnetPropertyID.PropRecipientList:
                    {
                        notificationPersistentModel.RecipientList.Clear();
                        IEnumerable destinationModels = (IEnumerable)writePropertyNotificationResponse.WritePropertyCallbackData.PropertyValue;
                        foreach (var destinationModel in destinationModels)
                        {
                            notificationPersistentModel.RecipientList.Add(DestinationTypeConverter.ConvertToDestinationModel((BacnetDestinationModel)destinationModel));
                        }
                    }
                    break;

                case BacnetPropertyID.PropAckRequired:
                    {
                        notificationPersistentModel.AckRequired = EventTransitionModelConverter.ConvertToEventTransitionModel((EventTransitionBitsModel)writePropertyNotificationResponse.WritePropertyCallbackData.PropertyValue);
                    }
                    break;
                case BacnetPropertyID.PropPriority:
                    {
                        notificationPersistentModel.NotificationPriority = (writePropertyNotificationResponse.WritePropertyCallbackData.PropertyValue as uint[]).ToList();
                    }
                    break;
            }

            PersistenceStorageManager.Instance.Store(EntityHelper.GetControllerEntityModel(model), AppCoreRepo.Instance.PersistentModelCache);
        }

        internal void WriteTrendLogProperties(WritePropertyNotificationResponse writePropertyNotificationResponse)
        {
            EntityBaseModel model = EntityHelper.GetEntityModelFromDeviceID(writePropertyNotificationResponse.WritePropertyCallbackData.DeviceId);
            if (model == null)
                return;

            TrendLogPersistedModel trendLogPersistedModel = (TrendLogPersistedModel)GetPersistedModel(writePropertyNotificationResponse, model);

            switch (writePropertyNotificationResponse.WritePropertyCallbackData.ObjectProperty)
            {
                case BacnetPropertyID.PropStartTime:
                    {
                        trendLogPersistedModel.StartTime = DateTimeModelConverter.ConvertToDateTimeModel((BacnetDateTimeModel)writePropertyNotificationResponse.WritePropertyCallbackData.PropertyValue);
                    }
                    break;
                case BacnetPropertyID.PropStopTime:
                    {
                        trendLogPersistedModel.StopTime = DateTimeModelConverter.ConvertToDateTimeModel((BacnetDateTimeModel)writePropertyNotificationResponse.WritePropertyCallbackData.PropertyValue);
                    }
                    break;
                case BacnetPropertyID.PropStopWhenFull:
                    {
                        trendLogPersistedModel.StopWhenFull = (bool)writePropertyNotificationResponse.WritePropertyCallbackData.PropertyValue;
                    }
                    break;
                case BacnetPropertyID.PropEnable:
                    {
                        trendLogPersistedModel.Enable = (bool)writePropertyNotificationResponse.WritePropertyCallbackData.PropertyValue;
                    }
                    break;
                case BacnetPropertyID.PropLogInterval:
                    {
                        trendLogPersistedModel.LogInterval = (uint)writePropertyNotificationResponse.WritePropertyCallbackData.PropertyValue;
                    }
                    break;
                case BacnetPropertyID.PropLogDeviceObjectProperty:
                    {
                        trendLogPersistedModel.LogDeviceObject = DevObjPropRefModelConverter.ConvertToDevObjPropRefModel((BACnetDevObjPropRefModel)writePropertyNotificationResponse.WritePropertyCallbackData.PropertyValue);// (uint)writePropertyNotificationResponse.WritePropertyCallbackData.PropertyValue;
                    }
                    break;
            }

            PersistenceStorageManager.Instance.Store(EntityHelper.GetControllerEntityModel(model), AppCoreRepo.Instance.PersistentModelCache);
        }

        internal void WriteEventEnrollmentProperties(WritePropertyNotificationResponse writePropertyNotificationResponse)
        {
            EntityBaseModel model = EntityHelper.GetEntityModelFromDeviceID(writePropertyNotificationResponse.WritePropertyCallbackData.DeviceId);
            if (model == null)
                return;

            EventEnrollmentPersistedModel eventEnrollmentPersistentModel = (EventEnrollmentPersistedModel)GetPersistedModel(writePropertyNotificationResponse, model);

            switch (writePropertyNotificationResponse.WritePropertyCallbackData.ObjectProperty)
            {
                case BacnetPropertyID.PropNotifyType:
                    {
                        eventEnrollmentPersistentModel.NotificationType = (BacnetNotifyType)writePropertyNotificationResponse.WritePropertyCallbackData.PropertyValue;
                    }
                    break;
                case BacnetPropertyID.PropEventEnable:
                    {
                        eventEnrollmentPersistentModel.EventEnable = EventTransitionModelConverter.ConvertToEventTransitionModel((EventTransitionBitsModel)writePropertyNotificationResponse.WritePropertyCallbackData.PropertyValue);
                    }
                    break;
                case BacnetPropertyID.PropNotificationClass:
                    {
                        eventEnrollmentPersistentModel.NotificationClass = (uint)writePropertyNotificationResponse.WritePropertyCallbackData.PropertyValue;
                    }
                    break;
                case BacnetPropertyID.PropObjectPropertyReference:
                    {
                        eventEnrollmentPersistentModel.ObjectPropertyReference = DevObjPropRefModelConverter.ConvertToDevObjPropRefModel((BACnetDevObjPropRefModel)writePropertyNotificationResponse.WritePropertyCallbackData.PropertyValue); //DateTimeModelConverter.ConvertToDateTimeModel((BacnetDateTimeModel)writePropertyNotificationResponse.WritePropertyCallbackData.PropertyValue);
                    }
                    break;
                case BacnetPropertyID.PropEventParameters:
                    {
                        var eventParemetersModel = (EventParametersBase)writePropertyNotificationResponse.WritePropertyCallbackData.PropertyValue;
                        eventEnrollmentPersistentModel.EventType = eventParemetersModel.EventType;
                        switch (eventParemetersModel.EventType)
                        {
                            case BacnetEventType.EventChangeOfState:
                                eventEnrollmentPersistentModel.ChangeOfStateEventModel = ChangeOfStateConverter.ConvertToChangeOfStateModel((ChangeOfStateEventParametersModel)writePropertyNotificationResponse.WritePropertyCallbackData.PropertyValue);
                                break;

                            case BacnetEventType.EventCommandFailure:
                                {
                                    writePropertyNotificationResponse.ErrorModel = new ErrorResponseModel();
                                    writePropertyNotificationResponse.ErrorModel.ErrorClass = (int)BacnetErrorClass.ErrorClassProperty;
                                    writePropertyNotificationResponse.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeOptionalFunctionalityNotSupported;
                                    writePropertyNotificationResponse.IsSucceed = false;
                                    return;
                                }
                            //eventEnrollmentPersistentModel.CommandFailureModel = CommandFailureModelConverter.ConvertToCommandFailureModel((CommandFailureEventParametersModel)writePropertyNotificationResponse.WritePropertyCallbackData.PropertyValue);


                            case BacnetEventType.EventOutOfRange:
                                eventEnrollmentPersistentModel.OutOfRangeModel = OutOfRangeModelConverter.ConvertToOutOfRangeModel((OutOfRangeEventParametersModel)writePropertyNotificationResponse.WritePropertyCallbackData.PropertyValue);
                                break;
                        }
                    }
                    break;
            }

            PersistenceStorageManager.Instance.Store(EntityHelper.GetControllerEntityModel(model), AppCoreRepo.Instance.PersistentModelCache);
        }

        internal void WriteCalendarProperties(WritePropertyNotificationResponse writePropertyNotificationResponse)
        {
            EntityBaseModel model = EntityHelper.GetEntityModelFromDeviceID(writePropertyNotificationResponse.WritePropertyCallbackData.DeviceId);
            if (model == null)
                return;

            CalendarPersistedModel calendarPersistentModel = (CalendarPersistedModel)GetPersistedModel(writePropertyNotificationResponse, model);

            switch (writePropertyNotificationResponse.WritePropertyCallbackData.ObjectProperty)
            {
                case BacnetPropertyID.PropDateList:
                    {
                        IEnumerable bacnetDateListObjects = (IEnumerable)writePropertyNotificationResponse.WritePropertyCallbackData.PropertyValue;
                        calendarPersistentModel.DateList.Clear();
                        foreach (var dateValues in bacnetDateListObjects)
                        {
                            calendarPersistentModel.DateList.Add(CalendarEntryModelConverter.ConvertToCalendarEntryModel((BacnetCalendarEntryModel)dateValues));
                        }
                    }
                    break;
            }

            PersistenceStorageManager.Instance.Store(EntityHelper.GetControllerEntityModel(model), AppCoreRepo.Instance.PersistentModelCache);
        }

        #endregion

        #region privatemethod
        /// <summary>
        /// Gets the failed zone i ds.
        /// </summary>
        /// <param name="jsonData">The json data.</param>
        /// <returns></returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>9/13/20176:11 PM</TimeStamp>
        private List<string> GetFailedZoneIDs(string jsonData)
        {
            List<string> failZoneIds = new List<string>();

            if (!string.IsNullOrEmpty(jsonData))
            {
                string[] ZoneIDs = jsonData.Split(',');

                for (int i = 0; i < ZoneIDs.Length; i++)
                {
                    failZoneIds.Add(Convert.ToString(ZoneIDs[i]));
                }
            }

            return failZoneIds;
        }
        #endregion
    }
}
