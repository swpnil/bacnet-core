﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              HTTPAPIManager
///   Description:        This class is used to Manage API opertaions
///   Author:             Nazneen Zahid                  
///   Date:               05/23/17
///---------------------------------------------------------------------------------------------
#endregion

using Molex.BACnet.Gateway.Log;
using Molex.BACnet.Gateway.ServiceCore.AppResources;
using Molex.BACnet.Gateway.ServiceCore.Constants;
using Molex.BACnet.Gateway.ServiceCore.Helpers;
using Molex.BACnet.Gateway.ServiceCore.Models;
using Molex.BACnet.Gateway.ServiceCore.Models.APIModels;
using Molex.BACnet.Gateway.ServiceCore.Models.Entity;
using Molex.BACnet.Gateway.ServiceCore.Models.MolexAPIModels;
using Molex.BACnet.Gateway.ServiceCore.Models.SubsriberModels;
using Molex.BACnet.Gateway.ServiceCore.Repository;
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Response.AutoResponses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Managers
{
    /// <summary>
    /// This class is used to Manage API opertaions
    /// </summary>
    public class HTTPAPIManager : SingletonCore.SingletonBase<HTTPAPIManager>
    {
        #region PrivateVariable
        // it contains Base URL
        string _baseURL;

        // it contains Project URL
        string _projectURL;

        string _lightScenesURL;

        //it contains CoreSync server base url
        string _molexapiBaseURL;

        MolexAPIResponseModel _responseData;

        APIMolexParameterModel _apiMolexParameterModel;

        #endregion PrivateVariable




        #region PublicMethods

        /// <summary>
        /// Initializes the specified project URL.
        /// </summary>
        /// <param name="CoreSyncServerBaseURL">The CoreSync Server BaseURL.</param>
        /// <param name="password">The password</param>
        /// <param name="username">The username</param>
        /// <param name="projectId">The project id</param>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>26-05-201703:31 PM</TimeStamp>
        public void Init(string molexapiBaseURL, string username, string password, string projectId)
        {
            _molexapiBaseURL = molexapiBaseURL;
            PrepareUrls(_molexapiBaseURL, projectId);
            _apiMolexParameterModel = new APIMolexParameterModel();
            _apiMolexParameterModel.UserName = username;
            _apiMolexParameterModel.Password = password;
        }

        /// <summary>
        /// Deinitilization of HTTP API.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>09-06-201705:23 PM</TimeStamp>
        public void DeInit()
        {

        }


        /// <summary>
        /// This method will read Project File from CoreSync Server and it will return received data in Json string. 
        /// </summary>
        /// <returns>This will hold the GetProjectData operation execution result. If succeed then IsSuccess will be true and Data property will have JSON data.</returns>        
        public ResultModel<string> GetProjectData()
        {
            ResultModel<string> result = new ResultModel<string>();
            try
            {
                result = HTTPHelper.GetWebAPIAsync(_projectURL, _apiMolexParameterModel).Result;
                GetAPIRespResult(result);
            }
            catch (Exception ex)
            {
                SetRespOnException(result, ex.Message);
            }
            return result;
        }

        public ResultModel<string> GetProjectLightScenes()
        {
            ResultModel<string> result = new ResultModel<string>();
            try
            {
                result = HTTPHelper.GetWebAPIAsync(_lightScenesURL, _apiMolexParameterModel).Result;
                GetAPIRespResult(result);
            }
            catch (Exception ex)
            {
                SetRespOnException(result, ex.Message);
            }
            return result;
        }

        /// <summary>
        /// This method will read Project File from CoreSync Server and it will return received data in Json string. 
        /// </summary>
        /// <returns>This will hold the GetProjectData operation execution result. If succeed then IsSuccess will be true and Data property will have JSON data.</returns>        
        public ResultModel<string> GetProjectDevices(string projectID , string ID)
        {
            ResultModel<string> result = new ResultModel<string>();
            try
            {
                //result = HTTPHelper.GetWebAPIAsync(_projectURL, _apiMolexParameterModel).Result;
                string URL = _baseURL + "devices?type=building&id=" + ID + "&projectid=" + projectID + "&device=all";
                result = HTTPHelper.GetWebAPIAsync(URL, _apiMolexParameterModel).Result;
                GetAPIRespResult(result);
            }
            catch (Exception ex)
            {
                SetRespOnException(result, ex.Message);
            }
            return result;
        }

        /// <summary>
        /// This method will read Project File from CoreSync Server and it will return received data in Json string. 
        /// </summary>
        /// <returns>This will hold the GetProjectData operation execution result. If succeed then IsSuccess will be true and Data property will have JSON data.</returns>        
        public ResultModel<string> GetZoneLightScenes(string projectID, string ID)
        {
            ResultModel<string> result = new ResultModel<string>();
            try
            {
                //result = HTTPHelper.GetWebAPIAsync(_projectURL, _apiMolexParameterModel).Result;
                string URL = _baseURL + "zone/lights?projectid=" + projectID + "&zoneid=" + ID;
                result = HTTPHelper.GetWebAPIAsync(URL, _apiMolexParameterModel).Result;
                GetAPIRespResult(result);
            }
            catch (Exception ex)
            {
                SetRespOnException(result, ex.Message);
            }
            return result;
        }

        private BlindZoneLevelModel GetBlindLevelForAllLocations(EntityBaseModel model, BlindZoneLevelModel updateModel, string location)
        {
            string[] locations = new string[] { "C", "N", "E", "W", "S", "NE", "NW", "SE", "SW", "ALL" };
            foreach (var item in locations)
            {
                if (item == location)
                    continue;
                string key = string.Empty;
                if (item != "ALL")
                    key = EntityHelper.GetObjectDetailsKey(BacnetObjectType.ObjectAnalogValue, AppResource.AnalogValueBlindObjectName + "_" + item);
                else
                    key = EntityHelper.GetObjectDetailsKey(BacnetObjectType.ObjectAnalogValue, AppResource.AnalogValueBlindObjectName);

                if (!model.BACnetData.ObjectDetails.ContainsKey(key))
                    continue;
                switch (item)
                {
                    case "C":
                        {
                            int value = 0;
                            if (int.TryParse(model.BACnetData.ObjectDetails[key].PresentValue.ToString(), out value))
                                updateModel.c.level = value;
                        }
                        break;
                    case "N":
                        {
                            int value = 0;
                            if (int.TryParse(model.BACnetData.ObjectDetails[key].PresentValue.ToString(), out value))
                                updateModel.n.level = value;
                        }
                        break;
                    case "E":
                        {
                            int value = 0;
                            if (int.TryParse(model.BACnetData.ObjectDetails[key].PresentValue.ToString(), out value))
                                updateModel.e.level = value;
                        }
                        break;
                    case "W":
                        {
                            int value = 0;
                            if (int.TryParse(model.BACnetData.ObjectDetails[key].PresentValue.ToString(), out value))
                                updateModel.w.level = value;
                        }
                        break;
                    case "S":
                        {
                            int value = 0;
                            if (int.TryParse(model.BACnetData.ObjectDetails[key].PresentValue.ToString(), out value))
                                updateModel.s.level = value;
                        }
                        break;
                    case "NE":
                        {
                            int value = 0;
                            if (int.TryParse(model.BACnetData.ObjectDetails[key].PresentValue.ToString(), out value))
                                updateModel.ne.level = value;
                        }
                        break;
                    case "NW":
                        {
                            int value = 0;
                            if (int.TryParse(model.BACnetData.ObjectDetails[key].PresentValue.ToString(), out value))
                                updateModel.nw.level = value;
                        }
                        break;
                    case "SE":
                        {
                            int value = 0;
                            if (int.TryParse(model.BACnetData.ObjectDetails[key].PresentValue.ToString(), out value))
                                updateModel.se.level = value;
                        }
                        break;
                    case "SW":
                        {
                            int value = 0;
                            if (int.TryParse(model.BACnetData.ObjectDetails[key].PresentValue.ToString(), out value))
                                updateModel.sw.level = value;
                        }
                        break;
                    case "ALL":
                        {
                            int value = 0;
                            if (int.TryParse(model.BACnetData.ObjectDetails[key].PresentValue.ToString(), out value))
                                updateModel.all.level = value;
                        }
                        break;
                    default:
                        {
                            int value = 0;
                            if (int.TryParse(model.BACnetData.ObjectDetails[key].PresentValue.ToString(), out value))
                                updateModel.all.level = value;
                        }
                        break;
                }
            }
            return updateModel;
        }

        /// <summary>
        ///  This method is used to update brightness value at zone level 
        /// </summary>
        /// <param name="model">It will have model as entity for which brightness value need to update</param>
        /// <param name="brightness">it will have the brighteness value to update for Zone </param>
        /// <returns>string ZoneBrightness</returns>
        public ResultModel<string> UpdateZoneBlindLevel(EntityBaseModel model, int level, uint objectID)
        {
            string objectKey = string.Empty;
            foreach (var item in model.BACnetData.ObjectDetails)
            {
                if (objectID == item.Value.ObjectID)
                {
                    objectKey = item.Key;
                    break;
                }
            }
            string Location = string.Empty;
            if (objectKey.LastIndexOf("_") > 0)
                Location = objectKey.Substring(objectKey.LastIndexOf("_") + 1);
            string APIUrl = string.Empty;
            ResultModel<string> result = new ResultModel<string>();
            BlindZoneLevelModel aPISpaceStateModel = new BlindZoneLevelModel();
            if (string.IsNullOrEmpty(Location) || Location == AppResource.AnalogValueBlindObjectName)
                Location = "ALL";
            switch (Location.ToUpperInvariant())
            {
                case "C":
                case "CENTER":
                    aPISpaceStateModel.c = new C();
                    aPISpaceStateModel.c.level = level;
                    break;
                case "N":
                case "NORTH":
                    aPISpaceStateModel.n = new N();
                    aPISpaceStateModel.n.level = level;
                    break;
                case "E":
                case "EAST":
                    aPISpaceStateModel.e = new E();
                    aPISpaceStateModel.e.level = level;
                    break;
                case "W":
                case "WEST":
                    aPISpaceStateModel.w = new W();
                    aPISpaceStateModel.w.level = level;
                    break;
                case "S":
                case "SOUTH":
                    aPISpaceStateModel.s = new S();
                    aPISpaceStateModel.s.level = level;
                    break;
                case "NE":
                case "NORTH-EAST":
                    aPISpaceStateModel.ne = new Ne();
                    aPISpaceStateModel.ne.level = level;
                    break;
                case "NW":
                case "NORTH-WEST":
                    aPISpaceStateModel.nw = new Nw();
                    aPISpaceStateModel.nw.level = level;
                    break;
                case "SE":
                case "SOUTH-EAST":
                    aPISpaceStateModel.se = new Se();
                    aPISpaceStateModel.se.level = level;
                    break;
                case "SW":
                case "SOUTH-WEST":
                    aPISpaceStateModel.sw = new Sw();
                    aPISpaceStateModel.sw.level = level;
                    break;
                case "ALL":
                default:
                    aPISpaceStateModel.all = new All();
                    aPISpaceStateModel.all.level = level;
                    break;
            }
            //aPISpaceStateModel = GetBlindLevelForAllLocations(model, aPISpaceStateModel, Location);
            _responseData = new MolexAPIResponseModel();

            try
            {
                APIUrl = GetCompleteURL(model, _baseURL, AppConstants.ZoneTypes.BlindSpace.ToString());
                result = HTTPHelper.PutHttpWebRequest(APIUrl, aPISpaceStateModel, _apiMolexParameterModel);
                if (result.IsSuccess)
                {
                    //Changes Done To Consider the below situation
                    //If Suppose user has only 2 orientations 'Master' & else let's say 'North'
                    //User has modified the Master Orientation, so in case of metasys it will send values from 16 to 9 priority
                    //due to WritePropertyZoneLevel() method InputValue == null case the Master collection value will set to -1
                    //Now if suppose user has tried to set the value for North before the monitoring COV comes up then it will send -1 
                    //to API as per GetBlindLevelForAllLocations() method. so below changes has been done to aviod the same.
                    if (model.BACnetData.ObjectDetails.ContainsKey(objectKey))
                        model.BACnetData.ObjectDetails[objectKey].PresentValue = level;
                }
                //Condition to check if API response result parse success with error result having error details i.e API executed successfully
                //but with some errors like building/floor level brighness apply API executed successfully on all zone except for some of the zone it fails
                //and that failure zone id list API set in error object of resonse
                if (GetAPIRespResult(result))
                {
                    string zoneIDs = string.Empty;

                    foreach (Error error in _responseData.result.error)
                    {
                        PropertyInfo[] propertyInfo = error.propertyInfo;
                        Logger.Instance.Log(LogLevel.ERROR, "API error message :" + error.errorMsg + " ");
                        zoneIDs = zoneIDs + (string.IsNullOrEmpty(zoneIDs) ? propertyInfo[0].zoneId : "," + propertyInfo[0].zoneId);
                    }

                    result.Data = zoneIDs;
                }
            }
            catch (Exception ex)
            {
                SetRespOnException(result, ex.Message);
            }
            return result;
        }


        /// <summary>
        ///  This method is used to update brightness value at zone level 
        /// </summary>
        /// <param name="model">It will have model as entity for which brightness value need to update</param>
        /// <param name="brightness">it will have the brighteness value to update for Zone </param>
        /// <returns>string ZoneBrightness</returns>
        public ResultModel<string> UpdateZoneControlLevel(EntityBaseModel model, string property, int level)
        {
            string APIUrl = string.Empty;
            ResultModel<string> result = new ResultModel<string>();
            BioDynamicSaturationControlModel bioDynamicSaturationControlModel = new BioDynamicSaturationControlModel();
            bioDynamicSaturationControlModel.property = property;
            bioDynamicSaturationControlModel.value = level.ToString();
            _responseData = new MolexAPIResponseModel();

            try
            {
                if (model.EntityType == AppConstants.EntityTypes.Zone)
                {
                    string zoneType = string.Empty;
                    if (model as ZoneEntityModel != null)
                    {
                        switch ((model as ZoneEntityModel).ZoneType)
                        {
                            case AppConstants.ZoneTypes.UserSpace:
                                zoneType = AppResource.UserZoneTypeName;
                                break;
                            case AppConstants.ZoneTypes.GeneralSpace:
                                zoneType = AppResource.GeneralZoneTypeName;
                                break;
                            default:
                                break;
                        }
                    }
                    APIUrl = _baseURL + "project/property/control?projectid=" + AppCoreRepo.Instance.MolexProjectID.ToString() + "&type=zone&id=" + model.EntityKey + "&zonetype=" + zoneType;
                    result = HTTPHelper.PutHttpWebRequest(APIUrl, bioDynamicSaturationControlModel, _apiMolexParameterModel);
                }
                else if (model.EntityType == AppConstants.EntityTypes.Floor)
                {
                    List<string> zoneType = new List<string>();
                    foreach (var item in (model as FloorEntityModel).Childs)
                    {
                        switch ((item as ZoneEntityModel).ZoneType)
                        {
                            case AppConstants.ZoneTypes.UserSpace:
                                if (!zoneType.Contains(AppResource.UserZoneTypeName))
                                    zoneType.Add(AppResource.UserZoneTypeName);
                                break;
                            case AppConstants.ZoneTypes.GeneralSpace:
                                if (!zoneType.Contains(AppResource.GeneralZoneTypeName))
                                    zoneType.Add(AppResource.GeneralZoneTypeName);
                                break;
                            default:
                                break;
                        }
                    }
                    foreach (var item in zoneType)
                    {
                        APIUrl = _baseURL + "project/property/control?projectid=" + AppCoreRepo.Instance.MolexProjectID.ToString() + "&type=floor&id=" + model.EntityKey + "&zonetype=" + item;
                        result = HTTPHelper.PutHttpWebRequest(APIUrl, bioDynamicSaturationControlModel, _apiMolexParameterModel);
                        if (!result.IsSuccess)
                            break;
                    }
                }
                else if (model.EntityType == AppConstants.EntityTypes.Building)
                {
                    List<string> zoneType = new List<string>();
                    foreach (var floorItem in (model as BuildingEntityModel).Childs)
                    {
                        foreach (var zoneItem in (floorItem as FloorEntityModel).Childs)
                        {
                            switch ((zoneItem as ZoneEntityModel).ZoneType)
                            {
                                case AppConstants.ZoneTypes.UserSpace:
                                    if (!zoneType.Contains(AppResource.UserZoneTypeName))
                                        zoneType.Add(AppResource.UserZoneTypeName);
                                    break;
                                case AppConstants.ZoneTypes.GeneralSpace:
                                    if (!zoneType.Contains(AppResource.GeneralZoneTypeName))
                                        zoneType.Add(AppResource.GeneralZoneTypeName);
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    foreach (var item in zoneType)
                    {
                        APIUrl = _baseURL + "project/property/control?projectid=" + AppCoreRepo.Instance.MolexProjectID.ToString() + "&type=building&id=" + model.EntityKey + "&zonetype=" + item;
                        result = HTTPHelper.PutHttpWebRequest(APIUrl, bioDynamicSaturationControlModel, _apiMolexParameterModel);
                        if (!result.IsSuccess)
                            break;
                    }
                }

                //Condition to check if API response result parse success with error result having error details i.e API executed successfully
                //but with some errors like building/floor level brighness apply API executed successfully on all zone except for some of the zone it fails
                //and that failure zone id list API set in error object of resonse
                if (GetAPIRespResult(result))
                {
                    string zoneIDs = string.Empty;

                    foreach (Error error in _responseData.result.error)
                    {
                        PropertyInfo[] propertyInfo = error.propertyInfo;
                        Logger.Instance.Log(LogLevel.ERROR, "API error message :" + error.errorMsg + " ");
                        zoneIDs = zoneIDs + (string.IsNullOrEmpty(zoneIDs) ? propertyInfo[0].zoneId : "," + propertyInfo[0].zoneId);
                    }

                    result.Data = zoneIDs;
                }
            }
            catch (Exception ex)
            {
                SetRespOnException(result, ex.Message);
            }
            return result;
        }

        /// <summary>
        ///  This method is used to update brightness value at zone level 
        /// </summary>
        /// <param name="model">It will have model as entity for which brightness value need to update</param>
        /// <param name="brightness">it will have the brighteness value to update for Zone </param>
        /// <returns>string ZoneBrightness</returns>

        public ResultModel<string> UpdateZoneBrightness(EntityBaseModel model, int brightness, uint objectID = 0, AppConstants.ZoneTypes appliedZoneType = AppConstants.ZoneTypes.GeneralSpace)
        {
            string APIUrl = string.Empty;
            ResultModel<string> result = new ResultModel<string>();
            BioDynamicSaturationControlModel bioDynamicSaturationControlModel = new BioDynamicSaturationControlModel();
            bioDynamicSaturationControlModel.property = "brightness";
            bioDynamicSaturationControlModel.value = brightness.ToString();
            _responseData = new MolexAPIResponseModel();

            try
            {
                if (model.EntityType == AppConstants.EntityTypes.Zone)
                {
                    string zoneType = string.Empty;
                    if (model as ZoneEntityModel != null)
                    {
                        switch ((model as ZoneEntityModel).ZoneType)
                        {
                            case AppConstants.ZoneTypes.UserSpace:
                                zoneType = AppResource.UserZoneTypeName;
                                break;
                            case AppConstants.ZoneTypes.GeneralSpace:
                                zoneType = AppResource.GeneralZoneTypeName;
                                break;
                            case AppConstants.ZoneTypes.BeaconSpace:
                                zoneType = AppResource.BeaconZoneTypeName;
                                break;
                            default:
                                break;
                        }
                    }
                    APIUrl = _baseURL + "project/property/control?projectid=" + AppCoreRepo.Instance.MolexProjectID.ToString() + "&type=zone&id=" + model.EntityKey + "&zonetype=" + zoneType;
                    result = HTTPHelper.PutHttpWebRequest(APIUrl, bioDynamicSaturationControlModel, _apiMolexParameterModel);
                }
                else if (model.EntityType == AppConstants.EntityTypes.Floor)
                {
                    string zoneType = string.Empty;
                    switch (appliedZoneType)
                    {
                        case AppConstants.ZoneTypes.UserSpace:
                            zoneType = AppResource.UserZoneTypeName;
                            break;
                        case AppConstants.ZoneTypes.GeneralSpace:
                            zoneType = AppResource.GeneralZoneTypeName;
                            break;
                        case AppConstants.ZoneTypes.BeaconSpace:
                            zoneType = AppResource.BeaconZoneTypeName;
                            break;
                        default:
                            break;
                    }
                    APIUrl = _baseURL + "project/property/control?projectid=" + AppCoreRepo.Instance.MolexProjectID.ToString() + "&type=floor&id=" + model.EntityKey + "&zonetype=" + zoneType;
                    result = HTTPHelper.PutHttpWebRequest(APIUrl, bioDynamicSaturationControlModel, _apiMolexParameterModel);
                }
                else if (model.EntityType == AppConstants.EntityTypes.Building)
                {
                    string zoneType = string.Empty;
                    switch (appliedZoneType)
                    {
                        case AppConstants.ZoneTypes.UserSpace:
                            zoneType = AppResource.UserZoneTypeName;
                            break;
                        case AppConstants.ZoneTypes.GeneralSpace:
                            zoneType = AppResource.GeneralZoneTypeName;
                            break;
                        case AppConstants.ZoneTypes.BeaconSpace:
                            zoneType = AppResource.BeaconZoneTypeName;
                            break;
                        default:
                            break;
                    }
                    APIUrl = _baseURL + "project/property/control?projectid=" + AppCoreRepo.Instance.MolexProjectID.ToString() + "&type=building&id=" + model.EntityKey + "&zonetype=" + zoneType;
                    result = HTTPHelper.PutHttpWebRequest(APIUrl, bioDynamicSaturationControlModel, _apiMolexParameterModel);

                }
                //Condition to check if API response result parse success with error result having error details i.e API executed successfully
                //but with some errors like building/floor level brighness apply API executed successfully on all zone except for some of the zone it fails
                //and that failure zone id list API set in error object of resonse
                if (GetAPIRespResult(result))
                {
                    string zoneIDs = string.Empty;

                    foreach (Error error in _responseData.result.error)
                    {
                        PropertyInfo[] propertyInfo = error.propertyInfo;
                        Logger.Instance.Log(LogLevel.ERROR, "API error message :" + error.errorMsg + " ");
                        zoneIDs = zoneIDs + (string.IsNullOrEmpty(zoneIDs) ? propertyInfo[0].zoneId : "," + propertyInfo[0].zoneId);
                    }

                    result.Data = zoneIDs;
                }
            }
            catch (Exception ex)
            {
                SetRespOnException(result, ex.Message);
            }
            return result;
        }

        public ResultModel<string> UpdateOnOffControlLevel(EntityBaseModel model, string property, string value)
        {
            string APIUrl = string.Empty, type = string.Empty;
            ResultModel<string> result = new ResultModel<string>();

            BioDynamicSaturationControlModel bioDynamicSaturationControlModel = new BioDynamicSaturationControlModel();
            bioDynamicSaturationControlModel.property = property;
            bioDynamicSaturationControlModel.value = value;

            switch (model.EntityType)
            {
                case AppConstants.EntityTypes.Building:
                    type = "building";
                   
                    break;
                case AppConstants.EntityTypes.Floor:
                    type = "floor";
                    break;
                case AppConstants.EntityTypes.Zone:
                    type = "zone";
                    break;
                case AppConstants.EntityTypes.Controller:
                case AppConstants.EntityTypes.Fixture:
                case AppConstants.EntityTypes.Sensor:
                default:
                    break;
            }

            APIUrl = _baseURL + "project/property/control?projectid=" + AppCoreRepo.Instance.MolexProjectID.ToString() + "&type=" + type + "&id=" + model.EntityKey + "&zonetype=all";
            result = HTTPHelper.PutHttpWebRequest(APIUrl, bioDynamicSaturationControlModel, _apiMolexParameterModel);

            Logger.Instance.Log(LogLevel.DEBUG, "UpdateOnOffControlLevel => model name : " + model.EntityName + ", result: " + result.IsSuccess + ", Url: " + APIUrl);                                

            return result; 
        }
        
        /// <summary>
        /// Updates the zone light scene.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="lightScene">The light scene.</param>
        /// <returns>The result model</returns>
        /// <CreatedBy>prasad.joshi</CreatedBy><TimeStamp>6/2/20172:00 PM</TimeStamp>
        public ResultModel<string> UpdateLightScene(EntityBaseModel model, string lightScene)
        {
            string APIUrl = string.Empty;
            ResultModel<string> result = new ResultModel<string>();
            LightSceneZoneModel lightSceneZoneModel = new LightSceneZoneModel();
            lightSceneZoneModel.lightscene = lightScene;
            _responseData = new MolexAPIResponseModel();

            try
            {
                APIUrl = GetCompleteURL(model, _baseURL, AppConstants.API_LIGHTSCENE_ARGUMENT);
                result = HTTPHelper.PutHttpWebRequest(APIUrl, lightSceneZoneModel, _apiMolexParameterModel);

                //Condition to check if API response result parse success with error result having error details i.e API executed successfully
                //but with some errors like building/floor level lightscene apply API executed successfully on all zone except for some of the zone it fails
                //and that failure zone id list API set in error object of response
                if (GetAPIRespResult(result))
                {                    
                    string zoneIDs = string.Empty;

                    foreach (Error error in _responseData.result.error)
                    {
                        Logger.Instance.Log(LogLevel.ERROR, "API error message :" + error.errorMsg);
                        PropertyInfo[] propertyInfo = error.propertyInfo;
                        zoneIDs = zoneIDs + (string.IsNullOrEmpty(zoneIDs) ? propertyInfo[0].zoneId : "," + propertyInfo[0].zoneId);
                    }

                    result.Data = zoneIDs;
                }

            }
            catch (Exception ex)
            {
                SetRespOnException(result, ex.Message);
            }
            return result;
        }

        /// <summary>
        /// Updates the PIV DHTargetLux data.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="dhTargetLux">The DHTargetLux value.</param>
        /// <returns>True or False</returns>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>04-07-201707:06 PM</TimeStamp>
        public ResultModel<string> UpdateDHTargetData(EntityBaseModel model, uint dhTargetLux)
        {
            string APIUrl = string.Empty;
            ResultModel<string> result = new ResultModel<string>();
            DHTargetLuxZoneModel aPISpaceStateModel = new DHTargetLuxZoneModel();
            aPISpaceStateModel.lux = dhTargetLux;

            try
            {
                APIUrl = GetCompleteURL(model, _baseURL, AppConstants.API_DHTARGETLUX_ARGUMENT);
                result = HTTPHelper.PutHttpWebRequest(APIUrl, aPISpaceStateModel, _apiMolexParameterModel);
                GetAPIRespResult(result);
            }
            catch (Exception ex)
            {
                SetRespOnException(result, ex.Message);
            }
            return result;
        }



        /// <summary>
        /// Updates the PIV presTimeout data.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="presTimeout">The presTimeout value.</param>
        /// <returns>True or False</returns>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>04-07-201707:06 PM</TimeStamp>
        public ResultModel<string> UpdatepresTimeout(EntityBaseModel model, uint presTimeout)
        {
            string APIUrl = string.Empty;
            ResultModel<string> result = new ResultModel<string>();
            PresTimeoutZoneModel aPISpaceStateModel = new PresTimeoutZoneModel();
            aPISpaceStateModel.prestimeout = presTimeout;

            try
            {
                APIUrl = GetCompleteURL(model, _baseURL, AppConstants.API_PRESTIMEOUT_ARGUMENT);
                result = HTTPHelper.PutHttpWebRequest(APIUrl, aPISpaceStateModel, _apiMolexParameterModel);
                GetAPIRespResult(result);
            }
            catch (Exception ex)
            {
                SetRespOnException(result, ex.Message);
            }
            return result;
        }

        /// <summary>
        /// Updates the PIV presRes data.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="presRes">The presRes value.</param>
        /// <returns>True or False</returns>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>04-07-201707:06 PM</TimeStamp>
        public ResultModel<string> UpdatepresRate(EntityBaseModel model, uint presRes)
        {
            string APIUrl = string.Empty;
            ResultModel<string> result = new ResultModel<string>();
            PresRateZoneModel aPISpaceStateModel = new PresRateZoneModel();
            aPISpaceStateModel.presrate = presRes;

            try
            {
                APIUrl = GetCompleteURL(model, _baseURL, AppConstants.API_PRESRATE_ARGUMENT);
                result = HTTPHelper.PutHttpWebRequest(APIUrl, aPISpaceStateModel, _apiMolexParameterModel);
                GetAPIRespResult(result);
            }
            catch (Exception ex)
            {
                SetRespOnException(result, ex.Message);
            }
            return result;
        }

        /// <summary>
        /// Gets the complete URL.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="baseURL">The base URL.</param>
        /// <param name="setArgument">The set argument.</param>
        /// <returns></returns>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>13-07-201702:54 PM</TimeStamp>
        public string GetCompleteURL(EntityBaseModel model, string baseURL, string setArgument)
        {
            switch (model.EntityType)
            {
                case AppConstants.EntityTypes.Building:
                    if (setArgument == AppConstants.ZoneTypes.BlindSpace.ToString())
                    {
                        baseURL = baseURL + "project/blind?type=building&id=" + model.EntityKey + "&projectid=" + AppCoreRepo.Instance.MolexProjectID;
                        break;
                    }
                    baseURL = string.Concat(baseURL, "building/", setArgument) + "?projectid=" + AppCoreRepo.Instance.MolexProjectID + "&buildingid=" + model.EntityKey;
                    break;
                case AppConstants.EntityTypes.Floor:
                    if (setArgument == AppConstants.ZoneTypes.BlindSpace.ToString())
                    {
                        baseURL = baseURL + "project/blind?type=floor&id=" + model.EntityKey + "&projectid=" + AppCoreRepo.Instance.MolexProjectID;
                        break;
                    }
                    baseURL = string.Concat(baseURL, "floor/", setArgument) + "?projectid=" + AppCoreRepo.Instance.MolexProjectID + "&floorid=" + model.EntityKey;
                    break;
                case AppConstants.EntityTypes.Zone:
                    ZoneEntityModel zoneEntityModel = model as ZoneEntityModel;
                    if (((Molex.BACnet.Gateway.ServiceCore.Models.Entity.ZoneEntityModel)(model)).ZoneType == AppConstants.ZoneTypes.BlindSpace)
                    {
                        baseURL = baseURL + "project/blind?type=zone&id=" + model.EntityKey + "&projectid=" + AppCoreRepo.Instance.MolexProjectID;
                        break;
                    }
                    baseURL = string.Concat(baseURL, "zone/", setArgument) + "?projectid=" + AppCoreRepo.Instance.MolexProjectID + "&zoneid=" + model.EntityKey;
                    break;
            }
            return baseURL;
        }

        /// <summary>
        /// Gets the complete URL.
        /// </summary>
        /// <param name="baseURL">The base URL.</param>
        /// <param name="setArgument">The set argument.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        /// <CreatedBy>prasad.joshi</CreatedBy><TimeStamp>8/28/20176:08 PM</TimeStamp>
        public string GetCompleteURL(string baseURL, string setArgument)
        {
            baseURL = string.Concat(baseURL, setArgument);
            return baseURL;
        }

        /// <summary>
        /// Updates the mood.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="mood">The mood.</param>
        /// <returns></returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>8/10/201712:11 PM</TimeStamp>
        /// <exception cref="System.NotImplementedException"></exception>
        public ResultModel<string> UpdateMood(EntityBaseModel model, string mood)
        {
            string APIUrl = string.Empty;
            ResultModel<string> result = new ResultModel<string>();
            MoodZoneModel moodZoneModel = new MoodZoneModel();
            moodZoneModel.mood = mood;
            _responseData = new MolexAPIResponseModel();

            try
            {
                APIUrl = GetCompleteURL(model, _baseURL, AppConstants.API_MOOD_ARGUMENT);
                result = HTTPHelper.PutHttpWebRequest(APIUrl, moodZoneModel, _apiMolexParameterModel);

                //Condition to check if API response result parse success with error object having error details i.e API executed successfully
                //but with some errors like building/floor level mood apply API executed successfully on all zone except for some of the zone it fails
                //and that failure zone id list API set in error object of response
                if (GetAPIRespResult(result))
                {                   
                    string zoneIDs = string.Empty;

                    foreach (Error error in _responseData.result.error)
                    {
                        Logger.Instance.Log(LogLevel.ERROR, "API error message :" + error.errorMsg);
                        PropertyInfo[] propertyInfo = error.propertyInfo;
                        zoneIDs = zoneIDs + (string.IsNullOrEmpty(zoneIDs) ? propertyInfo[0].zoneId : "," + propertyInfo[0].zoneId);
                    }

                    result.Data = zoneIDs;
                }
            }
            catch (Exception ex)
            {
                SetRespOnException(result, ex.Message);
            }
            return result;
        }

        public ResultModel<string> UpdateResetAttributeAfterTimeOut(EntityBaseModel model, int resetAttributes)
        {
            string APIUrl = string.Empty;
            ResultModel<string> result = new ResultModel<string>();
            ResetAtrributeModel resetAtrributeModel = new ResetAtrributeModel();
            resetAtrributeModel.afterIdleTimeout = ConvertMsvStateValueToApiAttributes(resetAttributes);
            try
            {
                APIUrl = string.Format(_baseURL + "project/occupancySetting?type=zone&id={0}&projectid={1}", model.EntityKey, AppCoreRepo.Instance.MolexProjectID);
                result = HTTPHelper.PutHttpWebRequest(APIUrl, resetAtrributeModel, _apiMolexParameterModel);

                result.IsSuccess = true;    
            }
            catch (Exception ex)
            {
                SetRespOnException(result, ex.Message);
            }
            return result;
        }

        private string[] ConvertMsvStateValueToApiAttributes(int resetComboNo)
        {
            List<string> resetAttributes = new List<string>();

            string combo = AppCoreRepo.Instance.AvailableResetAttributeCombos[resetComboNo - 1].Replace("+", "");
            int index = 0;

            for (int ii = 0; ii < combo.Length; ii++)
            {
                if (int.TryParse(combo[ii].ToString(), out index))
                {
                    if (index == 0) // 'none' is selected
                        continue;

                    resetAttributes.Add(AppCoreRepo.Instance.ResetAttributes[index].Trim());
                }                                
            }

            return resetAttributes.ToArray();
        }

        /// <summary>
        /// Updates the time synchronize.
        /// </summary>
        /// <param name="timeSync">The time synchronize.</param>
        /// <returns></returns>
        /// <CreatedBy>prasad.joshi</CreatedBy><TimeStamp>8/30/20172:45 PM</TimeStamp>
        public ResultModel<string> UpdateTimeSync(string timeSync)
        {
            string APIUrl = string.Empty;
            ResultModel<string> result = new ResultModel<string>();
            TimeSyncModel timeSyncModel = new TimeSyncModel();
            timeSyncModel.utctimestamp = timeSync;

            try
            {
                APIUrl = GetCompleteURL(_baseURL, AppConstants.API_SYSTEM_TIME_ARGUMENT);
                result = HTTPHelper.PutHttpWebRequest(APIUrl, timeSyncModel, _apiMolexParameterModel);
                GetAPIRespResult(result);
            }
            catch (Exception ex)
            {
                SetRespOnException(result, ex.Message);
            }
            return result;
        }

        private int GetMaxObjectID(EntityBaseModel model,out int Count)
        {
            Count = 1;
            if (model == null)
                return 12626;
            if (model as ZoneEntityModel == null)
                return 12626;
            if ((model as ZoneEntityModel).ZoneType != AppConstants.ZoneTypes.BeaconSpace)
                return 12626;
            string lightSceneSelected = string.Empty;
            foreach (var item in (model as ZoneEntityModel).BACnetData.ObjectDetails)
            {
                if (item.Value.ObjectName.Contains("Light Scene"))
                {
                    lightSceneSelected = item.Value.PresentValue.ToString();
                }
            }
            switch (lightSceneSelected.ToUpperInvariant())
            {
                case "ONECOLOUR":
                    return 12626;
                case "CYCLE":
                    Count = 5;
                    return 12626 + 4;
                case "CHASE":
                    Count = 4;
                    return 12626 + 3;
                case "TWOCOLOUR":
                case "SLOWFLASH":
                case "FASTFLASH":
                case "SLOWPULSE":
                case "FASTPULSE":
                    Count = 2;
                    return 12626 + 1;
                case "THREECOLOUR":
                    Count = 3;
                    return 12626 + 2;
                default:
                    return 12626;
            }
        }

        /// <summary>
        /// Updates the color of the beacon.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="beaconZoneEntityModel">The beaconZoneEntityModel.</param>
        /// <returns></returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>11/13/20175:40 PM</TimeStamp>
        public ResultModel<string> UpdateBeaconColor(EntityBaseModel model, BeaconZoneEntityModel beaconZoneEntityModel, uint objectID)
        {
            ResultModel<string> result = new ResultModel<string>();
            BeaconPaletteModel aPISpaceStateModel = new BeaconPaletteModel();

            bool isObjectIDFound;
            int objectCount = 0;
            if (objectID > GetMaxObjectID(model, out objectCount))
            {
                return null;
            }

            #region Apply Palette Values
            aPISpaceStateModel.PaletteModelList = new PaletteModel[objectCount];

            if (aPISpaceStateModel.PaletteModelList.Length > 0)
            {
                aPISpaceStateModel.PaletteModelList[0] = GetPalettesModel(model, AppResource.BeaconColor + " 1", objectID, out isObjectIDFound);
                if (aPISpaceStateModel.PaletteModelList[0] == null && isObjectIDFound)
                {
                    aPISpaceStateModel.PaletteModelList[0] = new PaletteModel();
                    aPISpaceStateModel.PaletteModelList[0].red = beaconZoneEntityModel.Red;
                    aPISpaceStateModel.PaletteModelList[0].green = beaconZoneEntityModel.Green;
                    aPISpaceStateModel.PaletteModelList[0].blue = beaconZoneEntityModel.Blue;
                }
            }
            if (aPISpaceStateModel.PaletteModelList.Length > 1)
            {
                aPISpaceStateModel.PaletteModelList[1] = GetPalettesModel(model, AppResource.BeaconColor + " 2", objectID, out isObjectIDFound);
                if (aPISpaceStateModel.PaletteModelList[1] == null && isObjectIDFound)
                {
                    aPISpaceStateModel.PaletteModelList[1] = new PaletteModel();
                    aPISpaceStateModel.PaletteModelList[1].red = beaconZoneEntityModel.Red;
                    aPISpaceStateModel.PaletteModelList[1].green = beaconZoneEntityModel.Green;
                    aPISpaceStateModel.PaletteModelList[1].blue = beaconZoneEntityModel.Blue;
                }
            }
            if (aPISpaceStateModel.PaletteModelList.Length > 2)
            {
                aPISpaceStateModel.PaletteModelList[2] = GetPalettesModel(model, AppResource.BeaconColor + " 3", objectID, out isObjectIDFound);
                if (aPISpaceStateModel.PaletteModelList[2] == null && isObjectIDFound)
                {
                    aPISpaceStateModel.PaletteModelList[2] = new PaletteModel();
                    aPISpaceStateModel.PaletteModelList[2].red = beaconZoneEntityModel.Red;
                    aPISpaceStateModel.PaletteModelList[2].green = beaconZoneEntityModel.Green;
                    aPISpaceStateModel.PaletteModelList[2].blue = beaconZoneEntityModel.Blue;
                }
            }
            if (aPISpaceStateModel.PaletteModelList.Length > 3)
            {
                aPISpaceStateModel.PaletteModelList[3] = GetPalettesModel(model, AppResource.BeaconColor + " 4", objectID, out isObjectIDFound);
                if (aPISpaceStateModel.PaletteModelList[3] == null && isObjectIDFound)
                {
                    aPISpaceStateModel.PaletteModelList[3] = new PaletteModel();
                    aPISpaceStateModel.PaletteModelList[3].red = beaconZoneEntityModel.Red;
                    aPISpaceStateModel.PaletteModelList[3].green = beaconZoneEntityModel.Green;
                    aPISpaceStateModel.PaletteModelList[3].blue = beaconZoneEntityModel.Blue;
                }
            }
            if (aPISpaceStateModel.PaletteModelList.Length > 4)
            {
                aPISpaceStateModel.PaletteModelList[4] = GetPalettesModel(model, AppResource.BeaconColor + " 5", objectID, out isObjectIDFound);
                if (aPISpaceStateModel.PaletteModelList[4] == null && isObjectIDFound)
                {
                    aPISpaceStateModel.PaletteModelList[4] = new PaletteModel();
                    aPISpaceStateModel.PaletteModelList[4].red = beaconZoneEntityModel.Red;
                    aPISpaceStateModel.PaletteModelList[4].green = beaconZoneEntityModel.Green;
                    aPISpaceStateModel.PaletteModelList[4].blue = beaconZoneEntityModel.Blue;
                }
            }
            #endregion

            ZoneEntityModel enity = (ZoneEntityModel)model;
            string APIUrl = string.Empty;

            try
            {
                APIUrl = _baseURL + "palette/color?projectid=" + AppCoreRepo.Instance.MolexProjectID + "&type=zone&zonetype=beacon&id=" + model.EntityKey;
                result = HTTPHelper.PutHttpWebRequest(APIUrl, aPISpaceStateModel, _apiMolexParameterModel);
                GetAPIRespResult(result);
            }
            catch (Exception ex)
            {
                SetRespOnException(result, ex.Message);
            }
            return result;
        }

        private PaletteModel GetPalettesModel(EntityBaseModel model, string objectname, uint objectid, out bool IsObjectIDsame)
        {
            IsObjectIDsame = false;
            if (model == null)
                return null;
            if (model as ZoneEntityModel == null)
                return null;
            if ((model as ZoneEntityModel).ZoneType != AppConstants.ZoneTypes.BeaconSpace)
                return null;
            foreach (var item in (model as ZoneEntityModel).BACnetData.ObjectDetails)
            {
                if (item.Value == null)
                    continue;
                if (item.Value.ObjectID == objectid && item.Value.ObjectName == objectname  && item.Value.ObjectType == BacnetObjectType.ObjectPositiveIntegerValue)
                {
                    IsObjectIDsame = true;
                    return null;
                }
                else if (item.Value.ObjectName == objectname)
                {
                    PaletteModel _PaletteModel = new PaletteModel();
                    uint value;
                    uint.TryParse(item.Value.PresentValue.ToString(), out value);
                    _PaletteModel.red = CommonHelper.GetRGBValues(value).Red;
                    _PaletteModel.green = CommonHelper.GetRGBValues(value).Green;
                    _PaletteModel.blue = CommonHelper.GetRGBValues(value).Blue;
                    return _PaletteModel;
                }
            }
            return null;
        }

        /// <summary>
        /// Updates the state of the zone.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="state">The state.</param>
        /// <returns></returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>11/24/201712:07 PM</TimeStamp>
        public ResultModel<string> UpdateZoneState(EntityBaseModel model, string state)
        {
            ResultModel<string> result = new ResultModel<string>();
            ZoneStateModel aPISpaceStateModel = new ZoneStateModel();
            aPISpaceStateModel.state = state;
            string APIUrl = string.Empty;

            try
            {
                APIUrl = GetCompleteURL(model, _baseURL, AppConstants.API_ZONE_STATE_ARGUMENT);
                result = HTTPHelper.PutHttpWebRequest(APIUrl, aPISpaceStateModel, _apiMolexParameterModel);
                GetAPIRespResult(result);
            }
            catch (Exception ex)
            {
                SetRespOnException(result, ex.Message);
            }
            return result;
        }

        /// <summary>
        /// Gets the reliability data.
        /// </summary>
        /// <returns></returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>1/4/20183:51 PM</TimeStamp>
        public ResultModel<string> GetReliabilityData()
        {
            ResultModel<string> result = new ResultModel<string>();
            string APIUrl = string.Empty;
            try
            {
                APIUrl = GetCompleteURL(_baseURL, AppConstants.API_RELIABILITY_ARGUMENT);
                APIUrl = APIUrl + "?projectid=" + AppCoreRepo.Instance.MolexProjectID;
                result = HTTPHelper.GetWebAPIAsync(APIUrl, _apiMolexParameterModel).Result;
                GetAPIRespResult(result);
            }
            catch (Exception ex)
            {
                SetRespOnException(result, ex.Message);
            }
            return result;
        }

        /// <summary>
        /// Subscribes the s pace state API.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>3/13/20188:01 PM</TimeStamp>
        public ResultModel<string> SubscribeAPI(SubscriptionModel model)
        {
            ResultModel<string> result = new ResultModel<string>();
            _responseData = new MolexAPIResponseModel();
            string APIUrl = string.Empty;

            try
            {
                APIUrl = GetCompleteURL(_baseURL, AppConstants.API_SUBSCRIBE_ARGUMENT);
                result = HTTPHelper.SubscribeAPI(APIUrl, _apiMolexParameterModel, model);

                //Condition to check if API response result parse success and to get client and subscription id
                //from api rsponse in case of subscription done success or already subscription done response get from API.
                if (GetAPIRespResult(result))
                {
                    result.Data = JsonConvert.SerializeObject(_responseData.result.error[0].propertyInfo[0]);
                }
                else
                {
                    result.Data = JsonConvert.SerializeObject(_responseData.result.data[0]);
                }
            }
            catch (Exception ex)
            {
                SetRespOnException(result, model.topic + ":" + ex.Message);
            }
            return result;
        }

        /// <summary>
        /// Uns the subscribe API.
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>3/15/20184:21 PM</TimeStamp>
        public void UnSubscribeAPI(string unsubscribeId)
        {
            ResultModel<string> result = new ResultModel<string>();
            string APIUrl = string.Empty;
            try
            {
                APIUrl = GetCompleteURL(_baseURL, AppConstants.API_UNSUBSCRIBE_ARGUMENT);
                result = HTTPHelper.UnSubscribeAPI(APIUrl, _apiMolexParameterModel, unsubscribeId);
                GetAPIRespResult(result);
            }
            catch (Exception ex)
            {
                SetRespOnException(result, ex.Message);
            }
        }

        /// <summary>
        /// Gets the API live status.
        /// </summary>
        /// <param name="clientID">The client identifier.</param>
        /// <returns></returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/201812:14 PM</TimeStamp>
        public ResultModel<string> GetAPILiveStatus(string clientID)
        {
            ResultModel<string> result = new ResultModel<string>();
            string APIUrl = string.Empty;
            try
            {
                APIUrl = GetCompleteURL(_baseURL, AppConstants.API_LIVE_ARGUMENT);
                result = HTTPHelper.GetAPILiveStatus(APIUrl, _apiMolexParameterModel, clientID).Result;
                GetAPIRespResult(result);
            }
            catch (Exception ex)
            {
                SetRespOnException(result, ex.Message);
            }

            return result;
        }

        #endregion
        #region PrivateMethods

        /// <summary>
        /// Prepares the urls.
        /// </summary>
        /// <param name="baseURL">The base URL.</param>
        /// <param name="projectId">The Project Id</param>
        /// <CreatedBy>prasad.joshi</CreatedBy><TimeStamp>6/19/20174:54 PM</TimeStamp>
        private void PrepareUrls(string baseURL, string projectId)
        {
            _baseURL = baseURL.TrimEnd('/') + "/";
            _projectURL = baseURL + AppConstants.PROJECT_ACTION + "id=" + projectId;
            _lightScenesURL = baseURL + "project/lightscenes/list?projectid=" + projectId;
            // _projectURL = baseURL + AppConstants.PROJECT_ACTION;
        }


        /// <summary>
        /// Generic method to parse and return API response
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        private bool GetAPIRespResult(ResultModel<string> result)
        {
            bool isErrRespParseReq = false;
            //To get calling function name
            string callingFunName = new StackFrame(1).GetMethod().Name;

            try
            {
                if (!result.IsSuccess)
                {
                    if (result.Error.ErrorCode == (int)HttpStatusCode.Unauthorized)
                    {
                        result.Error.ErrorCode = AppConstants.ERROR_CATEGORY_SERVER_AUTHETICATION_FAILED;
                        result.Error.OptionalMessage = "HTTPAPIManager:" + callingFunName + ":API authentication fail with username :" + _apiMolexParameterModel.UserName + ",password :" + _apiMolexParameterModel.Password;
                    }

                    return isErrRespParseReq;
                }
                _responseData = JsonConvert.DeserializeObject<MolexAPIResponseModel>(result.Data);

                if (!_responseData.status)
                {
                    result.IsSuccess = false;
                    result.Error = new ErrorModel();
                    result.Error.OptionalMessage = "HTTPAPIManager:" + callingFunName + ":" + _responseData.result.error[0].errorMsg;
                    result.Error.ErrorCode = _responseData.result.error[0].statusCode;
                    result.Error.ErrorCategory = AppConstants.ERROR_CATEGORY_MODELDATA;
                    isErrRespParseReq = true;
                }
            }
            catch (Exception ex)
            {
                Log.Logger.Instance.Log(ex, Log.LogLevel.ERROR, "HTTPAPIManager:SetAPIResponse:Error occured");
                result.IsSuccess = false;
                result.Error = new ErrorModel();
                result.Error.ErrorCategory = AppConstants.ERROR_CATEGORY_SERVER;
                result.Error.OptionalMessage = "HTTPAPIManager:" + callingFunName + ":set api response failed";
                result.Error.ErrorCode = AppConstants.ERROR_CATEGORY_SERVER_UNKNOWN;
            }
            return isErrRespParseReq;
        }

        /// <summary>
        /// To set exception message in response result
        /// </summary>
        /// <param name="result"></param>
        private void SetRespOnException(ResultModel<string> result, string message)
        {
            string callingFunName = new StackFrame(1).GetMethod().Name;
            Log.Logger.Instance.Log(Log.LogLevel.ERROR, "HTTPAPIManager:SetExceptionResoponse:" + callingFunName + ":Error occured");
            result.IsSuccess = false;
            result.Error = new ErrorModel();
            result.Error.ErrorCategory = AppConstants.ERROR_CATEGORY_SERVER;
            result.Error.OptionalMessage = "HTTPAPIManager:SetExceptionResoponse:" + callingFunName + ":" + message;
            result.Error.ErrorCode = AppConstants.ERROR_CATEGORY_SERVER_UNAVAILABLE;
        }

        #endregion PrivateMethods
    }

}


