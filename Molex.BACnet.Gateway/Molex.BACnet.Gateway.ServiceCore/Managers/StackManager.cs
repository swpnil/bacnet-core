﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              <StackManager.cs>
///   Description:        <This class will hold the functionality to initialize stack>
///   Author:             Amol Kulkarni                  
///   Date:               05/24/17
///---------------------------------------------------------------------------------------------
#endregion

using Molex.BACnet.Gateway.Log;
using Molex.BACnet.Gateway.ServiceCore.BACnetHandlers;
using Molex.BACnet.Gateway.ServiceCore.Constants;
using Molex.BACnet.Gateway.ServiceCore.Repository;
using Molex.BACnet.Gateway.ServiceCore.SingletonCore;
using Molex.BACnetStackIntegration.Handler;
using Molex.BACnetStackIntegration.StackManagedApi;
using Molex.StackDataObjects.APIModels;
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Response;
using Molex.StackDataObjects.StackInititializationModels;
using System;
using System.Collections.Generic;
using System.Net;

namespace Molex.BACnet.Gateway.ServiceCore.Managers
{

    /// <summary>
    /// This class will hold the functionality to initialize stack
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:23 PM</TimeStamp>
    /// <seealso cref="Molex.BACnet.Gateway.ServiceCore.SingletonCore.SingletonBase{Molex.BACnet.Gateway.ServiceCore.Managers.StackManager}" />
    public class StackManager : SingletonBase<StackManager>
    {

        #region public methods

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        /// <returns>bool</returns>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:23 PM</TimeStamp>
        public bool Init(bool isLogDebugEnabled, int logDeviceId)
        {
            ResponseResult result = StackManagedApi.InitializeStackIntegration(false, isLogDebugEnabled, logDeviceId);
            return result.IsSucceed;
        }

        /// <summary>
        /// Configures the stack.
        /// </summary>
        /// <returns>ResponseResult</returns>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:24 PM</TimeStamp>
        public ResponseResult ConfigureStack()
        {
            Logger.Instance.Log(LogLevel.DEBUG, "ConfigureStack: Configure Vendor related information and create Directory");

            VendorModel vendor;
            ResponseResult result = null;

            vendor = new VendorModel
            {
                VendorID = Convert.ToUInt16(AppCoreRepo.Instance.BACnetConfigurationData.VendorIdentifier),
                VendorName = AppCoreRepo.Instance.BACnetConfigurationData.VendorName,
                ModelName = AppCoreRepo.Instance.BACnetConfigurationData.ModelName,
                Location = string.Empty,
                SoftwareVersion = AppCoreRepo.Instance.ApplicationSoftwareVersion,
                ProfileName = AppResources.AppResource.ProfileName,
                SerialNumber = string.Empty,
                DeviceDescription = AppCoreRepo.Instance.BACnetConfigurationData.Description,
                ObjectDescription = AppResources.AppResource.VendorObjectDescription,
                Password = AppCoreRepo.Instance.BACnetConfigurationData.BACnetPassword,
                DeviceID = AppCoreRepo.Instance.BACnetConfigurationData.ControllerId
            };

            //set vendor specific data
            result = StackManagedApi.SetVendorSpecificdata(vendor);

            if (!result.IsSucceed)
            {
                Logger.Instance.Log(LogLevel.ERROR, "Set vendor specification fail during stack initialize");
                return result;
            }

            //create directory - folder required for BBMD & Backup-Restore
            result = StackManagedApi.CreateDirectory();

            if (!result.IsSucceed)
            {
                Logger.Instance.Log(LogLevel.ERROR, "Create directory fail during stack initialize");
                return result;
            }

            //configure maximum limits of stack queues
            //TODO: Add and Call BACDEL_Set_Max_Limits() API
            BACAppMaxLimitsModel model = new BACAppMaxLimitsModel();
            model.MaxStateText = AppConstants.LIGHTSCENE_MAX_LIMIT;
            StackManagedApi.SetMaxLimits(model, BacnetMaxlimitParameters.BacnetMaxlimitMaxStateText, BacnetMaxlimitParameters.BacnetMaxlimitMaxStateText, true);

            result = RegisterCallback();

            Logger.Instance.Log(LogLevel.DEBUG, "ConfigureStack: Configure Vendor related information and create Directory- Completed");

            return result;
        }


        /// <summary>
        /// Add Device object for Controller and Zone Level 
        /// </summary>
        /// <param name="deviceId">Device ID </param>
        /// <param name="deviceAddress">deviceAddress</param>
        /// <param name="SNet">SNet</param>
        /// <returns>ResponseResult</returns>
        public ResponseResult CreateDeviceObject(uint deviceId, uint deviceAddress, ushort SNet)
        {
            Logger.Instance.Log(LogLevel.DEBUG, "CreateDeviceObject: Create device Object - deviceId " + deviceId);
            ResponseResult result = null;

            try
            {
                result = StackManagedApi.AddDevice(deviceId, deviceAddress, SNet);

                if (!result.IsSucceed)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "Error in add device during stack initialization - deviceId " + deviceId);
                }

            }
            catch (Exception ex)
            {
                Logger.Instance.Log(LogLevel.ERROR, ex.Message);
                result.IsSucceed = false;
            }

            return result;
        }


        /// <summary>
        /// Creates the object for all Structure/Analog values 
        /// </summary>
        /// <param name="deviceId">The device identifier.</param>
        /// <param name="objectType">Type of the object.</param>
        /// <param name="objectId">The object identifier.</param>
        /// <param name="objectName">Name of the object.</param>
        /// <returns>bool</returns>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201702:48 PM</TimeStamp>
        public bool CreateObject(uint deviceId, BacnetObjectType objectType, uint objectId, string objectName)
        {
            Logger.Instance.Log(LogLevel.DEBUG, "CreateObject: Create object for Building and Floor Level");
            ResponseResult result = StackManagedApi.AddObject(deviceId, objectType, objectId, objectName);

            if (!result.IsSucceed)
            {
                Logger.Instance.Log(LogLevel.ERROR, "Object Creation failure in BACnet Stack for-deviceId : '" + deviceId + "',objectType : '" + objectType + "',objectId : '" + objectId + "', objectName : '" + objectName + "'");
                return false;
            }

            Logger.Instance.Log(LogLevel.DEBUG, "CreateObject: Create object for Building and Floor Level - Completed");
            return true;
        }


        /// <summary>
        /// Creates the object for all Structure/Analog values 
        /// </summary>
        /// <param name="deviceId">The device identifier.</param>
        /// <param name="objectType">Type of the object.</param>
        /// <param name="objectId">The object identifier.</param>
        /// <param name="objectName">Name of the object.</param>
        /// <returns>bool</returns>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201702:48 PM</TimeStamp>
        public bool RemoveObject(uint deviceId, BacnetObjectType objectType, uint objectId)
        {
            Logger.Instance.Log(LogLevel.DEBUG, "RemoveObject: Removing object in stack");
            ResponseResult result = StackManagedApi.RemoveObject(deviceId, objectType, objectId);

            if (!result.IsSucceed)
            {
                Logger.Instance.Log(LogLevel.ERROR, "Object deletion failure in BACnet Stack for-deviceId : '" + deviceId + "',objectType : '" + objectType + "',objectId : '" + objectId);
                return false;
            }

            Logger.Instance.Log(LogLevel.DEBUG, "RemoveObject: Remove object - Completed");
            return true;
        }

        /// <summary>
        /// Writes the property.
        /// </summary>
        /// <param name="deviceId">The device identifier.</param>
        /// <param name="objectType">Type of the object.</param>
        /// <param name="objectId">The object identifier.</param>
        /// <param name="model">The model.</param>
        /// <param name="bacnetPropertyID">The bacnet property identifier.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        /// <param name="priority">The priority.</param>
        /// <param name="isArrayIndexPresent">if set to <c>true</c> [is array index present].</param>
        /// <param name="bacnetDataType">Type of the bacnet data.</param>
        /// <returns></returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>7/7/20171:05 PM</TimeStamp>
        public bool WriteProperty(uint deviceId, BacnetObjectType objectType, uint objectId, object model, BacnetPropertyID bacnetPropertyID, uint arrayIndex, uint priority, byte isArrayIndexPresent, BacnetDataType bacnetDataType)
        {
            ResponseResult result = StackManagedApi.SetObjectPropertyValue(deviceId, objectId, objectType, bacnetPropertyID, arrayIndex, model, (byte)priority, Convert.ToBoolean(isArrayIndexPresent), bacnetDataType);

            if (!result.IsSucceed)
            {
                Logger.Instance.Log(LogLevel.ERROR, "StackManager.WriteProperty: Set Property failure in BACnet Stack for objectType:'" + objectType + "',Property :'" + bacnetPropertyID + "',objectId: '" + objectId + "',deviceId: '" + deviceId + "',priority: '" + priority + "',isArrayIndexPresent: '" + isArrayIndexPresent + "',arrayIndex" + arrayIndex);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Writes the property.
        /// </summary>
        /// <param name="deviceId">The device identifier.</param>
        /// <param name="objectType">Type of the object.</param>
        /// <param name="objectId">The object identifier.</param>
        /// <param name="model">The model.</param>
        /// <param name="bacnetPropertyID">The bacnet property identifier.</param>
        /// <param name="bacnetDataType">Type of the bacnet data.</param>
        /// <returns>bool</returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/29/20174:34 PM</TimeStamp>
        public bool WriteProperty(uint deviceId, BacnetObjectType objectType, uint objectId, object model, BacnetPropertyID bacnetPropertyID, BacnetDataType bacnetDataType)
        {
            ResponseResult result = StackManagedApi.SetObjectPropertyValue(deviceId, objectId, objectType, bacnetPropertyID, uint.MaxValue, model, (byte)AppCoreRepo.Instance.BACnetConfigurationData.MonitoringDefaultPriority, false, bacnetDataType);

            if (!result.IsSucceed)
            {
                Logger.Instance.Log(LogLevel.ERROR, "StackManager.WriteProperty: Set Property failure in BACnet Stack for objectType:'" + objectType + "',Property :'" + bacnetPropertyID + "',objectId: '" + objectId + "',deviceId: '" + deviceId + "'value=,'" + model + "'");
                return false;
            }

            return true;
        }

        /// <summary>
        /// Writes the property.
        /// </summary>
        /// <param name="deviceId">The device identifier.</param>
        /// <param name="objectType">Type of the object.</param>
        /// <param name="objectId">The object identifier.</param>
        /// <param name="model">The model.</param>
        /// <param name="bacnetPropertyID">The bacnet property identifier.</param>
        /// <returns>bool</returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/29/20176:55 PM</TimeStamp>
        public bool WriteProperty(uint deviceId, BacnetObjectType objectType, uint objectId, object model, BacnetPropertyID bacnetPropertyID)
        {
            return WriteProperty(deviceId, objectType, objectId, model, bacnetPropertyID, GetBacnetDataType(bacnetPropertyID, objectType));
        }

        /// <summary>
        /// Gets the type of the object from EntityTypes to BACnetObjectType.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>BacnetObjectType</returns>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201702:52 PM</TimeStamp>
        public static BacnetObjectType GetObjectType(AppConstants.EntityTypes type)
        {
            switch (type)
            {
                case AppConstants.EntityTypes.Controller:
                    return BacnetObjectType.ObjectDevice;
                case AppConstants.EntityTypes.Building:
                case AppConstants.EntityTypes.Floor:
                    return BacnetObjectType.ObjectStructuredView;
                case AppConstants.EntityTypes.Zone:
                    return BacnetObjectType.ObjectDevice;
                case AppConstants.EntityTypes.Sensor:
                    return BacnetObjectType.ObjectAnalogInput;
                case AppConstants.EntityTypes.Fixture:
                    return BacnetObjectType.ObjectAnalogValue;
                default:
                    Logger.Instance.Log(LogLevel.ERROR, "StackManager.GetObjectType: Object type not avalible for entity type :'" + type + "'");
                    return 0;
            }
        }

        /// <summary>
        /// Stacks the initialize.
        /// </summary>
        /// <returns>ResponseResult</returns>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201702:47 PM</TimeStamp>
        public ResponseResult StackInit()
        {
            Logger.Instance.Log(LogLevel.DEBUG, "StackInit: Stack Initialization - Started");

            ResponseResult result = null;

            try
            {
                StackConfigModel stackConfigSelfInfo = new StackConfigModel
                {
                    IpAddress = IPAddress.Parse(AppCoreRepo.Instance.BACnetConfigurationData.IPAddress),
                    Port = Convert.ToUInt16(AppCoreRepo.Instance.BACnetConfigurationData.UDPPort),
                    LocalNetworkNumber = (ushort)AppCoreRepo.Instance.BACnetConfigurationData.LocalNetworkNumber,
                    VirtualNetworkNumber = (ushort)AppCoreRepo.Instance.BACnetConfigurationData.VirtualNetworkNumber
                };

                result = StackManagedApi.InitializeStack(stackConfigSelfInfo);

                if (!result.IsSucceed)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "Stack init fail Error Code: " + result.ErrorModel.ErrorCode + ", Error Type: " + result.ErrorModel.Type);
                }

            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR);
                result.IsSucceed = false;
            }

            Logger.Instance.Log(LogLevel.DEBUG, "StackInit: Stack Initialization - Completed");

            return result;
        }
        /// <summary>
        /// De-initialization of stack
        /// </summary>
        /// <returns>bool</returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/29/20175:40 PM</TimeStamp>
        public bool DeInit()
        {
            ResponseResult result = StackManagedApi.DeInitializeStackIntegration();

            if (!result.IsSucceed)
            {
                Logger.Instance.Log(LogLevel.ERROR, "Stack De-Init: Stack DeInitialization failed-Error Code:" + result.ErrorCode);
            }
            return result.IsSucceed;
        }

        /// <summary>
        /// Gets the type of the data.
        /// </summary>
        /// <param name="bacnetPropertyID">The bacnet property identifier.</param>
        /// <param name="objectType">Type of the object.</param>
        /// <returns>BacnetDataType</returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/29/20176:54 PM</TimeStamp>
        public static BacnetDataType GetBacnetDataType(BacnetPropertyID bacnetPropertyID, BacnetObjectType objectType)
        {
            BacnetDataType bacnetDataType = 0;

            switch (bacnetPropertyID)
            {
                case BacnetPropertyID.PropPriority:
                    if (objectType == BacnetObjectType.ObjectNotificationClass)
                    {
                        bacnetDataType = BacnetDataType.BacnetDTUnsignedArray;
                        return bacnetDataType;
                    }
                    else
                    {
                        bacnetDataType = 0;
                        return 0;
                    }
                case BacnetPropertyID.PropUnits:
                //case BacnetPropertyID.PropProprietaryColorTemperatureUnit:
                //    bacnetDataType = BacnetDataType.BacnetDTEnumNew;
                //    return bacnetDataType;
                case BacnetPropertyID.PropProfileName:
                case BacnetPropertyID.PropObjectName:
                case BacnetPropertyID.PropDescription:
                case BacnetPropertyID.PropDeviceType:
                case BacnetPropertyID.PropLocation:
                case BacnetPropertyID.PropApplicationSoftwareVersion:
                case BacnetPropertyID.PropNodeSubType:
                case BacnetPropertyID.PropActiveText:
                case BacnetPropertyID.PropInactiveText:
                    bacnetDataType = BacnetDataType.BacnetDTCharString;
                    return bacnetDataType;

                case BacnetPropertyID.PropProprietaryGridX:
                case BacnetPropertyID.PropProprietaryGridY:
                case BacnetPropertyID.PropNumberOfStates:
                case BacnetPropertyID.PropNotificationClass:
                case BacnetPropertyID.PropRelinquishDefault:
                case BacnetPropertyID.PropTimeDelay:
                    bacnetDataType = BacnetDataType.BacnetDTUnsigned;
                    return bacnetDataType;
                case BacnetPropertyID.PropHighLimit:
                    switch (objectType)
                    {
                        case BacnetObjectType.ObjectPositiveIntegerValue:
                            bacnetDataType = BacnetDataType.BacnetDTUnsigned;
                            break;
                        default:
                            bacnetDataType = BacnetDataType.BacnetDTReal;
                            break;
                    }
                    return bacnetDataType;
                case BacnetPropertyID.PropLowLimit:
                    switch (objectType)
                    {
                        case BacnetObjectType.ObjectPositiveIntegerValue:
                            bacnetDataType = BacnetDataType.BacnetDTUnsigned;
                            break;
                        default:
                            bacnetDataType = BacnetDataType.BacnetDTReal;
                            break;
                    }
                    return bacnetDataType;
                case BacnetPropertyID.PropDeadBand:
                case BacnetPropertyID.PropCOVIncrement:
                case BacnetPropertyID.PropMaxPresValue:
                case BacnetPropertyID.PropMinPresValue:
                //case BacnetPropertyID.PropProprietaryColorTemperature:
                case BacnetPropertyID.PropResolution:
                    bacnetDataType = BacnetDataType.BacnetDTReal;
                    return bacnetDataType;
                case BacnetPropertyID.PropStopWhenFull:
                case BacnetPropertyID.PropEnable:
                case BacnetPropertyID.PropDayLightSavingsStatus:
                    bacnetDataType = BacnetDataType.BacnetDTBoolean;
                    return bacnetDataType;

                case BacnetPropertyID.PropUtcOffSet:
                    bacnetDataType = BacnetDataType.BacnetDTInteger;
                    return bacnetDataType;

                case BacnetPropertyID.PropApduTimesOut:
                case BacnetPropertyID.PropApduSegmentTimesOut:
                case BacnetPropertyID.PropNumberOfApduRetries:
                    bacnetDataType = BacnetDataType.BacnetDTUnsigned32;
                    return bacnetDataType;
                case BacnetPropertyID.PropNotifyType:
                case BacnetPropertyID.PropSystemStatus:
                case BacnetPropertyID.PropReliability:
                case BacnetPropertyID.PropNodeType:
                    bacnetDataType = BacnetDataType.BacnetDTEnumNew;
                    return bacnetDataType;

                case BacnetPropertyID.PropStateText:
                    bacnetDataType = BacnetDataType.BacnetDTCharStringArray;
                    return bacnetDataType;
                case BacnetPropertyID.PropPresentValue:
                    switch (objectType)
                    {
                        case BacnetObjectType.ObjectAnalogInput:
                            bacnetDataType = BacnetDataType.BacnetDTReal;
                            break;
                        case BacnetObjectType.ObjectBinaryInput:
                            bacnetDataType = BacnetDataType.BacnetDTEnum;
                            break;
                    }
                    return bacnetDataType;
                case BacnetPropertyID.PropSubordinateList:
                    bacnetDataType = BacnetDataType.BacnetDTDevObjReffArray;
                    return bacnetDataType;
                case BacnetPropertyID.PropProtocolObjectTypesSupported:
                    bacnetDataType = BacnetDataType.BacnetDTObjectTypeSupported;
                    return bacnetDataType;
                case BacnetPropertyID.PropProtocolServicesSupported:
                    bacnetDataType = BacnetDataType.BacnetDTServicesSupported;
                    return bacnetDataType;
                case BacnetPropertyID.PropEffectivePeriod:
                    bacnetDataType = BacnetDataType.BacnetDTDateRange;
                    return bacnetDataType;

                case BacnetPropertyID.PropLogInterval:
                case BacnetPropertyID.PropPriorityForWriting:
                    bacnetDataType = BacnetDataType.BacnetDTUnsigned32;
                    return bacnetDataType;
                case BacnetPropertyID.PropLogDeviceObjectProperty:
                case BacnetPropertyID.PropListOfObjectPropertyReferences:
                    bacnetDataType = BacnetDataType.BacnetDTDevObjPropReffList;
                    return bacnetDataType;
                case BacnetPropertyID.PropWeeklySchedule:
                    bacnetDataType = BacnetDataType.BacnetDTDailyScheduleArray;
                    return bacnetDataType;
                case BacnetPropertyID.PropExceptionSchedule:
                    bacnetDataType = BacnetDataType.BacnetDTSpecialEventArray;
                    return bacnetDataType;
                case BacnetPropertyID.PropScheduleDefault:
                    bacnetDataType = BacnetDataType.BacnetDTSchedulePresentDefault;
                    return bacnetDataType;
                case BacnetPropertyID.PropDateList:
                    bacnetDataType = BacnetDataType.BacnetDTDateList;
                    return bacnetDataType;
                case BacnetPropertyID.PropStartTime:
                case BacnetPropertyID.PropStopTime:
                    bacnetDataType = BacnetDataType.BacnetDTDateTime;
                    return bacnetDataType;
                case BacnetPropertyID.PropEventParameters:
                    bacnetDataType = BacnetDataType.BacnetDTEventParameters; ;
                    return bacnetDataType;
                case BacnetPropertyID.PropObjectPropertyReference:
                    bacnetDataType = BacnetDataType.BacnetDTDevObjPropReff;
                    return bacnetDataType;
                case BacnetPropertyID.PropRecipientList:
                    bacnetDataType = BacnetDataType.BacnetDTDestinationList;
                    return bacnetDataType;
                case BacnetPropertyID.PropEventEnable:
                case BacnetPropertyID.PropAckRequired:
                case BacnetPropertyID.PropLimitEnable:
                    bacnetDataType = BacnetDataType.BacnetDTBitString;
                    return bacnetDataType;
                case BacnetPropertyID.PropAlarmValue:
                    bacnetDataType = BacnetDataType.BacnetDTEnum;
                    return bacnetDataType;
                default:
                    Logger.Instance.Log(LogLevel.ERROR, "StackManager GetBacnetDataType: BACnet data type not found for property:" + bacnetPropertyID);
                    bacnetDataType = 0;
                    return 0;
            }
        }

        /// <summary>
        /// Sets the device password.
        /// </summary>
        /// <param name="deviceId">The device identifier.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>19-06-201703:15 PM</TimeStamp>
        public ResponseResult SetDevicePassword(int deviceId, string password)
        {
            ResponseResult result = StackManagedApi.SetDevicePassword(deviceId, password);
            if (!result.IsSucceed)
            {
                Logger.Instance.Log(LogLevel.ERROR, "StackManager.SetDevicePassword: Set vendor Password fail for Device Id " + deviceId);
            }
            return result;
        }

        /// <summary>
        /// Gets the object Property value from BACnet stack.
        /// </summary>
        /// <param name="deviceId">The device identifier.</param>
        /// <param name="objectId">The object identifier.</param>
        /// <param name="objectType">Type of the object.</param>
        /// <param name="objectProperty">The object property.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        /// <param name="isArrayIndexPresent">if set to <c>true</c> [is array index present].</param>
        /// <returns></returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>6/21/20176:44 PM</TimeStamp>
        public ResponseResult GetObjectPropertyValue(uint deviceId, uint objectId, BacnetObjectType objectType, BacnetPropertyID objectProperty, uint arrayIndex = 0, bool isArrayIndexPresent = false)
        {
            ResponseResult result = StackManagedApi.GetObjectPropertyValue(deviceId, objectId, objectType, objectProperty, arrayIndex, isArrayIndexPresent);

            if (!result.IsSucceed)
            {
                Logger.Instance.Log(LogLevel.ERROR, "StackManager.GetObjectPresentValue: Get object present value failed for objectType:'" + objectType + "',Property :'" + objectProperty + "',objectId: '" + objectId + "',deviceId: '" + deviceId + "'");
            }
            return result;
        }

        /// <summary>
        /// Registers the i am delay.
        /// </summary>
        /// <param name="delayTime">The delay time.</param>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>8/7/201711:50 AM</TimeStamp>
        public static bool RegisterPacketDelay(uint delayTime)
        {
            Logger.Instance.Log(LogLevel.DEBUG, "RegisterPacketDelay time in stack started");

            try
            {
                StackManagedApi.RegisterPacketDelay(delayTime);
                Logger.Instance.Log(LogLevel.DEBUG, "RegisterPacketDelay time in stack completed");
                return true;
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "StackManager:RegisterPacketDelay: fail to register packet delay register time in stack");
                return false;
            }
        }

        /// <summary>
        /// Sets the virtual network number.
        /// </summary>
        /// <param name="virtualNetworkNumber">The virtual network number.</param>
        /// <returns></returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>8/28/20177:02 PM</TimeStamp>
        public static bool SetVirtualNetworkNumber(uint virtualNetworkNumber)
        {
            Logger.Instance.Log(LogLevel.DEBUG, "RegisterPacketDelay time in stack started");

            try
            {
                StackManagedApi.SetVirtualNetworkNumber(virtualNetworkNumber);
                Logger.Instance.Log(LogLevel.DEBUG, "SetVirtualNetworkNumber in stack completed");
                return true;
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "StackManager:SetVirtualNetworkNumber: fail to set virtual network number in stack");
                return false;
            }
        }

        /// <summary>
        /// Sets the local network number.
        /// </summary>
        /// <param name="localNetworkNumber">The local network number.</param>
        /// <returns></returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>8/28/20177:02 PM</TimeStamp>
        public static bool SetLocalNetworkNumber(uint localNetworkNumber)
        {
            Logger.Instance.Log(LogLevel.DEBUG, "RegisterPacketDelay time in stack started");

            try
            {
                StackManagedApi.SetLocalNetworkNumber(localNetworkNumber);
                Logger.Instance.Log(LogLevel.DEBUG, "SetLocalNetworkNumber in stack completed");
                return true;
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "StackManager:SetLocalNetworkNumber: fail to set local network number in stack");
                return false;
            }
        }

        /// <summary>
        /// Sets the type of the get property access.
        /// </summary>
        /// <param name="deviceId">The device identifier.</param>
        /// <param name="objectId">The object identifier.</param>
        /// <param name="bacnetObjectType">Type of the bacnet object.</param>
        /// <param name="bacnetPropertyID">The bacnet property identifier.</param>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>12/27/20173:57 PM</TimeStamp>
        public static void SetPropertyAccessType(uint deviceId, uint objectId, BacnetObjectType bacnetObjectType, BacnetPropertyID bacnetPropertyID, PropAccessType accessType, bool isReadOrWrite)
        {
            Logger.Instance.Log(LogLevel.DEBUG, "Set Property Access Type execution started");

            try
            {
                StackManagedApi.SetGetPropertyAccessType(deviceId, objectId, bacnetObjectType, bacnetPropertyID, accessType, isReadOrWrite);
                Logger.Instance.Log(LogLevel.DEBUG, "Set Property AccessType in stack completed");
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "StackManager:SetGetPropertyAccessType: fail to set property access type in stack");
            }
        }
        #endregion

        #region private methods

        /// <summary>
        /// Registers the callback.
        /// </summary>
        /// <returns>ResponseResult</returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>25-05-201702:54 PM</TimeStamp>
        private ResponseResult RegisterCallback()
        {
            Logger.Instance.Log(LogLevel.DEBUG, "RegisterCallback: Register call back - started");
            ResponseResult result = null;

            try
            {
                Dictionary<BacAppCallBackFunChoice, List<CallBackModel>> callbacks = new Dictionary<BacAppCallBackFunChoice, List<CallBackModel>>();

                #region Writeproperty callbacks registeration
                //For analog Value
                CallBackModel callbackModel = new CallBackModel();
                callbackModel.ObjectType = BacnetObjectType.ObjectAnalogValue;
                callbackModel.PropertyIds.Add(BacnetPropertyID.PropPresentValue);
                callbackModel.PropertyIds.Add(BacnetPropertyID.PropDescription);
                callbackModel.PropertyIds.Add(BacnetPropertyID.PropObjectName);
                callbackModel.PropertyIds.Add(BacnetPropertyID.PropOutOfService);
                callbackModel.PropertyIds.Add(BacnetPropertyID.PropRelinquishDefault);
                callbackModel.PropertyIds.Add(BacnetPropertyID.PropHighLimit);
                callbackModel.PropertyIds.Add(BacnetPropertyID.PropLowLimit);
                //For Multistate value
                CallBackModel callbackMSVModel = new CallBackModel();
                callbackMSVModel.ObjectType = BacnetObjectType.ObjectMultiStateValue;
                callbackMSVModel.PropertyIds.Add(BacnetPropertyID.PropPresentValue);
                callbackMSVModel.PropertyIds.Add(BacnetPropertyID.PropDescription);
                callbackMSVModel.PropertyIds.Add(BacnetPropertyID.PropObjectName);
                callbackMSVModel.PropertyIds.Add(BacnetPropertyID.PropOutOfService);
                //callbackMSVModel.PropertyIds.Add(BacnetPropertyID.PropProprietaryMood);
                callbackMSVModel.PropertyIds.Add(BacnetPropertyID.PropRelinquishDefault);
                //For Positive Integer value
                CallBackModel callbackPIVModel = new CallBackModel();
                callbackPIVModel.ObjectType = BacnetObjectType.ObjectPositiveIntegerValue;
                callbackPIVModel.PropertyIds.Add(BacnetPropertyID.PropPresentValue);
                callbackPIVModel.PropertyIds.Add(BacnetPropertyID.PropObjectName);
                callbackPIVModel.PropertyIds.Add(BacnetPropertyID.PropDescription);
                callbackPIVModel.PropertyIds.Add(BacnetPropertyID.PropOutOfService);
                callbackPIVModel.PropertyIds.Add(BacnetPropertyID.PropRelinquishDefault);
                callbackPIVModel.PropertyIds.Add(BacnetPropertyID.PropHighLimit);
                callbackPIVModel.PropertyIds.Add(BacnetPropertyID.PropLowLimit);
                //For Analog Input
                CallBackModel callbackAIModel = new CallBackModel();
                callbackAIModel.ObjectType = BacnetObjectType.ObjectAnalogInput;
                callbackAIModel.PropertyIds.Add(BacnetPropertyID.PropDescription);
                callbackAIModel.PropertyIds.Add(BacnetPropertyID.PropObjectName);
                callbackAIModel.PropertyIds.Add(BacnetPropertyID.PropOutOfService);
                callbackAIModel.PropertyIds.Add(BacnetPropertyID.PropHighLimit);
                callbackAIModel.PropertyIds.Add(BacnetPropertyID.PropLowLimit);
                //For Binary Input
                CallBackModel callbackBIModel = new CallBackModel();
                callbackBIModel.ObjectType = BacnetObjectType.ObjectBinaryInput;
                callbackBIModel.PropertyIds.Add(BacnetPropertyID.PropDescription);
                callbackBIModel.PropertyIds.Add(BacnetPropertyID.PropObjectName);
                callbackBIModel.PropertyIds.Add(BacnetPropertyID.PropOutOfService);
                //For Notification class
                CallBackModel callbackNotificationModel = new CallBackModel();
                callbackNotificationModel.ObjectType = BacnetObjectType.ObjectNotificationClass;
                callbackNotificationModel.PropertyIds.Add(BacnetPropertyID.PropAckRequired);
                callbackNotificationModel.PropertyIds.Add(BacnetPropertyID.PropPriority);
                callbackNotificationModel.PropertyIds.Add(BacnetPropertyID.PropRecipientList);
                callbackNotificationModel.PropertyIds.Add(BacnetPropertyID.PropDescription);
                //For ObjectDevice 
                CallBackModel callbackObjectDeviceModel = new CallBackModel();
                callbackObjectDeviceModel.ObjectType = BacnetObjectType.ObjectDevice;
                callbackObjectDeviceModel.PropertyIds.Add(BacnetPropertyID.PropObjectName);
                callbackObjectDeviceModel.PropertyIds.Add(BacnetPropertyID.PropDescription);
                callbackObjectDeviceModel.PropertyIds.Add(BacnetPropertyID.PropNumberOfApduRetries);
                callbackObjectDeviceModel.PropertyIds.Add(BacnetPropertyID.PropApduSegmentTimesOut);
                callbackObjectDeviceModel.PropertyIds.Add(BacnetPropertyID.PropApduTimesOut);
                //For Structured view
                CallBackModel callbackStructureViewModel = new CallBackModel();
                callbackStructureViewModel.ObjectType = BacnetObjectType.ObjectStructuredView;
                callbackStructureViewModel.PropertyIds.Add(BacnetPropertyID.PropObjectName);
                callbackStructureViewModel.PropertyIds.Add(BacnetPropertyID.PropDescription);
                //For Schedule Object
                CallBackModel callbackScheduleObject = new CallBackModel();
                callbackScheduleObject.ObjectType = BacnetObjectType.ObjectSchedule;
                callbackScheduleObject.PropertyIds.Add(BacnetPropertyID.PropPriorityForWriting);
                callbackScheduleObject.PropertyIds.Add(BacnetPropertyID.PropScheduleDefault);
                callbackScheduleObject.PropertyIds.Add(BacnetPropertyID.PropEffectivePeriod);
                callbackScheduleObject.PropertyIds.Add(BacnetPropertyID.PropListOfObjectPropertyReferences);
                callbackScheduleObject.PropertyIds.Add(BacnetPropertyID.PropWeeklySchedule);
                callbackScheduleObject.PropertyIds.Add(BacnetPropertyID.PropExceptionSchedule);
                callbackScheduleObject.PropertyIds.Add(BacnetPropertyID.PropDescription);

                //For trendLog
                CallBackModel callbackTrendLogObject = new CallBackModel();
                callbackTrendLogObject.ObjectType = BacnetObjectType.ObjectTrendlog;
                callbackTrendLogObject.PropertyIds.Add(BacnetPropertyID.PropStartTime);
                callbackTrendLogObject.PropertyIds.Add(BacnetPropertyID.PropStopTime);
                callbackTrendLogObject.PropertyIds.Add(BacnetPropertyID.PropEnable);
                callbackTrendLogObject.PropertyIds.Add(BacnetPropertyID.PropStopWhenFull);
                callbackTrendLogObject.PropertyIds.Add(BacnetPropertyID.PropLogDeviceObjectProperty);
                callbackTrendLogObject.PropertyIds.Add(BacnetPropertyID.PropLogInterval);
                callbackTrendLogObject.PropertyIds.Add(BacnetPropertyID.PropDescription);
                //For Event enrollment
                CallBackModel callbackEventEnrollmentObject = new CallBackModel();
                callbackEventEnrollmentObject.ObjectType = BacnetObjectType.ObjectEventEnrollment;
                callbackEventEnrollmentObject.PropertyIds.Add(BacnetPropertyID.PropNotifyType);
                callbackEventEnrollmentObject.PropertyIds.Add(BacnetPropertyID.PropEventEnable);
                callbackEventEnrollmentObject.PropertyIds.Add(BacnetPropertyID.PropNotificationClass);
                callbackEventEnrollmentObject.PropertyIds.Add(BacnetPropertyID.PropObjectPropertyReference);
                callbackEventEnrollmentObject.PropertyIds.Add(BacnetPropertyID.PropEventParameters);
                callbackEventEnrollmentObject.PropertyIds.Add(BacnetPropertyID.PropDescription);

                CallBackModel callbackCalendarObject = new CallBackModel();
                callbackCalendarObject.ObjectType = BacnetObjectType.ObjectCalendar;
                callbackCalendarObject.PropertyIds.Add(BacnetPropertyID.PropDateList);
                callbackCalendarObject.PropertyIds.Add(BacnetPropertyID.PropDescription);

                //CallBackModel callbackDeviceCommunicationControlObject = new CallBackModel();
                // callbackDeviceCommunicationControlObject.ObjectType = BacnetObjectType;
                // callbacks.Add(BacAppCallBackFunChoice.AppCbCOVNotification, null);
                //callbacks.Add(BacAppCallBackFunChoice.AppCbCreateObject, null);
                // callbacks.Add(BacAppCallBackFunChoice.AppCbDeleteObject, null);
                //callbacks.Add(BacAppCallBackFunChoice.AppCbIAm, null);

                //For binary Value
                CallBackModel callbackBVModel = new CallBackModel();
                callbackBVModel.ObjectType = BacnetObjectType.ObjectBinaryValue;
                callbackBVModel.PropertyIds.Add(BacnetPropertyID.PropPresentValue);
                callbackBVModel.PropertyIds.Add(BacnetPropertyID.PropDescription);
                callbackBVModel.PropertyIds.Add(BacnetPropertyID.PropOutOfService);
                callbackBVModel.PropertyIds.Add(BacnetPropertyID.PropRelinquishDefault);

                callbacks.Add(BacAppCallBackFunChoice.AppCbWriteProperty, new List<CallBackModel>());
                callbacks[BacAppCallBackFunChoice.AppCbWriteProperty].Add(callbackModel);
                callbacks[BacAppCallBackFunChoice.AppCbWriteProperty].Add(callbackMSVModel);
                callbacks[BacAppCallBackFunChoice.AppCbWriteProperty].Add(callbackPIVModel);
                callbacks[BacAppCallBackFunChoice.AppCbWriteProperty].Add(callbackAIModel);
                callbacks[BacAppCallBackFunChoice.AppCbWriteProperty].Add(callbackBIModel);
                callbacks[BacAppCallBackFunChoice.AppCbWriteProperty].Add(callbackNotificationModel);
                callbacks[BacAppCallBackFunChoice.AppCbWriteProperty].Add(callbackObjectDeviceModel);
                callbacks[BacAppCallBackFunChoice.AppCbWriteProperty].Add(callbackStructureViewModel);
                callbacks[BacAppCallBackFunChoice.AppCbWriteProperty].Add(callbackScheduleObject);
                callbacks[BacAppCallBackFunChoice.AppCbWriteProperty].Add(callbackTrendLogObject);
                callbacks[BacAppCallBackFunChoice.AppCbWriteProperty].Add(callbackEventEnrollmentObject);
                callbacks[BacAppCallBackFunChoice.AppCbWriteProperty].Add(callbackCalendarObject);
                callbacks[BacAppCallBackFunChoice.AppCbWriteProperty].Add(callbackBVModel);

                #endregion

                #region Create Service Registeration

                callbacks.Add(BacAppCallBackFunChoice.AppCbCreateObject, new List<CallBackModel>());
                callbacks[BacAppCallBackFunChoice.AppCbCreateObject].Add(callbackScheduleObject);
                callbacks[BacAppCallBackFunChoice.AppCbCreateObject].Add(callbackNotificationModel);
                callbacks[BacAppCallBackFunChoice.AppCbCreateObject].Add(callbackTrendLogObject);
                callbacks[BacAppCallBackFunChoice.AppCbCreateObject].Add(callbackEventEnrollmentObject);
                callbacks[BacAppCallBackFunChoice.AppCbCreateObject].Add(callbackCalendarObject);

                #endregion


                callbacks.Add(BacAppCallBackFunChoice.AppCbDeleteObject, new List<CallBackModel>());
                callbacks[BacAppCallBackFunChoice.AppCbDeleteObject].Add(callbackScheduleObject);
                callbacks[BacAppCallBackFunChoice.AppCbDeleteObject].Add(callbackNotificationModel);
                callbacks[BacAppCallBackFunChoice.AppCbDeleteObject].Add(callbackTrendLogObject);
                callbacks[BacAppCallBackFunChoice.AppCbDeleteObject].Add(callbackEventEnrollmentObject);
                callbacks[BacAppCallBackFunChoice.AppCbDeleteObject].Add(callbackCalendarObject);


                #region Reinitialize Devices

                callbacks.Add(BacAppCallBackFunChoice.AppCbReinitializeDevice, new List<CallBackModel>());

                #endregion

                #region timeSyncService
                callbacks.Add(BacAppCallBackFunChoice.AppCbTimeSync, null);
                callbacks.Add(BacAppCallBackFunChoice.AppCbUtcTimeSync, null);
                callbacks.Add(BacAppCallBackFunChoice.AppCbDeviceCommunicationControl, null);
                #endregion timeSyncService


                callbacks.Add(BacAppCallBackFunChoice.AppCbAddListElement, new List<CallBackModel>());

                callbacks.Add(BacAppCallBackFunChoice.AppCbRemoveListElement, new List<CallBackModel>());

                result = StackManagedApi.RegisterCallBacks(callbacks);

                //This is a after call back. Stack does not need any response from this call back.
                StackManagedApi.RegisterInternalCallback();

                if (!result.IsSucceed)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "Callback registers fail during stack initialize");
                }

                WPCallbackHandler.OnMessage = WritePropertyHandler.OnWriteProperty;
                CreateObjectCallbackHandler.OnMessage = CreateObjectHandler.OnCreateObject;
                DeleteObjectCallbackHandler.OnMessage = DeleteObjectHandler.OnDeleteObject;
                ReInitDeviceCallbackHandler.OnMessage = ReInitializeDeviceHandler.OnReInitializeDevice;
                TimeSyncCallbackHandler.OnMessage = TimeSyncHandler.OnTimeSync;
                DeviceCommunicationControlCallbackHandler.OnMessage = DeviceCommunicationHandler.OnDCC;
                AddListElementCallBackHandler.OnMessage = AddListElementHandler.OnAddListElementHandler;
                ListElementCallBackHandler.OnMessage = ListElementProcessHandler.OnListElementProcessHandler;
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR);
            }

            Logger.Instance.Log(LogLevel.DEBUG, "RegisterCallback: Register call back - completed");

            return result;
        }


        #endregion
    }
}
