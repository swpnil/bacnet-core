﻿
using Molex.BACnet.Gateway.Log;
using Molex.BACnet.Gateway.ServiceCore.Constants;
using Molex.BACnet.Gateway.ServiceCore.Helpers;
using Molex.BACnet.Gateway.ServiceCore.Models;
using Molex.BACnet.Gateway.ServiceCore.Models.Entity;
using Molex.BACnet.Gateway.ServiceCore.Repository;
using Molex.BACnet.Gateway.ServiceCore.SingletonCore;
using Molex.StackDataObjects.Constants;
using System;
using System.Timers;
namespace Molex.BACnet.Gateway.ServiceCore.Managers
{
    /// <summary>
    /// This class used to check api pub-sub status aftery regular interval of time
    /// </summary>
    /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>3/29/20186:45 PM</TimeStamp>
    /// <seealso cref="Molex.BACnet.Gateway.ServiceCore.SingletonCore.SingletonBase{Molex.BACnet.Gateway.ServiceCore.Managers.HeartBeatManager}" />
    public class HeartBeatManager : SingletonBase<HeartBeatManager>
    {
        #region Private Fields
        private Timer _timer;
        #endregion

        #region Initialization and Deinitialization Handlers

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>3/29/20186:43 PM</TimeStamp>
        public void Init()
        {
            if (!AppCoreRepo.Instance.BACnetConfigurationData.IsMQTTPublishSelected)
            {
                _timer = new Timer();
                //Config file will have value in seconds. Interval understands milliseconds. Hence *1000
                _timer.Interval = AppCoreRepo.Instance.HeartBeatTimerIntervalInSeconds * 1000;
                _timer.Elapsed += ElapsedTime;
                Start();
            }
        }

        /// <summary>
        /// Des the initialize.
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>3/29/20186:43 PM</TimeStamp>
        public void DeInit()
        {
            Stop();
            Logger.Instance.Log(LogLevel.DEBUG, "HeartBeatManager.DeInit, De-Initialization Called");
        }
        #endregion

        #region Timer Start, Stop and Elapse Handlers

        /// <summary>
        /// Starts this instance.
        /// </summary>
        /// <returns></returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>3/29/20186:43 PM</TimeStamp>
        public bool Start()
        {
            try
            {
                _timer.Start();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// Elapseds the time.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="ElapsedEventArgs"/> instance containing the event data.</param>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>3/29/20186:43 PM</TimeStamp>
        private void ElapsedTime(object sender, ElapsedEventArgs e)
        {
            ResultModel<string> result = HTTPAPIManager.Instance.GetAPILiveStatus(AppCoreRepo.Instance.ClientID);

            if (!result.IsSuccess)
            {
                try
                {
                    Logger.Instance.Log(LogLevel.ERROR, "Molex API in fault state");
                    string bacnetObjectKey = CommonHelper.GenerateBACnetObjectMappedID(AppCoreRepo.Instance.BACnetConfigurationData.ControllerId, BacnetObjectType.ObjectDevice, AppCoreRepo.Instance.BACnetConfigurationData.ControllerId);
                    EntityBaseModel entityBaseModel = AppCoreRepo.Instance.BACnetObjectEntityLookUp[bacnetObjectKey];
                    MonitoredDataStackUpdateHelper.UpdateSystemStatus(entityBaseModel, BacnetDeviceStatus.StatusNonOperational);

                    foreach (string zoneID in AppCoreRepo.Instance.ZonesLookUp.Keys)
                    {
                        entityBaseModel = AppCoreRepo.Instance.ZonesLookUp[zoneID];
                        MonitoredDataStackUpdateHelper.UpdateSystemStatus(entityBaseModel, BacnetDeviceStatus.StatusNonOperational);
                    }

                    //On auto unsubscribe confirmation (with error code 1048) of BACnet Gateway from API resubscribe done again by GW
                    if (result.Error.ErrorCode == AppConstants.API_AUTO_UNSUBSCRIBE_ERROR_CODE)
                    {
                        Logger.Instance.Log(LogLevel.ERROR, "HeartBeatManager:ElapsedTime:Gateway resubscribing due to API auto unsubscribe Gateway");
                        StartUpManager.Instance.APISubscription(StartUpManager.Instance.GetLocalEndPointAddress());
                    }
                }
                catch (Exception ex)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "HeartBeatManager:ElapsedTime:Update system status failed for devices");
                }
            }
        }


        /// <summary>
        /// Stops this instance.
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>3/29/20186:43 PM</TimeStamp>
        private void Stop()
        {
            if (_timer != null)
            {
                _timer.Stop();
                _timer.Close();
            }
        }
        #endregion
    }
}
