﻿using Molex.BACnet.Gateway.Log;
using Molex.BACnet.Gateway.ServiceCore.AppResources;
using Molex.BACnet.Gateway.ServiceCore.Constants;
using Molex.BACnet.Gateway.ServiceCore.Controllers;
using Molex.BACnet.Gateway.ServiceCore.Helpers;
using Molex.BACnet.Gateway.ServiceCore.Models;
using Molex.BACnet.Gateway.ServiceCore.Models.Entity;
using Molex.BACnet.Gateway.ServiceCore.Models.MolexAPIModels;
using Molex.BACnet.Gateway.ServiceCore.Repository;
using Molex.BACnet.Gateway.ServiceCore.SingletonCore;
using Molex.StackDataObjects.Constants;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Managers
{
    internal class MonitoringDataManager
    {
        /// <summary>
        /// Processes the specified queue item.
        /// </summary>
        /// <param name="publishData">The queue item.</param>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>7/7/20174:09 PM</TimeStamp>
        /// <CreatedBy>Rohit Galgali</CreatedBy><TimeStamp>25-05-201705:51 PM</TimeStamp>
        public void Process(object publishData)
        {
            try
            {
                MolexAPIPublishModel queueItem = JsonConvert.DeserializeObject<MolexAPIPublishModel>(publishData.ToString());
                AppConstants.SubscriptionType subscriptionType;
                string topic = queueItem.topic;
 
                if (topic.Contains(AppConstants.SubscriptionType.occupancy.ToString()))
                    topic = AppConstants.SubscriptionType.occupancy.ToString();
                if (topic.Contains(AppConstants.SubscriptionType.average.ToString()))
                    topic = AppConstants.SubscriptionType.average.ToString();
                if (topic.Contains(AppConstants.SubscriptionType.power.ToString()))
                    topic = AppConstants.SubscriptionType.power.ToString();
                
                if (Enum.TryParse(topic, true, out subscriptionType))
                {
                    switch (subscriptionType)
                    {
                        case AppConstants.SubscriptionType.notification:
                            queueItem.id = queueItem.data["id"].ToString();
                            UpdateNotification(queueItem);
                            RestartProjectNotification(queueItem);
                            break;
                        case AppConstants.SubscriptionType.status:
                            //Logger.Instance.Log(LogLevel.ERROR, "MonitoringDataManager:Process: " + queueItem.topic + " json => " + queueItem.data.ToString());

                            queueItem.id = queueItem.data["id"].ToString();
                            UpdateReliability(queueItem);
                            break;
                        case AppConstants.SubscriptionType.sensorData:
                            if (AppCoreRepo.Instance.BACnetConfigurationData.MapIndividualSensors)
                            {
                                queueItem.id = queueItem.data["zoneID"].ToString();
                                UpdateZoneSensorValues(queueItem);
                            }
                            break;
                        case AppConstants.SubscriptionType.configurations:
                            //if (queueItem.data["afterIdleTimeout"] != null)
                            queueItem.id = queueItem.data["id"].ToString();
                            UpdateZoneConfigurationData(queueItem);
                            break;
                        case AppConstants.SubscriptionType.properties:
                            queueItem.id = queueItem.data["id"].ToString();
							UpdateZoneObjectsPropertyValues(queueItem);                            
                            break;                        
                        case AppConstants.SubscriptionType.average:
                        case AppConstants.SubscriptionType.power:
                            //Logger.Instance.Log(LogLevel.ERROR, "MonitoringDataManager:Process: " + subscriptionType + " json => " + queueItem.data.ToString());
                            string floorId = queueItem.data["floorID"].ToString();
                            AppConstants.SensorType sensorType = MolexAPIConverterHelper.GetSensorType(queueItem.data["resourceType"].ToString());
                            float average = float.Parse(queueItem.data["value"].ToString());
                            UpdateFloorSensorAverage(floorId, sensorType, average);
                            break;
                        default:
                            break;
                    }

                    if (Logger.Instance.GetLogFlag(0) == (int)(subscriptionType))
                    {
                        Logger.Instance.Log(LogLevel.ERROR, "MonitoringDataManager:Process: " + subscriptionType + " json => " + queueItem.data.ToString());
                    }
                }


                queueItem = null;
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "MonitoringDataManager:Process(JObject publishData) Exception Received");
            }
        }

        /// <summary>
        /// This method will restart the BACnet Gateway if any project configuration modification has been done
        /// </summary>
        /// <param name="queueItem"></param>
        private void RestartProjectNotification(MolexAPIPublishModel queueItem)
        {
            try
            {
                if (queueItem == null)
                    return;
                if (queueItem.data == null)
                    return;
                if (queueItem.data["id"].ToString() != AppCoreRepo.Instance.MolexProjectID.ToString())
                    return;
                if (queueItem.data["type"].ToString() != AppResources.AppResource.ConfigurationType)
                    return;
                if (queueItem.data["resourceType"].ToString() != AppResources.AppResource.ProjectResourceType)
                    return;
                DateTime additionalInfo;
                DateTime.TryParse(queueItem.data["additionalInfo"].ToString(), out additionalInfo);

                if (AppCoreRepo.Instance.BACnetConfigurationData.IsMQTTPublishSelected) // do this check only in case of MQTT
                {
                    //Compare DateTime in UTC
                    TimeSpan outDifference = DateTime.Now.ToUniversalTime() - additionalInfo;
                    if (outDifference.TotalSeconds > 20)// check if the project restart time is less than 20 sec with current time
                        return;
                }
                if (!AppHelper.IsGatewayStartCompleted)
                    return;
                AppHelper.IsGatewayStartCompleted = false;//BACnet Gateway Re-Start received.
                //Now Re-Start the project
                AppHelper.DeInit();//stop all the components
                Thread.Sleep(1000);//Thread sleep to wait for all listeners & timers to close
                AppHelper.Start();//start all the components
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "MonitoringDataManager:RestartProjectNotification Exception Received");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queueItem"></param>
        private void UpdateZoneObjectsPropertyValues(MolexAPIPublishModel queueItem)
        {
            try
            {
                if (AppCoreRepo.Instance.ZonesLookUp.ContainsKey(queueItem.id))
                {
                    AppConstants.PacketType packetType;

                    if (Enum.TryParse(queueItem.data["type"].ToString(), true, out packetType))
                    {
                        ZoneEntityModel zoneEntityModel = AppCoreRepo.Instance.ZonesLookUp[queueItem.id] as ZoneEntityModel;
                        var Data = queueItem.data["value"];
                        switch (packetType)
                        {
                            case AppConstants.PacketType.beacon:
                                {
                                    if (Data != null)
                                    {
                                        int state = Data.ToString().ToUpperInvariant().Trim() == "OFF" ? 0 : 1;
                                        MonitoredDataStackUpdateHelper.UpdateZoneState(state, zoneEntityModel);
                                    }
                                }
                                break;
                            case AppConstants.PacketType.light:
                                {
                                    if (Data != null)
                                    {
                                        int state = Data.ToString().ToUpperInvariant().Trim() == "OFF" ? 0 : 1;
                                        MonitoredDataStackUpdateHelper.UpdateZoneState(state, zoneEntityModel);
                                    }
                                }
                                break;
                            case AppConstants.PacketType.occupied:
                                {
                                    if (Data != null)
                                    {
                                        int occupied = Data.ToString().ToUpperInvariant() == "TRUE" ? 1 : 0;
                                        MonitoredDataStackUpdateHelper.UpdateZoneOccupancyState(occupied, zoneEntityModel);
                                    }
                                }
                                break;
                            case AppConstants.PacketType.lightScene:
                                MonitoredDataStackUpdateHelper.UpdateLightScene(Data["scene"].ToString(), zoneEntityModel);
                                if (zoneEntityModel.ZoneType != AppConstants.ZoneTypes.BeaconSpace)
                                    MonitoredDataStackUpdateHelper.UpdateMood(Data["mood"].ToString(), zoneEntityModel);
                                else
                                    MonitoredDataStackUpdateHelper.UpdateBeaconPalettes(Data["mood"].ToString(), zoneEntityModel);
                                break;
                            case AppConstants.PacketType.brightness:
                                if (zoneEntityModel.ZoneType != AppConstants.ZoneTypes.BlindSpace)
                                {
                                    BrightnessDataObject brightnessObject = Newtonsoft.Json.JsonConvert.DeserializeObject<BrightnessDataObject>(Data.ToString());
                                    if (brightnessObject != null)
                                    {
                                        float value = 0;
                                        float.TryParse(brightnessObject.appliedBrightness, out value);
                                        MonitoredDataStackUpdateHelper.UpdateBrightness(value, zoneEntityModel);
                                    }
                                }
                                break;
                            case AppConstants.PacketType.palette:
                                switch (zoneEntityModel.ZoneType)
                                {
                                    case AppConstants.ZoneTypes.BeaconSpace:
                                        BeaconPaletteModel _BeaconPaletteModel = new BeaconPaletteModel();
                                        _BeaconPaletteModel.PaletteModelList = JsonConvert.DeserializeObject<PaletteModel[]>(Convert.ToString(Data["applied"]));
                                        if (_BeaconPaletteModel != null)
                                        {
                                            MonitoredDataStackUpdateHelper.UpdatePIVBeaconPalattes(_BeaconPaletteModel, zoneEntityModel);
                                        }
                                        break;
                                    default:
                                        break;
                                }
                                break;
                            case AppConstants.PacketType.sensorStats:
                                SetSensorAVGValue(Data, zoneEntityModel);
                                break;
                            case AppConstants.PacketType.state:
                                int oldstate = Data.ToList().Contains("off") ? 0 : 1;
                                MonitoredDataStackUpdateHelper.UpdateZoneState(oldstate, zoneEntityModel);
                                break;
                            case AppConstants.PacketType.blind:
                                if (zoneEntityModel.ZoneType == AppConstants.ZoneTypes.BlindSpace)
                                {
                                    BlindZoneLevelModel _BlindZoneLevelModel = Utility.Helper.JSONReaderWriterHelper.ReadJsonFromString<BlindZoneLevelModel>(Data.ToString());
                                    if (_BlindZoneLevelModel != null)
                                        MonitoredDataStackUpdateHelper.UpdateBlindLevel(_BlindZoneLevelModel, zoneEntityModel);
                                }
                                break;
                            case AppConstants.PacketType.biodynamic:
                                MonitoredDataStackUpdateHelper.UpdateBioDynamicControl((float)Data, zoneEntityModel);
                                break;
                            case AppConstants.PacketType.saturation:
                                MonitoredDataStackUpdateHelper.UpdateSaturationControl((float)Data, zoneEntityModel);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "MonitoringDataManager.Process:UpdateZoneConfigurationData failed for zone id :" + queueItem.id);
            }
        }

        /// <summary>
        /// SetSensorAVGValue
        /// </summary>
        /// <param name="Data"></param>
        /// <param name="zoneEntityModel"></param>
        private void SetSensorAVGValue(Newtonsoft.Json.Linq.JToken Data, ZoneEntityModel zoneEntityModel)
        {
            Dictionary<string, SensorStat> sensorStat = JsonConvert.DeserializeObject<Dictionary<string, SensorStat>>(Data.ToString());
            foreach (string role in sensorStat.Keys)
            {
                AppConstants.SensorType sensorType = MolexAPIConverterHelper.GetSensorType(role);
              
                switch(sensorType)
                {
                    case AppConstants.SensorType.Power:
                    case AppConstants.SensorType.Presence:
                        var totalval = !string.IsNullOrEmpty(sensorStat[role].total) ? Convert.ToSingle(sensorStat[role].total) : AppConstants.SENSOR_DEFAULT_VALUE;
                        MonitoredDataStackUpdateHelper.UpdateAverageSensorValues(totalval, zoneEntityModel, sensorType);
                        break;
                    case AppConstants.SensorType.AirQuality:
                    case AppConstants.SensorType.ColorTemperature:
                    case AppConstants.SensorType.Humidity:
                    case AppConstants.SensorType.LuxLevel:
                    case AppConstants.SensorType.Temperature:
                        var avgval = !string.IsNullOrEmpty(sensorStat[role].average) ? Convert.ToSingle(sensorStat[role].average) : AppConstants.SENSOR_DEFAULT_VALUE;
                        MonitoredDataStackUpdateHelper.UpdateAverageSensorValues(avgval, zoneEntityModel, sensorType);
                        break;
                    default:
                        break;
                }
                
            }
        }

        /// <summary>
        /// UpdateZoneConfigurationData
        /// </summary>
        /// <param name="queueItem"></param>
        private void UpdateZoneConfigurationData(MolexAPIPublishModel queueItem)
        {
            try
            {
                if (AppCoreRepo.Instance.ZonesLookUp.ContainsKey(queueItem.id))
                {
                    AppConstants.PacketType packetType;
                    if (Enum.TryParse(queueItem.data["type"].ToString(), true, out packetType))
                    {
                        ZoneEntityModel zoneEntityModel = AppCoreRepo.Instance.ZonesLookUp[queueItem.id] as ZoneEntityModel;
                        var Data = queueItem.data["value"];
                        switch (packetType)
                        {
                            case AppConstants.PacketType.dayLightHarvest:
                                switch (zoneEntityModel.ZoneType)
                                {
                                    case AppConstants.ZoneTypes.UserSpace:
                                    case AppConstants.ZoneTypes.GeneralSpace:
                                        MonitoredDataStackUpdateHelper.UpdatePIVPresentValue(Convert.ToInt64(Data["targetLux"]), zoneEntityModel, AppConstants.PositiveIntegerValueType.DHTargetLux);
                                        break;
                                    default:
                                        break;

                                }
                                break;
                            case AppConstants.PacketType.occupancy:
                                switch (zoneEntityModel.ZoneType)
                                {
                                    case AppConstants.ZoneTypes.UserSpace:
                                    case AppConstants.ZoneTypes.GeneralSpace:
                                        MonitoredDataStackUpdateHelper.UpdatePIVPresentValue(Convert.ToInt64(Data["timeout"]), zoneEntityModel, AppConstants.PositiveIntegerValueType.OccupancyTimeOut);
                                        MonitoredDataStackUpdateHelper.UpdatePIVPresentValue(Convert.ToInt64(Data["fadeout"]), zoneEntityModel, AppConstants.PositiveIntegerValueType.OccupancyFadeOutTime);
                                        if (Data["afterIdleTimeout"] != null)
                                            MonitoredDataStackUpdateHelper.UpdateMsvPresentValue(Data["afterIdleTimeout"].ToString(), zoneEntityModel, AppConstants.RESET_ATTRIBUTES_AFTER_TIMEOUT);
                                        break;
                                    default:
                                        break;
                                }
                                break;
                            case AppConstants.PacketType.lightScene:
                                {
                                    MonitoredDataStackUpdateHelper.UpdateZoneStateText(zoneEntityModel, queueItem);
                                    string key = EntityHelper.GetObjectDetailsKey(BacnetObjectType.ObjectMultiStateValue, AppConstants.LIGHTING_LIGHTSCENE_OBJECT);
                                    if (zoneEntityModel.BACnetData.ObjectDetails.ContainsKey(key) &&
                                        zoneEntityModel.BACnetData.ObjectDetails[key].PresentValue.GetType() != typeof(object) &&
                                        !string.IsNullOrEmpty(zoneEntityModel.BACnetData.ObjectDetails[key].PresentValue.ToString()))
                                        MonitoredDataStackUpdateHelper.UpdateLightScene(zoneEntityModel.BACnetData.ObjectDetails[key].PresentValue.ToString(), zoneEntityModel, true);

                                }
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "MonitoringDataManager.Process:UpdateZoneConfigurationData failed for zone id :" + queueItem.id);
            }

        }

        #region privatemethod
        /// <summary>
        /// To update zone individual sensor objects present value with API published sensor data
        /// </summary>
        /// <param name="queueItem"></param>
        private void UpdateZoneSensorValues(MolexAPIPublishModel queueItem)
        {
            try
            {
                if (AppCoreRepo.Instance.ZonesLookUp.ContainsKey(queueItem.id))
                {
                    ResultModel<MolexAPISensorSubRespModel> apiSensorSubRespModel = MolexAPIConverterHelper.ParseSensorSubRespData(Convert.ToString(queueItem.data));
                    if (!apiSensorSubRespModel.IsSuccess)
                    {
                        Logger.Instance.Log(LogLevel.ERROR, "MonitoringDataManager.Process:UpdateZoneSensorValue:fail to parse json data");
                        return;
                    }
                    //Handling done for the <null> values received for Sensor data, so that <0> value can not get written over the stack object
                    //in below case as we skip the writeproperty call the present value will either default -99 or the last received value.
                    if (string.IsNullOrEmpty(apiSensorSubRespModel.Data.value))
                        return;
                    SensorSubRespModel sensorSubRespModel = MolexAPIConverterHelper.ParseSensorSubRespModel(apiSensorSubRespModel.Data);
                    if (!AppCoreRepo.Instance.AllEntityLookUp.ContainsKey(sensorSubRespModel.SensorID))
                        return;
                    SensorEntityModel sensorModel = (SensorEntityModel)AppCoreRepo.Instance.AllEntityLookUp[sensorSubRespModel.SensorID];
                    MonitoredDataStackUpdateHelper.UpdateSensorPresentValue(sensorSubRespModel.Value, sensorModel);
                    sensorModel = null;
                    sensorSubRespModel = null;
                    apiSensorSubRespModel = null;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "MonitoringDataManager.Process:UpdateZoneSensorValues failed for sensor id :" + queueItem.id);
            }
        }

        /// <summary>
        /// Update 
        /// </summary>
        /// <param name="floorId"></param>
        /// <param name="sensorType"></param>
        /// <param name="value"></param>
        private void UpdateFloorSensorAverage(string floorId, AppConstants.SensorType sensorType, float value)
        {
            if (!AppCoreRepo.Instance.AllEntityLookUp.ContainsKey(floorId))
            {
                Logger.Instance.Log(LogLevel.ERROR, "UpdateFloorSensorAverage: FloorID: " + floorId + " NOT FOUND");
                return;
            }

            FloorEntityModel floorModel = (FloorEntityModel)(AppCoreRepo.Instance.AllEntityLookUp[floorId]);
            BacnetObjectType objectType = EntityHelper.GetBACnetObjectTypeBySensorType(sensorType);

            Logger.Instance.Log(LogLevel.ERROR, "UpdateFloorSensorAverage: FloorId: " + floorId + " " + sensorType.ToString() + " Value: " + value.ToString());
            //ObjectAnalogInput_Floor 1-Temperature
            string key = objectType.ToString() + "_" + floorModel.EntityName + "-" + sensorType.ToString();
            if (!floorModel.BACnetData.ObjectDetails.ContainsKey(key))
            {
                Logger.Instance.Log(LogLevel.ERROR, "UpdateFloorSensorAverage: FloorID: " + floorId + " KEY NOT FOUND => Key: " + key);
                return;
            }

            var result  = EntityHelper.GetDeviceId(floorModel);
            if (!result.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.ERROR, "UpdateFloorSensorAverage: FloorID: " + floorId + " DeviceId NOT FOUND for WriteProperty to stack");
                return;
            }
            Molex.BACnet.Gateway.ServiceCore.Models.BACnet.BACnetObjectDetails bacnetObjInfo = floorModel.BACnetData.ObjectDetails[key];

            if (sensorType == AppConstants.SensorType.Temperature && AppCoreRepo.Instance.BACnetConfigurationData.IsTempRequiredInFahrenheit)
            {
                value = EntityHelper.ConvertCelsiusToFahrenheit(value);
            }

            if (!StackManager.Instance.WriteProperty((uint)result.Data, objectType, 
                                                        bacnetObjInfo.ObjectID, value, 
                                                        StackDataObjects.Constants.BacnetPropertyID.PropPresentValue))
            {
                Logger.Instance.Log(LogLevel.ERROR, "Property update: Property update for Analog Input - Failed");
                return;
            }

            bacnetObjInfo.PresentValue = value;
        }

        public void UpdateFloorOnOffControl(string floorId, int value)
        {
            if (!AppCoreRepo.Instance.AllEntityLookUp.ContainsKey(floorId))
            {
                Logger.Instance.Log(LogLevel.ERROR, "UpdateFloorOnOffControl: FloorID: " + floorId + " NOT FOUND");
                return;
            }

            FloorEntityModel floorModel = (FloorEntityModel)(AppCoreRepo.Instance.AllEntityLookUp[floorId]);

            string key = BacnetObjectType.ObjectBinaryValue.ToString() + "_" + AppConstants.FLOOR_ONOFF_OBJECT;
            if (!floorModel.BACnetData.ObjectDetails.ContainsKey(key))
            {
                Logger.Instance.Log(LogLevel.ERROR, "UpdateFloorOnOffControl: FloorID: " + floorId + " KEY NOT FOUND => Key: " + key);
                return;
            }

            var result = EntityHelper.GetDeviceId(floorModel);
            if (!result.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.ERROR, "UpdateFloorOnOffControl: FloorID: " + floorId + " DeviceId NOT FOUND for WriteProperty to stack");
                return;
            }
            Molex.BACnet.Gateway.ServiceCore.Models.BACnet.BACnetObjectDetails bacnetObjInfo = floorModel.BACnetData.ObjectDetails[key];


            if (!StackManager.Instance.WriteProperty((uint)result.Data, StackDataObjects.Constants.BacnetObjectType.ObjectBinaryValue, bacnetObjInfo.ObjectID,
            value, StackDataObjects.Constants.BacnetPropertyID.PropPresentValue, StackDataObjects.Constants.BacnetDataType.BacnetDTReal))
            {
                Logger.Instance.Log(LogLevel.ERROR, "Property update: Property update for Analog Input - Failed");
                return;
            }

            bacnetObjInfo.PresentValue = value;
        }

        /// <summary>
        /// Updates the notification get from API for sensor enable/disable change.
        /// </summary>
        /// <param name="queueItem">The queue item.</param>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/2/20187:39 PM</TimeStamp>
        private void UpdateNotification(MolexAPIPublishModel queueItem)
        {
            try
            {
                if (AppCoreRepo.Instance.ZonesLookUp.ContainsKey(queueItem.id))
                {
                    ResultModel<MolexAPINotificationSubRespModel> apiNotificationResponseModel = MolexAPIConverterHelper.ParseNotificationJSONData(Convert.ToString(queueItem.data));
                    if (!apiNotificationResponseModel.IsSuccess)
                    {
                        Logger.Instance.Log(LogLevel.ERROR, "MonitoringDataManager.Process:UpdateNotification:fail to parse json data");
                        return;
                    }
                    NotificationDataModel notificationData = MolexAPIConverterHelper.ParseNotificationRespModel(apiNotificationResponseModel.Data);
                    AppConstants.NotificationType notificationType;

                    if (Enum.TryParse(notificationData.Type, true, out notificationType))
                    {
                        ZoneEntityModel zoneEntityModel = AppCoreRepo.Instance.ZonesLookUp[queueItem.id] as ZoneEntityModel;

                        switch (notificationType)
                        {
                            case AppConstants.NotificationType.sensorDisabled:
                                if (AppCoreRepo.Instance.BACnetConfigurationData.MapIndividualSensors)
                                {
                                    //Code to change reliability of disabled sensor to NoSensor
                                    List<SensorEntityModel> disabledSensorList = zoneEntityModel.Sensors.Where(x => x.Role == notificationData.AdditionalInfo).ToList();
                                    AppConstants.SensorType sensorType = MolexAPIConverterHelper.GetSensorType(notificationData.AdditionalInfo);
                                    BacnetObjectType bacnetObjectType = EntityHelper.GetBACnetObjectTypeBySensorType(sensorType);

                                    foreach (SensorEntityModel sensorEntityModel in disabledSensorList)
                                    {
                                        GetSetReliability(AppConstants.ReliabilityStatus.NoSensor.ToString(), sensorEntityModel, bacnetObjectType, DateTime.Now.ToString());
                                    }
                                    disabledSensorList.Clear();
                                    disabledSensorList = null;
                                    // On sensor disable map no senosr to sensor depedent objects at zone level
                                    SetSensorBasedObjectsReliability(zoneEntityModel, sensorType, bacnetObjectType, AppConstants.ReliabilityStatus.NoSensor.ToString(), DateTime.Now.ToString());
                                }
                                break;
                            default:
                                break;
                        }
                        zoneEntityModel = null;
                    }
                    apiNotificationResponseModel = null;
                    notificationData = null;
                }
            }
            catch (Exception e)
            {
                Logger.Instance.Log(e, LogLevel.ERROR, "MonitoringDataManager.Process: Notification monitoring failed for zone :" + queueItem.id);
            }
        }

        /// <summary>
        /// Sets the reliability for Occupancy timeout,fadeout and DHTragetLux on respective sensor disabled i.e PIR for occupancy
        /// TimeOut and FadeOut and Lux for DHTargetLux
        /// </summary>
        /// <param name="entityBaseModel">The entity base model.</param>
        /// <param name="sensorType">Type of the sensor.</param>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/20185:25 PM</TimeStamp>
        private void SetSensorBasedObjectsReliability(EntityBaseModel entityBaseModel, AppConstants.SensorType sensorType, BacnetObjectType objectType, string reliabilityStatus, string timestamp)
        {
            //To map average sensor reliability to no sensor on sensor disabled
            GetSetReliability(reliabilityStatus, entityBaseModel, objectType, timestamp, sensorType.ToString());
            switch ((entityBaseModel as ZoneEntityModel).ZoneType)
            {
                case AppConstants.ZoneTypes.UserSpace:
                case AppConstants.ZoneTypes.GeneralSpace:
                    switch (sensorType)
                    {
                        case AppConstants.SensorType.Presence:
                            //Code for on Presence sensor disabled zone level occupancy timeout and fadeout objects reliability map as NoSensor
                            GetSetReliability(reliabilityStatus, entityBaseModel, BacnetObjectType.ObjectPositiveIntegerValue, timestamp, AppConstants.PositiveIntegerValueType.OccupancyFadeOutTime.ToString());
                            GetSetReliability(reliabilityStatus, entityBaseModel, BacnetObjectType.ObjectPositiveIntegerValue, timestamp, AppConstants.PositiveIntegerValueType.OccupancyTimeOut.ToString());
                            break;
                        case AppConstants.SensorType.LuxLevel:
                            //Code for on Lux sensor disabled zone level DHtargetLux object reliability map as NoSensor
                            GetSetReliability(reliabilityStatus, entityBaseModel, BacnetObjectType.ObjectPositiveIntegerValue, timestamp, AppConstants.PositiveIntegerValueType.DHTargetLux.ToString());
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Updates the reliability.
        /// </summary>
        /// <param name="queueItem">The queue item.</param>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>4/12/201810:17 AM</TimeStamp>
        private void UpdateReliability(MolexAPIPublishModel queueItem)
        {
            try
            {
                if (AppCoreRepo.Instance.AllEntityLookUp.ContainsKey(queueItem.id))
                {
                    EntityBaseModel entityBaseModel = AppCoreRepo.Instance.AllEntityLookUp[queueItem.id];
                    ResultModel<MolexAPIReliabilitySubRespModel> molexAPIReliabilityData = MolexAPIConverterHelper.ParseReliabilityData(Convert.ToString(queueItem.data));

                    #region MyRegion
                    if (!molexAPIReliabilityData.IsSuccess)
                    {
                        return;
                    }

                    ReliabilityDataModel reliabilityDataModel = MolexAPIConverterHelper.ParseReliabilitySubRespModel(molexAPIReliabilityData.Data);

                    switch (entityBaseModel.EntityType)
                    {
                        case AppConstants.EntityTypes.Building:
                            GetSetReliability(reliabilityDataModel.Resources.FixtureModel.Value, entityBaseModel, BacnetObjectType.ObjectBinaryInput, queueItem.timestamp, AppConstants.BUILDING_OCCUPANCY_OBJECT);
                            GetSetReliabilityForBlind(reliabilityDataModel.Resources.BlindModel.Value, entityBaseModel, BacnetObjectType.ObjectAnalogValue, queueItem.timestamp);
                            GetSetReliability(reliabilityDataModel.Resources.BeaconModel.Value, entityBaseModel, BacnetObjectType.ObjectAnalogValue, queueItem.timestamp, entityBaseModel.EntityType + " " + AppResources.AppResource.BeaconLightLevel);
                            GetSetReliability(reliabilityDataModel.Resources.FixtureModel.Value, entityBaseModel, BacnetObjectType.ObjectAnalogValue, queueItem.timestamp, entityBaseModel.EntityType + " " + AppResources.AppResource.UserLightLevel);
                            GetSetReliability(reliabilityDataModel.Resources.FixtureModel.Value, entityBaseModel, BacnetObjectType.ObjectAnalogValue, queueItem.timestamp, entityBaseModel.EntityType + " " + AppResources.AppResource.GeneralLightLevel);
                            GetSetReliability(reliabilityDataModel.Resources.FixtureModel.Value, entityBaseModel, BacnetObjectType.ObjectAnalogValue, queueItem.timestamp, AppResources.AppResource.BioDynamicControlName);
                            GetSetReliability(reliabilityDataModel.Resources.FixtureModel.Value, entityBaseModel, BacnetObjectType.ObjectAnalogValue, queueItem.timestamp, AppResources.AppResource.SaturationControlName);
                            GetSetReliability(reliabilityDataModel.Resources.FixtureModel.Value, entityBaseModel, BacnetObjectType.ObjectMultiStateValue, queueItem.timestamp);
                            break;
                        case AppConstants.EntityTypes.Floor:
                            GetSetReliability(reliabilityDataModel.Resources.FixtureModel.Value, entityBaseModel, BacnetObjectType.ObjectBinaryInput, queueItem.timestamp, AppConstants.FLOOR_OCCUPANCY_OBJECT);
                            GetSetReliabilityForBlind(reliabilityDataModel.Resources.BlindModel.Value, entityBaseModel, BacnetObjectType.ObjectAnalogValue, queueItem.timestamp);
                            GetSetReliability(reliabilityDataModel.Resources.BeaconModel.Value, entityBaseModel, BacnetObjectType.ObjectAnalogValue, queueItem.timestamp, entityBaseModel.EntityType + " " + AppResources.AppResource.BeaconLightLevel);
                            GetSetReliability(reliabilityDataModel.Resources.FixtureModel.Value, entityBaseModel, BacnetObjectType.ObjectAnalogValue, queueItem.timestamp, entityBaseModel.EntityType + " " + AppResources.AppResource.UserLightLevel);
                            GetSetReliability(reliabilityDataModel.Resources.FixtureModel.Value, entityBaseModel, BacnetObjectType.ObjectAnalogValue, queueItem.timestamp, entityBaseModel.EntityType + " " + AppResources.AppResource.GeneralLightLevel);
                            GetSetReliability(reliabilityDataModel.Resources.FixtureModel.Value, entityBaseModel, BacnetObjectType.ObjectAnalogValue, queueItem.timestamp, AppResources.AppResource.BioDynamicControlName);
                            GetSetReliability(reliabilityDataModel.Resources.FixtureModel.Value, entityBaseModel, BacnetObjectType.ObjectAnalogValue, queueItem.timestamp, AppResources.AppResource.SaturationControlName);
                            GetSetReliability(reliabilityDataModel.Resources.FixtureModel.Value, entityBaseModel, BacnetObjectType.ObjectMultiStateValue, queueItem.timestamp);


                            GetSetReliabilityForFloorSensor(reliabilityDataModel.Resources.FixtureModel.Value, entityBaseModel, BacnetObjectType.ObjectBinaryValue, queueItem.timestamp, "ObjectBinaryValue_FloorState");
                            //GetSetReliability(reliabilityDataModel.Resources.FixtureModel.Value, entityBaseModel, BacnetObjectType.ObjectBinaryValue, queueItem.timestamp, entityBaseModel.EntityName + " " + "ON/OFF");
                            //To update zone level sensor avg objects reliability      
                            Dictionary<string, string> avgBeaconSensorReliability = JsonConvert.DeserializeObject<Dictionary<string, string>>(reliabilityDataModel.Resources.SensorModel.SensorType.ToString());
                            foreach (string sensorRole in avgBeaconSensorReliability.Keys)
                            {
                                AppConstants.SensorType sensorType = MolexAPIConverterHelper.GetSensorType(sensorRole);
                                BacnetObjectType bacnetObjectType = EntityHelper.GetBACnetObjectTypeBySensorType(sensorType);
                                //To update zone level average sensor and PIV objects reliability
                                string key = bacnetObjectType.ToString() + "_" + entityBaseModel.EntityName + "-" + sensorType.ToString();
                                GetSetReliabilityForFloorSensor(avgBeaconSensorReliability[sensorRole], entityBaseModel, bacnetObjectType, queueItem.timestamp, key);
                                //SetSensorBasedObjectsReliability(entityBaseModel, sensorType, bacnetObjectType, avgBeaconSensorReliability[sensorRole], queueItem.timestamp);
                            }
                            avgBeaconSensorReliability.Clear();
                            avgBeaconSensorReliability = null;
                            break;
                        case AppConstants.EntityTypes.Zone:
                            if (reliabilityDataModel.Value == AppConstants.ReliabilityStatus.NA.ToString())
                            {
                                MonitoredDataStackUpdateHelper.UpdateSystemStatus(entityBaseModel, BacnetDeviceStatus.StatusNonOperational);
                                //To set virtual router device system status property value based on all VD system status i.e if all non-operational then its non-operational else operational
                                SetVRDSystemStatus();
                            }
                            else
                            {
                                MonitoredDataStackUpdateHelper.UpdateSystemStatus(entityBaseModel, BacnetDeviceStatus.StatusOperational);
                                //To set virtual router device system status property value based on all VD system status i.e if all non-operational then its non-operational else operational
                                SetVRDSystemStatus();
                            }


                            ZoneEntityModel zoneEntityModel = entityBaseModel as ZoneEntityModel;

                            switch (zoneEntityModel.ZoneType)
                            {
                                case AppConstants.ZoneTypes.BeaconSpace:
                                    {
                                        //To set zone level fixture based object reliability
                                        //SCScheduleManager.GetInstance().SetScheduleReliability(reliabilityDataModel.Resources.BeaconModel.Value, entityBaseModel);
                                        GetSetReliability(reliabilityDataModel.Resources.BeaconModel.Value, entityBaseModel, BacnetObjectType.ObjectPositiveIntegerValue, queueItem.timestamp, AppResource.BeaconColor + " 1");
                                        GetSetReliability(reliabilityDataModel.Resources.BeaconModel.Value, entityBaseModel, BacnetObjectType.ObjectPositiveIntegerValue, queueItem.timestamp, AppResource.BeaconColor + " 2");
                                        GetSetReliability(reliabilityDataModel.Resources.BeaconModel.Value, entityBaseModel, BacnetObjectType.ObjectPositiveIntegerValue, queueItem.timestamp, AppResource.BeaconColor + " 3");
                                        GetSetReliability(reliabilityDataModel.Resources.BeaconModel.Value, entityBaseModel, BacnetObjectType.ObjectPositiveIntegerValue, queueItem.timestamp, AppResource.BeaconColor + " 4");
                                        GetSetReliability(reliabilityDataModel.Resources.BeaconModel.Value, entityBaseModel, BacnetObjectType.ObjectPositiveIntegerValue, queueItem.timestamp, AppResource.BeaconColor + " 5");
                                        GetSetReliability(reliabilityDataModel.Resources.BeaconModel.Value, entityBaseModel, BacnetObjectType.ObjectMultiStateValue, queueItem.timestamp, "BeaconPalettes");
                                        GetSetReliability(reliabilityDataModel.Resources.BeaconModel.Value, entityBaseModel, BacnetObjectType.ObjectBinaryInput, queueItem.timestamp, AppConstants.ZONE_OCCUPANCY_OBJECT);
                                        GetSetReliability(reliabilityDataModel.Resources.BeaconModel.Value, entityBaseModel, BacnetObjectType.ObjectMultiStateValue, queueItem.timestamp);
                                        GetSetReliability(reliabilityDataModel.Resources.BeaconModel.Value, entityBaseModel, BacnetObjectType.ObjectAnalogValue, queueItem.timestamp);
                                        GetSetReliability(reliabilityDataModel.Resources.BeaconModel.Value, entityBaseModel, BacnetObjectType.ObjectBinaryValue, queueItem.timestamp);

                                        GetSetReliability(reliabilityDataModel.Resources.BeaconModel.Value, entityBaseModel, BacnetObjectType.ObjectPositiveIntegerValue, queueItem.timestamp, AppConstants.PositiveIntegerValueType.RGB.ToString());

                                        //To update zone level sensor avg objects reliability      
                                        Dictionary<string, string> avgBeaconSensorReliability2 = JsonConvert.DeserializeObject<Dictionary<string, string>>(reliabilityDataModel.Resources.SensorModel.SensorType.ToString());
                                        foreach (string sensorRole in avgBeaconSensorReliability2.Keys)
                                        {
                                            AppConstants.SensorType sensorType = MolexAPIConverterHelper.GetSensorType(sensorRole);
                                            BacnetObjectType bacnetObjectType = EntityHelper.GetBACnetObjectTypeBySensorType(sensorType);
                                            //To update zone level average sensor and PIV objects reliability
                                            SetSensorBasedObjectsReliability(entityBaseModel, sensorType, bacnetObjectType, avgBeaconSensorReliability2[sensorRole], queueItem.timestamp);
                                        }
                                        avgBeaconSensorReliability2.Clear();
                                        avgBeaconSensorReliability2 = null;
                                    }
                                    break;
                                case AppConstants.ZoneTypes.UserSpace:
                                case AppConstants.ZoneTypes.GeneralSpace:
                                    //To set zone level fixture based object reliability
                                    //SCScheduleManager.GetInstance().SetScheduleReliability(reliabilityDataModel.Resources.FixtureModel.Value, entityBaseModel);
                                    GetSetReliability(reliabilityDataModel.Resources.FixtureModel.Value, entityBaseModel, BacnetObjectType.ObjectBinaryInput, queueItem.timestamp, AppConstants.ZONE_OCCUPANCY_OBJECT);
                                    GetSetReliability(reliabilityDataModel.Resources.FixtureModel.Value, entityBaseModel, BacnetObjectType.ObjectMultiStateValue, queueItem.timestamp);
                                    GetSetReliability(reliabilityDataModel.Resources.FixtureModel.Value, entityBaseModel, BacnetObjectType.ObjectMultiStateValue, queueItem.timestamp, AppConstants.LIGHTING_MOOD_OBJECT);
                                    GetSetReliability(reliabilityDataModel.Resources.FixtureModel.Value, entityBaseModel, BacnetObjectType.ObjectMultiStateValue, queueItem.timestamp, AppConstants.RESET_ATTRIBUTES_AFTER_TIMEOUT);
                                    GetSetReliability(reliabilityDataModel.Resources.FixtureModel.Value, entityBaseModel, BacnetObjectType.ObjectAnalogValue, queueItem.timestamp);
                                    GetSetReliability(reliabilityDataModel.Resources.FixtureModel.Value, entityBaseModel, BacnetObjectType.ObjectAnalogValue, queueItem.timestamp, AppResources.AppResource.BioDynamicControlName);
                                    GetSetReliability(reliabilityDataModel.Resources.FixtureModel.Value, entityBaseModel, BacnetObjectType.ObjectAnalogValue, queueItem.timestamp, AppResources.AppResource.SaturationControlName);
                                    GetSetReliability(reliabilityDataModel.Resources.FixtureModel.Value, entityBaseModel, BacnetObjectType.ObjectBinaryValue, queueItem.timestamp);

                                    //To update zone level sensor avg objects reliability      
                                    Dictionary<string, string> avgUserSensorReliability = JsonConvert.DeserializeObject<Dictionary<string, string>>(reliabilityDataModel.Resources.SensorModel.SensorType.ToString());
                                    foreach (string sensorRole in avgUserSensorReliability.Keys)
                                    {
                                        AppConstants.SensorType sensorType = MolexAPIConverterHelper.GetSensorType(sensorRole);
                                        BacnetObjectType bacnetObjectType = EntityHelper.GetBACnetObjectTypeBySensorType(sensorType);
                                        //To update zone level average sensor and PIV objects reliability
                                        SetSensorBasedObjectsReliability(entityBaseModel, sensorType, bacnetObjectType, avgUserSensorReliability[sensorRole], queueItem.timestamp);
                                    }
                                    avgUserSensorReliability.Clear();
                                    avgUserSensorReliability = null;
                                    break;
                                case AppConstants.ZoneTypes.BlindSpace:
                                    //SCScheduleManager.GetInstance().SetScheduleReliability(reliabilityDataModel.Resources.BlindModel.Value, entityBaseModel);
                                    GetSetReliabilityForBlind(reliabilityDataModel.Resources.BlindModel.Value, zoneEntityModel, BacnetObjectType.ObjectAnalogValue, queueItem.timestamp);
                                    break;
                            }
                            break;
                        case AppConstants.EntityTypes.Fixture:
                            switch (((ZoneEntityModel)entityBaseModel.Parent).ZoneType)
                            {
                                case AppConstants.ZoneTypes.UserSpace:
                                case AppConstants.ZoneTypes.GeneralSpace:
                                    if (AppCoreRepo.Instance.BACnetConfigurationData.MapIndividualFixture)
                                    {
                                        GetSetReliability(reliabilityDataModel.Value, entityBaseModel, BacnetObjectType.ObjectAnalogValue, queueItem.timestamp);
                                    }
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case AppConstants.EntityTypes.Sensor:
                            if (AppCoreRepo.Instance.BACnetConfigurationData.MapIndividualSensors)
                            {
                                BacnetObjectType bacnetObjectType = EntityHelper.GetBACnetObjectTypeBySensorType(((SensorEntityModel)entityBaseModel).SensorType);
                                GetSetReliability(reliabilityDataModel.Value, entityBaseModel, bacnetObjectType, queueItem.timestamp);
                            }
                            break;
                    } 
                    #endregion

                    reliabilityDataModel = null;
                    molexAPIReliabilityData = null;
                }
            }
            catch (Exception e)
            {
                Logger.Instance.Log(e, LogLevel.ERROR, "MonitoringDataManager.Process: Update reliability failed for entitykey: " + queueItem.id);
            }
        }

        /// <summary>
        /// Gets the set reliability.
        /// </summary>
        /// <param name="apiReliabilityStatus">The API reliability status.</param>
        /// <param name="entityBaseModel">The entity base model.</param>
        /// <param name="bacnetObjectType">Type of the bacnet object.</param>
        /// <param name="type">The type.</param>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/201810:16 AM</TimeStamp>
        private void GetSetReliability(string apiReliabilityStatus, EntityBaseModel entityBaseModel, BacnetObjectType bacnetObjectType, string timestamp, string type = "")
        {
            ResultModel<int> deviceId = EntityHelper.GetDeviceId(entityBaseModel);
            BacnetReliability bacnetReliability;
            AppConstants.ReliabilityStatus reliabilityStatus;

            if (Enum.TryParse(apiReliabilityStatus, true, out reliabilityStatus))
            {
                switch (reliabilityStatus)
                {
                    case AppConstants.ReliabilityStatus.Good:
                        bacnetReliability = BacnetReliability.ReliabilityNoFaultDetected;
                        break;
                    case AppConstants.ReliabilityStatus.Warning:
                    case AppConstants.ReliabilityStatus.Critical:
                        bacnetReliability = BacnetReliability.ReliabilityUnreliableOther;
                        break;
                    case AppConstants.ReliabilityStatus.NoSensor:
                    case AppConstants.ReliabilityStatus.NA:
                        bacnetReliability = BacnetReliability.ReliabilityNoSensor;
                        break;
                    default:
                        return;
                }
                string objectKey = GetObjectKeyByBACnetObjectType(bacnetObjectType, entityBaseModel.EntityType, type);
                MonitoredDataStackUpdateHelper.UpdateReliability(bacnetReliability, entityBaseModel, objectKey, timestamp);
            }
        }

        /// <summary>
        /// Gets the set reliability.
        /// </summary>
        /// <param name="apiReliabilityStatus">The API reliability status.</param>
        /// <param name="entityBaseModel">The entity base model.</param>
        /// <param name="bacnetObjectType">Type of the bacnet object.</param>
        /// <param name="type">The type.</param>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/201810:16 AM</TimeStamp>
        private void GetSetReliabilityForFloorSensor(string apiReliabilityStatus, EntityBaseModel entityBaseModel, BacnetObjectType bacnetObjectType, string timestamp, string type = "")
        {
            ResultModel<int> deviceId = EntityHelper.GetDeviceId(entityBaseModel);
            BacnetReliability bacnetReliability;
            AppConstants.ReliabilityStatus reliabilityStatus;

            if (Enum.TryParse(apiReliabilityStatus, true, out reliabilityStatus))
            {
                switch (reliabilityStatus)
                {
                    case AppConstants.ReliabilityStatus.Good:
                        bacnetReliability = BacnetReliability.ReliabilityNoFaultDetected;
                        break;
                    case AppConstants.ReliabilityStatus.Warning:
                    case AppConstants.ReliabilityStatus.Critical:
                        bacnetReliability = BacnetReliability.ReliabilityUnreliableOther;
                        break;
                    case AppConstants.ReliabilityStatus.NoSensor:
                        bacnetReliability = BacnetReliability.ReliabilityNoSensor;
                        break;
                    default:
                        return;
                }
                //string objectKey = GetObjectKeyByBACnetObjectType(bacnetObjectType, entityBaseModel.EntityType, type);
                MonitoredDataStackUpdateHelper.UpdateReliability(bacnetReliability, entityBaseModel, type, timestamp);
            }
        }

        /// <summary>
        /// Gets the set reliability for Blind Zone
        /// </summary>
        /// <param name="apiReliabilityStatus">The API reliability status.</param>
        /// <param name="entityBaseModel">The entity base model.</param>
        /// <param name="bacnetObjectType">Type of the bacnet object.</param>
        /// <param name="type">The type.</param>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/201810:16 AM</TimeStamp>
        private void GetSetReliabilityForBlind(string apiReliabilityStatus, EntityBaseModel entityBaseModel, BacnetObjectType bacnetObjectType, string timestamp)
        {
            ResultModel<int> deviceId = EntityHelper.GetDeviceId(entityBaseModel);
            BacnetReliability bacnetReliability;
            AppConstants.ReliabilityStatus reliabilityStatus;

            if (Enum.TryParse(apiReliabilityStatus, true, out reliabilityStatus))
            {
                switch (reliabilityStatus)
                {
                    case AppConstants.ReliabilityStatus.Good:
                        bacnetReliability = BacnetReliability.ReliabilityNoFaultDetected;
                        break;
                    case AppConstants.ReliabilityStatus.Warning:
                    case AppConstants.ReliabilityStatus.Critical:
                        bacnetReliability = BacnetReliability.ReliabilityUnreliableOther;
                        break;
                    case AppConstants.ReliabilityStatus.NoSensor:
                        bacnetReliability = BacnetReliability.ReliabilityNoSensor;
                        break;
                    default:
                        return;
                }
                if (entityBaseModel.EntityType == AppConstants.EntityTypes.Zone)
                {
                    foreach (var item in entityBaseModel.BACnetData.ObjectDetails)
                    {
                        if (item.Value.ObjectType == BacnetObjectType.ObjectAnalogValue)
                            MonitoredDataStackUpdateHelper.UpdateReliability(bacnetReliability, entityBaseModel, item.Key, timestamp);
                    }
                }
                else
                {
                    foreach (var item in entityBaseModel.BACnetData.ObjectDetails)
                    {
                        if (item.Value.ObjectType == BacnetObjectType.ObjectAnalogValue && item.Value.ObjectName.Contains(AppResources.AppResource.AnalogValueBlindLocationObjectSubName))
                            MonitoredDataStackUpdateHelper.UpdateReliability(bacnetReliability, entityBaseModel, item.Key, timestamp);
                    }
                }
            }
        }

        /// <summary>
        /// Gets the type of the object key by ba cnet objecct.
        /// </summary>
        /// <param name="bacnetObjectType">Type of the bacnet object.</param>
        /// <param name="EntityType">Type of the entity.</param>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/20182:43 PM</TimeStamp>
        private string GetObjectKeyByBACnetObjectType(BacnetObjectType bacnetObjectType, AppConstants.EntityTypes EntityType, string type)
        {
            string key = string.Empty;
            switch (bacnetObjectType)
            {
                case BacnetObjectType.ObjectAnalogValue:
                    if (string.IsNullOrEmpty(type))
                        key = EntityHelper.GetObjectDetailsKey(bacnetObjectType, AppConstants.LIGHTING_BRIGHTENESS_OBJECT);
                    else
                        key = EntityHelper.GetObjectDetailsKey(bacnetObjectType, type);
                    break;
                case BacnetObjectType.ObjectMultiStateValue:
                    if (string.IsNullOrEmpty(type))
                        key = EntityHelper.GetObjectDetailsKey(bacnetObjectType, AppConstants.LIGHTING_LIGHTSCENE_OBJECT);
                    else
                        key = EntityHelper.GetObjectDetailsKey(bacnetObjectType, type);
                    break;
                case BacnetObjectType.ObjectPositiveIntegerValue:
                    //type is the PIV object name i.e "DHTragetLux,OccupancyTimeOut,OccupancyFadeOut"
                    key = EntityHelper.GetObjectDetailsKey(bacnetObjectType, type);
                    break;
                case BacnetObjectType.ObjectBinaryValue:
                    key = EntityHelper.GetObjectDetailsKey(bacnetObjectType, AppConstants.ZONE_ONOFF_OBJECT);

                    break;
                case BacnetObjectType.ObjectBinaryInput:
                case BacnetObjectType.ObjectAnalogInput:
                    switch (EntityType)
                    {
                        case AppConstants.EntityTypes.Zone:
                        case AppConstants.EntityTypes.Floor:
                        case AppConstants.EntityTypes.Building:
                            //type is the sensor type i.e "Lux,AirQuality,Occupancy etc."
                            key = EntityHelper.GetObjectDetailsKey(bacnetObjectType, type);
                            break;
                        case AppConstants.EntityTypes.Sensor:
                            key = EntityHelper.GetObjectDetailsKey(bacnetObjectType, AppConstants.LIGHTING_SENSOR_OBJECT);
                            break;
                    }
                    break;
                default:
                    Logger.Instance.Log(LogLevel.ERROR, "MonitoringDataManager:GetObjectKeyByBACnetObjecctType:Key not fount for bacnet object type: " + bacnetObjectType);
                    break;
            }

            return key;

        }

        /// <summary>
        /// Sets the VRD system status.
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>4/11/20187:53 PM</TimeStamp>
        private void SetVRDSystemStatus()
        {
            string bacnetObjectKey = CommonHelper.GenerateBACnetObjectMappedID(AppCoreRepo.Instance.BACnetConfigurationData.ControllerId, BacnetObjectType.ObjectDevice, AppCoreRepo.Instance.BACnetConfigurationData.ControllerId);
            EntityBaseModel entityBaseModel = AppCoreRepo.Instance.BACnetObjectEntityLookUp[bacnetObjectKey];

            if (AppCoreRepo.Instance.BACnetObjectEntityLookUp.Where(x => x.Value.EntityType == AppConstants.EntityTypes.Zone && x.Value.BACnetData.SystemStatus == BacnetDeviceStatus.StatusOperational).Count() == 0)//Updating System Status to Non-Operational for controller
            {
                MonitoredDataStackUpdateHelper.UpdateSystemStatus(entityBaseModel, BacnetDeviceStatus.StatusNonOperational);
            }
            else
            {
                MonitoredDataStackUpdateHelper.UpdateSystemStatus(entityBaseModel, BacnetDeviceStatus.StatusOperational);
            }
        }
        #endregion
    }

    #region BrightnessDataObject Model Class
    public class BrightnessDataObject
    {
        public string brightness { get; set; }
        public string appliedBrightness { get; set; }
    } 
    #endregion
}