﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              CreateObjectCallBackManager
///   Description:        Handles call backs from stack to create supported objects
///   Author:             Rohit Galgali                  
///   Date:               MM/DD/YY
#endregion

using Molex.BACnet.Gateway.Log;
using Molex.BACnet.Gateway.ServiceCore.Constants;
using Molex.BACnet.Gateway.ServiceCore.Helpers;
using Molex.BACnet.Gateway.ServiceCore.Models.Entity;
using Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel;
using Molex.BACnet.Gateway.ServiceCore.Repository;
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Models;
using Molex.StackDataObjects.Response;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Molex.BACnet.Gateway.ServiceCore.Managers
{
    /// <summary>
    /// Manages all the call backs from stack to create supported objects
    /// </summary>
    /// <CreatedBy>Rohit Galgali</CreatedBy>
    /// <TimeStamp>8/3/20172:07 PM</TimeStamp>
    internal class CreateObjectCallBackManager
    {
        /// <summary>
        /// Creates the feature bacnet object.
        /// </summary>
        /// <param name="createObjectNotificationResponse">The create object notification response.</param>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>8/3/20173:37 PM</TimeStamp>
        /// <exception cref="System.NotImplementedException"></exception>
        internal void CreateFeatureObjects(StackDataObjects.Response.AutoResponses.CreateObjectNotification createObjectNotificationResponse)
        {
            EntityBaseModel entityBaseModel = EntityHelper.GetEntityModelFromDeviceID(createObjectNotificationResponse.CreateObjectResponse.DeviceId);

            uint objectId = createObjectNotificationResponse.CreateObjectResponse.ObjectId;

            if (entityBaseModel == null)
            {
                return;
            }

            PersistedModels model;
            if (AppCoreRepo.Instance.PersistentModelCache.ContainsKey(entityBaseModel.MappingKey))
            {
                model = AppCoreRepo.Instance.PersistentModelCache[entityBaseModel.MappingKey];
            }
            else
            {
                model = new PersistedModels();
                model.DeviceID = (uint)entityBaseModel.BACnetData.DeviceID;
            }

            if (createObjectNotificationResponse.CreateObjectResponse.IsObjTypeSpecifier)
            {
                objectId = Convert.ToUInt32(ObjectIdentifierManager.Instance.GetObjectId(createObjectNotificationResponse.CreateObjectResponse.DeviceId, createObjectNotificationResponse.CreateObjectResponse.ObjectType, entityBaseModel.EntityType));
            }

            string ObjectName = string.Empty;
            switch (createObjectNotificationResponse.CreateObjectResponse.ObjectType)
            {
                case BacnetObjectType.ObjectCalendar:
                    ObjectName = GetObjectName(entityBaseModel,createObjectNotificationResponse, model.Calendars.ConvertAll(s => (PersistentBase)s));
                    break;
                case BacnetObjectType.ObjectEventEnrollment:
                    ObjectName = GetObjectName(entityBaseModel,createObjectNotificationResponse, model.EventEnrollments.ConvertAll(s => (PersistentBase)s));
                    break;
                case BacnetObjectType.ObjectNotificationClass:
                    ObjectName = GetObjectName(entityBaseModel,createObjectNotificationResponse, model.NotificationClasses.ConvertAll(s => (PersistentBase)s));
                    break;
                case BacnetObjectType.ObjectSchedule:
                    ObjectName = GetObjectName(entityBaseModel,createObjectNotificationResponse, model.Schedules.ConvertAll(s => (PersistentBase)s));
                    break;
                case BacnetObjectType.ObjectTrendlog:
                    ObjectName = GetObjectName(entityBaseModel,createObjectNotificationResponse, model.TrendLogs.ConvertAll(s => (PersistentBase)s));
                    break;
                default:
                    break;
            }
            if (string.IsNullOrEmpty(ObjectName))
            {
                Logger.Instance.Log(LogLevel.ERROR, "ObjectName = Empty Recevied from CreateObjectCallBackManager.GetObjectName() Setting Default");
                ObjectName = CommonHelper.GetObjectName(createObjectNotificationResponse.CreateObjectResponse.ObjectType);
            }

            //This condition will check for max configured value.
            if (!UpdatePersistentModel(createObjectNotificationResponse, model, objectId, ObjectName))
            {
                Logger.Instance.Log(LogLevel.DEBUG, "Cannot create more than max configured objects into a device.");
                createObjectNotificationResponse.ErrorModel = new ErrorResponseModel();
                createObjectNotificationResponse.ErrorModel.Type = ErrorType.StackError;
                createObjectNotificationResponse.IsSucceed = false;
                createObjectNotificationResponse.ErrorModel.ErrorCode = (int)StackDataObjects.Constants.BacnetErrorCode.ErrorCodeNoSpaceForObject;
                createObjectNotificationResponse.ErrorModel.ErrorClass = (int)StackDataObjects.Constants.BacnetErrorClass.ErrorClassResources;
                return;
            }

            if (AppCoreRepo.Instance.PersistentModelCache.ContainsKey(entityBaseModel.MappingKey))
            {
                AppCoreRepo.Instance.PersistentModelCache[entityBaseModel.MappingKey] = model;
            }
            else
            {
                AppCoreRepo.Instance.PersistentModelCache.Add(entityBaseModel.MappingKey, model);
            }

            bool isSuccess = StackManager.Instance.CreateObject((uint)createObjectNotificationResponse.CreateObjectResponse.DeviceId, createObjectNotificationResponse.CreateObjectResponse.ObjectType, objectId, ObjectName);

            if (!isSuccess)
            {
                Logger.Instance.Log(LogLevel.ERROR, "CreateObjectCallBackManager.CreateFeatureObjects, Failed to create object for object type:" + createObjectNotificationResponse.CreateObjectResponse.ObjectType.ToString());
                createObjectNotificationResponse.ErrorModel = new ErrorResponseModel();
                createObjectNotificationResponse.ErrorModel.Type = ErrorType.StackError;
                createObjectNotificationResponse.IsSucceed = false;
                createObjectNotificationResponse.ErrorModel.ErrorCode = (int)StackDataObjects.Constants.BacnetErrorCode.ErrorCodeOther;
                createObjectNotificationResponse.ErrorModel.ErrorClass = (int)StackDataObjects.Constants.BacnetErrorClass.ErrorClassObject;
                return;
            }

            bool isAddObjectIdSuccess = ObjectIdentifierManager.Instance.AddObjectId(createObjectNotificationResponse.CreateObjectResponse.DeviceId, createObjectNotificationResponse.CreateObjectResponse.ObjectType, (int)objectId, entityBaseModel.EntityType);

            if (!isAddObjectIdSuccess)
            {
                Logger.Instance.Log(LogLevel.ERROR, "fail to add created dynamic object id in to object identifier with id: " + objectId + ",entity type: " + entityBaseModel.EntityType + ",object type: " + createObjectNotificationResponse.CreateObjectResponse.ObjectType + ",device id: " + createObjectNotificationResponse.CreateObjectResponse.DeviceId);
                return;
            }

            createObjectNotificationResponse.IsSucceed = true;
            createObjectNotificationResponse.CreateObjectResponse.ObjectId = objectId;
            StackManager.Instance.WriteProperty((uint)createObjectNotificationResponse.CreateObjectResponse.DeviceId,
                                                 createObjectNotificationResponse.CreateObjectResponse.ObjectType,
                                                 objectId,
                                                 CommonHelper.GetDescription(createObjectNotificationResponse.CreateObjectResponse.ObjectType),
                                                 BacnetPropertyID.PropDescription);

            UpdatePropertyValue(createObjectNotificationResponse, entityBaseModel, objectId);
            if (createObjectNotificationResponse.CreateObjectResponse.ObjectType == BacnetObjectType.ObjectNotificationClass)
            {
                StackManager.SetPropertyAccessType((uint)createObjectNotificationResponse.CreateObjectResponse.DeviceId, (uint)objectId, BacnetObjectType.ObjectNotificationClass, BacnetPropertyID.PropPriority, PropAccessType.READ_WRITE, true);
                uint[] priorityListArray = null;
                if (entityBaseModel.EntityType == AppConstants.EntityTypes.Zone)
                {
                    priorityListArray = new uint[3];
                    priorityListArray[2] = (uint)AppCoreRepo.Instance.BACnetConfigurationData.ZoneNotificationPriority.ToNormal;
                    priorityListArray[1] = (uint)AppCoreRepo.Instance.BACnetConfigurationData.ZoneNotificationPriority.ToFault;
                    priorityListArray[0] = (uint)AppCoreRepo.Instance.BACnetConfigurationData.ZoneNotificationPriority.ToOffNormal;
                }
                else
                {
                    priorityListArray = new uint[3];
                    priorityListArray[2] = (uint)AppCoreRepo.Instance.BACnetConfigurationData.BuildingNotificationPriority.ToNormal;
                    priorityListArray[1] = (uint)AppCoreRepo.Instance.BACnetConfigurationData.BuildingNotificationPriority.ToFault;
                    priorityListArray[0] = (uint)AppCoreRepo.Instance.BACnetConfigurationData.BuildingNotificationPriority.ToOffNormal;
                }
                StackManager.Instance.WriteProperty((uint)createObjectNotificationResponse.CreateObjectResponse.DeviceId, createObjectNotificationResponse.CreateObjectResponse.ObjectType, objectId, priorityListArray, BacnetPropertyID.PropPriority);
            }
            PersistenceStorageManager.Instance.Store(EntityHelper.GetControllerEntityModel(entityBaseModel), AppCoreRepo.Instance.PersistentModelCache);
        }

        private string GetObjectName(EntityBaseModel entityBaseModel, StackDataObjects.Response.AutoResponses.CreateObjectNotification createObjectNotificationResponse, List<PersistentBase> objectList)
        {
            string result = string.Empty;
            try
            {
                int MaxIDvalue = 0;
                if (createObjectNotificationResponse == null)
                    return string.Empty;
                if (createObjectNotificationResponse.CreateObjectResponse == null)
                    return string.Empty;
                if (objectList != null && objectList.Count != 0)
                {
                    int MaxID = 0;
                    foreach (var item in objectList)
                    {
                        if (item == null)
                            continue;
                        if ((item as PersistentBase) == null)
                            continue;
                        int value = 0;
                        string strMaxID = (item as PersistentBase).Name.Replace(CommonHelper.GetObjectName(createObjectNotificationResponse.CreateObjectResponse.ObjectType).ToString(), "").Trim();
                        int.TryParse(strMaxID, out value);
                        if (value > MaxID)
                            MaxID = value;
                    }
                    MaxIDvalue = MaxID;
                }
                else
                {
                    if (entityBaseModel == null)
                        return string.Empty;
                    if (entityBaseModel.BACnetData == null)
                        return string.Empty;
                    if (entityBaseModel.BACnetData.ObjectDetails == null)
                        return string.Empty;
                    int countValue = 0;
                    foreach (var item in entityBaseModel.BACnetData.ObjectDetails)
                    {
                        if (item.Value == null)
                            continue;
                        if (item.Value.ObjectType == createObjectNotificationResponse.CreateObjectResponse.ObjectType)
                            countValue++;
                    }
                    MaxIDvalue = countValue;
                }
                string ObjectName = CommonHelper.GetObjectName(createObjectNotificationResponse.CreateObjectResponse.ObjectType) + " " + (MaxIDvalue + 1).ToString();
                if (objectList != null && objectList.Count != 0)//check if it's <NULL> or not
                {
                    if (!objectList.Any(s => (s as PersistentBase) != null && (s as PersistentBase).Name == ObjectName))
                        result = ObjectName;
                    else
                    {
                        int value = 0;
                        string strMaxID = ObjectName.Replace(CommonHelper.GetObjectName(createObjectNotificationResponse.CreateObjectResponse.ObjectType).ToString(), "").Trim();
                        int.TryParse(strMaxID, out value);
                        result = CommonHelper.GetObjectName(createObjectNotificationResponse.CreateObjectResponse.ObjectType) + " " + (value + 1).ToString();
                    }
                }
                else
                    result = ObjectName;
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "Error Received at CreateObjectCallBackManager.GetObjectName()");
            }
            return result;
        }

        private void UpdatePropertyValue(StackDataObjects.Response.AutoResponses.CreateObjectNotification createObjectNotificationResponse, EntityBaseModel entityBaseModel, uint objectId)
        {
            switch (createObjectNotificationResponse.CreateObjectResponse.ObjectType)
            {
                case BacnetObjectType.ObjectEventEnrollment:
                case BacnetObjectType.ObjectTrendlog:
                    {
                        EventTransitionBitsModel eventTransitionBitsModel = new EventTransitionBitsModel();
                        switch (entityBaseModel.EntityType)
                        {
                            case AppConstants.EntityTypes.Controller:
                                {
                                    if (AppCoreRepo.Instance.BACnetConfigurationData.IsBuildingAlarm)
                                    {
                                        eventTransitionBitsModel.ToFault = true;
                                        eventTransitionBitsModel.ToNormal = true;
                                        eventTransitionBitsModel.ToOffNormal = true;
                                    }
                                    else
                                    {
                                        eventTransitionBitsModel.ToFault = false;
                                        eventTransitionBitsModel.ToNormal = false;
                                        eventTransitionBitsModel.ToOffNormal = false;
                                    }
                                }
                                break;
                            case AppConstants.EntityTypes.Zone:
                                {
                                    string zoneType = string.Empty;
                                    switch (((ZoneEntityModel)entityBaseModel).ZoneType)
                                    {
                                        case AppConstants.ZoneTypes.BeaconSpace:
                                            zoneType = "Beacon Zone";
                                            break;
                                        case AppConstants.ZoneTypes.UserSpace:
                                            zoneType = "User Zone";
                                            break;
                                        case AppConstants.ZoneTypes.GeneralSpace:
                                            zoneType = "General Zone";
                                            break;
                                    }
                                    string objectName = createObjectNotificationResponse.CreateObjectResponse.ObjectType == BacnetObjectType.ObjectEventEnrollment ? AppConstants.LIGHTING_EVENTENROLLEMENT_OBJECT : AppConstants.LIGHTING_TRENDLOG_OBJECT;
                                    if (AppCoreRepo.Instance.BACnetConfigurationData.AlarmNotificationSetting != null && AppCoreRepo.Instance.BACnetConfigurationData.AlarmNotificationSetting[zoneType].Any(x => x.Name == objectName && x.ZoneValue))
                                    {
                                        eventTransitionBitsModel.ToFault = true;
                                        eventTransitionBitsModel.ToNormal = true;
                                        eventTransitionBitsModel.ToOffNormal = true;
                                    }
                                    else
                                    {
                                        eventTransitionBitsModel.ToFault = false;
                                        eventTransitionBitsModel.ToNormal = false;
                                        eventTransitionBitsModel.ToOffNormal = false;
                                    }
                                }
                                break;
                            default:
                                break;
                        }
                        //To set notification class property
                        string objectDetailsKey = EntityHelper.GetObjectDetailsKey(BacnetObjectType.ObjectNotificationClass, AppConstants.LIGHTING_NOTIFICATIONCLASS_OBJECT);

                        if (entityBaseModel.BACnetData.ObjectDetails.ContainsKey(objectDetailsKey))//Blind device doest exists notification class object
                        {
                            uint notificationObjectID = entityBaseModel.BACnetData.ObjectDetails[objectDetailsKey].ObjectID;
                            StackManager.Instance.WriteProperty((uint)createObjectNotificationResponse.CreateObjectResponse.DeviceId,
                                                       createObjectNotificationResponse.CreateObjectResponse.ObjectType, objectId,
                                                       notificationObjectID, BacnetPropertyID.PropNotificationClass);
                        }
                        else
                        {
                            Logger.Instance.Log(LogLevel.ERROR, "UpdatePropertyValue failed at CreateObjectCallbackManager Error : ObjectNotificationClass not found in Device ID " + createObjectNotificationResponse.CreateObjectResponse.DeviceId.ToString());
                        }

                        if (entityBaseModel.BACnetData.ObjectDetails.Values.Any(s=> s.ObjectType == BacnetObjectType.ObjectEventEnrollment))
                        {
                            //Setting event bits
                            StackManager.Instance.WriteProperty(createObjectNotificationResponse.CreateObjectResponse.DeviceId,
                                                                BacnetObjectType.ObjectEventEnrollment, objectId,
                                                                eventTransitionBitsModel, BacnetPropertyID.PropEventEnable);
                        }
                        else
                        {
                            Logger.Instance.Log(LogLevel.ERROR, "UpdatePropertyValue failed at CreateObjectCallbackManager Error : ObjectEventEnrollment not found in Device ID " + createObjectNotificationResponse.CreateObjectResponse.DeviceId.ToString());
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        private bool UpdatePersistentModel(StackDataObjects.Response.AutoResponses.CreateObjectNotification createNotificationResponse, PersistedModels model, uint objectID, string objectName)
        {
            switch (createNotificationResponse.CreateObjectResponse.ObjectType)
            {
                case BacnetObjectType.ObjectCalendar:
                    {
                        CalendarPersistedModel calendarObject = new CalendarPersistedModel();
                        calendarObject.ObjectID = objectID;
                        calendarObject.Name = objectName;
                        if (!(model.Calendars.Count >= AppConstants.MAX_NUMBER_OF_CALENDAR_OBJECTS))
                        {
                            model.Calendars.Add(calendarObject);
                            return true;
                        }
                        else
                            return false;
                    }

                case BacnetObjectType.ObjectEventEnrollment:
                    {
                        EventEnrollmentPersistedModel eventEnrollementObject = new EventEnrollmentPersistedModel();

                        eventEnrollementObject.ObjectID = objectID;
                        eventEnrollementObject.Name = objectName;
                        if (!(model.EventEnrollments.Count >= AppConstants.MAX_NUMBER_OF_EVENTENROLLMENT_OBJECTS))
                        {
                            model.EventEnrollments.Add(eventEnrollementObject);
                            return true;
                        }
                        else
                            return false;
                    }

                case BacnetObjectType.ObjectNotificationClass:
                    {
                        NotificationClassPersistedModel notificationObject = new NotificationClassPersistedModel();
                        notificationObject.ObjectID = objectID;
                        notificationObject.Name = objectName;
                        if (!(model.NotificationClasses.Count >= AppConstants.MAX_NUMBER_OF_NOTIFICATION_OBJECTS))
                        {
                            model.NotificationClasses.Add(notificationObject);
                            return true;
                        }
                        else
                            return false;
                    }

                case BacnetObjectType.ObjectSchedule:
                    {
                        SchedulePersistedModel schuduleObject = new SchedulePersistedModel();
                        schuduleObject.ObjectID = objectID;
                        schuduleObject.Name = objectName;
                        schuduleObject.PriorityForWriting = 16;
                        if (!(model.Schedules.Count >= AppConstants.MAX_NUMBER_OF_SCHEDULE_OBJECTS))
                        {
                            model.Schedules.Add(schuduleObject);
                            return true;
                        }
                        else
                            return false;
                    }

                case BacnetObjectType.ObjectTrendlog:
                    {
                        TrendLogPersistedModel trendLogObject = new TrendLogPersistedModel();
                        trendLogObject.ObjectID = objectID;
                        trendLogObject.Name = objectName;
                        if (!(model.TrendLogs.Count >= AppConstants.MAX_NUMBER_OF_TRENDLOG_OBJECTS))
                        {
                            model.TrendLogs.Add(trendLogObject);
                            return true;
                        }
                        else
                            return false;
                    }
                default:
                    return false;
            }
        }
    }
}
