﻿#region File Header
// Copyright ©  2017 UniDEL Systems Pvt. Ltd.
// All rights are reserved. Reproduction or transmission in whole or in part, in any form or by any
// means, electronic, mechanical or otherwise, is prohibited without the prior written consent of the
// copyright owner.
//
// Filename          : DeleteObjectHandler.cs
// Author            : Amol Kulkarni
// Description       : This class is act as handler for delete object
// Revision History  : 
// 
// Date        Author            Modification
// 19-05-2017  Amol Kulkarni      File Created
#endregion

using Molex.BACnet.Gateway.Log;
using Molex.BACnet.Gateway.ServiceCore.Managers;
using Molex.StackDataObjects.Response;
using Molex.StackDataObjects.Response.AutoResponses;

namespace Molex.BACnet.Gateway.ServiceCore.BACnetHandlers
{
    /// <summary>
    /// This class is act as handler for delete object in BACnet Stack
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201704:49 PM</TimeStamp>
    internal static class DeleteObjectHandler
    {
        /// <summary>
        ///To handle delete object action of BMS
        /// </summary>
        /// <param name="response">response</param>
        internal static void OnDeleteObject(AutoResponse response)
        {
            DeleteObjectNotification deleteObjectNotification = response as DeleteObjectNotification;

            if (!ValidateDeleteRequest(deleteObjectNotification))
            {
                Logger.Instance.Log(LogLevel.ERROR, "DeleteObjectHandler:OnDeleteObject: Unsupported object deletion request");
                deleteObjectNotification.ErrorModel = new ErrorResponseModel();
                deleteObjectNotification.ErrorModel.Type = ErrorType.StackError;
                deleteObjectNotification.IsSucceed = false;
                deleteObjectNotification.ErrorModel.ErrorCode = (int)StackDataObjects.Constants.BacnetErrorCode.ErrorCodeObjectDeletionNotPermitted;
                deleteObjectNotification.ErrorModel.ErrorClass = (int)StackDataObjects.Constants.BacnetErrorClass.ErrorClassObject;
                return;
            }
            DeleteCallbackManager.DeleteFeatureObjects(deleteObjectNotification);
        }

        private static bool ValidateDeleteRequest(DeleteObjectNotification deleteObjectNotification)
        {
            switch (deleteObjectNotification.DeleteObjectResponse.ObjectType)
            {
                case Molex.StackDataObjects.Constants.BacnetObjectType.ObjectCalendar:
                case Molex.StackDataObjects.Constants.BacnetObjectType.ObjectEventEnrollment:
                case Molex.StackDataObjects.Constants.BacnetObjectType.ObjectNotificationClass:
                case Molex.StackDataObjects.Constants.BacnetObjectType.ObjectSchedule:
                case Molex.StackDataObjects.Constants.BacnetObjectType.ObjectTrendlog:
                    return true;

                default:
                    return false;
            }
        }
    }
}
