﻿#region File Header
// Copyright ©  2017 UniDEL Systems Pvt. Ltd.
// All rights are reserved. Reproduction or transmission in whole or in part, in any form or by any
// means, electronic, mechanical or otherwise, is prohibited without the prior written consent of the
// copyright owner.
//
// Filename          : CreateObjectHandler.cs
// Author            : Amol Kulkarni
// Description       : This class is act as handler for create object
// Revision History  : 
// 
// Date        Author            Modification
// 19-05-2017  Amol Kulkarni      File Created
#endregion

using Molex.BACnet.Gateway.Log;
using Molex.BACnet.Gateway.ServiceCore.Managers;
using Molex.StackDataObjects.Response;
using Molex.StackDataObjects.Response.AutoResponses;
using System;

namespace Molex.BACnet.Gateway.ServiceCore.BACnetHandlers
{
    /// <summary>
    /// This class is act as handler for create object in BACnetStack
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201704:49 PM</TimeStamp>
    internal static class CreateObjectHandler
    {
        /// <summary>
        /// To handle create object action of BMS
        /// </summary>
        /// <param name="response">response</param>
        public static void OnCreateObject(AutoResponse response)
        {
            CreateObjectNotification createObjectNotificationResponse = response as CreateObjectNotification;

            HandleCreateObject(createObjectNotificationResponse);
        }

        private static void HandleCreateObject(CreateObjectNotification createObjectNotificationResponse)
        {
             try
             {
                 //To verify only supported objects are created and unknown object request is returned with error.
                 if(!ValidateObjectCreation(createObjectNotificationResponse))
                 {
                     Logger.Instance.Log(LogLevel.ERROR, "CreateObjectHandler:OnCreateObject: Unsupported object creation request");
                     createObjectNotificationResponse.ErrorModel = new ErrorResponseModel();
                     createObjectNotificationResponse.ErrorModel.Type = ErrorType.StackError;
                     createObjectNotificationResponse.IsSucceed = false;
                     createObjectNotificationResponse.ErrorModel.ErrorCode = (int)StackDataObjects.Constants.BacnetErrorCode.ErrorCodeDynamicCreationNotSupported;
                     createObjectNotificationResponse.ErrorModel.ErrorClass = (int)StackDataObjects.Constants.BacnetErrorClass.ErrorClassObject;
                     return;
                 }
                 (new CreateObjectCallBackManager()).CreateFeatureObjects(createObjectNotificationResponse);
             }
             catch (Exception ex)
             {
                 Logger.Instance.Log(ex, LogLevel.ERROR, "CreateObjectHandler:OnCreateObject: Error occured while creating object");
                 createObjectNotificationResponse.ErrorModel = new ErrorResponseModel();
                 createObjectNotificationResponse.ErrorModel.Type = ErrorType.StackError;
                 createObjectNotificationResponse.IsSucceed = false;
                 createObjectNotificationResponse.ErrorModel.ErrorCode = (int)StackDataObjects.Constants.BacnetErrorCode.ErrorCodeOther;
                 createObjectNotificationResponse.ErrorModel.ErrorClass = (int)StackDataObjects.Constants.BacnetErrorClass.ErrorClassObject;
             }
        }

        private static bool ValidateObjectCreation(CreateObjectNotification createObjectNotificationResponse)
        {
            switch (createObjectNotificationResponse.CreateObjectResponse.ObjectType)
            {
                case Molex.StackDataObjects.Constants.BacnetObjectType.ObjectCalendar:
                case Molex.StackDataObjects.Constants.BacnetObjectType.ObjectEventEnrollment:
                case Molex.StackDataObjects.Constants.BacnetObjectType.ObjectNotificationClass:
                case Molex.StackDataObjects.Constants.BacnetObjectType.ObjectSchedule:
                case Molex.StackDataObjects.Constants.BacnetObjectType.ObjectTrendlog:
                    return true;
        
                default:
                    return false;
            }
        }
    }
}
