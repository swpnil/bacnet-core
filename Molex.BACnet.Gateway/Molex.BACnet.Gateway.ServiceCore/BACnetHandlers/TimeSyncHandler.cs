﻿using Molex.BACnet.Gateway.Log;
using Molex.BACnet.Gateway.ServiceCore.Helpers;
using Molex.BACnet.Gateway.ServiceCore.Managers;
using Molex.BACnet.Gateway.ServiceCore.Models;
using Molex.StackDataObjects.Response;
using Molex.StackDataObjects.Response.AutoResponses;
using System;

/// <summary>
/// To make time of two machine in sync
/// </summary>
/// <CreatedBy>[ERROR: invalid expression Environment.UserName]</CreatedBy><TimeStamp>[ERROR: invalid expression Environment.Date][ERROR: invalid expression Environment.Time]</TimeStamp>
namespace Molex.BACnet.Gateway.ServiceCore.BACnetHandlers
{
    /// <summary>
    /// TimeSyncHandler
    /// </summary>
    /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>12/29/20176:23 PM</TimeStamp>
    public static class TimeSyncHandler
    {
        #region PublicMethod
        /// <summary>
        /// Called when [time synchronize].
        /// </summary>
        /// <param name="response">The response.</param>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>12/29/20176:23 PM</TimeStamp>
        public static void OnTimeSync(AutoResponse response)
        {
            ResultModel<string> result = null;
            try
            {
                TimeSyncNotification timeSyncNotification = response as TimeSyncNotification;

                DateTime dateTime = timeSyncNotification.TimeSyncResponse.DateTimeSync;

                if (timeSyncNotification.TimeSyncResponse.IsUTCFormat)
                {
                    result = HTTPAPIManager.Instance.UpdateTimeSync(
                        timeSyncNotification.TimeSyncResponse.DateTimeSync.ToString("yyyy-MM-ddTHH\\:mm\\:ssZ"));

                    if (!result.IsSuccess)
                    {
                        Logger.Instance.Log(LogLevel.ERROR, "TimeSyncHandler:OnUTCTimeSync" + result.Error.OptionalMessage);
                        return;
                    }
                    response.IsSucceed = SysDateTimeHelper.SetDateTime(timeSyncNotification.TimeSyncResponse.DateTimeSync - DateTimeOffset.Now.Offset);
                }
                else
                {
                    result = HTTPAPIManager.Instance.UpdateTimeSync(
                       timeSyncNotification.TimeSyncResponse.DateTimeSync.ToUniversalTime().ToString("yyyy-MM-ddTHH\\:mm\\:ssZ"));

                    if (!result.IsSuccess)
                    {
                        Logger.Instance.Log(LogLevel.ERROR, "TimeSyncHandler:OnTimeSync" + result.Error.OptionalMessage);
                        return;
                    }
                    response.IsSucceed = SysDateTimeHelper.SetDateTime(timeSyncNotification.TimeSyncResponse.DateTimeSync - DateTimeOffset.Now.Offset);
                }
                response.IsSucceed = true;
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(LogLevel.ERROR, ex.Message);
                response.ErrorModel = new ErrorResponseModel();
                response.ErrorModel.Type = ErrorType.StackError;
                response.IsSucceed = false;
                response.ErrorModel.ErrorCode = (int)StackDataObjects.Constants.BacnetErrorCode.ErrorCodeOther;
                response.ErrorModel.ErrorClass = (int)StackDataObjects.Constants.BacnetErrorClass.ErrorClassObject;
            }
        }

        #endregion PublicMethod





    }
}
