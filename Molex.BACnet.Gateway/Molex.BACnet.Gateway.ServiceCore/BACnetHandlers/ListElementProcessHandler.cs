﻿using Molex.BACnet.Gateway.Encryption;
using Molex.BACnet.Gateway.Log;
using Molex.BACnet.Gateway.ServiceCore.Constants;
using Molex.BACnet.Gateway.ServiceCore.Helpers;
using Molex.BACnet.Gateway.ServiceCore.Helpers.PersistentDataConverters;
using Molex.BACnet.Gateway.ServiceCore.Managers;
using Molex.BACnet.Gateway.ServiceCore.Models.Entity;
using Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel;
using Molex.BACnet.Gateway.ServiceCore.Repository;
using Molex.BACnet.Gateway.Utility.Helper;
using Molex.BACnetStackIntegration.StackManagedApi;
using Molex.StackDataObjects.APIModels;
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Response;
using Molex.StackDataObjects.Response.InternalResponses;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Molex.BACnet.Gateway.ServiceCore.BACnetHandlers
{
    /// <summary>
    /// This is a internal call back / after callback. Using the data 
    /// passed by stack, GW will store it in the .json file.
    /// This call back is generic. It will be used for Add & remove list and also for others.
    /// </summary>
    /// <CreatedBy>hp</CreatedBy>
    /// <TimeStamp>10/6/20179:09 PM</TimeStamp>
    public static class ListElementProcessHandler
    {
        /// <summary>
        /// Called when [list element process handler].
        /// </summary>
        /// <param name="response">The response.</param>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>10/7/201712:33 PM</TimeStamp>
        public static void OnListElementProcessHandler(InternalResponse response)
        {
            ListElementInternalResponse listElementResponseData = response as ListElementInternalResponse;

            UpdatePersistentModel(listElementResponseData);
        }

        private static void UpdatePersistentModel(ListElementInternalResponse listElementResponseData)
        {
            switch (listElementResponseData.ListElementInternalModelData.CallbackReason)
            {
                case BacnetCallbackReason.CallbackReasonListElementAdded:
                case BacnetCallbackReason.CallbackReasonListElementDeleted:
                    {
                        ResponseResult result = StackManagedApi.GetObjectPropertyValue(listElementResponseData.ListElementInternalModelData.DeviceId,
                                                                                       listElementResponseData.ListElementInternalModelData.ObjectId,
                                                                                       listElementResponseData.ListElementInternalModelData.ObjectType,
                                                                                       listElementResponseData.ListElementInternalModelData.PropertyType,
                                                                                       0, false);


                        WriteToPersistentModels((GetObjectPropertyValueResponse)result.Response,
                                                listElementResponseData.ListElementInternalModelData.DeviceId,
                                                listElementResponseData.ListElementInternalModelData.ObjectId,
                                                listElementResponseData.ListElementInternalModelData.ObjectType,
                                                listElementResponseData.ListElementInternalModelData.PropertyType);
                    }
                    break;
                case BacnetCallbackReason.CallbackReasonBDTUpdated:
                    {
                        ResponseResult result = StackManagedApi.GetBDTList();
                        if (!result.IsSucceed)
                        {
                            Logger.Instance.Log(LogLevel.ERROR, "BDT list read failed");
                        }
                        WriteBDTListToConfigurationFile(result.Response);
                    }
                    break;
                case BacnetCallbackReason.CallbackReasonFDTUpdated:
                    {
                        ResponseResult result = StackManagedApi.GetFDTList();
                        if (!result.IsSucceed)
                        {
                            Logger.Instance.Log(LogLevel.ERROR, "FDT list read failed");
                        }
                        WriteFDTListToConfigurationFile(result.Response);
                    }
                    break;
                default:
                    break;
            }
        }

        private static void WriteFDTListToConfigurationFile(ResponseBase responseBase)
        {
            ReadFDTResponse readFDTResponse = responseBase as ReadFDTResponse;
            if (readFDTResponse.FDTEntries.Count > 0)
            {
                AppCoreRepo.Instance.BACnetConfigurationData.FDTList = new List<FDTModel>();
                foreach (FDTModel fdtModel in readFDTResponse.FDTEntries)
                {
                    if (fdtModel.TimeRemaining == 0)
                    {
                        continue;
                    }

                    AppCoreRepo.Instance.BACnetConfigurationData.FDTList.Add(fdtModel);
                }
                AppCoreRepo.Instance.BACnetConfigurationData.BACnetPassword = CryptoHelper.Encrypt3DES(AppCoreRepo.Instance.BACnetConfigurationData.BACnetPassword);
                AppCoreRepo.Instance.BACnetConfigurationData.UserName = CryptoHelper.Encrypt3DES(AppCoreRepo.Instance.BACnetConfigurationData.UserName);
                AppCoreRepo.Instance.BACnetConfigurationData.Password = CryptoHelper.Encrypt3DES(AppCoreRepo.Instance.BACnetConfigurationData.Password);
                JSONReaderWriterHelper.WriteToJsonFile(Path.Combine(AppCoreRepo.Instance.ConfigurationFolder, AppConstants.BACNET_CONFIGURATION_FILENAME), AppCoreRepo.Instance.BACnetConfigurationData);
            }
        }

        private static void WriteBDTListToConfigurationFile(ResponseBase responseBase)
        {
            ReadBDTResponse readBDTResponse = responseBase as ReadBDTResponse;
            if (readBDTResponse.BDTEntries.Count > 0)
            {
                AppCoreRepo.Instance.BACnetConfigurationData.BDTList = new List<BDTModel>();
                AppCoreRepo.Instance.BACnetConfigurationData.BDTList = readBDTResponse.BDTEntries;
                AppCoreRepo.Instance.BACnetConfigurationData.BACnetPassword = CryptoHelper.Encrypt3DES(AppCoreRepo.Instance.BACnetConfigurationData.BACnetPassword);
                AppCoreRepo.Instance.BACnetConfigurationData.UserName = CryptoHelper.Encrypt3DES(AppCoreRepo.Instance.BACnetConfigurationData.UserName);
                AppCoreRepo.Instance.BACnetConfigurationData.Password = CryptoHelper.Encrypt3DES(AppCoreRepo.Instance.BACnetConfigurationData.Password);
                JSONReaderWriterHelper.WriteToJsonFile(Path.Combine(AppCoreRepo.Instance.ConfigurationFolder, AppConstants.BACNET_CONFIGURATION_FILENAME), AppCoreRepo.Instance.BACnetConfigurationData);
            }
        }


        private static void WriteToPersistentModels(GetObjectPropertyValueResponse responseBase, uint deviceId, uint objectId, BacnetObjectType objType, BacnetPropertyID propertyType)
        {
            EntityBaseModel entityModel = EntityHelper.GetEntityModelFromDeviceID(deviceId);
            switch (objType)
            {
                case BacnetObjectType.ObjectCalendar:
                    {
                        EntityBaseModel model = EntityHelper.GetEntityModelFromDeviceID(deviceId);
                        if (model == null)
                            return;

                        CalendarPersistedModel calendarPersistentModel = (CalendarPersistedModel)GetPersistedModel(BacnetObjectType.ObjectCalendar, objectId, model);
                        switch (propertyType)
                        {
                            case BacnetPropertyID.PropDateList:
                                {
                                    IEnumerable bacnetDateListObjects = (IEnumerable)responseBase.PropertyValue;
                                    calendarPersistentModel.DateList.Clear();
                                    foreach (var dateValues in bacnetDateListObjects)
                                    {
                                        calendarPersistentModel.DateList.Add(CalendarEntryModelConverter.ConvertToCalendarEntryModel((BacnetCalendarEntryModel)dateValues));
                                    }
                                }
                                break;
                        }
                    }
                    break;

                case BacnetObjectType.ObjectSchedule:
                    {
                        SchedulePersistedModel schedulePersistentModel = (SchedulePersistedModel)GetPersistedModel(BacnetObjectType.ObjectSchedule, objectId, entityModel);

                        switch (propertyType)
                        {
                            case BacnetPropertyID.PropListOfObjectPropertyReferences:
                                {
                                    List<BACnetDevObjPropRefModel> bacnetDevObjPropReferences = (List<BACnetDevObjPropRefModel>)responseBase.PropertyValue;

                                    schedulePersistentModel.ObjectPropertyRefernces.Clear();

                                    if (bacnetDevObjPropReferences == null)
                                    {
                                        AppCoreRepo.Instance.ScheduledObjects.Remove(objectId);
                                        break;
                                    }

                                    foreach (var bacnetDevObjRef in bacnetDevObjPropReferences)
                                    {
                                        schedulePersistentModel.ObjectPropertyRefernces.Add(DevObjPropRefModelConverter.ConvertToDevObjPropRefModel((BACnetDevObjPropRefModel)bacnetDevObjRef));
                                        if (!AppCoreRepo.Instance.ScheduledObjects.ContainsKey(objectId))
                                        {
                                            AppCoreRepo.Instance.ScheduledObjects.Add(objectId, CommonHelper.GenerateScheduleObjectMappedID((uint)deviceId, ((BACnetDevObjPropRefModel)bacnetDevObjRef).ObjectType, ((BACnetDevObjPropRefModel)bacnetDevObjRef).PropertyIdentifier, ((BACnetDevObjPropRefModel)bacnetDevObjRef).ObjId));
                                        }
                                    }
                                }
                                break;
                            case BacnetPropertyID.PropWeeklySchedule:
                                {
                                    IEnumerable bacnetTimeValueArrayModel = (IEnumerable)responseBase.PropertyValue;
                                    schedulePersistentModel.WeeklySchedule.Clear();

                                    foreach (var bacnetTimeValueModel in bacnetTimeValueArrayModel)
                                    {
                                        schedulePersistentModel.WeeklySchedule.Add(TimeValueArrayModelConverter.ConvertToTimeValueArrayModel((BacnetTimeValueArrayModel)bacnetTimeValueModel));
                                    }
                                }
                                break;
                            case BacnetPropertyID.PropExceptionSchedule:
                                {
                                    IEnumerable bacnetExceptionScheduleModel = (IEnumerable)responseBase.PropertyValue;
                                    schedulePersistentModel.ExceptionSchedule.Clear();

                                    foreach (var bacnetExceptionSchedule in bacnetExceptionScheduleModel)
                                    {
                                        schedulePersistentModel.ExceptionSchedule.Add(SpecialEventModelConverter.ConvertToSpecialEventModel((BacnetSpecialEventModel)bacnetExceptionSchedule));
                                    }
                                }
                                break;
                        }
                    }

                    break;
                case BacnetObjectType.ObjectNotificationClass:
                    {
                        EntityBaseModel model = EntityHelper.GetEntityModelFromDeviceID(deviceId);
                        if (model == null)
                            return;

                        NotificationClassPersistedModel notificationPersistentModel = (NotificationClassPersistedModel)GetPersistedModel(BacnetObjectType.ObjectNotificationClass, objectId, entityModel);

                        switch (propertyType)
                        {
                            case BacnetPropertyID.PropRecipientList:
                                {
                                    notificationPersistentModel.RecipientList.Clear();
                                    IEnumerable destinationModels = (IEnumerable)responseBase.PropertyValue;
                                    foreach (var destinationModel in destinationModels)
                                    {
                                        notificationPersistentModel.RecipientList.Add(DestinationTypeConverter.ConvertToDestinationModel((BacnetDestinationModel)destinationModel));
                                    }
                                }
                                break;
                        }
                    }
                    break;
            }

            PersistenceStorageManager.Instance.Store(EntityHelper.GetControllerEntityModel(entityModel), AppCoreRepo.Instance.PersistentModelCache);
        }


        private static PersistentBase GetPersistedModel(BacnetObjectType objectType, uint ObjectId, EntityBaseModel entityBaseModel)
        {
            PersistentBase model = null;
            switch (objectType)
            {
                case BacnetObjectType.ObjectCalendar:
                    model = AppCoreRepo.Instance.PersistentModelCache[entityBaseModel.MappingKey].Calendars.Where(x => x.ObjectID == ObjectId).FirstOrDefault();
                    break;

                case BacnetObjectType.ObjectSchedule:
                    model = AppCoreRepo.Instance.PersistentModelCache[entityBaseModel.MappingKey].Schedules.Where(x => x.ObjectID == ObjectId).FirstOrDefault();
                    break;

                case BacnetObjectType.ObjectNotificationClass:
                    {
                        //This logic is implemented since one Notification class is created for each VRD and VD.
                        // To handle changes for this particular object null checks and key checks are included.
                        if (AppCoreRepo.Instance.PersistentModelCache.ContainsKey(entityBaseModel.MappingKey))
                        {
                            model = AppCoreRepo.Instance.PersistentModelCache[entityBaseModel.MappingKey].NotificationClasses.Where(x => x.ObjectID == ObjectId).FirstOrDefault();
                        }
                        else
                        {
                            AppCoreRepo.Instance.PersistentModelCache[entityBaseModel.MappingKey] = new PersistedModels();
                        }

                        if (model == null)
                        {
                            NotificationClassPersistedModel notificationObject = new NotificationClassPersistedModel();
                            notificationObject.ObjectID = ObjectId;
                            AppCoreRepo.Instance.PersistentModelCache[entityBaseModel.MappingKey].NotificationClasses.Add(notificationObject);
                            model = AppCoreRepo.Instance.PersistentModelCache[entityBaseModel.MappingKey].NotificationClasses.Where(x => x.ObjectID == ObjectId).FirstOrDefault();
                        }
                    }
                    break;
            }
            return model;
        }
    }
}

