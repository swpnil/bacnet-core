﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              ReInitializeDeviceHandler
///   Description:        Application level handler for Reinitialize device service 
///   Author:             Prasad Joshi                  
///   Date:               06/20/17
#endregion

using Molex.BACnet.Gateway.Log;
using Molex.BACnet.Gateway.ServiceCore.Helpers;
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Response;
using Molex.StackDataObjects.Response.AutoResponses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.BACnetHandlers
{
    /// <summary>
    /// This class is used for handle Reinitialize device request
    /// </summary>
    /// <CreatedBy>prasad.joshi</CreatedBy><TimeStamp>6/27/20175:44 PM</TimeStamp>
    internal class ReInitializeDeviceHandler
    {
        #region InternalMethod

        /// <summary>
        /// Called when [re initialize device].
        /// </summary>
        /// <param name="response">The Auto response.</param>
        /// <CreatedBy>prasad.joshi</CreatedBy><TimeStamp>6/27/20175:45 PM</TimeStamp>
        internal static void OnReInitializeDevice(AutoResponse response)
        {
            try
            {
                //TODO: Implement cold start and warm start logic like properties retain
                ReInitDeviceResponse reInitDeviceResponse = response as ReInitDeviceResponse;

                switch (reInitDeviceResponse.ReInitializeDevice.ReInitState)
                {
                    case BacnetReinitializedState.BacnetReinitColdstart:
                    case BacnetReinitializedState.BacnetReinitWarmstart:
                        {
                            System.Threading.Thread.Sleep(1000);
                            AppHelper.RestartService();
                        }
                        break;

                    default:
                        {
                            Logger.Instance.Log(LogLevel.ERROR, "ReInitializeDeviceHandler.OnReInitializeDevice: state not supported " + reInitDeviceResponse.ReInitializeDevice.ReInitState);
                            response.ErrorModel = new ErrorResponseModel();
                            response.ErrorModel.Type = ErrorType.StackError;
                            response.IsSucceed = false;
                            response.ErrorModel.ErrorCode = (int)StackDataObjects.Constants.BacnetErrorCode.ErrorCodeConfigurationInProgress;
                            response.ErrorModel.ErrorClass = (int)StackDataObjects.Constants.BacnetErrorClass.ErrorClassDevice;
                        }
                        break;
                }
            }
            catch (Exception e)
            {
                Logger.Instance.Log(e, LogLevel.ERROR, "ReInitializeDeviceHandler.OnReInitializeDevice: failed");
                response.ErrorModel = new ErrorResponseModel();
                response.ErrorModel.Type = ErrorType.StackError;
                response.IsSucceed = false;
                response.ErrorModel.ErrorCode = (int)StackDataObjects.Constants.BacnetErrorCode.ErrorCodeOther;
                response.ErrorModel.ErrorClass = (int)StackDataObjects.Constants.BacnetErrorClass.ErrorClassObject;
            }
        }

        #endregion InternalMethod
    }
}
