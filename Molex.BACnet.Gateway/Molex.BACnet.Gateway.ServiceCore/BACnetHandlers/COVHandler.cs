﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              <COVHandler.cs>
///   Description:        <This will handle change of value in BACnet Stack>
///   Author:             Sonal Choudhary                  
///   Date:               05/01/17
///   Notes:              <Notes>
///   Revision History: 
///   Modified By:     Modified Date:    Reviewed By:      Reviewed Date:    Review Comments:
///---------------------------------------------------------------------------------------------
#endregion

using Molex.StackDataObjects.Response.AutoResponses;

namespace Molex.BACnet.Gateway.ServiceCore.BACnetHandlers
{
    /// <summary>
    /// This will handle change of value in BACnet Stack
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201704:48 PM</TimeStamp>
    internal static class COVHandler
    {
        /// <summary>
        ///To handle change of value action of BMS
        /// </summary>
        /// <param name="response">response</param>
        internal static void OnChangeOfValue(AutoResponse response)
        {

        }
    }
}
