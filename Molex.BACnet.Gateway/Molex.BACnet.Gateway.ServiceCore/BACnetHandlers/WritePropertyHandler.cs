﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              WritePropertyHandler
///   Description:        This class is used as handler for write property
///   Author:             Amol Kulkarni                  
///   Date:               05/19/17
#endregion


using Molex.BACnet.Gateway.Log;
using Molex.BACnet.Gateway.ServiceCore.Constants;
using Molex.BACnet.Gateway.ServiceCore.Helpers;
using Molex.BACnet.Gateway.ServiceCore.Managers;
using Molex.BACnet.Gateway.ServiceCore.Models.Entity;
using Molex.BACnet.Gateway.ServiceCore.Repository;
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Response;
using Molex.StackDataObjects.Response.AutoResponses;
using System.Linq;
using System;
using Molex.BACnet.Gateway.ServiceCore.Models.BACnet;

namespace Molex.BACnet.Gateway.ServiceCore.BACnetHandlers
{
    /// <summary>
    /// This class is used as handler for write property in BACnet 
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201704:50 PM</TimeStamp>
    public static class WritePropertyHandler
    {
        #region PublicMethod
        /// <summary>
        ///  To handle Write Property action at Building Level,Floor Level and Zone Level from BMS
        /// </summary>
        /// <param name="response">The response object is used as action delegate for write property</param>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>30-05-201714:32</TimeStamp>"
        public static void OnWriteProperty(AutoResponse response)
        {
            try
            {
                WritePropertyNotificationResponse writePropertyNotificationResponse = response as WritePropertyNotificationResponse;
                //To get key of entity model

                switch (writePropertyNotificationResponse.WritePropertyCallbackData.ObjectProperty)
                {
                    case BacnetPropertyID.PropDescription:
                    case BacnetPropertyID.PropObjectName:
                        HandleNameDescription(writePropertyNotificationResponse);
                        break;
                    case BacnetPropertyID.PropRelinquishDefault:
                    case BacnetPropertyID.PropPresentValue:
                        HandlePresentValue(writePropertyNotificationResponse);
                        break;
                    case BacnetPropertyID.PropHighLimit:
                    case BacnetPropertyID.PropLowLimit:
                        HandleHighLimitLowLimitValues(writePropertyNotificationResponse);
                        break;
                    case BacnetPropertyID.PropOutOfService:
                        HandleOutOfServiceValue(writePropertyNotificationResponse);
                        break;
                    default:
                        {
                            switch (writePropertyNotificationResponse.WritePropertyCallbackData.ObjectType)
                            {
                                case BacnetObjectType.ObjectCalendar:
                                    HandleCalendarObjectProperties(writePropertyNotificationResponse);
                                    break;

                                case BacnetObjectType.ObjectDevice:
                                    HandleAPDUProperties(writePropertyNotificationResponse);
                                    break;

                                case BacnetObjectType.ObjectEventEnrollment:
                                    HandleEventEnrollmentProperties(writePropertyNotificationResponse);
                                    break;

                                case BacnetObjectType.ObjectNotificationClass:
                                    HandleNotificationClassObjectProperty(writePropertyNotificationResponse);
                                    break;

                                case BacnetObjectType.ObjectSchedule:
                                    HandleScheduleObjectProperties(writePropertyNotificationResponse);
                                    break;

                                case BacnetObjectType.ObjectTrendlog:
                                    HandleTrendLogObject(writePropertyNotificationResponse);
                                    break;
                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(LogLevel.ERROR, ex.Message);
                response.ErrorModel = new ErrorResponseModel();
                response.ErrorModel.Type = ErrorType.StackError;
                response.IsSucceed = false;
                response.ErrorModel.ErrorCode = (int)StackDataObjects.Constants.BacnetErrorCode.ErrorCodeOther;
                response.ErrorModel.ErrorClass = (int)StackDataObjects.Constants.BacnetErrorClass.ErrorClassObject;
            }
        }

        private static void HandleHighLimitLowLimitValues(WritePropertyNotificationResponse writePropertyNotificationResponse)
        {
            try
            {
                if (writePropertyNotificationResponse == null)
                    return;
                if (writePropertyNotificationResponse.WritePropertyCallbackData == null)
                    return;
                switch (writePropertyNotificationResponse.WritePropertyCallbackData.ObjectType)
                {
                    case BacnetObjectType.ObjectAnalogValue:
                        {
                            Single brightness = Convert.ToSingle(writePropertyNotificationResponse.WritePropertyCallbackData.InputValue);
                            if (brightness < AppConstants.BRIGHTNESS_MIN_LIMIT || brightness > AppConstants.BRIGHTNESS_MAX_LIMIT)
                            {
                                writePropertyNotificationResponse.ErrorModel = new ErrorResponseModel();
                                writePropertyNotificationResponse.ErrorModel.Type = ErrorType.StackError;
                                writePropertyNotificationResponse.IsSucceed = false;
                                writePropertyNotificationResponse.ErrorModel.ErrorCode = (int)StackDataObjects.Constants.BacnetErrorCode.ErrorCodeValueOutOfRange;
                                writePropertyNotificationResponse.ErrorModel.ErrorClass = (int)StackDataObjects.Constants.BacnetErrorClass.ErrorClassProperty;
                                return;
                            }
                        }
                        break;
                    case BacnetObjectType.ObjectAnalogInput:
                        {
                            string key = CommonHelper.GenerateBACnetObjectMappedID(writePropertyNotificationResponse.WritePropertyCallbackData.DeviceId, writePropertyNotificationResponse.WritePropertyCallbackData.ObjectType, writePropertyNotificationResponse.WritePropertyCallbackData.ObjectInstance);

                            if (!AppCoreRepo.Instance.BACnetObjectEntityLookUp.Keys.Contains(key))
                            {
                                Logger.Instance.Log(LogLevel.ERROR, "Error occurred due to key not found in BACnetObjectEntityLookUp");
                                writePropertyNotificationResponse.ErrorModel = new ErrorResponseModel();
                                writePropertyNotificationResponse.ErrorModel.Type = ErrorType.Unknown;
                                writePropertyNotificationResponse.IsSucceed = false;
                                writePropertyNotificationResponse.ErrorModel.ErrorCode = (int)StackDataObjects.Constants.BacnetErrorCode.ErrorCodeUnknownObject;
                                writePropertyNotificationResponse.ErrorModel.ErrorClass = (int)StackDataObjects.Constants.BacnetErrorClass.ErrorClassObject;
                                return;
                            }
                            EntityBaseModel entityBaseModel = AppCoreRepo.Instance.BACnetObjectEntityLookUp[key];
                            if (entityBaseModel as SensorEntityModel != null)
                            {
                                Single sensorMinValue = EntityHelper.GetSensorMinValue((entityBaseModel as SensorEntityModel).SensorType);
                                Single sensorMaxValue = EntityHelper.GetSensorMaxValue((entityBaseModel as SensorEntityModel).SensorType);
                                Single brightness = Convert.ToSingle(writePropertyNotificationResponse.WritePropertyCallbackData.InputValue);
                                if (brightness < sensorMinValue || brightness > sensorMaxValue)
                                {
                                    writePropertyNotificationResponse.ErrorModel = new ErrorResponseModel();
                                    writePropertyNotificationResponse.ErrorModel.Type = ErrorType.StackError;
                                    writePropertyNotificationResponse.IsSucceed = false;
                                    writePropertyNotificationResponse.ErrorModel.ErrorCode = (int)StackDataObjects.Constants.BacnetErrorCode.ErrorCodeValueOutOfRange;
                                    writePropertyNotificationResponse.ErrorModel.ErrorClass = (int)StackDataObjects.Constants.BacnetErrorClass.ErrorClassProperty;
                                    return;
                                }
                            }
                            else if (entityBaseModel as ZoneEntityModel != null)
                            {
                                if (!(entityBaseModel as ZoneEntityModel).BACnetData.ObjectDetails.Values.Any(s => s.ObjectID == writePropertyNotificationResponse.WritePropertyCallbackData.ObjectInstance && s.ObjectType == writePropertyNotificationResponse.WritePropertyCallbackData.ObjectType))
                                    return;
                                BACnetObjectDetails objDetails = (entityBaseModel as ZoneEntityModel).BACnetData.ObjectDetails.Values.Where(s => s.ObjectID == writePropertyNotificationResponse.WritePropertyCallbackData.ObjectInstance && s.ObjectType == writePropertyNotificationResponse.WritePropertyCallbackData.ObjectType).First();
                                AppConstants.SensorType sensorType = AppConstants.SensorType.AirQuality;
                                switch (objDetails.ObjectID)
                                {
                                    case AppConstants.INITIAL_AVERAGE_SENSOR_AI_OBJECT_ID:
                                        sensorType = AppConstants.SensorType.LuxLevel;
                                        break;
                                    case AppConstants.INITIAL_AVERAGE_SENSOR_AI_OBJECT_ID + 1:
                                        sensorType = AppConstants.SensorType.AirQuality;
                                        break;
                                    case AppConstants.INITIAL_AVERAGE_SENSOR_AI_OBJECT_ID + 2:
                                        sensorType = AppConstants.SensorType.ColorTemperature;
                                        break;
                                    case AppConstants.INITIAL_AVERAGE_SENSOR_AI_OBJECT_ID + 3:
                                        sensorType = AppConstants.SensorType.Power;
                                        break;
                                    case AppConstants.INITIAL_AVERAGE_SENSOR_AI_OBJECT_ID + 4:
                                        sensorType = AppConstants.SensorType.Temperature;
                                        break;
                                    case AppConstants.INITIAL_AVERAGE_SENSOR_AI_OBJECT_ID + 5:
                                        sensorType = AppConstants.SensorType.Humidity;
                                        break;
                                    default:
                                        break;
                                }
                                Single sensorMinValue = EntityHelper.GetSensorMinValue(sensorType);
                                Single sensorMaxValue = EntityHelper.GetSensorMaxValue(sensorType);
                                if (sensorType == AppConstants.SensorType.Power)
                                {
                                    //max value of individual power sensor multiply by max allowed sensor in zone
                                    sensorMaxValue = sensorMaxValue * 2500;
                                }
                                Single brightness = Convert.ToSingle(writePropertyNotificationResponse.WritePropertyCallbackData.InputValue);
                                if (brightness < sensorMinValue || brightness > sensorMaxValue)
                                {
                                    writePropertyNotificationResponse.ErrorModel = new ErrorResponseModel();
                                    writePropertyNotificationResponse.ErrorModel.Type = ErrorType.StackError;
                                    writePropertyNotificationResponse.IsSucceed = false;
                                    writePropertyNotificationResponse.ErrorModel.ErrorCode = (int)StackDataObjects.Constants.BacnetErrorCode.ErrorCodeValueOutOfRange;
                                    writePropertyNotificationResponse.ErrorModel.ErrorClass = (int)StackDataObjects.Constants.BacnetErrorClass.ErrorClassProperty;
                                    return;
                                }
                            }
                        }
                        break;
                    case BacnetObjectType.ObjectPositiveIntegerValue:
                        {
                            string key = CommonHelper.GenerateBACnetObjectMappedID(writePropertyNotificationResponse.WritePropertyCallbackData.DeviceId, writePropertyNotificationResponse.WritePropertyCallbackData.ObjectType, writePropertyNotificationResponse.WritePropertyCallbackData.ObjectInstance);

                            if (!AppCoreRepo.Instance.BACnetObjectEntityLookUp.Keys.Contains(key))
                            {
                                Logger.Instance.Log(LogLevel.ERROR, "Error occurred due to key not found in BACnetObjectEntityLookUp");
                                writePropertyNotificationResponse.ErrorModel = new ErrorResponseModel();
                                writePropertyNotificationResponse.ErrorModel.Type = ErrorType.Unknown;
                                writePropertyNotificationResponse.IsSucceed = false;
                                writePropertyNotificationResponse.ErrorModel.ErrorCode = (int)StackDataObjects.Constants.BacnetErrorCode.ErrorCodeUnknownObject;
                                writePropertyNotificationResponse.ErrorModel.ErrorClass = (int)StackDataObjects.Constants.BacnetErrorClass.ErrorClassObject;
                                return;
                            }
                            EntityBaseModel entityBaseModel = AppCoreRepo.Instance.BACnetObjectEntityLookUp[key];
                            if (entityBaseModel == null)
                                return;
                            if (entityBaseModel as ZoneEntityModel == null)
                                return;
                            if ((entityBaseModel as ZoneEntityModel).ZoneType != AppConstants.ZoneTypes.BeaconSpace)
                                return;
                            Single brightness = Convert.ToSingle(writePropertyNotificationResponse.WritePropertyCallbackData.InputValue);
                            if (brightness < 0 || brightness > AppConstants.MAX_RGB_VALUE)
                            {
                                writePropertyNotificationResponse.ErrorModel = new ErrorResponseModel();
                                writePropertyNotificationResponse.ErrorModel.Type = ErrorType.StackError;
                                writePropertyNotificationResponse.IsSucceed = false;
                                writePropertyNotificationResponse.ErrorModel.ErrorCode = (int)StackDataObjects.Constants.BacnetErrorCode.ErrorCodeValueOutOfRange;
                                writePropertyNotificationResponse.ErrorModel.ErrorClass = (int)StackDataObjects.Constants.BacnetErrorClass.ErrorClassProperty;
                                return;
                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "Error occurred In HandleHighLimitLowLimitValues() Method in WritePropertyHandler ");
                writePropertyNotificationResponse.ErrorModel = new ErrorResponseModel();
                writePropertyNotificationResponse.ErrorModel.Type = ErrorType.Unknown;
                writePropertyNotificationResponse.IsSucceed = false;
                writePropertyNotificationResponse.ErrorModel.ErrorCode = (int)StackDataObjects.Constants.BacnetErrorCode.ErrorCodeUnknownObject;
                writePropertyNotificationResponse.ErrorModel.ErrorClass = (int)StackDataObjects.Constants.BacnetErrorClass.ErrorClassObject;
                return;
            }
            
        }

        private static void HandleNotificationClassObjectProperty(WritePropertyNotificationResponse writePropertyNotificationResponse)
        {
            try
            {
                (new WritePropertyCallbackManager()).WriteNotificationClassProperties(writePropertyNotificationResponse);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "WritePropertyCallbackManager:HandleNotificationClassObjectProperty: Unable to write property value for " + writePropertyNotificationResponse.WritePropertyCallbackData.ObjectProperty.ToString());
            }
        }

        private static void HandleCalendarObjectProperties(WritePropertyNotificationResponse writePropertyNotificationResponse)
        {
            try
            {
                (new WritePropertyCallbackManager()).WriteCalendarProperties(writePropertyNotificationResponse);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "WritePropertyCallbackManager:HandleCalendarObject: Unable to write property value for " + writePropertyNotificationResponse.WritePropertyCallbackData.ObjectProperty.ToString());
            }
        }

        private static void HandleEventEnrollmentProperties(WritePropertyNotificationResponse writePropertyNotificationResponse)
        {
            try
            {
                (new WritePropertyCallbackManager()).WriteEventEnrollmentProperties(writePropertyNotificationResponse);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "WritePropertyCallbackManager:HandleEventEnrollmentProperties: Unable to write property value for " + writePropertyNotificationResponse.WritePropertyCallbackData.ObjectProperty.ToString());
            }
        }

        private static void HandleTrendLogObject(WritePropertyNotificationResponse writePropertyNotificationResponse)
        {
            try
            {
                (new WritePropertyCallbackManager()).WriteTrendLogProperties(writePropertyNotificationResponse);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "WritePropertyCallbackManager:HandleTrendLogObject: Unable to write property value for " + writePropertyNotificationResponse.WritePropertyCallbackData.ObjectProperty.ToString());
            }
        }

        private static void HandleScheduleObjectProperties(WritePropertyNotificationResponse writePropertyNotificationResponse)
        {
            try
            {
                (new WritePropertyCallbackManager()).WriteSchduleObjectProperties(writePropertyNotificationResponse);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "WritePropertyCallbackManager:HandleScheduleObjectProperties: Unable to write property value for " + writePropertyNotificationResponse.WritePropertyCallbackData.ObjectProperty.ToString());
            }
        }

        private static void HandleAPDUProperties(WritePropertyNotificationResponse writePropertyNotificationResponse)
        {
            try
            {
                (new WritePropertyCallbackManager()).WriteAPDUValues(writePropertyNotificationResponse);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "WritePropertyCallbackManager:UpdateNameDescription: Error occured");
            }
        }


        /// <summary>
        /// Handles the out of service value.
        /// </summary>
        /// <param name="writePropertyNotificationResponse">The write property notification response.</param>
        /// <CreatedBy>Rohit Galgali</CreatedBy>
        /// <TimeStamp>7/12/20174:27 PM</TimeStamp>
        private static void HandleOutOfServiceValue(WritePropertyNotificationResponse writePropertyNotificationResponse)
        {
            try
            {
                (new WritePropertyCallbackManager()).UpdateOutOfServiceValue(writePropertyNotificationResponse);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "WritePropertyCallbackManager:UpdateNameDescription: Error occured");
            }
        }


        #endregion PublicMethod

        #region PrivateMethods

        private static bool ValidateWriteAccess(string key, EntityBaseModel entityBaseModel, AutoResponse response)
        {
            string ObjKey = string.Empty;
            WritePropertyNotificationResponse writePropertyNotificationResponse = response as WritePropertyNotificationResponse;
            bool isSuccess = true;

            switch (entityBaseModel.EntityType)
            {
                case AppConstants.EntityTypes.Fixture:
                    FixtureEntityModel fixtureEntityModel = entityBaseModel as FixtureEntityModel;
                    ObjKey = EntityHelper.GetObjectKey(writePropertyNotificationResponse, AppCoreRepo.Instance.BACnetObjectEntityLookUp[key]);

                    if (!fixtureEntityModel.BACnetData.ObjectDetails[ObjKey].IsOutOfService)
                    {
                        writePropertyNotificationResponse.ErrorModel = new ErrorResponseModel();
                        writePropertyNotificationResponse.ErrorModel.Type = ErrorType.BACnetError;
                        writePropertyNotificationResponse.IsSucceed = false;
                        writePropertyNotificationResponse.ErrorModel.ErrorCode = (int)StackDataObjects.Constants.BacnetErrorCode.ErrorCodeWriteAccessDenied;
                        writePropertyNotificationResponse.ErrorModel.ErrorClass = (int)StackDataObjects.Constants.BacnetErrorClass.ErrorClassProperty;
                        isSuccess = false;
                    }
                    break;
                case AppConstants.EntityTypes.Sensor:
                    SensorEntityModel sensorEntityModel = entityBaseModel as SensorEntityModel;
                    ObjKey = EntityHelper.GetObjectKey(writePropertyNotificationResponse, AppCoreRepo.Instance.BACnetObjectEntityLookUp[key]);

                    if (!sensorEntityModel.BACnetData.ObjectDetails[ObjKey].IsOutOfService)
                    {
                        writePropertyNotificationResponse.ErrorModel = new ErrorResponseModel();
                        writePropertyNotificationResponse.ErrorModel.Type = ErrorType.BACnetError;
                        writePropertyNotificationResponse.IsSucceed = false;
                        writePropertyNotificationResponse.ErrorModel.ErrorCode = (int)StackDataObjects.Constants.BacnetErrorCode.ErrorCodeWriteAccessDenied;
                        writePropertyNotificationResponse.ErrorModel.ErrorClass = (int)StackDataObjects.Constants.BacnetErrorClass.ErrorClassProperty;
                        isSuccess = false;
                    }
                    break;
                default:
                    isSuccess = true;
                    break;
            }

            return isSuccess;

        }

        /// <summary>
        /// Validates the write property present value.
        /// If validation fail it will fill response as ErrorResponseModel
        /// </summary>
        /// <param name="response">The response.</param>
        /// <param name="entityBaseModel">The entityBaseModel.</param>
        /// <returns>bool</returns>
        /// <CreatedBy>prasad.joshi</CreatedBy><TimeStamp>6/2/20176:41 PM</TimeStamp>
        private static bool ValidateWritePropertyPresentValue(EntityBaseModel entityBaseModel, AutoResponse response)
        {
            try
            {
                WritePropertyNotificationResponse WritePropertyNotificationResponse = response as WritePropertyNotificationResponse;

                if (WritePropertyNotificationResponse.WritePropertyCallbackData.InputValue == null)
                {
                    return true;
                }

                switch (WritePropertyNotificationResponse.WritePropertyCallbackData.ObjectType)
                {
                    case BacnetObjectType.ObjectAnalogValue:
                        {
                            Single brightness = Convert.ToSingle(WritePropertyNotificationResponse.WritePropertyCallbackData.InputValue);
                            if (brightness < AppConstants.BRIGHTNESS_MIN_LIMIT || brightness > AppConstants.BRIGHTNESS_MAX_LIMIT)
                            {
                                Logger.Instance.Log(LogLevel.ERROR, "Analog Value: Present value:" + brightness + ", validation failed");
                                response.ErrorModel = new ErrorResponseModel();
                                response.ErrorModel.Type = ErrorType.StackError;
                                response.IsSucceed = false;
                                response.ErrorModel.ErrorCode = (int)StackDataObjects.Constants.BacnetErrorCode.ErrorCodeValueOutOfRange;
                                response.ErrorModel.ErrorClass = (int)StackDataObjects.Constants.BacnetErrorClass.ErrorClassProperty;
                                return false;
                            }
                            return true;
                        }
                    case BacnetObjectType.ObjectMultiStateValue:
                        {
                            switch (WritePropertyNotificationResponse.WritePropertyCallbackData.ObjectProperty)
                            {
                                case BacnetPropertyID.PropPresentValue:
                                    Single lightScene = Convert.ToSingle(WritePropertyNotificationResponse.WritePropertyCallbackData.InputValue);

                                    if (lightScene < AppConstants.LIGHTSCENE_MIN_LIMIT || lightScene > AppCoreRepo.Instance.AllLightScene.Length)
                                    {
                                        Logger.Instance.Log(LogLevel.ERROR, "Multi state value: Present value:" + lightScene + ", validation failed");
                                        response.ErrorModel = new ErrorResponseModel();
                                        response.ErrorModel.Type = ErrorType.StackError;
                                        response.IsSucceed = false;
                                        response.ErrorModel.ErrorCode = (int)StackDataObjects.Constants.BacnetErrorCode.ErrorCodeValueOutOfRange;
                                        response.ErrorModel.ErrorClass = (int)StackDataObjects.Constants.BacnetErrorClass.ErrorClassProperty;
                                        return false;
                                    }
                                    break;
                                //case BacnetPropertyID.PropProprietaryMood:
                                //    int mood = Convert.ToInt32(WritePropertyNotificationResponse.WritePropertyCallbackData.InputValue);

                                //    if (mood < AppConstants.MOOD_MIN_LIMIT || mood > AppCoreRepo.Instance.AvailableMoods.Length)
                                //    {
                                //        Logger.Instance.Log(LogLevel.ERROR, "Multi state value: Mood propraitary value:" + mood + ", validation failed");
                                //        response.ErrorModel = new ErrorResponseModel();
                                //        response.ErrorModel.Type = ErrorType.StackError;
                                //        response.IsSucceed = false;
                                //        response.ErrorModel.ErrorCode = (int)StackDataObjects.Constants.BacnetErrorCode.ErrorCodeValueOutOfRange;
                                //        response.ErrorModel.ErrorClass = (int)StackDataObjects.Constants.BacnetErrorClass.ErrorClassProperty;
                                //        return false;
                                //    }
                                //    break;
                            }

                            return true;
                        }
                    case BacnetObjectType.ObjectPositiveIntegerValue:
                        {
                            EntityBaseModel entity = EntityHelper.GetEntityModelFromDeviceID(WritePropertyNotificationResponse.WritePropertyCallbackData.DeviceId);
                            ZoneEntityModel zoneEntityModel = entity as ZoneEntityModel;

                            switch (zoneEntityModel.ZoneType)
                            {
                                case AppConstants.ZoneTypes.BeaconSpace:
                                    uint presentRGB = Convert.ToUInt32(WritePropertyNotificationResponse.WritePropertyCallbackData.InputValue);
                                    if (presentRGB > AppConstants.MAX_RGB_VALUE)
                                    {
                                        Logger.Instance.Log(LogLevel.ERROR, "Beacon Positive Integer Value: Present value of RGB:" + presentRGB + ", validation failed");
                                        response.ErrorModel = new ErrorResponseModel();
                                        response.ErrorModel.Type = ErrorType.StackError;
                                        response.IsSucceed = false;
                                        response.ErrorModel.ErrorCode = (int)StackDataObjects.Constants.BacnetErrorCode.ErrorCodeValueOutOfRange;
                                        response.ErrorModel.ErrorClass = (int)StackDataObjects.Constants.BacnetErrorClass.ErrorClassProperty;
                                        return false;
                                    }
                                    BeaconZoneEntityModel beaconColorModel = CommonHelper.GetRGBValues(presentRGB);

                                    if (beaconColorModel.Red < AppConstants.COLOR_MIN_LIMIT || beaconColorModel.Red > AppConstants.COLOR_MAX_LIMIT)
                                    {
                                        Logger.Instance.Log(LogLevel.ERROR, "Beacon Positive Integer Value: Present value of red:" + beaconColorModel.Red + ", validation failed");
                                        response.ErrorModel = new ErrorResponseModel();
                                        response.ErrorModel.Type = ErrorType.StackError;
                                        response.IsSucceed = false;
                                        response.ErrorModel.ErrorCode = (int)StackDataObjects.Constants.BacnetErrorCode.ErrorCodeValueOutOfRange;
                                        response.ErrorModel.ErrorClass = (int)StackDataObjects.Constants.BacnetErrorClass.ErrorClassProperty;
                                        return false;
                                    }
                                    if (beaconColorModel.Green < AppConstants.COLOR_MIN_LIMIT || beaconColorModel.Green > AppConstants.COLOR_MAX_LIMIT)
                                    {
                                        Logger.Instance.Log(LogLevel.ERROR, "Beacon Positive Integer Value: Present value of green:" + beaconColorModel.Green + ", validation failed");
                                        response.ErrorModel = new ErrorResponseModel();
                                        response.ErrorModel.Type = ErrorType.StackError;
                                        response.IsSucceed = false;
                                        response.ErrorModel.ErrorCode = (int)StackDataObjects.Constants.BacnetErrorCode.ErrorCodeValueOutOfRange;
                                        response.ErrorModel.ErrorClass = (int)StackDataObjects.Constants.BacnetErrorClass.ErrorClassProperty;
                                        return false;
                                    }
                                    if (beaconColorModel.Blue < AppConstants.COLOR_MIN_LIMIT || beaconColorModel.Blue > AppConstants.COLOR_MAX_LIMIT)
                                    {
                                        Logger.Instance.Log(LogLevel.ERROR, "Beacon Positive Integer Value: Present value of blue:" + beaconColorModel.Blue + ", validation failed");
                                        response.ErrorModel = new ErrorResponseModel();
                                        response.ErrorModel.Type = ErrorType.StackError;
                                        response.IsSucceed = false;
                                        response.ErrorModel.ErrorCode = (int)StackDataObjects.Constants.BacnetErrorCode.ErrorCodeValueOutOfRange;
                                        response.ErrorModel.ErrorClass = (int)StackDataObjects.Constants.BacnetErrorClass.ErrorClassProperty;
                                        return false;
                                    }
                                    return true;
                                case AppConstants.ZoneTypes.UserSpace:
                                case AppConstants.ZoneTypes.GeneralSpace:
                                    Single pivValue;
                                    bool result = Single.TryParse(WritePropertyNotificationResponse.WritePropertyCallbackData.InputValue.ToString(), out pivValue);
                                    if (result)
                                        return true;
                                    else
                                        return false;
                                default:
                                    return false;
                            }


                        }
                    case BacnetObjectType.ObjectBinaryValue:
                        return true;
                    default:
                        {
                            response.ErrorModel = new ErrorResponseModel();
                            response.ErrorModel.Type = ErrorType.Unknown;
                            response.IsSucceed = false;
                            response.ErrorModel.ErrorCode = (int)StackDataObjects.Constants.BacnetErrorCode.ErrorCodeUnknownObject;
                            response.ErrorModel.ErrorClass = (int)StackDataObjects.Constants.BacnetErrorClass.ErrorClassObject;
                            return false;
                        }
                }
            }
            catch (Exception e)
            {
                Logger.Instance.Log(e, LogLevel.ERROR, "WritePropertyHandler:ValidateWritePropertyPresentValue :ValidateWritePropertyPresentValue fail");
                response.ErrorModel = new ErrorResponseModel();
                response.ErrorModel.Type = ErrorType.StackError;
                response.IsSucceed = false;
                response.ErrorModel.ErrorCode = (int)StackDataObjects.Constants.BacnetErrorCode.ErrorCodeOther;
                response.ErrorModel.ErrorClass = (int)StackDataObjects.Constants.BacnetErrorClass.ErrorClassObject;
                return false;
            }

        }

        /// <summary>
        /// Validates the name and description of callback property.
        /// </summary>
        /// <param name="writePropertyNotificationResponse">The write property notification response.</param>
        /// <returns></returns>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>7/7/20174:14 PM</TimeStamp>
        private static bool ValidateNameAndDescription(WritePropertyNotificationResponse writePropertyNotificationResponse)
        {
            string description = Convert.ToString(writePropertyNotificationResponse.WritePropertyCallbackData.PropertyValue);
            if (string.IsNullOrEmpty(description))
            {
                Logger.Instance.Log(LogLevel.ERROR, "Analog Value: Description value:" + description + ", validation failed");
                writePropertyNotificationResponse.ErrorModel = new ErrorResponseModel();
                writePropertyNotificationResponse.ErrorModel.Type = ErrorType.StackError;
                writePropertyNotificationResponse.IsSucceed = false;
                writePropertyNotificationResponse.ErrorModel.ErrorCode = (int)StackDataObjects.Constants.BacnetErrorCode.ErrorCodeValueOutOfRange;
                writePropertyNotificationResponse.ErrorModel.ErrorClass = (int)StackDataObjects.Constants.BacnetErrorClass.ErrorClassProperty;
                return false;
            }
            return true;
        }

        /// <summary>
        /// Handles the present value callback.
        /// </summary>
        /// <param name="writePropertyNotificationResponse">The write property notification response.</param>
        /// <CreatedBy>Rohit Galgali</CreatedBy>
        /// <TimeStamp>7/7/20174:31 PM</TimeStamp>
        private static void HandlePresentValue(WritePropertyNotificationResponse writePropertyNotificationResponse)
        {
            bool IsSucceed = false;
            try
            {
                if (!AppCoreRepo.Instance.BACnetConfigurationData.WriteAccess)
                {
                    writePropertyNotificationResponse.ErrorModel = new ErrorResponseModel();
                    writePropertyNotificationResponse.ErrorModel.Type = ErrorType.StackError;
                    writePropertyNotificationResponse.IsSucceed = false;
                    writePropertyNotificationResponse.ErrorModel.ErrorCode = (int)StackDataObjects.Constants.BacnetErrorCode.ErrorCodeWriteAccessDenied;
                    writePropertyNotificationResponse.ErrorModel.ErrorClass = (int)StackDataObjects.Constants.BacnetErrorClass.ErrorClassProperty;
                    return;
                }

                string key = CommonHelper.GenerateBACnetObjectMappedID(writePropertyNotificationResponse.WritePropertyCallbackData.DeviceId, writePropertyNotificationResponse.WritePropertyCallbackData.ObjectType, writePropertyNotificationResponse.WritePropertyCallbackData.ObjectInstance);

                if (!AppCoreRepo.Instance.BACnetObjectEntityLookUp.Keys.Contains(key))
                {
                    Logger.Instance.Log(LogLevel.ERROR, "Error occurred due to key not found in BACnetObjectEntityLookUp");
                    writePropertyNotificationResponse.ErrorModel = new ErrorResponseModel();
                    writePropertyNotificationResponse.ErrorModel.Type = ErrorType.Unknown;
                    writePropertyNotificationResponse.IsSucceed = false;
                    writePropertyNotificationResponse.ErrorModel.ErrorCode = (int)StackDataObjects.Constants.BacnetErrorCode.ErrorCodeUnknownObject;
                    writePropertyNotificationResponse.ErrorModel.ErrorClass = (int)StackDataObjects.Constants.BacnetErrorClass.ErrorClassObject;
                    return;
                }

                EntityBaseModel entityBaseModel = AppCoreRepo.Instance.BACnetObjectEntityLookUp[key];

                if (!ValidateWriteAccess(key, entityBaseModel, writePropertyNotificationResponse))
                {
                    return;
                }

                if (!ValidateWritePropertyPresentValue(entityBaseModel, writePropertyNotificationResponse))
                {
                    Logger.Instance.Log(LogLevel.ERROR, "Present value write property validation failed for object type:" + writePropertyNotificationResponse.WritePropertyCallbackData.ObjectType + ",value:" + writePropertyNotificationResponse.WritePropertyCallbackData.PropertyValue);
                    return;
                }

                if (writePropertyNotificationResponse.WritePropertyCallbackData.ObjectProperty == BacnetPropertyID.PropRelinquishDefault && writePropertyNotificationResponse.WritePropertyCallbackData.Priority != 0)
                {
                    return;
                }

                switch (entityBaseModel.EntityType)
                {
                    case Constants.AppConstants.EntityTypes.Building:
                        IsSucceed = (new WritePropertyCallbackManager()).WritePropertyBuildingLevel(key, writePropertyNotificationResponse);
                        break;
                    case Constants.AppConstants.EntityTypes.Floor:
                        IsSucceed = (new WritePropertyCallbackManager()).WritePropertyFloorLevel(key, writePropertyNotificationResponse);
                        break;
                    case Constants.AppConstants.EntityTypes.Zone:
                        //Fix for BAC-18 issue
                        //To do remove the code after pub-sub done with multiple channel
                        AppCoreRepo.Instance.BACnetObjectsInWPProcess.Add(writePropertyNotificationResponse.WritePropertyCallbackData.ObjectInstance);
                        IsSucceed = (new WritePropertyCallbackManager()).WritePropertyZoneLevel(key, writePropertyNotificationResponse);
                        break;
                    case Constants.AppConstants.EntityTypes.Fixture:
                    case Constants.AppConstants.EntityTypes.Sensor:
                        IsSucceed = true;
                        break;
                }

                if (!IsSucceed)
                {
                    if (writePropertyNotificationResponse.ErrorModel != null && writePropertyNotificationResponse.ErrorModel.ErrorClass == (int)BacnetErrorClass.ErrorClassProperty && writePropertyNotificationResponse.ErrorModel.ErrorCode == (int)BacnetErrorCode.ErrorCodeValueOutOfRange)
                    {
                        //Fix for BAC-18 issue
                        //To do remove the code after pub-sub done with multiple channel
                        AppCoreRepo.Instance.BACnetObjectsInWPProcess.Remove(writePropertyNotificationResponse.WritePropertyCallbackData.ObjectInstance);
                        return;
                    }
                    else if (writePropertyNotificationResponse.ErrorModel != null && writePropertyNotificationResponse.ErrorModel.ErrorClass == (int)BacnetErrorClass.ErrorClassDevice && writePropertyNotificationResponse.ErrorModel.ErrorCode == (int)BacnetErrorCode.ErrorCodeOperationalProblem)
                    {
                        //Fix for BAC-18 issue
                        //To do remove the code after pub-sub done with multiple channel
                        AppCoreRepo.Instance.BACnetObjectsInWPProcess.Remove(writePropertyNotificationResponse.WritePropertyCallbackData.ObjectInstance);
                        return;
                    }
                    else
                    {
                        writePropertyNotificationResponse.ErrorModel = new ErrorResponseModel();
                        writePropertyNotificationResponse.ErrorModel.Type = ErrorType.BACnetError;
                        writePropertyNotificationResponse.IsSucceed = false;
                        writePropertyNotificationResponse.ErrorModel.ErrorCode = (int)StackDataObjects.Constants.BacnetErrorCode.ErrorCodeOther;
                        writePropertyNotificationResponse.ErrorModel.ErrorClass = (int)StackDataObjects.Constants.BacnetErrorClass.ErrorClassObject;
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "OnWriteProperty:HandlePresentValue: fail HandlePresentValue");
                writePropertyNotificationResponse.ErrorModel = new ErrorResponseModel();
                writePropertyNotificationResponse.ErrorModel.Type = ErrorType.StackError;
                writePropertyNotificationResponse.IsSucceed = false;
                writePropertyNotificationResponse.ErrorModel.ErrorCode = (int)StackDataObjects.Constants.BacnetErrorCode.ErrorCodeOther;
                writePropertyNotificationResponse.ErrorModel.ErrorClass = (int)StackDataObjects.Constants.BacnetErrorClass.ErrorClassObject;
            }

            //Fix for BAC-18 issue
            //To do remove the code after pub-sub done with multiple channel
            AppCoreRepo.Instance.BACnetObjectsInWPProcess.Remove(writePropertyNotificationResponse.WritePropertyCallbackData.ObjectInstance);
        }

        /// <summary>
        /// Handles the name and description value callback.
        /// </summary>
        /// <param name="writePropertyNotificationResponse">The write property notification response.</param>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>7/7/20174:31 PM</TimeStamp>
        private static void HandleNameDescription(WritePropertyNotificationResponse writePropertyNotificationResponse)
        {
            try
            {
                if (!ValidateNameAndDescription(writePropertyNotificationResponse))
                {
                    Logger.Instance.Log(LogLevel.ERROR, "Incorrect Description");
                    return;
                }
                (new WritePropertyCallbackManager()).UpdateNameDescription(writePropertyNotificationResponse);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "WritePropertyCallbackManager:UpdateNameDescription: Error occured");
            }
        }

        #endregion PrivateMethods
    }
}
