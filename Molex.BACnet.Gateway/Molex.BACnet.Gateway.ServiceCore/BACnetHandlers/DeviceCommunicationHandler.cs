﻿
using Molex.StackDataObjects.Response.AutoResponses;
namespace Molex.BACnet.Gateway.ServiceCore.BACnetHandlers
{
    /// <summary>
    /// The DeviceCommunicationHandler
    /// </summary>
    /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>9/13/20178:35 PM</TimeStamp>
    public class DeviceCommunicationHandler
    {
        /// <summary>
        /// Called when [DCC].
        /// </summary>
        /// <param name="response">The response.</param>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>9/13/20178:35 PM</TimeStamp>
        public static void OnDCC(AutoResponse response)
        {
            response.IsSucceed = true;
        }
    }
}
