﻿using Molex.BACnet.Gateway.ServiceCore.Helpers;
using Molex.BACnet.Gateway.ServiceCore.Models.Entity;
using Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel;
using Molex.BACnet.Gateway.ServiceCore.Repository;
using Molex.BACnetStackIntegration.Processor;
using Molex.StackDataObjects.APIModels;
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Response.AutoResponses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.BACnetHandlers
{
    /// <summary>
    /// This is a autoresponse callback. It will only validate the data,
    /// and respond to stack to process the list. Another call back will be used to 
    /// process the list in GW.
    /// </summary>
    /// <CreatedBy>hp</CreatedBy>
    /// <TimeStamp>10/6/20175:57 PM</TimeStamp>
    public static class AddListElementHandler
    {
        /// <summary>
        /// Called when [add list element handler].
        /// </summary>
        /// <param name="response">The response.</param>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>10/6/20175:57 PM</TimeStamp>
        public static void OnAddListElementHandler(AutoResponse response)
        {
            AddListElementResponse addElementResonse  = response as AddListElementResponse;
            if (ValidateResponse(addElementResonse))
            {
                response.IsSucceed = true;
            }
            else
            {
                response.IsSucceed = false;
            }
            
        }

        private static bool ValidateResponse(AddListElementResponse addElementResonse)
        {
            switch (addElementResonse.AddListElementResponseData.ObjectType)
            {
                case BacnetObjectType.ObjectCalendar:
                case BacnetObjectType.ObjectDevice:
                case BacnetObjectType.ObjectEventEnrollment:
                case BacnetObjectType.ObjectNotificationClass:
                case BacnetObjectType.ObjectTrendlog:
                    return true;

                case BacnetObjectType.ObjectSchedule:
                    {
                        switch (addElementResonse.AddListElementResponseData.ObjectProperty)
                        {
                            case BacnetPropertyID.PropListOfObjectPropertyReferences:
                                {
                                    List<BACnetDevObjPropRefModel> bacnetDevObjPropReferences = (List<BACnetDevObjPropRefModel>)addElementResonse.AddListElementResponseData.PropertyValue;
                                    EntityBaseModel model = EntityHelper.GetEntityModelFromDeviceID(addElementResonse.AddListElementResponseData.DeviceId);

                                    if(VerifyForZoneLevelObjects(bacnetDevObjPropReferences, model, addElementResonse.AddListElementResponseData.ObjectInstance))
                                    {
                                        return true;
                                    }
                                    return false;
                                }
                            
                            default:
                                return true;
                        }
                    }
                default:
                    return false;
            }
        }

        private static bool VerifyForZoneLevelObjects(List<BACnetDevObjPropRefModel> listOfObjectFromCallBack, EntityBaseModel model, uint scheduleObjectId)
        {
            //User has set null or empty value. Which is considered as valid scenario
            if (listOfObjectFromCallBack == null)
                return true;

            //Verify list has only one element in it
            if (listOfObjectFromCallBack.Count > 1)
            {
                return false;
            }

            //Check if there is already element present in Stack
            //If the schedule object has already one list-object-reference configured from other BMS
            //And another BMS sends add list request then this condition will be handled
            if(AppCoreRepo.Instance.PersistentModelCache.ContainsKey(model.MappingKey))
            {
                SchedulePersistedModel persistedObject = AppCoreRepo.Instance.PersistentModelCache[model.MappingKey].Schedules.Where(x => x.ObjectID == scheduleObjectId).FirstOrDefault();

                if (persistedObject != null && persistedObject.ObjectPropertyRefernces != null && persistedObject.ObjectPropertyRefernces.Count > 0)
                {
                    return false;
                }
            }
            

            //Get the callback value from the list. Since only one schedule is restricted getting value from 0 index.
            BACnetDevObjPropRefModel objectPropertyReference = listOfObjectFromCallBack[0];

            //Verify if object is for AV, MSV, PIV only
            if (objectPropertyReference.ObjectType != BacnetObjectType.ObjectAnalogValue &&
                objectPropertyReference.ObjectType != BacnetObjectType.ObjectMultiStateValue &&
                objectPropertyReference.ObjectType != BacnetObjectType.ObjectPositiveIntegerValue)
            {
                return false;
            }

            //Verify if any property other than present value or propriotery value(Mood) is added by user.
            if (objectPropertyReference.PropertyIdentifier != BacnetPropertyID.PropPresentValue)
            {
                return false;
            }

            //Verify only zone level objects are scheduled
            if (!model.BACnetData.ObjectDetails.Values.Where(x => x.ObjectType == objectPropertyReference.ObjectType).Any(x => x.ObjectID == objectPropertyReference.ObjId))
            {
                return false;
            }

            //Check across all the schedule objects within the device for duplicate entries.
            foreach (var scheduleObject in AppCoreRepo.Instance.ScheduledObjects)
            {
                if (scheduleObject.Key == scheduleObjectId)
                {
                    continue;
                }

                string objectKey = CommonHelper.GenerateScheduleObjectMappedID((uint)model.BACnetData.DeviceID, objectPropertyReference.ObjectType, objectPropertyReference.PropertyIdentifier, objectPropertyReference.ObjId);

                if (scheduleObject.Value == objectKey)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
