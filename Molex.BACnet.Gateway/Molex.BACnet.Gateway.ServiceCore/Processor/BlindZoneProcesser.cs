﻿using Molex.BACnet.Gateway.Log;
using Molex.BACnet.Gateway.ServiceCore.AppResources;
using Molex.BACnet.Gateway.ServiceCore.Constants;
using Molex.BACnet.Gateway.ServiceCore.Helpers;
using Molex.BACnet.Gateway.ServiceCore.Managers;
using Molex.BACnet.Gateway.ServiceCore.Models;
using Molex.BACnet.Gateway.ServiceCore.Models.BACnet;
using Molex.BACnet.Gateway.ServiceCore.Models.Entity;
using Molex.BACnet.Gateway.ServiceCore.Models.MolexAPIModels;
using Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel;
using Molex.BACnet.Gateway.ServiceCore.Repository;
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Models;
using Molex.StackDataObjects.Response;
using System.Collections.Generic;
using System.Linq;

namespace Molex.BACnet.Gateway.ServiceCore.Processor
{
    /// <summary>
    /// 
    /// </summary>
    /// <CreatedBy>Swpnil Vyas</CreatedBy>
    /// <TimeStamp>12-09-2018</TimeStamp>
    /// <seealso cref="Molex.BACnet.Gateway.ServiceCore.Processor.ProcessorBase" />
    public class BlindZoneProcesser : ZoneBaseProcessor
    {
        /// <summary>
        /// Processes the entity.
        /// </summary>
        /// <param name="entityBaseModel">The entity base model.</param>
        /// <param name="processorConfiguration">The processor configuration.</param>
        /// <returns>
        /// boolean
        /// </returns>
        /// <CreatedBy>Swpnil Vyas</CreatedBy>
        /// <TimeStamp>12-09-2018</TimeStamp>
        /// <CreatedBy>Swpnil Vyas</CreatedBy><TimeStamp>12-09-2018</TimeStamp>
        public override bool ProcessEntity(Models.Entity.EntityBaseModel entityBaseModel, Models.ProcessorConfigurationModel processorConfiguration)
        {
            EventTransitionBitsModel eventTransitionBitsModel = new EventTransitionBitsModel();
            LimitEnableBitsModel limitEnableBitsModel = new LimitEnableBitsModel();
            limitEnableBitsModel.HighLimit = true;
            limitEnableBitsModel.LowLimit = true;
            Logger.Instance.Log(LogLevel.DEBUG, "ZoneProcessor.ProcessEntity : Processing Zone");

            if (!(entityBaseModel.EntityType == Constants.AppConstants.EntityTypes.Zone))
            {
                Logger.Instance.Log(LogLevel.ERROR, "ZoneProcessor.ProcessEntity : Entity model mismatch..Zone Entity model required ");
                return false;
            }

            BACnetConfigurationModel bacnetConfigurationModel = processorConfiguration.BACnetConfigurationData;

            if (processorConfiguration.BACnetConfigurationData.AlarmNotificationSetting != null)
            {
                processorConfiguration.ZoneAlarmNotificationSetting = processorConfiguration.BACnetConfigurationData.AlarmNotificationSetting["Blind Zone"];
            }
            ZoneEntityModel zoneEntityModel = entityBaseModel as ZoneEntityModel;


            uint deviceId = 0;

            switch (processorConfiguration.RestoreStrategy)
            {
                case AppConstants.RestoreIDReclaimStrategy.Preserve:
                    {
                        if (!processorConfiguration.EntityBACnetCache.ContainsKey(entityBaseModel.MappingKey))
                        {
                            deviceId = (uint)ObjectIdentifierManager.Instance.GetDeviceId();
                            deviceId = VerifyDeviceId(processorConfiguration, deviceId);
                            ObjectIdentifierManager.Instance.AddDeviceId(deviceId);
                        }
                        else
                        {
                            deviceId = (uint)processorConfiguration.EntityBACnetCache[entityBaseModel.MappingKey].DeviceId;
                            ObjectIdentifierManager.Instance.AddDeviceId(deviceId);
                        }
                    }
                    break;
                case AppConstants.RestoreIDReclaimStrategy.Incremental:
                    {
                        deviceId = (uint)ObjectIdentifierManager.Instance.GetDeviceId();
                        deviceId = VerifyDeviceId(processorConfiguration, deviceId);
                        ObjectIdentifierManager.Instance.AddDeviceId(deviceId);
                    }
                    break;
            }

            //For zone SNet is zero
            ResponseResult result = StackManager.Instance.CreateDeviceObject(deviceId, deviceId, (ushort)AppCoreRepo.Instance.BACnetConfigurationData.VirtualNetworkNumber);

            if (!result.IsSucceed)
            {
                Logger.Instance.Log(LogLevel.ERROR, "ZoneProcessor.ProcessEntity : Required Device could not be created");
                return false;
            }

            // Set Password for Zone Level device
            ResponseResult passwordResult = StackManager.Instance.SetDevicePassword((int)deviceId, bacnetConfigurationModel.BACnetPassword);
            if (!passwordResult.IsSucceed)
            {
                Logger.Instance.Log(LogLevel.ERROR, "ZoneProcessor.ProcessEntity: Set vendor Password fail for Zone " + deviceId);
                return false;
            }

            entityBaseModel.BACnetData.DeviceID = (int)deviceId;

            if (!CacheData(deviceId, BacnetObjectType.ObjectDevice, deviceId, entityBaseModel, AppConstants.LIGHTING_OBJECTDEVICE_OBJECT))
            {
                Logger.Instance.Log(LogLevel.DEBUG, "ZoneProcessor.ProcessEntity : Object Device caching failed");
                return false;
            }

            ResultModel<int> resultDeviceId = EntityHelper.GetDeviceId(entityBaseModel);

            if (!resultDeviceId.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.ERROR, "ZoneProcessor.ProcessEntity : Required Device not found..");
                return false;
            }

            #region Notification class


            ResultModel<CreateObjectResultModel> resultModel = CreateObjectAndCacheData(entityBaseModel, processorConfiguration, BacnetObjectType.ObjectNotificationClass, AppConstants.LIGHTING_NOTIFICATIONCLASS_OBJECT, AppConstants.EntityTypes.Zone);

            if (!resultModel.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.DEBUG, "ZoneProcessor.ProcessEntity: Create object and cache data failed for Notification class ");
                return false;
            }

            SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, BacnetObjectType.ObjectNotificationClass, BacnetPropertyID.PropDescription, AppResource.NotificationClassRepresent, AppConstants.LIGHTING_NOTIFICATIONCLASS_OBJECT, processorConfiguration, entityBaseModel);
            SetPropertyAccessType(resultModel.Data.DeviceId, resultModel.Data.ObjectId, BacnetObjectType.ObjectNotificationClass, BacnetPropertyID.PropPriority, PropAccessType.READ_WRITE, true);

            uint[] priorityListArray = null;
            if (processorConfiguration.UserBACnetObjects != null && AppCoreRepo.Instance.BACnetConfigurationData.ZoneNotificationPriority.OverridePersistantState == false)
            {
                foreach (var item in processorConfiguration.UserBACnetObjects)
                {
                    if (item.Key == zoneEntityModel.EntityKey)
                    {
                        if (item.Value.NotificationClasses.Any(s => s.ObjectID == 16001))
                        {
                            NotificationClassPersistedModel obj = item.Value.NotificationClasses.Where(s => s.ObjectID == 16001).First();
                            if (obj == null)
                                break;
                            if (obj.NotificationPriority == null)
                                break;
                            if (obj.NotificationPriority.Count != 3)
                                break;
                            priorityListArray = new uint[3];
                            priorityListArray[2] = obj.NotificationPriority[2];
                            priorityListArray[1] = obj.NotificationPriority[1];
                            priorityListArray[0] = obj.NotificationPriority[0];
                            break;
                        }
                    }
                }
            }
            if (priorityListArray == null)
            {
                priorityListArray = new uint[3];
                priorityListArray[2] = (uint)AppCoreRepo.Instance.BACnetConfigurationData.ZoneNotificationPriority.ToNormal;
                priorityListArray[1] = (uint)AppCoreRepo.Instance.BACnetConfigurationData.ZoneNotificationPriority.ToFault;
                priorityListArray[0] = (uint)AppCoreRepo.Instance.BACnetConfigurationData.ZoneNotificationPriority.ToOffNormal;
            }

            SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, BacnetObjectType.ObjectNotificationClass, BacnetPropertyID.PropPriority, priorityListArray, AppConstants.LIGHTING_NOTIFICATIONCLASS_OBJECT, processorConfiguration, entityBaseModel);
            processorConfiguration.VDNotificationClassInstanceNumber = resultModel.Data.ObjectId;
            #endregion  Notification class

            #region AnalogValue - All

            ResultModel<CreateObjectResultModel> globalAnalogModel = CreateObjectAndCacheData(entityBaseModel, processorConfiguration, BacnetObjectType.ObjectAnalogValue, AppResource.AnalogValueBlindObjectName, AppConstants.EntityTypes.Zone, 14001, true);

            if (!globalAnalogModel.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.ERROR, "ZoneProcessor.ProcessEntity : Zone object creation failure in BACnet stack..");
                return globalAnalogModel.IsSuccess;
            }
            SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropObjectName, AppResource.AnalogValueBlindObjectName);
            SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropDescription, AppResource.AnalogViewRepresentBlindZone, AppConstants.LIGHTING_BRIGHTENESS_OBJECT, processorConfiguration, entityBaseModel);
            SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropUnits, BacnetEngineeringUnits.UnitsPercent);
            SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropRelinquishDefault, AppConstants.RELINQUISH_DEFAULT);
            SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropTimeDelay, processorConfiguration.BACnetConfigurationData.TimeDelay);
            SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropNotificationClass, processorConfiguration.VDNotificationClassInstanceNumber);
            SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropHighLimit, AppConstants.LIGHT_FIXTURE_HIGH_LIMIT);
            SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropProprietaryGridX, zoneEntityModel.ProprietaryGridX);
            SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropProprietaryGridY, zoneEntityModel.ProprietaryGridY);
            SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropNotifyType, processorConfiguration.BACnetConfigurationData.NotifyType);

            //Info: Default "Limit enable" property override with true flag
            SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropLimitEnable, limitEnableBitsModel);
            eventTransitionBitsModel = GetEventTransitionBit(processorConfiguration.ZoneAlarmNotificationSetting, "Blind");
            SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropEventEnable, eventTransitionBitsModel);

            #endregion AnalogValue

            #region Location -Wise
            (entityBaseModel as ZoneEntityModel).Blinds.Sort((a, b) => (a.location.CompareTo(b.location)));
            foreach (var item in (entityBaseModel as ZoneEntityModel).Blinds)
            {
                #region Object Creation Validation
                bool isObjectAlreadyCreated = false;
                foreach (var objItem in (entityBaseModel as ZoneEntityModel).BACnetData.ObjectDetails)
                {
                    string objectKey = objItem.Key.Substring(objItem.Key.LastIndexOf('_') + 1);
                    if (objectKey.ToUpperInvariant() == AppHelper.GetFullLocationName(item.location.ToUpperInvariant()).ToUpperInvariant())
                    {
                        isObjectAlreadyCreated = true;
                        break;
                    }
                }
                if (isObjectAlreadyCreated)
                    continue;
                #endregion

                #region AnalogValue Location Wise
                int BlindLocationObjectID = 0;
                string locationName = string.Empty;
                switch (item.location)
                {
                    case "C":
                        locationName = "Center";
                        BlindLocationObjectID = 14002;
                        break;
                    case "E":
                        locationName = "East";
                        BlindLocationObjectID = 14003;
                        break;
                    case "N":
                        locationName = "North";
                        BlindLocationObjectID = 14004;
                        break;
                    case "NE":
                        locationName = "North-East";
                        BlindLocationObjectID = 14005;
                        break;
                    case "NW":
                        locationName = "North-West";
                        BlindLocationObjectID = 14006;
                        break;
                    case "S":
                        locationName = "South";
                        BlindLocationObjectID = 14007;
                        break;
                    case "SE":
                        locationName = "South-East";
                        BlindLocationObjectID = 14008;
                        break;
                    case "SW":
                        locationName = "South-West";
                        BlindLocationObjectID = 14009;
                        break;
                    case "W":
                        locationName = "West";
                        BlindLocationObjectID = 14010;
                        break;
                    case "ALL":
                        locationName = "All";
                        break;
                    default:
                        locationName = "All";
                        break;
                }
                ResultModel<CreateObjectResultModel> globalAnaloglocationModel = CreateObjectAndCacheData(entityBaseModel, processorConfiguration, BacnetObjectType.ObjectAnalogValue, AppResource.AnalogValueBlindObjectName + "_" + locationName, AppConstants.EntityTypes.Zone, BlindLocationObjectID, true);

                if (!globalAnaloglocationModel.IsSuccess)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "ZoneProcessor.ProcessEntity : Zone object creation failure in BACnet stack..");
                    return globalAnaloglocationModel.IsSuccess;
                }
                SetProperty(globalAnaloglocationModel.Data.DeviceId, globalAnaloglocationModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropObjectName, AppResource.AnalogValueBlindLocationObjectSubName + " " + locationName);
                SetProperty(globalAnaloglocationModel.Data.DeviceId, globalAnaloglocationModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropDescription, AppResource.AnalogViewRepresentBlindZone, AppConstants.LIGHTING_BRIGHTENESS_OBJECT, processorConfiguration, entityBaseModel);
                SetProperty(globalAnaloglocationModel.Data.DeviceId, globalAnaloglocationModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropUnits, BacnetEngineeringUnits.UnitsPercent);
                SetProperty(globalAnaloglocationModel.Data.DeviceId, globalAnaloglocationModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropRelinquishDefault, AppConstants.RELINQUISH_DEFAULT);
                SetProperty(globalAnaloglocationModel.Data.DeviceId, globalAnaloglocationModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropTimeDelay, processorConfiguration.BACnetConfigurationData.TimeDelay);
                SetProperty(globalAnaloglocationModel.Data.DeviceId, globalAnaloglocationModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropNotificationClass, processorConfiguration.VDNotificationClassInstanceNumber);
                SetProperty(globalAnaloglocationModel.Data.DeviceId, globalAnaloglocationModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropHighLimit, AppConstants.LIGHT_FIXTURE_HIGH_LIMIT);
                SetProperty(globalAnaloglocationModel.Data.DeviceId, globalAnaloglocationModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropProprietaryGridX, zoneEntityModel.ProprietaryGridX);
                SetProperty(globalAnaloglocationModel.Data.DeviceId, globalAnaloglocationModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropProprietaryGridY, zoneEntityModel.ProprietaryGridY);
                SetProperty(globalAnaloglocationModel.Data.DeviceId, globalAnaloglocationModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropNotifyType, processorConfiguration.BACnetConfigurationData.NotifyType);

                //Info: Default "Limit enable" property override with true flag
                SetProperty(globalAnaloglocationModel.Data.DeviceId, globalAnaloglocationModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropLimitEnable, limitEnableBitsModel);
                eventTransitionBitsModel = GetEventTransitionBit(processorConfiguration.ZoneAlarmNotificationSetting, "Blind");
                SetProperty(globalAnaloglocationModel.Data.DeviceId, globalAnaloglocationModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropEventEnable, eventTransitionBitsModel);

                #endregion AnalogValue
            }
            #endregion

            #region Set Zone Level Properties
            string key = EntityHelper.GetObjectDetailsKey(BacnetObjectType.ObjectDevice, AppConstants.LIGHTING_OBJECTDEVICE_OBJECT);

            string deviceName = GetObjectName(processorConfiguration.EntityBACnetCache, entityBaseModel, key, BacnetObjectType.ObjectDevice, deviceId);

            SetProperty(deviceId, deviceId, BacnetObjectType.ObjectDevice, BacnetPropertyID.PropObjectName, deviceName);
            SetProperty(deviceId, deviceId, BacnetObjectType.ObjectDevice, BacnetPropertyID.PropDescription, bacnetConfigurationModel.Description, AppConstants.LIGHTING_OBJECTDEVICE_OBJECT, processorConfiguration, entityBaseModel);
            SetProperty(deviceId, deviceId, BacnetObjectType.ObjectDevice, BacnetPropertyID.PropDayLightSavingsStatus, bacnetConfigurationModel.DaylightSavingStatus);
            SetProperty(deviceId, deviceId, BacnetObjectType.ObjectDevice, BacnetPropertyID.PropUtcOffSet, bacnetConfigurationModel.UTCOffset);
            SetProperty(deviceId, deviceId, BacnetObjectType.ObjectDevice, BacnetPropertyID.PropApduTimesOut, bacnetConfigurationModel.APDUTimeout);
            SetProperty(deviceId, deviceId, BacnetObjectType.ObjectDevice, BacnetPropertyID.PropApduSegmentTimesOut, bacnetConfigurationModel.APDUSegmentTimeout);
            SetProperty(deviceId, deviceId, BacnetObjectType.ObjectDevice, BacnetPropertyID.PropNumberOfApduRetries, bacnetConfigurationModel.APDURetries);
            SetProperty(deviceId, deviceId, BacnetObjectType.ObjectDevice, BacnetPropertyID.PropLocation, processorConfiguration.Location);
            SetProperty(deviceId, deviceId, BacnetObjectType.ObjectDevice, BacnetPropertyID.PropApplicationSoftwareVersion, AppCoreRepo.Instance.ApplicationSoftwareVersion);

            #endregion Set Zone Level Properties
            return true;
        }

        /// <summary>
        /// Verifies the device identifier. Verifies with mapped ids to not duplicate the ids.
        /// </summary>
        /// <param name="processorConfiguration">The processor configuration.</param>
        /// <param name="deviceId">The device identifier.</param>
        /// <returns></returns>
        /// <CreatedBy>Swpnil Vyas</CreatedBy>
        /// <TimeStamp>12-09-2018</TimeStamp>
        private static uint VerifyDeviceId(Models.ProcessorConfigurationModel processorConfiguration, uint deviceId)
        {
            if (processorConfiguration.MappedObjectIds != null && processorConfiguration.MappedObjectIds.ContainsKey(deviceId))
            {
                deviceId = (uint)ObjectIdentifierManager.Instance.GetDeviceId(deviceId);
                deviceId = VerifyDeviceId(processorConfiguration, deviceId);
            }
            return deviceId;
        }

        /// <summary>
        /// Gets the event transition bit.
        /// </summary>
        /// <returns></returns>
        /// <CreatedBy>Swpnil Vyas</CreatedBy>
        /// <TimeStamp>12-09-2018</TimeStamp>
        private EventTransitionBitsModel GetEventTransitionBit(List<ZoneObject> AlarmNotificationSetting, string entity)
        {
            EventTransitionBitsModel eventTransitionBitsModel = new EventTransitionBitsModel();
            if (AlarmNotificationSetting.Any(x => x.Name == entity && x.ZoneValue))
            {
                eventTransitionBitsModel.ToFault = true;
                eventTransitionBitsModel.ToNormal = true;
                eventTransitionBitsModel.ToOffNormal = true;
            }
            else
            {
                eventTransitionBitsModel.ToFault = false;
                eventTransitionBitsModel.ToNormal = false;
                eventTransitionBitsModel.ToOffNormal = false;
            }

            return eventTransitionBitsModel;
        }
    }
}
