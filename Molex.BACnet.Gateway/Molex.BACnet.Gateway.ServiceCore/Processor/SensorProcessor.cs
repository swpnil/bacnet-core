﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              SensorProcessor.cs
///   Description:        This class is used for processing the Entity module to BACnet Stack integration Module
///   Author:             Amol Kulkarni           
///   Date:               06/02/17
///---------------------------------------------------------------------------------------------
#endregion
using Molex.BACnet.Gateway.Log;
using Molex.BACnet.Gateway.ServiceCore.Constants;
using Molex.BACnet.Gateway.ServiceCore.Helpers;
using Molex.BACnet.Gateway.ServiceCore.Models;
using Molex.BACnet.Gateway.ServiceCore.Models.BACnet;
using Molex.BACnet.Gateway.ServiceCore.Models.Entity;
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Molex.BACnet.Gateway.ServiceCore.Processor
{
    /// <summary>
    /// This class is used for processing the Entity module to BACnet Stack integration Module
    /// </summary>
    /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>2/5/2017 10:43 AM</TimeStamp>
    public class SensorProcessor : ProcessorBase
    {
        /// <summary>
        /// Creating objects for SensorEnityModel mapped to Bacnet Stack
        /// </summary>
        /// <param name="entityBaseModel">The entity base model.</param>
        /// <param name="processorConfiguration">The processorConfiguration.</param>
        /// <returns>
        /// bool
        /// </returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>2/5/2017 10:51 AM</TimeStamp>
        public override bool ProcessEntity(Models.Entity.EntityBaseModel entityBaseModel, Models.ProcessorConfigurationModel processorConfiguration)
        {
            EventTransitionBitsModel eventTransitionBitsModel = null;
            LimitEnableBitsModel limitEnableBitsModel = new LimitEnableBitsModel();
            limitEnableBitsModel.HighLimit = true;
            limitEnableBitsModel.LowLimit = true;

            try
            {
                Logger.Instance.Log(LogLevel.DEBUG, "SensorProcessor.ProcessEntity: Processing Sensor " + entityBaseModel.EntityName);

                if (!(entityBaseModel.EntityType == Constants.AppConstants.EntityTypes.Sensor))
                {
                    Logger.Instance.Log(LogLevel.DEBUG, "SensorProcessor.ProcessEntity: Entity model mismatch..Sensor Entity model required. Entity type Available : " + entityBaseModel.EntityType);
                    return false;
                }

                SensorEntityModel sensorEntityModel = (SensorEntityModel)entityBaseModel;
                BacnetObjectType bacnetObjectType = EntityHelper.GetBACnetObjectTypeBySensorType(sensorEntityModel.SensorType);

                if (sensorEntityModel.SensorType == AppConstants.SensorType.NotSupported)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "SensorProcessor.ProcessEntity: Sensor Type not supported. " + sensorEntityModel.EntityName);
                    return false;
                }

                if (processorConfiguration.ZoneAlarmNotificationSetting.Any(x => x.Name == sensorEntityModel.SensorType.ToString() && x.IndividualValue))
                {
                    eventTransitionBitsModel = new EventTransitionBitsModel();
                    eventTransitionBitsModel.ToFault = true;
                    eventTransitionBitsModel.ToNormal = true;
                    eventTransitionBitsModel.ToOffNormal = true;
                }
                else
                {
                    eventTransitionBitsModel = new EventTransitionBitsModel();
                    eventTransitionBitsModel.ToFault = false;
                    eventTransitionBitsModel.ToNormal = false;
                    eventTransitionBitsModel.ToOffNormal = false;
                }

                #region Analog/Binary Input

                ResultModel<CreateObjectResultModel> resultModel = CreateObjectAndCacheData(entityBaseModel, processorConfiguration, bacnetObjectType, AppConstants.LIGHTING_SENSOR_OBJECT, AppConstants.EntityTypes.Sensor);


                if (!resultModel.IsSuccess)
                {
                    Logger.Instance.Log(LogLevel.DEBUG, "SensorProcessor.ProcessEntity: Create object and cache data failed ");
                    return false;
                }

                #region Set Sensor Level Properties

                SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, bacnetObjectType, BacnetPropertyID.PropDeviceType, string.Format("{0} {1}", sensorEntityModel.DeviceType, sensorEntityModel.Role));
                SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, bacnetObjectType, BacnetPropertyID.PropNotificationClass, processorConfiguration.VDNotificationClassInstanceNumber);
                SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, bacnetObjectType, BacnetPropertyID.PropTimeDelay, processorConfiguration.BACnetConfigurationData.TimeDelay);
                SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, bacnetObjectType, BacnetPropertyID.PropProprietaryGridX, sensorEntityModel.ProprietaryGridX);
                SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, bacnetObjectType, BacnetPropertyID.PropProprietaryGridY, sensorEntityModel.ProprietaryGridY);

                SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, bacnetObjectType, BacnetPropertyID.PropEventEnable, eventTransitionBitsModel);

                switch (bacnetObjectType)
                {
                    case BacnetObjectType.ObjectAnalogInput:
                        {
                            BacnetEngineeringUnits unit =EntityHelper.GenerateAnalogObjectUnits(sensorEntityModel.SensorType);
                            Single sensorMinValue = EntityHelper.GetSensorMinValue(sensorEntityModel.SensorType);
                            Single sensorMaxValue = EntityHelper.GetSensorMaxValue(sensorEntityModel.SensorType);
                            Single sensorLowLimit = EntityHelper.GetSensorLowLimit(sensorEntityModel.SensorType);
                            Single sensorHighLimit = EntityHelper.GetSensorHighLimit(sensorEntityModel.SensorType);
                            Single sensorDeadband = EntityHelper.GetSensorDeadband(sensorEntityModel.SensorType);
                            Single sensorCOVIncrement = EntityHelper.GetCOVIncrement(sensorEntityModel.SensorType);

                            SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, bacnetObjectType, BacnetPropertyID.PropDescription, sensorEntityModel.Description, AppConstants.LIGHTING_SENSOR_OBJECT, processorConfiguration, entityBaseModel);
                            SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, bacnetObjectType, BacnetPropertyID.PropUnits, unit);
                            SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, bacnetObjectType, BacnetPropertyID.PropMaxPresValue, sensorMaxValue);
                            SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, bacnetObjectType, BacnetPropertyID.PropMinPresValue, sensorMinValue);
                            SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, bacnetObjectType, BacnetPropertyID.PropHighLimit, sensorHighLimit);
                            SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, bacnetObjectType, BacnetPropertyID.PropLowLimit, sensorLowLimit);
                            SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, bacnetObjectType, BacnetPropertyID.PropDeadBand, sensorDeadband);
                            SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, bacnetObjectType, BacnetPropertyID.PropCOVIncrement, sensorCOVIncrement);
                            SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, bacnetObjectType, BacnetPropertyID.PropNotifyType, processorConfiguration.BACnetConfigurationData.NotifyType);
                            SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, bacnetObjectType, BacnetPropertyID.PropLimitEnable, limitEnableBitsModel);

                            switch (sensorEntityModel.SensorType)
                            {
                                case AppConstants.SensorType.Power:
                                case AppConstants.SensorType.Humidity:
                                case AppConstants.SensorType.Temperature:
                                    SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, bacnetObjectType, BacnetPropertyID.PropResolution, AppConstants.DEFAULT_RESOLUTION_POWER_TEMP_HUMINITY_SENSOR);
                                    break;
                                default:
                                    SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, bacnetObjectType, BacnetPropertyID.PropResolution, AppConstants.DEFAULT_ANALOG_OBJECT_RESOLUTION);
                                    break;
                            }
                        }
                        break;

                    case BacnetObjectType.ObjectBinaryInput:
                        {
                            SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, bacnetObjectType, BacnetPropertyID.PropDescription, sensorEntityModel.Description, AppConstants.LIGHTING_SENSOR_OBJECT, processorConfiguration, entityBaseModel);
                            SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, bacnetObjectType, BacnetPropertyID.PropInactiveText, AppConstants.BINARY_OBJECT_INACTIVE_TEXT);
                            SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, bacnetObjectType, BacnetPropertyID.PropActiveText, AppConstants.BINARY_OBJECT_ACTIVE_TEXT);
                            SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, bacnetObjectType, BacnetPropertyID.PropNotifyType, processorConfiguration.BACnetConfigurationData.NotifyType);
                        }
                        break;
                }

                #endregion Set Sensor Level Properties

                #endregion Analog/Binary Value

                Logger.Instance.Log(LogLevel.DEBUG, "SensorProcessor.ProcessEntity: Processing Fixture Completed");

                return true;
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "SensorProcessor.ProcessEntity exception: " + ex.Message);
                return false;
            }
        }
    }
}
