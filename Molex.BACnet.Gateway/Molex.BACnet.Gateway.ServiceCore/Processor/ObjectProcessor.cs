﻿using Molex.BACnet.Gateway.Log;
using Molex.BACnet.Gateway.ServiceCore.Constants;
using Molex.BACnet.Gateway.ServiceCore.Helpers;
using Molex.BACnet.Gateway.ServiceCore.Helpers.PersistentDataConverters;
using Molex.BACnet.Gateway.ServiceCore.Managers;
using Molex.BACnet.Gateway.ServiceCore.Models;
using Molex.BACnet.Gateway.ServiceCore.Models.Entity;
using Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel;
using Molex.BACnet.Gateway.ServiceCore.Repository;
using Molex.StackDataObjects.APIModels;
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Molex.BACnet.Gateway.ServiceCore.Processor
{
    /// <summary>
    /// 
    /// </summary>
    /// <CreatedBy>hp</CreatedBy>
    /// <TimeStamp>8/1/20173:48 PM</TimeStamp>
    /// <seealso cref="Molex.BACnet.Gateway.ServiceCore.Processor.ProcessorBase" />
    public class ObjectProcessor : ProcessorBase
    {
        /// <summary>
        /// Processes the entity.
        /// </summary>
        /// <param name="entityBaseModel">The entity base model.</param>
        /// <param name="processorConfiguration">The processor configuration.</param>
        /// <returns>
        /// boolean
        /// </returns>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>8/1/20173:50 PM</TimeStamp>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>09-06-201705:39 PM</TimeStamp>
        public override bool ProcessEntity(Models.Entity.EntityBaseModel entityBaseModel, Models.ProcessorConfigurationModel processorConfiguration)
        {
            Dictionary<string, uint> entityKeyDeviceIDMapping;
            Dictionary<string, PersistedModels> updatedPersitedModels;
            UpdatePersistedModel(entityBaseModel, processorConfiguration, out entityKeyDeviceIDMapping, out updatedPersitedModels);

            try
            {
                UpdateDeviceProperties(entityKeyDeviceIDMapping, updatedPersitedModels, processorConfiguration);

                if (processorConfiguration.BACnetConfigurationData.Scheduling)
                {
                    UpdateScheduleObject(updatedPersitedModels);
                }

                UpdateNotificationClass(updatedPersitedModels);

                UpdateTrendLogObject(updatedPersitedModels, processorConfiguration.BACnetConfigurationData);

                UpdateEventEnrollmentObject(updatedPersitedModels, processorConfiguration.BACnetConfigurationData);

                UpdateCalendarObject(updatedPersitedModels);

                return true;
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(LogLevel.ERROR, "ObjectProcessor.ProcessEntity, Failed to update feature object. Error is :" + ex.Message);
                return false;
            }
        }

        #region Create and Update Objects
        private void UpdateCalendarObject(Dictionary<string, PersistedModels> updatedPersitedModels)
        {
            foreach (var device in updatedPersitedModels)
            {
                foreach (var objects in device.Value.Calendars)
                {
                    try
                    {
                        string name = GetName(objects, BacnetObjectType.ObjectCalendar);
                        bool isCreateObjectSuccess = StackManager.Instance.CreateObject(device.Value.DeviceID, BacnetObjectType.ObjectCalendar, objects.ObjectID, name);

                        if (!isCreateObjectSuccess)
                        {
                            Logger.Instance.Log(LogLevel.ERROR, "ObjectProcessor:UpdateCalendarObject:Create calender object fail");
                            return;
                        }

                        EntityBaseModel entityBaseModel = EntityHelper.GetEntityModelFromDeviceID(device.Value.DeviceID);
                        bool isAddObjectSuccess = ObjectIdentifierManager.Instance.AddObjectId(device.Value.DeviceID, BacnetObjectType.ObjectCalendar, (int)objects.ObjectID, entityBaseModel.EntityType);

                        if (!isAddObjectSuccess)
                        {
                            Logger.Instance.Log(LogLevel.ERROR, "fail to add object id in to object identifier list with id: " + objects.ObjectID + ",entity type: " + entityBaseModel.EntityType + ",deviceid: " + device.Value.DeviceID + " for calender object");
                            return;
                        }

                        SetProperty(device.Value.DeviceID, objects.ObjectID, BacnetObjectType.ObjectCalendar, BacnetPropertyID.PropDescription, objects.Description, null, null, null);

                        List<BacnetCalendarEntryModel> listOfBacnetCalendarEntryModel = new List<BacnetCalendarEntryModel>();
                        foreach (var dateModel in objects.DateList)
                        {
                            BacnetCalendarEntryModel model = CalendarEntryModelConverter.ConvertToBacnetCalendarEntryModel(dateModel);
                            listOfBacnetCalendarEntryModel.Add(model);
                        }

                        SetProperty(device.Value.DeviceID, objects.ObjectID, BacnetObjectType.ObjectCalendar, BacnetPropertyID.PropDateList, listOfBacnetCalendarEntryModel);
                    }
                    catch (Exception e)
                    {
                        Logger.Instance.Log(LogLevel.ERROR, "ObjectProcessor.UpdateCalendarObject, Failed to set property for calendar object.");
                    }
                }
            }
        }

        private void UpdateEventEnrollmentObject(Dictionary<string, PersistedModels> updatedPersitedModels, BACnetConfigurationModel configurationModel)
        {
            foreach (var device in updatedPersitedModels)
            {
                foreach (var objects in device.Value.EventEnrollments)
                {
                    try
                    {
                        string name = GetName(objects, BacnetObjectType.ObjectEventEnrollment);
                        bool isCreateObjectSuccess = StackManager.Instance.CreateObject(device.Value.DeviceID, BacnetObjectType.ObjectEventEnrollment, objects.ObjectID, name);

                        if (!isCreateObjectSuccess)
                        {
                            Logger.Instance.Log(LogLevel.ERROR, "ObjectProcessor:UpdateEventEnrollmentObject:Create event enrollment object fail");
                            return;
                        }

                        EntityBaseModel entityBaseModel = EntityHelper.GetEntityModelFromDeviceID(device.Value.DeviceID);
                        bool isAddObjectSuccess = ObjectIdentifierManager.Instance.AddObjectId(device.Value.DeviceID, BacnetObjectType.ObjectEventEnrollment, (int)objects.ObjectID, entityBaseModel.EntityType);

                        if (!isAddObjectSuccess)
                        {
                            Logger.Instance.Log(LogLevel.ERROR, "fail to add object id in to object identifier list with id: " + objects.ObjectID + ",entity type: " + entityBaseModel.EntityType + ",deviceid: " + device.Value.DeviceID + " for event enrollment object");
                            return;
                        }

                        SetProperty(device.Value.DeviceID, objects.ObjectID, BacnetObjectType.ObjectEventEnrollment, BacnetPropertyID.PropDescription, objects.Description, null, null, null);

                        BACnetDevObjPropRefModel devObjPropRefModel = DevObjPropRefModelConverter.ConvertToBacnetDevObjPropRefModel(objects.ObjectPropertyReference);
                        SetProperty(device.Value.DeviceID, objects.ObjectID, BacnetObjectType.ObjectEventEnrollment, BacnetPropertyID.PropObjectPropertyReference, devObjPropRefModel);

                        SetProperty(device.Value.DeviceID, objects.ObjectID, BacnetObjectType.ObjectEventEnrollment, BacnetPropertyID.PropNotifyType, objects.NotificationType);

                        SetProperty(device.Value.DeviceID, objects.ObjectID, BacnetObjectType.ObjectEventEnrollment, BacnetPropertyID.PropNotificationClass, objects.NotificationClass);

                        switch (objects.EventType)
                        {
                            case BacnetEventType.EventChangeOfState:
                                {
                                    ChangeOfStateEventParametersModel changeOfState = ChangeOfStateConverter.ConvertToBacnetChangeOfStateModel(objects.ChangeOfStateEventModel);
                                    SetProperty(device.Value.DeviceID, objects.ObjectID, BacnetObjectType.ObjectEventEnrollment, BacnetPropertyID.PropEventParameters, changeOfState);
                                }
                                break;

                            case BacnetEventType.EventCommandFailure:
                                {
                                    CommandFailureEventParametersModel commandFailure = CommandFailureModelConverter.ConvertToBacnetCommandFailureModel(objects.CommandFailureModel);
                                    SetProperty(device.Value.DeviceID, objects.ObjectID, BacnetObjectType.ObjectEventEnrollment, BacnetPropertyID.PropEventParameters, commandFailure);
                                }
                                break;

                            case BacnetEventType.EventOutOfRange:
                                {
                                    OutOfRangeEventParametersModel outOfRange = OutOfRangeModelConverter.ConvertToBacnetOutOfRangeModel(objects.OutOfRangeModel);
                                    SetProperty(device.Value.DeviceID, objects.ObjectID, BacnetObjectType.ObjectEventEnrollment, BacnetPropertyID.PropEventParameters, outOfRange);
                                }
                                break;
                        }

                        EntityBaseModel entityModel = EntityHelper.GetEntityModelFromDeviceID(device.Value.DeviceID);

                        EventTransitionBitsModel model = EventTransitionModelConverter.ConvertToBacnetEventTransitionModel(objects.EventEnable);
                        if (objects.EventEnable == null)
                        {
                            model = GetBitsFromBCT(configurationModel, entityModel, BacnetObjectType.ObjectEventEnrollment);
                        }
                        else
                        {
                            model = EventTransitionModelConverter.ConvertToBacnetEventTransitionModel(objects.EventEnable);
                        }

                        SetProperty(device.Value.DeviceID, objects.ObjectID, BacnetObjectType.ObjectEventEnrollment, BacnetPropertyID.PropEventEnable, model);
                        UpdateNotificationPropertyValue(objects, entityModel, BacnetObjectType.ObjectEventEnrollment);
                    }
                    catch (Exception)
                    {
                        Logger.Instance.Log(LogLevel.ERROR, "ObjectProcessor.UpdateEventEnrollmentObject, Failed to set property for Event enrollment object .");
                    }


                }
            }
        }

        private void UpdateTrendLogObject(Dictionary<string, PersistedModels> updatedPersitedModels, BACnetConfigurationModel configurationModel)
        {
            foreach (var device in updatedPersitedModels)
            {
                foreach (var objects in device.Value.TrendLogs)
                {
                    try
                    {
                        string name = GetName(objects, BacnetObjectType.ObjectTrendlog);

                        bool isCreateObjectSuccess = StackManager.Instance.CreateObject(device.Value.DeviceID, BacnetObjectType.ObjectTrendlog, objects.ObjectID, name);

                        if (!isCreateObjectSuccess)
                        {
                            Logger.Instance.Log(LogLevel.ERROR, "ObjectProcessor:UpdateTrendLogObject:Create trendlog object fail");
                            return;
                        }

                        EntityBaseModel entityBaseModel = EntityHelper.GetEntityModelFromDeviceID(device.Value.DeviceID);
                        bool isAddObjectSuccess = ObjectIdentifierManager.Instance.AddObjectId(device.Value.DeviceID, BacnetObjectType.ObjectTrendlog, (int)objects.ObjectID, entityBaseModel.EntityType);

                        if (!isAddObjectSuccess)
                        {
                            Logger.Instance.Log(LogLevel.ERROR, "fail to add object id in to object identifier list with id: " + objects.ObjectID + ",entity type: " + entityBaseModel.EntityType + ",deviceid: " + device.Value.DeviceID + " for trendlog object");
                            return;
                        }

                        SetProperty(device.Value.DeviceID, objects.ObjectID, BacnetObjectType.ObjectTrendlog, BacnetPropertyID.PropDescription, objects.Description, null, null, null);

                        SetProperty(device.Value.DeviceID, objects.ObjectID, BacnetObjectType.ObjectTrendlog, BacnetPropertyID.PropEnable, objects.Enable);
                        SetProperty(device.Value.DeviceID, objects.ObjectID, BacnetObjectType.ObjectTrendlog, BacnetPropertyID.PropStopWhenFull, objects.StopWhenFull);
                        SetProperty(device.Value.DeviceID, objects.ObjectID, BacnetObjectType.ObjectTrendlog, BacnetPropertyID.PropLogInterval, objects.LogInterval);

                        BacnetDateTimeModel startTime = DateTimeModelConverter.ConvertToBacnetDateTimeModel(objects.StartTime);
                        SetProperty(device.Value.DeviceID, objects.ObjectID, BacnetObjectType.ObjectTrendlog, BacnetPropertyID.PropStartTime, startTime);

                        BacnetDateTimeModel stopTime = DateTimeModelConverter.ConvertToBacnetDateTimeModel(objects.StopTime);
                        SetProperty(device.Value.DeviceID, objects.ObjectID, BacnetObjectType.ObjectTrendlog, BacnetPropertyID.PropStopTime, stopTime);

                        BACnetDevObjPropRefModel devObjPropRefModel = DevObjPropRefModelConverter.ConvertToBacnetDevObjPropRefModel(objects.LogDeviceObject);
                        SetProperty(device.Value.DeviceID, objects.ObjectID, BacnetObjectType.ObjectTrendlog, BacnetPropertyID.PropLogDeviceObjectProperty, devObjPropRefModel);

                        EntityBaseModel entityModel = EntityHelper.GetEntityModelFromDeviceID(device.Value.DeviceID);

                        EventTransitionBitsModel model = GetBitsFromBCT(configurationModel, entityModel, BacnetObjectType.ObjectTrendlog);
                        SetProperty(device.Value.DeviceID, objects.ObjectID, BacnetObjectType.ObjectTrendlog, BacnetPropertyID.PropEventEnable, model);

                        UpdateNotificationPropertyValue(objects, entityModel, BacnetObjectType.ObjectTrendlog);
                    }
                    catch (Exception)
                    {
                        Logger.Instance.Log(LogLevel.ERROR, "ObjectProcessor.UpdateTrendLogObject, Failed to set property for trend log object .");
                    }
                }
            }
        }

        private void UpdateNotificationClass(Dictionary<string, PersistedModels> updatedPersitedModels)
        {
            foreach (var device in updatedPersitedModels)
            {
                foreach (var objects in device.Value.NotificationClasses)
                {
                    try
                    {
                        EntityBaseModel entityBaseModel = EntityHelper.GetEntityModelFromDeviceID(device.Value.DeviceID);
                        string name = GetName(objects, BacnetObjectType.ObjectNotificationClass);

                        string key = EntityHelper.GetObjectDetailsKey(BacnetObjectType.ObjectNotificationClass, AppConstants.LIGHTING_NOTIFICATIONCLASS_OBJECT);

                        if (entityBaseModel.BACnetData.ObjectDetails[key].ObjectID != objects.ObjectID)
                        {
                            bool isCreateObjectSuccess = StackManager.Instance.CreateObject(device.Value.DeviceID, BacnetObjectType.ObjectNotificationClass, objects.ObjectID, name);

                            if (!isCreateObjectSuccess)
                            {
                                Logger.Instance.Log(LogLevel.ERROR, "ObjectProcessor:UpdateNotificationClass:Create notification class object fail");
                                return;
                            }

                            bool isAddObjectSuccess = ObjectIdentifierManager.Instance.AddObjectId(device.Value.DeviceID, BacnetObjectType.ObjectNotificationClass, (int)objects.ObjectID, entityBaseModel.EntityType);

                            if (!isAddObjectSuccess)
                            {
                                Logger.Instance.Log(LogLevel.ERROR, "fail to add object id in to object identifier list with id: " + objects.ObjectID + ",entity type: " + entityBaseModel.EntityType + ",deviceid: " + device.Value.DeviceID + " for notification class object");
                                return;
                            }
                        }

                        SetProperty(device.Value.DeviceID, objects.ObjectID, BacnetObjectType.ObjectNotificationClass, BacnetPropertyID.PropDescription, objects.Description, null, null, null);
                        StackManager.SetPropertyAccessType(device.Value.DeviceID, objects.ObjectID, BacnetObjectType.ObjectNotificationClass, BacnetPropertyID.PropPriority, PropAccessType.READ_WRITE, true);
                        List<BacnetDestinationModel> bacnetDestinationModels = new List<BacnetDestinationModel>();
                        foreach (var destinationModel in objects.RecipientList)
                        {
                            bacnetDestinationModels.Add(DestinationTypeConverter.ConvertToBacnetDestinationModel(destinationModel));
                        }

                        if (objects.RecipientList.Count > 0)
                            SetProperty(device.Value.DeviceID, objects.ObjectID, BacnetObjectType.ObjectNotificationClass, BacnetPropertyID.PropRecipientList, bacnetDestinationModels);

                        EventTransitionBitsModel bacnetModel = EventTransitionModelConverter.ConvertToBacnetEventTransitionModel(objects.AckRequired);
                        SetProperty(device.Value.DeviceID, objects.ObjectID, BacnetObjectType.ObjectNotificationClass, BacnetPropertyID.PropAckRequired, bacnetModel);

                        uint[] priorityListArray = null;
                        priorityListArray = new uint[3];
                        if (objects == null)
                            continue;
                        if (objects.NotificationPriority == null)
                            continue;
                        if (objects.NotificationPriority.Count != 3)
                            continue;
                        priorityListArray[2] = objects.NotificationPriority[2];
                        priorityListArray[1] = objects.NotificationPriority[1];
                        priorityListArray[0] = objects.NotificationPriority[0];
                        SetProperty(device.Value.DeviceID, objects.ObjectID, BacnetObjectType.ObjectNotificationClass, BacnetPropertyID.PropPriority, priorityListArray);
                    }
                    catch (Exception)
                    {
                        Logger.Instance.Log(LogLevel.ERROR, "ObjectProcessor.UpdateNotificationClass, Failed to set property for notification object .");
                    }
                }
            }
        }

        private void UpdateScheduleObject(Dictionary<string, PersistedModels> updatedPersitedModels)
        {
            foreach (var device in updatedPersitedModels)
            {
                foreach (var objects in device.Value.Schedules)
                {
                    try
                    {
                        string name = GetName(objects, BacnetObjectType.ObjectSchedule);
                        bool isCreateObjectSuccess = StackManager.Instance.CreateObject(device.Value.DeviceID, BacnetObjectType.ObjectSchedule, objects.ObjectID, name);

                        if (!isCreateObjectSuccess)
                        {
                            Logger.Instance.Log(LogLevel.ERROR, "ObjectProcessor:UpdateNotificationClass:Create schedule object fail");
                            return;
                        }

                        EntityBaseModel entityBaseModel = EntityHelper.GetEntityModelFromDeviceID(device.Value.DeviceID);
                        bool isAddObjectSuccess = ObjectIdentifierManager.Instance.AddObjectId(device.Value.DeviceID, BacnetObjectType.ObjectSchedule, (int)objects.ObjectID, entityBaseModel.EntityType);

                        if (!isAddObjectSuccess)
                        {
                            Logger.Instance.Log(LogLevel.ERROR, "fail to add object id in to object identifier list with id: " + objects.ObjectID + ",entity type: " + entityBaseModel.EntityType + ",deviceid: " + device.Value.DeviceID + " for schedule object");
                            return;
                        }

                        SetProperty(device.Value.DeviceID, objects.ObjectID, BacnetObjectType.ObjectSchedule, BacnetPropertyID.PropPriorityForWriting, objects.PriorityForWriting);

                        SetProperty(device.Value.DeviceID, objects.ObjectID, BacnetObjectType.ObjectSchedule, BacnetPropertyID.PropDescription, objects.Description, null, null, null);

                        BacnetDateRangeModel model = DateRangeConverter.ConvertToBacnetDateRangeModel(objects.EffectivePeriod);
                        SetProperty(device.Value.DeviceID, objects.ObjectID, BacnetObjectType.ObjectSchedule, BacnetPropertyID.PropEffectivePeriod, model);

                        BacnetValueModel valueModel = ValueModelConverter.ConvertToBacnetValueModel(objects.ScheduleDefault);
                        SetProperty(device.Value.DeviceID, objects.ObjectID, BacnetObjectType.ObjectSchedule, BacnetPropertyID.PropScheduleDefault, valueModel);

                        List<BacnetTimeValueArrayModel> bacnetTimeValues = new List<BacnetTimeValueArrayModel>();
                        foreach (var schedule in objects.WeeklySchedule)
                        {
                            bacnetTimeValues.Add(TimeValueArrayModelConverter.ConvertToBacnetTimeValueArrayModel(schedule));
                        }
                        SetProperty(device.Value.DeviceID, objects.ObjectID, BacnetObjectType.ObjectSchedule, BacnetPropertyID.PropWeeklySchedule, bacnetTimeValues);

                        List<BacnetSpecialEventModel> bacnetSpecialEvents = new List<BacnetSpecialEventModel>();
                        foreach (var specialEvents in objects.ExceptionSchedule)
                        {
                            bacnetSpecialEvents.Add(SpecialEventModelConverter.ConvertBacnetSpecialEventModel(specialEvents));
                        }
                        SetProperty(device.Value.DeviceID, objects.ObjectID, BacnetObjectType.ObjectSchedule, BacnetPropertyID.PropExceptionSchedule, bacnetSpecialEvents);

                        List<BACnetDevObjPropRefModel> bacnetDevObjPropRef = new List<BACnetDevObjPropRefModel>();
                        foreach (var objectReferences in objects.ObjectPropertyRefernces)
                        {
                            bacnetDevObjPropRef.Add(DevObjPropRefModelConverter.ConvertToBacnetDevObjPropRefModel(objectReferences));
                            AppCoreRepo.Instance.ScheduledObjects.Add(objects.ObjectID, CommonHelper.GenerateScheduleObjectMappedID((uint)device.Value.DeviceID, objectReferences.ObjectType, objectReferences.PropertyIdentifier, objectReferences.ObjId));
                        }

                        SetProperty(device.Value.DeviceID, objects.ObjectID, BacnetObjectType.ObjectSchedule, BacnetPropertyID.PropListOfObjectPropertyReferences, bacnetDevObjPropRef);
                    }
                    catch (Exception)
                    {
                        Logger.Instance.Log(LogLevel.ERROR, "ObjectProcessor.UpdateScheduleObject, Failed to set property for Schedule object .");
                    }
                }
            }
        }

        private void UpdateDeviceProperties(Dictionary<string, uint> entityKeyDeviceIDMapping, Dictionary<string, PersistedModels> updatedPersitedModels, Models.ProcessorConfigurationModel processorConfiguration)
        {
            try
            {
                foreach (var item in updatedPersitedModels)
                {
                    if (entityKeyDeviceIDMapping.ContainsKey(item.Key))
                    {
                        uint deviceId = entityKeyDeviceIDMapping[item.Key];
                        SetProperty(deviceId, deviceId, BacnetObjectType.ObjectDevice, BacnetPropertyID.PropApduTimesOut, item.Value.ObjectDevice.APDUTimeout != null ? item.Value.ObjectDevice.APDUTimeout.Value : (int)processorConfiguration.BACnetConfigurationData.APDUTimeout);
                        SetProperty(deviceId, deviceId, BacnetObjectType.ObjectDevice, BacnetPropertyID.PropNumberOfApduRetries, item.Value.ObjectDevice.NumberOfAPDURetries != null ? item.Value.ObjectDevice.NumberOfAPDURetries.Value : (int)processorConfiguration.BACnetConfigurationData.APDURetries);
                        SetProperty(deviceId, deviceId, BacnetObjectType.ObjectDevice, BacnetPropertyID.PropApduSegmentTimesOut, item.Value.ObjectDevice.APDUSegmentTimeOut != null ? item.Value.ObjectDevice.APDUSegmentTimeOut.Value : (int)processorConfiguration.BACnetConfigurationData.APDUSegmentTimeout);
                    }
                }
            }
            catch (Exception)
            {
                Logger.Instance.Log(LogLevel.ERROR, "ObjectProcessor.UpdateDeviceProperties, error in setting properties.");
            }
        }

        private void UpdatePersistedModel(Models.Entity.EntityBaseModel entityBaseModel, Models.ProcessorConfigurationModel processorConfiguration, out Dictionary<string, uint> entityKeyDeviceIDMapping, out Dictionary<string, PersistedModels> updatedPersitedModels)
        {
            entityKeyDeviceIDMapping = ObjectMappingHelper.GetPersistenceEntityKeyDeviceIDMapping(processorConfiguration.UserBACnetObjects);
            //creating flat struture with entitykeys and device ids from entity base model.
            Dictionary<string, uint> flatFormatEntityKeyDeviceID = GetEntityKeyDeviceIDPair(entityBaseModel);

            //update processorConfiguration persistence cache based on the above key value pairs
            //this ensures that the association with entity key and device id is in sync with.
            UpdatePersistenceCache(flatFormatEntityKeyDeviceID, entityKeyDeviceIDMapping);

            updatedPersitedModels = ObjectMappingHelper.GetUpdatePersistentModels(entityKeyDeviceIDMapping, processorConfiguration.UserBACnetObjects);
        }

        private void UpdatePersistenceCache(Dictionary<string, uint> entityKeyDeviceID, Dictionary<string, uint> storedEntityKeyDeviceID)
        {
            Dictionary<string, uint> values = new Dictionary<string, uint>(storedEntityKeyDeviceID);
            foreach (var item in values)
            {
                if (entityKeyDeviceID.ContainsKey(item.Key))
                {
                    if (entityKeyDeviceID[item.Key] != item.Value)
                        storedEntityKeyDeviceID[item.Key] = entityKeyDeviceID[item.Key];
                }
                else
                {
                    storedEntityKeyDeviceID.Remove(item.Key);
                }
            }
        }

        private Dictionary<string, uint> GetEntityKeyDeviceIDPair(Models.Entity.EntityBaseModel entityBaseModel)
        {
            Dictionary<string, uint> values = new Dictionary<string, uint>();

            values[entityBaseModel.MappingKey] = (uint)entityBaseModel.BACnetData.DeviceID;

            GetValues(entityBaseModel, values);

            return values;
        }

        private void GetValues(Models.Entity.EntityBaseModel entityBaseModel, Dictionary<string, uint> values)
        {
            foreach (var entityModel in entityBaseModel.Childs)
            {
                if (entityModel.EntityType == AppConstants.EntityTypes.Zone)
                    values[entityModel.MappingKey] = (uint)entityModel.BACnetData.DeviceID;

                GetValues(entityModel, values);
            }
        }

        #endregion

        /// <summary>
        /// Sets the poperties.
        /// </summary>
        /// <param name="deviceId">The device identifier.</param>
        /// <param name="objectId">The object identifier.</param>
        /// <param name="bacnetObjectType">Type of the bacnet object.</param>
        /// <param name="bacnetPropertyID">The bacnet property identifier.</param>
        /// <param name="propertyValue">The property value.</param>
        /// <param name="lightingPropertyValue">The lighting property value.</param>
        /// <param name="processorConfigurationModel">The processor configuration.</param>
        /// <param name="entityBaseModel">The entity base model.</param>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>8/23/201712:11 PM</TimeStamp>
        /// <CreatedBy>prasad.joshi</CreatedBy><TimeStamp>5/30/201710:33 AM</TimeStamp>
        protected override void SetProperty(uint deviceId, uint objectId, BacnetObjectType bacnetObjectType, BacnetPropertyID bacnetPropertyID, object propertyValue, string lightingPropertyValue, ProcessorConfigurationModel processorConfigurationModel, Models.Entity.EntityBaseModel entityBaseModel)
        {
            bool isSuccess = false;
            try
            {
                if (propertyValue == null)
                {
                    if (bacnetPropertyID == BacnetPropertyID.PropDescription)
                    {
                        propertyValue = CommonHelper.GetDescription(bacnetObjectType);
                    }
                    else
                        return;
                }

                isSuccess = StackManager.Instance.WriteProperty(deviceId, bacnetObjectType, objectId, propertyValue, bacnetPropertyID);
                if (!isSuccess)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "ProcessorBase:SetPoperties: Is Success " + isSuccess + ", for " + bacnetObjectType);
                }
            }
            catch (Exception e)
            {
                Logger.Instance.Log(e, LogLevel.ERROR, "ProcessorBase:SetPoperties: Failed for " + bacnetObjectType);
            }
        }


        private void UpdateNotificationPropertyValue(PersistentBase objects, EntityBaseModel entityModel, BacnetObjectType type)
        {
            string objectDetailsKey = EntityHelper.GetObjectDetailsKey(BacnetObjectType.ObjectNotificationClass, AppConstants.LIGHTING_NOTIFICATIONCLASS_OBJECT);
            uint notificationObjectID = entityModel.BACnetData.ObjectDetails[objectDetailsKey].ObjectID;

            StackManager.Instance.WriteProperty((uint)entityModel.BACnetData.DeviceID, type,
                                       (uint)objects.ObjectID, notificationObjectID, BacnetPropertyID.PropNotificationClass);
        }

        private EventTransitionBitsModel GetBitsFromBCT(BACnetConfigurationModel processorConfiguration, EntityBaseModel entityBaseModel, BacnetObjectType bacnetObjectType)
        {
            EventTransitionBitsModel model = new EventTransitionBitsModel();
            switch (entityBaseModel.EntityType)
            {
                case AppConstants.EntityTypes.Controller:
                    if (AppCoreRepo.Instance.BACnetConfigurationData.IsBuildingAlarm)
                    {
                        model.ToFault = true;
                        model.ToNormal = true;
                        model.ToOffNormal = true;
                    }
                    break;

                case AppConstants.EntityTypes.Zone:
                    string zoneType = string.Empty;
                    switch (((ZoneEntityModel)entityBaseModel).ZoneType)
                    {
                        case AppConstants.ZoneTypes.BeaconSpace:
                            zoneType = "Beacon Zone";
                            break;
                        case AppConstants.ZoneTypes.UserSpace:
                            zoneType = "User Zone";
                            break;
                        case AppConstants.ZoneTypes.GeneralSpace:
                            zoneType = "General Zone";
                            break;
                    }
                    string objectName = bacnetObjectType == BacnetObjectType.ObjectEventEnrollment ? AppConstants.LIGHTING_EVENTENROLLEMENT_OBJECT : AppConstants.LIGHTING_TRENDLOG_OBJECT;
                    if (processorConfiguration.AlarmNotificationSetting != null && processorConfiguration.AlarmNotificationSetting[zoneType].Any(x => x.Name == objectName && x.ZoneValue))
                    {
                        model.ToFault = true;
                        model.ToNormal = true;
                        model.ToOffNormal = true;
                    }
                    break;
            }
            return model;
        }

        private string GetName(PersistentBase objects, BacnetObjectType bacnetObjectType)
        {
            if (objects.Name == null)
            {
                return CommonHelper.GetObjectName(bacnetObjectType);
            }
            else
            {
                return objects.Name;
            }
        }
    }
}
