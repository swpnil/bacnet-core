﻿using Molex.BACnet.Gateway.Log;
using Molex.BACnet.Gateway.ServiceCore.AppResources;
using Molex.BACnet.Gateway.ServiceCore.Constants;
using Molex.BACnet.Gateway.ServiceCore.Helpers;
using Molex.BACnet.Gateway.ServiceCore.Models;
using Molex.BACnet.Gateway.ServiceCore.Models.BACnet;
using Molex.BACnet.Gateway.ServiceCore.Models.Entity;
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Models;
using System;
using System.Linq;

/// <summary>
/// Base class of zones
/// </summary>
/// <CreatedBy>[ERROR: invalid expression Environment.UserName]</CreatedBy><TimeStamp>[ERROR: invalid expression Environment.Date][ERROR: invalid expression Environment.Time]</TimeStamp>
namespace Molex.BACnet.Gateway.ServiceCore.Processor
{
    /// <summary>
    /// Base class zone processor
    /// </summary>
    /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/16/201811:01 AM</TimeStamp>
    /// <seealso cref="Molex.BACnet.Gateway.ServiceCore.Processor.ProcessorBase" />
    public class ZoneBaseProcessor : ProcessorBase
    {
        #region Internal methods
        /// <summary>
        /// Processes the zone average sensor entity.
        /// </summary>
        /// <param name="entityBaseModel">The entity base model.</param>
        /// <param name="processorConfiguration">The processor configuration.</param>
        /// <param name="eventTransitionBitsModel">The event transition bits model.</param>
        /// <param name="sensorType">Type of the sensor.</param>
        /// <param name="initialAvgSensorObjectID">The initial average sensor object identifier.</param>
        /// <returns></returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/16/201811:01 AM</TimeStamp>
        internal bool ProcessZoneAverageSensorEntity(Models.Entity.EntityBaseModel entityBaseModel, Models.ProcessorConfigurationModel processorConfiguration, EventTransitionBitsModel eventTransitionBitsModel, AppConstants.SensorType sensorType, int initialAvgSensorObjectID)
        {
            LimitEnableBitsModel limitEnableBitsModel = new LimitEnableBitsModel();
            limitEnableBitsModel.HighLimit = true;
            limitEnableBitsModel.LowLimit = true;

            try
            {
                Logger.Instance.Log(LogLevel.DEBUG, "SensorProcessor.ProcessEntity: Processing Sensor " + entityBaseModel.EntityName);

                ZoneEntityModel zoneEntityModel = (ZoneEntityModel)entityBaseModel;
                BacnetObjectType bacnetObjectType = EntityHelper.GetBACnetObjectTypeBySensorType(sensorType);

                if (sensorType == AppConstants.SensorType.NotSupported)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "SensorProcessor.ProcessZoneAverageSensorEntity: Sensor Type not supported. " + zoneEntityModel.EntityName);
                    return false;
                }

                #region Analog/Binary Input

                ResultModel<CreateObjectResultModel> resultModel = CreateObjectAndCacheData(entityBaseModel, processorConfiguration, bacnetObjectType, sensorType.ToString(), AppConstants.EntityTypes.Zone, initialAvgSensorObjectID);


                if (!resultModel.IsSuccess)
                {
                    Logger.Instance.Log(LogLevel.DEBUG, "SensorProcessor.ProcessEntity: Create object and cache data failed ");
                    return false;
                }

                #region Set Sensor Level Properties
                string deviceType = GetDeviceType((ZoneEntityModel)entityBaseModel, sensorType);
                SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, bacnetObjectType, BacnetPropertyID.PropDeviceType, deviceType);
                SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, bacnetObjectType, BacnetPropertyID.PropNotificationClass, processorConfiguration.VDNotificationClassInstanceNumber);
                SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, bacnetObjectType, BacnetPropertyID.PropTimeDelay, processorConfiguration.BACnetConfigurationData.TimeDelay);
                SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, bacnetObjectType, BacnetPropertyID.PropProprietaryGridX, zoneEntityModel.ProprietaryGridX);
                SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, bacnetObjectType, BacnetPropertyID.PropProprietaryGridY, zoneEntityModel.ProprietaryGridY);


                if (eventTransitionBitsModel != null)
                {
                    SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, bacnetObjectType, BacnetPropertyID.PropEventEnable, eventTransitionBitsModel);
                }


                switch (bacnetObjectType)
                {
                    case BacnetObjectType.ObjectAnalogInput:
                        {
                            BacnetEngineeringUnits unit = EntityHelper.GenerateAnalogObjectUnits(sensorType);
                            Single sensorMinValue = EntityHelper.GetSensorMinValue(sensorType);
                            Single sensorMaxValue = EntityHelper.GetSensorMaxValue(sensorType);
                            Single sensorLowLimit = EntityHelper.GetSensorLowLimit(sensorType);
                            Single sensorHighLimit = EntityHelper.GetSensorHighLimit(sensorType);
                            Single sensorDeadband = EntityHelper.GetSensorDeadband(sensorType);
                            Single sensorCOVIncrement = EntityHelper.GetCOVIncrement(sensorType);

                            if (sensorType == AppConstants.SensorType.Power)
                            {
                                //max value of individual power sensor multiply by max allowed sensor in zone
                                sensorMaxValue = sensorMaxValue * 2500;
                                SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, bacnetObjectType, BacnetPropertyID.PropDescription, AppResource.AnalogInputPowerObjectRepresent, AppConstants.LIGHTING_SENSOR_OBJECT, processorConfiguration, entityBaseModel);
                            }
                            else
                            {
                                SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, bacnetObjectType, BacnetPropertyID.PropDescription, string.Format(AppResource.AnalogInputObjectRepresent, sensorType), AppConstants.LIGHTING_SENSOR_OBJECT, processorConfiguration, entityBaseModel);
                            }
                            SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, bacnetObjectType, BacnetPropertyID.PropUnits, unit);
                            SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, bacnetObjectType, BacnetPropertyID.PropMinPresValue, sensorMinValue);
                            SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, bacnetObjectType, BacnetPropertyID.PropMaxPresValue, sensorMaxValue);
                            SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, bacnetObjectType, BacnetPropertyID.PropHighLimit, sensorHighLimit);
                            SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, bacnetObjectType, BacnetPropertyID.PropLowLimit, sensorLowLimit);
                            SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, bacnetObjectType, BacnetPropertyID.PropDeadBand, sensorDeadband);
                            SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, bacnetObjectType, BacnetPropertyID.PropCOVIncrement, sensorCOVIncrement);
                            SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, bacnetObjectType, BacnetPropertyID.PropNotifyType, processorConfiguration.BACnetConfigurationData.NotifyType);

                            SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, bacnetObjectType, BacnetPropertyID.PropLimitEnable, limitEnableBitsModel);

                            switch (sensorType)
                            {
                                case AppConstants.SensorType.Power:
                                case AppConstants.SensorType.Humidity:
                                case AppConstants.SensorType.Temperature:
                                    SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, bacnetObjectType, BacnetPropertyID.PropResolution, AppConstants.DEFAULT_RESOLUTION_POWER_TEMP_HUMINITY_SENSOR);
                                    break;
                                default:
                                    SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, bacnetObjectType, BacnetPropertyID.PropResolution, AppConstants.DEFAULT_ANALOG_OBJECT_RESOLUTION);
                                    break;
                            }
                        }
                        break;

                    case BacnetObjectType.ObjectBinaryInput:
                        {
                            SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, bacnetObjectType, BacnetPropertyID.PropDescription, AppResource.BinaryInputObjectRepresent, AppConstants.LIGHTING_SENSOR_OBJECT, processorConfiguration, entityBaseModel);
                            SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, bacnetObjectType, BacnetPropertyID.PropInactiveText, AppConstants.BINARY_OBJECT_INACTIVE_TEXT);
                            SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, bacnetObjectType, BacnetPropertyID.PropActiveText, AppConstants.BINARY_OBJECT_ACTIVE_TEXT);
                            SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, bacnetObjectType, BacnetPropertyID.PropNotifyType, processorConfiguration.BACnetConfigurationData.NotifyType);
                        }
                        break;
                }

                #endregion Set Sensor Level Properties

                #endregion Analog/Binary Value

                Logger.Instance.Log(LogLevel.DEBUG, "SensorProcessor.ProcessEntity: Processing Fixture Completed");

                return true;
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "SensorProcessor.ProcessEntity exception: " + ex.Message);
                return false;
            }
        }
        #endregion

        #region privatemethods
        /// <summary>
        /// Gets the type of the device.
        /// </summary>
        /// <param name="entityBaseModel">The entity base model.</param>
        /// <param name="sensorType">Type of the sensor.</param>
        /// <returns></returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/16/201811:03 AM</TimeStamp>
        private string GetDeviceType(ZoneEntityModel entityBaseModel, AppConstants.SensorType sensorType)
        {
            SensorEntityModel sensorModel = entityBaseModel.Sensors.Where(x => x.SensorType == sensorType).FirstOrDefault();
            return string.Format("{0} {1}", sensorModel.DeviceType, sensorModel.Role);
        }

        /// <summary>
        /// This Method will return the Zone Type like 'General' , 'User' , 'Beacon' & 'Blind' for appending it to Name or Description Property
        /// </summary>
        /// <param name="zoneModel"></param>
        /// <returns></returns>
        protected string GetZoneTypeKey(EntityBaseModel zoneModel)
        {
            try
            {
                if (zoneModel == null)
                    return string.Empty;
                if ((zoneModel as ZoneEntityModel) == null)
                    return string.Empty;
                switch ((zoneModel as ZoneEntityModel).ZoneType)
                {
                    case AppConstants.ZoneTypes.UserSpace:
                        return "User ";
                    case AppConstants.ZoneTypes.BeaconSpace:
                        return "Beacon ";
                    case AppConstants.ZoneTypes.BlindSpace:
                        return "Blind ";
                    case AppConstants.ZoneTypes.GeneralSpace:
                        return "General ";
                    default:
                        return string.Empty;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "ZoneBaseProcesser.GetZoneTypeKey exception: " + ex.Message);
                return string.Empty;
            }
        }
        #endregion privatemethods

    }
}
