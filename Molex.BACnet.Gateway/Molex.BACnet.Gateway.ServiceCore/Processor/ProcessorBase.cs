﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              ProcessorBase.cs
///   Description:        This class is providing base templete for processing the Entity module to BACnet Stack integration Module
///   Author:             Rupesh Saw                  
///   Date:               05/23/17
///   Notes:              <Notes>
///   Revision History: 
///   Modified By:     Rupesh Saw Modified Date:    Reviewed By:      Reviewed Date:    Review Comments:
///---------------------------------------------------------------------------------------------
#endregion

using Molex.BACnet.Gateway.Log;
using Molex.BACnet.Gateway.ServiceCore.AppResources;
using Molex.BACnet.Gateway.ServiceCore.Constants;
using Molex.BACnet.Gateway.ServiceCore.Helpers;
using Molex.BACnet.Gateway.ServiceCore.Managers;
using Molex.BACnet.Gateway.ServiceCore.Models;
using Molex.BACnet.Gateway.ServiceCore.Models.BACnet;
using Molex.BACnet.Gateway.ServiceCore.Models.Entity;
using Molex.BACnet.Gateway.ServiceCore.Models.ObjectMap;
using Molex.BACnet.Gateway.ServiceCore.Repository;
using Molex.StackDataObjects.APIModels;
using Molex.StackDataObjects.Constants;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Molex.BACnet.Gateway.ServiceCore.Processor
{
    /// <summary>
    /// This class is providing base templete for processing the Entity module to BACnet Stack integration Module
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:42 PM</TimeStamp>
    public class ProcessorBase
    {
        #region PrivateMethods

        /// <summary>
        /// Gets the processor.
        /// </summary>
        /// <param name="dataModel">The data model.</param>
        /// <returns>ProcessorBase</returns>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201712:48 PM</TimeStamp>
        private ProcessorBase GetProcessor(EntityBaseModel dataModel)
        {
            switch (dataModel.EntityType)
            {
                case Molex.BACnet.Gateway.ServiceCore.Constants.AppConstants.EntityTypes.Controller:
                    return new ControllerProcessor();
                case Molex.BACnet.Gateway.ServiceCore.Constants.AppConstants.EntityTypes.Building:
                    return new BuildingProcessor();
                case Molex.BACnet.Gateway.ServiceCore.Constants.AppConstants.EntityTypes.Floor:
                    return new FloorProcessor();
                case Molex.BACnet.Gateway.ServiceCore.Constants.AppConstants.EntityTypes.Zone:
                    return new ZoneProcessor();
                default:
                    return null;

            }
        }

        private static uint GetObjectIDFromMappingFile(string mappingKey, ProcessorConfigurationModel processorConfiguration, BacnetObjectType objectType, AppConstants.EntityTypes entityType, uint deviceID, string objectKey, int objectId = 0)
        {
            if (objectId != 0)
            {
                return (uint)objectId;
            }

            ResultModel<uint> mappingObjectID = ObjectMappingHelper.GetObjectIdFromMappingData(processorConfiguration, mappingKey, objectKey);

            if (mappingObjectID.IsSuccess)
            {
                bool isValidObjectID = ObjectIdentifierManager.Instance.AddObjectId((uint)deviceID, objectType, (int)mappingObjectID.Data, entityType);
                if (!isValidObjectID)
                {
                    mappingObjectID.Data = GetIncrementalObjectID(processorConfiguration, objectType, entityType, objectKey, deviceID);
                }
            }
            else
            {
                mappingObjectID.Data = GetIncrementalObjectID(processorConfiguration, objectType, entityType, objectKey, deviceID);
            }
            return mappingObjectID.Data;
        }

        private static uint GetIncrementalObjectID(ProcessorConfigurationModel processorConfiguration, BacnetObjectType objectType, AppConstants.EntityTypes entityType, string objectKey, uint deviceID, int objectId = 0)
        {
            if (objectId != 0)
            {
                return (uint)objectId;
            }
            uint objectID = (uint)ObjectIdentifierManager.Instance.GetObjectId(deviceID, objectType, entityType);
            bool canFindNextIdentifier = true;

            do
            {
                if (processorConfiguration.MappedObjectIds.ContainsKey((uint)deviceID) && processorConfiguration.MappedObjectIds[(uint)deviceID].ContainsKey(objectKey))
                {
                    canFindNextIdentifier = processorConfiguration.MappedObjectIds[(uint)deviceID][objectKey].Contains(objectID);

                    if (canFindNextIdentifier)
                        objectID = (uint)ObjectIdentifierManager.Instance.GetObjectId((uint)deviceID, objectType, entityType, (int)objectID);
                }
                else
                    canFindNextIdentifier = false;

            } while (canFindNextIdentifier);

            ObjectIdentifierManager.Instance.AddObjectId((uint)deviceID, objectType, (int)objectID, entityType);

            return objectID;
        }

        private static ResultModel<CreateObjectResultModel> GetDeviceIDFromMappingFile(EntityBaseModel entityBaseModel, ProcessorConfigurationModel processorConfiguration, BacnetObjectType objectType, AppConstants.EntityTypes entityType)
        {
            ResultModel<CreateObjectResultModel> objectCreationResult = new ResultModel<CreateObjectResultModel>() { IsSuccess = false };
            objectCreationResult.Data = new CreateObjectResultModel();

            ResultModel<int> resultDeviceId = new ResultModel<int>();

            switch (processorConfiguration.RestoreStrategy)
            {
                case AppConstants.RestoreIDReclaimStrategy.Incremental:
                    {
                        objectCreationResult = GetDeviceIDFromEntityModel(entityBaseModel, objectType, entityType);
                        return objectCreationResult;
                    }

                case AppConstants.RestoreIDReclaimStrategy.Preserve:
                    {
                        resultDeviceId = ObjectMappingHelper.GetDeviceIdFromMappingData(entityBaseModel, processorConfiguration.EntityBACnetCache);

                        if (resultDeviceId.Data == AppConstants.DEFAULT_DEVICE_ID || resultDeviceId.IsSuccess == false)
                        {
                            objectCreationResult = GetDeviceIDFromEntityModel(entityBaseModel, objectType, entityType);
                            return objectCreationResult;
                        }
                        else
                        {
                            objectCreationResult.Data.DeviceId = (uint)resultDeviceId.Data;
                            objectCreationResult.IsSuccess = true;
                            return objectCreationResult;
                        }
                    }
                default:
                    return objectCreationResult;
            }
        }

        private static ResultModel<CreateObjectResultModel> GetDeviceIDFromEntityModel(EntityBaseModel entityBaseModel, BacnetObjectType objectType, AppConstants.EntityTypes entityType)
        {
            ResultModel<CreateObjectResultModel> objectCreationResult = new ResultModel<CreateObjectResultModel>();
            objectCreationResult.Data = new CreateObjectResultModel();

            ResultModel<int> resultDeviceId = EntityHelper.GetDeviceId(entityBaseModel);

            if (!resultDeviceId.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.ERROR, "ProcessorBase:GetDeviceIDFromEntityModel: Object Creation failed");
                objectCreationResult.IsSuccess = false;
                return objectCreationResult;
            }

            objectCreationResult.Data.DeviceId = (uint)resultDeviceId.Data;
            objectCreationResult.IsSuccess = true;

            return objectCreationResult;
        }

        #endregion

        #region PublicMethods

        /// <summary>
        /// Processes the specified data model.
        /// </summary>
        /// <param name="dataModel">The data model.</param>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:43 PM</TimeStamp>
        public void Process(EntityBaseModel dataModel)
        {
            ProcessorBase processor = GetProcessor(dataModel);
        }

        /// <summary>
        /// Processes the entity.
        /// </summary>
        /// <param name="entityBaseModel">The entity base model.</param>
        /// <param name="processorConfiguration">The processor configuration.</param>
        /// <returns>boolean</returns>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>09-06-201705:39 PM</TimeStamp>
        public virtual bool ProcessEntity(EntityBaseModel entityBaseModel, Models.ProcessorConfigurationModel processorConfiguration)
        {

            return false;
        }

        #endregion

        #region ProtectedMethods
        /// <summary>
        /// Creates the object.
        /// </summary>
        /// <param name="entityBaseModel">The entity base model.</param>
        /// <param name="processorConfiguration">The processor configuration.</param>
        /// <param name="objectType">Type of the object.</param>
        /// <param name="lightingProperty">The lighting property.</param>
        /// <param name="entityType">The entityType.</param>
        /// <returns>
        /// ResultModel
        /// </returns>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201712:48 PM</TimeStamp>
        protected ResultModel<CreateObjectResultModel> CreateObject(EntityBaseModel entityBaseModel, ProcessorConfigurationModel processorConfiguration, BacnetObjectType objectType, string lightingProperty, AppConstants.EntityTypes entityType, int objectId, bool isKeySpecificID = false)
        {
            ResultModel<CreateObjectResultModel> objectCreationResult = new ResultModel<CreateObjectResultModel>();
            objectCreationResult.Data = new CreateObjectResultModel();

            string objectKey = EntityHelper.GetObjectDetailsKey(objectType, lightingProperty);

            if (processorConfiguration.EntityBACnetCache.Count == 0 || !processorConfiguration.EntityBACnetCache.ContainsKey(entityBaseModel.MappingKey))
            {
                objectCreationResult = GetDeviceIDFromEntityModel(entityBaseModel, objectType, entityType);

                if (!objectCreationResult.IsSuccess)
                    return objectCreationResult;
                if (objectId == 0)
                {
                    objectId = ObjectIdentifierManager.Instance.GetObjectId((uint)objectCreationResult.Data.DeviceId, objectType, entityType,0,isKeySpecificID, lightingProperty);
                }
                bool isSuccess = ObjectIdentifierManager.Instance.AddObjectId((uint)objectCreationResult.Data.DeviceId, objectType, objectId, entityType, isKeySpecificID, lightingProperty);

                if (!isSuccess)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "ProcessorBase.CreateObject: Add Object id to identifier manager fail");
                    objectCreationResult.IsSuccess = false;
                }

                objectCreationResult.Data.ObjectId = (uint)objectId;
            }
            else
            {
                objectCreationResult = GetDeviceIDFromMappingFile(entityBaseModel, processorConfiguration, objectType, entityType);

                if (!objectCreationResult.IsSuccess)
                    return objectCreationResult;

                if (processorConfiguration.RestoreStrategy == AppConstants.RestoreIDReclaimStrategy.Preserve && objectCreationResult.Data.DeviceId != (uint)EntityHelper.GetDeviceId(entityBaseModel).Data)
                {
                    objectCreationResult.Data.DeviceId = (uint)EntityHelper.GetDeviceId(entityBaseModel).Data;

                    objectCreationResult.Data.ObjectId = GetIncrementalObjectID(processorConfiguration, objectType, entityType, objectKey, objectCreationResult.Data.DeviceId, objectId);
                }
                else
                {
                    objectCreationResult.Data.ObjectId = GetObjectIDFromMappingFile(entityBaseModel.MappingKey, processorConfiguration, objectType, entityType, objectCreationResult.Data.DeviceId, objectKey, objectId);
                }
            }

            string objectName = GetObjectName(processorConfiguration.EntityBACnetCache, entityBaseModel, objectKey, objectType, objectCreationResult.Data.ObjectId);

            bool result = StackManager.Instance.CreateObject((uint)objectCreationResult.Data.DeviceId, objectType, objectCreationResult.Data.ObjectId, objectName);

            if (!result)
            {
                Logger.Instance.Log(LogLevel.ERROR, "ProcessorBase.CreateObject: Unable to create object");
                return objectCreationResult;
            }

            return objectCreationResult;
        }

        /// <summary>
        /// Caches the data based on Dictonary (Key/Value) pair.
        /// </summary>
        /// <param name="deviceId">The device identifier.</param>
        /// <param name="objectType">Type of the object.</param>
        /// <param name="objectId">The object identifier.</param>
        /// <param name="entityBaseModel">The entity base model.</param>
        /// <param name="lightingProperty">The lighting property.</param>
        /// <returns>True or False</returns>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201712:48 PM</TimeStamp>
        protected bool CacheData(uint deviceId, BacnetObjectType objectType, uint objectId, EntityBaseModel entityBaseModel, string lightingProperty)
        {
            string key = EntityHelper.GetObjectDetailsKey(objectType, lightingProperty);
            try
            {
                if (!entityBaseModel.BACnetData.ObjectDetails.ContainsKey(key))
                {
                    entityBaseModel.BACnetData.ObjectDetails[key] = new BACnetObjectDetails();
                }

                entityBaseModel.BACnetData.ObjectDetails[key].ObjectID = objectId;
                entityBaseModel.BACnetData.ObjectDetails[key].ObjectType = objectType;
                string bacnetObjectKey = CommonHelper.GenerateBACnetObjectMappedID(deviceId, objectType, objectId);
                AppCoreRepo.Instance.BACnetObjectEntityLookUp[bacnetObjectKey] = entityBaseModel;
                return true;
            }
            catch (Exception e)
            {
                Logger.Instance.Log(e, LogLevel.ERROR, "ProcessorBase.CacheData :  Caching Data failed");
                return false;
            }
        }

        /// <summary>
        /// Creates the object and cache data.
        /// </summary>
        /// <param name="entityBaseModel">The entity base model.</param>
        /// <param name="processorConfiguration">The processor configuration.</param>
        /// <param name="bacnetObjectType">Type of the bacnet object.</param>
        /// <param name="lightingProperty">The lighting property.</param>
        /// <param name="entityType">Type of the entity.</param>
        /// <returns>
        /// true/false on object creation
        /// </returns>
        /// <CreatedBy>prasad.joshi</CreatedBy><TimeStamp>5/29/201710:00 AM</TimeStamp>
        protected ResultModel<CreateObjectResultModel> CreateObjectAndCacheData(EntityBaseModel entityBaseModel, ProcessorConfigurationModel processorConfiguration, BacnetObjectType bacnetObjectType, string lightingProperty, AppConstants.EntityTypes entityType, int objectId = 0 , bool isKeySpecificID = false)
        {
            ResultModel<CreateObjectResultModel> result = CreateObject(entityBaseModel, processorConfiguration, bacnetObjectType, lightingProperty, entityType, objectId, isKeySpecificID);

            if (!result.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.DEBUG, "CreateObjectAndCacheData: Create object Failure for entity model :" + entityBaseModel.EntityName);
                return result;
            }

            // Caches the data based on Dictonary (Key/Value) Key- BACnetobjectId and Value- EntityModel.
            if (!CacheData(result.Data.DeviceId, bacnetObjectType, (uint)result.Data.ObjectId, entityBaseModel, lightingProperty))
            {
                result.IsSuccess = false;
                return result;
            }

            return result;
        }

        /// <summary>
        /// Sets the poperties.
        /// </summary>
        /// <param name="deviceId">The device identifier.</param>
        /// <param name="objectId">The object identifier.</param>
        /// <param name="bacnetObjectType">Type of the bacnet object.</param>
        /// <param name="bacnetPropertyID">The bacnet property identifier.</param>
        /// <param name="propertyValue">The property value.</param>
        /// <param name="lightingPropertyValue">The lighting property value.</param>
        /// <param name="processorConfigurationModel">The processor configuration.</param>
        /// <param name="entityBaseModel">The entity base model.</param>
        /// <CreatedBy>prasad.joshi</CreatedBy><TimeStamp>5/30/201710:33 AM</TimeStamp>
        protected virtual void SetProperty(uint deviceId, uint objectId, BacnetObjectType bacnetObjectType, BacnetPropertyID bacnetPropertyID, object propertyValue, string lightingPropertyValue, ProcessorConfigurationModel processorConfigurationModel, EntityBaseModel entityBaseModel)
        {
            bool isSuccess = false;
            try
            {
                switch (bacnetPropertyID)
                {
                    case BacnetPropertyID.PropDescription:
                        {
                            string key = EntityHelper.GetObjectDetailsKey(bacnetObjectType, lightingPropertyValue);
                            propertyValue = GetObjectDescription(processorConfigurationModel.EntityBACnetCache, entityBaseModel, propertyValue.ToString(), key);
                        }
                        break;
                }

                isSuccess = StackManager.Instance.WriteProperty(deviceId, bacnetObjectType, objectId, propertyValue, bacnetPropertyID);
                if (!isSuccess)
                {
                    Logger.Instance.Log(LogLevel.DEBUG, "ProcessorBase:SetPoperties: Is Success " + isSuccess + ", for " + bacnetObjectType);
                }
            }
            catch (Exception e)
            {
                Logger.Instance.Log(e, LogLevel.ERROR, "ProcessorBase:SetPoperties: Failed for " + bacnetObjectType);
            }
        }

        /// <summary>
        /// Sets the property.
        /// </summary>
        /// <param name="deviceId">The device identifier.</param>
        /// <param name="objectId">The object identifier.</param>
        /// <param name="bacnetObjectType">Type of the bacnet object.</param>
        /// <param name="bacnetPropertyID">The bacnet property identifier.</param>
        /// <param name="propertyValue">The property value.</param>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>7/7/20177:14 PM</TimeStamp>
        protected void SetProperty(uint deviceId, uint objectId, BacnetObjectType bacnetObjectType, BacnetPropertyID bacnetPropertyID, object propertyValue)
        {
            SetProperty(deviceId, objectId, bacnetObjectType, bacnetPropertyID, propertyValue, string.Empty, null, null);
        }

        /// <summary>
        /// Sets the MultiState State-Text Properties.
        /// </summary>
        /// <param name="resultModel">The result model.</param>
        /// <param name="processorConfiguration">Theprocessor configuration model.</param>
        /// <returns>Success/Fail</returns>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>03-07-201705:38 PM</TimeStamp>
        protected ResultModel<bool> SetMSVStateText(ResultModel<CreateObjectResultModel> resultModel, string[] lightScenes)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            result.IsSuccess = false;
            if (lightScenes.Length > 0)
                result.IsSuccess = StackManager.Instance.WriteProperty((uint)resultModel.Data.DeviceId, BacnetObjectType.ObjectMultiStateValue, resultModel.Data.ObjectId, lightScenes, BacnetPropertyID.PropStateText, BacnetDataType.BacnetDTCharStringArray);

            return result;
        }

        //WriteProperty defined for SubordinateList        
        /// <summary>
        /// SetProperties for Structured View object Subordinatelist property.
        /// </summary>
        /// <param name="resultModel">The result model.</param>
        /// <param name="entityBaseModel">The entity base model.</param>
        /// <returns>Success/Fail</returns>
        protected ResultModel<bool> SetStructuredViewSubOrdinateListProperty(ResultModel<CreateObjectResultModel> resultModel, EntityBaseModel entityBaseModel)
        {
            try
            {
                ResultModel<bool> result = new ResultModel<bool>();
                result.IsSuccess = true;
                BacnetObjectType type = entityBaseModel.EntityType == AppConstants.EntityTypes.Building ? BacnetObjectType.ObjectStructuredView : BacnetObjectType.ObjectDevice;
                List<BacnetDevObjRefModel> models = new List<BacnetDevObjRefModel>();
                foreach (EntityBaseModel data in entityBaseModel.Childs)
                {
                    BacnetDevObjRefModel model = new BacnetDevObjRefModel();
                    model.DeviceIDPresent = false;
                    if (!data.BACnetData.ObjectDetails.ContainsKey(type.ToString()))
                    {
                        Logger.Instance.Log(LogLevel.ERROR, "Error Received At ProcesserBase.SetStructuredViewSubOrdinateListProperty() key not found " + type.ToString());
                        continue;
                    }
                    model.ObjectID = data.BACnetData.ObjectDetails[type.ToString()].ObjectID;
                    model.ObjectType = type;
                    models.Add(model);
                }

                if (entityBaseModel.Childs.Count > 0)
                    result.IsSuccess = StackManager.Instance.WriteProperty((uint)resultModel.Data.DeviceId, BacnetObjectType.ObjectStructuredView, resultModel.Data.ObjectId, models, BacnetPropertyID.PropSubordinateList, BacnetDataType.BacnetDTDevObjReffArray);
                return result;
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "Error Received At ProcesserBase.SetStructuredViewSubOrdinateListProperty()");
                return new ResultModel<bool>();
            }
        }

        //WriteProperty defined for Subordinate Annotations        
        /// <summary>
        ///SetProperties for Structured View object Subordinatelist annotation property.
        /// </summary>
        /// <param name="resultModel">The result model.</param>
        /// <param name="entityBaseModel">The entity base model.</param>
        /// <returns>Success/Fail</returns>
        protected ResultModel<bool> SetStructuredViewSubOrdinateAnnotationProperty(ResultModel<CreateObjectResultModel> resultModel, EntityBaseModel entityBaseModel)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            result.IsSuccess = true;

            string[] subordinateAnnotationsArray = new string[entityBaseModel.Childs.Count];
            int i = 0;
            BacnetObjectType bacnetObjectType = new BacnetObjectType();

            switch (entityBaseModel.EntityType)
            {
                case AppConstants.EntityTypes.Building:
                    bacnetObjectType = BacnetObjectType.ObjectStructuredView;
                    break;
                case AppConstants.EntityTypes.Floor:
                    bacnetObjectType = BacnetObjectType.ObjectDevice;
                    break;
            }
            foreach (EntityBaseModel data in entityBaseModel.Childs)
            {
                subordinateAnnotationsArray[i] = data.BACnetData.ObjectDetails[bacnetObjectType.ToString()].ObjectName;
                i++;
            }

            if (entityBaseModel.Childs.Count > 0)
                result.IsSuccess = StackManager.Instance.WriteProperty((uint)resultModel.Data.DeviceId, BacnetObjectType.ObjectStructuredView, resultModel.Data.ObjectId, subordinateAnnotationsArray, BacnetPropertyID.PropSubordinateAnnotations, BacnetDataType.BacnetDTCharStringArray);
            return result;
        }

        /// <summary>
        /// Updates the current present value for objects which have present value property.
        /// </summary>
        /// <param name="entityBaseModel">The entity base model.</param>
        /// <param name="deviceId">The device identifier.</param>
        /// <param name="objectId">The object identifier.</param>
        /// <param name="objectType">Type of the object.</param>
        /// <param name="entityCacheKey">The entity cache key.</param>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>7/5/201710:47 AM</TimeStamp>
        protected void UpdateCurrentPresentValue(EntityBaseModel entityBaseModel, uint deviceId, uint objectId, BacnetObjectType objectType, string entityCacheKey)
        {
            try
            {
                switch (objectType)
                {
                    case BacnetObjectType.ObjectAnalogValue:
                        {
                            entityBaseModel.BACnetData.ObjectDetails[entityCacheKey].PresentValue = AppConstants.ANALOG_VALUE_OBJECT_UNKNOWN_PRESENT_VALUE;
                        }
                        break;
                    case BacnetObjectType.ObjectPositiveIntegerValue:
                    case BacnetObjectType.ObjectMultiStateValue:
                    case BacnetObjectType.ObjectAnalogInput:
                    case BacnetObjectType.ObjectBinaryInput:
                        {
                            entityBaseModel.BACnetData.ObjectDetails[entityCacheKey].PresentValue = AppConstants.ANALOG_VALUE_OBJECT_UNKNOWN_PRESENT_VALUE;
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "ProcessorBase:UpdateCurrentPresentValue:Unable to update present in cache for deviceId:" + deviceId + ", objectId:" + objectId + ", objectType:" + objectType + ", entityCacheKey:" + entityCacheKey);
            }
        }
        #endregion

        /// <summary>
        /// Gets the entity name for objects.
        /// </summary>
        /// <param name="cachedObjects">The cached objects.</param>
        /// <param name="entityBaseModel">The entity base model.</param>
        /// <param name="objectKey">The object key.</param>
        /// <returns></returns>
        /// <CreatedBy>Rohit Galgali</CreatedBy>
        /// <TimeStamp>6/29/20173:34 PM</TimeStamp>
        protected string GetObjectName(Dictionary<string, EntityObject> cachedObjects, EntityBaseModel entityBaseModel, string objectKey, BacnetObjectType objectType, uint objectID)
        {
            bool isNameChangeRequired = false;

            if (cachedObjects != null && cachedObjects.ContainsKey(entityBaseModel.MappingKey)
                && cachedObjects[entityBaseModel.MappingKey].BACnetEntityMap.ContainsKey(objectKey)
                && cachedObjects[entityBaseModel.MappingKey].BACnetEntityMap[objectKey].ObjectName != null)
            {
                switch (entityBaseModel.EntityType)
                {
                    case AppConstants.EntityTypes.Building:
                        if (entityBaseModel.EntityName != cachedObjects[entityBaseModel.MappingKey].DTName)
                        {
                            isNameChangeRequired = true;
                        }
                        if (cachedObjects[entityBaseModel.MappingKey].DTName != cachedObjects[entityBaseModel.Parent.MappingKey].BACnetEntityMap[BacnetObjectType.ObjectDevice.ToString()].ObjectName)
                        {
                            isNameChangeRequired = true;
                            entityBaseModel.EntityName = cachedObjects[entityBaseModel.Parent.MappingKey].BACnetEntityMap[BacnetObjectType.ObjectDevice.ToString()].ObjectName;
                        }

                        break;
                    case AppConstants.EntityTypes.Controller:

                        if (entityBaseModel.Childs[0].EntityName != cachedObjects[entityBaseModel.MappingKey].DTName)
                        {
                            isNameChangeRequired = true;
                        }
                        if (cachedObjects[entityBaseModel.MappingKey].DTName != cachedObjects[entityBaseModel.MappingKey].BACnetEntityMap[BacnetObjectType.ObjectDevice.ToString()].ObjectName)
                        {
                            isNameChangeRequired = true;
                            entityBaseModel.EntityName = cachedObjects[entityBaseModel.MappingKey].BACnetEntityMap[BacnetObjectType.ObjectDevice.ToString()].ObjectName;
                        }
                        break;

                }
                //Verifying BMS name vs DT name.
                //isNameChangeRequired used for if bulding name change from BMS or DT-
                //then that change should reflect for buidling notification and structured view object
                if (entityBaseModel.EntityName == cachedObjects[entityBaseModel.MappingKey].DTName && !isNameChangeRequired)
                {
                    if (!entityBaseModel.BACnetData.ObjectDetails.ContainsKey(objectKey))
                    {
                        entityBaseModel.BACnetData.ObjectDetails[objectKey] = new BACnetObjectDetails();
                    }
                    entityBaseModel.BACnetData.ObjectDetails[objectKey].ObjectName = cachedObjects[entityBaseModel.MappingKey].BACnetEntityMap[objectKey].ObjectName;
                    entityBaseModel.BACnetData.DTName = cachedObjects[entityBaseModel.MappingKey].DTName;
                    return cachedObjects[entityBaseModel.MappingKey].BACnetEntityMap[objectKey].ObjectName;
                }
                else
                {
                    return GetNewName(entityBaseModel, objectKey, objectType, objectID);
                }
            }
            else
            {
                return GetNewName(entityBaseModel, objectKey, objectType, objectID);
            }
        }

        private static string GetSensorName(EntityBaseModel entityBaseModel)
        {
            if (entityBaseModel == null)
                return string.Empty;
            if (entityBaseModel as SensorEntityModel == null)
                return entityBaseModel.EntityName;
            if (!string.IsNullOrEmpty((entityBaseModel as SensorEntityModel).EntityName))
                return entityBaseModel.EntityName;
            if (string.IsNullOrEmpty((entityBaseModel as SensorEntityModel).EntityName))
            {
                try
                {
                    List<SensorEntityModel> Sensordetails = (entityBaseModel.Parent as ZoneEntityModel).Sensors.Where(s => (s as SensorEntityModel).SensorType == (entityBaseModel as SensorEntityModel).SensorType).ToList();
                    int index = Sensordetails.IndexOf(Sensordetails.Where(s => s.EntityKey == entityBaseModel.EntityKey).First());

                    return (entityBaseModel as SensorEntityModel).SensorType.ToString() + " Sensor " + (index + 1).ToString();
                }
                catch (Exception ex)
                {
                    Logger.Instance.Log(ex, LogLevel.ERROR, "GetSensorName Exception Received");
                }
            }
            return string.Empty;
        }

        private static string GetNewName(EntityBaseModel entityBaseModel, string objectKey, BacnetObjectType objectType, uint objectID)
        {
            string objectName = string.Empty;

            switch (objectType)
            {
                case BacnetObjectType.ObjectAnalogInput:
                    switch (entityBaseModel.EntityType)
                    {
                        case AppConstants.EntityTypes.Sensor:
                            objectName = GetSensorName(entityBaseModel);
                            break;
                        case AppConstants.EntityTypes.Zone:
                        case AppConstants.EntityTypes.Floor:
                        case AppConstants.EntityTypes.Building:
                            AppConstants.SensorType sensorType = 0;
                            foreach (AppConstants.SensorType type in Enum.GetValues(typeof(AppConstants.SensorType)))
                            {
                                if (objectKey.Contains(type.ToString()))
                                {
                                    sensorType = type;
                                    break;
                                }
                            }
                            switch (sensorType)
                            {
                                case AppConstants.SensorType.AirQuality:
                                case AppConstants.SensorType.ColorTemperature:
                                case AppConstants.SensorType.Humidity:
                                case AppConstants.SensorType.LuxLevel:
                                case AppConstants.SensorType.Temperature:
                                    if (entityBaseModel.EntityType == AppConstants.EntityTypes.Zone)
                                        objectName = sensorType.ToString();
                                    else
                                        objectName = entityBaseModel.EntityName + " " + string.Format(AppResources.AppResource.Average, sensorType.ToString());
                                    break;
                                case AppConstants.SensorType.Power:
                                    if (entityBaseModel.EntityType == AppConstants.EntityTypes.Zone)
                                        objectName = sensorType.ToString();
                                    else
                                        objectName = entityBaseModel.EntityName + " " + string.Format(AppResources.AppResource.Total, sensorType.ToString());
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case BacnetObjectType.ObjectAnalogValue:
                    switch (entityBaseModel.EntityType)
                    {
                        case AppConstants.EntityTypes.Building:
                            if (objectKey == BacnetObjectType.ObjectAnalogValue + "_" + AppResource.BioDynamicControlName)
                                objectName = entityBaseModel.EntityName + " " + AppResource.BioDynamicControlName;
                            else if (objectKey == BacnetObjectType.ObjectAnalogValue + "_" + AppResource.SaturationControlName)
                                objectName = entityBaseModel.EntityName + " " + AppResource.SaturationControlName;
                            else if (objectKey == BacnetObjectType.ObjectAnalogValue + "_" + entityBaseModel.EntityType + " " + AppResource.UserLightLevel)
                                objectName = entityBaseModel.EntityName + " " + AppResource.UserLightLevel;
                            else if (objectKey == BacnetObjectType.ObjectAnalogValue + "_" + entityBaseModel.EntityType + " " + AppResource.GeneralLightLevel)
                                objectName = entityBaseModel.EntityName + " " + AppResource.GeneralLightLevel;
                            else if (objectKey == BacnetObjectType.ObjectAnalogValue + "_" + entityBaseModel.EntityType + " " + AppResource.BeaconLightLevel)
                                objectName = entityBaseModel.EntityName + " " + AppResource.BeaconLightLevel;
                            else if (objectKey == BacnetObjectType.ObjectAnalogValue + "_" + AppResource.AnalogValueBlindObjectName)
                                objectName = entityBaseModel.EntityName + " " + AppResource.AnalogValueBlindObjectName;
                            else if (objectKey.StartsWith(BacnetObjectType.ObjectAnalogValue + "_" + AppResource.AnalogValueBlindObjectName))
                            {
                                string strDirection = objectKey.Replace(BacnetObjectType.ObjectAnalogValue + "_" + AppResource.AnalogValueBlindObjectName + "_", "");
                                objectName = entityBaseModel.EntityName + " " + AppResource.AnalogValueBlindLocationObjectSubName + " " + strDirection;
                            }
                            break;
                        case AppConstants.EntityTypes.Floor:
                            if (objectKey == BacnetObjectType.ObjectAnalogValue + "_" + AppResource.BioDynamicControlName)
                                objectName = entityBaseModel.EntityName + " " + AppResource.BioDynamicControlName;
                            else if (objectKey == BacnetObjectType.ObjectAnalogValue + "_" + AppResource.SaturationControlName)
                                objectName = entityBaseModel.EntityName + " " + AppResource.SaturationControlName;
                            else if (objectKey == BacnetObjectType.ObjectAnalogValue + "_" + entityBaseModel.EntityType + " " + AppResource.UserLightLevel)
                                objectName = entityBaseModel.EntityName + " " + AppResource.UserLightLevel;
                            else if (objectKey == BacnetObjectType.ObjectAnalogValue + "_" + entityBaseModel.EntityType + " " + AppResource.GeneralLightLevel)
                                objectName = entityBaseModel.EntityName + " " + AppResource.GeneralLightLevel;
                            else if (objectKey == BacnetObjectType.ObjectAnalogValue + "_" + entityBaseModel.EntityType + " " + AppResource.BeaconLightLevel)
                                objectName = entityBaseModel.EntityName + " " + AppResource.BeaconLightLevel;
                            else if (objectKey == BacnetObjectType.ObjectAnalogValue + "_" + AppResource.AnalogValueBlindObjectName)
                                objectName = entityBaseModel.EntityName + " " + AppResource.AnalogValueBlindObjectName;
                            else if (objectKey.StartsWith(BacnetObjectType.ObjectAnalogValue + "_" + AppResource.AnalogValueBlindObjectName))
                            {
                                string strDirection = objectKey.Replace(BacnetObjectType.ObjectAnalogValue + "_" + AppResource.AnalogValueBlindObjectName + "_", "");
                                objectName = entityBaseModel.EntityName + " " + AppResource.AnalogValueBlindLocationObjectSubName + " " + strDirection;
                            }
                            break;
                        case AppConstants.EntityTypes.Zone:
                            switch (((ZoneEntityModel)entityBaseModel).ZoneType)
                            {
                                case AppConstants.ZoneTypes.BeaconSpace:
                                    objectName = AppResources.AppResource.BeaconBrightness;
                                    break;
                                case AppConstants.ZoneTypes.UserSpace:
                                case AppConstants.ZoneTypes.GeneralSpace:
                                    if (objectKey == "ObjectAnalogValue_Brightness")
                                        objectName = AppResources.AppResource.ZoneBrightness;
                                    else if (objectKey == BacnetObjectType.ObjectAnalogValue + "_" + AppResource.BioDynamicControlName)
                                        objectName = AppResource.BioDynamicControlName;
                                    else if (objectKey == BacnetObjectType.ObjectAnalogValue + "_" + AppResource.SaturationControlName)
                                        objectName = AppResource.SaturationControlName;
                                    else
                                        objectName = AppResources.AppResource.ZoneBrightness;
                                    break;
                            }
                            break;
                        case AppConstants.EntityTypes.Fixture:
                            objectName = entityBaseModel.EntityName + " " + AppResources.AppResource.ZoneBrightness + " " + (((ZoneEntityModel)entityBaseModel.Parent).Fixtures.IndexOf((FixtureEntityModel)entityBaseModel) + 1);
                            break;
                        default:
                            break;
                    }
                    break;
                case BacnetObjectType.ObjectBinaryInput:
                    switch (entityBaseModel.EntityType)
                    {
                        case AppConstants.EntityTypes.Sensor:
                            objectName = GetSensorName(entityBaseModel);
                            break;
                        default:
                        case AppConstants.EntityTypes.Zone:
                        case AppConstants.EntityTypes.Floor:
                        case AppConstants.EntityTypes.Building:
                            AppConstants.SensorType sensorType = 0;
                            if (objectKey.Contains(AppConstants.SensorType.Presence.ToString()))
                            {
                                sensorType = AppConstants.SensorType.Presence;
                            }
                            switch (sensorType)
                            {
                                case AppConstants.SensorType.Presence:
                                    objectName = string.Format(AppResources.AppResource.Status, sensorType.ToString());
                                    break;
                                default:
                                    break;
                            }
                            if (objectKey.Contains(AppConstants.ZONE_OCCUPANCY_OBJECT))
                                objectName = "Zone Occupancy";
                            if (objectKey.Contains(AppConstants.FLOOR_OCCUPANCY_OBJECT))
                                objectName = entityBaseModel.EntityName + " " + "Occupancy";
                            if (objectKey.Contains(AppConstants.BUILDING_OCCUPANCY_OBJECT))
                                objectName = entityBaseModel.EntityName + " " + "Occupancy";
                            break;                      
                    }
                    break;
                case BacnetObjectType.ObjectMultiStateValue:
                    switch (entityBaseModel.EntityType)
                    {
                        case AppConstants.EntityTypes.Building:
                            objectName = entityBaseModel.EntityName + " " + AppResources.AppResource.LightScene;
                            break;
                        case AppConstants.EntityTypes.Floor:
                            objectName = entityBaseModel.EntityName + " " + AppResources.AppResource.LightScene;
                            break;
                        case AppConstants.EntityTypes.Zone:
                            switch (((ZoneEntityModel)entityBaseModel).ZoneType)
                            {
                                case AppConstants.ZoneTypes.BeaconSpace:
                                    objectName = AppResources.AppResource.BeaconLightScene;
                                    if (objectKey.Contains(AppConstants.PALETTES_OBJECT))
                                        objectName = "Beacon Palettes";
                                    break;
                                case AppConstants.ZoneTypes.UserSpace:
                                case AppConstants.ZoneTypes.GeneralSpace:
                                    if (objectKey == BacnetObjectType.ObjectMultiStateValue + "_" + AppConstants.LIGHTING_MOOD_OBJECT)
                                        objectName = AppConstants.LIGHTING_MOOD_OBJECT;
                                    else if (objectKey == BacnetObjectType.ObjectMultiStateValue + "_" + AppConstants.RESET_ATTRIBUTES_AFTER_TIMEOUT)
                                        objectName = AppConstants.RESET_ATTRIBUTES_AFTER_TIMEOUT;
                                    else
                                        objectName = AppResources.AppResource.LightScene;
                                    break;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case BacnetObjectType.ObjectNotificationClass:
                    int objectCount = entityBaseModel.BACnetData.ObjectDetails.Count(s => s.Value.ObjectType == BacnetObjectType.ObjectNotificationClass);
                    objectName = AppResources.AppResource.ZoneNotification + " " + (objectCount + 1).ToString();
                    break;
                case BacnetObjectType.ObjectBinaryValue:
                    switch (entityBaseModel.EntityType)
                    {
                        case AppConstants.EntityTypes.Zone:
                            switch (((ZoneEntityModel)entityBaseModel).ZoneType)
                            {
                                case AppConstants.ZoneTypes.BeaconSpace:
                                    objectName = AppResources.AppResource.BeaconState;
                                    break;
                                case AppConstants.ZoneTypes.UserSpace:
                                case AppConstants.ZoneTypes.GeneralSpace:
                                    objectName = AppResources.AppResource.ZoneState;
                                    break;
                            }
                            break;
                        case AppConstants.EntityTypes.Floor:
                            if (objectKey.Contains(AppConstants.FLOOR_ONOFF_OBJECT))
                                objectName = entityBaseModel.EntityName + " " + "ON/OFF";
                            break;
                        case AppConstants.EntityTypes.Building:
                            if (objectKey.Contains(AppConstants.BUILDING_ONOFF_OBJECT))
                                objectName = entityBaseModel.EntityName + " " + "ON/OFF";
                            break;
                        default:
                            break;
                    }
                    break;
                case BacnetObjectType.ObjectDevice:
                    switch (entityBaseModel.EntityType)
                    {
                        case AppConstants.EntityTypes.Zone:
                            objectName = entityBaseModel.EntityName;
                            break;
                        case AppConstants.EntityTypes.Controller:
                            objectName = entityBaseModel.EntityName;
                            break;
                        default:
                            break;
                    }

                    break;
                case BacnetObjectType.ObjectStructuredView:
                    switch (entityBaseModel.EntityType)
                    {
                        case AppConstants.EntityTypes.Building:
                            objectName = entityBaseModel.EntityName + " " + AppResources.AppResource.Structure;
                            break;
                        case AppConstants.EntityTypes.Floor:
                            objectName = entityBaseModel.EntityName + " " + AppResources.AppResource.Structure;
                            break;
                        default:
                            break;
                    }
                    break;
                case BacnetObjectType.ObjectPositiveIntegerValue:
                    switch (entityBaseModel.EntityType)
                    {
                        case AppConstants.EntityTypes.Zone:
                            switch (((ZoneEntityModel)entityBaseModel).ZoneType)
                            {
                                case AppConstants.ZoneTypes.BeaconSpace:
                                    objectName = objectKey.Replace(BacnetObjectType.ObjectPositiveIntegerValue + "_", " ").Trim();
                                    break;
                                case AppConstants.ZoneTypes.UserSpace:
                                case AppConstants.ZoneTypes.GeneralSpace:
                                    if (objectKey.Contains(AppConstants.PositiveIntegerValueType.DHTargetLux.ToString()))
                                    {
                                        objectName = AppResources.AppResource.PIV1;
                                    }
                                    else if (objectKey.Contains(AppConstants.PositiveIntegerValueType.OccupancyFadeOutTime.ToString()))
                                    {
                                        objectName = AppResources.AppResource.PIV3;
                                    }
                                    else
                                    {
                                        objectName = AppResources.AppResource.PIV2;
                                    }
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }

            objectName = objectName.Length <= 255 ? objectName : objectName.Substring(0, 254);


            if (!entityBaseModel.BACnetData.ObjectDetails.ContainsKey(objectKey))
            {
                entityBaseModel.BACnetData.ObjectDetails[objectKey] = new BACnetObjectDetails() { ObjectName = objectName };
            }
            else
            {
                entityBaseModel.BACnetData.ObjectDetails[objectKey].ObjectName = objectName;
            }
            entityBaseModel.BACnetData.DTName = entityBaseModel.EntityName;
            return objectName;
        }

        /// <summary>
        /// Gets the object description.
        /// </summary>
        /// <param name="cacheObjects">The cache objects.</param>
        /// <param name="entityBaseModel">The entity base model.</param>
        /// <param name="defaultDesciption">The default desciption.</param>
        /// <param name="key">key.</param>
        /// <returns></returns>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>7/7/20171:59 PM</TimeStamp>
        protected string GetObjectDescription(Dictionary<string, Models.ObjectMap.EntityObject> cacheObjects, EntityBaseModel entityBaseModel, string defaultDesciption, string key)
        {
            if (cacheObjects.Count != 0 && cacheObjects.ContainsKey(entityBaseModel.MappingKey))
            {
                if (cacheObjects[entityBaseModel.MappingKey].BACnetEntityMap.ContainsKey(key) && !string.IsNullOrEmpty(cacheObjects[entityBaseModel.MappingKey].BACnetEntityMap[key].ObjectDescription))
                {
                    entityBaseModel.BACnetData.ObjectDetails[key].ObjectDescription = cacheObjects[entityBaseModel.MappingKey].BACnetEntityMap[key].ObjectDescription;
                    return cacheObjects[entityBaseModel.MappingKey].BACnetEntityMap[key].ObjectDescription;
                }
                else
                {
                    return defaultDesciption;
                }
            }
            return defaultDesciption;
        }

        /// <summary>
        /// Sets the type of the property access.
        /// </summary>
        /// <param name="deviceId">The device identifier.</param>
        /// <param name="objectId">The object identifier.</param>
        /// <param name="bacnetObjectType">Type of the bacnet object.</param>
        /// <param name="bacnetPropertyID">The bacnet property identifier.</param>
        /// <param name="accessType">Type of the access.</param>
        /// <param name="isReadOrWrite">if set to <c>true</c> [is read or write].</param>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>12/27/201712:42 PM</TimeStamp>
        protected virtual void SetPropertyAccessType(uint deviceId, uint objectId, BacnetObjectType bacnetObjectType, BacnetPropertyID bacnetPropertyID, PropAccessType accessType, bool isReadOrWrite)
        {
            StackManager.SetPropertyAccessType(deviceId, objectId, bacnetObjectType, bacnetPropertyID, accessType, isReadOrWrite);
        }
    }
}
