﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              ZoneProcessor.cs
///   Description:        This class is used for processing the Entity module to BACnet Stack integration Module
///   Author:             Rupesh Saw                  
///   Date:               05/23/17
///   Notes:              <Notes>
///   Revision History: 
///   Modified By:     Rupesh Saw Modified Date:    Reviewed By:      Reviewed Date:    Review Comments:
///---------------------------------------------------------------------------------------------
#endregion

using Molex.BACnet.Gateway.Log;
using Molex.BACnet.Gateway.ServiceCore.AppResources;
using Molex.BACnet.Gateway.ServiceCore.Constants;
using Molex.BACnet.Gateway.ServiceCore.Helpers;
using Molex.BACnet.Gateway.ServiceCore.Managers;
using Molex.BACnet.Gateway.ServiceCore.Models;
using Molex.BACnet.Gateway.ServiceCore.Models.BACnet;
using Molex.BACnet.Gateway.ServiceCore.Models.Entity;
using Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel;
using Molex.BACnet.Gateway.ServiceCore.Repository;
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Models;
using Molex.StackDataObjects.Response;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Molex.BACnet.Gateway.ServiceCore.Processor
{
    /// <summary>
    /// This class is used for processing the Entity module to BACnet Stack integration Module
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:44 PM</TimeStamp>
    /// <seealso cref="Molex.BACnet.Gateway.ServiceCore.Processor.ProcessorBase" />
    public class ZoneProcessor : ZoneBaseProcessor
    {

        /// <summary>
        /// Creating objects for ZoneEnityModel mapped to Bacnet Stack
        /// </summary>
        /// <param name="entityBaseModel">EntityBaseModel</param>
        /// <param name="processorConfiguration">BACnetConfigurationModel</param>
        /// <returns>bool</returns>
        public override bool ProcessEntity(Models.Entity.EntityBaseModel entityBaseModel, Models.ProcessorConfigurationModel processorConfiguration)
        {
            EventTransitionBitsModel eventTransitionBitsModel;
            LimitEnableBitsModel limitEnableBitsModel = new LimitEnableBitsModel();
            limitEnableBitsModel.HighLimit = true;
            limitEnableBitsModel.LowLimit = true;
            Logger.Instance.Log(LogLevel.DEBUG, "ZoneProcessor.ProcessEntity : Processing Zone");

            if (!(entityBaseModel.EntityType == Constants.AppConstants.EntityTypes.Zone))
            {
                Logger.Instance.Log(LogLevel.ERROR, "ZoneProcessor.ProcessEntity : Entity model mismatch..Zone Entity model required ");
                return false;
            }

            BACnetConfigurationModel bacnetConfigurationModel = processorConfiguration.BACnetConfigurationData;

            ZoneEntityModel zoneEntityModel = entityBaseModel as ZoneEntityModel;

            if (processorConfiguration.BACnetConfigurationData.AlarmNotificationSetting != null)
            {
                processorConfiguration.ZoneAlarmNotificationSetting = processorConfiguration.BACnetConfigurationData.AlarmNotificationSetting["User Zone"];
            }

            uint deviceId = 0;

            switch (processorConfiguration.RestoreStrategy)
            {
                case AppConstants.RestoreIDReclaimStrategy.Preserve:
                    {
                        if (!processorConfiguration.EntityBACnetCache.ContainsKey(entityBaseModel.MappingKey))
                        {
                            deviceId = (uint)ObjectIdentifierManager.Instance.GetDeviceId();
                            deviceId = VerifyDeviceId(processorConfiguration, deviceId);
                            ObjectIdentifierManager.Instance.AddDeviceId(deviceId);
                        }
                        else
                        {
                            deviceId = (uint)processorConfiguration.EntityBACnetCache[entityBaseModel.MappingKey].DeviceId;
                            ObjectIdentifierManager.Instance.AddDeviceId(deviceId);
                        }
                    }
                    break;
                case AppConstants.RestoreIDReclaimStrategy.Incremental:
                    {
                        deviceId = (uint)ObjectIdentifierManager.Instance.GetDeviceId();
                        deviceId = VerifyDeviceId(processorConfiguration, deviceId);
                        ObjectIdentifierManager.Instance.AddDeviceId(deviceId);
                    }
                    break;
            }

            //For zone SNet is zero
            ResponseResult result = StackManager.Instance.CreateDeviceObject(deviceId, deviceId, (ushort)AppCoreRepo.Instance.BACnetConfigurationData.VirtualNetworkNumber);

            if (!result.IsSucceed)
            {
                Logger.Instance.Log(LogLevel.ERROR, "ZoneProcessor.ProcessEntity : Required Device not found..");
                return false;
            }

            // Set Password for Zone Level device
            ResponseResult passwordResult = StackManager.Instance.SetDevicePassword((int)deviceId, bacnetConfigurationModel.BACnetPassword);
            if (!passwordResult.IsSucceed)
            {
                Logger.Instance.Log(LogLevel.ERROR, "ZoneProcessor.ProcessEntity: Set vendor Password fail for Zone " + deviceId);
                return false;
            }

            entityBaseModel.BACnetData.DeviceID = (int)deviceId;

            if (!CacheData(deviceId, BacnetObjectType.ObjectDevice, deviceId, entityBaseModel, AppConstants.LIGHTING_OBJECTDEVICE_OBJECT))
            {
                Logger.Instance.Log(LogLevel.DEBUG, "ZoneProcessor.ProcessEntity : Object Device caching failed");
                return false;
            }

            ResultModel<int> resultDeviceId = EntityHelper.GetDeviceId(entityBaseModel);

            if (!resultDeviceId.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.ERROR, "ZoneProcessor.ProcessEntity : Required Device not found..");
                return false;
            }


            #region Notification class


            ResultModel<CreateObjectResultModel> resultModel = CreateObjectAndCacheData(entityBaseModel, processorConfiguration, BacnetObjectType.ObjectNotificationClass, AppConstants.LIGHTING_NOTIFICATIONCLASS_OBJECT, AppConstants.EntityTypes.Zone);

            if (!resultModel.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.DEBUG, "ZoneProcessor.ProcessEntity: Create object and cache data failed for Notification class ");
                return false;
            }

            SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, BacnetObjectType.ObjectNotificationClass, BacnetPropertyID.PropDescription, GetZoneTypeKey(entityBaseModel) + AppResource.NotificationClassRepresent, AppConstants.LIGHTING_NOTIFICATIONCLASS_OBJECT, processorConfiguration, entityBaseModel);
            SetPropertyAccessType(resultModel.Data.DeviceId, resultModel.Data.ObjectId, BacnetObjectType.ObjectNotificationClass, BacnetPropertyID.PropPriority, PropAccessType.READ_WRITE,true);

            uint[] priorityListArray = null;
            if (processorConfiguration.UserBACnetObjects != null && AppCoreRepo.Instance.BACnetConfigurationData.ZoneNotificationPriority.OverridePersistantState == false)
            {
                foreach (var item in processorConfiguration.UserBACnetObjects)
                {
                    if (item.Key == zoneEntityModel.EntityKey)
                    {
                        if (item.Value.NotificationClasses.Any(s => s.ObjectID == 16001))
                        {
                            NotificationClassPersistedModel obj = item.Value.NotificationClasses.Where(s => s.ObjectID == 16001).First();
                            if (obj == null)
                                break;
                            if (obj.NotificationPriority == null)
                                break;
                            if (obj.NotificationPriority.Count != 3)
                                break;
                            priorityListArray = new uint[3];
                            priorityListArray[2] = obj.NotificationPriority[2];
                            priorityListArray[1] = obj.NotificationPriority[1];
                            priorityListArray[0] = obj.NotificationPriority[0];
                            break;
                        }
                    }
                }
            }
            if (priorityListArray == null)
            {
                priorityListArray = new uint[3];
                priorityListArray[2] = (uint)AppCoreRepo.Instance.BACnetConfigurationData.ZoneNotificationPriority.ToNormal;
                priorityListArray[1] = (uint)AppCoreRepo.Instance.BACnetConfigurationData.ZoneNotificationPriority.ToFault;
                priorityListArray[0] = (uint)AppCoreRepo.Instance.BACnetConfigurationData.ZoneNotificationPriority.ToOffNormal;
            }
            SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, BacnetObjectType.ObjectNotificationClass, BacnetPropertyID.PropPriority, priorityListArray, AppConstants.LIGHTING_NOTIFICATIONCLASS_OBJECT, processorConfiguration, entityBaseModel);
            processorConfiguration.VDNotificationClassInstanceNumber = resultModel.Data.ObjectId;
            #endregion  Notification class

            #region AnalogValue


            ResultModel<CreateObjectResultModel> globalAnalogModel = CreateObjectAndCacheData(entityBaseModel, processorConfiguration, BacnetObjectType.ObjectAnalogValue, AppConstants.LIGHTING_BRIGHTENESS_OBJECT, AppConstants.EntityTypes.Zone);

            if (!globalAnalogModel.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.ERROR, "ZoneProcessor.ProcessEntity : Zone object creation failure in BACnet stack..");
                return globalAnalogModel.IsSuccess;
            }

            SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropDescription, GetZoneTypeKey(entityBaseModel) + AppResource.AnalogViewRepresentZone, AppConstants.LIGHTING_BRIGHTENESS_OBJECT, processorConfiguration, entityBaseModel);
            SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropUnits, BacnetEngineeringUnits.UnitsPercent);
            SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropRelinquishDefault, AppConstants.RELINQUISH_DEFAULT);
            SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropTimeDelay, processorConfiguration.BACnetConfigurationData.TimeDelay);
            SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropNotificationClass, processorConfiguration.VDNotificationClassInstanceNumber);
            SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropHighLimit, AppConstants.LIGHT_FIXTURE_HIGH_LIMIT);
            SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropProprietaryGridX, zoneEntityModel.ProprietaryGridX);
            SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropProprietaryGridY, zoneEntityModel.ProprietaryGridY);
            //SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropProprietaryColorTemperature, AppResource.FixtureColorTemperature);
            //SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropProprietaryColorTemperatureUnit, BacnetEngineeringUnits.UnitsDegreesKelvin);
            SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropNotifyType, processorConfiguration.BACnetConfigurationData.NotifyType);

            //Info: Default "Limit enable" property override with true flag
            SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropLimitEnable, limitEnableBitsModel);

            eventTransitionBitsModel = GetEventTransitionBit(processorConfiguration.ZoneAlarmNotificationSetting, AppConstants.LIGHTING_BRIGHTENESS_OBJECT);
            SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropEventEnable, eventTransitionBitsModel);
            #endregion AnalogValue

            #region Occupancy Zone Level

            ResultModel<CreateObjectResultModel> globalOccupancyBinaryInputModel = CreateObjectAndCacheData(entityBaseModel, processorConfiguration, BacnetObjectType.ObjectBinaryInput, AppConstants.ZONE_OCCUPANCY_OBJECT, AppConstants.EntityTypes.Zone, 11502);

            if (!globalOccupancyBinaryInputModel.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.ERROR, "ZoneProcessor.ProcessEntity : Zone object creation failure in BACnet stack..");
                return globalAnalogModel.IsSuccess;
            }

            SetProperty(globalOccupancyBinaryInputModel.Data.DeviceId, globalOccupancyBinaryInputModel.Data.ObjectId, BacnetObjectType.ObjectBinaryInput, BacnetPropertyID.PropDescription, GetZoneTypeKey(entityBaseModel) + "Zone Occupancy State", AppConstants.ZONE_OCCUPANCY_OBJECT, processorConfiguration, entityBaseModel);
            SetProperty(globalOccupancyBinaryInputModel.Data.DeviceId, globalOccupancyBinaryInputModel.Data.ObjectId, BacnetObjectType.ObjectBinaryInput, BacnetPropertyID.PropInactiveText, AppConstants.BINARY_OBJECT_INACTIVE_TEXT);
            SetProperty(globalOccupancyBinaryInputModel.Data.DeviceId, globalOccupancyBinaryInputModel.Data.ObjectId, BacnetObjectType.ObjectBinaryInput, BacnetPropertyID.PropActiveText, AppConstants.BINARY_OBJECT_ACTIVE_TEXT);
            SetProperty(globalOccupancyBinaryInputModel.Data.DeviceId, globalOccupancyBinaryInputModel.Data.ObjectId, BacnetObjectType.ObjectBinaryInput, BacnetPropertyID.PropNotifyType, processorConfiguration.BACnetConfigurationData.NotifyType);
            SetProperty(globalOccupancyBinaryInputModel.Data.DeviceId, globalOccupancyBinaryInputModel.Data.ObjectId, BacnetObjectType.ObjectBinaryInput, BacnetPropertyID.PropTimeDelay, processorConfiguration.BACnetConfigurationData.TimeDelay);
            eventTransitionBitsModel = GetEventTransitionBit(processorConfiguration.ZoneAlarmNotificationSetting, AppConstants.ZONE_OCCUPANCY_OBJECT);
            SetProperty(globalOccupancyBinaryInputModel.Data.DeviceId, globalOccupancyBinaryInputModel.Data.ObjectId, BacnetObjectType.ObjectBinaryInput, BacnetPropertyID.PropEventEnable, eventTransitionBitsModel);
            SetProperty(globalOccupancyBinaryInputModel.Data.DeviceId, globalOccupancyBinaryInputModel.Data.ObjectId, BacnetObjectType.ObjectBinaryInput, BacnetPropertyID.PropNotificationClass, processorConfiguration.VDNotificationClassInstanceNumber);
            SetProperty(globalOccupancyBinaryInputModel.Data.DeviceId, globalOccupancyBinaryInputModel.Data.ObjectId, BacnetObjectType.ObjectBinaryInput, BacnetPropertyID.PropProprietaryGridX, zoneEntityModel.ProprietaryGridX);
            SetProperty(globalOccupancyBinaryInputModel.Data.DeviceId, globalOccupancyBinaryInputModel.Data.ObjectId, BacnetObjectType.ObjectBinaryInput, BacnetPropertyID.PropProprietaryGridY, zoneEntityModel.ProprietaryGridY);
            SetProperty(globalOccupancyBinaryInputModel.Data.DeviceId, globalOccupancyBinaryInputModel.Data.ObjectId, BacnetObjectType.ObjectBinaryInput, BacnetPropertyID.PropDeviceType, AppConstants.ZONE_OCCUPANCY_OBJECT);
            SetPropertyAccessType(globalOccupancyBinaryInputModel.Data.DeviceId, globalOccupancyBinaryInputModel.Data.ObjectId, BacnetObjectType.ObjectBinaryInput, BacnetPropertyID.PropDeviceType, PropAccessType.NOT_SUPPORTED, true);

            #endregion

            #region AnalogValue-BioDynamic


            ResultModel<CreateObjectResultModel> globalBioDynamicAnalogModel = CreateObjectAndCacheData(entityBaseModel, processorConfiguration, BacnetObjectType.ObjectAnalogValue, AppResource.BioDynamicControlName, AppConstants.EntityTypes.Zone, 0, true);

            if (!globalBioDynamicAnalogModel.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.ERROR, "ZoneProcessor.ProcessEntity : Zone object creation failure in BACnet stack..");
                return globalBioDynamicAnalogModel.IsSuccess;
            }

            SetProperty(globalBioDynamicAnalogModel.Data.DeviceId, globalBioDynamicAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropDescription, GetZoneTypeKey(entityBaseModel) + AppResource.AnalogValueBioDynamicRepresentZone, AppResource.BioDynamicControlName, processorConfiguration, entityBaseModel);
            SetProperty(globalBioDynamicAnalogModel.Data.DeviceId, globalBioDynamicAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropUnits, BacnetEngineeringUnits.UnitsPercent);
            SetProperty(globalBioDynamicAnalogModel.Data.DeviceId, globalBioDynamicAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropRelinquishDefault, AppConstants.RELINQUISH_DEFAULT);
            SetProperty(globalBioDynamicAnalogModel.Data.DeviceId, globalBioDynamicAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropTimeDelay, processorConfiguration.BACnetConfigurationData.TimeDelay);
            SetProperty(globalBioDynamicAnalogModel.Data.DeviceId, globalBioDynamicAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropNotificationClass, processorConfiguration.VDNotificationClassInstanceNumber);
            SetProperty(globalBioDynamicAnalogModel.Data.DeviceId, globalBioDynamicAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropHighLimit, AppConstants.LIGHT_FIXTURE_HIGH_LIMIT);
            SetProperty(globalBioDynamicAnalogModel.Data.DeviceId, globalBioDynamicAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropProprietaryGridX, zoneEntityModel.ProprietaryGridX);
            SetProperty(globalBioDynamicAnalogModel.Data.DeviceId, globalBioDynamicAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropProprietaryGridY, zoneEntityModel.ProprietaryGridY);
            //SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropProprietaryColorTemperature, AppResource.FixtureColorTemperature);
            //SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropProprietaryColorTemperatureUnit, BacnetEngineeringUnits.UnitsDegreesKelvin);
            SetProperty(globalBioDynamicAnalogModel.Data.DeviceId, globalBioDynamicAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropNotifyType, processorConfiguration.BACnetConfigurationData.NotifyType);

            //Info: Default "Limit enable" property override with true flag
            SetProperty(globalBioDynamicAnalogModel.Data.DeviceId, globalBioDynamicAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropLimitEnable, limitEnableBitsModel);

            eventTransitionBitsModel = GetEventTransitionBit(processorConfiguration.ZoneAlarmNotificationSetting, AppConstants.LIGHTING_BRIGHTENESS_OBJECT);
            SetProperty(globalBioDynamicAnalogModel.Data.DeviceId, globalBioDynamicAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropEventEnable, eventTransitionBitsModel);
            #endregion AnalogValue

            #region AnalogValue-Saturation


            ResultModel<CreateObjectResultModel> globalSaturationAnalogModel = CreateObjectAndCacheData(entityBaseModel, processorConfiguration, BacnetObjectType.ObjectAnalogValue, AppResource.SaturationControlName, AppConstants.EntityTypes.Zone, 0, true);

            if (!globalSaturationAnalogModel.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.ERROR, "ZoneProcessor.ProcessEntity : Zone object creation failure in BACnet stack..");
                return globalSaturationAnalogModel.IsSuccess;
            }

            SetProperty(globalSaturationAnalogModel.Data.DeviceId, globalSaturationAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropDescription, GetZoneTypeKey(entityBaseModel) + AppResource.AnalogValueSaturationRepresentZone, AppResource.SaturationControlName, processorConfiguration, entityBaseModel);
            SetProperty(globalSaturationAnalogModel.Data.DeviceId, globalSaturationAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropUnits, BacnetEngineeringUnits.UnitsPercent);
            SetProperty(globalSaturationAnalogModel.Data.DeviceId, globalSaturationAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropRelinquishDefault, AppConstants.RELINQUISH_DEFAULT);
            SetProperty(globalSaturationAnalogModel.Data.DeviceId, globalSaturationAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropTimeDelay, processorConfiguration.BACnetConfigurationData.TimeDelay);
            SetProperty(globalSaturationAnalogModel.Data.DeviceId, globalSaturationAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropNotificationClass, processorConfiguration.VDNotificationClassInstanceNumber);
            SetProperty(globalSaturationAnalogModel.Data.DeviceId, globalSaturationAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropHighLimit, AppConstants.LIGHT_FIXTURE_HIGH_LIMIT);
            SetProperty(globalSaturationAnalogModel.Data.DeviceId, globalSaturationAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropProprietaryGridX, zoneEntityModel.ProprietaryGridX);
            SetProperty(globalSaturationAnalogModel.Data.DeviceId, globalSaturationAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropProprietaryGridY, zoneEntityModel.ProprietaryGridY);
            //SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropProprietaryColorTemperature, AppResource.FixtureColorTemperature);
            //SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropProprietaryColorTemperatureUnit, BacnetEngineeringUnits.UnitsDegreesKelvin);
            SetProperty(globalSaturationAnalogModel.Data.DeviceId, globalSaturationAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropNotifyType, processorConfiguration.BACnetConfigurationData.NotifyType);

            //Info: Default "Limit enable" property override with true flag
            SetProperty(globalSaturationAnalogModel.Data.DeviceId, globalSaturationAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropLimitEnable, limitEnableBitsModel);

            eventTransitionBitsModel = GetEventTransitionBit(processorConfiguration.ZoneAlarmNotificationSetting, AppConstants.LIGHTING_BRIGHTENESS_OBJECT);
            SetProperty(globalSaturationAnalogModel.Data.DeviceId, globalSaturationAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropEventEnable, eventTransitionBitsModel);
            #endregion AnalogValue

            #region MultiStateValue


            ResultModel<CreateObjectResultModel> globalMultiStateValue = CreateObjectAndCacheData(entityBaseModel, processorConfiguration, BacnetObjectType.ObjectMultiStateValue, AppConstants.LIGHTING_LIGHTSCENE_OBJECT, AppConstants.EntityTypes.Zone);


            if (!globalMultiStateValue.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.DEBUG, "ZoneProcessor:ProcessEntity: create object and cache data failed for " + BacnetObjectType.ObjectMultiStateValue);
                return globalMultiStateValue.IsSuccess;
            }

            SetProperty(globalMultiStateValue.Data.DeviceId, globalMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropDescription, GetZoneTypeKey(entityBaseModel) + AppResource.MultiStateValueRepresentZone, AppConstants.LIGHTING_LIGHTSCENE_OBJECT, processorConfiguration, entityBaseModel);
            SetProperty(globalMultiStateValue.Data.DeviceId, globalMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropNumberOfStates, processorConfiguration.LightScene.Length);
            SetProperty(globalMultiStateValue.Data.DeviceId, globalMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropTimeDelay, processorConfiguration.BACnetConfigurationData.TimeDelay);
            SetProperty(globalMultiStateValue.Data.DeviceId, globalMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropNotificationClass, processorConfiguration.VDNotificationClassInstanceNumber);
            SetProperty(globalMultiStateValue.Data.DeviceId, globalMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropRelinquishDefault, 1);//Array.IndexOf(AppCoreRepo.Instance.UserAndGeneralLightScene, AppConstants.DEFAULT_LightScene) + 1
            //SetProperty(globalMultiStateValue.Data.DeviceId, globalMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropProprietaryMood, Array.IndexOf(AppCoreRepo.Instance.AvailableMoods, AppConstants.DEFAULT_MOOD));
            SetProperty(globalMultiStateValue.Data.DeviceId, globalMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropNotifyType, processorConfiguration.BACnetConfigurationData.NotifyType);

            eventTransitionBitsModel = GetEventTransitionBit(processorConfiguration.ZoneAlarmNotificationSetting, AppConstants.LIGHTING_LIGHTSCENE_OBJECT);
            SetProperty(globalMultiStateValue.Data.DeviceId, globalMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropEventEnable, eventTransitionBitsModel);

            SetPropertyAccessType(globalMultiStateValue.Data.DeviceId, globalMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropProprietaryMood, PropAccessType.NOT_SUPPORTED, true);
            // State-Text SetProperties 
            ResultModel<bool> StateTextResult = SetMSVStateText(globalMultiStateValue, processorConfiguration.UserAndGeneralLightScenes);
            if (!StateTextResult.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.ERROR, "ZoneProcessor.ProcessEntity: State-Text SetProperties failed for Multistate Value Id " + globalMultiStateValue.Data.DeviceId);
            }
            #endregion MultiStateValue

            if (entityBaseModel as ZoneEntityModel != null && (entityBaseModel as ZoneEntityModel).ZoneType == AppConstants.ZoneTypes.UserSpace)
            {
                #region MultiStateValue for MOOD
                ResultModel<CreateObjectResultModel> globalMoodMultiStateValue = CreateObjectAndCacheData(entityBaseModel, processorConfiguration, BacnetObjectType.ObjectMultiStateValue, AppConstants.LIGHTING_MOOD_OBJECT, AppConstants.EntityTypes.Zone);


                if (!globalMoodMultiStateValue.IsSuccess)
                {
                    Logger.Instance.Log(LogLevel.DEBUG, "ZoneProcessor:ProcessEntity: create object and cache data failed for " + BacnetObjectType.ObjectMultiStateValue);
                    return globalMultiStateValue.IsSuccess;
                }

                SetProperty(globalMoodMultiStateValue.Data.DeviceId, globalMoodMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropDescription, GetZoneTypeKey(entityBaseModel) + AppResource.MoodMultiStateValueRepresentZone, AppConstants.LIGHTING_MOOD_OBJECT, processorConfiguration, entityBaseModel);
                SetProperty(globalMoodMultiStateValue.Data.DeviceId, globalMoodMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropNumberOfStates, AppCoreRepo.Instance.AvailableMoods.Length);
                SetProperty(globalMoodMultiStateValue.Data.DeviceId, globalMoodMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropTimeDelay, processorConfiguration.BACnetConfigurationData.TimeDelay);
                SetProperty(globalMoodMultiStateValue.Data.DeviceId, globalMoodMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropNotificationClass, processorConfiguration.VDNotificationClassInstanceNumber);
                SetProperty(globalMoodMultiStateValue.Data.DeviceId, globalMoodMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropRelinquishDefault, 1);//Array.IndexOf(AppCoreRepo.Instance.UserAndGeneralLightScene, AppConstants.DEFAULT_LightScene) + 1
                //SetProperty(globalMoodMultiStateValue.Data.DeviceId, globalMoodMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropProprietaryMood, Array.IndexOf(AppCoreRepo.Instance.AvailableMoods, AppConstants.DEFAULT_MOOD));
                SetProperty(globalMoodMultiStateValue.Data.DeviceId, globalMoodMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropNotifyType, processorConfiguration.BACnetConfigurationData.NotifyType);

                eventTransitionBitsModel = GetEventTransitionBit(processorConfiguration.ZoneAlarmNotificationSetting, AppConstants.LIGHTING_MOOD_OBJECT);
                SetProperty(globalMoodMultiStateValue.Data.DeviceId, globalMoodMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropEventEnable, eventTransitionBitsModel);

                SetPropertyAccessType(globalMoodMultiStateValue.Data.DeviceId, globalMoodMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropProprietaryMood, PropAccessType.NOT_SUPPORTED, true);
                // State-Text SetProperties 
                ResultModel<bool> MoodStateTextResult = SetMSVStateText(globalMoodMultiStateValue, AppCoreRepo.Instance.AvailableMoods);
                if (!MoodStateTextResult.IsSuccess)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "ZoneProcessor.ProcessEntity: State-Text SetProperties failed for Multistate Value Id " + globalMultiStateValue.Data.DeviceId);
                }
                #endregion
            }

            #region MultiStateValue for 'ResetAfterTimeout'

            ResultModel<CreateObjectResultModel> resetAfterTOmsvObj = CreateObjectAndCacheData(entityBaseModel, processorConfiguration, BacnetObjectType.ObjectMultiStateValue, AppConstants.RESET_ATTRIBUTES_AFTER_TIMEOUT, AppConstants.EntityTypes.Zone);


            if (!resetAfterTOmsvObj.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.DEBUG, "ZoneProcessor:ProcessEntity: create object and cache data failed for " + BacnetObjectType.ObjectMultiStateValue);
                return resetAfterTOmsvObj.IsSuccess;
            }

            SetProperty(resetAfterTOmsvObj.Data.DeviceId, resetAfterTOmsvObj.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropDescription, string.Format("{0} {1}", GetZoneTypeKey(entityBaseModel), entityBaseModel.EntityName), AppConstants.RESET_ATTRIBUTES_AFTER_TIMEOUT, processorConfiguration, entityBaseModel);
            SetProperty(resetAfterTOmsvObj.Data.DeviceId, resetAfterTOmsvObj.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropNumberOfStates, AppCoreRepo.Instance.AvailableResetAttributeComboDescriptions.Length);
            SetProperty(resetAfterTOmsvObj.Data.DeviceId, resetAfterTOmsvObj.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropNotifyType, processorConfiguration.BACnetConfigurationData.NotifyType);
            SetProperty(resetAfterTOmsvObj.Data.DeviceId, resetAfterTOmsvObj.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropTimeDelay, processorConfiguration.BACnetConfigurationData.TimeDelay);
            SetProperty(resetAfterTOmsvObj.Data.DeviceId, resetAfterTOmsvObj.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropNotificationClass, processorConfiguration.VDNotificationClassInstanceNumber);
            SetProperty(resetAfterTOmsvObj.Data.DeviceId, resetAfterTOmsvObj.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropRelinquishDefault, 1);//Array.IndexOf(AppCoreRepo.Instance.UserAndGeneralLightScene, AppConstants.DEFAULT_LightScene) + 1
            //SetProperty(resetAfterTOmsvObj.Data.DeviceId, resetAfterTOmsvObj.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropProprietaryMood, Array.IndexOf(AppCoreRepo.Instance.AvailableMoods, AppConstants.DEFAULT_MOOD));

            eventTransitionBitsModel = GetEventTransitionBit(processorConfiguration.ZoneAlarmNotificationSetting, AppConstants.RESET_ATTRIBUTES_AFTER_TIMEOUT);
            SetProperty(resetAfterTOmsvObj.Data.DeviceId, resetAfterTOmsvObj.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropEventEnable, eventTransitionBitsModel);

            // State-Text SetProperties 
            ResultModel<bool> setStateTextResult = SetMSVStateText(resetAfterTOmsvObj, AppCoreRepo.Instance.AvailableResetAttributeComboDescriptions);
            if (!setStateTextResult.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.ERROR, "ZoneProcessor.ProcessEntity: State-Text SetProperties failed for Multistate Value Id " + resetAfterTOmsvObj.Data.DeviceId);
            }

            #endregion

            #region Zone level Average sensor objects
            #region Analog Input Object
            int initialAvgSensorObjectID = 0;
            if (zoneEntityModel.Sensors.Any(x => x.SensorType == AppConstants.SensorType.LuxLevel))
            {
                initialAvgSensorObjectID = AppConstants.INITIAL_AVERAGE_SENSOR_AI_OBJECT_ID;
                eventTransitionBitsModel = GetEventTransitionBit(processorConfiguration.ZoneAlarmNotificationSetting, AppConstants.SensorType.LuxLevel.ToString());
                ProcessZoneAverageSensorEntity(zoneEntityModel, processorConfiguration, eventTransitionBitsModel, AppConstants.SensorType.LuxLevel, initialAvgSensorObjectID);
            }
            if (zoneEntityModel.Sensors.Any(x => x.SensorType == AppConstants.SensorType.AirQuality))
            {
                initialAvgSensorObjectID = AppConstants.INITIAL_AVERAGE_SENSOR_AI_OBJECT_ID + 1;
                eventTransitionBitsModel = GetEventTransitionBit(processorConfiguration.ZoneAlarmNotificationSetting, AppConstants.SensorType.AirQuality.ToString());
                ProcessZoneAverageSensorEntity(zoneEntityModel, processorConfiguration, eventTransitionBitsModel, AppConstants.SensorType.AirQuality, initialAvgSensorObjectID);
            }

            if (zoneEntityModel.Sensors.Any(x => x.SensorType == AppConstants.SensorType.ColorTemperature))
            {
                initialAvgSensorObjectID = AppConstants.INITIAL_AVERAGE_SENSOR_AI_OBJECT_ID + 2;
                eventTransitionBitsModel = GetEventTransitionBit(processorConfiguration.ZoneAlarmNotificationSetting, AppConstants.SensorType.ColorTemperature.ToString());
                ProcessZoneAverageSensorEntity(zoneEntityModel, processorConfiguration, eventTransitionBitsModel, AppConstants.SensorType.ColorTemperature, initialAvgSensorObjectID);
            }
            if (zoneEntityModel.Sensors.Any(x => x.SensorType == AppConstants.SensorType.Power))
            {
                initialAvgSensorObjectID = AppConstants.INITIAL_AVERAGE_SENSOR_AI_OBJECT_ID + 3;
                eventTransitionBitsModel = GetEventTransitionBit(processorConfiguration.ZoneAlarmNotificationSetting, AppConstants.SensorType.Power.ToString());
                ProcessZoneAverageSensorEntity(zoneEntityModel, processorConfiguration, eventTransitionBitsModel, AppConstants.SensorType.Power, initialAvgSensorObjectID);
            }
            if (zoneEntityModel.Sensors.Any(x => x.SensorType == AppConstants.SensorType.Temperature))
            {
                initialAvgSensorObjectID = AppConstants.INITIAL_AVERAGE_SENSOR_AI_OBJECT_ID + 4;
                eventTransitionBitsModel = GetEventTransitionBit(processorConfiguration.ZoneAlarmNotificationSetting, AppConstants.SensorType.Temperature.ToString());
                ProcessZoneAverageSensorEntity(zoneEntityModel, processorConfiguration, eventTransitionBitsModel, AppConstants.SensorType.Temperature, initialAvgSensorObjectID);
            }
            if (zoneEntityModel.Sensors.Any(x => x.SensorType == AppConstants.SensorType.Humidity))
            {
                initialAvgSensorObjectID = AppConstants.INITIAL_AVERAGE_SENSOR_AI_OBJECT_ID + 5;
                eventTransitionBitsModel = GetEventTransitionBit(processorConfiguration.ZoneAlarmNotificationSetting, AppConstants.SensorType.Humidity.ToString());
                ProcessZoneAverageSensorEntity(zoneEntityModel, processorConfiguration, eventTransitionBitsModel, AppConstants.SensorType.Humidity, initialAvgSensorObjectID);
            }
            #endregion Analog Input Object

            #region Binary Input Object
            //FOR BINARY VALUE OBJECTS THERE IS SEPERATE OBJECT ID RANGE 13751-14500
            if (zoneEntityModel.Sensors.Any(x => x.SensorType == AppConstants.SensorType.Presence))
            {
                initialAvgSensorObjectID = AppConstants.INITIAL_AVERAGE_SENSOR_BI_OBJECT_ID;
                eventTransitionBitsModel = GetEventTransitionBit(processorConfiguration.ZoneAlarmNotificationSetting, AppConstants.SensorType.Presence.ToString());
                ProcessZoneAverageSensorEntity(zoneEntityModel, processorConfiguration, eventTransitionBitsModel, AppConstants.SensorType.Presence, initialAvgSensorObjectID);
            }
            //zoneEntityModel extract each sensor single instance and create its zone level object.
            #endregion Binary Input Object

            #endregion

            #region Set Zone Level Properties
            string key = EntityHelper.GetObjectDetailsKey(BacnetObjectType.ObjectDevice, AppConstants.LIGHTING_OBJECTDEVICE_OBJECT);

            string deviceName = GetObjectName(processorConfiguration.EntityBACnetCache, entityBaseModel, key, BacnetObjectType.ObjectDevice, deviceId);

            SetProperty(deviceId, deviceId, BacnetObjectType.ObjectDevice, BacnetPropertyID.PropObjectName, deviceName);
            SetProperty(deviceId, deviceId, BacnetObjectType.ObjectDevice, BacnetPropertyID.PropDescription, bacnetConfigurationModel.Description, AppConstants.LIGHTING_OBJECTDEVICE_OBJECT, processorConfiguration, entityBaseModel);
            SetProperty(deviceId, deviceId, BacnetObjectType.ObjectDevice, BacnetPropertyID.PropDayLightSavingsStatus, bacnetConfigurationModel.DaylightSavingStatus);
            SetProperty(deviceId, deviceId, BacnetObjectType.ObjectDevice, BacnetPropertyID.PropUtcOffSet, bacnetConfigurationModel.UTCOffset);
            SetProperty(deviceId, deviceId, BacnetObjectType.ObjectDevice, BacnetPropertyID.PropApduTimesOut, bacnetConfigurationModel.APDUTimeout);
            SetProperty(deviceId, deviceId, BacnetObjectType.ObjectDevice, BacnetPropertyID.PropApduSegmentTimesOut, bacnetConfigurationModel.APDUSegmentTimeout);
            SetProperty(deviceId, deviceId, BacnetObjectType.ObjectDevice, BacnetPropertyID.PropNumberOfApduRetries, bacnetConfigurationModel.APDURetries);
            SetProperty(deviceId, deviceId, BacnetObjectType.ObjectDevice, BacnetPropertyID.PropLocation, processorConfiguration.Location);
            SetProperty(deviceId, deviceId, BacnetObjectType.ObjectDevice, BacnetPropertyID.PropApplicationSoftwareVersion, AppCoreRepo.Instance.ApplicationSoftwareVersion);

            #endregion Set Zone Level Properties

            if (bacnetConfigurationModel.MapIndividualFixture)
            {
                Logger.Instance.Log(LogLevel.DEBUG, "ZoneProcessor.ProcessEntity: Individual level Fixture Mapping is enabled ");

                FixtureProcessor fixtureProcessor = new FixtureProcessor();
                bool isSuccess = true;

                foreach (var item in zoneEntityModel.Fixtures)
                {
                    isSuccess = fixtureProcessor.ProcessEntity(item, processorConfiguration);

                    if (!isSuccess)
                    {
                        Logger.Instance.Log(LogLevel.DEBUG, "ZoneProcessor.ProcessEntity : Fixture Name " + item.EntityName);
                        continue;
                    }
                }
            }

            if (bacnetConfigurationModel.MapIndividualSensors)
            {
                Logger.Instance.Log(LogLevel.DEBUG, "ZoneProcessor.ProcessEntity: Individual level Sensor Mapping is enabled ");

                SensorProcessor sensorProcessor = new SensorProcessor();
                bool isSuccess = true;

                foreach (var item in zoneEntityModel.Sensors)
                {
                    isSuccess = sensorProcessor.ProcessEntity(item, processorConfiguration);

                    if (!isSuccess)
                    {
                        Logger.Instance.Log(LogLevel.DEBUG, "ZoneProcessor.ProcessEntity : Sensor Name " + item.EntityName);
                        continue;
                    }
                }
            }

            #region PositiveIntergerValue


            if (zoneEntityModel.Sensors.Any(x => x.SensorType == AppConstants.SensorType.LuxLevel))
            {
                #region Properties for DHTargetLux

                ResultModel<CreateObjectResultModel> pivResultModel = CreateObjectAndCacheData(zoneEntityModel, processorConfiguration, BacnetObjectType.ObjectPositiveIntegerValue, AppConstants.PositiveIntegerValueType.DHTargetLux.ToString(), AppConstants.EntityTypes.Zone);

                if (!pivResultModel.IsSuccess)
                {
                    Logger.Instance.Log(LogLevel.DEBUG, "SensorProcessor.ProcessEntity: Create object and cache data failed for postive interger value");
                    return false;
                }

                SetProperty(pivResultModel.Data.DeviceId, pivResultModel.Data.ObjectId, BacnetObjectType.ObjectPositiveIntegerValue, BacnetPropertyID.PropDescription, GetZoneTypeKey(entityBaseModel) + AppResource.PIVDHTargetRepresent, AppConstants.LIGHTING_DHTARGETLUX_OBJECT, processorConfiguration, entityBaseModel);
                SetProperty(pivResultModel.Data.DeviceId, pivResultModel.Data.ObjectId, BacnetObjectType.ObjectPositiveIntegerValue, BacnetPropertyID.PropTimeDelay, processorConfiguration.BACnetConfigurationData.TimeDelay);
                SetProperty(pivResultModel.Data.DeviceId, pivResultModel.Data.ObjectId, BacnetObjectType.ObjectPositiveIntegerValue, BacnetPropertyID.PropUnits, BacnetEngineeringUnits.UnitsLuxes);
                SetProperty(pivResultModel.Data.DeviceId, pivResultModel.Data.ObjectId, BacnetObjectType.ObjectPositiveIntegerValue, BacnetPropertyID.PropNotificationClass, processorConfiguration.VDNotificationClassInstanceNumber);
                SetProperty(pivResultModel.Data.DeviceId, pivResultModel.Data.ObjectId, BacnetObjectType.ObjectPositiveIntegerValue, BacnetPropertyID.PropNotifyType, processorConfiguration.BACnetConfigurationData.NotifyType);
                SetProperty(pivResultModel.Data.DeviceId, pivResultModel.Data.ObjectId, BacnetObjectType.ObjectPositiveIntegerValue, BacnetPropertyID.PropHighLimit, AppConstants.PIV_DHTARGET_LUX_HIGH_LIMIT);
                SetProperty(pivResultModel.Data.DeviceId, pivResultModel.Data.ObjectId, BacnetObjectType.ObjectPositiveIntegerValue, BacnetPropertyID.PropLowLimit, AppConstants.PIV_DHTARGET_LUX_LOW_LIMIT);
                SetProperty(pivResultModel.Data.DeviceId, pivResultModel.Data.ObjectId, BacnetObjectType.ObjectPositiveIntegerValue, BacnetPropertyID.PropRelinquishDefault, AppConstants.PIV_DEFAULT_TARGET_LUX_LEVEL);

                //Info: Default "Limit enable" property override with true flag
                SetProperty(pivResultModel.Data.DeviceId, pivResultModel.Data.ObjectId, BacnetObjectType.ObjectPositiveIntegerValue, BacnetPropertyID.PropLimitEnable, limitEnableBitsModel);


                eventTransitionBitsModel = GetEventTransitionBit(processorConfiguration.ZoneAlarmNotificationSetting, AppConstants.LIGHTING_DHTARGETLUX_OBJECT.ToString());

                SetProperty(pivResultModel.Data.DeviceId, pivResultModel.Data.ObjectId, BacnetObjectType.ObjectPositiveIntegerValue, BacnetPropertyID.PropEventEnable, eventTransitionBitsModel);

                #endregion Properties for DHTargetLux
            }

            if (zoneEntityModel.Sensors.Any(x => x.SensorType == AppConstants.SensorType.Presence))
            {

                #region Properties for PresTimeout

                ResultModel<CreateObjectResultModel> pivPresenceResultModel = CreateObjectAndCacheData(zoneEntityModel, processorConfiguration, BacnetObjectType.ObjectPositiveIntegerValue, AppConstants.PositiveIntegerValueType.OccupancyTimeOut.ToString(), AppConstants.EntityTypes.Zone);

                if (!pivPresenceResultModel.IsSuccess)
                {
                    Logger.Instance.Log(LogLevel.DEBUG, "SensorProcessor.ProcessEntity: Create object and cache data failed for postive interger value");
                    return false;
                }

                SetProperty(pivPresenceResultModel.Data.DeviceId, pivPresenceResultModel.Data.ObjectId, BacnetObjectType.ObjectPositiveIntegerValue, BacnetPropertyID.PropDescription, GetZoneTypeKey(entityBaseModel) + AppResource.PIVPresTimeOutRepresent, AppConstants.LIGHTING_PRESTIMEOUT_OBJECT, processorConfiguration, entityBaseModel);
                SetProperty(pivPresenceResultModel.Data.DeviceId, pivPresenceResultModel.Data.ObjectId, BacnetObjectType.ObjectPositiveIntegerValue, BacnetPropertyID.PropTimeDelay, processorConfiguration.BACnetConfigurationData.TimeDelay);
                SetProperty(pivPresenceResultModel.Data.DeviceId, pivPresenceResultModel.Data.ObjectId, BacnetObjectType.ObjectPositiveIntegerValue, BacnetPropertyID.PropUnits, BacnetEngineeringUnits.UnitsSeconds);
                SetProperty(pivPresenceResultModel.Data.DeviceId, pivPresenceResultModel.Data.ObjectId, BacnetObjectType.ObjectPositiveIntegerValue, BacnetPropertyID.PropNotificationClass, processorConfiguration.VDNotificationClassInstanceNumber);
                SetProperty(pivPresenceResultModel.Data.DeviceId, pivPresenceResultModel.Data.ObjectId, BacnetObjectType.ObjectPositiveIntegerValue, BacnetPropertyID.PropNotifyType, processorConfiguration.BACnetConfigurationData.NotifyType);
                SetProperty(pivPresenceResultModel.Data.DeviceId, pivPresenceResultModel.Data.ObjectId, BacnetObjectType.ObjectPositiveIntegerValue, BacnetPropertyID.PropHighLimit, AppConstants.PIV_TIME_OUT_HIGH_LIMIT);
                SetProperty(pivPresenceResultModel.Data.DeviceId, pivPresenceResultModel.Data.ObjectId, BacnetObjectType.ObjectPositiveIntegerValue, BacnetPropertyID.PropLowLimit, AppConstants.PIV_TIME_OUT_LOW_LIMIT);
                SetProperty(pivPresenceResultModel.Data.DeviceId, pivPresenceResultModel.Data.ObjectId, BacnetObjectType.ObjectPositiveIntegerValue, BacnetPropertyID.PropRelinquishDefault, AppConstants.PIV_DEFAULT_OCCUPANCY_TIME_OUT);

                //Info: Default "Limit enable" property override with true flag
                SetProperty(pivPresenceResultModel.Data.DeviceId, pivPresenceResultModel.Data.ObjectId, BacnetObjectType.ObjectPositiveIntegerValue, BacnetPropertyID.PropLimitEnable, limitEnableBitsModel);

                eventTransitionBitsModel = GetEventTransitionBit(processorConfiguration.ZoneAlarmNotificationSetting, AppConstants.LIGHTING_PRESTIMEOUT_OBJECT.ToString());

                SetProperty(pivPresenceResultModel.Data.DeviceId, pivPresenceResultModel.Data.ObjectId, BacnetObjectType.ObjectPositiveIntegerValue, BacnetPropertyID.PropEventEnable, eventTransitionBitsModel);


                #endregion Properties for PresTimeout

                #region Properties for PresRate

                ResultModel<CreateObjectResultModel> pivPresRateResultModel = CreateObjectAndCacheData(zoneEntityModel, processorConfiguration, BacnetObjectType.ObjectPositiveIntegerValue, AppConstants.PositiveIntegerValueType.OccupancyFadeOutTime.ToString(), AppConstants.EntityTypes.Zone);

                if (!pivPresenceResultModel.IsSuccess)
                {
                    Logger.Instance.Log(LogLevel.DEBUG, "SensorProcessor.ProcessEntity: Create object and cache data failed for postive interger value");
                    return false;
                }

                SetProperty(pivPresRateResultModel.Data.DeviceId, pivPresRateResultModel.Data.ObjectId, BacnetObjectType.ObjectPositiveIntegerValue, BacnetPropertyID.PropDescription, GetZoneTypeKey(entityBaseModel) + AppResource.PIVPresRateRepresent, AppConstants.LIGHTING_PRESRATE_OBJECT, processorConfiguration, entityBaseModel);
                SetProperty(pivPresRateResultModel.Data.DeviceId, pivPresRateResultModel.Data.ObjectId, BacnetObjectType.ObjectPositiveIntegerValue, BacnetPropertyID.PropTimeDelay, processorConfiguration.BACnetConfigurationData.TimeDelay);
                SetProperty(pivPresRateResultModel.Data.DeviceId, pivPresRateResultModel.Data.ObjectId, BacnetObjectType.ObjectPositiveIntegerValue, BacnetPropertyID.PropUnits, BacnetEngineeringUnits.UnitsSeconds);
                SetProperty(pivPresRateResultModel.Data.DeviceId, pivPresRateResultModel.Data.ObjectId, BacnetObjectType.ObjectPositiveIntegerValue, BacnetPropertyID.PropNotificationClass, processorConfiguration.VDNotificationClassInstanceNumber);
                SetProperty(pivPresRateResultModel.Data.DeviceId, pivPresRateResultModel.Data.ObjectId, BacnetObjectType.ObjectPositiveIntegerValue, BacnetPropertyID.PropNotifyType, processorConfiguration.BACnetConfigurationData.NotifyType);
                SetProperty(pivPresRateResultModel.Data.DeviceId, pivPresRateResultModel.Data.ObjectId, BacnetObjectType.ObjectPositiveIntegerValue, BacnetPropertyID.PropHighLimit, AppConstants.PIV_FADE_OUT_HIGH_LIMIT);
                SetProperty(pivPresRateResultModel.Data.DeviceId, pivPresRateResultModel.Data.ObjectId, BacnetObjectType.ObjectPositiveIntegerValue, BacnetPropertyID.PropLowLimit, AppConstants.PIV_FADE_OUT_LOW_LIMIT);
                SetProperty(pivPresRateResultModel.Data.DeviceId, pivPresenceResultModel.Data.ObjectId, BacnetObjectType.ObjectPositiveIntegerValue, BacnetPropertyID.PropRelinquishDefault, AppConstants.PIV_DEFAULT_OCCUPANCY_FADE_OUT);

                //Info: Default "Limit enable" property override with true flag
                SetProperty(pivPresRateResultModel.Data.DeviceId, pivPresRateResultModel.Data.ObjectId, BacnetObjectType.ObjectPositiveIntegerValue, BacnetPropertyID.PropLimitEnable, limitEnableBitsModel);

                eventTransitionBitsModel = GetEventTransitionBit(processorConfiguration.ZoneAlarmNotificationSetting, AppConstants.LIGHTING_PRESRATE_OBJECT.ToString());
                SetProperty(pivPresRateResultModel.Data.DeviceId, pivPresRateResultModel.Data.ObjectId, BacnetObjectType.ObjectPositiveIntegerValue, BacnetPropertyID.PropEventEnable, eventTransitionBitsModel);

                #endregion Properties for PresRate
            }

            #endregion PositiveIntergerValue

            #region BinaryValue
            ResultModel<CreateObjectResultModel> globalBinaryValueModel = CreateObjectAndCacheData(entityBaseModel, processorConfiguration, BacnetObjectType.ObjectBinaryValue, AppConstants.ZONE_ONOFF_OBJECT, AppConstants.EntityTypes.Zone);

            SetProperty(globalBinaryValueModel.Data.DeviceId, globalBinaryValueModel.Data.ObjectId, BacnetObjectType.ObjectBinaryValue, BacnetPropertyID.PropDescription, GetZoneTypeKey(entityBaseModel) + AppResource.BinaryValueRepresent, AppConstants.ZONE_ONOFF_OBJECT, processorConfiguration, entityBaseModel);
            SetProperty(globalBinaryValueModel.Data.DeviceId, globalBinaryValueModel.Data.ObjectId, BacnetObjectType.ObjectBinaryValue, BacnetPropertyID.PropNotifyType, processorConfiguration.BACnetConfigurationData.NotifyType);
            SetProperty(globalBinaryValueModel.Data.DeviceId, globalBinaryValueModel.Data.ObjectId, BacnetObjectType.ObjectBinaryValue, BacnetPropertyID.PropNotificationClass, processorConfiguration.VDNotificationClassInstanceNumber);
            SetProperty(globalBinaryValueModel.Data.DeviceId, globalBinaryValueModel.Data.ObjectId, BacnetObjectType.ObjectBinaryValue, BacnetPropertyID.PropTimeDelay, processorConfiguration.BACnetConfigurationData.TimeDelay);
            SetProperty(globalBinaryValueModel.Data.DeviceId, globalBinaryValueModel.Data.ObjectId, BacnetObjectType.ObjectBinaryValue, BacnetPropertyID.PropAlarmValue, BacnetBinaryPV.BinaryInactive);

            eventTransitionBitsModel = GetEventTransitionBit(processorConfiguration.ZoneAlarmNotificationSetting, AppConstants.ZONE_ONOFF_OBJECT.ToString());

            SetProperty(globalBinaryValueModel.Data.DeviceId, globalBinaryValueModel.Data.ObjectId, BacnetObjectType.ObjectBinaryValue, BacnetPropertyID.PropEventEnable, eventTransitionBitsModel);

            #endregion

            Logger.Instance.Log(LogLevel.DEBUG, "ZoneProcessor.ProcessEntity : Processing Zone Completed");

            return true;
        }

        #region PrivateMethods

        /// <summary>
        /// Gets the event transition bit.
        /// </summary>
        /// <returns></returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy>
        /// <TimeStamp>4/11/20186:36 PM</TimeStamp>
        private static EventTransitionBitsModel GetEventTransitionBit(List<ZoneObject> AlarmNotificationSetting, string entity)
        {
            EventTransitionBitsModel eventTransitionBitsModel = new EventTransitionBitsModel();
            if (AlarmNotificationSetting.Any(x => x.Name == entity && x.ZoneValue))
            {
                eventTransitionBitsModel.ToFault = true;
                eventTransitionBitsModel.ToNormal = true;
                eventTransitionBitsModel.ToOffNormal = true;
            }
            else
            {
                eventTransitionBitsModel.ToFault = false;
                eventTransitionBitsModel.ToNormal = false;
                eventTransitionBitsModel.ToOffNormal = false;
            }

            return eventTransitionBitsModel;
        }

        /// <summary>
        /// Verifies the device identifier. Verifies with mapped ids to not duplicate the ids.
        /// </summary>
        /// <param name="processorConfiguration">The processor configuration.</param>
        /// <param name="deviceId">The device identifier.</param>
        /// <returns></returns>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>6/27/20174:18 PM</TimeStamp>
        private static uint VerifyDeviceId(Models.ProcessorConfigurationModel processorConfiguration, uint deviceId)
        {
            if (processorConfiguration.MappedObjectIds != null && processorConfiguration.MappedObjectIds.ContainsKey(deviceId))
            {
                deviceId = (uint)ObjectIdentifierManager.Instance.GetDeviceId(deviceId);
                deviceId = VerifyDeviceId(processorConfiguration, deviceId);
            }
            return deviceId;
        }

        private string GetDeviceType(ZoneEntityModel entityBaseModel, AppConstants.SensorType sensorType)
        {
            SensorEntityModel sensorModel = entityBaseModel.Sensors.Where(x => x.SensorType == sensorType).FirstOrDefault();
            return string.Format("{0} {1}", sensorModel.DeviceType, sensorModel.Role);
        }
        #endregion PrivateMethods

    }
}
