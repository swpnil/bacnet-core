﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              <PostProcessor.cs>
///   Description:        <This class is used for updating the properties after all Objects initialised>
///   Author:             <Rupesh Saw>                  
///   Date:               MM/DD/YY
#endregion

using Molex.BACnet.Gateway.Log;
using Molex.BACnet.Gateway.ServiceCore.Constants;
using Molex.BACnet.Gateway.ServiceCore.Managers;
using Molex.BACnet.Gateway.ServiceCore.Models;
using Molex.BACnet.Gateway.ServiceCore.Models.Entity;
using Molex.BACnet.Gateway.ServiceCore.Repository;
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Response;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnet.Gateway.ServiceCore.Processor
{
    /// <summary>
    /// This class is used for updating the properties after all Objects initialised
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>26-06-201701:34 PM</TimeStamp>
    /// <seealso cref="Molex.BACnet.Gateway.ServiceCore.Processor.ProcessorBase" />
    public class PostProcessor : ProcessorBase
    {
        /// <summary>
        /// This will set ProtocolObjectTypesSuppots and ProtocolServiceSupport properties for Controller and Device object
        /// </summary>
        /// <param name="entityBaseModel"></param>
        /// <param name="processorConfiguration"></param>
        /// <returns>Sucess/Falied</returns>
        public override bool ProcessEntity(Models.Entity.EntityBaseModel entityBaseModel, Models.ProcessorConfigurationModel processorConfiguration)
        {
            Logger.Instance.Log(LogLevel.DEBUG, "PostProcessor.ProcessEntity: Set Properties Initiatiated for POTS and PSS ");

            BACnetConfigurationModel bacnetConfigurationModel = processorConfiguration.BACnetConfigurationData;
            ResultModel<Dictionary<string, EntityBaseModel>> result = new ResultModel<Dictionary<string, EntityBaseModel>>();
            Dictionary<string, EntityBaseModel> zonesLookUp = AppCoreRepo.Instance.ZonesLookUp;

            // ProtocolObjectTypesSupported Set Properties defined for Virtual Router (Controller)
            VRDProtocolObjectTypesSupported(bacnetConfigurationModel);
            //ProtocolObjectServiceSupported Set Properties defined for Virtula Router (Controller)
            VRDProtocolServiceSupported(bacnetConfigurationModel);
            // ProtocolObjectTypesSupported Set Properties defined for Virtual Device (Zone Level)
            VDProtocolObjectTypesSupported(bacnetConfigurationModel, zonesLookUp);
            // ProtocolObjectTypesSupported Set Properties defined for Virtual Router (Controller)
            VDProtocolServiceSupported(bacnetConfigurationModel, zonesLookUp);

            Logger.Instance.Log(LogLevel.DEBUG, "PostProcessor.ProcessEntity: Set Properties Initiatiated for POTS and PSS Completed");

            return true;
        }

        /// <summary>
        /// ProtocolObjectTypesSupport properties defined for Virtual Router Device 
        /// </summary>
        /// <param name="bacnetConfigurationModel">bacnetConfigurationModel</param>
        internal void VRDProtocolObjectTypesSupported(BACnetConfigurationModel bacnetConfigurationModel)
        {
            #region VRD ProtocolObjectTypesSupported setproperties

            try
            {
                BitArray VRDbitArray = new BitArray(54);
                VRDbitArray.SetAll(false);
                //VRDbitArray.Set(0, true); // Analog Input
                VRDbitArray.Set(2, true); // Analog Value
                //VRDbitArray.Set(6, true); // Calendar
                VRDbitArray.Set(8, true); // device
                VRDbitArray.Set(9, true); // Event Enrollment
                VRDbitArray.Set(15, true); // Notification Class
                //VRDbitArray.Set(17, true); // Schedule
                VRDbitArray.Set(19, true); // Multistate value
                //VRDbitArray.Set(20, true); // trend log
                VRDbitArray.Set(29, true); // Structure View

                byte[] VRDposArray = new byte[8];
                VRDbitArray.CopyTo(VRDposArray, 0);

                SetProperty(bacnetConfigurationModel.ControllerId, bacnetConfigurationModel.ControllerId, BacnetObjectType.ObjectDevice, BacnetPropertyID.PropProtocolObjectTypesSupported, VRDposArray);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "PostProcessor.VRDProtocolObjectTypesSupported : VRD ProtocolObjectTypesSupported setproperties failed ");
            }

            #endregion for VRD ProtocolObjectTypesSupported setproperties
        }

        /// <summary>
        /// ProtocolObjectTypesSupport properties defined for Virtual Device
        /// </summary>
        /// <param name="bacnetConfigurationModel">bacnetConfigurationModel</param>
        /// <param name="zonesLookUp">zonesLookUp</param>
        internal void VDProtocolObjectTypesSupported(BACnetConfigurationModel bacnetConfigurationModel, Dictionary<string, EntityBaseModel> zonesLookUp)
        {
            #region  VD ProtocolObjectTypesSupported setproperties
            BitArray VDbitArray = new BitArray(54);
            VDbitArray.SetAll(false);
            VDbitArray.Set(0, true); // Analog Input
            VDbitArray.Set(2, true); // Analog Value
            VDbitArray.Set(3, true); // Binary Input
            VDbitArray.Set(5, true); // Binary Value
            VDbitArray.Set(6, true); // Calendar
            VDbitArray.Set(8, true); // device
            VDbitArray.Set(9, true); // Event Enrollment
            VDbitArray.Set(15, true); // Notification Class

            if (bacnetConfigurationModel.Scheduling)
            {
                VDbitArray.Set(17, true); // Schedule
            }
            else
            {
                VDbitArray.Set(17, false); // Schedule
            }


            VDbitArray.Set(19, true); // Multistate value
            VDbitArray.Set(20, true); // trend log
            VDbitArray.Set(48, true); // Positive Integer value

            byte[] VDposArray = new byte[8];
            VDbitArray.CopyTo(VDposArray, 0);

            // ProtocolObjectTypesSupported Set Properties defined for Virtual Device (Zone Level)
            foreach (var zone in zonesLookUp)
            {
                uint deviceID = Convert.ToUInt32(zone.Value.BACnetData.DeviceID);
                if ((zone.Value as ZoneEntityModel).ZoneType == AppConstants.ZoneTypes.BlindSpace)
                {
                    #region Blind Zone POTS Mapping
                    BitArray VDblindbitArray = new BitArray(54);
                    VDblindbitArray.SetAll(false);
                    VDblindbitArray.Set(2, true); // Analog Value
                    VDblindbitArray.Set(6, true); // Calendar
                    VDblindbitArray.Set(8, true); // device
                    VDblindbitArray.Set(9, true); // Event Enrollment
                    VDblindbitArray.Set(15, true); // Notification Class

                    if (bacnetConfigurationModel.Scheduling)
                    {
                        VDblindbitArray.Set(17, true); // Schedule
                    }
                    else
                    {
                        VDblindbitArray.Set(17, false); // Schedule
                    }
                    VDblindbitArray.Set(20, true); // trend log
                    byte[] VDblindposArray = new byte[8];
                    VDblindbitArray.CopyTo(VDblindposArray, 0);
                    SetProperty(deviceID, deviceID, BacnetObjectType.ObjectDevice, BacnetPropertyID.PropProtocolObjectTypesSupported, VDblindposArray);
                    continue; 
                    #endregion
                }
                SetProperty(deviceID, deviceID, BacnetObjectType.ObjectDevice, BacnetPropertyID.PropProtocolObjectTypesSupported, VDposArray);
            }

            #endregion for VD ProtocolObjectTypesSupported setproperties
        }

        private void VRDProtocolServiceSupported(BACnetConfigurationModel bacnetConfigurationModel)
        {
            #region ProtocolSupportServices setproperties for Virtual Router

            BitArray PSSbitArray = new BitArray(41);
            PSSbitArray.SetAll(false);
            PSSbitArray.Set(0, true); // Acknowledge Alarm
            PSSbitArray.Set(39, true); // Get Event Information
            PSSbitArray.Set(3, true); // Get Alarm Summary
            PSSbitArray.Set(4, true); // Get Enrollment Summary
            PSSbitArray.Set(5, true); // Subscribe COV
            PSSbitArray.Set(8, true); // Add List Element
            PSSbitArray.Set(9, true); // Remove List Element
            PSSbitArray.Set(10, true); // Create Object
            PSSbitArray.Set(11, true); // Delete Object
            PSSbitArray.Set(12, true); // Read Property
            PSSbitArray.Set(14, true); // Read Property Multiple
            PSSbitArray.Set(35, true); // Read Range
            PSSbitArray.Set(15, true); // Write Property
            PSSbitArray.Set(16, true); // Write Property Multiple
            PSSbitArray.Set(17, true); // Device Communication Control
            PSSbitArray.Set(20, true); // Reinitialize Device
            if (bacnetConfigurationModel.TimeSynchronization)
            {
                PSSbitArray.Set(32, true); // Time Synchronization
                PSSbitArray.Set(36, true); // UTC Time Synchronization 
            }
            else
            {
                PSSbitArray.Set(32, false); // Time Synchronization
                PSSbitArray.Set(36, false); // UTC Time Synchronization 
            }
            PSSbitArray.Set(34, true); // Who-Is
            PSSbitArray.Set(26, true); // I-Am
            PSSbitArray.Set(33, true); // Who-Has

            byte[] PSSposArray = new byte[8];

            PSSbitArray.CopyTo(PSSposArray, 0);

            // ProtocolObjectTypesSupported Set Properties defined for Virtual Router (Controller)
            SetProperty(bacnetConfigurationModel.ControllerId, bacnetConfigurationModel.ControllerId, BacnetObjectType.ObjectDevice, BacnetPropertyID.PropProtocolServicesSupported, PSSposArray);

            #endregion
        }


        /// <summary>
        /// ProtocolServiceSupported setproperties defined for Controller and device level
        /// </summary>
        /// <param name="bacnetConfigurationModel">bacnetConfigurationModel</param>
        /// <param name="zonesLookUp">zonesLookUp</param>
        internal void VDProtocolServiceSupported(BACnetConfigurationModel bacnetConfigurationModel, Dictionary<string, EntityBaseModel> zonesLookUp)
        {
            #region for ProtocolSupportServices setproperties for Virtual Device

            BitArray PSSbitArray = new BitArray(41);
            PSSbitArray.SetAll(false);
            PSSbitArray.Set(0, true); // Acknowledge Alarm
            PSSbitArray.Set(39, true); // Get Event Information
            PSSbitArray.Set(3, true); // Get Alarm Summary
            PSSbitArray.Set(4, true); // Get Enrollment Summary
            PSSbitArray.Set(5, true); // Subscribe COV
            PSSbitArray.Set(8, true); // Add List Element
            PSSbitArray.Set(9, true); // Remove List Element
            PSSbitArray.Set(10, true); // Create Object
            PSSbitArray.Set(11, true); // Delete Object
            PSSbitArray.Set(12, true); // Read Property
            PSSbitArray.Set(14, true); // Read Property Multiple
            PSSbitArray.Set(35, true); // Read Range
            PSSbitArray.Set(15, true); // Write Property
            PSSbitArray.Set(16, true); // Write Property Multiple
            PSSbitArray.Set(17, true); // Device Communication Control
            //PSSbitArray.Set(20, true); // Reinitialize Device
            if (bacnetConfigurationModel.TimeSynchronization)
            {
                PSSbitArray.Set(32, true); // Time Synchronization
                PSSbitArray.Set(36, true); // UTC Time Synchronization 
            }
            else
            {
                PSSbitArray.Set(32, false); // Time Synchronization
                PSSbitArray.Set(36, false); // UTC Time Synchronization 
            }
            PSSbitArray.Set(34, true); // Who-Is
            PSSbitArray.Set(26, true); // I-Am
            PSSbitArray.Set(33, true); // Who-Has

            byte[] PSSposArray = new byte[8];

            PSSbitArray.CopyTo(PSSposArray, 0);

            // ProtocolObjectTypesSupported Set Properties defined for Virtual Router (Controller)
            //SetProperty(bacnetConfigurationModel.ControllerId, bacnetConfigurationModel.ControllerId, BacnetObjectType.ObjectDevice, BacnetPropertyID.PropProtocolServicesSupported, PSSposArray);

            // ProtocolObjectTypesSupported Set Properties defined for Virtual Device (Zone Level)
            foreach (var zone in zonesLookUp)
            {
                uint deviceID = Convert.ToUInt32(zone.Value.BACnetData.DeviceID);
                SetProperty(deviceID, deviceID, BacnetObjectType.ObjectDevice, BacnetPropertyID.PropProtocolServicesSupported, PSSposArray);
            }

            #endregion
        }
    }
}
