﻿using Molex.BACnet.Gateway.Log;
using Molex.BACnet.Gateway.ServiceCore.AppResources;
using Molex.BACnet.Gateway.ServiceCore.Constants;
using Molex.BACnet.Gateway.ServiceCore.Helpers;
using Molex.BACnet.Gateway.ServiceCore.Managers;
using Molex.BACnet.Gateway.ServiceCore.Models;
using Molex.BACnet.Gateway.ServiceCore.Models.BACnet;
using Molex.BACnet.Gateway.ServiceCore.Models.Entity;
using Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel;
using Molex.BACnet.Gateway.ServiceCore.Repository;
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Models;
using Molex.StackDataObjects.Response;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Molex.BACnet.Gateway.ServiceCore.Processor
{
    /// <summary>
    /// 
    /// </summary>
    /// <CreatedBy>hp</CreatedBy>
    /// <TimeStamp>10/31/20173:47 PM</TimeStamp>
    /// <seealso cref="Molex.BACnet.Gateway.ServiceCore.Processor.ProcessorBase" />
    public class BeaconZoneProcessor : ZoneBaseProcessor
    {
        /// <summary>
        /// Processes the entity.
        /// </summary>
        /// <param name="entityBaseModel">The entity base model.</param>
        /// <param name="processorConfiguration">The processor configuration.</param>
        /// <returns>
        /// boolean
        /// </returns>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>10/31/20173:47 PM</TimeStamp>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>09-06-201705:39 PM</TimeStamp>
        public override bool ProcessEntity(Models.Entity.EntityBaseModel entityBaseModel, Models.ProcessorConfigurationModel processorConfiguration)
        {
            EventTransitionBitsModel eventTransitionBitsModel = new EventTransitionBitsModel();
            LimitEnableBitsModel limitEnableBitsModel = new LimitEnableBitsModel();
            limitEnableBitsModel.HighLimit = true;
            limitEnableBitsModel.LowLimit = true;
            Logger.Instance.Log(LogLevel.DEBUG, "ZoneProcessor.ProcessEntity : Processing Zone");

            if (!(entityBaseModel.EntityType == Constants.AppConstants.EntityTypes.Zone))
            {
                Logger.Instance.Log(LogLevel.ERROR, "ZoneProcessor.ProcessEntity : Entity model mismatch..Zone Entity model required ");
                return false;
            }

            BACnetConfigurationModel bacnetConfigurationModel = processorConfiguration.BACnetConfigurationData;

            if (processorConfiguration.BACnetConfigurationData.AlarmNotificationSetting != null)
            {
                processorConfiguration.ZoneAlarmNotificationSetting = processorConfiguration.BACnetConfigurationData.AlarmNotificationSetting["Beacon Zone"];
            }
            ZoneEntityModel zoneEntityModel = entityBaseModel as ZoneEntityModel;


            uint deviceId = 0;

            switch (processorConfiguration.RestoreStrategy)
            {
                case AppConstants.RestoreIDReclaimStrategy.Preserve:
                    {
                        if (!processorConfiguration.EntityBACnetCache.ContainsKey(entityBaseModel.MappingKey))
                        {
                            deviceId = (uint)ObjectIdentifierManager.Instance.GetDeviceId();
                            deviceId = VerifyDeviceId(processorConfiguration, deviceId);
                            ObjectIdentifierManager.Instance.AddDeviceId(deviceId);
                        }
                        else
                        {
                            deviceId = (uint)processorConfiguration.EntityBACnetCache[entityBaseModel.MappingKey].DeviceId;
                            ObjectIdentifierManager.Instance.AddDeviceId(deviceId);
                        }
                    }
                    break;
                case AppConstants.RestoreIDReclaimStrategy.Incremental:
                    {
                        deviceId = (uint)ObjectIdentifierManager.Instance.GetDeviceId();
                        deviceId = VerifyDeviceId(processorConfiguration, deviceId);
                        ObjectIdentifierManager.Instance.AddDeviceId(deviceId);
                    }
                    break;
            }

            //For zone SNet is zero
            ResponseResult result = StackManager.Instance.CreateDeviceObject(deviceId, deviceId, (ushort)AppCoreRepo.Instance.BACnetConfigurationData.VirtualNetworkNumber);

            if (!result.IsSucceed)
            {
                Logger.Instance.Log(LogLevel.ERROR, "ZoneProcessor.ProcessEntity : Required Device could not be created");
                return false;
            }

            // Set Password for Zone Level device
            ResponseResult passwordResult = StackManager.Instance.SetDevicePassword((int)deviceId, bacnetConfigurationModel.BACnetPassword);
            if (!passwordResult.IsSucceed)
            {
                Logger.Instance.Log(LogLevel.ERROR, "ZoneProcessor.ProcessEntity: Set vendor Password fail for Zone " + deviceId);
                return false;
            }

            entityBaseModel.BACnetData.DeviceID = (int)deviceId;

            if (!CacheData(deviceId, BacnetObjectType.ObjectDevice, deviceId, entityBaseModel, AppConstants.LIGHTING_OBJECTDEVICE_OBJECT))
            {
                Logger.Instance.Log(LogLevel.DEBUG, "ZoneProcessor.ProcessEntity : Object Device caching failed");
                return false;
            }

            ResultModel<int> resultDeviceId = EntityHelper.GetDeviceId(entityBaseModel);

            if (!resultDeviceId.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.ERROR, "ZoneProcessor.ProcessEntity : Required Device not found..");
                return false;
            }


            #region Notification class


            ResultModel<CreateObjectResultModel> resultModel = CreateObjectAndCacheData(entityBaseModel, processorConfiguration, BacnetObjectType.ObjectNotificationClass, AppConstants.LIGHTING_NOTIFICATIONCLASS_OBJECT, AppConstants.EntityTypes.Zone);

            if (!resultModel.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.DEBUG, "ZoneProcessor.ProcessEntity: Create object and cache data failed for Notification class ");
                return false;
            }

            SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, BacnetObjectType.ObjectNotificationClass, BacnetPropertyID.PropDescription, AppResource.BeaconNotification, AppConstants.LIGHTING_NOTIFICATIONCLASS_OBJECT, processorConfiguration, entityBaseModel);
            SetPropertyAccessType(resultModel.Data.DeviceId, resultModel.Data.ObjectId, BacnetObjectType.ObjectNotificationClass, BacnetPropertyID.PropPriority, PropAccessType.READ_WRITE, true);

            uint[] priorityListArray = null;
            if (processorConfiguration.UserBACnetObjects != null && AppCoreRepo.Instance.BACnetConfigurationData.ZoneNotificationPriority.OverridePersistantState == false)
            {
                foreach (var item in processorConfiguration.UserBACnetObjects)
                {
                    if (item.Key == zoneEntityModel.EntityKey)
                    {
                        if (item.Value.NotificationClasses.Any(s => s.ObjectID == 16001))
                        {
                            NotificationClassPersistedModel obj = item.Value.NotificationClasses.Where(s => s.ObjectID == 16001).First();
                            if (obj == null)
                                break;
                            if (obj.NotificationPriority == null)
                                break;
                            if (obj.NotificationPriority.Count != 3)
                                break;
                            priorityListArray = new uint[3];
                            priorityListArray[2] = obj.NotificationPriority[2];
                            priorityListArray[1] = obj.NotificationPriority[1];
                            priorityListArray[0] = obj.NotificationPriority[0];
                            break;
                        }
                    }
                }
            }
            if (priorityListArray == null)
            {
                priorityListArray = new uint[3];
                priorityListArray[2] = (uint)AppCoreRepo.Instance.BACnetConfigurationData.ZoneNotificationPriority.ToNormal;
                priorityListArray[1] = (uint)AppCoreRepo.Instance.BACnetConfigurationData.ZoneNotificationPriority.ToFault;
                priorityListArray[0] = (uint)AppCoreRepo.Instance.BACnetConfigurationData.ZoneNotificationPriority.ToOffNormal;
            }
            SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, BacnetObjectType.ObjectNotificationClass, BacnetPropertyID.PropPriority, priorityListArray, AppConstants.LIGHTING_NOTIFICATIONCLASS_OBJECT, processorConfiguration, entityBaseModel);
            processorConfiguration.VDNotificationClassInstanceNumber = resultModel.Data.ObjectId;
            #endregion  Notification class

            #region AnalogValue

            ResultModel<CreateObjectResultModel> globalAnalogModel = CreateObjectAndCacheData(entityBaseModel, processorConfiguration, BacnetObjectType.ObjectAnalogValue, AppConstants.LIGHTING_BRIGHTENESS_OBJECT, AppConstants.EntityTypes.Zone);

            if (!globalAnalogModel.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.ERROR, "ZoneProcessor.ProcessEntity : Zone object creation failure in BACnet stack..");
                return globalAnalogModel.IsSuccess;
            }

            SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropDescription, GetZoneTypeKey(entityBaseModel) + AppResource.AnalogViewRepresentZone, AppConstants.LIGHTING_BRIGHTENESS_OBJECT, processorConfiguration, entityBaseModel);
            SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropUnits, BacnetEngineeringUnits.UnitsPercent);
            SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropRelinquishDefault, AppConstants.RELINQUISH_DEFAULT);
            SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropTimeDelay, processorConfiguration.BACnetConfigurationData.TimeDelay);
            SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropNotificationClass, processorConfiguration.VDNotificationClassInstanceNumber);
            SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropHighLimit, AppConstants.LIGHT_FIXTURE_HIGH_LIMIT);
            SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropProprietaryGridX, zoneEntityModel.ProprietaryGridX);
            SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropProprietaryGridY, zoneEntityModel.ProprietaryGridY);
            SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropNotifyType, processorConfiguration.BACnetConfigurationData.NotifyType);

            //Info: Default "Limit enable" property override with true flag
            SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropLimitEnable, limitEnableBitsModel);
            eventTransitionBitsModel = GetEventTransitionBit(processorConfiguration.ZoneAlarmNotificationSetting, AppConstants.LIGHTING_BRIGHTENESS_OBJECT);
            SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropEventEnable, eventTransitionBitsModel);

            #endregion AnalogValue

            #region Occupancy Zone Level

            ResultModel<CreateObjectResultModel> globalOccupancyBinaryInputModel = CreateObjectAndCacheData(entityBaseModel, processorConfiguration, BacnetObjectType.ObjectBinaryInput, AppConstants.ZONE_OCCUPANCY_OBJECT, AppConstants.EntityTypes.Zone, 11502);

            if (!globalOccupancyBinaryInputModel.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.ERROR, "ZoneProcessor.ProcessEntity : Zone object creation failure in BACnet stack..");
                return globalAnalogModel.IsSuccess;
            }

            SetProperty(globalOccupancyBinaryInputModel.Data.DeviceId, globalOccupancyBinaryInputModel.Data.ObjectId, BacnetObjectType.ObjectBinaryInput, BacnetPropertyID.PropDescription, GetZoneTypeKey(entityBaseModel) + "Zone Occupancy State", AppConstants.ZONE_OCCUPANCY_OBJECT, processorConfiguration, entityBaseModel);
            SetProperty(globalOccupancyBinaryInputModel.Data.DeviceId, globalOccupancyBinaryInputModel.Data.ObjectId, BacnetObjectType.ObjectBinaryInput, BacnetPropertyID.PropInactiveText, AppConstants.BINARY_OBJECT_INACTIVE_TEXT);
            SetProperty(globalOccupancyBinaryInputModel.Data.DeviceId, globalOccupancyBinaryInputModel.Data.ObjectId, BacnetObjectType.ObjectBinaryInput, BacnetPropertyID.PropActiveText, AppConstants.BINARY_OBJECT_ACTIVE_TEXT);
            SetProperty(globalOccupancyBinaryInputModel.Data.DeviceId, globalOccupancyBinaryInputModel.Data.ObjectId, BacnetObjectType.ObjectBinaryInput, BacnetPropertyID.PropNotifyType, processorConfiguration.BACnetConfigurationData.NotifyType);
            SetProperty(globalOccupancyBinaryInputModel.Data.DeviceId, globalOccupancyBinaryInputModel.Data.ObjectId, BacnetObjectType.ObjectBinaryInput, BacnetPropertyID.PropTimeDelay, processorConfiguration.BACnetConfigurationData.TimeDelay);
            eventTransitionBitsModel = GetEventTransitionBit(processorConfiguration.ZoneAlarmNotificationSetting, AppConstants.ZONE_OCCUPANCY_OBJECT);
            SetProperty(globalOccupancyBinaryInputModel.Data.DeviceId, globalOccupancyBinaryInputModel.Data.ObjectId, BacnetObjectType.ObjectBinaryInput, BacnetPropertyID.PropEventEnable, eventTransitionBitsModel);
            SetProperty(globalOccupancyBinaryInputModel.Data.DeviceId, globalOccupancyBinaryInputModel.Data.ObjectId, BacnetObjectType.ObjectBinaryInput, BacnetPropertyID.PropNotificationClass, processorConfiguration.VDNotificationClassInstanceNumber);
            SetProperty(globalOccupancyBinaryInputModel.Data.DeviceId, globalOccupancyBinaryInputModel.Data.ObjectId, BacnetObjectType.ObjectBinaryInput, BacnetPropertyID.PropProprietaryGridX, zoneEntityModel.ProprietaryGridX);
            SetProperty(globalOccupancyBinaryInputModel.Data.DeviceId, globalOccupancyBinaryInputModel.Data.ObjectId, BacnetObjectType.ObjectBinaryInput, BacnetPropertyID.PropProprietaryGridY, zoneEntityModel.ProprietaryGridY);
            SetProperty(globalOccupancyBinaryInputModel.Data.DeviceId, globalOccupancyBinaryInputModel.Data.ObjectId, BacnetObjectType.ObjectBinaryInput, BacnetPropertyID.PropDeviceType, AppConstants.ZONE_OCCUPANCY_OBJECT);

            SetPropertyAccessType(globalOccupancyBinaryInputModel.Data.DeviceId, globalOccupancyBinaryInputModel.Data.ObjectId, BacnetObjectType.ObjectBinaryInput, BacnetPropertyID.PropDeviceType, PropAccessType.NOT_SUPPORTED, true);

            #endregion

            #region MultiStateValue


            ResultModel<CreateObjectResultModel> globalMultiStateValue = CreateObjectAndCacheData(entityBaseModel, processorConfiguration, BacnetObjectType.ObjectMultiStateValue, AppConstants.LIGHTING_LIGHTSCENE_OBJECT, AppConstants.EntityTypes.Zone);


            if (!globalMultiStateValue.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.DEBUG, "ZoneProcessor:ProcessEntity: create object and cache data failed for " + BacnetObjectType.ObjectMultiStateValue);
                return globalMultiStateValue.IsSuccess;
            }

            SetProperty(globalMultiStateValue.Data.DeviceId, globalMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropDescription, GetZoneTypeKey(entityBaseModel) + AppResource.MultiStateValueRepresentZone, AppConstants.LIGHTING_LIGHTSCENE_OBJECT, processorConfiguration, entityBaseModel);
            SetProperty(globalMultiStateValue.Data.DeviceId, globalMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropNumberOfStates, processorConfiguration.LightScene.Length);
            SetProperty(globalMultiStateValue.Data.DeviceId, globalMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropTimeDelay, processorConfiguration.BACnetConfigurationData.TimeDelay);
            SetProperty(globalMultiStateValue.Data.DeviceId, globalMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropNotificationClass, processorConfiguration.VDNotificationClassInstanceNumber);
            //SetProperty(globalMultiStateValue.Data.DeviceId, globalMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropRelinquishDefault, Array.IndexOf(AppCoreRepo.Instance.AllLightScene, AppConstants.DEFAULT_LightScene) + 1);
            //SetProperty(globalMultiStateValue.Data.DeviceId, globalMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropProprietaryMood, Array.IndexOf(AppCoreRepo.Instance.AvailableMoods, AppConstants.DEFAULT_MOOD));
            SetProperty(globalMultiStateValue.Data.DeviceId, globalMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropNotifyType, processorConfiguration.BACnetConfigurationData.NotifyType);
            eventTransitionBitsModel = GetEventTransitionBit(processorConfiguration.ZoneAlarmNotificationSetting, AppConstants.LIGHTING_LIGHTSCENE_OBJECT);
            SetProperty(globalMultiStateValue.Data.DeviceId, globalMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropEventEnable, eventTransitionBitsModel);

            SetPropertyAccessType(globalMultiStateValue.Data.DeviceId, globalMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropProprietaryMood, PropAccessType.NOT_SUPPORTED, true);
            // State-Text SetProperties 
            ResultModel<bool> StateTextResult = SetMSVStateText(globalMultiStateValue, processorConfiguration.BeaconLightScenes);
            if (!StateTextResult.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.ERROR, "ZoneProcessor.ProcessEntity: State-Text SetProperties failed for Multistate Value Id " + globalMultiStateValue.Data.DeviceId);
            }
            #endregion MultiStateValue

            #region Set Zone Level Properties
            string key = EntityHelper.GetObjectDetailsKey(BacnetObjectType.ObjectDevice, AppConstants.LIGHTING_OBJECTDEVICE_OBJECT);
            string deviceName = GetObjectName(processorConfiguration.EntityBACnetCache, entityBaseModel, key, BacnetObjectType.ObjectDevice, deviceId);

            //if (!deviceName.ToLower().Contains("beacon"))
            //{
            //    deviceName = string.Format("Beacon" + " " + deviceName);
            //}

            SetProperty(deviceId, deviceId, BacnetObjectType.ObjectDevice, BacnetPropertyID.PropObjectName, deviceName);
            SetProperty(deviceId, deviceId, BacnetObjectType.ObjectDevice, BacnetPropertyID.PropDescription, bacnetConfigurationModel.Description, AppConstants.LIGHTING_OBJECTDEVICE_OBJECT, processorConfiguration, entityBaseModel);
            //SetProperty(deviceId, deviceId, BacnetObjectType.ObjectDevice, BacnetPropertyID.PropDayLightSavingsStatus, bacnetConfigurationModel.DaylightSavingStatus);
            //SetProperty(deviceId, deviceId, BacnetObjectType.ObjectDevice, BacnetPropertyID.PropUtcOffSet, bacnetConfigurationModel.UTCOffset);
            //SetProperty(deviceId, deviceId, BacnetObjectType.ObjectDevice, BacnetPropertyID.PropApduTimesOut, bacnetConfigurationModel.APDUTimeout);
            //SetProperty(deviceId, deviceId, BacnetObjectType.ObjectDevice, BacnetPropertyID.PropApduSegmentTimesOut, bacnetConfigurationModel.APDUSegmentTimeout);
            //SetProperty(deviceId, deviceId, BacnetObjectType.ObjectDevice, BacnetPropertyID.PropNumberOfApduRetries, bacnetConfigurationModel.APDURetries);
            SetProperty(deviceId, deviceId, BacnetObjectType.ObjectDevice, BacnetPropertyID.PropLocation, processorConfiguration.Location);
            SetProperty(deviceId, deviceId, BacnetObjectType.ObjectDevice, BacnetPropertyID.PropApplicationSoftwareVersion, AppCoreRepo.Instance.ApplicationSoftwareVersion);

            #endregion Set Zone Level Properties

            #region PIV Beacon Palettes
            CreateBeaconColorPalettes(entityBaseModel, processorConfiguration, limitEnableBitsModel, 1);
            CreateBeaconColorPalettes(entityBaseModel, processorConfiguration, limitEnableBitsModel, 2);
            CreateBeaconColorPalettes(entityBaseModel, processorConfiguration, limitEnableBitsModel, 3);
            CreateBeaconColorPalettes(entityBaseModel, processorConfiguration, limitEnableBitsModel, 4);
            CreateBeaconColorPalettes(entityBaseModel, processorConfiguration, limitEnableBitsModel, 5); 
            #endregion

            #region MultiStateValue - Palettes


            ResultModel<CreateObjectResultModel> globalPalettesMultiStateValue = CreateObjectAndCacheData(entityBaseModel, processorConfiguration, BacnetObjectType.ObjectMultiStateValue, AppConstants.PALETTES_OBJECT, AppConstants.EntityTypes.Zone);


            if (!globalPalettesMultiStateValue.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.DEBUG, "ZoneProcessor:ProcessEntity: create object and cache data failed for " + BacnetObjectType.ObjectMultiStateValue);
                return globalPalettesMultiStateValue.IsSuccess;
            }

            SetProperty(globalPalettesMultiStateValue.Data.DeviceId, globalPalettesMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropDescription, GetZoneTypeKey(entityBaseModel) + "Zone Palettes", AppConstants.PALETTES_OBJECT, processorConfiguration, entityBaseModel);
            //SetProperty(globalPalettesMultiStateValue.Data.DeviceId, globalPalettesMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropNumberOfStates, processorConfiguration.LightScene.Length);
            SetProperty(globalPalettesMultiStateValue.Data.DeviceId, globalPalettesMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropTimeDelay, processorConfiguration.BACnetConfigurationData.TimeDelay);
            SetProperty(globalPalettesMultiStateValue.Data.DeviceId, globalPalettesMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropNotificationClass, processorConfiguration.VDNotificationClassInstanceNumber);
            SetProperty(globalPalettesMultiStateValue.Data.DeviceId, globalPalettesMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropNotifyType, processorConfiguration.BACnetConfigurationData.NotifyType);
            eventTransitionBitsModel = GetEventTransitionBit(processorConfiguration.ZoneAlarmNotificationSetting, "Palette");
            SetProperty(globalPalettesMultiStateValue.Data.DeviceId, globalPalettesMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropEventEnable, eventTransitionBitsModel);

            SetPropertyAccessType(globalPalettesMultiStateValue.Data.DeviceId, globalPalettesMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropProprietaryMood, PropAccessType.NOT_SUPPORTED, true);
           
            #endregion MultiStateValue

            #region BinaryValue
            ResultModel<CreateObjectResultModel> globalBinaryValueModel = CreateObjectAndCacheData(entityBaseModel, processorConfiguration, BacnetObjectType.ObjectBinaryValue, AppConstants.ZONE_ONOFF_OBJECT, AppConstants.EntityTypes.Zone);

            SetProperty(globalBinaryValueModel.Data.DeviceId, globalBinaryValueModel.Data.ObjectId, BacnetObjectType.ObjectBinaryValue, BacnetPropertyID.PropDescription, GetZoneTypeKey(entityBaseModel) + AppResource.BinaryValueRepresent, AppConstants.ZONE_ONOFF_OBJECT, processorConfiguration, entityBaseModel);
            SetProperty(globalBinaryValueModel.Data.DeviceId, globalBinaryValueModel.Data.ObjectId, BacnetObjectType.ObjectBinaryValue, BacnetPropertyID.PropNotifyType, processorConfiguration.BACnetConfigurationData.NotifyType);
            SetProperty(globalBinaryValueModel.Data.DeviceId, globalBinaryValueModel.Data.ObjectId, BacnetObjectType.ObjectBinaryValue, BacnetPropertyID.PropNotificationClass, processorConfiguration.VDNotificationClassInstanceNumber);
            SetProperty(globalBinaryValueModel.Data.DeviceId, globalBinaryValueModel.Data.ObjectId, BacnetObjectType.ObjectBinaryValue, BacnetPropertyID.PropTimeDelay, processorConfiguration.BACnetConfigurationData.TimeDelay);
            SetProperty(globalBinaryValueModel.Data.DeviceId, globalBinaryValueModel.Data.ObjectId, BacnetObjectType.ObjectBinaryValue, BacnetPropertyID.PropAlarmValue, BacnetBinaryPV.BinaryInactive);

            eventTransitionBitsModel = GetEventTransitionBit(processorConfiguration.ZoneAlarmNotificationSetting, AppConstants.ZONE_ONOFF_OBJECT);
            SetProperty(globalBinaryValueModel.Data.DeviceId, globalBinaryValueModel.Data.ObjectId, BacnetObjectType.ObjectBinaryValue, BacnetPropertyID.PropEventEnable, eventTransitionBitsModel);

            #endregion

            #region Zone level Average sensor objects
            #region Analog Input Object
            int initialAvgSensorObjectID = 0;
            if (zoneEntityModel.Sensors.Any(x => x.SensorType == AppConstants.SensorType.LuxLevel))
            {
                initialAvgSensorObjectID = AppConstants.INITIAL_AVERAGE_SENSOR_AI_OBJECT_ID;
                eventTransitionBitsModel = GetEventTransitionBit(processorConfiguration.ZoneAlarmNotificationSetting, AppConstants.SensorType.LuxLevel.ToString());
                ProcessZoneAverageSensorEntity(zoneEntityModel, processorConfiguration, eventTransitionBitsModel, AppConstants.SensorType.LuxLevel, initialAvgSensorObjectID);
            }
            if (zoneEntityModel.Sensors.Any(x => x.SensorType == AppConstants.SensorType.AirQuality))
            {
                initialAvgSensorObjectID = AppConstants.INITIAL_AVERAGE_SENSOR_AI_OBJECT_ID + 1;
                eventTransitionBitsModel = GetEventTransitionBit(processorConfiguration.ZoneAlarmNotificationSetting, AppConstants.SensorType.AirQuality.ToString());
                ProcessZoneAverageSensorEntity(zoneEntityModel, processorConfiguration, eventTransitionBitsModel, AppConstants.SensorType.AirQuality, initialAvgSensorObjectID);
            }

            if (zoneEntityModel.Sensors.Any(x => x.SensorType == AppConstants.SensorType.ColorTemperature))
            {
                initialAvgSensorObjectID = AppConstants.INITIAL_AVERAGE_SENSOR_AI_OBJECT_ID + 2;
                eventTransitionBitsModel = GetEventTransitionBit(processorConfiguration.ZoneAlarmNotificationSetting, AppConstants.SensorType.ColorTemperature.ToString());
                ProcessZoneAverageSensorEntity(zoneEntityModel, processorConfiguration, eventTransitionBitsModel, AppConstants.SensorType.ColorTemperature, initialAvgSensorObjectID);
            }
            if (zoneEntityModel.Sensors.Any(x => x.SensorType == AppConstants.SensorType.Power))
            {
                initialAvgSensorObjectID = AppConstants.INITIAL_AVERAGE_SENSOR_AI_OBJECT_ID + 3;
                eventTransitionBitsModel = GetEventTransitionBit(processorConfiguration.ZoneAlarmNotificationSetting, AppConstants.SensorType.Power.ToString());
                ProcessZoneAverageSensorEntity(zoneEntityModel, processorConfiguration, eventTransitionBitsModel, AppConstants.SensorType.Power, initialAvgSensorObjectID);
            }
            if (zoneEntityModel.Sensors.Any(x => x.SensorType == AppConstants.SensorType.Temperature))
            {
                initialAvgSensorObjectID = AppConstants.INITIAL_AVERAGE_SENSOR_AI_OBJECT_ID + 4;
                eventTransitionBitsModel = GetEventTransitionBit(processorConfiguration.ZoneAlarmNotificationSetting, AppConstants.SensorType.Temperature.ToString());
                ProcessZoneAverageSensorEntity(zoneEntityModel, processorConfiguration, eventTransitionBitsModel, AppConstants.SensorType.Temperature, initialAvgSensorObjectID);
            }
            if (zoneEntityModel.Sensors.Any(x => x.SensorType == AppConstants.SensorType.Humidity))
            {
                initialAvgSensorObjectID = AppConstants.INITIAL_AVERAGE_SENSOR_AI_OBJECT_ID + 5;
                eventTransitionBitsModel = GetEventTransitionBit(processorConfiguration.ZoneAlarmNotificationSetting, AppConstants.SensorType.Humidity.ToString());
                ProcessZoneAverageSensorEntity(zoneEntityModel, processorConfiguration, eventTransitionBitsModel, AppConstants.SensorType.Humidity, initialAvgSensorObjectID);
            }
            #endregion Analog Input Object

            #region Binary Input Object
            //FOR BINARY VALUE OBJECTS THERE IS SEPERATE OBJECT ID RANGE 13751-14500
            if (zoneEntityModel.Sensors.Any(x => x.SensorType == AppConstants.SensorType.Presence))
            {
                initialAvgSensorObjectID = AppConstants.INITIAL_AVERAGE_SENSOR_BI_OBJECT_ID;
                eventTransitionBitsModel = GetEventTransitionBit(processorConfiguration.ZoneAlarmNotificationSetting, AppConstants.SensorType.Presence.ToString());
                ProcessZoneAverageSensorEntity(zoneEntityModel, processorConfiguration, eventTransitionBitsModel, AppConstants.SensorType.Presence, initialAvgSensorObjectID);
            }
            //zoneEntityModel extract each sensor single instance and create its zone level object.
            #endregion Binary Input Object

            if (bacnetConfigurationModel.MapIndividualSensors)
            {
                Logger.Instance.Log(LogLevel.DEBUG, "BeaconZoneProcessor.ProcessEntity: Individual level Sensor Mapping is enabled ");

                SensorProcessor sensorProcessor = new SensorProcessor();
                bool isSuccess = true;

                foreach (var item in zoneEntityModel.Sensors)
                {
                    isSuccess = sensorProcessor.ProcessEntity(item, processorConfiguration);

                    if (!isSuccess)
                    {
                        Logger.Instance.Log(LogLevel.DEBUG, "BeaconZoneProcessor.ProcessEntity : Sensor Name " + item.EntityName);
                        continue;
                    }
                }
            }
            #endregion

            Logger.Instance.Log(LogLevel.DEBUG, "BeaconZoneProcessor.ProcessEntity : Processing Beacon Zone Completed");

            return true;
        }

        /// <summary>
        /// Using this method will create the Beacon Palettes PIV objects
        /// </summary>
        /// <param name="entityBaseModel"></param>
        /// <param name="processorConfiguration"></param>
        /// <param name="limitEnableBitsModel"></param>
        /// <param name="colorCode"></param>
        /// <returns></returns>
        private bool CreateBeaconColorPalettes(Models.Entity.EntityBaseModel entityBaseModel, Models.ProcessorConfigurationModel processorConfiguration,LimitEnableBitsModel limitEnableBitsModel, int colorCode)
        {
            try
            {
                string objEventName = "RGB";
                string objName = AppResource.BeaconColor + " " + colorCode.ToString();
                string objDescription = "Beacon Zone RGB Color " + colorCode.ToString();
                if (entityBaseModel == null)
                    return false;
                if (processorConfiguration == null)
                    return false;
                if (limitEnableBitsModel == null)
                    return false;
                if (entityBaseModel as ZoneEntityModel == null)
                    return false;

                #region PositiveIntergerValue
                ResultModel<CreateObjectResultModel> rgbColourResultModel = CreateObjectAndCacheData((entityBaseModel as ZoneEntityModel), processorConfiguration, BacnetObjectType.ObjectPositiveIntegerValue, objName, AppConstants.EntityTypes.Zone);

                if (!rgbColourResultModel.IsSuccess)
                {
                    Logger.Instance.Log(LogLevel.DEBUG, "SensorProcessor.ProcessEntity: Create object and cache data failed for postive interger value");
                    return false;
                }

                SetProperty(rgbColourResultModel.Data.DeviceId, rgbColourResultModel.Data.ObjectId, BacnetObjectType.ObjectPositiveIntegerValue, BacnetPropertyID.PropDescription, objDescription, objName, processorConfiguration, entityBaseModel);
                SetProperty(rgbColourResultModel.Data.DeviceId, rgbColourResultModel.Data.ObjectId, BacnetObjectType.ObjectPositiveIntegerValue, BacnetPropertyID.PropTimeDelay, processorConfiguration.BACnetConfigurationData.TimeDelay);
                SetProperty(rgbColourResultModel.Data.DeviceId, rgbColourResultModel.Data.ObjectId, BacnetObjectType.ObjectPositiveIntegerValue, BacnetPropertyID.PropNotificationClass, processorConfiguration.VDNotificationClassInstanceNumber);
                SetProperty(rgbColourResultModel.Data.DeviceId, rgbColourResultModel.Data.ObjectId, BacnetObjectType.ObjectPositiveIntegerValue, BacnetPropertyID.PropNotifyType, processorConfiguration.BACnetConfigurationData.NotifyType);
                SetProperty(rgbColourResultModel.Data.DeviceId, rgbColourResultModel.Data.ObjectId, BacnetObjectType.ObjectPositiveIntegerValue, BacnetPropertyID.PropHighLimit, AppConstants.MAX_RGB_VALUE);
                SetProperty(rgbColourResultModel.Data.DeviceId, rgbColourResultModel.Data.ObjectId, BacnetObjectType.ObjectPositiveIntegerValue, BacnetPropertyID.PropLimitEnable, limitEnableBitsModel);
                EventTransitionBitsModel eventTransitionBitsModel = GetEventTransitionBit(processorConfiguration.ZoneAlarmNotificationSetting, objEventName);
                SetProperty(rgbColourResultModel.Data.DeviceId, rgbColourResultModel.Data.ObjectId, BacnetObjectType.ObjectPositiveIntegerValue, BacnetPropertyID.PropEventEnable, eventTransitionBitsModel);
                SetProperty(rgbColourResultModel.Data.DeviceId, rgbColourResultModel.Data.ObjectId, BacnetObjectType.ObjectPositiveIntegerValue, BacnetPropertyID.PropRelinquishDefault, 000000000);
                #endregion PositiveIntergerValue

                return true;
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.DEBUG, "BuildingProcesser:ProcessEntity: CreateFloorLevelControlObject() ");
                return false;
            }
        }

        /// <summary>
        /// Verifies the device identifier. Verifies with mapped ids to not duplicate the ids.
        /// </summary>
        /// <param name="processorConfiguration">The processor configuration.</param>
        /// <param name="deviceId">The device identifier.</param>
        /// <returns></returns>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>6/27/20174:18 PM</TimeStamp>
        private static uint VerifyDeviceId(Models.ProcessorConfigurationModel processorConfiguration, uint deviceId)
        {
            if (processorConfiguration.MappedObjectIds != null && processorConfiguration.MappedObjectIds.ContainsKey(deviceId))
            {
                deviceId = (uint)ObjectIdentifierManager.Instance.GetDeviceId(deviceId);
                deviceId = VerifyDeviceId(processorConfiguration, deviceId);
            }
            return deviceId;
        }

        /// <summary>
        /// Gets the event transition bit.
        /// </summary>
        /// <returns></returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy>
        /// <TimeStamp>4/11/20186:36 PM</TimeStamp>
        private EventTransitionBitsModel GetEventTransitionBit(List<ZoneObject> AlarmNotificationSetting, string entity)
        {
            EventTransitionBitsModel eventTransitionBitsModel = new EventTransitionBitsModel();
            if (AlarmNotificationSetting.Any(x => x.Name == entity && x.ZoneValue))
            {
                eventTransitionBitsModel.ToFault = true;
                eventTransitionBitsModel.ToNormal = true;
                eventTransitionBitsModel.ToOffNormal = true;
            }
            else
            {
                eventTransitionBitsModel.ToFault = false;
                eventTransitionBitsModel.ToNormal = false;
                eventTransitionBitsModel.ToOffNormal = false;
            }

            return eventTransitionBitsModel;
        }
    }
}
