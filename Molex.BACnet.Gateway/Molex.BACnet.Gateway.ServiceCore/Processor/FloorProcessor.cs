﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              FloorProcessor.cs
///   Description:        This class is used for processing the Entity module to BACnet Stack integration Module
///   Author:             Rupesh Saw                  
///   Date:               05/23/17
///---------------------------------------------------------------------------------------------
#endregion


using Molex.BACnet.Gateway.Log;
using Molex.BACnet.Gateway.ServiceCore.AppResources;
using Molex.BACnet.Gateway.ServiceCore.Constants;
using Molex.BACnet.Gateway.ServiceCore.Managers;
using Molex.BACnet.Gateway.ServiceCore.Models;
using Molex.BACnet.Gateway.ServiceCore.Models.BACnet;
using Molex.BACnet.Gateway.ServiceCore.Models.Entity;
using Molex.BACnet.Gateway.ServiceCore.Repository;
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Models;
using System.Linq;
using System;
using Molex.BACnet.Gateway.ServiceCore.Models.MolexAPIModels;
using System.Collections.Generic;
using Molex.BACnet.Gateway.ServiceCore.Helpers;

namespace Molex.BACnet.Gateway.ServiceCore.Processor
{
    /// <summary>
    /// This class is used for processing the Entity module to BACnet Stack integration Module
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:42 PM</TimeStamp>
    /// <seealso cref="Molex.BACnet.Gateway.ServiceCore.Processor.ProcessorBase" />
    public class FloorProcessor : ProcessorBase
    {


        /// <summary>
        /// Creating objects for FloorEnityModel mapped to Bacnet Stack
        /// </summary>
        /// <param name="entityBaseModel">EntityBaseModel</param>
        /// <param name="processorConfiguration">BACnetConfigurationModel</param>
        /// <returns>bool</returns>
        public bool ProcessEntity(Models.Entity.EntityBaseModel entityBaseModel, Models.ProcessorConfigurationModel processorConfiguration, int incrementObjectID)
        {
            EventTransitionBitsModel eventTransitionBitsModel = null;
            LimitEnableBitsModel limitEnableBitsModel = new LimitEnableBitsModel();
            limitEnableBitsModel.HighLimit = true;
            limitEnableBitsModel.LowLimit = true;

            Logger.Instance.Log(LogLevel.DEBUG, "FloorProcessor.ProcessEntity: Processing Floor");

            if (!(entityBaseModel.EntityType == Constants.AppConstants.EntityTypes.Floor))
            {
                Logger.Instance.Log(LogLevel.ERROR, "FloorProcessor.ProcessEntity: Entity model mismatch..Floor Entity model required Available Entity type " + entityBaseModel.EntityType);
                return false;
            }

            FloorEntityModel floorEntityModel = (FloorEntityModel)entityBaseModel;

            #region StructuredView

            ResultModel<CreateObjectResultModel> resultModel = CreateObjectAndCacheData(entityBaseModel, processorConfiguration, BacnetObjectType.ObjectStructuredView, AppConstants.LIGHTING_STRUCTUREDVIEW_OBJECT, AppConstants.EntityTypes.Floor, incrementObjectID);


            if (!resultModel.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.DEBUG, "FloorProcessor:ProcessEntity: create object and cache data failed for " + BacnetObjectType.ObjectStructuredView);
                return resultModel.IsSuccess;
            }

            SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, BacnetObjectType.ObjectStructuredView, BacnetPropertyID.PropDescription, floorEntityModel.EntityName, AppConstants.LIGHTING_STRUCTUREDVIEW_OBJECT, processorConfiguration, entityBaseModel);
            SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, BacnetObjectType.ObjectStructuredView, BacnetPropertyID.PropNodeType, AppConstants.NodeType.FLOOR);
            SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, BacnetObjectType.ObjectStructuredView, BacnetPropertyID.PropNodeSubType, floorEntityModel.EntityName);
            #endregion StructuredView

            if (processorConfiguration.BACnetConfigurationData.IsFloorAlarm)
            {
                eventTransitionBitsModel = new EventTransitionBitsModel();
                eventTransitionBitsModel.ToFault = true;
                eventTransitionBitsModel.ToNormal = true;
                eventTransitionBitsModel.ToOffNormal = true;
            }
            else
            {
                eventTransitionBitsModel = new EventTransitionBitsModel();
                eventTransitionBitsModel.ToFault = false;
                eventTransitionBitsModel.ToNormal = false;
                eventTransitionBitsModel.ToOffNormal = false;
            }


            

            //Spliting of User Zone Light Level , General Zone Light Level , Beacon Zone Light Level & Blind Sheding Control
            int analogValueIncrementObjectID = incrementObjectID;
            //User Space
            if (CreateFloorLevelControlObject(entityBaseModel, processorConfiguration, eventTransitionBitsModel, limitEnableBitsModel,
                resultModel, analogValueIncrementObjectID, AppConstants.ZoneTypes.UserSpace))
                analogValueIncrementObjectID++;

            //General Space
            if(CreateFloorLevelControlObject(entityBaseModel, processorConfiguration, eventTransitionBitsModel, limitEnableBitsModel,
                resultModel, analogValueIncrementObjectID, AppConstants.ZoneTypes.GeneralSpace))
                analogValueIncrementObjectID++;

            //Beacon Space
            if(CreateFloorLevelControlObject(entityBaseModel, processorConfiguration, eventTransitionBitsModel, limitEnableBitsModel,
                resultModel, analogValueIncrementObjectID, AppConstants.ZoneTypes.BeaconSpace))
                analogValueIncrementObjectID++;

            #region Occupancy Zone Level

            ResultModel<CreateObjectResultModel> globalOccupancyBinaryInputModel = CreateObjectAndCacheData(entityBaseModel, processorConfiguration, BacnetObjectType.ObjectBinaryInput, AppConstants.FLOOR_OCCUPANCY_OBJECT, AppConstants.EntityTypes.Floor, incrementObjectID);

            if (!globalOccupancyBinaryInputModel.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.ERROR, "ZoneProcessor.ProcessEntity : Zone object creation failure in BACnet stack..");
                return globalOccupancyBinaryInputModel.IsSuccess;
            }

            SetProperty(globalOccupancyBinaryInputModel.Data.DeviceId, globalOccupancyBinaryInputModel.Data.ObjectId, BacnetObjectType.ObjectBinaryInput, BacnetPropertyID.PropDescription, "Floor Occupancy State", AppConstants.FLOOR_OCCUPANCY_OBJECT, processorConfiguration, entityBaseModel);
            SetProperty(globalOccupancyBinaryInputModel.Data.DeviceId, globalOccupancyBinaryInputModel.Data.ObjectId, BacnetObjectType.ObjectBinaryInput, BacnetPropertyID.PropInactiveText, AppConstants.BINARY_OBJECT_INACTIVE_TEXT);
            SetProperty(globalOccupancyBinaryInputModel.Data.DeviceId, globalOccupancyBinaryInputModel.Data.ObjectId, BacnetObjectType.ObjectBinaryInput, BacnetPropertyID.PropActiveText, AppConstants.BINARY_OBJECT_ACTIVE_TEXT);
            SetProperty(globalOccupancyBinaryInputModel.Data.DeviceId, globalOccupancyBinaryInputModel.Data.ObjectId, BacnetObjectType.ObjectBinaryInput, BacnetPropertyID.PropNotifyType, processorConfiguration.BACnetConfigurationData.NotifyType);
            SetProperty(globalOccupancyBinaryInputModel.Data.DeviceId, globalOccupancyBinaryInputModel.Data.ObjectId, BacnetObjectType.ObjectBinaryInput, BacnetPropertyID.PropTimeDelay, processorConfiguration.BACnetConfigurationData.TimeDelay);  
            SetProperty(globalOccupancyBinaryInputModel.Data.DeviceId, globalOccupancyBinaryInputModel.Data.ObjectId, BacnetObjectType.ObjectBinaryInput, BacnetPropertyID.PropEventEnable, eventTransitionBitsModel);
            SetProperty(globalOccupancyBinaryInputModel.Data.DeviceId, globalOccupancyBinaryInputModel.Data.ObjectId, BacnetObjectType.ObjectBinaryInput, BacnetPropertyID.PropNotificationClass, processorConfiguration.VRDNotificationClassInstanceNumber);
            SetProperty(globalOccupancyBinaryInputModel.Data.DeviceId, globalOccupancyBinaryInputModel.Data.ObjectId, BacnetObjectType.ObjectBinaryInput, BacnetPropertyID.PropDeviceType, AppConstants.FLOOR_OCCUPANCY_OBJECT);
            SetPropertyAccessType(globalOccupancyBinaryInputModel.Data.DeviceId, globalOccupancyBinaryInputModel.Data.ObjectId, BacnetObjectType.ObjectBinaryInput, BacnetPropertyID.PropDeviceType, PropAccessType.NOT_SUPPORTED, true);

            #endregion

            #region BinaryValue Floor ON/OFF Control

            ResultModel<CreateObjectResultModel> globalBinaryValueModel = CreateObjectAndCacheData(entityBaseModel, processorConfiguration, BacnetObjectType.ObjectBinaryValue, AppConstants.FLOOR_ONOFF_OBJECT, AppConstants.EntityTypes.Floor, incrementObjectID);

            if (!globalBinaryValueModel.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.ERROR, "ZoneProcessor.ProcessEntity : Floor ON/OFF Control object creation failure in BACnet stack..");
                return globalBinaryValueModel.IsSuccess;
            }

            SetProperty(globalBinaryValueModel.Data.DeviceId, globalBinaryValueModel.Data.ObjectId, BacnetObjectType.ObjectBinaryValue, BacnetPropertyID.PropDescription,string.Format("{0} On/OFF", entityBaseModel.EntityName), AppConstants.FLOOR_ONOFF_OBJECT, processorConfiguration, entityBaseModel);
            SetProperty(globalBinaryValueModel.Data.DeviceId, globalBinaryValueModel.Data.ObjectId, BacnetObjectType.ObjectBinaryValue, BacnetPropertyID.PropNotifyType, processorConfiguration.BACnetConfigurationData.NotifyType);
            SetProperty(globalBinaryValueModel.Data.DeviceId, globalBinaryValueModel.Data.ObjectId, BacnetObjectType.ObjectBinaryValue, BacnetPropertyID.PropNotificationClass, processorConfiguration.VDNotificationClassInstanceNumber);
            SetProperty(globalBinaryValueModel.Data.DeviceId, globalBinaryValueModel.Data.ObjectId, BacnetObjectType.ObjectBinaryValue, BacnetPropertyID.PropTimeDelay, processorConfiguration.BACnetConfigurationData.TimeDelay);
            SetProperty(globalBinaryValueModel.Data.DeviceId, globalBinaryValueModel.Data.ObjectId, BacnetObjectType.ObjectBinaryValue, BacnetPropertyID.PropAlarmValue, BacnetBinaryPV.BinaryInactive);
            SetProperty(globalBinaryValueModel.Data.DeviceId, globalBinaryValueModel.Data.ObjectId, BacnetObjectType.ObjectBinaryValue, BacnetPropertyID.PropEventEnable, eventTransitionBitsModel);
            //SetProperty(globalBinaryValueModel.Data.DeviceId, globalBinaryValueModel.Data.ObjectId, BacnetObjectType.ObjectBinaryInput, BacnetPropertyID.PropInactiveText, AppConstants.BINARY_OBJECT_INACTIVE_TEXT);
            //SetProperty(globalBinaryValueModel.Data.DeviceId, globalBinaryValueModel.Data.ObjectId, BacnetObjectType.ObjectBinaryInput, BacnetPropertyID.PropActiveText, AppConstants.BINARY_OBJECT_ACTIVE_TEXT);

            #endregion

            #region Sensor Floor Average

            List<AppConstants.SensorType> sensorTypes = new List<AppConstants.SensorType>();
            for (int ii = 0; ii < floorEntityModel.Childs.Count; ii++)
            {
                ZoneEntityModel zone = (ZoneEntityModel)(floorEntityModel.Childs[ii]);
                foreach (var sensor in zone.Sensors)
                {
                    if (sensorTypes.Contains(sensor.SensorType))
                        continue;
                    sensorTypes.Add(sensor.SensorType);
                }
            }

            int floorSensorId = incrementObjectID;
            foreach (var sensorType in sensorTypes)
            {
                if (sensorType == AppConstants.SensorType.NotSupported
                    || sensorType == AppConstants.SensorType.Others
                    || sensorType == AppConstants.SensorType.Presence)
                    continue;

                BacnetObjectType objectType = EntityHelper.GetBACnetObjectTypeBySensorType(sensorType);

                ResultModel<CreateObjectResultModel> globalSensorModel = CreateObjectAndCacheData(entityBaseModel, processorConfiguration, objectType, entityBaseModel.EntityName + "-" + sensorType.ToString(), AppConstants.EntityTypes.Floor, floorSensorId++);
                if (!globalSensorModel.IsSuccess)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "FloorProcessor.ProcessEntity : " + sensorType + " object creation failure in BACnet stack..");
                    break;
                }

               
                BacnetEngineeringUnits unit = EntityHelper.GenerateAnalogObjectUnits(sensorType);
                Single sensorMinValue = EntityHelper.GetSensorMinValue(sensorType);
                Single sensorMaxValue = EntityHelper.GetSensorMaxValue(sensorType);
                //max value of individual power sensor multiply by max allowed sensor in zone
                if (sensorType == AppConstants.SensorType.Power)
                    sensorMaxValue = sensorMaxValue * 2500;
                Single sensorLowLimit = EntityHelper.GetSensorLowLimit(sensorType);
                Single sensorHighLimit = EntityHelper.GetSensorHighLimit(sensorType);
                Single sensorDeadband = EntityHelper.GetSensorDeadband(sensorType);
                Single sensorCOVIncrement = EntityHelper.GetCOVIncrement(sensorType);

                SetProperty(globalSensorModel.Data.DeviceId, globalSensorModel.Data.ObjectId, objectType, BacnetPropertyID.PropDescription, string.Format("Indicates the average {0} value for the floor: {1}", sensorType.ToString(), entityBaseModel.EntityName), AppConstants.LIGHTING_SENSOR_OBJECT, processorConfiguration, entityBaseModel);
                SetProperty(globalSensorModel.Data.DeviceId, globalSensorModel.Data.ObjectId, objectType, BacnetPropertyID.PropUnits, unit);
                SetProperty(globalSensorModel.Data.DeviceId, globalSensorModel.Data.ObjectId, objectType, BacnetPropertyID.PropMaxPresValue, sensorMaxValue);
                SetProperty(globalSensorModel.Data.DeviceId, globalSensorModel.Data.ObjectId, objectType, BacnetPropertyID.PropMinPresValue, sensorMinValue);
                SetProperty(globalSensorModel.Data.DeviceId, globalSensorModel.Data.ObjectId, objectType, BacnetPropertyID.PropHighLimit, sensorHighLimit);
                SetProperty(globalSensorModel.Data.DeviceId, globalSensorModel.Data.ObjectId, objectType, BacnetPropertyID.PropLowLimit, sensorLowLimit);
                SetProperty(globalSensorModel.Data.DeviceId, globalSensorModel.Data.ObjectId, objectType, BacnetPropertyID.PropDeadBand, sensorDeadband);
                SetProperty(globalSensorModel.Data.DeviceId, globalSensorModel.Data.ObjectId, objectType, BacnetPropertyID.PropCOVIncrement, sensorCOVIncrement);

                SetProperty(globalSensorModel.Data.DeviceId, globalSensorModel.Data.ObjectId, objectType, BacnetPropertyID.PropDeviceType, string.Format("{0} {1}", AppConstants.FLOOR_AVERAGE_OBJECT, sensorType.ToString()));
                SetProperty(globalSensorModel.Data.DeviceId, globalSensorModel.Data.ObjectId, objectType, BacnetPropertyID.PropNotificationClass, processorConfiguration.VRDNotificationClassInstanceNumber);
                SetProperty(globalSensorModel.Data.DeviceId, globalSensorModel.Data.ObjectId, objectType, BacnetPropertyID.PropNotifyType, processorConfiguration.BACnetConfigurationData.NotifyType);
                SetProperty(globalSensorModel.Data.DeviceId, globalSensorModel.Data.ObjectId, objectType, BacnetPropertyID.PropTimeDelay, processorConfiguration.BACnetConfigurationData.TimeDelay);
                SetProperty(globalSensorModel.Data.DeviceId, globalSensorModel.Data.ObjectId, objectType, BacnetPropertyID.PropLimitEnable, limitEnableBitsModel);
                SetProperty(globalSensorModel.Data.DeviceId, globalSensorModel.Data.ObjectId, objectType, BacnetPropertyID.PropEventEnable, eventTransitionBitsModel);

                if (eventTransitionBitsModel != null)
                {
                    SetProperty(globalSensorModel.Data.DeviceId, globalSensorModel.Data.ObjectId, objectType, BacnetPropertyID.PropEventEnable, eventTransitionBitsModel);
                }
            }

            #endregion

            if (entityBaseModel.Childs.Any(s => (s as ZoneEntityModel).ZoneType == AppConstants.ZoneTypes.GeneralSpace ||
                (s as ZoneEntityModel).ZoneType == AppConstants.ZoneTypes.UserSpace))
            {
                #region AnalogValue-BioDynamic


                ResultModel<CreateObjectResultModel> globalBioDynamicAnalogModel = CreateObjectAndCacheData(entityBaseModel, processorConfiguration, BacnetObjectType.ObjectAnalogValue, AppResource.BioDynamicControlName, AppConstants.EntityTypes.Floor, analogValueIncrementObjectID, true);

                if (!globalBioDynamicAnalogModel.IsSuccess)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "ZoneProcessor.ProcessEntity : Zone object creation failure in BACnet stack..");
                    return globalBioDynamicAnalogModel.IsSuccess;
                }

                SetProperty(globalBioDynamicAnalogModel.Data.DeviceId, globalBioDynamicAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropDescription, AppResource.AnalogValueBioDynamicRepresentFloor, AppResource.BioDynamicControlName, processorConfiguration, entityBaseModel);
                SetProperty(globalBioDynamicAnalogModel.Data.DeviceId, globalBioDynamicAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropUnits, BacnetEngineeringUnits.UnitsPercent);
                SetProperty(globalBioDynamicAnalogModel.Data.DeviceId, globalBioDynamicAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropRelinquishDefault, AppConstants.RELINQUISH_DEFAULT);
                SetProperty(globalBioDynamicAnalogModel.Data.DeviceId, globalBioDynamicAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropTimeDelay, processorConfiguration.BACnetConfigurationData.TimeDelay);
                SetProperty(globalBioDynamicAnalogModel.Data.DeviceId, globalBioDynamicAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropNotificationClass, processorConfiguration.VRDNotificationClassInstanceNumber);
                SetProperty(globalBioDynamicAnalogModel.Data.DeviceId, globalBioDynamicAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropHighLimit, AppConstants.LIGHT_FIXTURE_HIGH_LIMIT);
                //SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropProprietaryColorTemperature, AppResource.FixtureColorTemperature);
                //SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropProprietaryColorTemperatureUnit, BacnetEngineeringUnits.UnitsDegreesKelvin);
                SetProperty(globalBioDynamicAnalogModel.Data.DeviceId, globalBioDynamicAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropNotifyType, processorConfiguration.BACnetConfigurationData.NotifyType);

                //Info: Default "Limit enable" property override with true flag
                SetProperty(globalBioDynamicAnalogModel.Data.DeviceId, globalBioDynamicAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropLimitEnable, limitEnableBitsModel);

                SetProperty(globalBioDynamicAnalogModel.Data.DeviceId, globalBioDynamicAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropNotificationClass, processorConfiguration.VRDNotificationClassInstanceNumber);
                SetProperty(globalBioDynamicAnalogModel.Data.DeviceId, globalBioDynamicAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropEventEnable, eventTransitionBitsModel);
                #endregion AnalogValue

                analogValueIncrementObjectID++;

                #region AnalogValue-Saturation


                ResultModel<CreateObjectResultModel> globalSaturationAnalogModel = CreateObjectAndCacheData(entityBaseModel, processorConfiguration, BacnetObjectType.ObjectAnalogValue, AppResource.SaturationControlName, AppConstants.EntityTypes.Floor, analogValueIncrementObjectID, true);

                if (!globalSaturationAnalogModel.IsSuccess)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "ZoneProcessor.ProcessEntity : Zone object creation failure in BACnet stack..");
                    return globalSaturationAnalogModel.IsSuccess;
                }

                SetProperty(globalSaturationAnalogModel.Data.DeviceId, globalSaturationAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropDescription, AppResource.AnalogValueSaturationRepresentFloor, AppResource.SaturationControlName, processorConfiguration, entityBaseModel);
                SetProperty(globalSaturationAnalogModel.Data.DeviceId, globalSaturationAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropUnits, BacnetEngineeringUnits.UnitsPercent);
                SetProperty(globalSaturationAnalogModel.Data.DeviceId, globalSaturationAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropRelinquishDefault, AppConstants.RELINQUISH_DEFAULT);
                SetProperty(globalSaturationAnalogModel.Data.DeviceId, globalSaturationAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropTimeDelay, processorConfiguration.BACnetConfigurationData.TimeDelay);
                SetProperty(globalSaturationAnalogModel.Data.DeviceId, globalSaturationAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropNotificationClass, processorConfiguration.VDNotificationClassInstanceNumber);
                SetProperty(globalSaturationAnalogModel.Data.DeviceId, globalSaturationAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropHighLimit, AppConstants.LIGHT_FIXTURE_HIGH_LIMIT);
                //SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropProprietaryColorTemperature, AppResource.FixtureColorTemperature);
                //SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropProprietaryColorTemperatureUnit, BacnetEngineeringUnits.UnitsDegreesKelvin);
                SetProperty(globalSaturationAnalogModel.Data.DeviceId, globalSaturationAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropNotifyType, processorConfiguration.BACnetConfigurationData.NotifyType);

                //Info: Default "Limit enable" property override with true flag
                SetProperty(globalSaturationAnalogModel.Data.DeviceId, globalSaturationAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropLimitEnable, limitEnableBitsModel);
                SetProperty(globalSaturationAnalogModel.Data.DeviceId, globalSaturationAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropNotificationClass, processorConfiguration.VRDNotificationClassInstanceNumber);
                SetProperty(globalSaturationAnalogModel.Data.DeviceId, globalSaturationAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropEventEnable, eventTransitionBitsModel);
                #endregion AnalogValue

                analogValueIncrementObjectID++;
            }

            CreateFloorLevelShadingControlObjects(entityBaseModel, processorConfiguration, eventTransitionBitsModel, limitEnableBitsModel,
                resultModel, analogValueIncrementObjectID);

            if (entityBaseModel.Childs.Count(s => (s as ZoneEntityModel).ZoneType == AppConstants.ZoneTypes.BlindSpace) != entityBaseModel.Childs.Count)
            {
                #region MultiStateValue

                ResultModel<CreateObjectResultModel> globalMultiStateValue = CreateObjectAndCacheData(entityBaseModel, processorConfiguration, BacnetObjectType.ObjectMultiStateValue, AppConstants.LIGHTING_LIGHTSCENE_OBJECT, AppConstants.EntityTypes.Floor, incrementObjectID);

                if (!globalMultiStateValue.IsSuccess)
                {
                    Logger.Instance.Log(LogLevel.DEBUG, "FloorProcessor:ProcessEntity: create object and cache data failed for " + BacnetObjectType.ObjectMultiStateValue);
                    return globalMultiStateValue.IsSuccess;
                }

                SetProperty(globalMultiStateValue.Data.DeviceId, globalMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropDescription, AppResource.MultiStateValueRepresentFloor, AppConstants.LIGHTING_LIGHTSCENE_OBJECT, processorConfiguration, entityBaseModel);
                SetProperty(globalMultiStateValue.Data.DeviceId, globalMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropNumberOfStates, processorConfiguration.LightScene.Length);
                SetProperty(globalMultiStateValue.Data.DeviceId, globalMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropTimeDelay, processorConfiguration.BACnetConfigurationData.TimeDelay);
                SetProperty(globalMultiStateValue.Data.DeviceId, globalMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropNotificationClass, processorConfiguration.VRDNotificationClassInstanceNumber);
                SetProperty(globalMultiStateValue.Data.DeviceId, globalMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropRelinquishDefault, 1);//Array.IndexOf(AppCoreRepo.Instance.UserAndGeneralLightScene, AppConstants.DEFAULT_LightScene) + 1
                //SetProperty(globalMultiStateValue.Data.DeviceId, globalMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropProprietaryMood, Array.IndexOf(AppCoreRepo.Instance.AvailableMoods, AppConstants.DEFAULT_MOOD));
                SetProperty(globalMultiStateValue.Data.DeviceId, globalMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropNotifyType, processorConfiguration.BACnetConfigurationData.NotifyType);

                SetPropertyAccessType(globalMultiStateValue.Data.DeviceId, globalMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropProprietaryMood, PropAccessType.NOT_SUPPORTED, true);
                if (eventTransitionBitsModel != null)
                {
                    SetProperty(globalMultiStateValue.Data.DeviceId, globalMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropEventEnable, eventTransitionBitsModel);
                }
                // State-Text SetProperties 
                ResultModel<bool> msvResult = SetMSVStateText(globalMultiStateValue, processorConfiguration.LightScene);
                if (!msvResult.IsSuccess)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "BuildingProcessor.ProcessEntity: State-Text SetProperties failed for Multistate Value Id " + globalMultiStateValue.Data.DeviceId);
                }
                #endregion MultiStateValue
            }

            bool isSuccess = true;

            foreach (ZoneEntityModel item in entityBaseModel.Childs)
            {
                switch (item.ZoneType)
                {
                    case AppConstants.ZoneTypes.BlindSpace:
                        {
                            BlindZoneProcesser blindZoneProcesser = new BlindZoneProcesser();
                            isSuccess = blindZoneProcesser.ProcessEntity(item, processorConfiguration);
                        }
                        break;
                    case AppConstants.ZoneTypes.BeaconSpace:
                        {
                            BeaconZoneProcessor beaconZoneProcessor = new BeaconZoneProcessor();
                            isSuccess = beaconZoneProcessor.ProcessEntity(item, processorConfiguration);
                        }
                        break;
                    case AppConstants.ZoneTypes.UserSpace:
                    case AppConstants.ZoneTypes.GeneralSpace:
                    default:
                        {
                            ZoneProcessor zoneProcessor = new ZoneProcessor();
                            isSuccess = zoneProcessor.ProcessEntity(item, processorConfiguration);
                        }
                        break;
                }
                if (!isSuccess)
                    break;
            }
            #region SubordinateList and Subordinate Annotations

            //WriteProperty defined for SubordinateList
            ResultModel<bool>  result = base.SetStructuredViewSubOrdinateListProperty(resultModel, entityBaseModel);
            if (!result.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.ERROR, "BuildingProcessor.ProcessEntity: SubordinateList SetProperties failed for Multistate Value Id " + resultModel.Data.DeviceId);
            }

            //WriteProperty defined for Subordinate Annotations
            result = base.SetStructuredViewSubOrdinateAnnotationProperty(resultModel, entityBaseModel);
            if (!result.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.ERROR, "BuildingProcessor.ProcessEntity: Subordinate Annotations SetProperties failed for Multistate Value Id " + resultModel.Data.DeviceId);
            }

            #endregion SubordinateList and Subordinate Annotations

            Logger.Instance.Log(LogLevel.DEBUG, "FloorProcessor.ProcessEntity: Processing Floor Completed");

            return true;
        }


        private bool CreateFloorLevelControlObject(Models.Entity.EntityBaseModel entityBaseModel, Models.ProcessorConfigurationModel processorConfiguration,
            EventTransitionBitsModel eventTransitionBitsModel, LimitEnableBitsModel limitEnableBitsModel, ResultModel<CreateObjectResultModel> resultModel, int incrementObjectID,
            AppConstants.ZoneTypes zoneType)
        {
            try
            {
                string objDetails = string.Empty;
                if (entityBaseModel.Childs == null)
                    return false;
                if (!entityBaseModel.Childs.Any(s => (s as ZoneEntityModel).ZoneType == zoneType))
                    return false;
                switch (zoneType)
                {
                    case AppConstants.ZoneTypes.UserSpace:
                        objDetails = entityBaseModel.EntityType + " " + AppResource.UserLightLevel;
                        break;
                    case AppConstants.ZoneTypes.BeaconSpace:
                        objDetails = entityBaseModel.EntityType + " " + AppResource.BeaconLightLevel;
                        break;
                    case AppConstants.ZoneTypes.GeneralSpace:
                        objDetails = entityBaseModel.EntityType + " " + AppResource.GeneralLightLevel;
                        break;
                    default:
                        break;
                }
                #region AnalogValue

                ResultModel<CreateObjectResultModel> globalAnalogValue = CreateObjectAndCacheData(entityBaseModel, processorConfiguration, BacnetObjectType.ObjectAnalogValue, objDetails, AppConstants.EntityTypes.Floor, incrementObjectID);

                if (!globalAnalogValue.IsSuccess)
                {
                    Logger.Instance.Log(LogLevel.DEBUG, "FloorProcessor:ProcessEntity: create object and cache data failed for " + BacnetObjectType.ObjectAnalogValue);
                    return globalAnalogValue.IsSuccess;
                }

                SetProperty(globalAnalogValue.Data.DeviceId, globalAnalogValue.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropDescription, objDetails, objDetails, processorConfiguration, entityBaseModel);
                //SetProperty(globalAnalogValue.Data.DeviceId, globalAnalogValue.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropPresentValue, AppConstants.DEFAULT_BRIGHTNESS);
                SetProperty(globalAnalogValue.Data.DeviceId, globalAnalogValue.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropUnits, BacnetEngineeringUnits.UnitsPercent);
                SetProperty(globalAnalogValue.Data.DeviceId, globalAnalogValue.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropRelinquishDefault, AppConstants.RELINQUISH_DEFAULT);
                SetProperty(globalAnalogValue.Data.DeviceId, globalAnalogValue.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropTimeDelay, processorConfiguration.BACnetConfigurationData.TimeDelay);
                SetProperty(globalAnalogValue.Data.DeviceId, globalAnalogValue.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropNotificationClass, processorConfiguration.VRDNotificationClassInstanceNumber);
                SetProperty(globalAnalogValue.Data.DeviceId, globalAnalogValue.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropHighLimit, AppConstants.LIGHT_FIXTURE_HIGH_LIMIT);
                //SetProperty(globalAnalogValue.Data.DeviceId, globalAnalogValue.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropProprietaryColorTemperature, AppResource.FixtureColorTemperature);
                //SetProperty(globalAnalogValue.Data.DeviceId, globalAnalogValue.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropProprietaryColorTemperatureUnit, BacnetEngineeringUnits.UnitsDegreesKelvin);
                SetProperty(globalAnalogValue.Data.DeviceId, globalAnalogValue.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropNotifyType, processorConfiguration.BACnetConfigurationData.NotifyType);

                //Info: Default "Limit enable" property override with true flag
                SetProperty(globalAnalogValue.Data.DeviceId, resultModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropLimitEnable, limitEnableBitsModel);

                if (eventTransitionBitsModel != null)
                {
                    SetProperty(globalAnalogValue.Data.DeviceId, globalAnalogValue.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropEventEnable, eventTransitionBitsModel);
                }
                return true;
                #endregion AnalogValue
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.DEBUG, "FloorProcessor:ProcessEntity: CreateFloorLevelControlObject() ");
                return false;
            }
        }

        private bool CreateFloorLevelShadingControlObjects(Models.Entity.EntityBaseModel entityBaseModel, Models.ProcessorConfigurationModel processorConfiguration,
               EventTransitionBitsModel eventTransitionBitsModel, LimitEnableBitsModel limitEnableBitsModel, ResultModel<CreateObjectResultModel> resultModel, int incrementObjectID)
        {
            bool result = false;
            try
            {
                if (!(entityBaseModel as FloorEntityModel).Childs.Any(s => (s as ZoneEntityModel).ZoneType == AppConstants.ZoneTypes.BlindSpace))
                    return false;

                #region AnalogValue - All

                ResultModel<CreateObjectResultModel> globalAnalogModel = CreateObjectAndCacheData(entityBaseModel, processorConfiguration, BacnetObjectType.ObjectAnalogValue, AppResource.AnalogValueBlindObjectName, AppConstants.EntityTypes.Floor, incrementObjectID, true);

                if (!globalAnalogModel.IsSuccess)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "ZoneProcessor.ProcessEntity : Zone object creation failure in BACnet stack..");
                    return globalAnalogModel.IsSuccess;
                }

                SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropDescription, AppResource.AnalogViewRepresentBlindFloor, AppConstants.LIGHTING_BRIGHTENESS_OBJECT, processorConfiguration, entityBaseModel);
                SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropUnits, BacnetEngineeringUnits.UnitsPercent);
                SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropRelinquishDefault, AppConstants.RELINQUISH_DEFAULT);
                SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropTimeDelay, processorConfiguration.BACnetConfigurationData.TimeDelay);
                SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropNotificationClass, processorConfiguration.VDNotificationClassInstanceNumber);
                SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropHighLimit, AppConstants.LIGHT_FIXTURE_HIGH_LIMIT);
                //SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropProprietaryGridX, zoneEntityModel.ProprietaryGridX);
                //SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropProprietaryGridY, zoneEntityModel.ProprietaryGridY);
                SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropNotifyType, processorConfiguration.BACnetConfigurationData.NotifyType);

                //Info: Default "Limit enable" property override with true flag
                SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropLimitEnable, limitEnableBitsModel);
                if (eventTransitionBitsModel != null)
                {
                    SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropEventEnable, eventTransitionBitsModel);
                }
                entityBaseModel.BACnetData.ObjectDetails.Values.Where(s => s.ObjectID == globalAnalogModel.Data.ObjectId && s.ObjectType == BacnetObjectType.ObjectAnalogValue).First().PresentValue = AppConstants.RELINQUISH_DEFAULT;
                #endregion AnalogValue

                List<Blind> blinds = new List<Blind>();
                foreach (var item in (entityBaseModel as FloorEntityModel).Childs)
                {
                    if ((item as ZoneEntityModel).Blinds != null)
                        blinds.AddRange((item as ZoneEntityModel).Blinds);
                }
                blinds.Sort((a, b) => (a.location.CompareTo(b.location)));
                #region Location -Wise
                foreach (var item in blinds)
                {
                    #region Object Creation Validation
                    bool isObjectAlreadyCreated = false;
                    foreach (var objItem in (entityBaseModel as FloorEntityModel).BACnetData.ObjectDetails)
                    {
                        string objectKey = objItem.Key.Substring(objItem.Key.LastIndexOf('_') + 1);
                        if (objectKey.ToUpperInvariant() == AppHelper.GetFullLocationName(item.location.ToUpperInvariant()).ToUpperInvariant())
                        {
                            isObjectAlreadyCreated = true;
                            break;
                        }
                    }
                    if (isObjectAlreadyCreated)
                        continue;
                    #endregion

                    #region AnalogValue Location Wise
                    int BlindLocationObjectID = 0;
                    string locationName = string.Empty;
                    switch (item.location)
                    {
                        case "C":
                            locationName = "Center";
                            BlindLocationObjectID = incrementObjectID + 1;
                            break;
                        case "E":
                            locationName = "East";
                            BlindLocationObjectID = incrementObjectID + 2;
                            break;
                        case "N":
                            locationName = "North";
                            BlindLocationObjectID = incrementObjectID + 3;
                            break;
                        case "NE":
                            locationName = "North-East";
                            BlindLocationObjectID = incrementObjectID + 4;
                            break;
                        case "NW":
                            locationName = "North-West";
                            BlindLocationObjectID = incrementObjectID + 5;
                            break;
                        case "S":
                            locationName = "South";
                            BlindLocationObjectID = incrementObjectID + 6;
                            break;
                        case "SE":
                            locationName = "South-East";
                            BlindLocationObjectID = incrementObjectID + 7;
                            break;
                        case "SW":
                            locationName = "South-West";
                            BlindLocationObjectID = incrementObjectID + 8;
                            break;
                        case "W":
                            locationName = "West";
                             BlindLocationObjectID = incrementObjectID + 9;
                            break;
                        case "ALL":
                            locationName = "All";
                            break;
                        default:
                            locationName = "All";
                            break;
                    }
                    ResultModel<CreateObjectResultModel> globalAnaloglocationModel = CreateObjectAndCacheData(entityBaseModel, processorConfiguration, BacnetObjectType.ObjectAnalogValue, AppResource.AnalogValueBlindObjectName + "_" + locationName, AppConstants.EntityTypes.Floor, BlindLocationObjectID, true);

                    if (!globalAnaloglocationModel.IsSuccess)
                    {
                        Logger.Instance.Log(LogLevel.ERROR, "ZoneProcessor.ProcessEntity : Zone object creation failure in BACnet stack..");
                        return globalAnaloglocationModel.IsSuccess;
                    }
                    SetProperty(globalAnaloglocationModel.Data.DeviceId, globalAnaloglocationModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropDescription, AppResource.AnalogViewRepresentBlindFloor, AppConstants.LIGHTING_BRIGHTENESS_OBJECT, processorConfiguration, entityBaseModel);
                    SetProperty(globalAnaloglocationModel.Data.DeviceId, globalAnaloglocationModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropUnits, BacnetEngineeringUnits.UnitsPercent);
                    SetProperty(globalAnaloglocationModel.Data.DeviceId, globalAnaloglocationModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropRelinquishDefault, AppConstants.RELINQUISH_DEFAULT);
                    SetProperty(globalAnaloglocationModel.Data.DeviceId, globalAnaloglocationModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropTimeDelay, processorConfiguration.BACnetConfigurationData.TimeDelay);
                    SetProperty(globalAnaloglocationModel.Data.DeviceId, globalAnaloglocationModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropNotificationClass, processorConfiguration.VDNotificationClassInstanceNumber);
                    SetProperty(globalAnaloglocationModel.Data.DeviceId, globalAnaloglocationModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropHighLimit, AppConstants.LIGHT_FIXTURE_HIGH_LIMIT);
                    //SetProperty(globalAnaloglocationModel.Data.DeviceId, globalAnaloglocationModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropProprietaryGridX, zoneEntityModel.ProprietaryGridX);
                    //SetProperty(globalAnaloglocationModel.Data.DeviceId, globalAnaloglocationModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropProprietaryGridY, zoneEntityModel.ProprietaryGridY);
                    SetProperty(globalAnaloglocationModel.Data.DeviceId, globalAnaloglocationModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropNotifyType, processorConfiguration.BACnetConfigurationData.NotifyType);

                    //Info: Default "Limit enable" property override with true flag
                    SetProperty(globalAnaloglocationModel.Data.DeviceId, globalAnaloglocationModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropLimitEnable, limitEnableBitsModel);
                    if (eventTransitionBitsModel != null)
                    {
                        SetProperty(globalAnaloglocationModel.Data.DeviceId, globalAnaloglocationModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropEventEnable, eventTransitionBitsModel);
                    }
                    entityBaseModel.BACnetData.ObjectDetails.Values.Where(s => s.ObjectID == globalAnaloglocationModel.Data.ObjectId && s.ObjectType == BacnetObjectType.ObjectAnalogValue).First().PresentValue = AppConstants.RELINQUISH_DEFAULT;
                    #endregion AnalogValue
                }
                #endregion
                return true;
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "CreateBuildingLevelShadingControlObjects() Exception Received ");
                return false;
            }
        }
    }
}
