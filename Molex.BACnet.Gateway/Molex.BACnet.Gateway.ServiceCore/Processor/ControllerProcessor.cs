﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              ControllerProcessor.cs
///   Description:        This class is used for processing the Entity module to BACnet Stack integration Module
///   Author:             Rupesh Saw                  
///   Date:               05/23/17
///---------------------------------------------------------------------------------------------
#endregion


using Molex.BACnet.Gateway.Log;
using Molex.BACnet.Gateway.ServiceCore.AppResources;
using Molex.BACnet.Gateway.ServiceCore.Constants;
using Molex.BACnet.Gateway.ServiceCore.Helpers;
using Molex.BACnet.Gateway.ServiceCore.Managers;
using Molex.BACnet.Gateway.ServiceCore.Models;
using Molex.BACnet.Gateway.ServiceCore.Models.BACnet;
using Molex.BACnet.Gateway.ServiceCore.Models.Entity;
using Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel;
using Molex.BACnet.Gateway.ServiceCore.Repository;
using Molex.StackDataObjects.APIModels;
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Response;
using System;
using System.Linq;
using System.Collections.Generic;

namespace Molex.BACnet.Gateway.ServiceCore.Processor
{
    /// <summary>
    /// This class is used for processing the Entity module to BACnet Stack integration Module
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:41 PM</TimeStamp>
    /// <seealso cref="Molex.BACnet.Gateway.ServiceCore.Processor.ProcessorBase" />
    public class ControllerProcessor : ProcessorBase
    {
        /// <summary>
        /// Creating objects for ControllerEnityModel mapped to Bacnet Stack
        /// </summary>
        /// <param name="entityBaseModel">EntityBaseModel</param>
        /// <param name="processorConfiguration">BACnetConfigurationModel</param>
        /// <returns>bool</returns>

        public override bool ProcessEntity(EntityBaseModel entityBaseModel, Models.ProcessorConfigurationModel processorConfiguration)
        {
            Logger.Instance.Log(LogLevel.DEBUG, "ControllerProcessor.ProcessEntity: Processing Controller" + entityBaseModel.EntityName);

            try
            {
                BACnetConfigurationModel bacnetConfigurationModel = processorConfiguration.BACnetConfigurationData;
                ControllerEntityModel controllerEntityModel = (ControllerEntityModel)entityBaseModel;
                if (!(entityBaseModel.EntityType == Constants.AppConstants.EntityTypes.Controller))
                {
                    Logger.Instance.Log(LogLevel.ERROR, "ControllerProcessor.ProcessEntity: Entity model mismatch. Controller Entity model required, Received:" + entityBaseModel.EntityType);
                    return false;
                }

                bool isSuccess = ObjectIdentifierManager.Instance.AddDeviceId(processorConfiguration.BACnetConfigurationData.ControllerId);

                if (!isSuccess)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "ControllerProcessor.ProcessEntity: Device id is not successfully added to ObjectIdentifierManager. Device: " + processorConfiguration.BACnetConfigurationData.ControllerId);
                    return false;
                }

                ResponseResult result = StackManager.Instance.CreateDeviceObject(bacnetConfigurationModel.ControllerId, 0, 0);

                if (!result.IsSucceed)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "ControllerProcessor.ProcessEntity:Required Device not found ");
                    return false;
                }

                //Set password for Device
                ResponseResult passwordResult = StackManager.Instance.SetDevicePassword((int)bacnetConfigurationModel.ControllerId, bacnetConfigurationModel.BACnetPassword);
                if (!passwordResult.IsSucceed)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "ControllerProcessor.ProcessEntity: Set vendor Password fail for controller " + bacnetConfigurationModel.ControllerId);
                    return false;
                }


                entityBaseModel.BACnetData.DeviceID = (int)bacnetConfigurationModel.ControllerId;
                // Caches the data based on Dictonary (Key/Value) Key- BACnetobjectId and Value- EntityModel.
                CacheData(bacnetConfigurationModel.ControllerId, BacnetObjectType.ObjectDevice, bacnetConfigurationModel.ControllerId, entityBaseModel, AppConstants.LIGHTING_OBJECTDEVICE_OBJECT);
                #region Notification class

                ResultModel<CreateObjectResultModel> resultModel = CreateObjectAndCacheData(entityBaseModel, processorConfiguration, BacnetObjectType.ObjectNotificationClass, AppConstants.LIGHTING_NOTIFICATIONCLASS_OBJECT, AppConstants.EntityTypes.Controller);

                if (!resultModel.IsSuccess)
                {
                    Logger.Instance.Log(LogLevel.DEBUG, "ControllerProcessor.ProcessEntity: Create object and cache data failed for Notification class");
                    return false;
                }

                processorConfiguration.VRDNotificationClassInstanceNumber = resultModel.Data.ObjectId;
                SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, BacnetObjectType.ObjectNotificationClass, BacnetPropertyID.PropDescription, AppResource.NotificationClassRepresent, AppConstants.LIGHTING_NOTIFICATIONCLASS_OBJECT, processorConfiguration, entityBaseModel);
                SetPropertyAccessType(resultModel.Data.DeviceId, resultModel.Data.ObjectId, BacnetObjectType.ObjectNotificationClass, BacnetPropertyID.PropPriority, PropAccessType.READ_WRITE, true);

                uint[] priorityListArray = null;
                if (processorConfiguration.UserBACnetObjects != null && AppCoreRepo.Instance.BACnetConfigurationData.BuildingNotificationPriority.OverridePersistantState == false)
                {
                    foreach (var item in processorConfiguration.UserBACnetObjects)
                    {
                        if (item.Key == entityBaseModel.EntityKey)
                        {
                            if (item.Value.NotificationClasses.Any(s => s.ObjectID == 1))
                            {
                                NotificationClassPersistedModel obj = item.Value.NotificationClasses.Where(s => s.ObjectID == 1).First();
                                if (obj == null)
                                    break;
                                if (obj.NotificationPriority == null)
                                    break;
                                if (obj.NotificationPriority.Count != 3)
                                    break;
                                priorityListArray = new uint[3];
                                priorityListArray[2] = obj.NotificationPriority[2];
                                priorityListArray[1] = obj.NotificationPriority[1];
                                priorityListArray[0] = obj.NotificationPriority[0];
                                break;
                            }
                        }
                    }
                }
                if (priorityListArray == null)
                {
                    priorityListArray = new uint[3];
                    priorityListArray[2] = (uint)AppCoreRepo.Instance.BACnetConfigurationData.BuildingNotificationPriority.ToNormal;
                    priorityListArray[1] = (uint)AppCoreRepo.Instance.BACnetConfigurationData.BuildingNotificationPriority.ToFault;
                    priorityListArray[0] = (uint)AppCoreRepo.Instance.BACnetConfigurationData.BuildingNotificationPriority.ToOffNormal;
                }
                SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, BacnetObjectType.ObjectNotificationClass, BacnetPropertyID.PropPriority, priorityListArray, AppConstants.LIGHTING_NOTIFICATIONCLASS_OBJECT, processorConfiguration, entityBaseModel);
                #endregion  Notification class


                string key = EntityHelper.GetObjectDetailsKey(BacnetObjectType.ObjectDevice, AppConstants.LIGHTING_OBJECTDEVICE_OBJECT);
                string deviceName = GetObjectName(processorConfiguration.EntityBACnetCache, entityBaseModel, key, BacnetObjectType.ObjectDevice, bacnetConfigurationModel.ControllerId);

                SetProperty(bacnetConfigurationModel.ControllerId, bacnetConfigurationModel.ControllerId, BacnetObjectType.ObjectDevice, BacnetPropertyID.PropObjectName, deviceName);
                SetProperty(bacnetConfigurationModel.ControllerId, bacnetConfigurationModel.ControllerId, BacnetObjectType.ObjectDevice, BacnetPropertyID.PropDescription, bacnetConfigurationModel.Description, AppConstants.LIGHTING_OBJECTDEVICE_OBJECT, processorConfiguration, entityBaseModel);
                SetProperty(bacnetConfigurationModel.ControllerId, bacnetConfigurationModel.ControllerId, BacnetObjectType.ObjectDevice, BacnetPropertyID.PropDayLightSavingsStatus, bacnetConfigurationModel.DaylightSavingStatus);
                SetProperty(bacnetConfigurationModel.ControllerId, bacnetConfigurationModel.ControllerId, BacnetObjectType.ObjectDevice, BacnetPropertyID.PropUtcOffSet, bacnetConfigurationModel.UTCOffset);
                SetProperty(bacnetConfigurationModel.ControllerId, bacnetConfigurationModel.ControllerId, BacnetObjectType.ObjectDevice, BacnetPropertyID.PropApduTimesOut, bacnetConfigurationModel.APDUTimeout);
                SetProperty(bacnetConfigurationModel.ControllerId, bacnetConfigurationModel.ControllerId, BacnetObjectType.ObjectDevice, BacnetPropertyID.PropApduSegmentTimesOut, bacnetConfigurationModel.APDUSegmentTimeout);
                SetProperty(bacnetConfigurationModel.ControllerId, bacnetConfigurationModel.ControllerId, BacnetObjectType.ObjectDevice, BacnetPropertyID.PropNumberOfApduRetries, bacnetConfigurationModel.APDURetries);
                SetProperty(bacnetConfigurationModel.ControllerId, bacnetConfigurationModel.ControllerId, BacnetObjectType.ObjectDevice, BacnetPropertyID.PropApplicationSoftwareVersion, AppCoreRepo.Instance.ApplicationSoftwareVersion);

                BuildingProcessor buildingProcessor = new BuildingProcessor();

                foreach (var item in entityBaseModel.Childs)
                {
                    isSuccess = buildingProcessor.ProcessEntity(item, processorConfiguration);

                    if (!isSuccess)
                        break;
                }

                isSuccess = ((new ObjectProcessor()).ProcessEntity(entityBaseModel, processorConfiguration));

                //SetProperties  for ProtocolObjectTypesSupported and ProtocolServiceSupport
                isSuccess = (new PostProcessor()).ProcessEntity(entityBaseModel, processorConfiguration);
                if (!isSuccess)
                {
                    Logger.Instance.Log(LogLevel.DEBUG, "ControllerProcessor.ProcessEntity : SetProperties failed for ProtocolObjectTypesSupport or ProtocolServiceSupport under PostProcessor");
                    return isSuccess;
                }

                //Set Structure Object List Properties 
                List<ObjectIdentifierModel> listObjectIdentifierModel = new List<ObjectIdentifierModel>();
                foreach (EntityBaseModel data in entityBaseModel.Childs)
                {
                    ObjectIdentifierModel objectIdentifierModel = new ObjectIdentifierModel();
                    objectIdentifierModel.InstanceNumber = data.BACnetData.ObjectDetails[BacnetObjectType.ObjectStructuredView.ToString()].ObjectID;
                    objectIdentifierModel.ObjectType = BacnetObjectType.ObjectStructuredView;
                    listObjectIdentifierModel.Add(objectIdentifierModel);
                }

                if (listObjectIdentifierModel.Count > 0)
                    StackManager.Instance.WriteProperty((uint)resultModel.Data.DeviceId, BacnetObjectType.ObjectDevice, resultModel.Data.DeviceId, listObjectIdentifierModel, BacnetPropertyID.PropStructuredObjectList, BacnetDataType.BacnetDTObjectIDArray);

                Logger.Instance.Log(LogLevel.DEBUG, "ControllerProcessor.ProcessEntity:Processing Controller Completed");

                return isSuccess;
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "ControllerProcessor.ProcessEntity : Processing Controller failed ");
                return false;
            }
        }

    }
}
