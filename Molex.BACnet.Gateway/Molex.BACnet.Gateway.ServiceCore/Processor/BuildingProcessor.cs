﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              BuildingProcessor.cs
///   Description:        This class is used for processing the Entity module to BACnet Stack integration Module
///   Author:             Rupesh Saw                  
///   Date:               05/23/17
///---------------------------------------------------------------------------------------------
#endregion

using Molex.BACnet.Gateway.Log;
using Molex.BACnet.Gateway.ServiceCore.AppResources;
using Molex.BACnet.Gateway.ServiceCore.Constants;
using Molex.BACnet.Gateway.ServiceCore.Managers;
using Molex.BACnet.Gateway.ServiceCore.Models;
using Molex.BACnet.Gateway.ServiceCore.Models.BACnet;
using Molex.BACnet.Gateway.ServiceCore.Models.Entity;
using Molex.BACnet.Gateway.ServiceCore.Repository;
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Models;
using System;
using System.Linq;
using System.Collections.Generic;
using Molex.BACnet.Gateway.ServiceCore.Models.MolexAPIModels;
using Molex.BACnet.Gateway.ServiceCore.Helpers;

namespace Molex.BACnet.Gateway.ServiceCore.Processor
{
    /// <summary>
    /// This class is used for processing the Entity module to BACnet Stack integration Module
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:40 PM</TimeStamp>
    /// <seealso cref="Molex.BACnet.Gateway.ServiceCore.Processor.ProcessorBase" />
    public class BuildingProcessor : ProcessorBase
    {
        /// <summary>
        /// Creating objects for BuildingEnityModel mapped to Bacnet Stack
        /// </summary>
        /// <param name="entityBaseModel">EntityBaseModel</param>
        /// <param name="processorConfiguration">BACnetConfigurationModel</param>
        /// <returns>bool</returns>
        public override bool ProcessEntity(Models.Entity.EntityBaseModel entityBaseModel, Models.ProcessorConfigurationModel processorConfiguration)
        {
            EventTransitionBitsModel eventTransitionBitsModel = null;
            LimitEnableBitsModel limitEnableBitsModel = new LimitEnableBitsModel();
            limitEnableBitsModel.HighLimit = true;
            limitEnableBitsModel.LowLimit = true;
            Logger.Instance.Log(LogLevel.DEBUG, "BuildingProcessor.ProcessEntity: Processing Building " + entityBaseModel.EntityName);

            if (!(entityBaseModel.EntityType == Constants.AppConstants.EntityTypes.Building))
            {
                Logger.Instance.Log(LogLevel.ERROR, "BuildingProcessor.ProcessEntity: Entity model mismatch..Building Entity model required. Entity type Available : " + entityBaseModel.EntityType);
                return false;
            }

            BuildingEntityModel buildingEntityModel = (BuildingEntityModel)entityBaseModel;

            #region VDR(Controller) Level SetPropertiy defined for Location

            BACnetConfigurationModel bacnetConfigurationModel = processorConfiguration.BACnetConfigurationData;
            // Location data fetched from BuildingEntityModel as per project file.
            SetProperty(bacnetConfigurationModel.ControllerId, bacnetConfigurationModel.ControllerId, BacnetObjectType.ObjectDevice, BacnetPropertyID.PropLocation, buildingEntityModel.Location);

            //copy loaction to configurationmodel to set at zone level
            processorConfiguration.Location = buildingEntityModel.Location;

            #endregion VDR(Controller) Level Set Location Propertiy


            #region StructuredView

            ResultModel<CreateObjectResultModel> resultModel = CreateObjectAndCacheData(entityBaseModel, processorConfiguration, BacnetObjectType.ObjectStructuredView, AppConstants.LIGHTING_STRUCTUREDVIEW_OBJECT, AppConstants.EntityTypes.Building);

            if (!resultModel.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.DEBUG, "BuildingProcessor.ProcessEntity: Create object and cache data failed ");
                return false;
            }

            SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, BacnetObjectType.ObjectStructuredView, BacnetPropertyID.PropDescription, buildingEntityModel.EntityName, AppConstants.LIGHTING_STRUCTUREDVIEW_OBJECT, processorConfiguration, entityBaseModel);
            SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, BacnetObjectType.ObjectStructuredView, BacnetPropertyID.PropNodeType, AppConstants.NodeType.BUILDING);
            SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, BacnetObjectType.ObjectStructuredView, BacnetPropertyID.PropNodeSubType, buildingEntityModel.EntityName);

            #endregion StructuredView

            if (processorConfiguration.BACnetConfigurationData.IsBuildingAlarm)
            {
                eventTransitionBitsModel = new EventTransitionBitsModel();
                eventTransitionBitsModel.ToFault = true;
                eventTransitionBitsModel.ToNormal = true;
                eventTransitionBitsModel.ToOffNormal = true;
            }
            else
            {
                eventTransitionBitsModel = new EventTransitionBitsModel();
                eventTransitionBitsModel.ToFault = false;
                eventTransitionBitsModel.ToNormal = false;
                eventTransitionBitsModel.ToOffNormal = false;
            }

            //Light Level for all zone Type
            //Spliting of User Zone Light Level , General Zone Light Level , Beacon Zone Light Level & Blind Sheding Control

            //User Space
            CreateBuildingLevelControlObject(entityBaseModel, processorConfiguration, eventTransitionBitsModel, limitEnableBitsModel,
                resultModel, AppConstants.ZoneTypes.UserSpace);

            //General Space
            CreateBuildingLevelControlObject(entityBaseModel, processorConfiguration, eventTransitionBitsModel, limitEnableBitsModel,
                resultModel, AppConstants.ZoneTypes.GeneralSpace);

            //Beacon Space
            CreateBuildingLevelControlObject(entityBaseModel, processorConfiguration, eventTransitionBitsModel, limitEnableBitsModel,
                resultModel, AppConstants.ZoneTypes.BeaconSpace);

            #region Occupancy Building Level

            ResultModel<CreateObjectResultModel> globalOccupancyBinaryInputModel = CreateObjectAndCacheData(entityBaseModel, processorConfiguration, BacnetObjectType.ObjectBinaryInput, AppConstants.BUILDING_OCCUPANCY_OBJECT, AppConstants.EntityTypes.Building);

            if (!globalOccupancyBinaryInputModel.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.ERROR, "Building.ProcessEntity : Occupancy Building Level creation failure in BACnet stack..");
                return globalOccupancyBinaryInputModel.IsSuccess;
            }

            SetProperty(globalOccupancyBinaryInputModel.Data.DeviceId, globalOccupancyBinaryInputModel.Data.ObjectId, BacnetObjectType.ObjectBinaryInput, BacnetPropertyID.PropDescription, "Building Occupancy State", AppConstants.BUILDING_OCCUPANCY_OBJECT, processorConfiguration, entityBaseModel);
            SetProperty(globalOccupancyBinaryInputModel.Data.DeviceId, globalOccupancyBinaryInputModel.Data.ObjectId, BacnetObjectType.ObjectBinaryInput, BacnetPropertyID.PropInactiveText, AppConstants.BINARY_OBJECT_INACTIVE_TEXT);
            SetProperty(globalOccupancyBinaryInputModel.Data.DeviceId, globalOccupancyBinaryInputModel.Data.ObjectId, BacnetObjectType.ObjectBinaryInput, BacnetPropertyID.PropActiveText, AppConstants.BINARY_OBJECT_ACTIVE_TEXT);
            SetProperty(globalOccupancyBinaryInputModel.Data.DeviceId, globalOccupancyBinaryInputModel.Data.ObjectId, BacnetObjectType.ObjectBinaryInput, BacnetPropertyID.PropNotifyType, processorConfiguration.BACnetConfigurationData.NotifyType);
            SetProperty(globalOccupancyBinaryInputModel.Data.DeviceId, globalOccupancyBinaryInputModel.Data.ObjectId, BacnetObjectType.ObjectBinaryInput, BacnetPropertyID.PropTimeDelay, processorConfiguration.BACnetConfigurationData.TimeDelay);
            SetProperty(globalOccupancyBinaryInputModel.Data.DeviceId, globalOccupancyBinaryInputModel.Data.ObjectId, BacnetObjectType.ObjectBinaryInput, BacnetPropertyID.PropEventEnable, eventTransitionBitsModel);
            SetProperty(globalOccupancyBinaryInputModel.Data.DeviceId, globalOccupancyBinaryInputModel.Data.ObjectId, BacnetObjectType.ObjectBinaryInput, BacnetPropertyID.PropNotificationClass, processorConfiguration.VRDNotificationClassInstanceNumber);
            SetProperty(globalOccupancyBinaryInputModel.Data.DeviceId, globalOccupancyBinaryInputModel.Data.ObjectId, BacnetObjectType.ObjectBinaryInput, BacnetPropertyID.PropDeviceType, AppConstants.BUILDING_OCCUPANCY_OBJECT);
            SetPropertyAccessType(globalOccupancyBinaryInputModel.Data.DeviceId, globalOccupancyBinaryInputModel.Data.ObjectId, BacnetObjectType.ObjectBinaryInput, BacnetPropertyID.PropDeviceType, PropAccessType.NOT_SUPPORTED, true);

            #endregion

            #region BinaryValue Building ON/OFF Control

            ResultModel<CreateObjectResultModel> globalBinaryValueModel = CreateObjectAndCacheData(entityBaseModel, processorConfiguration, BacnetObjectType.ObjectBinaryValue, AppConstants.BUILDING_ONOFF_OBJECT, AppConstants.EntityTypes.Building);

            if (!globalBinaryValueModel.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.ERROR, "BuildingProcessor.ProcessEntity : Building ON/OFF Control object creation failure in BACnet stack..");
                return globalBinaryValueModel.IsSuccess;
            }

            SetProperty(globalBinaryValueModel.Data.DeviceId, globalBinaryValueModel.Data.ObjectId, BacnetObjectType.ObjectBinaryValue, BacnetPropertyID.PropDescription, string.Format("{0} On/OFF", entityBaseModel.EntityName), AppConstants.BUILDING_ONOFF_OBJECT, processorConfiguration, entityBaseModel);
            SetProperty(globalBinaryValueModel.Data.DeviceId, globalBinaryValueModel.Data.ObjectId, BacnetObjectType.ObjectBinaryValue, BacnetPropertyID.PropNotifyType, processorConfiguration.BACnetConfigurationData.NotifyType);
            SetProperty(globalBinaryValueModel.Data.DeviceId, globalBinaryValueModel.Data.ObjectId, BacnetObjectType.ObjectBinaryValue, BacnetPropertyID.PropNotificationClass, processorConfiguration.VDNotificationClassInstanceNumber);
            SetProperty(globalBinaryValueModel.Data.DeviceId, globalBinaryValueModel.Data.ObjectId, BacnetObjectType.ObjectBinaryValue, BacnetPropertyID.PropTimeDelay, processorConfiguration.BACnetConfigurationData.TimeDelay);
            SetProperty(globalBinaryValueModel.Data.DeviceId, globalBinaryValueModel.Data.ObjectId, BacnetObjectType.ObjectBinaryValue, BacnetPropertyID.PropAlarmValue, BacnetBinaryPV.BinaryInactive);
            SetProperty(globalBinaryValueModel.Data.DeviceId, globalBinaryValueModel.Data.ObjectId, BacnetObjectType.ObjectBinaryValue, BacnetPropertyID.PropEventEnable, eventTransitionBitsModel);
            //SetProperty(globalBinaryValueModel.Data.DeviceId, globalBinaryValueModel.Data.ObjectId, BacnetObjectType.ObjectBinaryInput, BacnetPropertyID.PropInactiveText, AppConstants.BINARY_OBJECT_INACTIVE_TEXT);
            //SetProperty(globalBinaryValueModel.Data.DeviceId, globalBinaryValueModel.Data.ObjectId, BacnetObjectType.ObjectBinaryInput, BacnetPropertyID.PropActiveText, AppConstants.BINARY_OBJECT_ACTIVE_TEXT);

            #endregion

            if (AppCoreRepo.Instance.ZonesLookUp.Any(s => (s.Value as ZoneEntityModel).ZoneType == AppConstants.ZoneTypes.GeneralSpace ||
                (s.Value as ZoneEntityModel).ZoneType == AppConstants.ZoneTypes.UserSpace))
            {
                #region AnalogValue-BioDynamic


                ResultModel<CreateObjectResultModel> globalBioDynamicAnalogModel = CreateObjectAndCacheData(entityBaseModel, processorConfiguration, BacnetObjectType.ObjectAnalogValue, AppResource.BioDynamicControlName, AppConstants.EntityTypes.Building, 0, true);

                if (!globalBioDynamicAnalogModel.IsSuccess)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "ZoneProcessor.ProcessEntity : Zone object creation failure in BACnet stack..");
                    return globalBioDynamicAnalogModel.IsSuccess;
                }

                SetProperty(globalBioDynamicAnalogModel.Data.DeviceId, globalBioDynamicAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropDescription, AppResource.AnalogValueBioDynamicRepresentBuilding, AppResource.BioDynamicControlName, processorConfiguration, entityBaseModel);
                SetProperty(globalBioDynamicAnalogModel.Data.DeviceId, globalBioDynamicAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropUnits, BacnetEngineeringUnits.UnitsPercent);
                SetProperty(globalBioDynamicAnalogModel.Data.DeviceId, globalBioDynamicAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropRelinquishDefault, AppConstants.RELINQUISH_DEFAULT);
                SetProperty(globalBioDynamicAnalogModel.Data.DeviceId, globalBioDynamicAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropTimeDelay, processorConfiguration.BACnetConfigurationData.TimeDelay);
                SetProperty(globalBioDynamicAnalogModel.Data.DeviceId, globalBioDynamicAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropNotificationClass, processorConfiguration.VRDNotificationClassInstanceNumber);
                SetProperty(globalBioDynamicAnalogModel.Data.DeviceId, globalBioDynamicAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropHighLimit, AppConstants.LIGHT_FIXTURE_HIGH_LIMIT);
                //SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropProprietaryColorTemperature, AppResource.FixtureColorTemperature);
                //SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropProprietaryColorTemperatureUnit, BacnetEngineeringUnits.UnitsDegreesKelvin);
                SetProperty(globalBioDynamicAnalogModel.Data.DeviceId, globalBioDynamicAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropNotifyType, processorConfiguration.BACnetConfigurationData.NotifyType);

                //Info: Default "Limit enable" property override with true flag
                SetProperty(globalBioDynamicAnalogModel.Data.DeviceId, globalBioDynamicAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropLimitEnable, limitEnableBitsModel);

                SetProperty(globalBioDynamicAnalogModel.Data.DeviceId, globalBioDynamicAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropNotificationClass, processorConfiguration.VRDNotificationClassInstanceNumber);
                SetProperty(globalBioDynamicAnalogModel.Data.DeviceId, globalBioDynamicAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropEventEnable, eventTransitionBitsModel);
                #endregion AnalogValue

                #region AnalogValue-Saturation


                ResultModel<CreateObjectResultModel> globalSaturationAnalogModel = CreateObjectAndCacheData(entityBaseModel, processorConfiguration, BacnetObjectType.ObjectAnalogValue, AppResource.SaturationControlName, AppConstants.EntityTypes.Building, 0, true);

                if (!globalSaturationAnalogModel.IsSuccess)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "ZoneProcessor.ProcessEntity : Zone object creation failure in BACnet stack..");
                    return globalSaturationAnalogModel.IsSuccess;
                }

                SetProperty(globalSaturationAnalogModel.Data.DeviceId, globalSaturationAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropDescription, AppResource.AnalogValueSaturationRepresentBuilding, AppResource.SaturationControlName, processorConfiguration, entityBaseModel);
                SetProperty(globalSaturationAnalogModel.Data.DeviceId, globalSaturationAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropUnits, BacnetEngineeringUnits.UnitsPercent);
                SetProperty(globalSaturationAnalogModel.Data.DeviceId, globalSaturationAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropRelinquishDefault, AppConstants.RELINQUISH_DEFAULT);
                SetProperty(globalSaturationAnalogModel.Data.DeviceId, globalSaturationAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropTimeDelay, processorConfiguration.BACnetConfigurationData.TimeDelay);
                SetProperty(globalSaturationAnalogModel.Data.DeviceId, globalSaturationAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropNotificationClass, processorConfiguration.VDNotificationClassInstanceNumber);
                SetProperty(globalSaturationAnalogModel.Data.DeviceId, globalSaturationAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropHighLimit, AppConstants.LIGHT_FIXTURE_HIGH_LIMIT);
                //SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropProprietaryColorTemperature, AppResource.FixtureColorTemperature);
                //SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropProprietaryColorTemperatureUnit, BacnetEngineeringUnits.UnitsDegreesKelvin);
                SetProperty(globalSaturationAnalogModel.Data.DeviceId, globalSaturationAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropNotifyType, processorConfiguration.BACnetConfigurationData.NotifyType);

                //Info: Default "Limit enable" property override with true flag
                SetProperty(globalSaturationAnalogModel.Data.DeviceId, globalSaturationAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropLimitEnable, limitEnableBitsModel);

                SetProperty(globalSaturationAnalogModel.Data.DeviceId, globalSaturationAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropNotificationClass, processorConfiguration.VRDNotificationClassInstanceNumber);
                SetProperty(globalSaturationAnalogModel.Data.DeviceId, globalSaturationAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropEventEnable, eventTransitionBitsModel);
                #endregion AnalogValue
            }

            CreateBuildingLevelShadingControlObjects(entityBaseModel, processorConfiguration, eventTransitionBitsModel, limitEnableBitsModel,
                resultModel);

            if (AppCoreRepo.Instance.ZonesLookUp.Count(s => (s.Value as ZoneEntityModel).ZoneType == AppConstants.ZoneTypes.BlindSpace) != AppCoreRepo.Instance.ZonesLookUp.Count)
            {
                #region MultiStateValue

                ResultModel<CreateObjectResultModel> globalMultiStateValue = CreateObjectAndCacheData(entityBaseModel, processorConfiguration, BacnetObjectType.ObjectMultiStateValue, AppConstants.LIGHTING_LIGHTSCENE_OBJECT, AppConstants.EntityTypes.Building);


                if (!globalMultiStateValue.IsSuccess)
                {
                    Logger.Instance.Log(LogLevel.DEBUG, "BuildingProcessor:ProcessEntity: create object and cache data failed for " + BacnetObjectType.ObjectMultiStateValue);
                    return globalMultiStateValue.IsSuccess;
                }

                SetProperty(globalMultiStateValue.Data.DeviceId, globalMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropDescription, AppResource.MultiStateValueRepresentBuilding, AppConstants.LIGHTING_LIGHTSCENE_OBJECT, processorConfiguration, entityBaseModel);
                SetProperty(globalMultiStateValue.Data.DeviceId, globalMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropNumberOfStates, processorConfiguration.LightScene.Length);
                SetProperty(globalMultiStateValue.Data.DeviceId, globalMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropTimeDelay, processorConfiguration.BACnetConfigurationData.TimeDelay);
                SetProperty(globalMultiStateValue.Data.DeviceId, globalMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropNotificationClass, processorConfiguration.VRDNotificationClassInstanceNumber);
                SetProperty(globalMultiStateValue.Data.DeviceId, globalMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropRelinquishDefault, 1);//Array.IndexOf(AppCoreRepo.Instance.UserAndGeneralLightScene, AppConstants.DEFAULT_LightScene) + 1
                //SetProperty(globalMultiStateValue.Data.DeviceId, globalMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropProprietaryMood, Array.IndexOf(AppCoreRepo.Instance.AvailableMoods, AppConstants.DEFAULT_MOOD));
                SetProperty(globalMultiStateValue.Data.DeviceId, globalMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropNotifyType, processorConfiguration.BACnetConfigurationData.NotifyType);

                SetPropertyAccessType(globalMultiStateValue.Data.DeviceId, globalMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropProprietaryMood, PropAccessType.NOT_SUPPORTED, true);
                if (eventTransitionBitsModel != null)
                {
                    SetProperty(globalMultiStateValue.Data.DeviceId, globalMultiStateValue.Data.ObjectId, BacnetObjectType.ObjectMultiStateValue, BacnetPropertyID.PropEventEnable, eventTransitionBitsModel);
                }
                // State-Text SetProperties 
                ResultModel<bool> msvResult = SetMSVStateText(globalMultiStateValue, processorConfiguration.LightScene);
                if (!msvResult.IsSuccess)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "BuildingProcessor.ProcessEntity: State-Text SetProperties failed for Multistate Value Id " + globalMultiStateValue.Data.DeviceId);
                }

                #endregion MultiStateValue
            }

            FloorProcessor floorProcessor = new FloorProcessor();
            bool isSuccess = true;
            int floorStartID = 1001;
            foreach (var item in entityBaseModel.Childs)
            {
                isSuccess = floorProcessor.ProcessEntity(item, processorConfiguration, floorStartID);
                floorStartID = floorStartID + 100;
                if (!isSuccess)
                    break;
            }

            #region SubordinateList and Subordinate Annotations

            //WriteProperty defined for SubordinateList
            ResultModel<bool> result = base.SetStructuredViewSubOrdinateListProperty(resultModel, entityBaseModel);
            if (!result.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.ERROR, "BuildingProcessor.ProcessEntity: SubordinateList SetProperties failed for Structured View Object Id " + resultModel.Data.DeviceId);
            }

            //WriteProperty defined for Subordinate Annotations
            result = base.SetStructuredViewSubOrdinateAnnotationProperty(resultModel, entityBaseModel);
            if (!result.IsSuccess)
            {
                Logger.Instance.Log(LogLevel.ERROR, "BuildingProcessor.ProcessEntity: Subordinate Annotations SetProperties failed for Structured View Object Id " + resultModel.Data.DeviceId);
            }

            #endregion SubordinateList and Subordinate Annotations

            Logger.Instance.Log(LogLevel.DEBUG, "BuildingProcessor.ProcessEntity: Processing Building Completed");
            return isSuccess;
        }


        private bool CreateBuildingLevelControlObject(Models.Entity.EntityBaseModel entityBaseModel, Models.ProcessorConfigurationModel processorConfiguration,
                EventTransitionBitsModel eventTransitionBitsModel, LimitEnableBitsModel limitEnableBitsModel, ResultModel<CreateObjectResultModel> resultModel,
                AppConstants.ZoneTypes zoneType)
        {
            try
            {
                string objDetails = string.Empty;
                if (entityBaseModel.Childs == null)
                    return false;
                if (!AppCoreRepo.Instance.ZonesLookUp.Any(s => (s.Value as ZoneEntityModel).ZoneType == zoneType))
                    return false;
                switch (zoneType)
                {
                    case AppConstants.ZoneTypes.UserSpace:
                        objDetails = entityBaseModel.EntityType + " " + AppResource.UserLightLevel;
                        break;
                    case AppConstants.ZoneTypes.BeaconSpace:
                        objDetails = entityBaseModel.EntityType + " " + AppResource.BeaconLightLevel;
                        break;
                    case AppConstants.ZoneTypes.GeneralSpace:
                        objDetails = entityBaseModel.EntityType + " " + AppResource.GeneralLightLevel;
                        break;
                    default:
                        break;
                }
                #region AnalogValue

                ResultModel<CreateObjectResultModel> globalAnalogValue = CreateObjectAndCacheData(entityBaseModel, processorConfiguration, BacnetObjectType.ObjectAnalogValue, objDetails, AppConstants.EntityTypes.Building);

                if (!globalAnalogValue.IsSuccess)
                {
                    Logger.Instance.Log(LogLevel.DEBUG, "BuildingProcessor.ProcessEntity: create object and cache data failed for " + BacnetObjectType.ObjectAnalogValue);
                    return resultModel.IsSuccess;
                }

                SetProperty(globalAnalogValue.Data.DeviceId, globalAnalogValue.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropDescription, objDetails, AppConstants.LIGHTING_BRIGHTENESS_OBJECT, processorConfiguration, entityBaseModel);
                //SetProperty(globalAnalogValue.Data.DeviceId, globalAnalogValue.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropPresentValue, AppConstants.DEFAULT_BRIGHTNESS);
                SetProperty(globalAnalogValue.Data.DeviceId, globalAnalogValue.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropUnits, BacnetEngineeringUnits.UnitsPercent);
                SetProperty(globalAnalogValue.Data.DeviceId, globalAnalogValue.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropRelinquishDefault, AppConstants.RELINQUISH_DEFAULT);
                SetProperty(globalAnalogValue.Data.DeviceId, globalAnalogValue.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropTimeDelay, processorConfiguration.BACnetConfigurationData.TimeDelay);
                SetProperty(globalAnalogValue.Data.DeviceId, globalAnalogValue.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropNotificationClass, processorConfiguration.VRDNotificationClassInstanceNumber);
                SetProperty(globalAnalogValue.Data.DeviceId, globalAnalogValue.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropHighLimit, AppConstants.LIGHT_FIXTURE_HIGH_LIMIT);
                //SetProperty(globalAnalogValue.Data.DeviceId, globalAnalogValue.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropProprietaryColorTemperature, AppResource.FixtureColorTemperature);
                //SetProperty(globalAnalogValue.Data.DeviceId, globalAnalogValue.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropProprietaryColorTemperatureUnit, BacnetEngineeringUnits.UnitsDegreesKelvin);
                SetProperty(globalAnalogValue.Data.DeviceId, globalAnalogValue.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropNotifyType, processorConfiguration.BACnetConfigurationData.NotifyType);

                //Info: Default "Limit enable" property override with true flag
                SetProperty(globalAnalogValue.Data.DeviceId, globalAnalogValue.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropLimitEnable, limitEnableBitsModel);

                if (eventTransitionBitsModel != null)
                {
                    SetProperty(globalAnalogValue.Data.DeviceId, globalAnalogValue.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropEventEnable, eventTransitionBitsModel);
                }
                return true;
                #endregion AnalogValue
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.DEBUG, "BuildingProcesser:ProcessEntity: CreateFloorLevelControlObject() ");
                return false;
            }
        }

        private bool CreateBuildingLevelShadingControlObjects(Models.Entity.EntityBaseModel entityBaseModel, Models.ProcessorConfigurationModel processorConfiguration,
                EventTransitionBitsModel eventTransitionBitsModel, LimitEnableBitsModel limitEnableBitsModel, ResultModel<CreateObjectResultModel> resultModel)
        {
            bool result = false;
            try
            {
                if (!AppCoreRepo.Instance.ZonesLookUp.Any(s => (s.Value as ZoneEntityModel).ZoneType == AppConstants.ZoneTypes.BlindSpace))
                    return false;

                #region AnalogValue - All

                ResultModel<CreateObjectResultModel> globalAnalogModel = CreateObjectAndCacheData(entityBaseModel, processorConfiguration, BacnetObjectType.ObjectAnalogValue, AppResource.AnalogValueBlindObjectName, AppConstants.EntityTypes.Building, 0, true);

                if (!globalAnalogModel.IsSuccess)
                {
                    Logger.Instance.Log(LogLevel.ERROR, "ZoneProcessor.ProcessEntity : Zone object creation failure in BACnet stack..");
                    return globalAnalogModel.IsSuccess;
                }
                SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropDescription, AppResource.AnalogViewRepresentBlindBuilding, AppConstants.LIGHTING_BRIGHTENESS_OBJECT, processorConfiguration, entityBaseModel);
                SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropUnits, BacnetEngineeringUnits.UnitsPercent);
                SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropRelinquishDefault, AppConstants.RELINQUISH_DEFAULT);
                SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropTimeDelay, processorConfiguration.BACnetConfigurationData.TimeDelay);
                SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropNotificationClass, processorConfiguration.VDNotificationClassInstanceNumber);
                SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropHighLimit, AppConstants.LIGHT_FIXTURE_HIGH_LIMIT);
                //SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropProprietaryGridX, zoneEntityModel.ProprietaryGridX);
                //SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropProprietaryGridY, zoneEntityModel.ProprietaryGridY);
                SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropNotifyType, processorConfiguration.BACnetConfigurationData.NotifyType);

                //Info: Default "Limit enable" property override with true flag
                SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropLimitEnable, limitEnableBitsModel);
                if (eventTransitionBitsModel != null)
                {
                    SetProperty(globalAnalogModel.Data.DeviceId, globalAnalogModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropEventEnable, eventTransitionBitsModel);
                }

                entityBaseModel.BACnetData.ObjectDetails.Values.Where(s => s.ObjectID == globalAnalogModel.Data.ObjectId && s.ObjectType == BacnetObjectType.ObjectAnalogValue).First().PresentValue = AppConstants.RELINQUISH_DEFAULT;
                #endregion AnalogValue

                List<Blind> blinds = new List<Blind>();
                foreach (var item in AppCoreRepo.Instance.ZonesLookUp.Values)
                {
                    if ((item as ZoneEntityModel).Blinds != null)
                        blinds.AddRange((item as ZoneEntityModel).Blinds);
                }

                #region Location -Wise
                blinds.Sort((a, b) => (a.location.CompareTo(b.location)));
                foreach (var item in blinds)
                {
                    #region Object Creation Validation
                    bool isObjectAlreadyCreated = false;
                    foreach (var objItem in (entityBaseModel as BuildingEntityModel).BACnetData.ObjectDetails)
                    {
                        string objectKey = objItem.Key.Substring(objItem.Key.LastIndexOf('_') + 1);
                        if (objectKey.ToUpperInvariant() == AppHelper.GetFullLocationName(item.location.ToUpperInvariant()).ToUpperInvariant())
                        {
                            isObjectAlreadyCreated = true;
                            break;
                        }
                    }
                    if (isObjectAlreadyCreated)
                        continue;
                    #endregion

                    #region AnalogValue Location Wise
                    string locationName = string.Empty;
                    switch (item.location)
                    {
                        case "C":
                            locationName = "Center";
                            break;
                        case "E":
                            locationName = "East";
                            break;
                        case "N":
                            locationName = "North";
                            break;
                        case "NE":
                            locationName = "North-East";
                            break;
                        case "NW":
                            locationName = "North-West";
                            break;
                        case "S":
                            locationName = "South";
                            break;
                        case "SE":
                            locationName = "South-East";
                            break;
                        case "SW":
                            locationName = "South-West";
                            break;
                        case "W":
                            locationName = "West";
                            break;
                        case "ALL":
                            locationName = "All";
                            break;
                        default:
                            locationName = "All";
                            break;
                    }
                    ResultModel<CreateObjectResultModel> globalAnaloglocationModel = CreateObjectAndCacheData(entityBaseModel, processorConfiguration, BacnetObjectType.ObjectAnalogValue, AppResource.AnalogValueBlindObjectName + "_" + locationName, AppConstants.EntityTypes.Building, 0, true);

                    if (!globalAnaloglocationModel.IsSuccess)
                    {
                        Logger.Instance.Log(LogLevel.ERROR, "ZoneProcessor.ProcessEntity : Zone object creation failure in BACnet stack..");
                        return globalAnaloglocationModel.IsSuccess;
                    }
                    SetProperty(globalAnaloglocationModel.Data.DeviceId, globalAnaloglocationModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropDescription, AppResource.AnalogViewRepresentBlindBuilding, AppConstants.LIGHTING_BRIGHTENESS_OBJECT, processorConfiguration, entityBaseModel);
                    SetProperty(globalAnaloglocationModel.Data.DeviceId, globalAnaloglocationModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropUnits, BacnetEngineeringUnits.UnitsPercent);
                    SetProperty(globalAnaloglocationModel.Data.DeviceId, globalAnaloglocationModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropRelinquishDefault, AppConstants.RELINQUISH_DEFAULT);
                    SetProperty(globalAnaloglocationModel.Data.DeviceId, globalAnaloglocationModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropTimeDelay, processorConfiguration.BACnetConfigurationData.TimeDelay);
                    SetProperty(globalAnaloglocationModel.Data.DeviceId, globalAnaloglocationModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropNotificationClass, processorConfiguration.VDNotificationClassInstanceNumber);
                    SetProperty(globalAnaloglocationModel.Data.DeviceId, globalAnaloglocationModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropHighLimit, AppConstants.LIGHT_FIXTURE_HIGH_LIMIT);
                    //SetProperty(globalAnaloglocationModel.Data.DeviceId, globalAnaloglocationModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropProprietaryGridX, zoneEntityModel.ProprietaryGridX);
                    //SetProperty(globalAnaloglocationModel.Data.DeviceId, globalAnaloglocationModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropProprietaryGridY, zoneEntityModel.ProprietaryGridY);
                    SetProperty(globalAnaloglocationModel.Data.DeviceId, globalAnaloglocationModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropNotifyType, processorConfiguration.BACnetConfigurationData.NotifyType);

                    //Info: Default "Limit enable" property override with true flag
                    SetProperty(globalAnaloglocationModel.Data.DeviceId, globalAnaloglocationModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropLimitEnable, limitEnableBitsModel);
                    if (eventTransitionBitsModel != null)
                    {
                        SetProperty(globalAnaloglocationModel.Data.DeviceId, globalAnaloglocationModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropEventEnable, eventTransitionBitsModel);
                    }
                    entityBaseModel.BACnetData.ObjectDetails.Values.Where(s => s.ObjectID == globalAnaloglocationModel.Data.ObjectId && s.ObjectType == BacnetObjectType.ObjectAnalogValue).First().PresentValue = AppConstants.RELINQUISH_DEFAULT;
                    #endregion AnalogValue
                }
                #endregion
                return true;
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "CreateBuildingLevelShadingControlObjects() Exception Received ");
                return false;
            }
        }
    }
}
