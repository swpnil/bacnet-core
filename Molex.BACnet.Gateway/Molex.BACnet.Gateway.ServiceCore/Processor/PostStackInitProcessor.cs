﻿#region File Header
// Copyright ©  2017 UniDEL Systems Pvt. Ltd.
// All rights are reserved. Reproduction or transmission in whole or in part, in any form or by any
// means, electronic, mechanical or otherwise, is prohibited without the prior written consent of the
// copyright owner.
//
// Filename          : PostStackInitProcessor.cs
// Author            : Amol Kulkarni
// Description       : This class will hold the functionality Register BACnet packet delay time and BBMD/FD functionality after stack init
// Revision History  : 
// 
// Date        Author            Modification
// 28-08-2017  Amol Kulkarni      File Created
#endregion
using Molex.BACnet.Gateway.Log;
using Molex.BACnet.Gateway.ServiceCore.Managers;
using Molex.BACnet.Gateway.ServiceCore.Repository;
using Molex.StackDataObjects.Response;
using System;
using System.Net;
namespace Molex.BACnet.Gateway.ServiceCore.Processor
{
    /// <summary>
    /// PostStackInitProcessor for BBMD/FD register and BACnet packet time delay after stack init
    /// </summary>
    /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>8/28/20176:53 PM</TimeStamp>
    /// <seealso cref="Molex.BACnet.Gateway.ServiceCore.Processor.ProcessorBase" />
    public class PostStackInitProcessor : ProcessorBase
    {

        #region PublicMethod
        /// <summary>
        /// Processes the specified processor configuration.
        /// </summary>
        /// <param name="processorConfiguration">The processor configuration.</param>
        /// <param name="BACNetRegisterPacketDelayTime">The bac net register packet delay time.</param>
        /// <returns></returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>8/28/20176:54 PM</TimeStamp>
        public bool Process(Models.ProcessorConfigurationModel processorConfiguration, uint BACNetRegisterPacketDelayTime)
        {
            bool IsSuccess = true;
            IsSuccess = RegisterPacketDelay(BACNetRegisterPacketDelayTime);

            if (!IsSuccess)
            {
                Logger.Instance.Log(LogLevel.ERROR, "PostStackInitProcessor:Process:Register packet delay fail");
                return false;
            }

            IsSuccess = RegisterForeignDevice(processorConfiguration);

            if (!IsSuccess)
            {
                Logger.Instance.Log(LogLevel.ERROR, "PostStackInitProcessor:Process:Register foreign device fail");
                return false;
            }

            return true;
        }

        #endregion PublicMethod

        #region InternalMethod
        /// <summary>
        /// Registers the packet delay.
        /// </summary>
        /// <param name="delayTime">The delay time.</param>
        /// <returns></returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>8/7/201712:05 PM</TimeStamp>
        internal static bool RegisterPacketDelay(uint delayTime)
        {
            return StackManager.RegisterPacketDelay(delayTime);
        }

        /// <summary>
        /// Registers the foreign device.
        /// </summary>
        /// <param name="processorConfiguration">Theconfiguration data.</param>
        /// <returns></returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>8/28/20174:57 PM</TimeStamp>
        internal static bool RegisterForeignDevice(Models.ProcessorConfigurationModel processorConfiguration)
        {
            //If IsBBMD:true EnableBBMD
            //else if FDIPAddress,FDUDPPort and FDTimeTOLive present then register foreign device
            //else Disable BBMD
            
            ResponseResult responseResult = null;
            try
            {
                responseResult = BBMDFDManager.Instance.GetBBMDStatus();

                if (processorConfiguration.BACnetConfigurationData.IsBBMD)
                {
                    if (!((Molex.StackDataObjects.Response.GetBBMDStatusResponse)(responseResult.Response)).IsBBMDEnabled)
                    {
                        responseResult = BBMDFDManager.Instance.EnableBBMD();

                        if (!responseResult.IsSucceed)
                        {
                            Logger.Instance.Log(LogLevel.ERROR, "PostStackInitProcessor:RegisterForeignDevice:Enable BBMD failed");
                            return false;
                        }

                    }

                    BBMDFDManager.Instance.WriteBDT(processorConfiguration.BACnetConfigurationData.IPAddress, processorConfiguration.BACnetConfigurationData.UDPPort, processorConfiguration.BACnetConfigurationData.BDTList);
                }
                else
                {
                    if (((Molex.StackDataObjects.Response.GetBBMDStatusResponse)(responseResult.Response)).IsBBMDEnabled)
                    {
                        responseResult = BBMDFDManager.Instance.DisableBBMD();
                    }

                    if (!responseResult.IsSucceed)
                    {
                        Logger.Instance.Log(LogLevel.ERROR, "PostStackInitProcessor:RegisterForeignDevice:Disable BBMD failed");
                        return false;
                    }

                    if (!AppCoreRepo.Instance.BACnetConfigurationData.FDReRegister)
                    {
                        bool IsFDRegister = BBMDFDManager.Instance.RegisterFD();
                        if (!IsFDRegister)
                        {
                            Logger.Instance.Log(LogLevel.ERROR, "PostStackInitProcessor:RegisterForeignDevice:Register Foreign device failed");
                            return false;
                        }
                    }
                    else
                        BBMDFDManager.Instance.StartReRegistration();

                }
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "PostStackInitProcessor:RegisterForeignDevice:Fail BBMD and FD functionality");
                return false;
            }

            return true;
        }



        #endregion InternalMethod
    }
}
