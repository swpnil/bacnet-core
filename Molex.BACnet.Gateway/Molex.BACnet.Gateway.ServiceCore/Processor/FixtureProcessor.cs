﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              FixtureProcessor.cs
///   Description:        This class is used for processing the Entity module to BACnet Stack integration Module
///   Author:             Amol Kulkarni           
///   Date:               06/02/17
///---------------------------------------------------------------------------------------------
#endregion

using Molex.BACnet.Gateway.Log;
using Molex.BACnet.Gateway.ServiceCore.Constants;
using Molex.BACnet.Gateway.ServiceCore.Models;
using Molex.BACnet.Gateway.ServiceCore.Models.BACnet;
using Molex.BACnet.Gateway.ServiceCore.Models.Entity;
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Models;
using System;
using System.Linq;

namespace Molex.BACnet.Gateway.ServiceCore.Processor
{
    /// <summary>
    /// This class is used for processing the Entity module to BACnet Stack integration Module
    /// </summary>
    /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>2/5/201710:41 AM</TimeStamp>
    public class FixtureProcessor : ProcessorBase
    {
        /// <summary>
        /// Creating objects for FixtureEnityModel mapped to Bacnet Stack
        /// </summary>
        /// <param name="entityBaseModel">The entity base model.</param>
        /// <param name="processorConfiguration">The processorConfiguration.</param>
        /// <returns>
        /// bool
        /// </returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>2/5/2017 10:51 AM</TimeStamp>
        public override bool ProcessEntity(Models.Entity.EntityBaseModel entityBaseModel, Models.ProcessorConfigurationModel processorConfiguration)
        {
            EventTransitionBitsModel eventTransitionBitsModel = null;
            LimitEnableBitsModel limitEnableBitsModel = new LimitEnableBitsModel();
            limitEnableBitsModel.HighLimit = true;
            limitEnableBitsModel.LowLimit = true;
            try
            {
                Logger.Instance.Log(LogLevel.DEBUG, "FixtureProcessor.ProcessEntity: Processing Fixture " + entityBaseModel.EntityName);

                if (!(entityBaseModel.EntityType == Constants.AppConstants.EntityTypes.Fixture))
                {
                    Logger.Instance.Log(LogLevel.DEBUG, "FixtureProcessor.ProcessEntity: Entity model mismatch.Fixture Entity model required. Entity type Available : " + entityBaseModel.EntityType);
                    return false;
                }
                FixtureEntityModel fixtureEntityModel = (FixtureEntityModel)entityBaseModel;


                #region Analog Value

                ResultModel<CreateObjectResultModel> resultModel = CreateObjectAndCacheData(entityBaseModel, processorConfiguration, BacnetObjectType.ObjectAnalogValue, AppConstants.LIGHTING_BRIGHTENESS_OBJECT, AppConstants.EntityTypes.Fixture);

                if (!resultModel.IsSuccess)
                {
                    Logger.Instance.Log(LogLevel.DEBUG, "FixtureProcessor.ProcessEntity: Create object and cache data failed ");
                    return false;
                }

                if (processorConfiguration.ZoneAlarmNotificationSetting.Any(x => x.Name == AppConstants.LIGHTING_BRIGHTENESS_OBJECT && x.IndividualValue))
                {
                    eventTransitionBitsModel = new EventTransitionBitsModel();
                    eventTransitionBitsModel.ToFault = true;
                    eventTransitionBitsModel.ToNormal = true;
                    eventTransitionBitsModel.ToOffNormal = true;
                }
                else
                {
                    eventTransitionBitsModel = new EventTransitionBitsModel();
                    eventTransitionBitsModel.ToFault = false;
                    eventTransitionBitsModel.ToNormal = false;
                    eventTransitionBitsModel.ToOffNormal = false;
                }

                #region Set Fixture Level Properties
                //info : To remove Relinquish default and priority array property from AV object(set with the access type not supported)
                SetPropertyAccessType(resultModel.Data.DeviceId, resultModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropRelinquishDefault, PropAccessType.NOT_SUPPORTED, true);
                SetPropertyAccessType(resultModel.Data.DeviceId, resultModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropPriorityArray, PropAccessType.NOT_SUPPORTED, true);

                SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropDescription, fixtureEntityModel.Description, AppConstants.LIGHTING_BRIGHTENESS_OBJECT, processorConfiguration, entityBaseModel);
                SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropUnits, BacnetEngineeringUnits.UnitsPercent);
                //SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropRelinquishDefault, AppConstants.RELINQUISH_DEFAULT);
                SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropProprietaryGridX, fixtureEntityModel.ProprietaryGridX);
                SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropProprietaryGridY, fixtureEntityModel.ProprietaryGridY);
                //SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropProprietaryColorTemperature, AppResource.FixtureColorTemperature);
                //SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropProprietaryColorTemperatureUnit, BacnetEngineeringUnits.UnitsDegreesKelvin);
                SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropNotificationClass, processorConfiguration.VDNotificationClassInstanceNumber);
                SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropHighLimit, AppConstants.LIGHT_FIXTURE_HIGH_LIMIT);
                SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropNotifyType, processorConfiguration.BACnetConfigurationData.NotifyType);
                SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropTimeDelay, processorConfiguration.BACnetConfigurationData.TimeDelay);

                SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropEventEnable, eventTransitionBitsModel);

                //Info: Default "Limit enable" property override with true flag
                SetProperty(resultModel.Data.DeviceId, resultModel.Data.ObjectId, BacnetObjectType.ObjectAnalogValue, BacnetPropertyID.PropLimitEnable, limitEnableBitsModel);
                #endregion Set Fixture Level Properties

                #endregion Analog Value

                Logger.Instance.Log(LogLevel.DEBUG, "FixtureProcessor.ProcessEntity: Processing Fixture Completed");

                return true;
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, LogLevel.ERROR, "FixtureProcessor.ProcessEntity:" + ex.Message);
                return false;
            }
        }
    }
}
