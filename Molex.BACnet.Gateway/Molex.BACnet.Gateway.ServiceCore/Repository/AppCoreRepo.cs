﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              <AppCoreRepo.cs>
///   Description:        <Application Repository class holding all data in one place to be used throughout application>
///   Author:             Mandar                  
///   Date:               05/19/17
///---------------------------------------------------------------------------------------------
#endregion

using Molex.BACnet.Gateway.ServiceCore.Constants;
using Molex.BACnet.Gateway.ServiceCore.Models;
using Molex.BACnet.Gateway.ServiceCore.Models.APIModels;
using Molex.BACnet.Gateway.ServiceCore.Models.Entity;
using Molex.BACnet.Gateway.ServiceCore.Models.PersistentModel;
using Molex.BACnet.Gateway.ServiceCore.SingletonCore;
using Molex.StackDataObjects.Constants;
using System;
using System.Linq;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace Molex.BACnet.Gateway.ServiceCore.Repository
{
    /// <summary>
    /// Application Repository class holding all data in one place to be used throughout application
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy>
    /// <TimeStamp>25-05-201705:46 PM</TimeStamp>
    /// <seealso cref="Molex.BACnet.Gateway.ServiceCore.SingletonCore.SingletonBase{Molex.BACnet.Gateway.ServiceCore.Repository.AppCoreRepo}" />
    public class AppCoreRepo : SingletonBase<AppCoreRepo>
    {

        private AppCoreRepo()
        {
            AllLightScene = new string[AppConstants.LIGHTSCENE_MAX_LIMIT];
        }

        #region Properties
        public int TotalDevicesCount { get; set; }
        /// <summary>
        /// Gets or sets the entity ba cnet cache.
        /// </summary>
        /// <value>
        /// The entity ba cnet cache.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>7/31/20177:03 PM</TimeStamp>
        public Dictionary<string, PersistedModels> PersistentModelCache { get; set; }

        /// <summary>
        /// Gets or sets the log folder path.
        /// </summary>
        public string LogFolderPath { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is logger disabled.
        /// </summary>
        public bool IsLoggerDisabled { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is syslog format disabled.
        /// </summary>
        public bool IsSyslogFormatDisabled { get; set; }

        public int LogDeviceId { get; set; }
        /// <summary>
        /// Gets or sets the BACnet configuration data.
        /// </summary>
        public BACnetConfigurationModel BACnetConfigurationData { get; set; }

        /// <summary>
        /// Gets or sets the molex project identifier.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>12-07-201701:55 PM</TimeStamp>
        public string MolexProjectID { get; set; }

        /// <summary>
        /// Gets or sets the controller entity model.
        /// </summary>
        public ControllerEntityModel ControllerEntityModel { get; set; }


        /// <summary>
        /// Holds Zonelevel lookup table
        /// key: zone/spacestate ID, value: EntityBaseModel
        /// </summary>
        public Dictionary<string, EntityBaseModel> ZonesLookUp { get; set; }

        /// <summary>
        /// Gets or sets the light scene.
        /// </summary>
        /// <value>
        /// The light scene.
        /// </value>
        public string[] AllLightScene { get; set; }

        /// <summary>
        /// Gets or sets the user and general light scene.
        /// </summary>
        /// <value>
        /// The user and general light scene.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>11/22/20173:36 PM</TimeStamp>
        public string[] UserAndGeneralLightScene { get; set; }

        /// <summary>
        /// Gets or sets the beacon light scene.
        /// </summary>
        /// <value>
        /// The beacon light scene.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>11/22/20173:36 PM</TimeStamp>
        public string[] BeaconLightScene { get; set; }

        /// <summary>
        /// Gets or sets the ba cnet object entity look up.Will be used for BACnet object Entity Lookup
        /// key:  BACnetMappedObjectID, value: EntityBaseModel
        /// </summary>
        public ConcurrentDictionary<string, EntityBaseModel> BACnetObjectEntityLookUp { get; set; }

        /// <summary>
        /// Gets or sets the API monitoring timer interval.
        /// </summary>
        /// <value>
        /// The API monitoring timer interval in seconds.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>5/29/20176:51 PM</TimeStamp>
        public int APIMonitoringTimerIntervalInSeconds { get; set; }

        /// <summary>
        /// Gets or sets the removed light scenes.
        /// </summary>
        /// <value>
        /// The removed light scenes.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>3/13/201812:10 PM</TimeStamp>
        public List<string> RemovedLightScenes { get; set; }

        /// <summary>
        /// Get or set BACnet configuration folder location -Configuration File Path
        /// </summary>
        public string ConfigurationFolder { get; set; }

        /// <summary>
        /// Gets or sets the project API URL retry in minutes.
        /// </summary>
        /// <value>
        /// The project API URL retry in minutes.
        /// </value>
        /// <CreatedBy>prasad.joshi</CreatedBy><TimeStamp>5/26/201710:34 AM</TimeStamp>
        public uint ProjectApiUrlRetryInMinutes { get; set; }

        /// <summary>
        /// Gets or sets the application software version.
        /// </summary>
        /// <value>
        /// The application software version.
        /// </value>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>30-05-201705:43 PM</TimeStamp>
        public string ApplicationSoftwareVersion { get; set; }

        /// <summary>
        /// Gets or sets the scheduled objects.
        /// </summary>
        /// <value>
        /// The scheduled objects.
        /// </value>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>9/13/201712:51 PM</TimeStamp>
        public Dictionary<uint, string> ScheduledObjects { get; set; }

        /// <summary>
        /// Gets or sets the sensor lookup.
        /// </summary>
        /// <value>
        /// The sensor lookup.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>6/7/20175:25 PM</TimeStamp>
        public Dictionary<string, EntityBaseModel> AllEntityLookUp { get; set; }

        /// <summary>
        /// Gets or sets the available moods.
        /// </summary>
        /// <value>
        /// The available moods.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>8/8/20176:48 PM</TimeStamp>
        public string[] AvailableMoods { get; set; }

        public string[] ResetAttributes { get; set; }

        public string[] AvailableResetAttributeCombos { get; set; }

        public string[] AvailableResetAttributeComboDescriptions { get; set; }

        /// <summary>
        /// Gets or sets the ba cnet packet delay time mili sec.
        /// </summary>
        /// <value>
        /// The ba cnet packet delay time mili sec.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>8/28/20175:18 PM</TimeStamp>
        public uint BACnetPacketDelayTimeMiliSec { get; set; }

        /// <summary>
        /// Gets or sets the available zone states.
        /// </summary>
        /// <value>
        /// The available zone states.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>11/23/201712:42 PM</TimeStamp>
        public string[] AvailableZoneStates { get; set; }


        /// <summary>
        /// Gets or sets the pub sub subscription ids.
        /// </summary>
        /// <value>
        /// The sub scription i ds.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>3/15/20184:02 PM</TimeStamp>
        public List<string> SubScriptionIDs { get; set; }

        /// <summary>
        /// This list use to add object id's for which WP is in process based on that 
        /// for those objects BACnet Gateway stop monitoring till complete the WP process.
        /// </summary>
        /// <value>
        /// The ba cnet objects wp.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>3/19/20187:05 PM</TimeStamp>
        public List<uint> BACnetObjectsInWPProcess { get; set; }

        /// <summary>
        /// Gets or sets the heart beat timer interval in seconds.
        /// </summary>
        /// <value>
        /// The heart beat timer interval in seconds.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>3/29/20186:12 PM</TimeStamp>
        public int HeartBeatTimerIntervalInSeconds { get; set; }

        /// <summary>
        /// Gets or sets the client identifier.
        /// </summary>
        /// <value>
        /// The client identifier.
        /// </value>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/25/20189:31 PM</TimeStamp>
        public string ClientID { get; set; }
        #endregion

        #region PublicMethod

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201705:46 PM</TimeStamp>
        public void Init()
        {
            ZonesLookUp = new Dictionary<string, EntityBaseModel>();
            BACnetObjectEntityLookUp = new ConcurrentDictionary<string, EntityBaseModel>();
            AllEntityLookUp = new Dictionary<string, EntityBaseModel>();
            PersistentModelCache = new Dictionary<string, PersistedModels>();
            ScheduledObjects = new Dictionary<uint, string>();
            SubScriptionIDs = new List<string>();
            BACnetObjectsInWPProcess = new List<uint>();
        }

        #endregion
    }
}
