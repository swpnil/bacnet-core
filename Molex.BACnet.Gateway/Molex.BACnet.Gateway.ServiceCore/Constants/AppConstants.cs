﻿#region CopyRights
///---------------------------------------------------------------------------------------------
///   Copyright (c) 2017 All Rights Reserved
///   Company Name:       Molex
///   Class:              <AppConstants.cs>
///   Description:        <This class will contain all constraint which can be used in all Modules>
///   Author:             Mandar                  
///   Date:               05/19/17
///---------------------------------------------------------------------------------------------
#endregion

using System;

namespace Molex.BACnet.Gateway.ServiceCore.Constants
{
    /// <summary>
    /// Constants defined, will be used through out application level
    /// </summary>
    /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201704:52 PM</TimeStamp>
    public class AppConstants
    {
        /// <summary>
        /// This value is used to set PV to -1 when out of service is true.
        /// </summary>
        /// <CreatedBy>Rohit Galgali</CreatedBy>
        /// <TimeStamp>7/12/20173:55 PM</TimeStamp>
        public const int ANALOG_VALUE_OBJECT_UNKNOWN_PRESENT_VALUE = -1;
        /// <summary>
        /// The building default zone identifier
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>12/29/20176:25 PM</TimeStamp>
        public const string BUILDING_DEFAULT_ZONE_ID = "0";

        /// <summary>
        /// The default building MSV description
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>12/29/20176:25 PM</TimeStamp>
        //public const string DEFAULT_BUILDING_MSV_DESCRIPTION = "PV is either a default value or a last value applied from Building level.";

        /// <summary>
        /// The default floor MSV description
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>12/29/20176:25 PM</TimeStamp>
        //public const string DEFAULT_FLOOR_MSV_DESCRIPTION = "PV is either a default value or a last value applied from Floor level.";

        /// <summary>
        /// The reinitialize device batchfile path
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>12/29/20176:25 PM</TimeStamp>
        public const string REINITIALIZE_DEVICE_BATCHFILE_PATH = "Scripts\\ReInitDeviceScript.bat";

        /// <summary>
        /// The maximum number of schedule objects
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>12/29/20176:25 PM</TimeStamp>
        public const int MAX_NUMBER_OF_SCHEDULE_OBJECTS = 10;

        /// <summary>
        /// The maximum number of trendlog objects
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>12/29/20176:25 PM</TimeStamp>
        public const int MAX_NUMBER_OF_TRENDLOG_OBJECTS = 10;

        /// <summary>
        /// The maximum number of eventenrollment objects
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>12/29/20176:25 PM</TimeStamp>
        public const int MAX_NUMBER_OF_EVENTENROLLMENT_OBJECTS = 10;

        /// <summary>
        /// The maximum number of calendar objects
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>12/29/20176:25 PM</TimeStamp>
        public const int MAX_NUMBER_OF_CALENDAR_OBJECTS = 10;

        /// <summary>
        /// The sensor default value
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>4/19/20186:51 PM</TimeStamp>
        public const int SENSOR_DEFAULT_VALUE = -99;

        /// <summary>
        /// The maximum number of notification objects
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>12/29/20176:26 PM</TimeStamp>
        public const int MAX_NUMBER_OF_NOTIFICATION_OBJECTS = 10;

        /// <summary>
        /// The maximum RGB value
        /// </summary>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>11/22/20175:30 PM</TimeStamp>
        public const int MAX_RGB_VALUE = 255255255;

        #region Lighting Object Properties

        /// <summary>
        /// The lighting structuredview object
        /// </summary>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>6/19/20172:49 PM</TimeStamp>
        /// Note: Marked static readonly cause assigned string.Empty. Make const while assigning value.
        public static readonly string LIGHTING_STRUCTUREDVIEW_OBJECT = string.Empty;

        /// <summary>
        /// The lighting brighteness object
        /// </summary>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>6/19/20172:36 PM</TimeStamp>
        public const string LIGHTING_BRIGHTENESS_OBJECT = "Brightness";

        /// <summary>
        /// The Zone Occupancy object
        /// </summary>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>6/19/20172:36 PM</TimeStamp>
        public const string ZONE_OCCUPANCY_OBJECT = "ZoneOccupancy";

        /// <summary>
        /// The Zone Occupancy object
        /// </summary>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>6/19/20172:36 PM</TimeStamp>
        public const string FLOOR_OCCUPANCY_OBJECT = "FloorOccupancy";

        /// <summary>
        /// The Zone Occupancy object
        /// </summary>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>6/19/20172:36 PM</TimeStamp>
        public const string BUILDING_OCCUPANCY_OBJECT = "BuildingOccupancy";

        /// <summary>
        /// The zone onoff object
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>12/4/20176:56 PM</TimeStamp>
        public const string ZONE_ONOFF_OBJECT = "ZoneState";

        public const string FLOOR_ONOFF_OBJECT = "FloorState";

        public const string BUILDING_ONOFF_OBJECT = "BuildingState";

        public const string FLOOR_AVERAGE_OBJECT = "FloorAverage";

        /// <summary>
        /// The lighting lightscene object
        /// </summary>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>6/19/20172:36 PM</TimeStamp>
        public const string LIGHTING_LIGHTSCENE_OBJECT = "LightScene";

        public const string RESET_ATTRIBUTES_AFTER_TIMEOUT = "ResetAttributesAfterTO";

        /// <summary>
        /// The lighting lightscene object
        /// </summary>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>6/19/20172:36 PM</TimeStamp>
        public const string PALETTES_OBJECT = "BeaconPalettes";

        /// <summary>
        /// The lighting lightscene object
        /// </summary>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>6/19/20172:36 PM</TimeStamp>
        public const string LIGHTING_MOOD_OBJECT = "Mood";

        /// <summary>
        /// The lighting dhtargetlux object
        /// </summary>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>6/19/20172:36 PM</TimeStamp>
        public const string LIGHTING_DHTARGETLUX_OBJECT = "DHTargetLux";

        /// <summary>
        /// The lighting prestimeout object
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>20-06-201711:53 AM</TimeStamp>
        public const string LIGHTING_PRESTIMEOUT_OBJECT = "OccupancyTimeOut";

        /// <summary>
        /// The lighting presrate object
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>20-06-201711:54 AM</TimeStamp>
        public const string LIGHTING_PRESRATE_OBJECT = "OccupancyFadeOutTime";

        /// <summary>
        /// The red colour
        /// </summary>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>11/1/20172:21 PM</TimeStamp>
        public const string LIGHTING_RGB_COLOUR_OBJECT = "RGB";

        /// <summary>
        /// The event enrollement object
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>4/12/201810:37 AM</TimeStamp>
        public const string LIGHTING_EVENTENROLLEMENT_OBJECT = "EventEnrollment";

        /// <summary>
        /// The trend log object
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>4/12/201810:37 AM</TimeStamp>
        public const string LIGHTING_TRENDLOG_OBJECT = "TrendLog";

        /// <summary>
        /// The lighting objectdevice object
        /// </summary>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>6/19/20172:50 PM</TimeStamp>
        /// Note: Marked static readonly cause assigned string.Empty. Make const while assigning value.
        public static readonly string LIGHTING_OBJECTDEVICE_OBJECT = string.Empty;

        /// <summary>
        /// The lighting sensor object
        /// </summary>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>6/19/20173:27 PM</TimeStamp>
        public static readonly string LIGHTING_SENSOR_OBJECT = string.Empty;

        /// <summary>
        /// The lighting notificationclass object
        /// </summary>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>6/19/20173:28 PM</TimeStamp>
        public static readonly string LIGHTING_NOTIFICATIONCLASS_OBJECT = string.Empty;



        #endregion

        #region Logger Constant

        /// <summary>
        /// The system log type application
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>26-05-201703:22 PM</TimeStamp>
        public const string SYS_LOG_TYPE_APPLICATION = "Gateway";

        #endregion

        #region BACnet Default Constant

        /// <summary>
        /// The default device identifier
        /// </summary>
        public const int DEFAULT_DEVICE_ID = -1;

        /// <summary>
        /// The default analog object resolution for Analog Input and Analog Value except (Temperature and Huminity sensor)
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>07-07-201702:01 PM</TimeStamp>
        public const Single DEFAULT_ANALOG_OBJECT_RESOLUTION = 1;

        /// <summary>
        /// The default resolution power/temporary/huminity sensor
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>07-07-201702:25 PM</TimeStamp>
        public const Single DEFAULT_RESOLUTION_POWER_TEMP_HUMINITY_SENSOR = 0.1f;

        #endregion

        /// <summary>
        /// EntityTypes declaration
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>25-05-201702:29 PM</TimeStamp>
        public enum EntityTypes
        {
            /// <summary>
            /// The controller
            /// </summary>
            /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>07-06-201706:21 PM</TimeStamp>
            Controller,
            /// <summary>
            /// The building/
            /// </summary>
            /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>07-06-201706:21 PM</TimeStamp>
            Building,
            /// <summary>
            /// The floor
            /// </summary>
            /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>07-06-201706:21 PM</TimeStamp>
            Floor,
            /// <summary>
            /// The zone
            /// </summary>
            /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>07-06-201706:21 PM</TimeStamp>
            Zone,
            /// <summary>
            /// The fixture
            /// </summary>
            /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>07-06-201706:21 PM</TimeStamp>
            Fixture,
            /// <summary>
            /// The sensor
            /// </summary>
            /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>07-06-201706:21 PM</TimeStamp>
            Sensor
        }

        /// <summary>
        /// Enum for subscription types
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/2/20187:16 PM</TimeStamp>
        public enum SubscriptionType
        {
            /// <summary>
            /// The space state data
            /// </summary>
            /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/2/20186:24 PM</TimeStamp>
            zoneData = 1,
            /// <summary>
            /// The object reliability data
            /// </summary>
            /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/2/20186:24 PM</TimeStamp>
            status = 2,
            /// <summary>
            /// The notification for sensor disabled of specific zone
            /// </summary>
            /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/2/20186:25 PM</TimeStamp>
            notification = 3,
            /// <summary>
            /// The sensor reading data
            /// </summary>
            sensorData = 4,

            /// <summary>
            /// The Occupancy,daylight and palette configurable values
            /// </summary>
            configurations = 5,

            /// <summary>
            /// The property values
            /// </summary>
            properties = 6,

            /// <summary>
            /// The Building & Floor level sensor average ("AL" | "AQ" | "TEMP" | "HUM" | "CT")
            /// </summary>
            average = 7,

            /// <summary>
            /// The occupancy state
            /// </summary>
            occupancy = 8,

            /// <summary>
            /// The power usage  
            /// </summary>
            power = 9
        }

        /// <summary>
        /// enum for published packet type
        /// </summary>
        public enum PacketType
        {
            dayLightHarvest,
            occupancy,
            lightScene,
            animation,
            state,
            brightness,
            palette,
            sensorStats,
            blind,
            biodynamic,
            saturation,
            beacon,
            light,
            occupied
        }

        /// <summary>
        /// As per BACnet Stack Node Type
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>31-05-201712:52 PM</TimeStamp>
        public enum NodeType
        {
            /// <summary>
            /// The unknown
            /// </summary>
            /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>07-06-201706:19 PM</TimeStamp>
            UNKNOWN = 0,
            /// <summary>
            /// The system
            /// </summary>
            /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>07-06-201706:19 PM</TimeStamp>
            SYSTEM,
            /// <summary>
            /// The network
            /// </summary>
            /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>07-06-201706:19 PM</TimeStamp>
            NETWORK,
            /// <summary>
            /// The device
            /// </summary>
            /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>07-06-201706:19 PM</TimeStamp>
            DEVICE,
            /// <summary>
            /// The organizational/
            /// </summary>
            /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>07-06-201706:20 PM</TimeStamp>
            ORGANIZATIONAL,
            /// <summary>
            /// The area
            /// </summary>
            /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>07-06-201706:20 PM</TimeStamp>
            AREA,
            /// <summary>
            /// The equipment
            /// </summary>
            /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>07-06-201706:20 PM</TimeStamp>
            EQUIPMENT,
            /// <summary>
            /// The point
            /// </summary>
            /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>07-06-201706:20 PM</TimeStamp>
            POINT,
            /// <summary>
            /// The collection
            /// </summary>
            /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>07-06-201706:20 PM</TimeStamp>
            COLLECTION,
            /// <summary>
            /// The property
            /// </summary>
            /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>07-06-201706:20 PM</TimeStamp>
            PROPERTY,
            /// <summary>
            /// The functional
            /// </summary>
            /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>07-06-201706:20 PM</TimeStamp>
            FUNCTIONAL,
            /// <summary>
            /// The other
            /// </summary>
            /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>07-06-201706:20 PM</TimeStamp>
            OTHER,
            /// <summary>
            /// The subsystem
            /// </summary>
            /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>8/10/20176:17 PM</TimeStamp>
            SUBSYSTEM,
            /// <summary>
            /// The building
            /// </summary>
            /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>8/10/20176:18 PM</TimeStamp>
            BUILDING,
            /// <summary>
            /// The floor
            /// </summary>
            /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>8/10/20176:21 PM</TimeStamp>
            FLOOR,
            /// <summary>
            /// The section
            /// </summary>
            /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>8/10/20176:21 PM</TimeStamp>
            SECTION,
            /// <summary>
            /// The module
            /// </summary>
            /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>8/10/20176:21 PM</TimeStamp>
            MODULE,
            /// <summary>
            /// The tree
            /// </summary>
            /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>8/10/20176:21 PM</TimeStamp>
            TREE,
            /// <summary>
            /// The member
            /// </summary>
            /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>8/10/20176:21 PM</TimeStamp>
            MEMBER,
            /// <summary>
            /// The protocol
            /// </summary>
            /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>8/10/20176:21 PM</TimeStamp>
            PROTOCOL,
            /// <summary>
            /// The room
            /// </summary>
            /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>8/10/20176:21 PM</TimeStamp>
            ROOM,
            /// <summary>
            /// The zone
            /// </summary>
            /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>8/10/20176:21 PM</TimeStamp>
            ZONE,
            /// <summary>
            /// The type
            /// </summary>
            /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>8/10/20176:21 PM</TimeStamp>
            TYPE
        }

        /// <summary>
        /// The split up string connecter
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>26-05-201703:23 PM</TimeStamp>
        public const string SPLIT_UP_STRING = "_";

        #region Error Constant
        /// <summary>
        /// The lightscene not supported error given by API
        /// </summary>
        /// <CreatedBy>Rohit Galgali</CreatedBy>
        /// <TimeStamp>9/27/20172:17 PM</TimeStamp>
        public const int ERROR_LIGHTSCENE_NOT_SUPPORTED = 1028;

        /// <summary>
        /// The error CoreSyncserver not supported
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>11/6/20172:22 PM</TimeStamp>
        public const int ERROR_CORESYNC_SERVER_NOT_SUPPORTED = 1019;

        /// <summary>
        /// The error category dataparsing
        /// </summary>
        public const int ERROR_CATEGORY_DATAPARSING = 1;

        /// <summary>
        /// The error category dataparsing empty json
        /// </summary>
        public const int ERROR_CATEGORY_DATAPARSING_EMPTY_JSON = 1;

        /// <summary>
        /// The error category dataparsing json format error
        /// </summary>
        public const int ERROR_CATEGORY_DATAPARSING_JSON_FORMAT_ERROR = 2;

        /// <summary>
        /// The error category modeldata
        /// </summary>
        public const int ERROR_CATEGORY_MODELDATA = 2;

        /// <summary>
        /// The error category modeldata data not found
        /// </summary>
        public const int ERROR_CATEGORY_MODELDATA_DATA_NOT_FOUND = 2;

        /// <summary>
        /// The error category server
        /// </summary>
        public const int ERROR_CATEGORY_SERVER = 3;

        /// <summary>
        /// The error category server unknown
        /// </summary>
        public const int ERROR_CATEGORY_SERVER_UNKNOWN = -1;

        /// <summary>
        /// The error category server unavaialble
        /// </summary>
        public const int ERROR_CATEGORY_SERVER_UNAVAILABLE = 3;

        /// <summary>
        /// The error category server authetication failed
        /// </summary>
        public const int ERROR_CATEGORY_SERVER_AUTHETICATION_FAILED = 4;

        /// <summary>
        /// The error category io
        /// </summary>
        public const int ERROR_CATEGORY_IO = 4;

        /// <summary>
        /// The error category io file not found
        /// </summary>
        public const int ERROR_CATEGORY_IO_FILE_NOT_FOUND = 4;

        /// <summary>
        /// The error category modelparsingerror
        /// </summary>
        public const int ERROR_CATEGORY_MODELPARSINGERROR = 5;

        /// <summary>
        /// The error category modelparsingerror model data parsing error
        /// </summary>
        public const int ERROR_CATEGORY_MODELPARSINGERROR_MODEL_DATA_PARSING_ERROR = 5;


        /// <summary>
        /// The already subscribe error code
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/9/20182:28 PM</TimeStamp>
        public const int API_ALREADY_SUBSCRIBE_ERROR_CODE = 1045;


        /// <summary>
        /// The automatic unsubscribe error code
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/201812:09 PM</TimeStamp>
        public const int API_AUTO_UNSUBSCRIBE_ERROR_CODE = 1048;

        #endregion

        #region BACnet Configuration

        /// <summary>
        /// The bacnet treestructured object filename
        /// </summary>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>7/20/201712:37 PM</TimeStamp>
        public const string BACNET_CSVSTRUCTURED_OBJECT_FILENAME = "Lighting Schedule Export-Molex.csv";

        /// <summary>
        /// The BACnet configuration file name
        /// </summary>
        public const string BACNET_CONFIGURATION_FILENAME = "BACnetConfig.json";

        /// <summary>
        /// The ba cnet obejct file name
        /// </summary>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>6/14/20175:25 PM</TimeStamp>
        public const string BACNET_OBEJCT_FILENAME = "ConfigurationData.json";

        /// <summary>
        /// The bacnet configuration Json schema file name
        /// </summary>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>29-05-201714:02</TimeStamp>"
        public const string BACnetConfigurationSchemaFileName = "BACnetSchemaConfig.json";

        /// <summary>
        /// Thisa is used for NotifyType object Property
        /// </summary>
        /// "<CreatedBy>nazneen.zahid</CreatedBy><TimeStamp>29-05-201714:19</TimeStamp>"
        public enum NotifyType
        {
            /// <summary>
            /// The alarm
            /// </summary>
            /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>12/29/20176:26 PM</TimeStamp>
            Alarm,

            /// <summary>
            /// The event
            /// </summary>
            /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>12/29/20176:26 PM</TimeStamp>
            Event,

            /// <summary>
            /// The ack notification
            /// </summary>
            /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>12/29/20176:26 PM</TimeStamp>
            ACKNotification
        }

        /// <summary>
        /// The color minimum limit
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>12/29/20176:24 PM</TimeStamp>
        public const int COLOR_MIN_LIMIT = 0;

        /// <summary>
        /// The brightness minimum limit
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>12/29/20176:24 PM</TimeStamp>
        public const Single BRIGHTNESS_MIN_LIMIT = 0;

        /// <summary>
        /// The brightness maximum limit
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>12/29/20176:24 PM</TimeStamp>
        public const Single BRIGHTNESS_MAX_LIMIT = 100;

        /// <summary>
        /// The lightscene minimum limit
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>12/29/20176:24 PM</TimeStamp>
        public const Single LIGHTSCENE_MIN_LIMIT = 1;

        /// <summary>
        /// The mood minimum limit
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>12/29/20176:24 PM</TimeStamp>
        public const Single MOOD_MIN_LIMIT = 0;

        /// <summary>
        /// The lightscene maximum limit
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>12/29/20176:24 PM</TimeStamp>
        public const int LIGHTSCENE_MAX_LIMIT = 100;

        /// <summary>
        /// The color maximum limit
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>12/29/20176:24 PM</TimeStamp>
        public const int COLOR_MAX_LIMIT = 255;

        /// <summary>
        /// The notification class
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>14-06-201706:48 PM</TimeStamp>
        //public const Single NOTIFICATION_CLASS = 1;

        /// <summary>
        /// The brightness value
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>15-06-201705:32 PM</TimeStamp>
        public const Single DEFAULT_BRIGHTNESS = 100;

        /// <summary>
        /// The relinquish default
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>15-06-201705:51 PM</TimeStamp>
        public const Single RELINQUISH_DEFAULT = 100;

        /// <summary>
        /// The active text
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>19-06-201704:42 PM</TimeStamp>
        public const string BINARY_OBJECT_ACTIVE_TEXT = "Occupied";

        /// <summary>
        /// The inactive text
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>19-06-201704:42 PM</TimeStamp>
        public const string BINARY_OBJECT_INACTIVE_TEXT = "Unoccupied";

        /// <summary>
        /// The milliseconds to second
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>21-06-201710:45 AM</TimeStamp>
        public const int MILLISECONDS_TO_SECOND = 1000;

        /// <summary>
        /// The present rate ratio
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>21-06-201710:45 AM</TimeStamp>
        public const int PRESENT_RATE_RATIO = 50;

        /// <summary>
        /// The light fixture high limit
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>21-06-201702:31 PM</TimeStamp>
        public const Single LIGHT_FIXTURE_HIGH_LIMIT = 100;

        // <summary>
        /// To set default light scene value at building/floor
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>8/1/20174:50 PM</TimeStamp>
        public const string DEFAULT_LightScene = "personal";

        /// <summary>
        /// The default mood
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>8/11/20171:14 PM</TimeStamp>
        public const string DEFAULT_MOOD = "vigilant";
        #endregion

        /// <summary>
        /// Sensor Types
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>05-06-201705:43 PM</TimeStamp>
        public enum SensorType
        {
            /// <summary>
            /// The not supported
            /// </summary>
            /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>07-06-201706:17 PM</TimeStamp>
            NotSupported,
            /// <summary>
            /// The color temperature
            /// </summary>
            /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>07-06-201706:17 PM</TimeStamp>
            ColorTemperature,
            /// <summary>
            /// The air quality
            /// </summary>
            /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>07-06-201706:17 PM</TimeStamp>
            AirQuality,
            /// <summary>
            /// The presense
            /// </summary>
            /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>07-06-201706:17 PM</TimeStamp>
            Presence,
            /// <summary>
            /// The power
            /// </summary>
            /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>07-06-201706:18 PM</TimeStamp>
            Power,
            /// <summary>
            /// The lux level
            /// </summary>
            /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>07-06-201706:18 PM</TimeStamp>
            LuxLevel,
            /// <summary>
            /// The temperature
            /// </summary>
            /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>07-06-201706:18 PM</TimeStamp>
            Temperature,
            /// <summary>
            /// The humidity
            /// </summary>
            /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>21-06-201702:48 PM</TimeStamp>
            Humidity,

            /// <summary>
            /// The LightFixture And Beacons
            /// </summary>
            /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>08-06-201712:46 PM</TimeStamp>
            Others
        }

        /// <summary>
        /// The pubsub server URL
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>3/15/201811:53 AM</TimeStamp>
        public const string PUBSUB_SERVER_URL = "http://{0}:7003";


        /// <summary>
        /// Sensor Unit
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>06-06-201705:26 PM</TimeStamp>
        public enum SensorUnit
        {
            /// <summary>
            /// The no units
            /// </summary>
            /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>07-06-201706:18 PM</TimeStamp>
            NOUnits,
            /// <summary>
            /// The PPM
            /// </summary>
            /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>07-06-201706:18 PM</TimeStamp>
            PPM,
            /// <summary>
            /// The lux
            /// </summary>
            /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>07-06-201706:18 PM</TimeStamp>
            Lux,
            /// <summary>
            /// The kelvin
            /// </summary>
            /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>07-06-201706:18 PM</TimeStamp>
            Kelvin,
            /// <summary>
            /// The watts
            /// </summary>
            /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>07-06-201706:19 PM</TimeStamp>
            Watts,
            /// <summary>
            /// The celsius
            /// </summary>
            /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>07-06-201706:19 PM</TimeStamp>
            Celsius,
            /// <summary>
            /// The Percent
            /// </summary>
            /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>08-06-201711:59 AM</TimeStamp>
            Percent
        }

        /// <summary>
        /// Enum defined for PositiveIntegerValue
        /// </summary>
        /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>20-06-201701:48 PM</TimeStamp>
        public enum PositiveIntegerValueType
        {
            /// <summary>
            /// The DHTarget lux for Ambient Light Sensor
            /// </summary>
            /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>21-06-201712:12 PM</TimeStamp>
            DHTargetLux,

            /// <summary>
            /// The present timeout for Presence Sensor
            /// </summary>
            /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>21-06-201712:14 PM</TimeStamp>
            OccupancyTimeOut,

            /// <summary>
            /// The present Rate for Presence Sensor
            /// </summary>
            /// <CreatedBy>rupesh.saw</CreatedBy><TimeStamp>21-06-201712:15 PM</TimeStamp>
            OccupancyFadeOutTime,

            /// <summary>
            /// The red colour in palette of RGBW
            /// </summary>
            /// <CreatedBy>hp</CreatedBy>
            /// <TimeStamp>11/1/20172:19 PM</TimeStamp>
            RGB
        }


        /// <summary>
        /// This enum persists strategy to get the device IDs.
        /// </summary>
        /// <CreatedBy>Rohit Galgali</CreatedBy>
        /// <TimeStamp>6/27/20173:12 PM</TimeStamp>
        public enum RestoreIDReclaimStrategy
        {
            /// <summary>
            /// Preserve strategy, retrieves data from mapping file
            /// </summary>
            /// <CreatedBy>hp</CreatedBy>
            /// <TimeStamp>6/27/20173:13 PM</TimeStamp>
            Preserve,

            /// <summary>
            /// Incremental strategy, increment device id after every iteration.
            /// </summary>
            /// <CreatedBy>hp</CreatedBy>
            /// <TimeStamp>6/27/20173:13 PM</TimeStamp>
            Incremental
        }

        /// <summary>
        /// The API zone state argument
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>11/23/201712:07 PM</TimeStamp>
        public const string API_ZONE_STATE_ARGUMENT = "state";

        /// <summary>
        /// The project api argument
        /// </summary>
        /// <CreatedBy>prasad.joshi</CreatedBy><TimeStamp>6/21/20176:47 PM</TimeStamp>
        public const string PROJECT_ACTION = "projects?info=detail&";

        /// <summary>
        /// The API brightness
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>7/26/20175:15 PM</TimeStamp>
        public const string API_BRIGHTNESS_ARGUMENT = "brightness";

        /// <summary>
        /// The API light scene
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>7/26/20175:15 PM</TimeStamp>
        public const string API_LIGHTSCENE_ARGUMENT = "lightscene";

        /// <summary>
        /// The dh target lux
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>7/26/20175:15 PM</TimeStamp>
        public const string API_DHTARGETLUX_ARGUMENT = "dhTargetLux";

        /// <summary>
        /// The pres timeout
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>7/26/20175:15 PM</TimeStamp>
        public const string API_PRESTIMEOUT_ARGUMENT = "presTimeout";

        /// <summary>
        /// The pres rate
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>7/26/20175:15 PM</TimeStamp>
        public const string API_PRESRATE_ARGUMENT = "presRate";

        /// <summary>
        /// The ap imood argument
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>11/14/20176:12 PM</TimeStamp>
        public const string API_MOOD_ARGUMENT = "mood";

        /// <summary>
        /// The API beacon argument
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>11/14/20176:12 PM</TimeStamp>
        public const string API_PALETTE_ARGUMENT = "color";

        /// <summary>
        /// The API for system time argument
        /// </summary>
        /// <CreatedBy>prasad.joshi</CreatedBy><TimeStamp>8/28/20176:01 PM</TimeStamp>
        public const string API_SYSTEM_TIME_ARGUMENT = "system/timestamp";

        /// <summary>
        /// The API reliability argument
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>12/28/20174:06 PM</TimeStamp>
        public const string API_RELIABILITY_ARGUMENT = "project/status";


        /// <summary>
        /// The API zone space state call argument
        /// </summary>
        public const string API_SPACE_STATE_ARGUMENT = "zone/state";

        /// <summary>
        /// The available moods
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>8/8/20173:21 PM</TimeStamp>
        public static readonly string[] AVAILABLE_MOODS = new string[] { "alert", "focused", "vigilant", "calm", "relaxed", "cozy" };

        /// <summary>
        /// The available zone states
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>11/23/201712:44 PM</TimeStamp>
        public static readonly string[] AVAILABLE_ZONE_STATES = new string[] { "off", "on" };

        /// <summary>
        /// The maximum device identifier
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>8/31/20173:43 PM</TimeStamp>
        public const int MAX_DEVICE_ID = 4194302;

        /// <summary>
        /// The prestimeoutdesc
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>9/13/20178:41 PM</TimeStamp>
        public const string PRES_TIMEOUT_DESC = "occupancyTimeOut";

        /// <summary>
        /// The presratedesc
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>9/13/20178:41 PM</TimeStamp>
        public const string PRES_RATE_DESC = "occupancyFadeOut";

        /// <summary>
        /// The disabled sensor notification
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/2/20187:26 PM</TimeStamp>
        public enum NotificationType
        {
            /// <summary>
            /// The sensor disabled
            /// </summary>
            /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/31/20188:48 PM</TimeStamp>
            sensorDisabled,

            /// <summary>
            /// The sensor enabled
            /// </summary>
            sensorEnabled,

            /// <summary>
            /// The operational status
            /// </summary>
            /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/31/20188:48 PM</TimeStamp>
            operationalStatus
        }

        /// <summary>
        /// The piv dhtarget lux highlimit
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>10/5/20172:49 PM</TimeStamp>
        public const int PIV_DHTARGET_LUX_HIGH_LIMIT = 1200;

        /// <summary>
        /// The piv dhtarget lux lowlimit
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>10/5/20172:49 PM</TimeStamp>
        public const int PIV_DHTARGET_LUX_LOW_LIMIT = 0;

        /// <summary>
        /// The piv time out highlimit
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>10/5/20172:52 PM</TimeStamp>
        public const int PIV_TIME_OUT_HIGH_LIMIT = 3600;

        /// <summary>
        /// The piv time out lowlimit
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>10/5/20172:52 PM</TimeStamp>
        public const int PIV_TIME_OUT_LOW_LIMIT = 0;

        /// <summary>
        /// The piv fade out highlimit
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>10/5/20172:52 PM</TimeStamp>
        public const int PIV_FADE_OUT_HIGH_LIMIT = 3600;

        /// <summary>
        /// The piv fade out lowlimit
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>10/5/20172:52 PM</TimeStamp>
        public const int PIV_FADE_OUT_LOW_LIMIT = 0;

        /// <summary>
        /// The piv default target lux level
        /// </summary>
        /// <CreatedBy>Anish Nair</CreatedBy><TimeStamp>27/5/20172:52 PM</TimeStamp>
        public const int PIV_DEFAULT_TARGET_LUX_LEVEL = 500;

        /// <summary>
        /// The piv default occupancy time out
        /// </summary>
        /// <CreatedBy>Anish Nair</CreatedBy><TimeStamp>27/5/20172:52 PM</TimeStamp>
        public const int PIV_DEFAULT_OCCUPANCY_TIME_OUT = 600;

        /// <summary>
        /// The piv default occupancy fade out
        /// </summary>
        /// <CreatedBy>Anish Nair</CreatedBy><TimeStamp>27/5/20172:52 PM</TimeStamp>
        public const int PIV_DEFAULT_OCCUPANCY_FADE_OUT = 900;

        /// <summary>
        /// Url use while subscription to get published data
        /// </summary>
        public const string SUBSCRIPTION_URL = "http://{0}:7003/Gateway/PostPublishData";


        /// <summary>
        /// Enum for reliability statuses get from molex API
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/30/201810:22 AM</TimeStamp>
        public enum ReliabilityStatus
        {
            /// <summary>
            /// The good
            /// </summary>
            /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/25/201810:31 PM</TimeStamp>
            Good,
            /// <summary>
            /// The bad
            /// </summary>
            /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/25/201810:31 PM</TimeStamp>
            Warning,
            /// <summary>
            /// The critical
            /// </summary>
            /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/25/201810:31 PM</TimeStamp>
            Critical,
            /// <summary>
            /// The no sensor
            /// </summary>
            /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/28/20183:33 PM</TimeStamp>
            NoSensor,
            /// <summary>
            /// The na
            /// </summary>
            /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/25/201810:31 PM</TimeStamp>
            NA
        }

        /// <summary>
        /// Zone types used to identify which zone entity model belongs to which type of zone.
        /// </summary>
        /// <CreatedBy>hp</CreatedBy>
        /// <TimeStamp>11/1/20172:31 PM</TimeStamp>
        public enum ZoneTypes
        {
            /// <summary>
            /// The user space
            /// </summary>
            /// <CreatedBy>hp</CreatedBy>
            /// <TimeStamp>11/1/20172:31 PM</TimeStamp>
            UserSpace,

            /// <summary>
            /// The beacon space
            /// </summary>
            /// <CreatedBy>hp</CreatedBy>
            /// <TimeStamp>11/1/20172:31 PM</TimeStamp>
            BeaconSpace,

            /// <summary>
            /// The Blind space
            /// </summary>
            /// <CreatedBy>Swpnil Vyas</CreatedBy>
            /// <TimeStamp>12-09-2018</TimeStamp>
            BlindSpace,

            /// <summary>
            /// The General space
            /// </summary>
            /// <CreatedBy>Swpnil Vyas</CreatedBy>
            /// <TimeStamp>26-10-2018</TimeStamp>
            GeneralSpace

        }
        /// <summary>
        /// The API subscribe argument
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>3/24/201811:13 AM</TimeStamp>
        public const string API_SUBSCRIBE_ARGUMENT = "subscribe";

        /// <summary>
        /// The API unsubscribe argument
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>3/24/201811:14 AM</TimeStamp>
        public const string API_UNSUBSCRIBE_ARGUMENT = "unsubscribe";

        /// <summary>
        /// The initial average sensor objectid
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>4/25/201812:01 PM</TimeStamp>
        public const int INITIAL_AVERAGE_SENSOR_AI_OBJECT_ID = 13001;

        /// <summary>
        /// The initial average sensor bi object identifier
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/8/20189:31 AM</TimeStamp>
        public const int INITIAL_AVERAGE_SENSOR_BI_OBJECT_ID = 13751;

        /// <summary>
        /// The API lives argument
        /// </summary>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/25/201810:30 PM</TimeStamp>
        public const string API_LIVE_ARGUMENT = "alive";

        #region Command
        /// <summary>
        /// Command value on runtime changes 
        /// </summary>
        public enum Command
        {
            IsReRegister = 128,
            IsNotReRegister
        }
        #endregion
    }
}

