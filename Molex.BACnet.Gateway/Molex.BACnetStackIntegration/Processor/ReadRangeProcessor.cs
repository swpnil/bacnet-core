﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.Processor
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.BACnetStackIntegration.Helper;
using Molex.BACnetStackIntegration.Log;
using Molex.BACnetStackIntegration.Model.Response;
using Molex.BACnetStackIntegration.Stack.Wrapper;
using Molex.BACnetStackIntegration.StackStructures;
using Molex.StackDataObjects.APIModels;
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Response;
using System;

namespace Molex.BACnetStackIntegration.Processor
{
    internal class ReadRangeProcessor : MessageProcessorBase
    {
        protected override StackIntegrationResponseResult Execute(StackIntegrationResponseResult responseResult)
        {
            try
            {
                if (responseResult.StackResponse.Response.stNPDUData.stAPDUData.pduType == BacnetPduType.PduTypeComplexAck)
                {
                    ReadRangeRes readRangeRes = StructureConverter.ByteArrayToStructure<ReadRangeRes>(responseResult.StackResponse.Response.stNPDUData.stAPDUData.serviceChoiceUnion);

                    ReadRangeResponse readRangeResponse = new ReadRangeResponse();
                    readRangeResponse.ArrayIndex = readRangeRes.arrayIndex;
                    readRangeResponse.ArrayIndexPresent = readRangeRes.arrayIndexPresent;
                    readRangeResponse.DataLen = readRangeRes.dataLen;
                    readRangeResponse.FirstSeqNo = readRangeRes.firstSeqNo;
                    readRangeResponse.ItemCount = readRangeRes.itemCount;
                    readRangeResponse.ObjectId = readRangeRes.objectId;
                    readRangeResponse.ObjectType = readRangeRes.objectType;
                    readRangeResponse.PropertyId = readRangeRes.propertyId;
                    readRangeResponse.ResultFlags = new BacnetBitStrModel();
                    readRangeResponse.ResultFlags.ByteCnt = readRangeRes.resultFlags.byteCnt;
                    readRangeResponse.ResultFlags.TransBits = readRangeRes.resultFlags.transBits;
                    readRangeResponse.ResultFlags.UnusedBits = readRangeRes.resultFlags.unusedBits;

                    BacnetDataType dataType = CommanHelper.GetPropertyDataType(readRangeRes.propertyId, readRangeRes.objectType, readRangeRes.dataType);
                    readRangeResponse.PropertyVal = DecodingHelper.DecodePropertyValue(dataType, readRangeRes.propertyId, readRangeRes.pvPropVal);

                    readRangeResponse.DataType = dataType;
                    responseResult.Response = readRangeResponse;
                    responseResult.IsSucceed = true;
                }
                else
                {
                    switch (responseResult.StackResponse.Response.stNPDUData.stAPDUData.pduType)
                    {
                        case BacnetPduType.PduTypeAbort:
                            AbortResponse abortResponse = StructureConverter.ByteArrayToStructure<AbortResponse>(responseResult.StackResponse.Response.stNPDUData.stAPDUData.serviceChoiceUnion);
                            Logger.Instance.Log(string.Format("IL Error: Abort error. Abort Reason: ", abortResponse.abortReason));

                            responseResult.IsSucceed = false;
                            responseResult.ErrorModel = new ErrorResponseModel();
                            responseResult.ErrorModel.Type = ErrorType.BACnetError;

                            switch (abortResponse.abortReason)
                            {
                                case BacnetAbortReason.AbortReasonBufferOverflow:
                                    responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortBufferOVERFLOW;
                                    break;
                                case BacnetAbortReason.AbortReasonInvalidApduInThisState:
                                    responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortInvalidApduInThisState;
                                    break;
                                case BacnetAbortReason.AbortReasonPreemtedByHigherPriorityTask:
                                    responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortPreemptedByHigherPriorityTASK;
                                    break;
                                case BacnetAbortReason.AbortReasonSegmentationNotSupported:
                                    responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortSegmentationNotSupported;
                                    break;
                                case BacnetAbortReason.AbortReasonSecurityError:
                                    responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortSecurityError;
                                    break;
                                case BacnetAbortReason.AbortReasonInSufficientSecurity:
                                    responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortInsufficientSecurity;
                                    break;
                                case BacnetAbortReason.AbortReasonWindowSizeOutOfRange:
                                    responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortWindowSizeOutOfRange;
                                    break;
                                case BacnetAbortReason.AbortReasonApplicationExceededReplyTime:
                                    responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortApplicationExceededReplyTime;
                                    break;
                                case BacnetAbortReason.AbortReasonOutOfResources:
                                    responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortOutOfResources;
                                    break;
                                case BacnetAbortReason.AbortReasonTsmTimeOut:
                                    responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortTsmTimesOut;
                                    break;
                                case BacnetAbortReason.AbortReasonApduTooLong:
                                    responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortApduTooLong;
                                    break;
                                default:
                                    responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortOther;
                                    break;
                            }
                            break;
                        case BacnetPduType.PduTypeReject:
                            RejectResponse rejectResponse = StructureConverter.ByteArrayToStructure<RejectResponse>(responseResult.StackResponse.Response.stNPDUData.stAPDUData.serviceChoiceUnion);
                            Logger.Instance.Log(string.Format("IL Error: Reject error. Reject Reason: ", rejectResponse.rejectReason));

                            responseResult.IsSucceed = false;
                            responseResult.ErrorModel = new ErrorResponseModel();
                            responseResult.ErrorModel.Type = ErrorType.BACnetError;
                            switch (rejectResponse.rejectReason)
                            {
                                case BacnetRejectReason.RejectReasonBufferOverflow:
                                    responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeRejectBufferOVERFLOW;
                                    break;
                                case BacnetRejectReason.RejectReasonInconsistentParameters:
                                    responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeRejectInconsistentParameters;
                                    break;
                                case BacnetRejectReason.RejectReasonInvalidParameterDataType:
                                    responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeRejectInvalidParameterDataType;
                                    break;
                                case BacnetRejectReason.RejectReasonInvalidTag:
                                    responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeRejectInvalidTag;
                                    break;
                                case BacnetRejectReason.RejectReasonMissingRequiredParameter:
                                    responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeRejectMissingRequiredParameter;
                                    break;
                                case BacnetRejectReason.RejectReasonParameterOutOfRange:
                                    responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeRejectParameterOutOfRange;
                                    break;
                                case BacnetRejectReason.RejectReasonTooManyArguments:
                                    responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeRejectTooManyArguments;
                                    break;
                                case BacnetRejectReason.RejectReasonUndefinedEnumeration:
                                    responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeRejectUndefinedEnumeration;
                                    break;
                                case BacnetRejectReason.RejectReasonUnrecognisedService:
                                    responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeRejectUnrecognizedService;
                                    break;
                                default:
                                    responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeRejectOther;
                                    break;
                            }
                            break;
                        default:
                            bool flag = responseResult.StackResponse.Response.stNPDUData.stAPDUData.errorFromServer;
                            Logger.Instance.Log(string.Format("IL Error: Error from server: ", flag));

                            ErrorResponse errorResponse = StructureConverter.ByteArrayToStructure<ErrorResponse>(responseResult.StackResponse.Response.stNPDUData.stAPDUData.serviceChoiceUnion);
                            Logger.Instance.Log(string.Format("IL Error: Error class: ", errorResponse.errorClass));
                            Logger.Instance.Log(string.Format("IL Error: Error code: ", errorResponse.errorCode));

                            responseResult.ErrorModel = new ErrorResponseModel();
                            responseResult.ErrorModel.Type = ErrorType.BACnetError;
                            responseResult.ErrorModel.ErrorCode = (int)errorResponse.errorCode;
                            break;
                    }

                    responseResult.IsSucceed = false;
                }
            }
            catch (Exception ex)
            {
                StackIntegrationResponseResult stackIntegrationResponseResult = new StackIntegrationResponseResult();
                stackIntegrationResponseResult.ErrorModel = new ErrorResponseModel();
                stackIntegrationResponseResult.ErrorModel.ErrorCode = ex.HResult;
                stackIntegrationResponseResult.ErrorModel.Type = ErrorType.Unknown;
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while processing ReadRange response.");
            }

            return responseResult;
        }
    }
}
