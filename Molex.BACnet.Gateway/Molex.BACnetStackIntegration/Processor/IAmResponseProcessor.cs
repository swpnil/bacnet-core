﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.Processor
/// Class: <ClassName>
/// Description: <Description>
/// Date: 2017-01-11 21:30

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.BACnetStackIntegration.Handler;
using Molex.BACnetStackIntegration.Helper;
using Molex.BACnetStackIntegration.Log;
using Molex.BACnetStackIntegration.Stack.Wrapper;
using Molex.StackDataObjects.APIModels;
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Response;
using Molex.StackDataObjects.Response.AutoResponses;
using System;

namespace Molex.BACnetStackIntegration.Processor
{
    internal static class IAmResponseProcessor
    {
        public static BacnetReturnType ProcessIAmResponse(uint devId,
                                                          uint callbackId,
                                                          BacnetServicesSupported serviceType,
                                                          IntPtr errorClass,
                                                          IntPtr errorCode,
                                                          IntPtr pduType,
                                                          IntPtr serviceData,
                                                          IntPtr serviceResp,
                                                          IntPtr otherData,
                                                          IntPtr rmtDvAddr,
                                                          IntPtr timeStamp)
        {
            IAmResponse responseResult = new IAmResponse();

            try
            {
                /* check input pointer */
                if (serviceData == IntPtr.Zero)
                {
                    responseResult.IsSucceed = false;
                    responseResult.ErrorModel = new ErrorResponseModel();
                    responseResult.ErrorModel.Type = ErrorType.BACnetError;
                    responseResult.ErrorModel.ErrorCode = (int)BacnetReturnType.BacdelError;
                    if (IAmCallbackHandler.OnMessage != null)
                        IAmCallbackHandler.OnMessage(responseResult);
                    return BacnetReturnType.BacdelError;
                }

                /* get pointer value */
                BacnetAddrBinding bacnetAddrBinding = StructureConverter.PtrToStructure<BacnetAddrBinding>(otherData);

                AddressBindingDataModel addressBindingDataModel = new AddressBindingDataModel();
                addressBindingDataModel.ObjId = bacnetAddrBinding.objId;
                addressBindingDataModel.ObjectType = bacnetAddrBinding.objectType;
                addressBindingDataModel.MaxAPDULengthAccepted = bacnetAddrBinding.maxAPDULenAccepted;
                addressBindingDataModel.SegmentationSupport = bacnetAddrBinding.segmentationSupport;
                addressBindingDataModel.BACnetDeviceAddress = new BacnetAddressModel();
                addressBindingDataModel.BACnetDeviceAddress.IPAddress = IpAddressHelper.ConvertBinaryIptoString(bacnetAddrBinding.stAddress.ipAddrs);
                addressBindingDataModel.BACnetDeviceAddress.IPAddressLength = bacnetAddrBinding.stAddress.macLen;
                addressBindingDataModel.BACnetDeviceAddress.MacAddress = IpAddressHelper.GetMacAddressStringFromByteArray(bacnetAddrBinding.stAddress.dLen, bacnetAddrBinding.stAddress.dvDadr);
                addressBindingDataModel.BACnetDeviceAddress.MacLength = bacnetAddrBinding.stAddress.dLen;
                addressBindingDataModel.BACnetDeviceAddress.NetworkNumber = bacnetAddrBinding.stAddress.net;
                addressBindingDataModel.BACnetDeviceAddress.Port = (uint)IpAddressHelper.GetPortFromBinaryIp(bacnetAddrBinding.stAddress.ipAddrs);

                responseResult.IsSucceed = true;
                responseResult.IAmNotification = addressBindingDataModel;

                if (IAmCallbackHandler.OnMessage != null)
                    IAmCallbackHandler.OnMessage(responseResult);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, "IL Error: Error occured while processing I-Am callback from stack.");

                responseResult.IsSucceed = false;
                responseResult.ErrorModel = new ErrorResponseModel();
                responseResult.ErrorModel.Type = ErrorType.BACnetError;
                responseResult.ErrorModel.ErrorCode = (int)BacnetReturnType.BacdelError;
                if (IAmCallbackHandler.OnMessage != null)
                    IAmCallbackHandler.OnMessage(responseResult);
                return BacnetReturnType.BacdelError;
            }

            return BacnetReturnType.BacdelSuccess;
        }
    }
}
