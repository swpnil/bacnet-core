﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.Manager
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.BACnetStackIntegration.Helper;
using System;
using System.Collections.Generic;
using Molex.BACnetStackIntegration.Stack.Wrapper;
using Molex.BACnetStackIntegration.StackStructures;

namespace Molex.BACnetStackIntegration.Manager
{
    internal class SyncMessageProcessor
    {
        #region Internal Methods

        internal static List<BacnetAddrBinding> ProcessAddressBiding(BacnetAddrBinding bacnetAddrBinding)
        {
            List<BacnetAddrBinding> bacnetAddrBindingList = new List<BacnetAddrBinding>();

            IntPtr nextPointer = bacnetAddrBinding.next;
            BacnetAddrBinding response = bacnetAddrBinding;
            bacnetAddrBindingList.Add(response);

            while (nextPointer != IntPtr.Zero)
            {
                response = StructureConverter.PtrToStructure<BacnetAddrBinding>(nextPointer);
                bacnetAddrBindingList.Add(response);
                nextPointer = response.next;
            }

            return bacnetAddrBindingList;
        }

        internal static List<FdtData> ProcessGetRegisteredFdListResponse(FdtData regFdDataResponse)
        {
            List<FdtData> RegisteredFdList = new List<FdtData>();

            IntPtr nextPointer = regFdDataResponse.next;
            FdtData response = regFdDataResponse;
            RegisteredFdList.Add(response);

            while (nextPointer != IntPtr.Zero)
            {
                response = StructureConverter.PtrToStructure<FdtData>(nextPointer);
                RegisteredFdList.Add(response);
                nextPointer = response.next;
            }

            return RegisteredFdList;
        }

        #endregion Internal Methods
    }
}