﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.Processor
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Response;
using Molex.BACnetStackIntegration.Helper;
using System;
using System.Runtime.InteropServices;
using Molex.BACnetStackIntegration.Log;
using Molex.BACnetStackIntegration.Manager;
using Molex.BACnetStackIntegration.Model.Response;
using Molex.BACnetStackIntegration.Stack.Wrapper;
using Molex.BACnetStackIntegration.StackStructures;

namespace Molex.BACnetStackIntegration.Processor
{
    internal class MessageProcessorBase
    {
        //protected DateTime startTime, stoptime;

        internal BacnetReturnType Process(IntPtr pstServiceArgs, byte reason)
        {
            //startTime = DateTime.Now;

            BacnetReturnType result = BacnetReturnType.BacdelSuccess;
            string reqtype = null;
            StackIntegrationResponseResult responseResult = new StackIntegrationResponseResult();

            if (pstServiceArgs == IntPtr.Zero)
            {
                responseResult.IsSucceed = false;
                responseResult.ErrorModel = new ErrorResponseModel();
                responseResult.ErrorModel.Type = ErrorType.BACnetError;
                responseResult.ErrorModel.ErrorCode = (int)BacnetReturnType.BacdelError;
                MessageReceiverManager.Instance.ProcessResponse(responseResult);
                return BacnetReturnType.BacdelError;
            }

            try
            {
                BacnetIPArguments bacnetIPResult = (BacnetIPArguments)Marshal.PtrToStructure(pstServiceArgs, typeof(BacnetIPArguments));
                responseResult.StackResponse = new StackResponse();
                responseResult.StackResponse.Response = bacnetIPResult;
                responseResult.TokenId = bacnetIPResult.tokenID;

                if (IsBacnetError(bacnetIPResult, reason, out result))
                {
                    responseResult.IsSucceed = false;
                    responseResult.ErrorModel = new ErrorResponseModel();
                    responseResult.ErrorModel.Type = ErrorType.StackError;
                    responseResult.ErrorModel.ErrorCode = (int)result;

                    MessageReceiverManager.Instance.ProcessResponse(responseResult);

                    return result;
                }

                responseResult.IsSucceed = true;

                StackIntegrationResponseResult stackIntegrationResponseResult = Execute(responseResult);
                reqtype = stackIntegrationResponseResult.ReqType.ToString();

                switch (stackIntegrationResponseResult.ProcessResponseCode)
                {
                    case (int)ProcessResponseCode.Normal: //Message process successfully
                        MessageReceiverManager.Instance.ProcessResponse(stackIntegrationResponseResult);
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured in processing response from stack.");
                result = BacnetReturnType.BacdelError;
            }

            //stoptime = DateTime.Now;
            //long ticks = (stoptime - startTime).Ticks;
            //TimeSpan tm = TimeSpan.FromTicks(ticks);
            // Logger.Instance.LogDebug("ticks " + ticks);
            //Logger.Instance.LogDebug(string.Format("IL callback processing time for command {0}: Second {1} and Milisecond {2}", reqtype, tm.Seconds, tm.Milliseconds));
            return result;
        }

        protected virtual StackIntegrationResponseResult Execute(StackIntegrationResponseResult responseResult)
        {
            return new StackIntegrationResponseResult();
        }

        private bool IsBacnetError(BacnetIPArguments bacnetIPArguments, byte reason, out BacnetReturnType result)
        {
            bool isError = false;
            result = BacnetReturnType.BacdelSuccess;

            if (reason != (byte)InitiateServiceState.StateResponseReceived)
            {
                if (reason == (byte)InitiateServiceState.StateSendRequest)
                {
                    Logger.Instance.Log(string.Format("IL Error: Retry. token Id: ", bacnetIPArguments.tokenID));
                }
                else if (reason == (byte)InitiateServiceState.StateTimeout)
                {
                    Logger.Instance.Log(string.Format("IL Error: Timeout error. token Id: ", bacnetIPArguments.tokenID));
                    Logger.Instance.Log(string.Format("IL Error: PDU type. token Id: ", bacnetIPArguments.stNPDUData.stAPDUData.pduType));

                    PduTypeError(bacnetIPArguments);
                    result = BacnetReturnType.BacdelInitiatorTimesOut;
                    isError = true;
                    return isError;
                }
                else
                {
                    Logger.Instance.Log(string.Format("IL Error: Error for token Id: ", bacnetIPArguments.tokenID));
                    Logger.Instance.Log(string.Format("IL Error: Error: ", reason));
                }

                result = BacnetReturnType.BacdelOther;
                isError = true;
            }

            return isError;
        }

        protected void PduTypeError(BacnetIPArguments bacnetIPArguments)
        {
            switch (bacnetIPArguments.stNPDUData.stAPDUData.pduType)
            {
                case BacnetPduType.PduTypeAbort:
                    AbortResponse abortResponse = StructureConverter.ByteArrayToStructure<AbortResponse>(bacnetIPArguments.stNPDUData.stAPDUData.serviceChoiceUnion);
                    Logger.Instance.Log(string.Format("IL Error: Abort error. Abort Reason: ", abortResponse.abortReason));
                    break;
                case BacnetPduType.PduTypeReject:
                    RejectResponse rejectResponse = StructureConverter.ByteArrayToStructure<RejectResponse>(bacnetIPArguments.stNPDUData.stAPDUData.serviceChoiceUnion);
                    Logger.Instance.Log(string.Format("IL Error: Reject error. Reject Reason: ", rejectResponse.rejectReason));
                    break;
                default:
                    ErrorResponse errorResponse = StructureConverter.ByteArrayToStructure<ErrorResponse>(bacnetIPArguments.stNPDUData.stAPDUData.serviceChoiceUnion);
                    Logger.Instance.Log(string.Format("IL Error: Error class: ", errorResponse.errorClass));
                    Logger.Instance.Log(string.Format("IL Error: Error code: ", errorResponse.errorCode));
                    break;
            }
        }
    }
}
