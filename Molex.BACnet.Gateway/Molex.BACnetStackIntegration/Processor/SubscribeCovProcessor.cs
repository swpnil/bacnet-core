﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.Processor
/// Class: <ClassName>
/// Description: <Description>
/// Date: 2017-01-03 16:15  

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.BACnetStackIntegration.Helper;
using Molex.BACnetStackIntegration.Log;
using Molex.BACnetStackIntegration.Model.Response;
using Molex.BACnetStackIntegration.Stack.Wrapper;
using Molex.StackDataObjects.Response;
using System;

namespace Molex.BACnetStackIntegration.Processor
{
    internal class SubscribeCovProcessor : MessageProcessorBase
    {
        protected override StackIntegrationResponseResult Execute(StackIntegrationResponseResult responseResult)
        {
            try
            {
                if (responseResult.StackResponse.Response.stNPDUData.stAPDUData.pduType == BacnetPduType.PduTypeSimpleAck)
                {
                    responseResult.IsSucceed = true;
                }
                else
                {
                    switch (responseResult.StackResponse.Response.stNPDUData.stAPDUData.pduType)
                    {
                        case BacnetPduType.PduTypeAbort:
                            AbortResponse abortResponse = StructureConverter.ByteArrayToStructure<AbortResponse>(responseResult.StackResponse.Response.stNPDUData.stAPDUData.serviceChoiceUnion);
                            Logger.Instance.Log(string.Format("IL Error: Abort error. Abort Reason: ", abortResponse.abortReason));
                            break;
                        case BacnetPduType.PduTypeReject:
                            RejectResponse rejectResponse = StructureConverter.ByteArrayToStructure<RejectResponse>(responseResult.StackResponse.Response.stNPDUData.stAPDUData.serviceChoiceUnion);
                            Logger.Instance.Log(string.Format("IL Error: Reject error. Reject Reason: ", rejectResponse.rejectReason));
                            break;
                        default:
                            bool flag = responseResult.StackResponse.Response.stNPDUData.stAPDUData.errorFromServer;
                            Logger.Instance.Log(string.Format("IL Error: Error from server: ", flag));

                            ErrorResponse errorResponse = StructureConverter.ByteArrayToStructure<ErrorResponse>(responseResult.StackResponse.Response.stNPDUData.stAPDUData.serviceChoiceUnion);
                            Logger.Instance.Log(string.Format("IL Error: Error class: ", errorResponse.errorClass));
                            Logger.Instance.Log(string.Format("IL Error: Error code: ", errorResponse.errorCode));

                            responseResult.ErrorModel = new ErrorResponseModel();
                            responseResult.ErrorModel.Type = ErrorType.BACnetError;
                            responseResult.ErrorModel.ErrorCode = (int)errorResponse.errorCode;
                            break;
                    }

                    responseResult.IsSucceed = false;
                }
            }
            catch (Exception ex)
            {
                StackIntegrationResponseResult stackIntegrationResponseResult = new StackIntegrationResponseResult();
                stackIntegrationResponseResult.ErrorModel = new ErrorResponseModel();
                stackIntegrationResponseResult.ErrorModel.ErrorCode = ex.HResult;
                stackIntegrationResponseResult.ErrorModel.Type = ErrorType.Unknown;
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while processing SubscribeCov response.");
            }

            return responseResult;
        }
    }
}
