﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.Processor
/// Class: CreateObjectNotificationProcessor
/// Description: To register callback for create object
/// Date: 2017-18-05 22:30

/// 
/// Notes: <Notes>
///-----------------------------------------------------------------
using Molex.BACnetStackIntegration.Handler;
using Molex.BACnetStackIntegration.Helper;
using Molex.BACnetStackIntegration.Log;
using Molex.BACnetStackIntegration.Stack.Wrapper;
using Molex.BACnetStackIntegration.StackStructures;
using Molex.StackDataObjects.APIModels;
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Response;
using Molex.StackDataObjects.Response.AutoResponses;
using System;
using System.Runtime.InteropServices;

namespace Molex.BACnetStackIntegration.Processor
{
    internal class CreateObjectCallbackProcessor
    {
        public static BacnetReturnType CreateObjectCallback(uint devId,
                                                           uint callbackId,
                                                           BacnetServicesSupported serviceType,
                                                           IntPtr errorClass,
                                                           IntPtr errorResponse,
                                                           IntPtr pduType,
                                                           IntPtr serviceData,
                                                           IntPtr serviceResp,
                                                           IntPtr otherData,
                                                           IntPtr rmtDvAddr,
                                                           IntPtr timeStamp
                                                           )
        {
            CreateObjectNotification responseResult = new CreateObjectNotification();

            try
            {

                /* check input pointer */
                if (serviceData == IntPtr.Zero)
                {
                    responseResult.IsSucceed = false;
                    responseResult.ErrorModel = new ErrorResponseModel();
                    responseResult.ErrorModel.Type = ErrorType.BACnetError;
                    responseResult.ErrorModel.ErrorCode = (int)BacnetReturnType.BacdelError;
                    if (AutoEventHandler.OnMessage != null)
                        AutoEventHandler.OnMessage(responseResult);
                    return BacnetReturnType.BacdelError;
                }

                /* get pointer value */
                CreateObjectReq createObjectReq = (CreateObjectReq)Marshal.PtrToStructure(serviceData, typeof(CreateObjectReq));

                CreateObjectModel createObjectModel = new CreateObjectModel();
                createObjectModel.ObjectId = createObjectReq.objectId;
                createObjectModel.DeviceId = devId;
                createObjectModel.ObjectType = createObjectReq.objectType;
                createObjectModel.IsObjTypeSpecifier = createObjectReq.isObjTypeSpecifier;

                //TODO:
                //createObjectModel.DeviceObjectID
                //createObjectModel.PropertyValues = createObjectReq.initialValues;

                responseResult.IsSucceed = true;
                responseResult.CreateObjectResponse = createObjectModel;

                if (CreateObjectCallbackHandler.OnMessage != null)
                    CreateObjectCallbackHandler.OnMessage(responseResult);

                //To pass ovverrided object id (with new object id) set by gateway while creating the dynamic object back to BACnet stack 
                createObjectReq.objectId = responseResult.CreateObjectResponse.ObjectId;
                Marshal.StructureToPtr(createObjectReq, serviceData, false);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, "IL Error: Error occured while processing create object.");

                responseResult.IsSucceed = false;
                responseResult.ErrorModel = new ErrorResponseModel();
                responseResult.ErrorModel.Type = ErrorType.BACnetError;
                responseResult.ErrorModel.ErrorCode = (int)BacnetReturnType.BacdelError;
                if (AutoEventHandler.OnMessage != null)
                    AutoEventHandler.OnMessage(responseResult);
                return BacnetReturnType.BacdelError;
            }

            if (responseResult.IsSucceed)
            {

                return BacnetReturnType.BacdelSuccess;
            }
            else
            {
                Marshal.WriteInt32(errorClass, responseResult.ErrorModel.ErrorClass);
                Marshal.WriteInt32(errorResponse, responseResult.ErrorModel.ErrorCode);
                Marshal.WriteInt32(pduType, (int)BacnetPduType.PduTypeError);
                return BacnetReturnType.BacdelError;
            }
        }
    }
}
