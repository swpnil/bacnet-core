﻿using Molex.BACnetStackIntegration.Handler;
using Molex.BACnetStackIntegration.Log;
using Molex.BACnetStackIntegration.Stack.Wrapper;
using Molex.BACnetStackIntegration.StackStructures;
using Molex.StackDataObjects.APIModels;
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Response;
using Molex.StackDataObjects.Response.AutoResponses;
using System;
using System.Runtime.InteropServices;
using System.Text;

namespace Molex.BACnetStackIntegration.Processor
{
    internal class DeviceCommunicationControlCallbackProcessor
    {
        public static BacnetReturnType ProcessDeviceCommunicationControl(uint devId,
                                                         uint callbackId,
                                                         BacnetServicesSupported serviceType,
                                                         IntPtr errorClass,
                                                         IntPtr errorCode,
                                                         IntPtr pduType,
                                                         IntPtr serviceData,
                                                         IntPtr serviceResp,
                                                         IntPtr otherData,
                                                         IntPtr rmtDvAddr,
                                                         IntPtr timeStamp)
        {

            DeviceCommunicationControl responseResult = new DeviceCommunicationControl();
            try
            {
                /* check input pointer */
                if (serviceData == IntPtr.Zero)
                {
                    responseResult.IsSucceed = false;
                    responseResult.ErrorModel = new ErrorResponseModel();
                    responseResult.ErrorModel.Type = ErrorType.BACnetError;
                    responseResult.ErrorModel.ErrorCode = (int)BacnetReturnType.BacdelError;

                    if (DeviceCommunicationControlCallbackHandler.OnMessage != null)
                        DeviceCommunicationControlCallbackHandler.OnMessage(responseResult);
                    return BacnetReturnType.BacdelError;
                }

                /* get pointer value */
                DCCRequest dccRequest = (DCCRequest)Marshal.PtrToStructure(serviceData, typeof(DCCRequest));

                DeviceCommunicationControlModel timeSyncModel = new DeviceCommunicationControlModel();
                int length = (int)(dccRequest.Password.strLen);
                byte[] unsigned = new byte[length];
                Buffer.BlockCopy(dccRequest.Password.charStr, 0, unsigned, 0, length);

                switch (dccRequest.Password.encoding)
                {
                    case (byte)BacnetCharacterStringEncoding.CharacterUCS2:
                        timeSyncModel.Password = Encoding.BigEndianUnicode.GetString(unsigned);
                        break;
                    default:
                        timeSyncModel.Password = Encoding.UTF8.GetString(unsigned);
                        break;
                }

                timeSyncModel.State = dccRequest.DccState;
                timeSyncModel.TimeDuration = dccRequest.TimeDuration;
                responseResult.WritePropertyCallbackData = timeSyncModel;
                responseResult.IsSucceed = true;

                if (DeviceCommunicationControlCallbackHandler.OnMessage != null)
                    DeviceCommunicationControlCallbackHandler.OnMessage(responseResult);


            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, "IL UTCTimeSyncCallbackProcessor Error: Error occured while processing auto event from stack.");

                responseResult.IsSucceed = false;
                responseResult.ErrorModel = new ErrorResponseModel();
                responseResult.ErrorModel.Type = ErrorType.BACnetError;
                responseResult.ErrorModel.ErrorCode = (int)BacnetReturnType.BacdelError;

                if (TimeSyncCallbackHandler.OnMessage != null)
                    TimeSyncCallbackHandler.OnMessage(responseResult);
                return BacnetReturnType.BacdelError;
            }

            if (responseResult.IsSucceed)
            {
                return BacnetReturnType.BacdelSuccess;
            }
            else
            {
                Marshal.WriteInt32(errorClass, responseResult.ErrorModel.ErrorClass);
                Marshal.WriteInt32(errorCode, responseResult.ErrorModel.ErrorCode);
                return BacnetReturnType.BacdelError;
            }

        }
    }
}
