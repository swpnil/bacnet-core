﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.Processor
/// Class: <ClassName>
/// Description: To register callback for object set property
/// Date: 2017-17-05 22:30

/// 
/// Notes: <Notes>
///
///----------------------------------------------------------------

using Molex.BACnetStackIntegration.Handler;
using Molex.BACnetStackIntegration.Helper;
using Molex.BACnetStackIntegration.Log;
using Molex.BACnetStackIntegration.Stack.Wrapper;
using Molex.BACnetStackIntegration.StackStructures;
using Molex.StackDataObjects.APIModels;
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Response;
using Molex.StackDataObjects.Response.AutoResponses;
using System;
using System.Runtime.InteropServices;
using System.Text;

namespace Molex.BACnetStackIntegration.Processor
{
    internal class WritePropertyCallbackProcessor
    {
        public static BacnetReturnType WritePropertyCallback(uint devId,
                                                           uint callbackId,
                                                           BacnetServicesSupported serviceType,
                                                           IntPtr errorClass,
                                                           IntPtr errorResponse,
                                                           IntPtr pduType,
                                                           IntPtr serviceData,
                                                           IntPtr serviceResp,
                                                           IntPtr otherData,
                                                           IntPtr rmtDvAddr,
                                                           IntPtr timeStamp
                                                           )
        {
            WritePropertyNotificationResponse responseResult = new WritePropertyNotificationResponse();

            try
            {
                /* check input pointer */
                if (serviceData == IntPtr.Zero)
                {
                    responseResult.IsSucceed = false;
                    responseResult.ErrorModel = new ErrorResponseModel();
                    responseResult.ErrorModel.Type = ErrorType.BACnetError;
                    responseResult.ErrorModel.ErrorCode = (int)BacnetReturnType.BacdelError;
                    if (AutoEventHandler.OnMessage != null)
                        AutoEventHandler.OnMessage(responseResult);
                    return BacnetReturnType.BacdelError;
                }

                /* get pointer value */
                WpRequest wpReq = (WpRequest)Marshal.PtrToStructure(serviceData, typeof(WpRequest));

                bool isCommandable = CommanHelper.IsCommandableObject(devId, wpReq.objectInstance, wpReq.objectType, wpReq.objectProperty);
                WritePropertyRequestModel writePropertyRequestModel = new WritePropertyRequestModel();

                writePropertyRequestModel.ArrayIndex = wpReq.arrayIndex;
                writePropertyRequestModel.ArrayIndexPresent = wpReq.arrayIndexPresent;
                writePropertyRequestModel.DataType = wpReq.dataType;
                writePropertyRequestModel.ObjectInstance = wpReq.objectInstance;
                writePropertyRequestModel.ObjectProperty = wpReq.objectProperty;
                writePropertyRequestModel.ObjectType = wpReq.objectType;
                writePropertyRequestModel.Priority = wpReq.priority;
                writePropertyRequestModel.DeviceId = devId;
                writePropertyRequestModel.InputValue = DecodingHelper.DecodePropertyValue(wpReq.dataType, wpReq.objectProperty, wpReq.pvPropVal);
                writePropertyRequestModel.BacnetAddress = GetBacnetAddressDataMode(rmtDvAddr);
                if (isCommandable)
                {
                    writePropertyRequestModel.PropertyValue = DecodingHelper.DecodePropertyValue(wpReq.dataType, wpReq.objectProperty, otherData);
                }
                else
                {
                    writePropertyRequestModel.PropertyValue = DecodingHelper.DecodePropertyValue(wpReq.dataType, wpReq.objectProperty, wpReq.pvPropVal);
                }


                responseResult.WritePropertyCallbackData = writePropertyRequestModel;
                responseResult.IsSucceed = true;

                if (WPCallbackHandler.OnMessage != null)
                    WPCallbackHandler.OnMessage(responseResult);

            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, "IL Error: Error occured while processing write property.");
                responseResult.IsSucceed = false;
                responseResult.ErrorModel = new ErrorResponseModel();
                responseResult.ErrorModel.Type = ErrorType.BACnetError;
                responseResult.ErrorModel.ErrorCode = (int)BacnetReturnType.BacdelError;
                if (AutoEventHandler.OnMessage != null)
                    AutoEventHandler.OnMessage(responseResult);
                return BacnetReturnType.BacdelError;
            }

            if (responseResult.IsSucceed)
            {
                return BacnetReturnType.BacdelSuccess;
            }
            else
            {
                //This check is used for schedule object. If two time values within time array is less than 60 secs
                //then GW notifies BacdelOther to Stack. Stack sets reliablity of this scehdule object to configuration error internally.
                if (responseResult.ErrorModel.ErrorCode == MolexAppConstants.BACNET_ERROR_SCHEDULE_RELIABILITYFLAG)
                {
                    return BacnetReturnType.BacdelOther;
                }
                Marshal.WriteInt32(errorClass, responseResult.ErrorModel.ErrorClass);
                Marshal.WriteInt32(errorResponse, responseResult.ErrorModel.ErrorCode);
                return BacnetReturnType.BacdelError;
            }

        }

        private static BacnetAddressModel GetBacnetAddressDataMode(IntPtr rmtDvAddr)
        {
            BacnetAddress addr = StructureConverter.PtrToStructure<BacnetAddress>(rmtDvAddr);
            return new BacnetAddressModel()
            {
                IPAddress = GetIpAddress(addr.ipAddrs),
                IPAddressLength = addr.dLen,
                MacLength = addr.macLen,
                NetworkNumber = addr.net
            };
        }

        private static string GetIpAddress(byte[] address)
        {
            StringBuilder builder = new StringBuilder();
            foreach (var bit in address)
            {
                builder.Append(bit);   
            }
            return builder.ToString();
        }
    }
}
