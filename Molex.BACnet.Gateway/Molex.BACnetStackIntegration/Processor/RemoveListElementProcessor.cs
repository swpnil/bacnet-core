﻿using Molex.BACnetStackIntegration.Helper;
using Molex.BACnetStackIntegration.Stack.Wrapper;
using Molex.BACnetStackIntegration.StackStructures;
using Molex.StackDataObjects.APIModels;
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Response.AutoResponses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnetStackIntegration.Processor
{
    internal static class RemoveListElementProcessor
    {
        public static BacnetReturnType RemoveListElementCallback(uint devId,
                                                           uint callbackId,
                                                           BacnetServicesSupported serviceType,
                                                           IntPtr errorClass,
                                                           IntPtr errorResponse,
                                                           IntPtr pduType,
                                                           IntPtr serviceData,
                                                           IntPtr serviceResp,
                                                           IntPtr otherData,
                                                           IntPtr rmtDvAddr,
                                                           IntPtr timeStamp
                                                           )
        {
            //No validation to be done in Gateway side. Returing BacdelSuccess lets stack perform the removal of
            //element in the stack object. If stack succeds in its procedure it will return another internal call back
            //with the property id. Using this second callback we need to get all the values in the list and process it.

            return BacnetReturnType.BacdelSuccess;
        }
    }
}
