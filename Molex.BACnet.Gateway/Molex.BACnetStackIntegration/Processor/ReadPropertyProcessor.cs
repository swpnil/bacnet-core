﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.Processor
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.BACnetStackIntegration.Helper;
using Molex.BACnetStackIntegration.Log;
using Molex.BACnetStackIntegration.ManagedWrapper.Helper;
using Molex.BACnetStackIntegration.Manager;
using Molex.BACnetStackIntegration.Model.Response;
using Molex.BACnetStackIntegration.Repository;
using Molex.BACnetStackIntegration.Stack.Wrapper;
using Molex.BACnetStackIntegration.StackStructures;
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Request;
using Molex.StackDataObjects.Response;
using System;

namespace Molex.BACnetStackIntegration.Processor
{
    internal class ReadPropertyProcessor : MessageProcessorBase
    {
        protected override StackIntegrationResponseResult Execute(StackIntegrationResponseResult responseResult)
        {
            try
            {
                if (responseResult.StackResponse.Response.stNPDUData.stAPDUData.pduType == BacnetPduType.PduTypeComplexAck)
                {
                    object convertedResult;
                    BacnetDataType dataType;
                    ProcessReadPropertyResponse(responseResult, out convertedResult, out dataType);

                    ReadPropertyResponse rpResponse = new ReadPropertyResponse();

                    rpResponse.PropertyValue = convertedResult;
                    rpResponse.Type = dataType.ToString();
                    responseResult.Response = rpResponse;

                    responseResult.IsSucceed = true;
                }
                else
                {
                    responseResult.Request = RequestManager.Instance.GetRequest(responseResult.TokenId);
                    ReadPropertyRequest readPropertyRequest = responseResult.Request.Request as ReadPropertyRequest;

                    switch (responseResult.StackResponse.Response.stNPDUData.stAPDUData.pduType)
                    {
                        case BacnetPduType.PduTypeAbort:
                            AbortResponse abortResponse = StructureConverter.ByteArrayToStructure<AbortResponse>(responseResult.StackResponse.Response.stNPDUData.stAPDUData.serviceChoiceUnion);
                            Logger.Instance.Log(string.Format("IL Error: Abort error. Abort Reason: ", abortResponse.abortReason));

                            if (abortResponse.abortReason == BacnetAbortReason.AbortReasonBufferOverflow || abortResponse.abortReason == BacnetAbortReason.AbortReasonSegmentationNotSupported || abortResponse.abortReason == BacnetAbortReason.AbortReasonApduTooLong)
                            {
                                if (!StackRepository.Instance.DisableFallback)
                                {
                                    if (FallbackHelper.IsArrayProperty(readPropertyRequest.RpRequest.ObjectType, readPropertyRequest.RpRequest.ObjectProperty))
                                    {
                                        if (readPropertyRequest.RpRequest.IsArrayIndexPresent == 0)
                                        {
                                            responseResult.ProcessResponseCode = 1;
                                            RPFallbackManager fallbackManager = new RPFallbackManager();
                                            fallbackManager.HandleFallback(responseResult);
                                        }
                                        else
                                        {
                                            ReadPropertyResponse response = new ReadPropertyResponse();
                                            response.Type = CommanHelper.GetPropertyDataType(readPropertyRequest.RpRequest.ObjectProperty, readPropertyRequest.RpRequest.ObjectType, BacnetDataType.BacnetDTEmpty).ToString();
                                            responseResult.Response = response;

                                            responseResult.IsSucceed = false;
                                            responseResult.ErrorModel = new ErrorResponseModel();
                                            responseResult.ErrorModel.Type = ErrorType.BACnetError;
                                            switch (abortResponse.abortReason)
                                            {
                                                case BacnetAbortReason.AbortReasonBufferOverflow:
                                                    responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortBufferOVERFLOW;
                                                    break;
                                                case BacnetAbortReason.AbortReasonInvalidApduInThisState:
                                                    responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortInvalidApduInThisState;
                                                    break;
                                                case BacnetAbortReason.AbortReasonPreemtedByHigherPriorityTask:
                                                    responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortPreemptedByHigherPriorityTASK;
                                                    break;
                                                case BacnetAbortReason.AbortReasonSegmentationNotSupported:
                                                    responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortSegmentationNotSupported;
                                                    break;
                                                case BacnetAbortReason.AbortReasonSecurityError:
                                                    responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortSecurityError;
                                                    break;
                                                case BacnetAbortReason.AbortReasonInSufficientSecurity:
                                                    responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortInsufficientSecurity;
                                                    break;
                                                case BacnetAbortReason.AbortReasonWindowSizeOutOfRange:
                                                    responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortWindowSizeOutOfRange;
                                                    break;
                                                case BacnetAbortReason.AbortReasonApplicationExceededReplyTime:
                                                    responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortApplicationExceededReplyTime;
                                                    break;
                                                case BacnetAbortReason.AbortReasonOutOfResources:
                                                    responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortOutOfResources;
                                                    break;
                                                case BacnetAbortReason.AbortReasonTsmTimeOut:
                                                    responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortTsmTimesOut;
                                                    break;
                                                case BacnetAbortReason.AbortReasonApduTooLong:
                                                    responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortApduTooLong;
                                                    break;
                                                default:
                                                    responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortOther;
                                                    break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ReadPropertyResponse response = new ReadPropertyResponse();
                                        response.Type = CommanHelper.GetPropertyDataType(readPropertyRequest.RpRequest.ObjectProperty, readPropertyRequest.RpRequest.ObjectType, BacnetDataType.BacnetDTEmpty).ToString();
                                        responseResult.Response = response;

                                        responseResult.IsSucceed = false;
                                        responseResult.ErrorModel = new ErrorResponseModel();
                                        responseResult.ErrorModel.Type = ErrorType.BACnetError;
                                        switch (abortResponse.abortReason)
                                        {
                                            case BacnetAbortReason.AbortReasonBufferOverflow:
                                                responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortBufferOVERFLOW;
                                                break;
                                            case BacnetAbortReason.AbortReasonInvalidApduInThisState:
                                                responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortInvalidApduInThisState;
                                                break;
                                            case BacnetAbortReason.AbortReasonPreemtedByHigherPriorityTask:
                                                responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortPreemptedByHigherPriorityTASK;
                                                break;
                                            case BacnetAbortReason.AbortReasonSegmentationNotSupported:
                                                responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortSegmentationNotSupported;
                                                break;
                                            case BacnetAbortReason.AbortReasonSecurityError:
                                                responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortSecurityError;
                                                break;
                                            case BacnetAbortReason.AbortReasonInSufficientSecurity:
                                                responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortInsufficientSecurity;
                                                break;
                                            case BacnetAbortReason.AbortReasonWindowSizeOutOfRange:
                                                responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortWindowSizeOutOfRange;
                                                break;
                                            case BacnetAbortReason.AbortReasonApplicationExceededReplyTime:
                                                responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortApplicationExceededReplyTime;
                                                break;
                                            case BacnetAbortReason.AbortReasonOutOfResources:
                                                responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortOutOfResources;
                                                break;
                                            case BacnetAbortReason.AbortReasonTsmTimeOut:
                                                responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortTsmTimesOut;
                                                break;
                                            case BacnetAbortReason.AbortReasonApduTooLong:
                                                responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortApduTooLong;
                                                break;
                                            default:
                                                responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortOther;
                                                break;
                                        }
                                    }
                                }
                                else
                                {
                                    ReadPropertyResponse response = new ReadPropertyResponse();
                                    response.Type = CommanHelper.GetPropertyDataType(readPropertyRequest.RpRequest.ObjectProperty, readPropertyRequest.RpRequest.ObjectType, BacnetDataType.BacnetDTEmpty).ToString();
                                    responseResult.Response = response;

                                    responseResult.IsSucceed = false;
                                    responseResult.ErrorModel = new ErrorResponseModel();
                                    responseResult.ErrorModel.Type = ErrorType.BACnetError;
                                    switch (abortResponse.abortReason)
                                    {
                                        case BacnetAbortReason.AbortReasonBufferOverflow:
                                            responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortBufferOVERFLOW;
                                            break;
                                        case BacnetAbortReason.AbortReasonInvalidApduInThisState:
                                            responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortInvalidApduInThisState;
                                            break;
                                        case BacnetAbortReason.AbortReasonPreemtedByHigherPriorityTask:
                                            responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortPreemptedByHigherPriorityTASK;
                                            break;
                                        case BacnetAbortReason.AbortReasonSegmentationNotSupported:
                                            responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortSegmentationNotSupported;
                                            break;
                                        case BacnetAbortReason.AbortReasonSecurityError:
                                            responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortSecurityError;
                                            break;
                                        case BacnetAbortReason.AbortReasonInSufficientSecurity:
                                            responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortInsufficientSecurity;
                                            break;
                                        case BacnetAbortReason.AbortReasonWindowSizeOutOfRange:
                                            responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortWindowSizeOutOfRange;
                                            break;
                                        case BacnetAbortReason.AbortReasonApplicationExceededReplyTime:
                                            responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortApplicationExceededReplyTime;
                                            break;
                                        case BacnetAbortReason.AbortReasonOutOfResources:
                                            responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortOutOfResources;
                                            break;
                                        case BacnetAbortReason.AbortReasonTsmTimeOut:
                                            responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortTsmTimesOut;
                                            break;
                                        case BacnetAbortReason.AbortReasonApduTooLong:
                                            responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortApduTooLong;
                                            break;
                                        default:
                                            responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortOther;
                                            break;
                                    }
                                }
                            }
                            else
                            {
                                ReadPropertyResponse response = new ReadPropertyResponse();
                                response.Type = CommanHelper.GetPropertyDataType(readPropertyRequest.RpRequest.ObjectProperty, readPropertyRequest.RpRequest.ObjectType, BacnetDataType.BacnetDTEmpty).ToString();
                                responseResult.Response = response;

                                responseResult.IsSucceed = false;
                                responseResult.ErrorModel = new ErrorResponseModel();
                                responseResult.ErrorModel.Type = ErrorType.BACnetError;
                                responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeAbortOther;
                            }
                            break;
                        case BacnetPduType.PduTypeReject:
                            RejectResponse rejectResponse = StructureConverter.ByteArrayToStructure<RejectResponse>(responseResult.StackResponse.Response.stNPDUData.stAPDUData.serviceChoiceUnion);
                            Logger.Instance.Log(string.Format("IL Error: Reject error. Reject Reason: ", rejectResponse.rejectReason));

                            if (rejectResponse.rejectReason == BacnetRejectReason.RejectReasonBufferOverflow)
                            {
                                if (!StackRepository.Instance.DisableFallback)
                                {
                                    if (FallbackHelper.IsArrayProperty(readPropertyRequest.RpRequest.ObjectType, readPropertyRequest.RpRequest.ObjectProperty))
                                    {
                                        if (readPropertyRequest.RpRequest.IsArrayIndexPresent == 0)
                                        {
                                            responseResult.ProcessResponseCode = 1;
                                            RPFallbackManager fallbackManager = new RPFallbackManager();
                                            fallbackManager.HandleFallback(responseResult);
                                        }
                                        else
                                        {
                                            ReadPropertyResponse response = new ReadPropertyResponse();
                                            response.Type = CommanHelper.GetPropertyDataType(readPropertyRequest.RpRequest.ObjectProperty, readPropertyRequest.RpRequest.ObjectType, BacnetDataType.BacnetDTEmpty).ToString();
                                            responseResult.Response = response;

                                            responseResult.IsSucceed = false;
                                            responseResult.ErrorModel = new ErrorResponseModel();
                                            responseResult.ErrorModel.Type = ErrorType.BACnetError;
                                            switch (rejectResponse.rejectReason)
                                            {
                                                case BacnetRejectReason.RejectReasonBufferOverflow:
                                                    responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeRejectBufferOVERFLOW;
                                                    break;
                                                case BacnetRejectReason.RejectReasonInconsistentParameters:
                                                    responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeRejectInconsistentParameters;
                                                    break;
                                                case BacnetRejectReason.RejectReasonInvalidParameterDataType:
                                                    responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeRejectInvalidParameterDataType;
                                                    break;
                                                case BacnetRejectReason.RejectReasonInvalidTag:
                                                    responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeRejectInvalidTag;
                                                    break;
                                                case BacnetRejectReason.RejectReasonMissingRequiredParameter:
                                                    responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeRejectMissingRequiredParameter;
                                                    break;
                                                case BacnetRejectReason.RejectReasonParameterOutOfRange:
                                                    responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeRejectParameterOutOfRange;
                                                    break;
                                                case BacnetRejectReason.RejectReasonTooManyArguments:
                                                    responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeRejectTooManyArguments;
                                                    break;
                                                case BacnetRejectReason.RejectReasonUndefinedEnumeration:
                                                    responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeRejectUndefinedEnumeration;
                                                    break;
                                                case BacnetRejectReason.RejectReasonUnrecognisedService:
                                                    responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeRejectUnrecognizedService;
                                                    break;
                                                default:
                                                    responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeRejectOther;
                                                    break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ReadPropertyResponse response = new ReadPropertyResponse();
                                        response.Type = CommanHelper.GetPropertyDataType(readPropertyRequest.RpRequest.ObjectProperty, readPropertyRequest.RpRequest.ObjectType, BacnetDataType.BacnetDTEmpty).ToString();
                                        responseResult.Response = response;

                                        responseResult.IsSucceed = false;
                                        responseResult.ErrorModel = new ErrorResponseModel();
                                        responseResult.ErrorModel.Type = ErrorType.BACnetError;
                                        switch (rejectResponse.rejectReason)
                                        {
                                            case BacnetRejectReason.RejectReasonBufferOverflow:
                                                responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeRejectBufferOVERFLOW;
                                                break;
                                            case BacnetRejectReason.RejectReasonInconsistentParameters:
                                                responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeRejectInconsistentParameters;
                                                break;
                                            case BacnetRejectReason.RejectReasonInvalidParameterDataType:
                                                responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeRejectInvalidParameterDataType;
                                                break;
                                            case BacnetRejectReason.RejectReasonInvalidTag:
                                                responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeRejectInvalidTag;
                                                break;
                                            case BacnetRejectReason.RejectReasonMissingRequiredParameter:
                                                responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeRejectMissingRequiredParameter;
                                                break;
                                            case BacnetRejectReason.RejectReasonParameterOutOfRange:
                                                responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeRejectParameterOutOfRange;
                                                break;
                                            case BacnetRejectReason.RejectReasonTooManyArguments:
                                                responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeRejectTooManyArguments;
                                                break;
                                            case BacnetRejectReason.RejectReasonUndefinedEnumeration:
                                                responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeRejectUndefinedEnumeration;
                                                break;
                                            case BacnetRejectReason.RejectReasonUnrecognisedService:
                                                responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeRejectUnrecognizedService;
                                                break;
                                            default:
                                                responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeRejectOther;
                                                break;
                                        }
                                    }
                                }
                                else
                                {
                                    ReadPropertyResponse response = new ReadPropertyResponse();
                                    response.Type = CommanHelper.GetPropertyDataType(readPropertyRequest.RpRequest.ObjectProperty, readPropertyRequest.RpRequest.ObjectType, BacnetDataType.BacnetDTEmpty).ToString();
                                    responseResult.Response = response;

                                    responseResult.IsSucceed = false;
                                    responseResult.ErrorModel = new ErrorResponseModel();
                                    responseResult.ErrorModel.Type = ErrorType.BACnetError;
                                    switch (rejectResponse.rejectReason)
                                    {
                                        case BacnetRejectReason.RejectReasonBufferOverflow:
                                            responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeRejectBufferOVERFLOW;
                                            break;
                                        case BacnetRejectReason.RejectReasonInconsistentParameters:
                                            responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeRejectInconsistentParameters;
                                            break;
                                        case BacnetRejectReason.RejectReasonInvalidParameterDataType:
                                            responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeRejectInvalidParameterDataType;
                                            break;
                                        case BacnetRejectReason.RejectReasonInvalidTag:
                                            responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeRejectInvalidTag;
                                            break;
                                        case BacnetRejectReason.RejectReasonMissingRequiredParameter:
                                            responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeRejectMissingRequiredParameter;
                                            break;
                                        case BacnetRejectReason.RejectReasonParameterOutOfRange:
                                            responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeRejectParameterOutOfRange;
                                            break;
                                        case BacnetRejectReason.RejectReasonTooManyArguments:
                                            responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeRejectTooManyArguments;
                                            break;
                                        case BacnetRejectReason.RejectReasonUndefinedEnumeration:
                                            responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeRejectUndefinedEnumeration;
                                            break;
                                        case BacnetRejectReason.RejectReasonUnrecognisedService:
                                            responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeRejectUnrecognizedService;
                                            break;
                                        default:
                                            responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeRejectOther;
                                            break;
                                    }
                                }
                            }
                            else
                            {
                                ReadPropertyResponse response = new ReadPropertyResponse();
                                response.Type = CommanHelper.GetPropertyDataType(readPropertyRequest.RpRequest.ObjectProperty, readPropertyRequest.RpRequest.ObjectType, BacnetDataType.BacnetDTEmpty).ToString();
                                responseResult.Response = response;

                                responseResult.IsSucceed = false;
                                responseResult.ErrorModel = new ErrorResponseModel();
                                responseResult.ErrorModel.Type = ErrorType.BACnetError;
                                responseResult.ErrorModel.ErrorCode = (int)BacnetErrorCode.ErrorCodeRejectOther;
                            }
                            break;
                        default:
                            bool flag = responseResult.StackResponse.Response.stNPDUData.stAPDUData.errorFromServer;
                            Logger.Instance.Log(string.Format("IL Error: Error from server: ", flag));

                            ErrorResponse errorResponse = StructureConverter.ByteArrayToStructure<ErrorResponse>(responseResult.StackResponse.Response.stNPDUData.stAPDUData.serviceChoiceUnion);
                            Logger.Instance.Log(string.Format("IL Error: Error class: ", errorResponse.errorClass));
                            Logger.Instance.Log(string.Format("IL Error: Error code: ", errorResponse.errorCode));

                            ReadPropertyResponse rpResponse = new ReadPropertyResponse();
                            rpResponse.Type = CommanHelper.GetPropertyDataType(readPropertyRequest.RpRequest.ObjectProperty, readPropertyRequest.RpRequest.ObjectType, BacnetDataType.BacnetDTEmpty).ToString();
                            responseResult.Response = rpResponse;

                            responseResult.ErrorModel = new ErrorResponseModel();
                            responseResult.ErrorModel.Type = ErrorType.BACnetError;
                            responseResult.ErrorModel.ErrorCode = (int)errorResponse.errorCode;
                            responseResult.IsSucceed = false;
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                StackIntegrationResponseResult stackIntegrationResponseResult = new StackIntegrationResponseResult();
                stackIntegrationResponseResult.ErrorModel = new ErrorResponseModel();
                stackIntegrationResponseResult.ErrorModel.ErrorCode = ex.HResult;
                stackIntegrationResponseResult.ErrorModel.Type = ErrorType.Unknown;
                responseResult = stackIntegrationResponseResult;
                responseResult.IsSucceed = false;
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured in processing ReadProperty response.");
            }

            responseResult.ReqType = StackConstants.RequestType.BACDEL_Send_RP;
            return responseResult;
        }

        private static void ProcessReadPropertyResponse(StackIntegrationResponseResult result, out object responseConverted, out BacnetDataType bacnetDataType)
        {
            responseConverted = null;
            RpResponse rpResponse = StructureConverter.ByteArrayToStructure<RpResponse>(result.StackResponse.Response.stNPDUData.stAPDUData.serviceChoiceUnion);

            BacnetDataType dataType = CommanHelper.GetPropertyDataType(rpResponse.objectProperty, rpResponse.objectType, rpResponse.dataType);
            responseConverted = DecodingHelper.DecodePropertyValue(dataType, rpResponse.objectProperty, rpResponse.pvPropVal);
            bacnetDataType = dataType;
        }
    }
}
