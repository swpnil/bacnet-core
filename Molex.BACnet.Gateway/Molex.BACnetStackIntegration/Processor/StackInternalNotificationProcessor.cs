﻿using Molex.BACnetStackIntegration.Handler;
using Molex.BACnetStackIntegration.Log;
using Molex.StackDataObjects.APIModels;
///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.Processor
/// Class: <ClassName>
/// Description: <Description>
/// Date: 2017-02-03 22:30

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Response;
using Molex.StackDataObjects.Response.AutoResponses;
using System;

namespace Molex.BACnetStackIntegration.Processor
{
    public class StackInternalNotificationProcessor
    {
        public static void StackInternalNotification(uint devId,
                                                           uint objectId,
                                                           BacnetObjectType objType,
                                                           BacnetCallbackType callbackType,
                                                           BacnetCallbackReason callbackReason,
                                                           uint dataValue,
                                                           IntPtr pvDataValue
                                                           )
        {
            StackInternalNotificationResponse responseResult = new StackInternalNotificationResponse();

            try
            {
                StackInternalNotificationModel stackInternalNotificationModel = new StackInternalNotificationModel();
                stackInternalNotificationModel.DeviceId = devId;
                stackInternalNotificationModel.ObjectId = objectId;
                stackInternalNotificationModel.ObjectType = objType;
                stackInternalNotificationModel.CallbackReason = callbackReason;
                stackInternalNotificationModel.CallbackType = callbackType;

                responseResult.IsSucceed = true;
                responseResult.StackInternalNotification = stackInternalNotificationModel;
                if (StackInternalNotificationHandler.OnMessage != null)
                    StackInternalNotificationHandler.OnMessage(responseResult);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, "IL Error: Error occured while processing auto event from stack.");

                responseResult.IsSucceed = false;
                responseResult.ErrorModel = new ErrorResponseModel();
                responseResult.ErrorModel.Type = ErrorType.BACnetError;
                responseResult.ErrorModel.ErrorCode = (int)BacnetReturnType.BacdelError;
                if (StackInternalNotificationHandler.OnMessage != null)
                    StackInternalNotificationHandler.OnMessage(responseResult);
            }
        }
    }
}
