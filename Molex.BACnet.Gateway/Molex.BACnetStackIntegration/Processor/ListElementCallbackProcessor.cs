﻿using System;
using System.Collections.Generic;
using System.Linq;
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Response;
using Molex.StackDataObjects.APIModels;
using Molex.BACnetStackIntegration.Handler;
using Molex.StackDataObjects.Response.InternalResponses;

namespace Molex.BACnetStackIntegration.Processor
{
    public static class ListElementCallbackProcessor
    {
        public static void ListElementInternalCallback(uint deviceId,
                                                       uint objectId,
                                                       BacnetObjectType objType,
                                                       BacnetCallbackType callbackType,
                                                       BacnetCallbackReason callbackReason,
                                                       uint dataValue,
                                                       IntPtr pvDataValue)
        {
            ListElementInternalResponse response = new ListElementInternalResponse();
            ListElementRequestModel requestModel = new ListElementRequestModel();

            requestModel.CallbackReason = callbackReason;
            requestModel.CallbackType = callbackType;
            requestModel.DeviceId = deviceId;
            requestModel.ObjectId = objectId;
            requestModel.ObjectType = objType;
            requestModel.PropertyType = (BacnetPropertyID)dataValue;

            response.ListElementInternalModelData = requestModel;

            if(ListElementCallBackHandler.OnMessage != null)
            {
                ListElementCallBackHandler.OnMessage(response);
            }
        }
    }
}
