﻿using Molex.BACnetStackIntegration.Handler;
using Molex.BACnetStackIntegration.Log;
using Molex.BACnetStackIntegration.Stack.Wrapper;
using Molex.BACnetStackIntegration.StackStructures;
using Molex.StackDataObjects.APIModels;
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Response;
using Molex.StackDataObjects.Response.AutoResponses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnetStackIntegration.Processor
{
    /// <summary>
    /// This class used for process utc time sync callback
    /// </summary>
    /// <CreatedBy>prasad.joshi</CreatedBy><TimeStamp>8/28/20172:28 PM</TimeStamp>
    internal class UTCTimeSyncCallbackProcessor
    {
        /// <summary>
        /// Processes the UTC time synchronize.
        /// </summary>
        /// <param name="devId">The dev identifier.</param>
        /// <param name="callbackId">The callback identifier.</param>
        /// <param name="serviceType">Type of the service.</param>
        /// <param name="errorClass">The error class.</param>
        /// <param name="errorCode">The error code.</param>
        /// <param name="pduType">Type of the pdu.</param>
        /// <param name="serviceData">The service data.</param>
        /// <param name="serviceResp">The service resp.</param>
        /// <param name="otherData">The other data.</param>
        /// <param name="rmtDvAddr">The RMT dv addr.</param>
        /// <param name="timeStamp">The time stamp.</param>
        /// <returns>BACnet return type</returns>
        /// <CreatedBy>prasad.joshi</CreatedBy><TimeStamp>8/28/20172:28 PM</TimeStamp>
        public static BacnetReturnType ProcessUTCTimeSync(uint devId,
                                                         uint callbackId,
                                                         BacnetServicesSupported serviceType,
                                                         IntPtr errorClass,
                                                         IntPtr errorCode,
                                                         IntPtr pduType,
                                                         IntPtr serviceData,
                                                         IntPtr serviceResp,
                                                         IntPtr otherData,
                                                         IntPtr rmtDvAddr,
                                                         IntPtr timeStamp)
        {

            TimeSyncNotification responseResult = new TimeSyncNotification();
            try
            {
                /* check input pointer */
                if (serviceData == IntPtr.Zero)
                {
                    responseResult.IsSucceed = false;
                    responseResult.ErrorModel = new ErrorResponseModel();
                    responseResult.ErrorModel.Type = ErrorType.BACnetError;
                    responseResult.ErrorModel.ErrorCode = (int)BacnetReturnType.BacdelError;

                    if (TimeSyncCallbackHandler.OnMessage != null)
                        TimeSyncCallbackHandler.OnMessage(responseResult);
                    return BacnetReturnType.BacdelError;
                }

                /* get pointer value */
                UTCTimeSyncRequest timeSyncRequest = (UTCTimeSyncRequest)Marshal.PtrToStructure(serviceData, typeof(UTCTimeSyncRequest));

                TimeSyncModel timeSyncModel = new TimeSyncModel();
                timeSyncModel.DateTimeSync = new DateTime(timeSyncRequest.utcDate.year, timeSyncRequest.utcDate.month, timeSyncRequest.utcDate.day
                                                , timeSyncRequest.utcTime.hour, timeSyncRequest.utcTime.min, timeSyncRequest.utcTime.sec);
                timeSyncModel.IsUTCFormat = true;

                responseResult.IsSucceed = true;
                responseResult.TimeSyncResponse = timeSyncModel;

                if (TimeSyncCallbackHandler.OnMessage != null)
                    TimeSyncCallbackHandler.OnMessage(responseResult);

            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, "IL UTCTimeSyncCallbackProcessor Error: Error occured while processing auto event from stack.");

                responseResult.IsSucceed = false;
                responseResult.ErrorModel = new ErrorResponseModel();
                responseResult.ErrorModel.Type = ErrorType.BACnetError;
                responseResult.ErrorModel.ErrorCode = (int)BacnetReturnType.BacdelError;

                if (TimeSyncCallbackHandler.OnMessage != null)
                    TimeSyncCallbackHandler.OnMessage(responseResult);
                return BacnetReturnType.BacdelError;
            }

            if (responseResult.IsSucceed)
            {
                return BacnetReturnType.BacdelSuccess;
            }
            else
            {
                Marshal.WriteInt32(errorClass, responseResult.ErrorModel.ErrorClass);
                Marshal.WriteInt32(errorCode, responseResult.ErrorModel.ErrorCode);
                return BacnetReturnType.BacdelError;
            }

        }
    }
}
