﻿using Molex.BACnetStackIntegration.Handler;
using Molex.BACnetStackIntegration.Helper;
using Molex.BACnetStackIntegration.Stack.Wrapper;
using Molex.BACnetStackIntegration.StackStructures;
using Molex.StackDataObjects.APIModels;
using Molex.StackDataObjects.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnetStackIntegration.Processor
{
    internal class AddListElementProcessor
    {
        public static BacnetReturnType AddListElementCallback(uint devId,
                                                           uint callbackId,
                                                           BacnetServicesSupported serviceType,
                                                           IntPtr errorClass,
                                                           IntPtr errorResponse,
                                                           IntPtr pduType,
                                                           IntPtr serviceData,
                                                           IntPtr serviceResp,
                                                           IntPtr otherData,
                                                           IntPtr rmtDvAddr,
                                                           IntPtr timeStamp
                                                           )
        {
            AddListElementResponse response = new AddListElementResponse();

            ListElementRequest wpReq = (ListElementRequest)Marshal.PtrToStructure(serviceData, typeof(ListElementRequest));

            AddListElementRequestModel addListElementRequestModel = new AddListElementRequestModel();

            addListElementRequestModel.DeviceId = devId;
            addListElementRequestModel.ObjectInstance = wpReq.objectInstance;
            addListElementRequestModel.ObjectProperty = wpReq.objectProperty;
            addListElementRequestModel.ObjectType = wpReq.objectType;
            addListElementRequestModel.DataType = wpReq.dataType;

            addListElementRequestModel.PropertyValue = DecodingHelper.DecodePropertyValue(wpReq.dataType, wpReq.objectProperty, wpReq.pvPropVal);

            response.AddListElementResponseData = addListElementRequestModel;

            if (AddListElementCallBackHandler.OnMessage != null)
                AddListElementCallBackHandler.OnMessage(response);

            if (response.IsSucceed)
            {
                return BacnetReturnType.BacdelSuccess;
            }
            else
            {
                return BacnetReturnType.BacdelError;
            }
        }

    }
}
