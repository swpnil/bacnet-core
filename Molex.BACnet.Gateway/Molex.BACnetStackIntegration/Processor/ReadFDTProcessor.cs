﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.Processor
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.APIModels;
using Molex.StackDataObjects.Response;
using Molex.BACnetStackIntegration.Helper;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Molex.BACnetStackIntegration.Model.Response;
using Molex.BACnetStackIntegration.Stack.Wrapper;
using Molex.BACnetStackIntegration.StackStructures;

namespace Molex.BACnetStackIntegration.Processor
{
    internal class ReadFDTProcessor : MessageProcessorBase
    {
        protected override StackIntegrationResponseResult Execute(StackIntegrationResponseResult responseResult)
        {
            if (responseResult.StackResponse.Response.bvlcFunctionType == BacnetBvlcFunction.BvlcReadForeignDeviceTable && responseResult.StackResponse.Response.bvlcResult == BacnetBvlcResult.BvlcResultSuccessfulCompletion)
            {
                ReadFdtResponse readFdtResponse = StructureConverter.ByteArrayToStructure<ReadFdtResponse>(responseResult.StackResponse.Response.stNPDUData.stAPDUData.serviceChoiceUnion);
                List<FdtData> response = ProcessReadFDTResponse(readFdtResponse);
                ReadFDTResponse fdtModel = new ReadFDTResponse();

                foreach (var item in response)
                {
                    FDTModel model = new FDTModel();
                    model.IPAddress = IpAddressHelper.ConvertBinaryIptoString(item.ipAddress);
                    model.Port = item.portNo;
                    model.TimeToLive = item.timeToLive;
                    model.TimeRemaining = item.timeRemaining;
                    fdtModel.FDTEntries.Add(model);
                }

                responseResult.Response = fdtModel;
            }
            else
            {
                bool flag = responseResult.StackResponse.Response.stNPDUData.stAPDUData.errorFromServer;
                ErrorResponse errorResponse = StructureConverter.ByteArrayToStructure<ErrorResponse>(responseResult.StackResponse.Response.stNPDUData.stAPDUData.serviceChoiceUnion);

                responseResult.ErrorModel = new ErrorResponseModel();
                responseResult.ErrorModel.Type = ErrorType.BACnetError;
                responseResult.ErrorModel.ErrorCode = (int)errorResponse.errorCode;

                responseResult.IsSucceed = false;
            }

            return responseResult;
        }

        private static List<FdtData> ProcessReadFDTResponse(ReadFdtResponse readFdtResponse)
        {
            var bacnetIpAddrList = new List<FdtData>();

            IntPtr nextPointer = readFdtResponse.fdtData;
            if (nextPointer != IntPtr.Zero)
            {
                FdtData fdt_data = (FdtData)Marshal.PtrToStructure(nextPointer, typeof(FdtData));

                bacnetIpAddrList.Add(fdt_data);
                nextPointer = fdt_data.next;

                while (nextPointer != IntPtr.Zero)
                {
                    fdt_data = (FdtData)Marshal.PtrToStructure(nextPointer, typeof(FdtData));
                    bacnetIpAddrList.Add(fdt_data);
                    nextPointer = fdt_data.next;
                }
            }

            return bacnetIpAddrList;
        }
    }
}
