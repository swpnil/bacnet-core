﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.Processor
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Response;
using Molex.BACnetStackIntegration.Helper;
using Molex.BACnetStackIntegration.Model.Response;
using Molex.BACnetStackIntegration.Stack.Wrapper;

namespace Molex.BACnetStackIntegration.Processor
{
    internal class WriteBDTProcessor : MessageProcessorBase
    {
        protected override StackIntegrationResponseResult Execute(StackIntegrationResponseResult responseResult)
        {
            if (responseResult.StackResponse.Response.bvlcResult == BacnetBvlcResult.BvlcResultSuccessfulCompletion)
            {
                responseResult.IsSucceed = true;
            }
            else
            {
                bool flag = responseResult.StackResponse.Response.stNPDUData.stAPDUData.errorFromServer;
                ErrorResponse errorResponse = StructureConverter.ByteArrayToStructure<ErrorResponse>(responseResult.StackResponse.Response.stNPDUData.stAPDUData.serviceChoiceUnion);

                responseResult.ErrorModel = new ErrorResponseModel();
                responseResult.ErrorModel.Type = ErrorType.BACnetError;
                responseResult.ErrorModel.ErrorCode = (int)errorResponse.errorCode;

                responseResult.IsSucceed = false;
            }

            return responseResult;
        }
    }
}
