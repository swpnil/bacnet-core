﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.Processor
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.APIModels;
using Molex.StackDataObjects.Response;
using Molex.BACnetStackIntegration.Helper;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Molex.BACnetStackIntegration.Model.Response;
using Molex.BACnetStackIntegration.Stack.Wrapper;
using Molex.BACnetStackIntegration.StackStructures;

namespace Molex.BACnetStackIntegration.Processor
{
    internal class ReadBDTProcessor : MessageProcessorBase
    {
        protected override StackIntegrationResponseResult Execute(StackIntegrationResponseResult responseResult)
        {
            if (responseResult.StackResponse.Response.bvlcFunctionType == BacnetBvlcFunction.BvlcReadBroadcastDistTable && responseResult.StackResponse.Response.bvlcResult == BacnetBvlcResult.BvlcResultSuccessfulCompletion)
            {
                ReadBdtResponse readBdtResponse = StructureConverter.ByteArrayToStructure<ReadBdtResponse>(responseResult.StackResponse.Response.stNPDUData.stAPDUData.serviceChoiceUnion);
                List<BacnetIPAddress> response = ProcessReadBDTResponse(readBdtResponse);
                ReadBDTResponse bdtModel = new ReadBDTResponse();

                foreach (var item in response)
                {
                    BDTModel model = new BDTModel();
                    model.IPAddress = IpAddressHelper.ConvertBinaryIptoString(item.ipAddress);
                    model.BroadcastMask = item.broadcastMask;
                    model.Port = item.portNo;
                    bdtModel.BDTEntries.Add(model);
                }

                responseResult.Response = bdtModel;
            }
            else
            {
                bool flag = responseResult.StackResponse.Response.stNPDUData.stAPDUData.errorFromServer;
                ErrorResponse errorResponse = StructureConverter.ByteArrayToStructure<ErrorResponse>(responseResult.StackResponse.Response.stNPDUData.stAPDUData.serviceChoiceUnion);

                responseResult.ErrorModel = new ErrorResponseModel();
                responseResult.ErrorModel.Type = ErrorType.BACnetError;
                responseResult.ErrorModel.ErrorCode = (int)errorResponse.errorCode;

                responseResult.IsSucceed = false;
            }

            return responseResult;
        }

        private static List<BacnetIPAddress> ProcessReadBDTResponse(ReadBdtResponse readBdtResponse)
        {
            var bacnetIpAddrList = new List<BacnetIPAddress>();

            IntPtr nextPointer = readBdtResponse.bIpAddress;

            if (nextPointer != IntPtr.Zero)
            {
                BacnetIPAddress bdt_data = (BacnetIPAddress)Marshal.PtrToStructure(nextPointer, typeof(BacnetIPAddress));

                bacnetIpAddrList.Add(bdt_data);
                nextPointer = bdt_data.next;

                while (nextPointer != IntPtr.Zero)
                {
                    bdt_data = (BacnetIPAddress)Marshal.PtrToStructure(nextPointer, typeof(BacnetIPAddress));
                    bacnetIpAddrList.Add(bdt_data);
                    nextPointer = bdt_data.next;
                }
            }

            return bacnetIpAddrList;
        }
    }
}
