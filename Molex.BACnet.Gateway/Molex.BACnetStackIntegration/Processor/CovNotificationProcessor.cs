﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.Processor
/// Class: <ClassName>
/// Description: <Description>
/// Date: 2017-01-03 19:30

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.BACnetStackIntegration.Handler;
using Molex.BACnetStackIntegration.Helper;
using Molex.BACnetStackIntegration.Log;
using Molex.BACnetStackIntegration.Stack.Wrapper;
using Molex.BACnetStackIntegration.StackStructures;
using Molex.StackDataObjects.APIModels;
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Response;
using Molex.StackDataObjects.Response.AutoResponses;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Molex.BACnetStackIntegration.Processor
{
    internal static class CovNotificationProcessor
    {
        public static BacnetReturnType ProcessCovNotification(uint devId,
                                                           uint callbackId,
                                                           BacnetServicesSupported serviceType,
                                                           IntPtr errorClass,
                                                           IntPtr errorCode,
                                                           IntPtr pduType,
                                                           IntPtr serviceData,
                                                           IntPtr serviceResp,
                                                           IntPtr otherData,
                                                           IntPtr rmtDvAddr,
                                                           IntPtr timeStamp)
        {
            CovNotificationResponse responseResult = new CovNotificationResponse();

            try
            {
                /* check input pointer */
                if (serviceData == IntPtr.Zero)
                {
                    responseResult.IsSucceed = false;
                    responseResult.ErrorModel = new ErrorResponseModel();
                    responseResult.ErrorModel.Type = ErrorType.BACnetError;
                    responseResult.ErrorModel.ErrorCode = (int)BacnetReturnType.BacdelError;
                    if (COVNotificationHandler.OnMessage != null)
                        COVNotificationHandler.OnMessage(responseResult);
                    return BacnetReturnType.BacdelError;
                }

                /* get pointer value */
                CovNotificationRequest covNotificationRequest = (CovNotificationRequest)Marshal.PtrToStructure(serviceData, typeof(CovNotificationRequest));

                CovNotificationModel covNotificationModel = new CovNotificationModel();
                covNotificationModel.SendSimpleAck = covNotificationRequest.sendSimpleAck;
                covNotificationModel.NotificationType = covNotificationRequest.notificationType;
                covNotificationModel.MonitoredObjectType = covNotificationRequest.monitoredObjectType;
                covNotificationModel.MonitoredObjectInstance = covNotificationRequest.monitoredObjectInstance;
                covNotificationModel.DeviceInstance = covNotificationRequest.deviceInstance;
                covNotificationModel.ProcessIdentifier = covNotificationRequest.processIdentifier;
                covNotificationModel.Lifetime = covNotificationRequest.lifetime;
                covNotificationModel.CovIncrement = covNotificationRequest.covIncrement;
                covNotificationModel.COVValue = SetPropertyValues(covNotificationRequest.stCOVValue);

                BacnetAddress bacnetAddress = (BacnetAddress)Marshal.PtrToStructure(rmtDvAddr, typeof(BacnetAddress));
                covNotificationModel.BacnetDeviceAddress = CommanHelper.GetBacnetAddressModel(bacnetAddress);

                responseResult.IsSucceed = true;
                responseResult.CovNotification = covNotificationModel;

                if (COVNotificationHandler.OnMessage != null)
                    COVNotificationHandler.OnMessage(responseResult);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, "IL Error: Error occured while processing auto event from stack.");

                responseResult.IsSucceed = false;
                responseResult.ErrorModel = new ErrorResponseModel();
                responseResult.ErrorModel.Type = ErrorType.BACnetError;
                responseResult.ErrorModel.ErrorCode = (int)BacnetReturnType.BacdelError;
                if (COVNotificationHandler.OnMessage != null)
                    COVNotificationHandler.OnMessage(responseResult);
                return BacnetReturnType.BacdelError;
            }

            return BacnetReturnType.BacdelSuccess;
        }

        private static List<PropertyValueModel> SetPropertyValues(PropertyValues propertyValues)
        {
            List<PropertyValueModel> PropertyValueList = new List<PropertyValueModel>();

            IntPtr nextPointer = propertyValues.nextVal;
            PropertyValueModel propertyValueModel = new PropertyValueModel();
            propertyValueModel.IsArrayIndxPresent = propertyValues.isArrayIndxPresent;
            propertyValueModel.ObjectProperty = propertyValues.objectProperty;
            propertyValueModel.PropertyArrayIndex = propertyValues.propertyArrayIndex;
            propertyValueModel.DataType = propertyValues.dataType;
            propertyValueModel.Priority = propertyValues.priority;
            propertyValueModel.PropertyValue = DecodingHelper.DecodePropertyValue(propertyValues.dataType, propertyValues.objectProperty, propertyValues.propVal);

            PropertyValueList.Add(propertyValueModel);

            while (nextPointer != IntPtr.Zero)
            {
                PropertyValues propertyVal = StructureConverter.PtrToStructure<PropertyValues>(nextPointer);

                propertyValueModel = new PropertyValueModel();
                propertyValueModel.IsArrayIndxPresent = propertyVal.isArrayIndxPresent;
                propertyValueModel.ObjectProperty = propertyVal.objectProperty;
                propertyValueModel.PropertyArrayIndex = propertyVal.propertyArrayIndex;
                propertyValueModel.DataType = propertyVal.dataType;
                propertyValueModel.Priority = propertyVal.priority;
                propertyValueModel.PropertyValue = DecodingHelper.DecodePropertyValue(propertyVal.dataType, propertyVal.objectProperty, propertyVal.propVal);

                PropertyValueList.Add(propertyValueModel);
                nextPointer = propertyVal.nextVal;
            }

            return PropertyValueList;
        }
    }
}
