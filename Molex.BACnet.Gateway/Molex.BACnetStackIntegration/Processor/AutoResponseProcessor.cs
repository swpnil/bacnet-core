///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.Processor
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------

﻿using Molex.BACnetStackIntegration.Handler;
using Molex.BACnetStackIntegration.Helper;
using Molex.BACnetStackIntegration.Log;
using Molex.BACnetStackIntegration.Stack.Wrapper;
using Molex.BACnetStackIntegration.StackStructures;
using Molex.StackDataObjects.APIModels;
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Response;
using Molex.StackDataObjects.Response.AutoResponses;
using System;
using System.Runtime.InteropServices;
using System.Text;

namespace Molex.BACnetStackIntegration.Processor
{
    internal static class AutoResponseProcessor
    {
        public static BacnetReturnType ProcessAutoResponse(uint devId,
                                                           uint callbackId,
                                                           BacnetServicesSupported serviceType,
                                                           IntPtr errorClass,
                                                           IntPtr errorResponse,
                                                           IntPtr pduType,
                                                           IntPtr serviceData,
                                                           IntPtr serviceResp,
                                                           IntPtr otherData,
                                                           IntPtr rmtDvAddr,
                                                           IntPtr timeStamp)
        {
            EventNotificationResponse responseResult = new EventNotificationResponse();

            try
            {

                /* check input pointer */
                if (serviceData == IntPtr.Zero)
                {
                    responseResult.IsSucceed = false;
                    responseResult.ErrorModel = new ErrorResponseModel();
                    responseResult.ErrorModel.Type = ErrorType.BACnetError;
                    responseResult.ErrorModel.ErrorCode = (int)BacnetReturnType.BacdelError;
                    if (AutoEventHandler.OnMessage != null)
                        AutoEventHandler.OnMessage(responseResult);
                    return BacnetReturnType.BacdelError;
                }

                /* get pointer value */
                EventNotification eventNotificationReq = (EventNotification)Marshal.PtrToStructure(serviceData, typeof(EventNotification));

                EventNotificationModel eventNotificationModel = new EventNotificationModel();
                eventNotificationModel.AckRequired = eventNotificationReq.m_u8AckRequired;
                eventNotificationModel.EventType = eventNotificationReq.m_eEventType;
                eventNotificationModel.FromState = eventNotificationReq.m_eFromState;
                eventNotificationModel.InitiatingDeviceId = eventNotificationReq.m_u32InitiatingDeviceId;
                eventNotificationModel.InitiatingObjectId = eventNotificationReq.m_u32InitiatingObjectId;
                eventNotificationModel.InitiatingObjectType = eventNotificationReq.m_eInitiatingObjectType;
                eventNotificationModel.IsConfirmedNotification = eventNotificationReq.m_u8IsConfirmedNotification;
                eventNotificationModel.NotificationClass = eventNotificationReq.m_u32NotificationClass;
                eventNotificationModel.NotifyType = eventNotificationReq.m_eNotifyType;
                eventNotificationModel.Priority = eventNotificationReq.m_u8Priority;
                eventNotificationModel.ProcessIdentifier = eventNotificationReq.m_u32ProcessIdentifier;
                eventNotificationModel.SendSimpleAck = eventNotificationReq.m_u8SendSimpleAck;
                eventNotificationModel.ToState = eventNotificationReq.m_eToState;
                eventNotificationModel.TimeStamp = new BacnetTimeStampModel();
                eventNotificationModel.TimeStamp.TimeStampType = eventNotificationReq.m_stTimeStamp.timeStampType;
                eventNotificationModel.TimeStamp.TimeStamp = new TimeStampModel();

                switch (eventNotificationReq.m_stTimeStamp.timeStampType)
                {
                    case BacnetTimeStampType.TimestampTypeTime:
                        BacnetTime bacnetTime = eventNotificationReq.m_stTimeStamp.timeStampUnion.time;
                        eventNotificationModel.TimeStamp.TimeStamp = new TimeStampModel();
                        eventNotificationModel.TimeStamp.TimeStamp.Time = new BacnetTimeModel();
                        eventNotificationModel.TimeStamp.TimeStamp.Time.Hour = eventNotificationReq.m_stTimeStamp.timeStampUnion.time.hour;
                        eventNotificationModel.TimeStamp.TimeStamp.Time.Min = eventNotificationReq.m_stTimeStamp.timeStampUnion.time.min;
                        eventNotificationModel.TimeStamp.TimeStamp.Time.Sec = eventNotificationReq.m_stTimeStamp.timeStampUnion.time.sec;
                        eventNotificationModel.TimeStamp.TimeStamp.Time.Hundredths = eventNotificationReq.m_stTimeStamp.timeStampUnion.time.hundredths;
                        break;
                    case BacnetTimeStampType.TimestampTypeDateTime:
                        BacnetDateTime bacnetDateTime = eventNotificationReq.m_stTimeStamp.timeStampUnion.dateTime;
                        eventNotificationModel.TimeStamp.TimeStamp.DateTime = new BacnetDateTimeModel();
                        eventNotificationModel.TimeStamp.TimeStamp.DateTime.Date = new BacnetDateModel();
                        eventNotificationModel.TimeStamp.TimeStamp.DateTime.Date.Day = bacnetDateTime.date.day;
                        eventNotificationModel.TimeStamp.TimeStamp.DateTime.Date.Month = bacnetDateTime.date.month;
                        eventNotificationModel.TimeStamp.TimeStamp.DateTime.Date.Year = bacnetDateTime.date.year;
                        eventNotificationModel.TimeStamp.TimeStamp.DateTime.Date.WeekDay = bacnetDateTime.date.weekDay;

                        eventNotificationModel.TimeStamp.TimeStamp.DateTime.Time = new BacnetTimeModel();
                        eventNotificationModel.TimeStamp.TimeStamp.DateTime.Time.Hour = bacnetDateTime.time.hour;
                        eventNotificationModel.TimeStamp.TimeStamp.DateTime.Time.Min = bacnetDateTime.time.min;
                        eventNotificationModel.TimeStamp.TimeStamp.DateTime.Time.Sec = bacnetDateTime.time.sec;
                        eventNotificationModel.TimeStamp.TimeStamp.DateTime.Time.Hundredths = bacnetDateTime.time.hundredths;
                        break;
                    case BacnetTimeStampType.TimestampTypeSequenceNO:
                        eventNotificationModel.TimeStamp.TimeStamp.SeqNo = eventNotificationReq.m_stTimeStamp.timeStampUnion.seqNo;
                        break;
                    default:
                        break;
                }

                BacnetCharStr bacnetCharStr = eventNotificationReq.m_CharString;

                string charString = null;

                int strLength = (int)(bacnetCharStr.strLen);
                byte[] charStringArray = new byte[strLength];
                Buffer.BlockCopy(bacnetCharStr.charStr, 0, charStringArray, 0, strLength);

                switch (bacnetCharStr.encoding)
                {
                    case (byte)BacnetCharacterStringEncoding.CharacterUCS2:
                        charString = Encoding.BigEndianUnicode.GetString(charStringArray);
                        break;
                    default:
                        charString = Encoding.UTF8.GetString(charStringArray);
                        break;
                }

                eventNotificationModel.CharStringMessage = charString;
                BacnetAddress bacnetAddress = (BacnetAddress)Marshal.PtrToStructure(rmtDvAddr, typeof(BacnetAddress));
                eventNotificationModel.BacnetDeviceAddress = CommanHelper.GetBacnetAddressModel(bacnetAddress);

                responseResult.IsSucceed = true;
                responseResult.EventNotification = eventNotificationModel;


                if (AutoEventHandler.OnMessage != null)
                    AutoEventHandler.OnMessage(responseResult);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, "IL Error: Error occured while processing auto event from stack.");

                responseResult.IsSucceed = false;
                responseResult.ErrorModel = new ErrorResponseModel();
                responseResult.ErrorModel.Type = ErrorType.BACnetError;
                responseResult.ErrorModel.ErrorCode = (int)BacnetReturnType.BacdelError;
                if (AutoEventHandler.OnMessage != null)
                    AutoEventHandler.OnMessage(responseResult);
                return BacnetReturnType.BacdelError;
            }

            return BacnetReturnType.BacdelSuccess;
        }
    }
}
