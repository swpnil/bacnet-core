﻿#region CopyRights
///---------------------------------------------------------------------------------------------
/// Copyright (c) 2017 All Rights Reserved
/// Company Name:       Molex
/// Class:              ReInitDeviceCallbackProcessor
/// Description:        Reinitailize device service callback processor
/// Author:             Prasad Joshi
/// Date:               06/20/17
#endregion

using Molex.BACnetStackIntegration.Handler;
using Molex.BACnetStackIntegration.Log;
using Molex.BACnetStackIntegration.Stack.Wrapper;
using Molex.BACnetStackIntegration.StackStructures;
using Molex.StackDataObjects.APIModels;
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnetStackIntegration.Processor
{

    /// <summary>
    /// This class used for Reinitailize device service callback processor
    /// </summary>
    internal class ReInitDeviceCallbackProcessor
    {
        /// <summary>
        /// Processes the re initialize device.
        /// </summary>
        /// <param name="devId">The device identifier.</param>
        /// <param name="callbackId">The callback identifier.</param>
        /// <param name="serviceType">Type of the service.</param>
        /// <param name="errorClass">The error class.</param>
        /// <param name="errorCode">The error code.</param>
        /// <param name="pduType">Type of the pdu.</param>
        /// <param name="serviceData">The service data.</param>
        /// <param name="serviceResp">The service response.</param>
        /// <param name="otherData">The other data.</param>
        /// <param name="rmtDvAddr">The remote device address.</param>
        /// <param name="timeStamp">The time stamp.</param>
        /// <returns></returns>
        /// <CreatedBy>prasad.joshi</CreatedBy><TimeStamp>6/20/20171:48 PM</TimeStamp>
        public static BacnetReturnType ProcessReInitDevice(uint devId,
                                                           uint callbackId,
                                                           BacnetServicesSupported serviceType,
                                                           IntPtr errorClass,
                                                           IntPtr errorCode,
                                                           IntPtr pduType,
                                                           IntPtr serviceData,
                                                           IntPtr serviceResp,
                                                           IntPtr otherData,
                                                           IntPtr rmtDvAddr,
                                                           IntPtr timeStamp)
        {

            ReInitDeviceResponse responseResult = new ReInitDeviceResponse();
            try
            {
                /* check input pointer */
                if (serviceData == IntPtr.Zero)
                {
                    responseResult.IsSucceed = false;
                    responseResult.ErrorModel = new ErrorResponseModel();
                    responseResult.ErrorModel.Type = ErrorType.BACnetError;
                    responseResult.ErrorModel.ErrorCode = (int)BacnetReturnType.BacdelError;

                    if (ReInitDeviceCallbackHandler.OnMessage != null)
                        ReInitDeviceCallbackHandler.OnMessage(responseResult);
                    return BacnetReturnType.BacdelError;
                }

                /* get pointer value */
                ReInitDeviceRequest reInitDeviceRequest = (ReInitDeviceRequest)Marshal.PtrToStructure(serviceData, typeof(ReInitDeviceRequest));

                ReInitDeviceRequestModel reInitDeviceModel = new ReInitDeviceRequestModel();
                reInitDeviceModel.ReInitState = reInitDeviceRequest.ReinitializeState;

                BacnetCharStr bacnetCharStr = reInitDeviceRequest.devicePassword;

                string charString = null;

                int strLength = (int)(bacnetCharStr.strLen);
                byte[] charStringArray = new byte[strLength];
                Buffer.BlockCopy(bacnetCharStr.charStr, 0, charStringArray, 0, strLength);

                switch (bacnetCharStr.encoding)
                {
                    case (byte)BacnetCharacterStringEncoding.CharacterUCS2:
                        charString = Encoding.BigEndianUnicode.GetString(charStringArray);
                        break;
                    default:
                        charString = Encoding.UTF8.GetString(charStringArray);
                        break;
                }

                reInitDeviceModel.DevicePassword = charString;

                responseResult.IsSucceed = true;
                responseResult.ReInitializeDevice = reInitDeviceModel;

                if (ReInitDeviceCallbackHandler.OnMessage != null)
                    ReInitDeviceCallbackHandler.OnMessage(responseResult);

            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, "IL Error: Error occured while processing auto event from stack.");

                responseResult.IsSucceed = false;
                responseResult.ErrorModel = new ErrorResponseModel();
                responseResult.ErrorModel.Type = ErrorType.BACnetError;
                responseResult.ErrorModel.ErrorCode = (int)BacnetReturnType.BacdelError;

                if (ReInitDeviceCallbackHandler.OnMessage != null)
                    ReInitDeviceCallbackHandler.OnMessage(responseResult);
                return BacnetReturnType.BacdelError;
            }

            if (responseResult.IsSucceed)
            {
                return BacnetReturnType.BacdelSuccess;
            }
            else
            {
                Marshal.WriteInt32(errorClass, responseResult.ErrorModel.ErrorClass);
                Marshal.WriteInt32(errorCode, responseResult.ErrorModel.ErrorCode);
                return BacnetReturnType.BacdelError;
            }

        }
    }
}
