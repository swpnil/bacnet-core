﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.StackManagedApi
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.BACnetStackIntegration.Log;
using Molex.BACnetStackIntegration.Manager;
using Molex.BACnetStackIntegration.Model.Request;
using Molex.BACnetStackIntegration.Repository;
using Molex.StackDataObjects.APIModels;
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Request;
using Molex.StackDataObjects.Response;
using Molex.StackDataObjects.StackInititializationModels;
using System;
using System.Collections.Generic;

namespace Molex.BACnetStackIntegration.StackManagedApi
{
    public static class StackManagedApi
    {
        #region Async API

        public static bool WhoIsAsync(uint sourceDeviceId, uint destinationDeviceId, BacnetAddressModel destinationAddress, bool destinationTypeFlag, StackDataObjects.Constants.DestinationType destinationType, bool isNetworkLayerMessage, ushort networkNo, int highLimit, int lowLimit)
        {
            try
            {
                WhoIsRequest whoIsRequest = new WhoIsRequest
                {
                    SourceDeviceId = sourceDeviceId,
                    DestinationDeviceId = destinationDeviceId,
                    DestinationAddress = destinationAddress,
                    DestinationTypeFlag = destinationTypeFlag,
                    DestinationType = destinationType,
                    IsNetworkLayerMessage = isNetworkLayerMessage,
                    NetworkNo = networkNo,
                    HighLimit = highLimit,
                    LowLimit = lowLimit
                };

                StackIntegrationRequestBase request = new StackIntegrationRequestBase();
                request.Request = whoIsRequest;

                MessageSenderManager.Instance.SendData(request);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while sending Who-Is request.");
                return false;
            }

            return true;
        }

        public static bool ReadPropertyAsync(uint sourceDeviceId, uint destinationDeviceId, BacnetAddressModel destinationAddress, bool destinationTypeFlag, ReadPropertyRequestModel rpRequest, Action<ResponseResult> callbackHandler, object tag = null)
        {
            try
            {
                ReadPropertyRequest readPropertyRequest = new ReadPropertyRequest
                {
                    SourceDeviceId = sourceDeviceId,
                    DestinationDeviceId = destinationDeviceId,
                    DestinationAddress = destinationAddress,
                    DestinationTypeFlag = destinationTypeFlag,
                    RpRequest = rpRequest,
                    ActionCompletedHandler = callbackHandler,
                    Tag = tag
                };

                StackIntegrationRequestBase request = new StackIntegrationRequestBase();
                request.Request = readPropertyRequest;

                MessageSenderManager.Instance.SendData(request);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while sending RP request.");
                return false;
            }

            return true;
        }

        public static bool ReadPropertyMultipleAsync(uint sourceDeviceId, uint destinationDeviceId, BacnetAddressModel destinationAddress, bool destinationTypeFlag, List<ReadPropertyMultipleRequestModel> rpmData, Action<ResponseResult> callbackHandler, object tag = null)
        {
            try
            {
                ReadPropertyMultipleRequest readPropertyMultipleRequest = new ReadPropertyMultipleRequest
                {
                    SourceDeviceId = sourceDeviceId,
                    DestinationDeviceId = destinationDeviceId,
                    DestinationAddress = destinationAddress,
                    DestinationTypeFlag = destinationTypeFlag,
                    RpmRequest = rpmData,
                    ActionCompletedHandler = callbackHandler,
                    Tag = tag
                };

                StackIntegrationRequestBase request = new StackIntegrationRequestBase();
                request.Request = readPropertyMultipleRequest;

                MessageSenderManager.Instance.SendData(request);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while sending RPM request.");
                return false;
            }

            return true;
        }

        public static bool WritePropertyAsync(uint sourceDeviceId, uint destinationDeviceId,
                                              BacnetAddressModel destinationAddress, bool destinationTypeFlag,
                                              WritePropertyRequestModel wpRequest, Action<ResponseResult> callbackHandler,
                                              object Tag = null)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                WritePropertyRequest writePropertyRequest = new WritePropertyRequest()
                {
                    SourceDeviceId = sourceDeviceId,
                    DestinationDeviceId = destinationDeviceId,
                    DestinationAddress = destinationAddress,
                    DestinationTypeFlag = destinationTypeFlag,
                    WpRequest = wpRequest,
                    ActionCompletedHandler = callbackHandler,
                    Tag = Tag
                };

                StackIntegrationRequestBase request = new StackIntegrationRequestBase();
                request.Request = writePropertyRequest;

                MessageSenderManager.Instance.SendData(request);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while sending WP request.");
                return false;
            }

            return true;
        }

        public static bool IAm(uint sourceDeviceId, uint destinationDeviceId, BacnetAddressModel destinationAddress, bool destinationTypeFlag, DestinationType destinationType, bool isNetworkLayerMessage)
        {
            ResponseResult result = new ResponseResult();

            try
            {
                IAmRequest iAmRequest = new IAmRequest()
                {
                    SourceDeviceId = sourceDeviceId,
                    DestinationDeviceId = destinationDeviceId,
                    DestinationAddress = destinationAddress,
                    DestinationTypeFlag = destinationTypeFlag,
                    DestinationType = destinationType,
                    IsNetworkLayerMessage = isNetworkLayerMessage
                };

                StackIntegrationRequestBase request = new StackIntegrationRequestBase();
                request.Request = iAmRequest;

                MessageSenderManager.Instance.SendData(request);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while sending I-Am request.");
                return false;
            }

            return true;
        }

        public static bool ReadBDT(ushort port, string ipAddress, Action<ResponseResult> callbackHandler)
        {
            try
            {
                ReadBDTRequest readBDTRequest = new ReadBDTRequest()
                {
                    IPAddress = ipAddress,
                    Port = port,
                    ActionCompletedHandler = callbackHandler
                };

                StackIntegrationRequestBase request = new StackIntegrationRequestBase();
                request.Request = readBDTRequest;

                MessageSenderManager.Instance.SendData(request);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while sending ReadBDT request.");
                return false;
            }

            return true;
        }

        public static bool ReadFDT(ushort port, string ipAddress, Action<ResponseResult> callbackHandler)
        {
            try
            {
                ReadFDTRequest readFDTRequest = new ReadFDTRequest()
                {
                    IPAddress = ipAddress,
                    Port = port,
                    ActionCompletedHandler = callbackHandler
                };

                StackIntegrationRequestBase request = new StackIntegrationRequestBase();
                request.Request = readFDTRequest;

                MessageSenderManager.Instance.SendData(request);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while sending ReadFDT request.");
                return false;
            }

            return true;
        }

        public static bool RegisterFD(string ipaddress, ushort port, ushort timeToLive, Action<ResponseResult> callbackHandler)
        {
            try
            {
                RegisterFDRequest registerFDRequest = new RegisterFDRequest()
                {
                    IPAddress = ipaddress,
                    Port = port,
                    TimeToLive = timeToLive,
                    ActionCompletedHandler = callbackHandler
                };

                StackIntegrationRequestBase request = new StackIntegrationRequestBase();
                request.Request = registerFDRequest;

                MessageSenderManager.Instance.SendData(request);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while sending Register FD request.");
                return false;
            }

            return true;
        }

        public static bool DeleteFDT(string destinationIPAddress, ushort destinationPort, string ipAddress, ushort port, Action<ResponseResult> callbackHandler)
        {
            try
            {
                DeleteFDEntryRequest deleteFDEntryRequest = new DeleteFDEntryRequest()
                {
                    IPAddress = ipAddress,
                    Port = port,
                    DestinationIPAddress = destinationIPAddress,
                    DestinationPort = destinationPort,
                    ActionCompletedHandler = callbackHandler
                };

                StackIntegrationRequestBase request = new StackIntegrationRequestBase();
                request.Request = deleteFDEntryRequest;

                MessageSenderManager.Instance.SendData(request);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while sending deleting FD entry request.");
                return false;
            }

            return true;
        }

        public static bool WriteBDT(String ipaddress, ushort port, List<BDTModel> bdtEntries, Action<ResponseResult> callbackHandler)
        {
            try
            {
                WriteBDTRequest writeBDTRequest = new WriteBDTRequest()
                {
                    IPAddress = ipaddress,
                    Port = port,
                    BDTEntries = bdtEntries,
                    ActionCompletedHandler = callbackHandler
                };

                StackIntegrationRequestBase request = new StackIntegrationRequestBase();
                request.Request = writeBDTRequest;

                MessageSenderManager.Instance.SendData(request);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while sending WriteBDT request.");
                return false;
            }

            return true;
        }

        public static bool CreateObject(uint sourceDeviceId, uint destinationDeviceId, BacnetAddressModel destinationAddress, bool destinationTypeFlag, DestinationType destinationType, CreateObjectModel createObjectModel, Action<ResponseResult> callbackHandler)
        {
            try
            {
                CreateObjectRequest createObjectRequest = new CreateObjectRequest()
                {
                    SourceDeviceId = sourceDeviceId,
                    DestinationDeviceId = destinationDeviceId,
                    DestinationTypeFlag = destinationTypeFlag,
                    DestinationAddress = destinationAddress,
                    CreateObjectData = createObjectModel,
                    ActionCompletedHandler = callbackHandler
                };

                StackIntegrationRequestBase request = new StackIntegrationRequestBase();
                request.Request = createObjectRequest;

                MessageSenderManager.Instance.SendData(request);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while sending CreateObject request.");
                return false;
            }

            return true;
        }

        public static bool DeleteObject(uint sourceDeviceId, uint destinationDeviceId, BacnetAddressModel destinationAddress, bool destinationTypeFlag, DestinationType destinationType, DeleteObjectModel deleteObjectModel, Action<ResponseResult> callbackHandler)
        {
            try
            {
                DeleteObjectRequest deleteObjectRequest = new DeleteObjectRequest()
                {
                    SourceDeviceId = sourceDeviceId,
                    DestinationDeviceId = destinationDeviceId,
                    DestinationTypeFlag = destinationTypeFlag,
                    DestinationAddress = destinationAddress,
                    DeleteObjectData = deleteObjectModel,
                    ActionCompletedHandler = callbackHandler
                };

                StackIntegrationRequestBase request = new StackIntegrationRequestBase();
                request.Request = deleteObjectRequest;

                MessageSenderManager.Instance.SendData(request);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while sending DeleteObject request.");
                return false;
            }

            return true;
        }

        public static bool ReadRange(uint sourceDeviceId, uint destinationDeviceId, BacnetAddressModel destinationAddress, bool destinationTypeFlag, ReadRangeRequestModel readRangeRequestModel, Action<ResponseResult> callbackHandler)
        {
            try
            {
                ReadRangeRequest readRangeRequest = new ReadRangeRequest()
                {
                    SourceDeviceId = sourceDeviceId,
                    DestinationDeviceId = destinationDeviceId,
                    DestinationTypeFlag = destinationTypeFlag,
                    DestinationAddress = destinationAddress,
                    ReadRangeRequestData = readRangeRequestModel,
                    ActionCompletedHandler = callbackHandler
                };

                StackIntegrationRequestBase request = new StackIntegrationRequestBase();
                request.Request = readRangeRequest;

                MessageSenderManager.Instance.SendData(request);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while sending DeleteObject request.");
                return false;
            }

            return true;
        }

        public static bool SubscribeCov(uint sourceDeviceId, uint destinationDeviceId, BacnetAddressModel destinationAddress, bool destinationTypeFlag, SubscribeCovRequestModel subscribeCovRequestModel, Action<ResponseResult> callbackHandler, object tag = null)
        {
            try
            {
                SubscribeCovRequest subscribeCovRequest = new SubscribeCovRequest()
                {
                    SourceDeviceId = sourceDeviceId,
                    DestinationDeviceId = destinationDeviceId,
                    DestinationTypeFlag = destinationTypeFlag,
                    DestinationAddress = destinationAddress,
                    SubscribeCovReq = subscribeCovRequestModel,
                    ActionCompletedHandler = callbackHandler,
                    Tag = tag
                };

                StackIntegrationRequestBase request = new StackIntegrationRequestBase();
                request.Request = subscribeCovRequest;

                MessageSenderManager.Instance.SendData(request);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while sending DeleteObject request.");
                return false;
            }

            return true;
        }

        #endregion

        #region Sync API

        public static ResponseResult InitializeStackIntegration(bool disableFallback = false, bool isLogDebugEnabled = false, int logDeviceId = -1)
        {
            ResponseResult responseResult = new ResponseResult();

            try
            {
                MessageSenderManager.Instance.Init();
                MessageReceiverManager.Instance.Init();
                Logger.Instance.Init();
                StackRepository.Instance.DisableFallback = disableFallback;
                StackRepository.Instance.ReadPropertySyncTimeout = 2000;
                StackRepository.Instance.FallbackRequestDelayTime = 100;
                Logger.Instance.IsDebugEnabled = isLogDebugEnabled;
                if (logDeviceId > -1)
                    Logger.Instance.SetLogFlag(1, logDeviceId);
                responseResult.IsSucceed = true;
            }
            catch (Exception ex)
            {
                responseResult.IsSucceed = false;
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while Initializing Stack Integration.");
            }

            return responseResult;
        }

        public static ResponseResult InitializeStack(StackConfigModel stackInitConfiguration)
        {

            ResponseResult responseResult = new ResponseResult();

            try
            {
                StackInitRequest stackInitRequest = new StackInitRequest()
                {
                    StackInitConfiguration = stackInitConfiguration
                };

                StackIntegrationRequestBase request = new StackIntegrationRequestBase();
                request.Request = stackInitRequest;

                responseResult = MessageSenderManager.Instance.CallSyncApi(request);
            }
            catch (Exception ex)
            {
                responseResult.IsSucceed = false;
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while Initializing Stack.");
            }

            return responseResult;
        }

        public static ResponseResult SetVendorSpecificdata(VendorModel vendorData)
        {
            ResponseResult responseResult = new ResponseResult();
            try
            {
                SetVendorSpecificDataRequest setVendorSpecificDataRequest = new SetVendorSpecificDataRequest()
                {
                    VendorData = vendorData
                };

                StackIntegrationRequestBase request = new StackIntegrationRequestBase();
                request.Request = setVendorSpecificDataRequest;

                responseResult = MessageSenderManager.Instance.CallSyncApi(request);
            }
            catch (Exception ex)
            {
                responseResult.IsSucceed = false;
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while setting vendor data.");
            }

            return responseResult;
        }

        public static ResponseResult CreateDirectory()
        {
            ResponseResult responseResult = new ResponseResult();
            try
            {
                StackIntegrationRequestBase request = new StackIntegrationRequestBase();
                request.Request = new CreateDirectoryRequest();

                responseResult = MessageSenderManager.Instance.CallSyncApi(request);
            }
            catch (Exception ex)
            {
                responseResult.IsSucceed = false;
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while creating directory.");
            }

            return responseResult;
        }

        public static ResponseResult SetMaxLimits(BACAppMaxLimitsModel bacAppMaxLimits, BacnetMaxlimitParameters firstFailedLimit, BacnetMaxlimitParameters limitType, bool setSpecifiedLimit)
        {
            ResponseResult responseResult = new ResponseResult();
            try
            {
                SetMaxLimitsRequest setMaxLimitsRequest = new SetMaxLimitsRequest()
                {
                    BACAppMaxLimits = bacAppMaxLimits,
                    FirstFailedLimit = firstFailedLimit,
                    LimitType = limitType,
                    SetSpecifiedLimit = setSpecifiedLimit
                };

                StackIntegrationRequestBase request = new StackIntegrationRequestBase();
                request.Request = setMaxLimitsRequest;

                responseResult = MessageSenderManager.Instance.CallSyncApi(request);
            }
            catch (Exception ex)
            {
                responseResult.IsSucceed = false;
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while setting max limits.");
            }

            return responseResult;
        }

        public static ResponseResult GetBACnetAddressBinding()
        {
            ResponseResult result = new ResponseResult();
            try
            {
                StackIntegrationRequestBase request = new StackIntegrationRequestBase();
                request.Request = new GetAddressBindingRequest();

                result = MessageSenderManager.Instance.CallSyncApi(request);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while sending GetBACnetAddressBinding request.");
                result.IsSucceed = false;
            }

            return result;
        }

        public static ResponseResult StackDeInit()
        {
            ResponseResult result = new ResponseResult();
            try
            {
                StackIntegrationRequestBase request = new StackIntegrationRequestBase();
                request.Request = new StackDeInit();

                result = MessageSenderManager.Instance.CallSyncApi(request);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while sending Stack DeInit request.");
                result.IsSucceed = false;
            }

            return result;
        }

        public static ResponseResult DeleteAddressBindingList(int deviceId, BacnetAddressModel bacnetAddress)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                DeleteAddressBindingRequest deleteAddressBindingRequest = new DeleteAddressBindingRequest();
                deleteAddressBindingRequest.DeviceId = deviceId;
                deleteAddressBindingRequest.DestinationAddress = bacnetAddress;

                StackIntegrationRequestBase request = new StackIntegrationRequestBase();
                request.Request = deleteAddressBindingRequest;

                result = MessageSenderManager.Instance.CallSyncApi(request);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while sending DeleteAddressBindingList request.");
                result.IsSucceed = false;
            }

            return result;
        }

        public static ResponseResult EnableBBMD()
        {
            ResponseResult result = new ResponseResult();

            try
            {
                StackIntegrationRequestBase request = new StackIntegrationRequestBase();
                request.Request = new EnableBBMDRequest();

                result = MessageSenderManager.Instance.CallSyncApi(request);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while sending EnableBBMD request.");
                result.IsSucceed = false;
            }

            return result;
        }

        public static ResponseResult DisableBBMD()
        {
            ResponseResult result = new ResponseResult();

            try
            {
                StackIntegrationRequestBase request = new StackIntegrationRequestBase();
                request.Request = new DisableBBMDRequest();

                result = MessageSenderManager.Instance.CallSyncApi(request);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while sending DisableBBMD request.");
                result.IsSucceed = false;
            }

            return result;
        }

        public static ResponseResult ChangeDeviceIpAddressPortNo(ushort port, string IpAddress)
        {
            ResponseResult result = new ResponseResult();

            try
            {
                ChangeIpAddressPortNoRequest changeIpAddressPortNoRequest = new ChangeIpAddressPortNoRequest
                {
                    IPAddress = IpAddress,
                    Port = port
                };

                StackIntegrationRequestBase request = new StackIntegrationRequestBase();
                request.Request = changeIpAddressPortNoRequest;

                result = MessageSenderManager.Instance.CallSyncApi(request);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while sending ChangeDeviceIpAddressPortNo request.");
                result.IsSucceed = false;
            }

            return result;
        }

        public static ResponseResult SetDeviceApduRetries(uint deviceId, uint apduRetries, bool isAllDevice)
        {
            ResponseResult result = new ResponseResult();

            try
            {
                SetDeviceApduRetriesRequest setDeviceApduRetriesRequest = new SetDeviceApduRetriesRequest()
                {
                    DeviceId = deviceId,
                    ApduRetries = apduRetries,
                    IsAllDevice = isAllDevice
                };

                StackIntegrationRequestBase request = new StackIntegrationRequestBase();
                request.Request = setDeviceApduRetriesRequest;

                result = MessageSenderManager.Instance.CallSyncApi(request);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while sending SetDeviceApduRetries request.");
                result.IsSucceed = false;
            }

            return result;
        }

        public static ResponseResult SetDeviceApduTimeout(uint deviceId, uint apduTimeout, bool isAllDevice)
        {
            ResponseResult result = new ResponseResult();

            try
            {
                SetDeviceApduTimeoutRequest setDeviceApduTimeoutRequest = new SetDeviceApduTimeoutRequest()
                {
                    DeviceId = deviceId,
                    ApduTimeout = apduTimeout,
                    IsAllDevice = isAllDevice
                };

                StackIntegrationRequestBase request = new StackIntegrationRequestBase();
                request.Request = setDeviceApduTimeoutRequest;

                result = MessageSenderManager.Instance.CallSyncApi(request);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while sending SetDeviceApduTimeout request.");
                result.IsSucceed = false;
            }

            return result;
        }

        public static ResponseResult ChangeDeviceId(uint oldDeviceId, uint newDeviceId, uint newDADR)
        {
            ResponseResult result = new ResponseResult();

            try
            {
                ChangeDeviceIdRequest changeDeviceIdRequest = new ChangeDeviceIdRequest()
                {
                    OldDeviceId = oldDeviceId,
                    NewDeviceId = newDeviceId,
                    NewDADR = newDADR
                };

                StackIntegrationRequestBase request = new StackIntegrationRequestBase();
                request.Request = changeDeviceIdRequest;

                result = MessageSenderManager.Instance.CallSyncApi(request);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while sending ChangeDeviceId request.");
                result.IsSucceed = false;
            }

            return result;
        }

        public static ResponseResult ReadRegisterdFDList()
        {
            ResponseResult result = new ResponseResult();
            try
            {
                StackIntegrationRequestBase request = new StackIntegrationRequestBase();
                request.Request = new ReadRegisterFDListRequest();

                result = MessageSenderManager.Instance.CallSyncApi(request);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while sending ReadRegisterdFDList request.");
                result.IsSucceed = false;
            }

            return result;
        }

        public static ResponseResult GetBBMDStatus()
        {
            ResponseResult result = new ResponseResult();

            try
            {
                StackIntegrationRequestBase request = new StackIntegrationRequestBase();
                request.Request = new GetBBMDStatusRequest();

                result = MessageSenderManager.Instance.CallSyncApi(request);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while sending GetBBMDStatus request.");
                result.IsSucceed = false;
            }

            return result;
        }

        public static ResponseResult GetFDStatus()
        {
            ResponseResult result = new ResponseResult();

            try
            {
                StackIntegrationRequestBase request = new StackIntegrationRequestBase();
                request.Request = new GetFDStatusRequest();

                result = MessageSenderManager.Instance.CallSyncApi(request);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while sending GetFDStatus request.");
                result.IsSucceed = false;
            }

            return result;
        }

        public static ResponseResult ClearDeviceObject()
        {
            ResponseResult result = new ResponseResult();

            try
            {
                StackIntegrationRequestBase request = new StackIntegrationRequestBase();
                request.Request = new ClearDeviceObjectRequest();

                result = MessageSenderManager.Instance.CallSyncApi(request);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while sending ClearDeviceObject request.");
                result.IsSucceed = false;
            }

            return result;
        }

        public static ResponseResult DeInitializeStackIntegration()
        {
            ResponseResult result = new ResponseResult();
            try
            {
                result = StackDeInit();
                MessageSenderManager.Instance.DeInit();
                MessageReceiverManager.Instance.DeInit();
                Logger.Instance.DeInit();
                result.IsSucceed = true;
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while De-Initializing StackIntegration.");
                result.IsSucceed = false;
            }

            return result;
        }

        public static ResponseResult SetPresentvalueForExternalFunctionality(uint sourceDeviceId, BacnetValueModel bacnetValueModel, ExtPropertyDataModel pvTriggerTypeModel)
        {
            ResponseResult result = new ResponseResult();

            try
            {
                GetPresentValueFromBACAppRequest getPresentValueFromBACAppRequest = new GetPresentValueFromBACAppRequest()
                {
                    SourceDeviceId = sourceDeviceId,
                    PVTriggerModel = pvTriggerTypeModel,
                    BacnetValueModel = bacnetValueModel
                };

                StackIntegrationRequestBase request = new StackIntegrationRequestBase();
                request.Request = getPresentValueFromBACAppRequest;

                result = MessageSenderManager.Instance.CallSyncApi(request);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while sending GetPresentValueFromBACapp request.");
                result.IsSucceed = false;
            }

            return result;
        }

        public static ResponseResult AddObject(uint deviceID, BacnetObjectType objectType, uint objectID, string objectName)
        {
            ResponseResult result = new ResponseResult();

            try
            {
                AddObjectRequest addObjectRequest = new AddObjectRequest()
                {
                    SourceDeviceId = deviceID,
                    ObjectType = objectType,
                    ObjectID = objectID,
                    ObjectName = objectName
                };

                StackIntegrationRequestBase request = new StackIntegrationRequestBase();
                request.Request = addObjectRequest;

                result = MessageSenderManager.Instance.CallSyncApi(request);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while sending GetPresentValueFromBACapp request.");
                result.IsSucceed = false;
            }

            return result;
        }

        public static ResponseResult AddDevice(uint deviceId, uint deviceAdr, ushort SNet)
        {
            ResponseResult result = new ResponseResult();

            try
            {
                AddDeviceRequest addDeviceRequest = new AddDeviceRequest()
                {
                    DeviceID = deviceId,
                    DeviceAdr = deviceAdr,
                    SNet = SNet,
                };

                StackIntegrationRequestBase request = new StackIntegrationRequestBase();
                request.Request = addDeviceRequest;

                result = MessageSenderManager.Instance.CallSyncApi(request);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while addning bacnetdevice request.");
                result.IsSucceed = false;
            }

            return result;
        }

        public static ResponseResult RemoveObject(uint deviceID, BacnetObjectType objectType, uint objectID)
        {
            ResponseResult result = new ResponseResult();

            try
            {
                RemoveObjectRequest removeObjectRequest = new RemoveObjectRequest()
                {
                    SourceDeviceId = deviceID,
                    ObjectType = objectType,
                    ObjectID = objectID,
                };

                StackIntegrationRequestBase request = new StackIntegrationRequestBase();
                request.Request = removeObjectRequest;

                result = MessageSenderManager.Instance.CallSyncApi(request);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while sending GetPresentValueFromBACapp request.");
                result.IsSucceed = false;
            }

            return result;
        }

        public static ResponseResult GetObjectPropertyValue(uint deviceId, uint objectId, BacnetObjectType objectType, BacnetPropertyID objectProperty, uint arrayIndex, bool isArrayIndexPresent)
        {
            ResponseResult result = new ResponseResult();

            try
            {
                GetObjectPropertyValueRequest getObjectPropertyValueRequest = new GetObjectPropertyValueRequest()
                {
                    SourceDeviceId = deviceId,
                    ObjectID = objectId,
                    ObjectType = objectType,
                    Property = objectProperty,
                    ArrayIndex = arrayIndex,
                    IsArrayIndexPresent = isArrayIndexPresent
                };

                StackIntegrationRequestBase request = new StackIntegrationRequestBase();
                request.Request = getObjectPropertyValueRequest;

                result = MessageSenderManager.Instance.CallSyncApi(request);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while sending GetPresentValueFromBACapp request.");
                result.IsSucceed = false;
            }

            return result;
        }

        public static ResponseResult SetObjectPropertyValue(uint deviceID, uint objectID, BacnetObjectType objectType, BacnetPropertyID objectProperty, uint arrayIndex, object propertyValue, byte priority, bool isArrayIndexPresent, BacnetDataType dataType)
        {
            ResponseResult result = new ResponseResult();

            try
            {
                SetObjectPropertyValueRequest setObjectPropertyValueRequest = new SetObjectPropertyValueRequest()
                {
                    SourceDeviceId = deviceID,
                    ObjectID = objectID,
                    ObjectType = objectType,
                    ObjectProperty = objectProperty,
                    ArrayIndex = arrayIndex,
                    IsArrayIndexPresent = isArrayIndexPresent,
                    Priority = priority,
                    DataType = dataType,
                    PropertyValue = propertyValue
                };

                StackIntegrationRequestBase request = new StackIntegrationRequestBase();
                request.Request = setObjectPropertyValueRequest;

                result = MessageSenderManager.Instance.CallSyncApi(request);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while sending GetPresentValueFromBACapp request.");
                result.IsSucceed = false;
            }

            return result;
        }

        public static ResponseResult UpdateCovASubscriptionLifeTime(uint lifeTime)
        {
            ResponseResult result = new ResponseResult();

            try
            {
                UpdateCovASubscriptionLifeTimeRequest updateCovASubscriptionLifeTimeRequest = new UpdateCovASubscriptionLifeTimeRequest()
                {
                    LifeTime = lifeTime
                };

                StackIntegrationRequestBase request = new StackIntegrationRequestBase();
                request.Request = updateCovASubscriptionLifeTimeRequest;

                result = MessageSenderManager.Instance.CallSyncApi(request);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while sending GetPresentValueFromBACapp request.");
                result.IsSucceed = false;
            }

            return result;
        }

        /// <summary>
        /// Registers the call backs.
        /// </summary>
        /// <param name="callBacks">The call backs.</param>
        /// <returns></returns>
        /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>5/23/2017 6:19 PM</TimeStamp>
        public static ResponseResult RegisterCallBacks(Dictionary<BacAppCallBackFunChoice, List<CallBackModel>> callBacks)
        {
            ResponseResult result = new ResponseResult();

            try
            {
                result = MessageSenderManager.Instance.RegisterCallbacks(callBacks);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while register callbacks.");
                result.IsSucceed = false;
            }

            return result;
        }

        public static void RegisterInternalCallback()
        {
            MessageSenderManager.Instance.RegisterInternalCallBack();
        }

        public static ResponseResult SetDevicePassword(int deviceID, string password)
        {
            ResponseResult responseResult = new ResponseResult();
            try
            {
                SetDevicePasswordRequest setDevicePasswordRequest = new SetDevicePasswordRequest()
                {
                    DeviceId = deviceID,
                    BACnetPassword = password
                };

                StackIntegrationRequestBase request = new StackIntegrationRequestBase();
                request.Request = setDevicePasswordRequest;

                responseResult = MessageSenderManager.Instance.CallSyncApi(request);
            }
            catch (Exception ex)
            {
                responseResult.IsSucceed = false;
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while setting vendor data.");
            }

            return responseResult;
        }

        public static void RegisterPacketDelay(uint delayTime)
        {
            MessageSenderManager.Instance.RegisterPacketDelay(delayTime);
        }

        public static ResponseResult SetVirtualNetworkNumber(uint virtualNetworkNumber)
        {
            ResponseResult responseResult = new ResponseResult();

            try
            {
                MessageSenderManager.Instance.SetVirtualNetworkNumber(virtualNetworkNumber);
            }
            catch (Exception ex)
            {
                responseResult.IsSucceed = false;
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while setting vendor data.");
            }

            return responseResult;
        }

        public static ResponseResult SetLocalNetworkNumber(uint localNetworkNumber)
        {
            ResponseResult responseResult = new ResponseResult();

            try
            {
                MessageSenderManager.Instance.SetLocalNetworkNumber(localNetworkNumber);
            }
            catch (Exception ex)
            {
                responseResult.IsSucceed = false;
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while setting vendor data.");
            }

            return responseResult;
        }

        public static ResponseResult SetGetPropertyAccessType(uint deviceId, uint objectId, BacnetObjectType objectType, BacnetPropertyID objectProperty, PropAccessType accessType, bool isReadOrWrite)
        {
            ResponseResult responseResult = new ResponseResult();

            try
            {
                SetGetPropertyAccessType setGetPropertyAccessType = new SetGetPropertyAccessType()
                {
                    DeviceId = deviceId,
                    ObjectId = objectId,
                    ObjectType = objectType,
                    PropertyID = objectProperty,
                    AccessType = accessType,
                    ReadOrWrite = isReadOrWrite
                };

                StackIntegrationRequestBase request = new StackIntegrationRequestBase();
                request.Request = setGetPropertyAccessType;

                MessageSenderManager.Instance.CallSyncApi(request);
                responseResult.IsSucceed = true;
            }
            catch (Exception ex)
            {
                responseResult.IsSucceed = false;
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while SetGetPropertyAccessType request.");
            }

            return responseResult;
        }

        public static ResponseResult GetBDTList()
        {
            ResponseResult responseResult = new ResponseResult();

            try
            {
                responseResult = MessageSenderManager.Instance.GetBDTList();
            }
            catch (Exception ex)
            {
                responseResult.IsSucceed = false;
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while GetBDTList request.");
            }

            return responseResult;
        }

        public static ResponseResult GetFDTList()
        {
            ResponseResult responseResult = new ResponseResult();

            try
            {
                responseResult = MessageSenderManager.Instance.GetFDTList();
            }
            catch (Exception ex)
            {
                responseResult.IsSucceed = false;
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured while GetFDTList request.");
            }

            return responseResult;
        }
        #endregion
    }
}