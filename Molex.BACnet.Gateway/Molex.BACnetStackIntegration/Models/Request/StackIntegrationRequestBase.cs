﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.Model.Request
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.BACnetStackIntegration.Manager;
using Molex.StackDataObjects.Request;

namespace Molex.BACnetStackIntegration.Model.Request
{
    internal class StackIntegrationRequestBase
    {
        public RequestBase Request { get; set; }
        internal AsyncToSyncHandler SyncHandler { get; set; }
    }
}