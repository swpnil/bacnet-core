﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.Models
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using System.Net;

namespace Molex.BACnetStackIntegration.Models
{
    internal class StackBasicInformation
    {
        public IPAddress IPAddress { get; set; }
        public ushort Port { get; set; }
        public uint InstanceNumber { get; set; }
    }
}
