﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.Model.Response
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using System.Collections.Generic;
using Molex.BACnetStackIntegration.StackStructures;

namespace Molex.BACnetStackIntegration.Model.Response
{
    internal class StackResponse
    {
        public BacnetIPArguments Response;
        public List<object> ResponseStructures { get; private set; }

        public StackResponse()
        {
            ResponseStructures = new List<object>();
        }
    }
}