﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.Model.Response
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Response;
using System;
using Molex.BACnetStackIntegration.Model.Request;

namespace Molex.BACnetStackIntegration.Model.Response
{
    internal class StackIntegrationResponseResult
    {
        public bool IsSucceed { get; set; }
        public int TokenId { get; set; }
        public ResponseBase Response { get; set; }
        public int ProcessResponseCode { get; set; }
        public StackIntegrationRequestBase Request { get; set; }
        public ErrorResponseModel ErrorModel { get; set; }
        public StackResponse StackResponse { get; set; }
        public bool StopProcessing { get; set; }
        public DateTime PacketReceivedTime { get; set; }
        public StackConstants.RequestType ReqType { get; set; }
    }

    internal enum ProcessResponseCode
    {
        Normal = 0,
        RPFallback,
        RPMFallback
    }
}