﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.Repository
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.BACnetStackIntegration.Core;
using Molex.BACnetStackIntegration.Models;

namespace Molex.BACnetStackIntegration.Repository
{
    internal class StackRepository : SingletonBase<StackRepository>
    {
        private StackRepository()
        {
            StackBasicInfo = new StackBasicInformation();
        }

        internal StackBasicInformation StackBasicInfo { get; set; }
        internal bool DisableFallback { get; set; }
        internal int ReadPropertySyncTimeout { get; set; }
        internal int FallbackRequestDelayTime { get; set; }
    }
}
