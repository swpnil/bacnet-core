﻿///---------------------------------------------------------------------------------------------
/// Copyright (c) 2017 All Rights Reserved
/// Company Name:       Molex
/// Class:              Molex.BACnetStackIntegration.Log
/// Description:        
/// Author:             Prasad Joshi
/// Date:               06/20/17
///---------------------------------------------------------------------------------------------


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Molex.BACnetStackIntegration.Core;
using Molex.BACnetStackIntegration.Handler;

namespace Molex.BACnetStackIntegration.Log
{
    internal class Logger : SingletonQueueBase<Logger, LogData>
    {
        DateTime _processStartTime = DateTime.Now;
        Dictionary<int, int> _logFlagCache { get; set; }
        /// <summary>
        /// Gets or Sets the boolean value for IsDebugEnabled or not.
        /// </summary>
        public bool IsDebugEnabled { get; set; }

        internal void Log(LogData data)
        {
            base.Enque(data);
        }

        internal void Log(string message)
        {
            LogData logData = new LogData();
            logData.LogLevel = LogLevel.ERROR;
            logData.MoreInfo = message;

            this.Log(logData);
        }

        internal void Log(Exception message, string moreInfo = null)
        {
            LogData logData = new LogData();
            logData.LogLevel = LogLevel.ERROR;
            logData.LogException = message;
            logData.MoreInfo = moreInfo;

            this.Log(logData);
        }

        public void LogDebug(string message)
        {
            if (!IsDebugEnabled) return;

            LogData logData = new LogData();
            logData.LogLevel = LogLevel.DEBUG;
            logData.MoreInfo = message;

            this.Log(logData);
        }

        protected override void Process(LogData queueItem)
        {
            CreateLog(queueItem);
        }

        private void CreateLog(LogData logData)
        {
            if (logData.LogException == null && logData.MoreInfo == null) return;
            StringBuilder logTextBuilder = new StringBuilder();

            logTextBuilder.AppendFormat("----------START----------");
            logTextBuilder.AppendLine();
            logTextBuilder.AppendFormat("Log Level:{0}{1}DateTime:{2}", logData.LogLevel, Environment.NewLine, logData.LogGenerationTime);
            logTextBuilder.AppendLine();
            logTextBuilder.AppendFormat("Process Start Time: {0}", _processStartTime);
            logTextBuilder.AppendLine();

            if (logData.LogException != null)
            {
                AppendExceptionInfo(logData.LogException, logTextBuilder);
                Exception exception = logData.LogException;
                while (exception.InnerException != null)
                {
                    logTextBuilder.AppendLine("Inner Exception Information: ");
                    exception = exception.InnerException;
                    AppendExceptionInfo(exception, logTextBuilder);
                }
            }
            if (logData.MoreInfo != null)
            {
                logTextBuilder.AppendFormat("More Info: {0}", logData.MoreInfo);
                logTextBuilder.AppendLine();
            }

            logTextBuilder.AppendLine("----------END----------");
            logTextBuilder.AppendLine();

            SendLogDataMessage(logData);
            SendMessage(logTextBuilder.ToString());
        }

        private void AppendExceptionInfo(Exception exception, StringBuilder logTextBuilder)
        {
            logTextBuilder.AppendLine("Exception Information");
            logTextBuilder.AppendLine("Exception Type: " + exception.GetType());
            if (!string.IsNullOrEmpty(exception.StackTrace))
                logTextBuilder.AppendLine("Exception Source: " + exception.Source);
            if (exception.TargetSite != null)
                logTextBuilder.AppendLine("Targate Site: " + exception.TargetSite.Name);
            logTextBuilder.AppendLine("Error Message: " + exception.Message);
            if (!string.IsNullOrEmpty(exception.StackTrace))
                logTextBuilder.AppendLine("Stack Trace: " + exception.StackTrace);
        }

        private void AppendApplicationInfo(StringBuilder logTextBuilder, Assembly caller)
        {
            logTextBuilder.AppendLine("Application Information");
            string productName = string.Empty;
            Version productVersion;

            // Get the product name from the AssemblyProductAttribute.
            //   Usually defined in AssemblyInfo.cs as: [assembly: AssemblyProduct("BACtest")]
            var assemblyProductAttribute = ((AssemblyProductAttribute[])caller.GetCustomAttributes(typeof(AssemblyProductAttribute), false)).SingleOrDefault();
            if (assemblyProductAttribute != null)
                productName = assemblyProductAttribute.Product;

            // Get the product version from the assembly by using its AssemblyName.
            productVersion = new AssemblyName(caller.FullName).Version;

            logTextBuilder.AppendLine("Product Name: " + productName);
            logTextBuilder.AppendLine("Product Version: " + productVersion.ToString());
            logTextBuilder.AppendLine("Assembly Full Name: " + caller.FullName);
            logTextBuilder.AppendLine("Image Runtime Version: " + caller.ImageRuntimeVersion);
            logTextBuilder.AppendLine("Location: " + caller.Location);
        }

        private void AppendModuleInfo(Exception exception, StringBuilder logTextBuilder, Process currentProcess)
        {
            logTextBuilder.AppendLine("Module Information");
            foreach (ProcessModule module in currentProcess.Modules)
            {
                try
                {
                    logTextBuilder.AppendLine(module.FileName + " | " + module.FileVersionInfo.FileVersion + " | " + module.ModuleMemorySize);
                }
                catch (FileNotFoundException)
                {
                    logTextBuilder.AppendLine("File Not Found: " + module.ToString());
                }
                catch (Exception)
                {
                    logTextBuilder.AppendLine("Exception: " + module.ToString());
                }
            }
        }

        private void AppendSystemInfo(StringBuilder logTextBuilder)
        {
            logTextBuilder.AppendLine("System Information");
            logTextBuilder.AppendLine("OS Version: " + Environment.OSVersion.ToString());
            logTextBuilder.AppendLine("Processor Count: " + Environment.ProcessorCount.ToString());
            logTextBuilder.AppendLine("Machine Name: " + Environment.MachineName);
            logTextBuilder.AppendLine("User Name: " + Environment.UserName);
            logTextBuilder.AppendLine("Process Working Set: " + Environment.WorkingSet.ToString());
            logTextBuilder.AppendLine("Current Culture: " + CultureInfo.CurrentCulture.Name);
            logTextBuilder.AppendLine(".NET Framework Version: " + Environment.Version.ToString());
            logTextBuilder.AppendLine("Process Start Time: " + _processStartTime.ToString());

        }

        void SendLogDataMessage(LogData logData)
        {
            if (LogHandler.OnLogDataMessage != null)
            {
                bool isDebug = (logData.LogLevel == LogLevel.DEBUG) ? true : false;
                LogHandler.OnLogDataMessage(isDebug, logData.LogException, logData.MoreInfo, logData.LogGenerationTime);
            }
        }

        void SendMessage(string message)
        {
            if (LogHandler.OnMessage != null)
                LogHandler.OnMessage(message);
        }

        public void LogStartup()
        {
            Assembly caller = Assembly.GetEntryAssembly();
            StringBuilder logTextBuilder = new StringBuilder();

            logTextBuilder.AppendFormat("----------START----------");
            logTextBuilder.AppendLine();
            logTextBuilder.AppendLine("APPLICATION STARTUP");
            logTextBuilder.AppendFormat("Logger Startup Time: {0}", _processStartTime);
            logTextBuilder.AppendLine();
            logTextBuilder.AppendLine();
            AppendSystemInfo(logTextBuilder);
            logTextBuilder.AppendLine();
            AppendApplicationInfo(logTextBuilder, caller);
            logTextBuilder.AppendLine("----------END----------");
            logTextBuilder.AppendLine();
            // WriteToFile(logTextBuilder);
        }

        public void SetLogFlag(int key, int value)
        {
            if (_logFlagCache == null)
                _logFlagCache = new Dictionary<int, int>();

            if (!_logFlagCache.ContainsKey(key))
                _logFlagCache.Add(key, value);
        }

        public int GetLogFlag(int key)
        {
            if (_logFlagCache == null)
               return -1;

            if (!_logFlagCache.ContainsKey(key))
                return -1;

            return _logFlagCache[key];
        }
    }
}
