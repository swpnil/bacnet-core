﻿///---------------------------------------------------------------------------------------------
/// Copyright (c) 2017 All Rights Reserved
/// Company Name:       Molex
/// Class:              Molex.BACnetStackIntegration.Log
/// Description:        
/// Author:             Prasad Joshi
/// Date:               06/20/17
///---------------------------------------------------------------------------------------------
using System;

namespace Molex.BACnetStackIntegration.Log
{
    /// <summary>
    /// Specifies the log levels.
    /// </summary>
    internal enum LogLevel
    {
        /// <summary>
        /// ERROR log level.
        /// </summary>
        ERROR,

        /// <summary>
        /// DEBUG log level.
        /// </summary>
        DEBUG
    }

    /// <summary>
    /// Responsible for holding log data.
    /// </summary>
    internal class LogData
    {
        /// <summary>
        /// Gets or Sets the log level.
        /// </summary>
        internal LogLevel LogLevel { get; set; }

        /// <summary>
        /// Gets or Sets the exception.
        /// </summary>
        internal Exception LogException { get; set; }

        /// <summary>
        /// Gets or Sets more information about exception.
        /// </summary>
        internal string MoreInfo { get; set; }

        /// <summary>
        /// Gets or Sets the time when exception occured.
        /// </summary>
        internal DateTime LogGenerationTime { get; private set; }

        internal LogData()
        {
            LogGenerationTime = DateTime.Now;
        }
    }
}
