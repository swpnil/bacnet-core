﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.Core
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using System;
using System.Collections.Concurrent;
using System.Threading;
using Molex.BACnetStackIntegration.Log;

namespace Molex.BACnetStackIntegration.Core
{
    internal class SingletonQueueBase<T, T1> : SingletonBase<T>
        where T1 : class
        where T : class
    {

        #region Private Veriabls

        static AutoResetEvent _autoResetEvent = null;
        static volatile object _syncAutoResetLock = new object();
        ConcurrentQueue<T1> _queue = new ConcurrentQueue<T1>();
        Thread _workerThread;
        string _threadName;
        bool _isQuit = false;

        #endregion

        #region Properties

        internal string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        #endregion

        #region Internal Methods

        internal void Init()
        {
            if (_workerThread != null)
            {
                //_workerThread.Abort();
                _workerThread = null;
                _isQuit = false;
                ConstructorInit();
            }
            _workerThread.Name = _threadName;
            _workerThread.Start();
        }

        internal void DeInit()
        {
            //TODO: Handle exception
            PreDeInit();

            _isQuit = true;
            lock (_syncAutoResetLock)
            {
                _autoResetEvent.Set();
            }
        }

        internal void Quit()
        {
            this._isQuit = true;
            _autoResetEvent.Set();
        }

        #endregion

        #region Override Methods

        protected override void ConstructorInit()
        {
            _autoResetEvent = new AutoResetEvent(false);
            _workerThread = new Thread(new ThreadStart(ProcessQueueItems));
            _workerThread.Priority = ThreadPriority.Normal;
        }

        #endregion

        #region Protected Methods

        protected void Enque(T1 item)
        {
            if (_isQuit) return;

            _queue.Enqueue(item);

            lock (_syncAutoResetLock)
            {
                _autoResetEvent.Set();
            }
        }

        protected virtual void Process(T1 queueItem)
        {

        }

        protected virtual void PreDeInit()
        {

        }

        #endregion

        #region Private Methods

        void ProcessQueueItems()
        {
            while (_autoResetEvent.WaitOne())
            {
                if (_isQuit) break;

                lock (_syncAutoResetLock)
                {
                    _autoResetEvent.Reset();
                }

                while (_queue.Count > 0)
                {
                    T1 queueItem;
                    try
                    {
                        if (_queue.TryDequeue(out queueItem))
                        {
                            Process(queueItem);
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Instance.Log(ex, "SingletonQueueBase (IL): ProcessQueueItems: Exception Occurred");
                    }
                }
            }
        }

        #endregion

    }
}
