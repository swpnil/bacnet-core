﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.Core
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using System;

namespace Molex.BACnetStackIntegration.Core
{
    internal abstract class SingletonBase<T> where T : class
    {
        private static object lockingObject = new object();
        private static T singleTonObject;

        protected SingletonBase()
        {
            ConstructorInit();
        }

        protected virtual void ConstructorInit()
        {

        }

        public static T Instance
        {
            get
            {
                if (singleTonObject == null)
                {
                    lock (lockingObject)
                    {
                        if (singleTonObject == null)
                        {
                            singleTonObject = Activator.CreateInstance(typeof(T), true) as T;
                        }
                    }
                }
                return singleTonObject;
            }
        }

    }
}
