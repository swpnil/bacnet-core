﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.Helper
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.APIModels;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Molex.BACnetStackIntegration.StackStructures;

namespace Molex.BACnetStackIntegration.Helper
{
    internal class RPMStructureConversionHelper
    {
        internal RPMStructureConversionHelper()
        {
        }

        internal RpmRequest GetRPMRequestStructure(List<ReadPropertyMultipleRequestModel> RPRequestList, ref Dictionary<IntPtr, Type> structurePointers)
        {
            int rpmRequestSize = Marshal.SizeOf(typeof(RpmRequest));

            RpmRequest serviceChoiseRequest = new RpmRequest();

            if (RPRequestList.Count > 0)
            {
                RpmRequest rpmRequest = new RpmRequest();
                ReadPropertyMultipleRequestModel rpmModel = RPRequestList[0];
                rpmRequest.arrayIndex = rpmModel.ArrayIndex;
                rpmRequest.arrayIndexPresent = rpmModel.ArrayIndexPresent;
                rpmRequest.objectInstance = rpmModel.ObjectInstance;
                rpmRequest.objectProperty = rpmModel.ObjectProperty;
                rpmRequest.objectType = rpmModel.ObjectType;

                serviceChoiseRequest = rpmRequest;

                IntPtr nextRPMPtr = IntPtr.Zero;

                if (RPRequestList.Count > 1)
                {
                    nextRPMPtr = Marshal.AllocHGlobal(rpmRequestSize);
                    serviceChoiseRequest.rpmNextElem = nextRPMPtr;

                    structurePointers.Add(nextRPMPtr, typeof(RpmRequest));
                }

                for (int i = 1; i < RPRequestList.Count; i++)
                {
                    rpmRequest = new RpmRequest();

                    rpmModel = new ReadPropertyMultipleRequestModel();
                    rpmModel = RPRequestList[i];
                    rpmRequest.arrayIndex = rpmModel.ArrayIndex;
                    rpmRequest.arrayIndexPresent = rpmModel.ArrayIndexPresent;
                    rpmRequest.objectInstance = rpmModel.ObjectInstance;
                    rpmRequest.objectProperty = rpmModel.ObjectProperty;
                    rpmRequest.objectType = rpmModel.ObjectType;

                    IntPtr current = nextRPMPtr;

                    if (i < RPRequestList.Count - 1)
                    {
                        nextRPMPtr = Marshal.AllocHGlobal(rpmRequestSize);
                        rpmRequest.rpmNextElem = nextRPMPtr;

                        if (!structurePointers.ContainsKey(nextRPMPtr))
                            structurePointers.Add(nextRPMPtr, typeof(RpmRequest));
                    }

                    Marshal.StructureToPtr(rpmRequest, current, false);
                }
            }

            return serviceChoiseRequest;
        }
    }
}
