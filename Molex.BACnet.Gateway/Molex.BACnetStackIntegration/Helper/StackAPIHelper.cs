﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.ManagedWrapper.Helper
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.BACnetStackIntegration.Helper;
using Molex.BACnetStackIntegration.Log;
using Molex.BACnetStackIntegration.Manager;
using Molex.BACnetStackIntegration.Model.Request;
using Molex.BACnetStackIntegration.Processor;
using Molex.BACnetStackIntegration.Stack.Wrapper;
using Molex.BACnetStackIntegration.StackStructures;
using Molex.StackDataObjects.APIModels;
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Request;
using Molex.StackDataObjects.Response;
using Molex.StackDataObjects.StackInititializationModels;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

namespace Molex.BACnetStackIntegration.ManagedWrapper.Helper
{
    internal static class StackAPIHelper
    {
        static volatile object _syncLock = new object();
        static volatile object _syncLockWhoIs = new object();
        static volatile object _syncLockRP = new object();
        static volatile object _syncLockRPM = new object();
        static volatile object _syncLockWP = new object();
        static volatile object _syncLockIAm = new object();
        static volatile object _syncLockReadBDT = new object();
        static volatile object _syncLockReadFDT = new object();
        static volatile object _syncLockWriteBDT = new object();
        static volatile object _syncLockDeleteFDT = new object();
        static volatile object _syncLockRegisterFD = new object();
        static volatile object _syncLockCreateObject = new object();
        static volatile object _syncLockDeleteObject = new object();
        static volatile object _syncLockReadRange = new object();
        static volatile object _syncLockSubscribeCov = new object();

        #region Internal Methods

        internal static BacnetReturnType ExecuteStackApi(StackIntegrationRequestBase req, out int tokenId)
        {
            App_Callback_Interface_t callback;
            BacnetReturnType result = BacnetReturnType.BacdelSuccess;
            int tknId = 0;
            bool isSucceed = true;

            do
            {
                switch (req.Request.ReqType)
                {
                    case StackConstants.RequestType.BACDEL_Send_Who_Is:
                        lock (_syncLockWhoIs)
                        {
                            BACDELSendWhoIs((WhoIsRequest)req.Request, out tknId);
                            tokenId = tknId;
                        }
                        break;
                    case StackConstants.RequestType.BACDEL_Send_RP:
                        lock (_syncLockRP)
                        {
                            ReadPropertyProcessor readPropertyProcessor = new ReadPropertyProcessor();
                            ReadPropertyRequest readPropertyRequest = req.Request as ReadPropertyRequest;
                            result = BACDELSendReadPropertyRequest(readPropertyRequest, readPropertyProcessor.Process, out tknId);
                            tokenId = tknId;
                        }
                        break;
                    case StackConstants.RequestType.BACDEL_Send_RPM:
                        lock (_syncLockRPM)
                        {
                            ReadPropertyMultipleProcessor readPropertyMultipleProcessor = new ReadPropertyMultipleProcessor();
                            ReadPropertyMultipleRequest readPropertyMultipleRequest = req.Request as ReadPropertyMultipleRequest;
                            result = BACDELSendReadPropertyMultipleRequest(readPropertyMultipleRequest, readPropertyMultipleProcessor.Process, out tknId);
                            tokenId = tknId;
                        }
                        break;
                    case StackConstants.RequestType.BACDEL_Send_WP:
                        lock (_syncLockWP)
                        {
                            WritePropertyProcessor writePropertyProcessor = new WritePropertyProcessor();
                            callback = writePropertyProcessor.Process;
                            result = BACDELSendWritePropertyRequest((WritePropertyRequest)req.Request, callback, out tknId);
                        }
                        break;
                    case StackConstants.RequestType.BACDEL_Send_I_Am:
                        lock (_syncLockIAm)
                        {
                            result = BACDELSendIAm((IAmRequest)req.Request, out tknId);
                        }
                        break;
                    case StackConstants.RequestType.BACDEL_BBMD_REQ_READ_BDT:
                        lock (_syncLockReadBDT)
                        {
                            ReadBDTProcessor readBDTProcessor = new ReadBDTProcessor();
                            callback = readBDTProcessor.Process;

                            result = BacdelSendReadBDTRequest((ReadBDTRequest)req.Request, callback, out tknId);
                        }
                        break;
                    case StackConstants.RequestType.BACDEL_BBMD_REQ_READ_FDT:
                        lock (_syncLockReadFDT)
                        {
                            ReadFDTProcessor readFDTProcessor = new ReadFDTProcessor();
                            callback = readFDTProcessor.Process;

                            result = BacdelSendReadFDTRequest((ReadFDTRequest)req.Request, callback, out tknId);
                        }
                        break;
                    case StackConstants.RequestType.BACDEL_BBMD_REQ_WRITE_BDT:
                        lock (_syncLockWriteBDT)
                        {
                            WriteBDTProcessor writeBDTProcessor = new WriteBDTProcessor();
                            callback = writeBDTProcessor.Process;

                            result = BacdelSendWriteBDTRequest((WriteBDTRequest)req.Request, callback, out tknId);
                        }
                        break;
                    case StackConstants.RequestType.BACDEL_BBMD_REQ_DELETE_FDT_ENTRY:
                        lock (_syncLockDeleteFDT)
                        {
                            DeleteFDTEntryProcessor deleteFDTEntryProcessor = new DeleteFDTEntryProcessor();
                            callback = deleteFDTEntryProcessor.Process;

                            result = BacdelSendDeleteFDTEntryRequest((DeleteFDEntryRequest)req.Request, callback, out tknId);
                        }
                        break;
                    case StackConstants.RequestType.BACDEL_BBMD_REQ_REGISTER_FD:
                        lock (_syncLockRegisterFD)
                        {
                            RegisterFDProcessor registerFDProcessor = new RegisterFDProcessor();
                            callback = registerFDProcessor.Process;

                            result = BacdelSendRegisterFDEntryRequest((RegisterFDRequest)req.Request, callback, out tknId);
                        }
                        break;
                    case StackConstants.RequestType.BACDEL_Send_Create_Object:
                        lock (_syncLockCreateObject)
                        {
                            CreateObjectProcessor createObjectProcessor = new CreateObjectProcessor();
                            callback = createObjectProcessor.Process;

                            result = BacdelSendCreateObjectRequest((CreateObjectRequest)req.Request, callback, out tknId);
                        }
                        break;
                    case StackConstants.RequestType.BACDEL_Send_Delete_Object:
                        lock (_syncLockDeleteObject)
                        {
                            DeleteObjectProcessor deleteObjectProcessor = new DeleteObjectProcessor();
                            callback = deleteObjectProcessor.Process;

                            result = BacdelSendDeleteObjectRequest((DeleteObjectRequest)req.Request, callback, out tknId);
                        }
                        break;
                    case StackConstants.RequestType.BACDEL_Send_RR:
                        lock (_syncLockReadRange)
                        {
                            ReadRangeProcessor readRangeProcessor = new ReadRangeProcessor();
                            callback = readRangeProcessor.Process;

                            result = BacdelSendReadRangeRequest((ReadRangeRequest)req.Request, callback, out tknId);
                        }
                        break;
                    case StackConstants.RequestType.BACDEL_SEND_SUBSCRIBE_COV:
                        lock (_syncLockSubscribeCov)
                        {
                            SubscribeCovProcessor subscribeCovProcessor = new SubscribeCovProcessor();
                            callback = subscribeCovProcessor.Process;

                            result = BacdelSendSubscribeCovRequest((SubscribeCovRequest)req.Request, callback, out tknId);
                        }
                        break;
                }

                if (result == BacnetReturnType.BacdelInitiatorNoInvokeID || result == BacnetReturnType.BacdelInitiatorQueueFull)
                {
                    Thread.Sleep(500);
                    isSucceed = false;
                }
                else
                    isSucceed = true;
            } while (!isSucceed);


            tokenId = tknId;
            return result;
        }

        internal static BacnetReturnType BACDELSendStackDeInit()
        {
            return BacDelApi.BACDEL_Stack_Deinit();
        }

        internal static ResponseResult BACDELSendGetAddressBinding()
        {
            ResponseResult response = new ResponseResult();
            AddressBindingResponse addressBindingResponses = new AddressBindingResponse();
            List<AddressBindingDataModel> addressBindingData = new List<AddressBindingDataModel>();

            IntPtr bacnetAddressBinding = Marshal.AllocHGlobal(4);
            BacDelApi.BACDEL_Get_Dynamic_Device_Address_Binding(bacnetAddressBinding);

            IntPtr bacnetAddressBindingPtr = StructureConverter.PtrToStructure<IntPtr>(bacnetAddressBinding);

            if (bacnetAddressBindingPtr != IntPtr.Zero)
            {
                BacnetAddrBinding bacnetAddrBinding = StructureConverter.PtrToStructure<BacnetAddrBinding>(bacnetAddressBindingPtr);
                List<BacnetAddrBinding> addressBindings = SyncMessageProcessor.ProcessAddressBiding(bacnetAddrBinding);

                foreach (BacnetAddrBinding addressBinding in addressBindings)
                {
                    AddressBindingDataModel model = new AddressBindingDataModel();
                    model.ObjId = addressBinding.objId;
                    model.ObjectType = addressBinding.objectType;
                    model.MaxAPDULengthAccepted = addressBinding.maxAPDULenAccepted;
                    model.SegmentationSupport = addressBinding.segmentationSupport;
                    model.BACnetDeviceAddress = new BacnetAddressModel();
                    model.BACnetDeviceAddress.IPAddress = IpAddressHelper.ConvertBinaryIptoString(addressBinding.stAddress.ipAddrs);
                    model.BACnetDeviceAddress.IPAddressLength = addressBinding.stAddress.macLen;
                    model.BACnetDeviceAddress.MacAddress = IpAddressHelper.GetMacAddressStringFromByteArray(addressBinding.stAddress.dLen, addressBinding.stAddress.dvDadr);
                    model.BACnetDeviceAddress.MacLength = addressBinding.stAddress.dLen;
                    model.BACnetDeviceAddress.NetworkNumber = addressBinding.stAddress.net;
                    model.BACnetDeviceAddress.Port = (uint)IpAddressHelper.GetPortFromBinaryIp(addressBinding.stAddress.ipAddrs);

                    addressBindingData.Add(model);
                }
            }

            addressBindingResponses.AddressBindingResponseData = addressBindingData;

            response.IsSucceed = true;
            response.Response = addressBindingResponses;

            BacDelApi.BACDEL_Free_Dynamic_Device_Address_Binding(bacnetAddressBinding);

            Marshal.FreeHGlobal(bacnetAddressBinding);
            bacnetAddressBinding = IntPtr.Zero;
            return response;
        }

        internal static BacnetReturnType BACDELSendDeleteAddressBinding(int deviceId, BacnetAddressModel bacnetDeviceAddress)
        {
            IntPtr bacnetAddress = IntPtr.Zero;

            BacnetAddress bacnetDevAddress = GetBacnetAddress(bacnetDeviceAddress);
            bacnetAddress = StructureConverter.StructureToPtr<BacnetAddress>(bacnetDevAddress);

            BacnetReturnType bacnetReturnType = BacDelApi.BACDEL_Delete_Dynamic_Device_Address_Binding(deviceId, bacnetAddress);
            Marshal.FreeHGlobal(bacnetAddress);
            bacnetAddress = IntPtr.Zero;
            return bacnetReturnType;
        }

        internal static BacnetReturnType BACDELSendDisableBBMDRequest()
        {
            BacnetReturnType returnType = BacDelApi.BACDEL_Disable_BBMD();

            return returnType;
        }

        internal static BacnetReturnType BACDELSendEnableBBMDRequest()
        {
            BacnetReturnType returnType = BacDelApi.BACDEL_Enable_BBMD();

            return returnType;
        }

        internal static BacnetReturnType BACDELSendSetDeviceApduTimeoutRequest(uint deviceId, uint apduTimeout, bool isAllDevice)
        {
            BacnetReturnType returnType = BacDelApi.BACDEL_Set_Device_Apdu_Timeout(deviceId, apduTimeout, isAllDevice);

            return returnType;
        }

        internal static BacnetReturnType BACDELSendSetDeviceApduRetriesRequest(uint deviceId, uint apduRetries, bool isAllDevice)
        {
            BacnetReturnType returnType = BacDelApi.BACDEL_Set_Device_Apdu_Retries(deviceId, apduRetries, isAllDevice);

            return returnType;
        }

        internal static BacnetReturnType BACDELSendChangeIpAddressPortNoRequest(ushort port, string address)
        {
            byte[] ipAddress = IPAddress.Parse(address).GetAddressBytes();
            BacnetReturnType returnType = BacDelApi.BACDEL_Change_IpAddress_PortNo(port, ipAddress);

            return returnType;
        }

        internal static BacnetReturnType BacdelSendChangeDeviceIdRequest(uint oldDeviceId, uint newDeviceId, uint newDADR)
        {
            BacnetReturnType returnType = BacDelApi.BACDEL_Change_Device_Id(oldDeviceId, newDeviceId, newDADR);
            return returnType;
        }

        internal static ResponseResult BacdelSendReadRegisterFDListRequest()
        {
            ResponseResult response = new ResponseResult();
            GetRegisteredFdListResponse getRegisterFdListResponse = new GetRegisteredFdListResponse();
            List<RegisteredFDListResponseModel> registeredFDListResponseData = new List<RegisteredFDListResponseModel>();

            IntPtr regFdData = Marshal.AllocHGlobal(4);
            BacDelApi.BACDEL_Get_Register_FD_List(regFdData);

            IntPtr regFdDataPtr = StructureConverter.PtrToStructure<IntPtr>(regFdData);
            if (regFdDataPtr != IntPtr.Zero)
            {
                FdtData regFdDataResponse = StructureConverter.PtrToStructure<FdtData>(regFdDataPtr);
                List<FdtData> registerdFdDataList = SyncMessageProcessor.ProcessGetRegisteredFdListResponse(regFdDataResponse);

                foreach (FdtData fdtData in registerdFdDataList)
                {
                    RegisteredFDListResponseModel model = new RegisteredFDListResponseModel();
                    model.IpAddress = IpAddressHelper.ConvertBinaryIptoString(fdtData.ipAddress);
                    model.PortNo = fdtData.portNo;
                    model.TimeToLive = fdtData.timeToLive;
                    model.TimeRemaining = fdtData.timeRemaining;

                    registeredFDListResponseData.Add(model);
                }
            }

            getRegisterFdListResponse.RegisteredFDList = registeredFDListResponseData;

            response.IsSucceed = true;
            response.Response = getRegisterFdListResponse;

            BacDelApi.BACDEL_Free_Register_FD_List(regFdData);

            Marshal.FreeHGlobal(regFdData);
            regFdData = IntPtr.Zero;
            return response;
        }

        internal static bool BACDELSendGetBBMDStatusRequest()
        {
            byte returnType = BacDelApi.BACDEL_Get_BBMD_Status();

            if (returnType == 1)
                return true;
            else
                return false;
        }

        internal static bool BACDELSendGetFDStatusRequest()
        {
            byte returnType = BacDelApi.BACDEL_Get_FD_Status();

            if (returnType == 1)
                return true;
            else
                return false;
        }

        internal static void RegisterAutoCallBack(BacAppCallBackFunChoice appChoice, App_AutoResp_Interface_t autoCallback)
        {
            BacDelApi.BACDEL_App_CallBack_Register(appChoice, autoCallback);
        }

        internal static BacnetReturnType BacdelSetPresentvalueForExternalFunctionality(uint sourceDeviceId, ExtPropertyDataModel pvTriggerTypeModel, BacnetValueModel bacnetValueModel)
        {
            IntPtr anyValuePtr = IntPtr.Zero;
            IntPtr pvTriggerTypePtr = IntPtr.Zero;

            ExtPropertyData extPropertyData = new ExtPropertyData();
            extPropertyData.alarmFlag = pvTriggerTypeModel.AlarmFlag;
            extPropertyData.eeObjectID = pvTriggerTypeModel.EEObjectID;
            extPropertyData.trendFlag = pvTriggerTypeModel.TrendFlag;
            extPropertyData.tlObjectID = pvTriggerTypeModel.TLObjectID;

            extPropertyData.trendsStatusFlags = new BacnetBitStr();
            extPropertyData.trendsStatusFlags.unusedBits = 4;
            extPropertyData.trendsStatusFlags.byteCnt = 1;
            byte statusFlagByte = 0x00;
            byte[] bitStringBytes = new byte[1];
            bitStringBytes[0] = statusFlagByte;
            extPropertyData.trendsStatusFlags.transBits = bitStringBytes;

            extPropertyData.alarmsStatusFlags = new BacnetBitStr();
            extPropertyData.alarmsStatusFlags.unusedBits = 4;
            extPropertyData.alarmsStatusFlags.byteCnt = 1;
            byte alarmStatusFlagByte = 0x00;
            byte[] alarmBitStringBytes = new byte[1];
            alarmBitStringBytes[0] = alarmStatusFlagByte;
            extPropertyData.alarmsStatusFlags.transBits = alarmBitStringBytes;

            pvTriggerTypePtr = StructureConverter.StructureToPtr<ExtPropertyData>(extPropertyData);
            AnyValue anyValue = new AnyValue();
            anyValue.propValue.tagType = bacnetValueModel.TagType;

            byte[] anyValueStructureBytes = EncodingHelper.SetBacnetPropertyValueUnion(bacnetValueModel);
            anyValue.propValue.BacnetPropertyValueUnion = new byte[BacDelPropertyDef.BacnetPropertyValueUnionSize];
            Buffer.BlockCopy(anyValueStructureBytes, 0, anyValue.propValue.BacnetPropertyValueUnion, 0, anyValueStructureBytes.Length);
            anyValuePtr = StructureConverter.StructureToPtr<AnyValue>(anyValue);

            BacnetReturnType returnType = BacDelApi.BACDEL_Set_MonitoredValues_For_External_Functionality(sourceDeviceId, anyValuePtr, pvTriggerTypePtr);

            Marshal.FreeHGlobal(anyValuePtr);
            Marshal.FreeHGlobal(pvTriggerTypePtr);
            anyValuePtr = IntPtr.Zero;
            pvTriggerTypePtr = IntPtr.Zero;
            return returnType;
        }

        internal static BacnetReturnType BacdelAddObject(uint deviceID, BacnetObjectType objType, uint objectID, string objectName)
        {
            string charStringVal = Convert.ToString(objectName);
            byte[] propertyVal = new byte[255];

            byte[] arrByte = Encoding.ASCII.GetBytes(charStringVal);
            int arrayLength = 0;

            if (arrByte.Length <= 255)
                arrayLength = arrByte.Length;
            else
                arrayLength = 255;

            BacnetCharStr charString = new BacnetCharStr();
            charString.strLen = (uint)(arrayLength);

            Buffer.BlockCopy(arrByte, 0, propertyVal, 0, arrayLength);
            charString.charStr = propertyVal;
            IntPtr PtrcharString = StructureConverter.StructureToPtr<BacnetCharStr>(charString);
            BacnetReturnType returnType = BacDelApi.BACDEL_Add_Object(deviceID, objType, objectID, PtrcharString);
            return returnType;
        }

        internal static BacnetReturnType BacdelRemoveObject(uint deviceID, BacnetObjectType objType, uint objID)
        {
            BacnetReturnType returnType = BacDelApi.BACDEL_Delete_Object(deviceID, objType, objID);
            return returnType;
        }

        internal static ResponseResult BacdelGetObjectPropertyValue(uint deviceId, uint objectId, BacnetObjectType objectType, BacnetPropertyID objectProperty, uint arrayIndex, bool isArrayIndexPresent)
        {
            ResponseResult response = new ResponseResult();
            GetObjectPropertyValueResponse getObjectPropertyValueResponse = new GetObjectPropertyValueResponse();
            IntPtr propVal = Marshal.AllocHGlobal(4);
            IntPtr propDataType = Marshal.AllocHGlobal(4);

            BacnetErrorCode bacnetErrorCode = BacDelApi.BACDEL_Get_Object_Property(deviceId, objectId, objectType, objectProperty, arrayIndex, propVal, isArrayIndexPresent, propDataType);

            if (bacnetErrorCode != BacnetErrorCode.MaxBacnetErrorCode)
            {
                response.IsSucceed = false;
                response.ErrorModel = new ErrorResponseModel();
                response.ErrorModel.Type = ErrorType.BACnetError;
                response.ErrorModel.ErrorCode = (int)bacnetErrorCode;

                Marshal.FreeHGlobal(propVal);
                Marshal.FreeHGlobal(propDataType);
                propVal = IntPtr.Zero;
                propDataType = IntPtr.Zero;
                return response;
            }

            IntPtr propValPtr = StructureConverter.PtrToStructure<IntPtr>(propVal);
            BacnetDataType dataType = (BacnetDataType)StructureConverter.PtrToStructure<int>(propDataType);
            getObjectPropertyValueResponse.DataType = dataType;

            if (propValPtr != IntPtr.Zero)
            {
                getObjectPropertyValueResponse.PropertyValue = DecodingHelper.DecodePropertyValue(dataType, objectProperty, propValPtr);
            }

            response.IsSucceed = true;
            response.Response = getObjectPropertyValueResponse;

            BacDelApi.BACDEL_Free_Object_Property(dataType, propVal);

            Marshal.FreeHGlobal(propVal);
            Marshal.FreeHGlobal(propDataType);
            propVal = IntPtr.Zero;
            propDataType = IntPtr.Zero;
            return response;
        }

        internal static ResponseResult BacdelSetObjectPropertyValue(uint deviceID, uint objectID, BacnetObjectType objectType, BacnetPropertyID objectProperty, uint arrayIndex, object propertyValue, byte priority, bool isArrayIndexPresent, BacnetDataType dataType)
        {
            ResponseResult response = new ResponseResult();
            bool hasLazyPointer = false;
            bool isError = false;

            Dictionary<IntPtr, Type> structurePointers = new Dictionary<IntPtr, Type>();
            BacnetDataType bacnetDataType = CommanHelper.GetPropertyDataType(objectProperty, objectType, dataType);
            IntPtr propVal = EncodingHelper.EncodePropertyValue(bacnetDataType, objectProperty, propertyValue, ref structurePointers, out hasLazyPointer, out isError);

            if (isError)
            {
                response.IsSucceed = false;
                response.ErrorModel = new ErrorResponseModel();
                response.ErrorModel.Type = ErrorType.StackError;
                response.ErrorModel.ErrorCode = (int)BacnetReturnType.BacdelInvalidInputParameter;

                Marshal.FreeHGlobal(propVal);
                propVal = IntPtr.Zero;
                return response;
            }

            BacnetErrorCode bacnetErrorCode = BacDelApi.BACDEL_Set_Object_Property(deviceID, objectID, objectType, objectProperty, arrayIndex, propVal, priority, isArrayIndexPresent, bacnetDataType);

            if (bacnetErrorCode != BacnetErrorCode.MaxBacnetErrorCode)
            {
                response.IsSucceed = false;
                response.ErrorModel = new ErrorResponseModel();
                response.ErrorModel.Type = ErrorType.BACnetError;
                response.ErrorModel.ErrorCode = (int)bacnetErrorCode;

                Marshal.FreeHGlobal(propVal);
                propVal = IntPtr.Zero;
                return response;
            }

            Marshal.FreeHGlobal(propVal);
            propVal = IntPtr.Zero;
            response.IsSucceed = true;
            return response;
        }

        internal static void RegisterAppLayerCallback(App_Interface_t appCallback)
        {
            BacDelApi.BACDEL_AppLayer_CallBack_Register(appCallback);
        }

        internal static BacnetReturnType BacdelUpdateCovASubscriptionLifeTime(uint lifeTime)
        {
            BacnetReturnType result = BacDelApi.BACDEL_Update_COVA_Subscription_LifeTime(lifeTime);
            return result;
        }

        internal static void RegisterPacketDelay(uint delayTime)
        {
            BacDelApi.BACDEL_Set_Time_Delay(true, false, false, delayTime, 0, 0);
        }

        internal static BacnetReturnType SetVirtualNetworkNumber(uint virtualNetworkNumber)
        {
            BacnetReturnType result = BacDelApi.BACDEL_Set_Virtual_Network_No(virtualNetworkNumber);
            return result;
        }

        //New function to read BDT list from stack
        internal static ResponseResult GetBDTList()
        {
            ResponseResult response = new ResponseResult();
            ReadBdtResponse readBdtResponse = new ReadBdtResponse();
            List<BDTModel> bdtList = new List<BDTModel>();
            IntPtr ptrBacnetIPArguments = StructureConverter.StructureToPtr(readBdtResponse);
            BacDelApi.BACDEL_Get_BDT_List(ptrBacnetIPArguments);
            readBdtResponse = StructureConverter.PtrToStructure<ReadBdtResponse>(ptrBacnetIPArguments);
            List<BacnetIPAddress> ipList = ProcessReadBDTResponse(readBdtResponse);

            foreach (BacnetIPAddress item in ipList)
            {
                BDTModel model = new BDTModel();
                model.IPAddress = IpAddressHelper.ConvertBinaryIptoString(item.ipAddress);
                model.Port = item.portNo;
                model.BroadcastMask = item.broadcastMask;
                bdtList.Add(model);
            }
            ReadBDTResponse readBDTResponse = new ReadBDTResponse();
            readBDTResponse.BDTEntries = bdtList;
            response.Response = readBDTResponse;
            response.IsSucceed = true;
            BacDelApi.BACDEL_Free_BDT_List(ptrBacnetIPArguments);
            Marshal.FreeHGlobal(ptrBacnetIPArguments);
            ptrBacnetIPArguments = IntPtr.Zero;
            return response;
        }

        internal static BacnetReturnType SetLocalNetworkNumber(uint localNetworkNumber)
        {
            BacnetReturnType result = BacDelApi.BACDEL_Set_Local_Network_No(localNetworkNumber);
            return result;
        }

        internal static BacnetReturnType BACDELWriteBDTEntry(List<BDTModel> bdtList)
        {
            IntPtr ptrBacnetIPArguments = StructureConverter.StructureToPtr(bdtList);
            BacnetReturnType returnType = BacDelApi.BACDEL_Set_BDT_List(ptrBacnetIPArguments);
            Marshal.FreeHGlobal(ptrBacnetIPArguments);
            ptrBacnetIPArguments = IntPtr.Zero;
            return returnType;
        }

        #region Stack Init API

        internal static void SetVendorSpecificdata(VendorModel vendor)
        {
            byte[] vendorNameArray = Encoding.UTF8.GetBytes(vendor.VendorName.ToCharArray());
            byte[] modelNameArray = Encoding.UTF8.GetBytes(vendor.ModelName.ToCharArray());
            byte[] firmwareRevisionArray = null;
            byte[] applicationSoftwareVersionArray = null;// Encoding.UTF8.GetBytes(vendor.SoftwareVersion.ToCharArray());
            byte[] locationArray = Encoding.UTF8.GetBytes(vendor.Location.ToCharArray());
            byte[] deviceDescriptionArray = Encoding.UTF8.GetBytes(vendor.DeviceDescription.ToCharArray());
            byte[] objectDescriptionArray = Encoding.UTF8.GetBytes(vendor.ObjectDescription.ToCharArray());
            byte[] profileNameArray = Encoding.UTF8.GetBytes(vendor.ProfileName.ToCharArray());
            byte[] serialNumberArray = Encoding.UTF8.GetBytes(vendor.SerialNumber.ToCharArray());

            BacDelApi.BACDEL_Set_Vendor_Specific_Data(vendor.VendorID, vendorNameArray, modelNameArray,
                firmwareRevisionArray, applicationSoftwareVersionArray, locationArray, deviceDescriptionArray,
                objectDescriptionArray, profileNameArray, serialNumberArray);
        }

        internal static void BACDELSetDevicePassword(int deviceID, string password)
        {
            byte[] vendorPasswordArray = Encoding.UTF8.GetBytes(password.ToCharArray());
            BacDelApi.BACDEL_Set_Device_Password(deviceID, vendorPasswordArray);
        }

        internal static BacnetReturnType BACDELCreateDirectory()
        {
            BacnetReturnType bacnetReturnType = BacDelApi.BACDEL_Create_Directory();
            return bacnetReturnType;
        }

        internal static BacnetReturnType BACDELAddDevice(BacdelDeviceModel device)
        {
            return BacDelApi.BACDEL_Add_Device(device.DAdr, device.DeviceID, device.SNet);
        }

        internal static BacnetReturnType BACDELClearDeviceObject()
        {
            BacDelApi.BACDEL_Clear_Device_Objects();
            return BacnetReturnType.BacdelSuccess;
        }

        internal static BacnetReturnType BACDELStackInit(StackConfigModel stackConfig)
        {
            IPAddress address = stackConfig.IpAddress;
            byte[] tempAdr = address.GetAddressBytes();

            return BacDelApi.BACDEL_Stack_Init(tempAdr, stackConfig.Port, stackConfig.LocalNetworkNumber, stackConfig.VirtualNetworkNumber);
        }

        internal static BacnetReturnType BACDELSetMaxLimits(BACAppMaxLimitsModel bacAppMaxLimitsModel, BacnetMaxlimitParameters firstFailedLimit, BacnetMaxlimitParameters limitType, bool setSpecifiedLimit)
        {
            BACAppMaxLimits bacAppMaxLimits = new BACAppMaxLimits();
            bacAppMaxLimits.InitiateQueNodes = bacAppMaxLimitsModel.InitiateQueNodes;
            bacAppMaxLimits.BBMDInitiateQueNodes = bacAppMaxLimitsModel.BBMDInitiateQueNodes;
            bacAppMaxLimits.MaxBdtFdtEntries = bacAppMaxLimitsModel.MaxBdtFdtEntries;
            bacAppMaxLimits.MaxObject = bacAppMaxLimitsModel.MaxObject;
            bacAppMaxLimits.MaxDynamicAddrBind = bacAppMaxLimitsModel.MaxDynamicAddrBind;
            bacAppMaxLimits.MaxCovSubsTx = bacAppMaxLimitsModel.MaxCovSubsTx;
            bacAppMaxLimits.MaxStateText = bacAppMaxLimitsModel.MaxStateText;
            bacAppMaxLimits.MaxAFValuesList = bacAppMaxLimitsModel.MaxAFValuesList;
            bacAppMaxLimits.MaxExSchdList = bacAppMaxLimitsModel.MaxExSchdList;
            bacAppMaxLimits.MaxTimeValueList = bacAppMaxLimitsModel.MaxTimeValueList;
            bacAppMaxLimits.MaxDateList = bacAppMaxLimitsModel.MaxDateList;
            bacAppMaxLimits.MaxDestinationList = bacAppMaxLimitsModel.MaxDestinationList;
            bacAppMaxLimits.MaxDevObjPropRefList = bacAppMaxLimitsModel.MaxDevObjPropRefList;
            bacAppMaxLimits.MaxEventParaList = bacAppMaxLimitsModel.MaxEventParaList;
            bacAppMaxLimits.MaxObjIdList = bacAppMaxLimitsModel.MaxObjIdList;
            bacAppMaxLimits.MaxActiveCovSubs = bacAppMaxLimitsModel.MaxActiveCovSubs;
            bacAppMaxLimits.MaxStaticAddrBind = bacAppMaxLimitsModel.MaxStaticAddrBind;
            bacAppMaxLimits.MaxNwAnalyzability = bacAppMaxLimitsModel.MaxNwAnalyzability;
            bacAppMaxLimits.CbNotifyQueNodes = bacAppMaxLimitsModel.CbNotifyQueNodes;
            bacAppMaxLimits.MaxFaultParaList = bacAppMaxLimitsModel.MaxFaultParaList;
            bacAppMaxLimits.MaxCovUSubs = bacAppMaxLimitsModel.MaxCovUSubs;
            bacAppMaxLimits.MaxSubordinateList = bacAppMaxLimitsModel.MaxSubordinateList;

            IntPtr structBACAppMaxLimitsPtr = StructureConverter.StructureToPtr<BACAppMaxLimits>(bacAppMaxLimits);
            uint frstFailedLimit = (uint)firstFailedLimit;
            IntPtr firstFailedLimitPtr = StructureConverter.StructureToPtr<uint>(frstFailedLimit);

            BacnetReturnType result = BacDelApi.BACDEL_Set_Max_Limits(structBACAppMaxLimitsPtr, firstFailedLimitPtr, limitType, setSpecifiedLimit);

            Marshal.FreeHGlobal(structBACAppMaxLimitsPtr);
            Marshal.FreeHGlobal(firstFailedLimitPtr);
            structBACAppMaxLimitsPtr = IntPtr.Zero;
            firstFailedLimitPtr = IntPtr.Zero;
            return result;
        }

        internal static void RegisterSetGetPropertyWPCallback(BacnetObjectType objectType, BacnetPropertyID propertyId, IntPtr configType, bool readOrWrite)
        {
            BacDelApi.BACDEL_Set_Get_Property_Wp_Callback(objectType, propertyId, configType, readOrWrite);
        }

        internal static void BACDELSetGetPropertyAccessType(uint u32DevID, uint u32ObjId, BacnetObjectType eObjectType, BacnetPropertyID ePropId, PropAccessType eAccessType, bool bReadOrWrite)
        {
            BacDelApi.BACDEL_Set_Get_Property_AccessType(u32DevID, u32ObjId, eObjectType, ePropId, eAccessType, bReadOrWrite);
        }


        #endregion
        //New function to read FDT list from stack
        internal static ResponseResult GetFDTList()
        {
            ResponseResult response = new ResponseResult();
            ReadFdtResponse readFdtResponse = new ReadFdtResponse();
            List<FDTModel> fdtList = new List<FDTModel>();
            IntPtr ptrBacnetIPArguments = StructureConverter.StructureToPtr(readFdtResponse);
            BacDelApi.BACDEL_Get_FDT_List(ptrBacnetIPArguments);
            readFdtResponse = StructureConverter.PtrToStructure<ReadFdtResponse>(ptrBacnetIPArguments);
            List<FdtData> fdCollection = ProcessReadFDTResponse(readFdtResponse);

            foreach (FdtData item in fdCollection)
            {
                FDTModel model = new FDTModel();
                model.IPAddress = IpAddressHelper.ConvertBinaryIptoString(item.ipAddress);
                model.Port = item.portNo;
                model.TimeRemaining = item.timeRemaining;
                model.TimeToLive = item.timeToLive;
                fdtList.Add(model);
            }
            ReadFDTResponse readFDTResponse = new ReadFDTResponse();
            readFDTResponse.FDTEntries = fdtList;
            response.Response = readFDTResponse;
            response.IsSucceed = true;
            BacDelApi.BACDEL_Free_FDT_List(ptrBacnetIPArguments);
            Marshal.FreeHGlobal(ptrBacnetIPArguments);
            ptrBacnetIPArguments = IntPtr.Zero;
            return response;
        }

        #endregion

        #region Private Methods

        private static BacnetReturnType BACDELSendWhoIs(WhoIsRequest whoIs, out int tokenId)
        {
            int tknId = 0;

            BacnetAddress bacnetAddress = GetBacnetAddress(whoIs.DestinationAddress);
            DdbWhoIs structWhoIs = new DdbWhoIs()
            {
                devRangeHighLimit = whoIs.HighLimit,
                devRangeLowLimit = whoIs.LowLimit
            };

            IntPtr destinationDeviceAddress = StructureConverter.StructureToPtr(bacnetAddress);
            IntPtr whoIsData = StructureConverter.StructureToPtr(structWhoIs);
            IntPtr tokenIdPtr = StructureConverter.StructureToPtr(tknId);

            BacnetReturnType returnType = BacDelApi.BACDEL_Send_Who_Is(whoIs.SourceDeviceId, whoIs.DestinationDeviceId,
               IntPtr.Zero, whoIs.DestinationTypeFlag,
               whoIs.DestinationType,
               whoIs.IsNetworkLayerMessage, whoIs.NetworkNo, whoIsData, tokenIdPtr);

            tokenId = StructureConverter.PtrToStructure<int>(tokenIdPtr);

            Marshal.FreeHGlobal(destinationDeviceAddress);
            Marshal.FreeHGlobal(whoIsData);
            Marshal.FreeHGlobal(tokenIdPtr);
            destinationDeviceAddress = IntPtr.Zero;
            whoIsData = IntPtr.Zero;
            tokenIdPtr = IntPtr.Zero;
            return returnType;
        }

        internal static BacnetReturnType BACDELSendReadPropertyRequest(ReadPropertyRequest readPropertyRequest,
                                                                       App_Callback_Interface_t fpCallbackFun,
                                                                      out int tokenId)
        {
            int tknId = 0;
            BacnetAddress bacnetAddress = GetBacnetAddress(readPropertyRequest.DestinationAddress);
            RpRequest rpRequest = new RpRequest();
            rpRequest.arrayIndex = readPropertyRequest.RpRequest.ArrayIndex;
            rpRequest.arrayIndexPresent = readPropertyRequest.RpRequest.IsArrayIndexPresent;
            rpRequest.objectInstance = readPropertyRequest.RpRequest.ObjectInstanceNumber;
            rpRequest.objectProperty = readPropertyRequest.RpRequest.ObjectProperty;
            rpRequest.objectType = readPropertyRequest.RpRequest.ObjectType;

            IntPtr destinationAddr = StructureConverter.StructureToPtr(bacnetAddress);
            IntPtr rpData = StructureConverter.StructureToPtr(rpRequest);
            IntPtr tokenIdPtr = StructureConverter.StructureToPtr(tknId);
            CallbackManager.Instance.AddToCallbackDictionary(fpCallbackFun, readPropertyRequest);

            BacnetReturnType returnType = BacDelApi.BACDEL_Send_RP(readPropertyRequest.SourceDeviceId,
                                                      readPropertyRequest.DestinationDeviceId,
                                                      destinationAddr,
                                                      readPropertyRequest.DestinationTypeFlag,
                                                      rpData,
                                                      fpCallbackFun,
                                                      tokenIdPtr);
            tokenId = StructureConverter.PtrToStructure<int>(tokenIdPtr);

            Marshal.FreeHGlobal(destinationAddr);
            Marshal.FreeHGlobal(rpData);
            Marshal.FreeHGlobal(tokenIdPtr);
            destinationAddr = IntPtr.Zero;
            rpData = IntPtr.Zero;
            tokenIdPtr = IntPtr.Zero;
            return returnType;
        }

        private static BacnetReturnType BACDELSendReadPropertyMultipleRequest(ReadPropertyMultipleRequest readPropertyMultipleRequest, App_Callback_Interface_t callback, out int tokenId)
        {
            int tknId = 0;
            Dictionary<IntPtr, Type> structurePointers = new Dictionary<IntPtr, Type>();
            RPMStructureConversionHelper rpmStructureConversionHelper = new RPMStructureConversionHelper();

            BacnetAddress bacnetAddress = GetBacnetAddress(readPropertyMultipleRequest.DestinationAddress);
            RpmRequest rpmRequest = new RpmRequest();

            if (readPropertyMultipleRequest.RpmRequest != null)
            {
                rpmRequest = rpmStructureConversionHelper.GetRPMRequestStructure(readPropertyMultipleRequest.RpmRequest, ref structurePointers);
            }

            IntPtr destinationAddr = StructureConverter.StructureToPtr(bacnetAddress);
            IntPtr rpmData = StructureConverter.StructureToPtr(rpmRequest);
            IntPtr tokenIdPtr = StructureConverter.StructureToPtr(tknId);
            CallbackManager.Instance.AddToCallbackDictionary(callback, readPropertyMultipleRequest);

            BacnetReturnType returnType = BacDelApi.BACDEL_Send_RPM(readPropertyMultipleRequest.SourceDeviceId,
                                                      readPropertyMultipleRequest.DestinationDeviceId,
                                                      destinationAddr,
                                                      readPropertyMultipleRequest.DestinationTypeFlag,
                                                      rpmData,
                                                      callback,
                                                      tokenIdPtr);
            tokenId = StructureConverter.PtrToStructure<int>(tokenIdPtr);

            LazyPointerManager.Instance.AddPointer(tokenId, structurePointers);

            Marshal.FreeHGlobal(destinationAddr);
            Marshal.FreeHGlobal(rpmData);
            Marshal.FreeHGlobal(tokenIdPtr);
            destinationAddr = IntPtr.Zero;
            rpmData = IntPtr.Zero;
            tokenIdPtr = IntPtr.Zero;
            return returnType;
        }

        private static BacnetReturnType BACDELSendWritePropertyRequest(WritePropertyRequest writePropertyRequest, App_Callback_Interface_t callback, out int tokenId)
        {
            int tknId = 0;
            bool hasLazyPointer = false;
            bool isError = false;
            Dictionary<IntPtr, Type> structurePointers = new Dictionary<IntPtr, Type>();
            BacnetAddress bacnetAddress = GetBacnetAddress(writePropertyRequest.DestinationAddress);
            IntPtr destinationAddr = StructureConverter.StructureToPtr(bacnetAddress);
            WpRequest wpRequest = new WpRequest();
            wpRequest.arrayIndex = writePropertyRequest.WpRequest.ArrayIndex;
            wpRequest.arrayIndexPresent = writePropertyRequest.WpRequest.ArrayIndexPresent;
            wpRequest.objectInstance = writePropertyRequest.WpRequest.ObjectInstance;
            wpRequest.objectProperty = writePropertyRequest.WpRequest.ObjectProperty;
            wpRequest.objectType = writePropertyRequest.WpRequest.ObjectType;
            wpRequest.priority = writePropertyRequest.WpRequest.Priority;

            BacnetDataType dataType = CommanHelper.GetPropertyDataType(writePropertyRequest.WpRequest.ObjectProperty, writePropertyRequest.WpRequest.ObjectType, writePropertyRequest.WpRequest.DataType);
            wpRequest.pvPropVal = EncodingHelper.EncodePropertyValue(dataType, writePropertyRequest.WpRequest.ObjectProperty, writePropertyRequest.WpRequest.PropertyValue, ref structurePointers, out hasLazyPointer, out isError);
            wpRequest.dataType = dataType;

            IntPtr wpData = StructureConverter.StructureToPtr(wpRequest);
            IntPtr tokenIdPtr = StructureConverter.StructureToPtr(tknId);
            CallbackManager.Instance.AddToCallbackDictionary(callback, writePropertyRequest);

            BacnetReturnType returnType = BacDelApi.BACDEL_Send_WP(writePropertyRequest.SourceDeviceId,
                writePropertyRequest.DestinationDeviceId,
                destinationAddr,
                writePropertyRequest.DestinationTypeFlag,
                wpData,
                callback,
                tokenIdPtr);

            tokenId = StructureConverter.PtrToStructure<int>(tokenIdPtr);

            if (hasLazyPointer)
                LazyPointerManager.Instance.AddPointer(tokenId, structurePointers);

            Marshal.FreeHGlobal(destinationAddr);
            Marshal.FreeHGlobal(wpData);
            Marshal.FreeHGlobal(tokenIdPtr);
            destinationAddr = IntPtr.Zero;
            wpData = IntPtr.Zero;
            tokenIdPtr = IntPtr.Zero;
            if (isError)
                return BacnetReturnType.BacdelInvalidInputParameter;
            else
                return returnType;

        }

        private static BacnetReturnType BACDELSendIAm(IAmRequest iAmRequest, out int tokenId)
        {
            int tknId = 0;
            IntPtr destinationDeviceAddress = StructureConverter.StructureToPtr(iAmRequest.DestinationAddress);
            IntPtr tokenIdPtr = StructureConverter.StructureToPtr(tknId);

            BacnetReturnType returnType = BacDelApi.BACDEL_Send_I_Am(iAmRequest.SourceDeviceId, iAmRequest.DestinationDeviceId,
                destinationDeviceAddress, iAmRequest.DestinationTypeFlag,
                iAmRequest.DestinationType, iAmRequest.IsNetworkLayerMessage, tokenIdPtr);

            tokenId = StructureConverter.PtrToStructure<int>(tokenIdPtr);

            Marshal.FreeHGlobal(destinationDeviceAddress);
            Marshal.FreeHGlobal(tokenIdPtr);
            destinationDeviceAddress = IntPtr.Zero;
            tokenIdPtr = IntPtr.Zero;
            return returnType;
        }

        //older function to read BDT list
        private static BacnetReturnType BacdelSendReadBDTRequest(ReadBDTRequest readBDTRequest, App_Callback_Interface_t callback, out int tokenId)
        {
            byte[] IpArray = new byte[6];
            Buffer.BlockCopy(IPAddress.Parse(readBDTRequest.IPAddress).GetAddressBytes(), 0, IpArray, 0, 4);
            byte[] port = BitConverter.GetBytes(readBDTRequest.Port);
            Array.Reverse(port);
            Buffer.BlockCopy(port, 0, IpArray, 4, 2);

            BacnetReturnType returnType = BacnetReturnType.BacdelSuccess;

            BacnetIPArguments bacnetIPArguments = new BacnetIPArguments();
            bacnetIPArguments.bvlcFunctionType = BacnetBvlcFunction.BvlcReadBroadcastDistTable;
            bacnetIPArguments.stDestBACnetAddr = new BacnetAddress() { dLen = 0, ipAddrs = IpArray, macLen = 6, dvDadr = new byte[6] };

            IntPtr ptrBacnetIPArguments = StructureConverter.StructureToPtr(bacnetIPArguments);
            CallbackManager.Instance.AddToCallbackDictionary(callback, readBDTRequest);

            InitiatorResponse response = BacDelApi.BACDEL_Generate_BBMD_Request(ptrBacnetIPArguments, callback);

            if (response.m_eRetType == BacnetReturnType.BacdelInitiatorSuccess)
            {
                tokenId = response.m_i32TokenID;
                returnType = BacnetReturnType.BacdelSuccess;
            }
            else
            {
                tokenId = 0;
                returnType = response.m_eErrorCode;
            }

            Marshal.FreeHGlobal(ptrBacnetIPArguments);
            ptrBacnetIPArguments = IntPtr.Zero;
            return returnType;
        }

        //older function to read FDT list
        private static BacnetReturnType BacdelSendReadFDTRequest(ReadFDTRequest readFDTRequest, App_Callback_Interface_t callback, out int tokenId)
        {
            byte[] IpArray = new byte[6];
            Buffer.BlockCopy(IPAddress.Parse(readFDTRequest.IPAddress).GetAddressBytes(), 0, IpArray, 0, 4);
            byte[] port = BitConverter.GetBytes(readFDTRequest.Port);
            Array.Reverse(port);
            Buffer.BlockCopy(port, 0, IpArray, 4, 2);

            BacnetReturnType returnType = BacnetReturnType.BacdelSuccess;

            BacnetIPArguments bacnetIPArguments = new BacnetIPArguments();
            bacnetIPArguments.bvlcFunctionType = BacnetBvlcFunction.BvlcReadForeignDeviceTable;
            bacnetIPArguments.stDestBACnetAddr = new BacnetAddress() { dLen = 0, ipAddrs = IpArray, macLen = 6, dvDadr = new byte[6] };

            IntPtr ptrBacnetIPArguments = StructureConverter.StructureToPtr(bacnetIPArguments);
            CallbackManager.Instance.AddToCallbackDictionary(callback, readFDTRequest);

            InitiatorResponse response = BacDelApi.BACDEL_Generate_BBMD_Request(ptrBacnetIPArguments, callback);

            if (response.m_eRetType == BacnetReturnType.BacdelInitiatorSuccess)
            {
                tokenId = response.m_i32TokenID;
                returnType = BacnetReturnType.BacdelSuccess;
            }
            else
            {
                tokenId = 0;
                returnType = response.m_eErrorCode;
            }

            Marshal.FreeHGlobal(ptrBacnetIPArguments);
            ptrBacnetIPArguments = IntPtr.Zero;
            return returnType;
        }

        private static BacnetReturnType BacdelSendWriteBDTRequest(WriteBDTRequest writeBDTRequest, App_Callback_Interface_t callback, out int tokenId)
        {
            byte[] IpArray = new byte[6];
            Buffer.BlockCopy(IPAddress.Parse(writeBDTRequest.IPAddress).GetAddressBytes(), 0, IpArray, 0, 4);
            byte[] port = BitConverter.GetBytes(writeBDTRequest.Port);
            Array.Reverse(port);
            Buffer.BlockCopy(port, 0, IpArray, 4, 2);

            BacnetReturnType returnType = BacnetReturnType.BacdelSuccess;

            BacnetIPArguments bacnetIPArguments = new BacnetIPArguments();
            bacnetIPArguments.bvlcFunctionType = BacnetBvlcFunction.BvlcWriteBroadcastDistributionTable;
            bacnetIPArguments.stDestBACnetAddr = new BacnetAddress() { dLen = 0, ipAddrs = IpArray, macLen = 6, dvDadr = new byte[6] };

            Dictionary<IntPtr, Type> structurePointers = new Dictionary<IntPtr, Type>();
            WriteBdtRequest writeBdtRequest = new WriteBdtRequest();

            if (writeBDTRequest.BDTEntries != null)
            {
                writeBdtRequest = BBMDHelper.GetWriteBDTStructure(writeBDTRequest.BDTEntries, ref structurePointers);
            }

            byte[] bdtStructureBytes = StructureConverter.StructureToByteArray<WriteBdtRequest>(writeBdtRequest);
            bacnetIPArguments.stNPDUData.stAPDUData.serviceChoiceUnion = new byte[BacDelPropertyDef.serviceChoiceUnionSize];

            Buffer.BlockCopy(bdtStructureBytes, 0, bacnetIPArguments.stNPDUData.stAPDUData.serviceChoiceUnion, 0, bdtStructureBytes.Length);

            IntPtr ptrBacnetIPArguments = StructureConverter.StructureToPtr(bacnetIPArguments);
            CallbackManager.Instance.AddToCallbackDictionary(callback, writeBDTRequest);

            InitiatorResponse response = BacDelApi.BACDEL_Generate_BBMD_Request(ptrBacnetIPArguments, callback);

            LazyPointerManager.Instance.AddPointer(response.m_i32TokenID, structurePointers);

            if (response.m_eRetType == BacnetReturnType.BacdelInitiatorSuccess)
            {
                tokenId = response.m_i32TokenID;
                returnType = BacnetReturnType.BacdelSuccess;
            }
            else
            {
                tokenId = 0;
                returnType = response.m_eErrorCode;
            }

            Marshal.FreeHGlobal(ptrBacnetIPArguments);
            ptrBacnetIPArguments = IntPtr.Zero;
            return returnType;
        }

        private static BacnetReturnType BacdelSendDeleteFDTEntryRequest(DeleteFDEntryRequest deleteFDEntryRequest, App_Callback_Interface_t callback, out int tokenId)
        {
            byte[] IpArray = new byte[6];
            Buffer.BlockCopy(IPAddress.Parse(deleteFDEntryRequest.DestinationIPAddress).GetAddressBytes(), 0, IpArray, 0, 4);
            byte[] port = BitConverter.GetBytes(deleteFDEntryRequest.DestinationPort);
            Array.Reverse(port);
            Buffer.BlockCopy(port, 0, IpArray, 4, 2);

            BacnetReturnType returnType = BacnetReturnType.BacdelSuccess;

            BacnetIPArguments bacnetIPArguments = new BacnetIPArguments();
            bacnetIPArguments.bvlcFunctionType = BacnetBvlcFunction.BvlcDeleteForeignDeviceTableEntry;
            bacnetIPArguments.stDestBACnetAddr = new BacnetAddress() { dLen = 0, ipAddrs = IpArray, macLen = 6, dvDadr = new byte[6] };

            DeleteFdtRequest deleteFdtRequest = BBMDHelper.GetDeleteFDTStructure(deleteFDEntryRequest);
            byte[] deletefdtStructureBytes = StructureConverter.StructureToByteArray<DeleteFdtRequest>(deleteFdtRequest);
            bacnetIPArguments.stNPDUData.stAPDUData.serviceChoiceUnion = new byte[BacDelPropertyDef.serviceChoiceUnionSize];

            Buffer.BlockCopy(deletefdtStructureBytes, 0, bacnetIPArguments.stNPDUData.stAPDUData.serviceChoiceUnion, 0, deletefdtStructureBytes.Length);

            IntPtr ptrBacnetIPArguments = StructureConverter.StructureToPtr(bacnetIPArguments);
            CallbackManager.Instance.AddToCallbackDictionary(callback, deleteFDEntryRequest);

            InitiatorResponse response = BacDelApi.BACDEL_Generate_BBMD_Request(ptrBacnetIPArguments, callback);

            if (response.m_eRetType == BacnetReturnType.BacdelInitiatorSuccess)
            {
                tokenId = response.m_i32TokenID;
                returnType = BacnetReturnType.BacdelSuccess;
            }
            else
            {
                tokenId = 0;
                returnType = response.m_eErrorCode;
            }

            Marshal.FreeHGlobal(ptrBacnetIPArguments);
            ptrBacnetIPArguments = IntPtr.Zero;
            return returnType;
        }

        private static BacnetReturnType BacdelSendRegisterFDEntryRequest(RegisterFDRequest registerFDRequest, App_Callback_Interface_t callback, out int tokenId)
        {
            byte[] IpArray = new byte[6];
            Buffer.BlockCopy(IPAddress.Parse(registerFDRequest.IPAddress).GetAddressBytes(), 0, IpArray, 0, 4);
            byte[] port = BitConverter.GetBytes(registerFDRequest.Port);
            Array.Reverse(port);
            Buffer.BlockCopy(port, 0, IpArray, 4, 2);

            BacnetReturnType returnType = BacnetReturnType.BacdelSuccess;

            BacnetIPArguments bacnetIPArguments = new BacnetIPArguments();
            bacnetIPArguments.bvlcFunctionType = BacnetBvlcFunction.BvlcRegisterForeignDevice;
            bacnetIPArguments.stDestBACnetAddr = new BacnetAddress() { dLen = 0, ipAddrs = IpArray, macLen = 6, dvDadr = new byte[6] };

            RegisterFdRequest structRegisterFdRequest = new RegisterFdRequest();
            structRegisterFdRequest.timeToLive = registerFDRequest.TimeToLive;

            byte[] fdtStructureBytes = StructureConverter.StructureToByteArray<RegisterFdRequest>(structRegisterFdRequest);
            bacnetIPArguments.stNPDUData.stAPDUData.serviceChoiceUnion = new byte[BacDelPropertyDef.serviceChoiceUnionSize];

            Buffer.BlockCopy(fdtStructureBytes, 0, bacnetIPArguments.stNPDUData.stAPDUData.serviceChoiceUnion, 0, fdtStructureBytes.Length);

            IntPtr ptrBacnetIPArguments = StructureConverter.StructureToPtr(bacnetIPArguments);

            CallbackManager.Instance.AddToCallbackDictionary(callback, registerFDRequest);
            InitiatorResponse response = BacDelApi.BACDEL_Generate_BBMD_Request(ptrBacnetIPArguments, callback);

            if (response.m_eRetType == BacnetReturnType.BacdelInitiatorSuccess)
            {
                tokenId = response.m_i32TokenID;
                returnType = BacnetReturnType.BacdelSuccess;
            }
            else
            {
                tokenId = 0;
                returnType = response.m_eErrorCode;
            }

            Marshal.FreeHGlobal(ptrBacnetIPArguments);
            ptrBacnetIPArguments = IntPtr.Zero;
            return returnType;
        }

        private static BacnetReturnType BacdelSendCreateObjectRequest(CreateObjectRequest createObjectRequest, App_Callback_Interface_t callback, out int tokenId)
        {
            int tknId = 0;
            BacnetAddress bacnetAddress = GetBacnetAddress(createObjectRequest.DestinationAddress);
            IntPtr destinationDeviceAddress = StructureConverter.StructureToPtr(bacnetAddress);
            IntPtr tokenIdPtr = StructureConverter.StructureToPtr(tknId);

            CreateObjectReq createObjectReq = new CreateObjectReq();
            createObjectReq.devID = createObjectRequest.CreateObjectData.DeviceId;
            createObjectReq.isObjTypeSpecifier = createObjectRequest.CreateObjectData.IsObjTypeSpecifier;
            createObjectReq.objectId = createObjectRequest.CreateObjectData.ObjectId;
            createObjectReq.objectType = createObjectRequest.CreateObjectData.ObjectType;

            PropertyValues propertyValues = new PropertyValues();
            Dictionary<IntPtr, Type> structurePointers = new Dictionary<IntPtr, Type>();
            IntPtr propertyValuesPointer = IntPtr.Zero;

            if (createObjectRequest.CreateObjectData.PropertyValues != null)
            {
                propertyValues = CommanHelper.GetPropertyValuesStructure(createObjectRequest.CreateObjectData.PropertyValues, ref structurePointers);
                propertyValuesPointer = StructureConverter.StructureToPtr(propertyValues);
            }

            createObjectReq.initialValues = propertyValuesPointer;

            IntPtr createObjectPtr = StructureConverter.StructureToPtr<CreateObjectReq>(createObjectReq);
            CallbackManager.Instance.AddToCallbackDictionary(callback, createObjectRequest);

            BacnetReturnType returnType = BacDelApi.BACDEL_Send_Create_Object(createObjectRequest.SourceDeviceId, createObjectRequest.DestinationDeviceId, destinationDeviceAddress, createObjectRequest.DestinationTypeFlag, createObjectPtr, callback, tokenIdPtr);
            tokenId = StructureConverter.PtrToStructure<int>(tokenIdPtr);
            LazyPointerManager.Instance.AddPointer(tokenId, structurePointers);

            Marshal.FreeHGlobal(createObjectPtr);
            Marshal.FreeHGlobal(destinationDeviceAddress);
            Marshal.FreeHGlobal(tokenIdPtr);
            Marshal.FreeHGlobal(propertyValuesPointer);
            createObjectPtr = IntPtr.Zero;
            destinationDeviceAddress = IntPtr.Zero;
            tokenIdPtr = IntPtr.Zero;
            propertyValuesPointer = IntPtr.Zero;
            return returnType;
        }

        private static BacnetReturnType BacdelSendDeleteObjectRequest(DeleteObjectRequest deleteObjectRequest, App_Callback_Interface_t callback, out int tokenId)
        {
            int tknId = 0;
            BacnetAddress bacnetAddress = GetBacnetAddress(deleteObjectRequest.DestinationAddress);
            IntPtr destinationDeviceAddress = StructureConverter.StructureToPtr(bacnetAddress);
            IntPtr tokenIdPtr = StructureConverter.StructureToPtr(tknId);

            DeleteObjectReq deleteObjectReq = new DeleteObjectReq();
            deleteObjectReq.deviceID = deleteObjectRequest.DeleteObjectData.DeviceId;
            deleteObjectReq.objectId = deleteObjectRequest.DeleteObjectData.ObjectId;
            deleteObjectReq.objectType = deleteObjectRequest.DeleteObjectData.ObjectType;

            IntPtr deleteObjectReqPointer = StructureConverter.StructureToPtr(deleteObjectReq);
            CallbackManager.Instance.AddToCallbackDictionary(callback, deleteObjectRequest);

            BacnetReturnType returnType = BacDelApi.BACDEL_Send_Delete_Object(deleteObjectRequest.SourceDeviceId, deleteObjectRequest.DestinationDeviceId, destinationDeviceAddress, deleteObjectRequest.DestinationTypeFlag, deleteObjectReqPointer, callback, tokenIdPtr);
            tokenId = StructureConverter.PtrToStructure<int>(tokenIdPtr);

            Marshal.FreeHGlobal(deleteObjectReqPointer);
            Marshal.FreeHGlobal(destinationDeviceAddress);
            Marshal.FreeHGlobal(tokenIdPtr);
            deleteObjectReqPointer = IntPtr.Zero;
            destinationDeviceAddress = IntPtr.Zero;
            tokenIdPtr = IntPtr.Zero;
            return returnType;
        }

        private static BacnetReturnType BacdelSendReadRangeRequest(ReadRangeRequest readRangeRequest, App_Callback_Interface_t callback, out int tokenId)
        {
            int tknId = 0;
            BacnetAddress bacnetAddress = GetBacnetAddress(readRangeRequest.DestinationAddress);
            IntPtr destinationDeviceAddress = StructureConverter.StructureToPtr(bacnetAddress);
            IntPtr tokenIdPtr = StructureConverter.StructureToPtr(tknId);

            ReadRangeReq readRangeReq = new ReadRangeReq();
            readRangeReq.arrayIndex = readRangeRequest.ReadRangeRequestData.ArrayIndex;
            readRangeReq.arrayIndexPresent = readRangeRequest.ReadRangeRequestData.ArrayIndexPresent;
            readRangeReq.objectId = readRangeRequest.ReadRangeRequestData.ObjectId;
            readRangeReq.objectType = readRangeRequest.ReadRangeRequestData.ObjectType;
            readRangeReq.propertyId = readRangeRequest.ReadRangeRequestData.PropertyId;
            readRangeReq.rangeType = readRangeRequest.ReadRangeRequestData.RangeType;

            if (readRangeRequest.ReadRangeRequestData.RangeType == BacnetReadRange.ReadRangeByPosition)
            {
                readRangeReq.rangeUnion.byPosition.count = readRangeRequest.ReadRangeRequestData.RangeParam.Count;
                ReadRangeByPosition readRangeByPosition = readRangeRequest.ReadRangeRequestData.RangeParam as ReadRangeByPosition;
                readRangeReq.rangeUnion.byPosition.referenceIndex = readRangeByPosition.ReferenceIndex;
            }
            else if (readRangeRequest.ReadRangeRequestData.RangeType == BacnetReadRange.ReadRangeBySequenceNo)
            {
                readRangeReq.rangeUnion.bySeqNo.count = readRangeRequest.ReadRangeRequestData.RangeParam.Count;
                ReadRangeBySeqNo readRangeBySeqNo = readRangeRequest.ReadRangeRequestData.RangeParam as ReadRangeBySeqNo;
                readRangeReq.rangeUnion.bySeqNo.referenceIndex = readRangeBySeqNo.ReferenceIndex;
            }
            else if (readRangeRequest.ReadRangeRequestData.RangeType == BacnetReadRange.ReadRangeByTime)
            {
                readRangeReq.rangeUnion.byTime.count = readRangeRequest.ReadRangeRequestData.RangeParam.Count;
                ReadRangeByTime readRangeByTime = readRangeRequest.ReadRangeRequestData.RangeParam as ReadRangeByTime;

                readRangeReq.rangeUnion.byTime.dateTime.date.day = readRangeByTime.dateTime.Date.Day;
                readRangeReq.rangeUnion.byTime.dateTime.date.month = readRangeByTime.dateTime.Date.Month;
                readRangeReq.rangeUnion.byTime.dateTime.date.year = readRangeByTime.dateTime.Date.Year;
                readRangeReq.rangeUnion.byTime.dateTime.date.weekDay = readRangeByTime.dateTime.Date.WeekDay;

                readRangeReq.rangeUnion.byTime.dateTime.time.hour = readRangeByTime.dateTime.Time.Hour;
                readRangeReq.rangeUnion.byTime.dateTime.time.min = readRangeByTime.dateTime.Time.Min;
                readRangeReq.rangeUnion.byTime.dateTime.time.sec = readRangeByTime.dateTime.Time.Sec;
                readRangeReq.rangeUnion.byTime.dateTime.time.hundredths = readRangeByTime.dateTime.Time.Hundredths;
            }

            IntPtr readRangeReqPointer = StructureConverter.StructureToPtr(readRangeReq);
            CallbackManager.Instance.AddToCallbackDictionary(callback, readRangeRequest);

            BacnetReturnType returnType = BacDelApi.BACDEL_Send_RR(readRangeRequest.SourceDeviceId, readRangeRequest.DestinationDeviceId, destinationDeviceAddress, readRangeRequest.DestinationTypeFlag, readRangeReqPointer, callback, tokenIdPtr);
            tokenId = StructureConverter.PtrToStructure<int>(tokenIdPtr);

            Marshal.FreeHGlobal(readRangeReqPointer);
            Marshal.FreeHGlobal(destinationDeviceAddress);
            Marshal.FreeHGlobal(tokenIdPtr);
            readRangeReqPointer = IntPtr.Zero;
            destinationDeviceAddress = IntPtr.Zero;
            tokenIdPtr = IntPtr.Zero;
            return returnType;
        }

        private static BacnetReturnType BacdelSendSubscribeCovRequest(SubscribeCovRequest subscribeCovRequest, App_Callback_Interface_t callback, out int tokenId)
        {
            int tknId = 0;
            BacnetAddress bacnetAddress = GetBacnetAddress(subscribeCovRequest.DestinationAddress);
            IntPtr destinationDeviceAddress = StructureConverter.StructureToPtr(bacnetAddress);
            IntPtr tokenIdPtr = StructureConverter.StructureToPtr(tknId);

            CovSubscribeRequest covSubscribeReq = new CovSubscribeRequest();
            covSubscribeReq.subscribe = subscribeCovRequest.SubscribeCovReq.Subscribe;
            covSubscribeReq.notificationType = subscribeCovRequest.SubscribeCovReq.NotificationType;
            covSubscribeReq.monitoredObjectType = subscribeCovRequest.SubscribeCovReq.MonitoredObjectType;
            covSubscribeReq.monitoredObjectInstance = subscribeCovRequest.SubscribeCovReq.MonitoredObjectInstance;
            covSubscribeReq.processIdentifier = subscribeCovRequest.SubscribeCovReq.ProcessIdentifier;
            covSubscribeReq.lifetime = subscribeCovRequest.SubscribeCovReq.Lifetime;

            IntPtr covSubscribeReqPointer = StructureConverter.StructureToPtr(covSubscribeReq);
            CallbackManager.Instance.AddToCallbackDictionary(callback, subscribeCovRequest);

            BacnetReturnType returnType = BacDelApi.BACDEL_Send_Subscribe_Cov(subscribeCovRequest.SourceDeviceId, subscribeCovRequest.DestinationDeviceId, destinationDeviceAddress, subscribeCovRequest.DestinationTypeFlag, covSubscribeReqPointer, callback, tokenIdPtr);
            tokenId = StructureConverter.PtrToStructure<int>(tokenIdPtr);

            Marshal.FreeHGlobal(covSubscribeReqPointer);
            Marshal.FreeHGlobal(destinationDeviceAddress);
            Marshal.FreeHGlobal(tokenIdPtr);
            covSubscribeReqPointer = IntPtr.Zero;
            destinationDeviceAddress = IntPtr.Zero;
            tokenIdPtr = IntPtr.Zero;
            return returnType;
        }

        public static BacnetAddress GetBacnetAddress(BacnetAddressModel bacnetAddressModel)
        {
            BacnetAddress destinationAddress = new BacnetAddress();

            try
            {
                byte[] IpArray = new byte[6];

                if (bacnetAddressModel.IPAddress != null)
                {
                    IPAddress ipAddress = IPAddress.Parse(bacnetAddressModel.IPAddress);

                    Buffer.BlockCopy(ipAddress.GetAddressBytes(), 0, IpArray, 0, 4);
                    byte[] port = BitConverter.GetBytes(bacnetAddressModel.Port);
                    Array.Reverse(port);
                    Buffer.BlockCopy(port, 2, IpArray, 4, 2);
                }

                PhysicalAddress mac = PhysicalAddress.Parse(bacnetAddressModel.MacAddress);

                destinationAddress.dLen = bacnetAddressModel.MacLength;
                destinationAddress.macLen = bacnetAddressModel.IPAddressLength;
                destinationAddress.net = bacnetAddressModel.NetworkNumber;
                destinationAddress.ipAddrs = IpArray;
                destinationAddress.dvDadr = new byte[6];
                Buffer.BlockCopy(mac.GetAddressBytes(), 0, destinationAddress.dvDadr, 0, mac.GetAddressBytes().Length);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occured in GetBacnetAddress method.");
            }

            return destinationAddress;
        }

        #endregion



        private static List<FdtData> ProcessReadFDTResponse(ReadFdtResponse readFdtResponse)
        {
            var bacnetIpAddrList = new List<FdtData>();

            IntPtr nextPointer = readFdtResponse.fdtData;
            if (nextPointer != IntPtr.Zero)
            {
                FdtData fdt_data = (FdtData)Marshal.PtrToStructure(nextPointer, typeof(FdtData));

                bacnetIpAddrList.Add(fdt_data);
                nextPointer = fdt_data.next;

                while (nextPointer != IntPtr.Zero)
                {
                    fdt_data = (FdtData)Marshal.PtrToStructure(nextPointer, typeof(FdtData));
                    bacnetIpAddrList.Add(fdt_data);
                    nextPointer = fdt_data.next;
                }
            }

            Marshal.FreeHGlobal(nextPointer);
            nextPointer = IntPtr.Zero;
            return bacnetIpAddrList;
        }

        private static List<BacnetIPAddress> ProcessReadBDTResponse(ReadBdtResponse readBdtResponse)
        {
            var bacnetIpAddrList = new List<BacnetIPAddress>();

            IntPtr nextPointer = readBdtResponse.bIpAddress;

            if (nextPointer != IntPtr.Zero)
            {
                BacnetIPAddress bdt_data = (BacnetIPAddress)Marshal.PtrToStructure(nextPointer, typeof(BacnetIPAddress));

                bacnetIpAddrList.Add(bdt_data);
                nextPointer = bdt_data.next;

                while (nextPointer != IntPtr.Zero)
                {
                    bdt_data = (BacnetIPAddress)Marshal.PtrToStructure(nextPointer, typeof(BacnetIPAddress));
                    bacnetIpAddrList.Add(bdt_data);
                    nextPointer = bdt_data.next;
                }
            }

            return bacnetIpAddrList;
        }
    }
}
