﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.Helper
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.BACnetStackIntegration.Stack.Wrapper;
using Molex.StackDataObjects.APIModels;
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Models;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace Molex.BACnetStackIntegration.Helper
{
    public class DecodingHelper
    {
        public static object DecodePropertyValue(BacnetDataType datatype, BacnetPropertyID propertyId, IntPtr pvPropVal)
        {
            object responseConverted = new object();

            /* check input pointer */
            if (pvPropVal == IntPtr.Zero && datatype != BacnetDataType.BacnetDTNull &&
                datatype != BacnetDataType.BacnetDTEmpty)
            {
                return null;
            }

            switch (datatype)
            {
                case BacnetDataType.BacnetDTBoolean:
                    PrBacnetBool prBacnetBool = (PrBacnetBool)Marshal.PtrToStructure(pvPropVal, typeof(PrBacnetBool));
                    responseConverted = prBacnetBool.val;
                    break;
                case BacnetDataType.BacnetDTEnum:
                    PrBinaryEnumPV prBinaryEnumPV = (PrBinaryEnumPV)Marshal.PtrToStructure(pvPropVal, typeof(PrBinaryEnumPV));
                    responseConverted = prBinaryEnumPV.val;
                    break;
                case BacnetDataType.BacnetDTEnumNew:
                    PrBacnetEnum prBacnetEnum = (PrBacnetEnum)Marshal.PtrToStructure(pvPropVal, typeof(PrBacnetEnum));
                    responseConverted = prBacnetEnum.val;
                    break;
                case BacnetDataType.BacnetDTUnsigned32:
                    PrBacnetUnsigned32 prBacnetUnsigned32 = (PrBacnetUnsigned32)Marshal.PtrToStructure(pvPropVal, typeof(PrBacnetUnsigned32));
                    responseConverted = prBacnetUnsigned32.val;
                    break;
                case BacnetDataType.BacnetDTUnsigned16:
                    PrBacnetUnsigned16 prBacnetUnsigned16 = (PrBacnetUnsigned16)Marshal.PtrToStructure(pvPropVal, typeof(PrBacnetUnsigned16));
                    responseConverted = prBacnetUnsigned16.val;
                    break;
                case BacnetDataType.BacnetDTUnsigned:
                    PrBacnetUnsigned32 prBacnetUnsigned = (PrBacnetUnsigned32)Marshal.PtrToStructure(pvPropVal, typeof(PrBacnetUnsigned32));
                    responseConverted = prBacnetUnsigned.val;
                    break;
                case BacnetDataType.BacnetDTInteger:
                    PrBacnetSigned32 prBacnetSigned32 = (PrBacnetSigned32)Marshal.PtrToStructure(pvPropVal, typeof(PrBacnetSigned32));
                    responseConverted = prBacnetSigned32.val;
                    break;
                case BacnetDataType.BacnetDTReal:
                    PrBacnetReal prBacnetReal = (PrBacnetReal)Marshal.PtrToStructure(pvPropVal, typeof(PrBacnetReal));
                    responseConverted = prBacnetReal.val;
                    break;
                case BacnetDataType.BacnetDTDouble:
                    PrBacnetDouble prBacnetDouble = (PrBacnetDouble)Marshal.PtrToStructure(pvPropVal, typeof(PrBacnetDouble));
                    responseConverted = prBacnetDouble.val;
                    break;
                case BacnetDataType.BacnetDTCharString:
                    PrBacnetCharStr prBacnetCharStr = (PrBacnetCharStr)Marshal.PtrToStructure(pvPropVal, typeof(PrBacnetCharStr));
                    int length = (int)(prBacnetCharStr.stCharString.strLen);
                    byte[] unsigned = new byte[length];
                    Buffer.BlockCopy(prBacnetCharStr.stCharString.charStr, 0, unsigned, 0, length);

                    switch (prBacnetCharStr.stCharString.encoding)
                    {
                        case (byte)BacnetCharacterStringEncoding.CharacterUCS2:
                            responseConverted = Encoding.BigEndianUnicode.GetString(unsigned);
                            break;
                        default:
                            responseConverted = Encoding.UTF8.GetString(unsigned);
                            break;
                    }

                    break;
                case BacnetDataType.BacnetDTObjectIDArray:
                    PrListOfObjId prListOfObjId = (PrListOfObjId)Marshal.PtrToStructure(pvPropVal, typeof(PrListOfObjId));

                    List<ObjectIdentifierModel> bacnetObjIDList = new List<ObjectIdentifierModel>();

                    if (prListOfObjId.arrayObjID != null)
                    {
                        ListOfObjId id = (ListOfObjId)Marshal.PtrToStructure(prListOfObjId.arrayObjID, typeof(ListOfObjId));

                        ObjectIdentifierModel objectIdentifierModel = new ObjectIdentifierModel();
                        objectIdentifierModel.InstanceNumber = id.stObjectID.objId;
                        objectIdentifierModel.ObjectType = id.stObjectID.objectType;

                        bacnetObjIDList.Add(objectIdentifierModel);

                        while (id.next != IntPtr.Zero)
                        {
                            id = (ListOfObjId)Marshal.PtrToStructure(id.next, typeof(ListOfObjId));

                            objectIdentifierModel = new ObjectIdentifierModel();
                            objectIdentifierModel.ObjectType = id.stObjectID.objectType;
                            objectIdentifierModel.InstanceNumber = id.stObjectID.objId;
                            bacnetObjIDList.Add(objectIdentifierModel);
                        }
                    }

                    responseConverted = bacnetObjIDList;
                    break;
                case BacnetDataType.BacnetDTBitString:
                    PrBacnetBitStr prBacnetBitStr = (PrBacnetBitStr)Marshal.PtrToStructure(pvPropVal, typeof(PrBacnetBitStr));

                    switch (propertyId)
                    {
                        case BacnetPropertyID.PropStatusFlags:
                        case BacnetPropertyID.PropMemberStatusFlags:
                            StatusFlagsModel statusFlagsModel = GetStatusFlagsBits(prBacnetBitStr.stBitStr);
                            responseConverted = statusFlagsModel;
                            break;
                        case BacnetPropertyID.PropEventEnable:
                        case BacnetPropertyID.PropAckedTransitions:
                        case BacnetPropertyID.PropAckRequired:
                            EventTransitionBitsModel eventTransitionBitsModel = GetEventTransitionBits(prBacnetBitStr.stBitStr);
                            responseConverted = eventTransitionBitsModel;
                            break;
                        case BacnetPropertyID.PropLimitEnable:
                            LimitEnableBitsModel limitEnableBitsModel = GetLimitEnableBits(prBacnetBitStr.stBitStr);
                            responseConverted = limitEnableBitsModel;
                            break;
                        default:
                            responseConverted = GetBitString(prBacnetBitStr.stBitStr);
                            break;
                    }

                    break;
                case BacnetDataType.BacnetDTObjectTypeSupported:
                    PrBacnetObjectTypesSupported prBacnetObjectTypesSupported = (PrBacnetObjectTypesSupported)Marshal.PtrToStructure(pvPropVal, typeof(PrBacnetObjectTypesSupported));
                    responseConverted = prBacnetObjectTypesSupported.objectTypesSupported.value;
                    break;
                case BacnetDataType.BacnetDTServicesSupported:
                    PrBacnetServicesSupported prBacnetServicesSupported = (PrBacnetServicesSupported)Marshal.PtrToStructure(pvPropVal, typeof(PrBacnetServicesSupported));
                    responseConverted = prBacnetServicesSupported.serviceSupported.value;
                    break;
                case BacnetDataType.BacnetDTDevObjPropReff:
                    PrBacnetDevObjPropRef prBacnetDevObjPropRef = (PrBacnetDevObjPropRef)Marshal.PtrToStructure(pvPropVal, typeof(PrBacnetDevObjPropRef));
                    BacnetDevObjPropRef bacnetDevObjPropRef = prBacnetDevObjPropRef.stDevObjPropReff;

                    BACnetDevObjPropRefModel bacnetDevObjPropRefModel = new BACnetDevObjPropRefModel();
                    bacnetDevObjPropRefModel.ObjectType = bacnetDevObjPropRef.objectType;
                    bacnetDevObjPropRefModel.ObjId = bacnetDevObjPropRef.objID;
                    bacnetDevObjPropRefModel.PropertyIdentifier = bacnetDevObjPropRef.propertyIdentifier;
                    bacnetDevObjPropRefModel.DeviceIdPresent = bacnetDevObjPropRef.deviceIDPresent;
                    bacnetDevObjPropRefModel.ArrIndxPresent = bacnetDevObjPropRef.arrIndxPresent;

                    if (bacnetDevObjPropRef.deviceIDPresent)
                    {
                        bacnetDevObjPropRefModel.DeviceType = bacnetDevObjPropRef.deviceType;
                        bacnetDevObjPropRefModel.DeviceInstance = bacnetDevObjPropRef.deviceInstace;
                    }
                    if (bacnetDevObjPropRef.arrIndxPresent)
                    {
                        bacnetDevObjPropRefModel.ArrayIndex = bacnetDevObjPropRef.arrayIndex;
                    }

                    responseConverted = bacnetDevObjPropRefModel;
                    break;
                case BacnetDataType.BacnetDTDevObjPropReffList:
                    PrListOfBacnetDevObjPropRef prListOfBacnetDevObjPropRef = (PrListOfBacnetDevObjPropRef)Marshal.PtrToStructure(pvPropVal, typeof(PrListOfBacnetDevObjPropRef));

                    List<BACnetDevObjPropRefModel> bacnetDevObjPropRefList = new List<BACnetDevObjPropRefModel>();
                    IntPtr bacnetDevObjPropRefNextPointer = prListOfBacnetDevObjPropRef.listOfBACnetDevObjPropReff;

                    while (bacnetDevObjPropRefNextPointer != IntPtr.Zero)
                    {
                        ListOfBacnetDevObjPropRef listOfBacnetDevObjPropRef = (ListOfBacnetDevObjPropRef)Marshal.PtrToStructure(bacnetDevObjPropRefNextPointer, typeof(ListOfBacnetDevObjPropRef));

                        BACnetDevObjPropRefModel bacnetDeviceObjPropRefModel = new BACnetDevObjPropRefModel();
                        bacnetDeviceObjPropRefModel.ObjectType = listOfBacnetDevObjPropRef.stDevObjPropRef.objectType;
                        bacnetDeviceObjPropRefModel.ObjId = listOfBacnetDevObjPropRef.stDevObjPropRef.objID;
                        bacnetDeviceObjPropRefModel.PropertyIdentifier = listOfBacnetDevObjPropRef.stDevObjPropRef.propertyIdentifier;
                        bacnetDeviceObjPropRefModel.DeviceIdPresent = listOfBacnetDevObjPropRef.stDevObjPropRef.deviceIDPresent;
                        bacnetDeviceObjPropRefModel.ArrIndxPresent = listOfBacnetDevObjPropRef.stDevObjPropRef.arrIndxPresent;

                        if (listOfBacnetDevObjPropRef.stDevObjPropRef.deviceIDPresent)
                        {
                            bacnetDeviceObjPropRefModel.DeviceType = listOfBacnetDevObjPropRef.stDevObjPropRef.deviceType;
                            bacnetDeviceObjPropRefModel.DeviceInstance = listOfBacnetDevObjPropRef.stDevObjPropRef.deviceInstace;
                        }

                        if (listOfBacnetDevObjPropRef.stDevObjPropRef.arrIndxPresent)
                        {
                            bacnetDeviceObjPropRefModel.ArrayIndex = listOfBacnetDevObjPropRef.stDevObjPropRef.arrayIndex;
                        }

                        bacnetDevObjPropRefList.Add(bacnetDeviceObjPropRefModel);

                        bacnetDevObjPropRefNextPointer = listOfBacnetDevObjPropRef.next;
                    }

                    responseConverted = bacnetDevObjPropRefList;
                    break;
                case BacnetDataType.BacnetDTLogBufferTrendlog:
                    PrListOfBacnetLogRecord prListOfBacnetLogRecord = (PrListOfBacnetLogRecord)Marshal.PtrToStructure(pvPropVal, typeof(PrListOfBacnetLogRecord));
                    ListOfBacnetLogRecord listOfBacnetLogRecord = (ListOfBacnetLogRecord)Marshal.PtrToStructure(prListOfBacnetLogRecord.logRecord, typeof(ListOfBacnetLogRecord));
                    IntPtr nextLogRecordPointer = IntPtr.Zero;
                    List<BacnetLogRecordModel> bacnetLogRecordModelList = new List<BacnetLogRecordModel>();

                    do
                    {
                        BacnetLogRecordModel bacnetLogRecordModel = new BacnetLogRecordModel();
                        bacnetLogRecordModel.SequenceNo = listOfBacnetLogRecord.sequenceNo;
                        bacnetLogRecordModel.LogRecord = new LogRecordModel();
                        bacnetLogRecordModel.LogRecord.TimeStamp = new BacnetDateTimeModel();
                        bacnetLogRecordModel.LogRecord.TimeStamp.Date = new BacnetDateModel();
                        bacnetLogRecordModel.LogRecord.TimeStamp.Date.Day = listOfBacnetLogRecord.bacnetLogRecord.m_stTimeStamp.date.day;
                        bacnetLogRecordModel.LogRecord.TimeStamp.Date.Month = listOfBacnetLogRecord.bacnetLogRecord.m_stTimeStamp.date.month;
                        bacnetLogRecordModel.LogRecord.TimeStamp.Date.Year = listOfBacnetLogRecord.bacnetLogRecord.m_stTimeStamp.date.year;
                        bacnetLogRecordModel.LogRecord.TimeStamp.Date.WeekDay = listOfBacnetLogRecord.bacnetLogRecord.m_stTimeStamp.date.weekDay;

                        bacnetLogRecordModel.LogRecord.TimeStamp.Time = new BacnetTimeModel();
                        bacnetLogRecordModel.LogRecord.TimeStamp.Time.Hour = listOfBacnetLogRecord.bacnetLogRecord.m_stTimeStamp.time.hour;
                        bacnetLogRecordModel.LogRecord.TimeStamp.Time.Min = listOfBacnetLogRecord.bacnetLogRecord.m_stTimeStamp.time.min;
                        bacnetLogRecordModel.LogRecord.TimeStamp.Time.Sec = listOfBacnetLogRecord.bacnetLogRecord.m_stTimeStamp.time.sec;
                        bacnetLogRecordModel.LogRecord.TimeStamp.Time.Hundredths = listOfBacnetLogRecord.bacnetLogRecord.m_stTimeStamp.time.hundredths;

                        int bitStringLen = (int)(listOfBacnetLogRecord.bacnetLogRecord.stStatusFlag.byteCnt);
                        StatusFlagsModel statusFlagsMod = new StatusFlagsModel();

                        if (bitStringLen > 0)
                        {
                            byte[] bitStrBytes = new byte[bitStringLen];
                            Buffer.BlockCopy(listOfBacnetLogRecord.bacnetLogRecord.stStatusFlag.transBits, 0, bitStrBytes, 0, bitStringLen);

                            byte statusFlag = bitStrBytes[0];

                            statusFlagsMod.InAlarm = CommanHelper.GetBit(statusFlag, 7);
                            statusFlagsMod.Fault = CommanHelper.GetBit(statusFlag, 6);
                            statusFlagsMod.Overridden = CommanHelper.GetBit(statusFlag, 5);
                            statusFlagsMod.OutOfService = CommanHelper.GetBit(statusFlag, 4);
                        }

                        bacnetLogRecordModel.LogRecord.StatusFlag = statusFlagsMod;
                        bacnetLogRecordModel.LogRecord.LogDatum = new LogDatumModel();
                        bacnetLogRecordModel.LogRecord.LogDatum.TagType = (byte)listOfBacnetLogRecord.bacnetLogRecord.logDatum.tagType;

                        listOfBacnetLogRecord = DecodeLogUsingTagType(listOfBacnetLogRecord, bacnetLogRecordModel);

                        bacnetLogRecordModelList.Add(bacnetLogRecordModel);
                        nextLogRecordPointer = listOfBacnetLogRecord.next;

                        if (nextLogRecordPointer != IntPtr.Zero)
                        {
                            listOfBacnetLogRecord = (ListOfBacnetLogRecord)Marshal.PtrToStructure(nextLogRecordPointer, typeof(ListOfBacnetLogRecord));
                        }

                    } while (nextLogRecordPointer != IntPtr.Zero);

                    responseConverted = bacnetLogRecordModelList;
                    break;
                case BacnetDataType.BacnetDTDevObjPropReffArray:
                    PrListOfBacnetDevObjPropRef prArrayOfBacnetDevObjPropRef = (PrListOfBacnetDevObjPropRef)Marshal.PtrToStructure(pvPropVal, typeof(PrListOfBacnetDevObjPropRef));

                    List<BACnetDevObjPropRefModel> bacnetDevObjPropRefArray = new List<BACnetDevObjPropRefModel>();
                    IntPtr bacnetDevObjPropRefArrayNextPointer = prArrayOfBacnetDevObjPropRef.listOfBACnetDevObjPropReff;

                    while (bacnetDevObjPropRefArrayNextPointer != IntPtr.Zero)
                    {
                        ListOfBacnetDevObjPropRef listOfBacnetDevObjPropRef = (ListOfBacnetDevObjPropRef)Marshal.PtrToStructure(bacnetDevObjPropRefArrayNextPointer, typeof(ListOfBacnetDevObjPropRef));

                        BACnetDevObjPropRefModel bacnetDeviceObjPropRefModel = new BACnetDevObjPropRefModel();
                        bacnetDeviceObjPropRefModel.ObjectType = listOfBacnetDevObjPropRef.stDevObjPropRef.objectType;
                        bacnetDeviceObjPropRefModel.ObjId = listOfBacnetDevObjPropRef.stDevObjPropRef.objID;
                        bacnetDeviceObjPropRefModel.PropertyIdentifier = listOfBacnetDevObjPropRef.stDevObjPropRef.propertyIdentifier;
                        bacnetDeviceObjPropRefModel.DeviceIdPresent = listOfBacnetDevObjPropRef.stDevObjPropRef.deviceIDPresent;
                        bacnetDeviceObjPropRefModel.ArrIndxPresent = listOfBacnetDevObjPropRef.stDevObjPropRef.arrIndxPresent;

                        if (listOfBacnetDevObjPropRef.stDevObjPropRef.deviceIDPresent)
                        {
                            bacnetDeviceObjPropRefModel.DeviceType = listOfBacnetDevObjPropRef.stDevObjPropRef.deviceType;
                            bacnetDeviceObjPropRefModel.DeviceInstance = listOfBacnetDevObjPropRef.stDevObjPropRef.deviceInstace;
                        }

                        if (listOfBacnetDevObjPropRef.stDevObjPropRef.arrIndxPresent)
                        {
                            bacnetDeviceObjPropRefModel.ArrayIndex = listOfBacnetDevObjPropRef.stDevObjPropRef.arrayIndex;
                        }

                        bacnetDevObjPropRefArray.Add(bacnetDeviceObjPropRefModel);

                        bacnetDevObjPropRefArrayNextPointer = listOfBacnetDevObjPropRef.next;
                    }

                    responseConverted = bacnetDevObjPropRefArray;
                    break;
                case BacnetDataType.BacnetDTLogBufferTLM:
                    PrListOfBacnetLogMultipleRecord prListOfBacnetLogMultipleRecord = (PrListOfBacnetLogMultipleRecord)Marshal.PtrToStructure(pvPropVal, typeof(PrListOfBacnetLogMultipleRecord));
                    List<BacnetLogMultipleRecordModel> bacnetLogMultipleRecordModelList = new List<BacnetLogMultipleRecordModel>();
                    IntPtr bacnetLogMultipleRecordPointer = prListOfBacnetLogMultipleRecord.logMultipleRecord;

                    while (bacnetLogMultipleRecordPointer != IntPtr.Zero)
                    {
                        ListOfBacnetLogMultipleRecord listOfBacnetLogMultipleRecord = (ListOfBacnetLogMultipleRecord)Marshal.PtrToStructure(bacnetLogMultipleRecordPointer, typeof(ListOfBacnetLogMultipleRecord)); ;
                        BacnetLogMultipleRecordModel bacnetLogMultipleRecordModel = new BacnetLogMultipleRecordModel();

                        bacnetLogMultipleRecordModel.SequenceNo = listOfBacnetLogMultipleRecord.sequenceNo;
                        bacnetLogMultipleRecordModel.TimeStamp = new BacnetDateTimeModel();
                        bacnetLogMultipleRecordModel.TimeStamp.Date = new BacnetDateModel();
                        bacnetLogMultipleRecordModel.TimeStamp.Date.Day = listOfBacnetLogMultipleRecord.stTimeStamp.date.day;
                        bacnetLogMultipleRecordModel.TimeStamp.Date.Month = listOfBacnetLogMultipleRecord.stTimeStamp.date.month;
                        bacnetLogMultipleRecordModel.TimeStamp.Date.Year = listOfBacnetLogMultipleRecord.stTimeStamp.date.year;
                        bacnetLogMultipleRecordModel.TimeStamp.Date.WeekDay = listOfBacnetLogMultipleRecord.stTimeStamp.date.weekDay;

                        bacnetLogMultipleRecordModel.TimeStamp.Time = new BacnetTimeModel();
                        bacnetLogMultipleRecordModel.TimeStamp.Time.Hour = listOfBacnetLogMultipleRecord.stTimeStamp.time.hour;
                        bacnetLogMultipleRecordModel.TimeStamp.Time.Min = listOfBacnetLogMultipleRecord.stTimeStamp.time.min;
                        bacnetLogMultipleRecordModel.TimeStamp.Time.Sec = listOfBacnetLogMultipleRecord.stTimeStamp.time.sec;
                        bacnetLogMultipleRecordModel.TimeStamp.Time.Hundredths = listOfBacnetLogMultipleRecord.stTimeStamp.time.hundredths;

                        bacnetLogMultipleRecordModel.BacnetLogData = new List<BacnetLogDataModel>();
                        IntPtr bacnetLogDataPointer = IntPtr.Zero;

                        BacnetLogData bacnetLogData = listOfBacnetLogMultipleRecord.bacnetLogData;

                        do
                        {
                            BacnetLogDataModel bacnetLogDataModel = new BacnetLogDataModel();
                            bacnetLogDataModel.TagType = (byte)bacnetLogData.tagType;

                            switch (bacnetLogData.tagType)
                            {
                                case (byte)BacnetDataType.BacnetDTReal:
                                    bacnetLogDataModel.BacnetLogData = StructureConverter.ByteArrayToStructure<float>(bacnetLogData.bacnetLogDataUnion);
                                    break;
                                case (byte)BacnetDataType.BacnetDTUnsigned:
                                case (byte)BacnetDataType.BacnetDTUnsigned16:
                                case (byte)BacnetDataType.BacnetDTUnsigned32:
                                    bacnetLogDataModel.BacnetLogData = StructureConverter.ByteArrayToStructure<uint>(bacnetLogData.bacnetLogDataUnion);
                                    break;
                                case (byte)BacnetDataType.BacnetDTEnum:
                                case (byte)BacnetDataType.BacnetDTEnumNew:
                                    bacnetLogDataModel.BacnetLogData = StructureConverter.ByteArrayToStructure<uint>(bacnetLogData.bacnetLogDataUnion);
                                    break;
                                default:
                                    break;
                            }

                            bacnetLogMultipleRecordModel.BacnetLogData.Add(bacnetLogDataModel);
                            bacnetLogDataPointer = bacnetLogData.next;

                            if (bacnetLogDataPointer != IntPtr.Zero)
                                bacnetLogData = (BacnetLogData)Marshal.PtrToStructure(bacnetLogDataPointer, typeof(BacnetLogData));

                        } while (bacnetLogDataPointer != IntPtr.Zero);

                        bacnetLogMultipleRecordModelList.Add(bacnetLogMultipleRecordModel);
                        bacnetLogMultipleRecordPointer = listOfBacnetLogMultipleRecord.next;
                    }

                    responseConverted = bacnetLogMultipleRecordModelList;
                    break;
                case BacnetDataType.BacnetDTDailySchedule:
                    PrBacnetDailySchedule prBacnetDailySchedule = (PrBacnetDailySchedule)Marshal.PtrToStructure(pvPropVal, typeof(PrBacnetDailySchedule));
                    BacnetTimeValue bacnetTimeValue = prBacnetDailySchedule.bacnetTimeVal;
                    List<BacnetTimeValueModel> bacnetTimeValueList = new List<BacnetTimeValueModel>();
                    IntPtr bacnetTimeValuePointer = IntPtr.Zero;

                    if (bacnetTimeValue.isUsed)
                    {
                        do
                        {
                            BacnetTimeValueModel bacnetTimeValueModel = new BacnetTimeValueModel();
                            bacnetTimeValueModel.Time = new BacnetTimeModel();
                            bacnetTimeValueModel.Time.Hour = bacnetTimeValue.stTime.hour;
                            bacnetTimeValueModel.Time.Min = bacnetTimeValue.stTime.min;
                            bacnetTimeValueModel.Time.Sec = bacnetTimeValue.stTime.sec;
                            bacnetTimeValueModel.Time.Hundredths = bacnetTimeValue.stTime.hundredths;

                            IntPtr bacnetValPointer = IntPtr.Zero;
                            BacnetPropertyValue bacnetPropertyValue = bacnetTimeValue.bacnetPropVal;
                            List<BacnetValueModel> BacnetValModelList = new List<BacnetValueModel>();

                            do
                            {
                                BacnetValueModel bacnetValueModel = new BacnetValueModel();
                                bacnetValueModel.TagType = bacnetPropertyValue.tagType;
                                bacnetValueModel.Value = BacnetTimeValueDecoding(bacnetPropertyValue);

                                BacnetValModelList.Add(bacnetValueModel);
                                bacnetValPointer = bacnetPropertyValue.nextPropVal;

                                if (bacnetValPointer != IntPtr.Zero)
                                    bacnetPropertyValue = (BacnetPropertyValue)Marshal.PtrToStructure(bacnetValPointer, typeof(BacnetPropertyValue));

                            } while (bacnetValPointer != IntPtr.Zero);

                            bacnetTimeValueModel.Values = BacnetValModelList;

                            bacnetTimeValueList.Add(bacnetTimeValueModel);
                            bacnetTimeValuePointer = bacnetTimeValue.next;

                            if (bacnetTimeValuePointer != IntPtr.Zero)
                                bacnetTimeValue = (BacnetTimeValue)Marshal.PtrToStructure(bacnetTimeValuePointer, typeof(BacnetTimeValue));

                        } while (bacnetTimeValuePointer != IntPtr.Zero);
                    }

                    responseConverted = bacnetTimeValueList;
                    break;
                case BacnetDataType.BacnetDTDailyScheduleArray:
                    PrListOfBacnetDailySchedule prListOfBacnetDailySchedule = (PrListOfBacnetDailySchedule)Marshal.PtrToStructure(pvPropVal, typeof(PrListOfBacnetDailySchedule));
                    List<BacnetTimeValueArrayModel> bacnetTimeValueListArray = new List<BacnetTimeValueArrayModel>();

                    for (int dailyScheduleArrayCount = 0; dailyScheduleArrayCount < BacDelDef.BacnetArrayOfSeven; dailyScheduleArrayCount++)
                    {
                        BacnetTimeValue bacnetTmValue = prListOfBacnetDailySchedule.timeValue[dailyScheduleArrayCount];
                        IntPtr bacnetTmValuePointer = IntPtr.Zero;
                        BacnetTimeValueArrayModel bacnetTimeValueArrayModel = new BacnetTimeValueArrayModel();

                        List<BacnetTimeValueModel> BacnetTimeValueModelList = new List<BacnetTimeValueModel>();

                        bacnetTimeValueArrayModel.DayCount = dailyScheduleArrayCount + 1;

                        if (bacnetTmValue.isUsed)
                        {
                            do
                            {
                                BacnetTimeValueModel bacnetTimeValueModel = new BacnetTimeValueModel();
                                bacnetTimeValueModel.Time = new BacnetTimeModel();
                                bacnetTimeValueModel.Time.Hour = bacnetTmValue.stTime.hour;
                                bacnetTimeValueModel.Time.Min = bacnetTmValue.stTime.min;
                                bacnetTimeValueModel.Time.Sec = bacnetTmValue.stTime.sec;
                                bacnetTimeValueModel.Time.Hundredths = bacnetTmValue.stTime.hundredths;

                                IntPtr bacnetValuePointer = IntPtr.Zero;
                                BacnetPropertyValue bacnetPropertyValue = bacnetTmValue.bacnetPropVal;
                                List<BacnetValueModel> BacnetValueModelList = new List<BacnetValueModel>();

                                do
                                {
                                    BacnetValueModel bacnetValueModel = new BacnetValueModel();
                                    bacnetValueModel.TagType = bacnetPropertyValue.tagType;
                                    bacnetValueModel.Value = BacnetTimeValueDecoding(bacnetPropertyValue);

                                    BacnetValueModelList.Add(bacnetValueModel);
                                    bacnetValuePointer = bacnetPropertyValue.nextPropVal;

                                    if (bacnetValuePointer != IntPtr.Zero)
                                        bacnetPropertyValue = (BacnetPropertyValue)Marshal.PtrToStructure(bacnetValuePointer, typeof(BacnetPropertyValue));

                                } while (bacnetValuePointer != IntPtr.Zero);

                                bacnetTimeValueModel.Values = BacnetValueModelList;
                                BacnetTimeValueModelList.Add(bacnetTimeValueModel);
                                bacnetTmValuePointer = bacnetTmValue.next;

                                if (bacnetTmValuePointer != IntPtr.Zero)
                                    bacnetTmValue = (BacnetTimeValue)Marshal.PtrToStructure(bacnetTmValuePointer, typeof(BacnetTimeValue));
                            } while (bacnetTmValuePointer != IntPtr.Zero);
                        }

                        bacnetTimeValueArrayModel.BacnetTimeValueList = BacnetTimeValueModelList;
                        bacnetTimeValueListArray.Add(bacnetTimeValueArrayModel);
                    }

                    responseConverted = bacnetTimeValueListArray;
                    break;
                case BacnetDataType.BacnetDTSpecialEventArray:
                case BacnetDataType.BacnetDTSpecialEvent:
                    PrListOfBacnetSpecialEvent prListOfBacnetSpecialEvent = (PrListOfBacnetSpecialEvent)Marshal.PtrToStructure(pvPropVal, typeof(PrListOfBacnetSpecialEvent));
                    IntPtr specialEventPointer = prListOfBacnetSpecialEvent.splEvent;
                    List<BacnetSpecialEventModel> bacnetSpecialEventModelList = new List<BacnetSpecialEventModel>();

                    while (specialEventPointer != IntPtr.Zero)
                    {
                        BacnetSpecialEventModel bacnetSpecialEventModel = new BacnetSpecialEventModel();

                        ListOfSpecialEvent listOfSpecialEvent = (ListOfSpecialEvent)Marshal.PtrToStructure(specialEventPointer, typeof(ListOfSpecialEvent));
                        bacnetSpecialEventModel.EventPriority = listOfSpecialEvent.bacnetSpecialEvt.eventPriority;
                        bacnetSpecialEventModel.StatusCalendar = listOfSpecialEvent.bacnetSpecialEvt.statusCalendar;

                        BacnetTimeValue bacnetTimeVal = listOfSpecialEvent.bacnetSpecialEvt.stListOfTimeValues;

                        if (listOfSpecialEvent.bacnetSpecialEvt.statusCalendar == CalendarEntryStatus.StatusCalReff)
                        {
                            ObjectIdentifierModel objectIdentifierMod = new ObjectIdentifierModel();
                            objectIdentifierMod.InstanceNumber = listOfSpecialEvent.bacnetSpecialEvt.bacnetSpecialEventUnion.calReff.objId;
                            objectIdentifierMod.ObjectType = listOfSpecialEvent.bacnetSpecialEvt.bacnetSpecialEventUnion.calReff.objectType;

                            bacnetSpecialEventModel.CalenderReference = objectIdentifierMod;
                        }
                        else
                        {
                            if (listOfSpecialEvent.bacnetSpecialEvt.statusCalendar == CalendarEntryStatus.StatusDate)
                            {
                                BacnetDateModel bacnetDateModel = new BacnetDateModel();
                                bacnetDateModel.Day = listOfSpecialEvent.bacnetSpecialEvt.bacnetSpecialEventUnion.stDate.day;
                                bacnetDateModel.Month = listOfSpecialEvent.bacnetSpecialEvt.bacnetSpecialEventUnion.stDate.month;
                                bacnetDateModel.Year = listOfSpecialEvent.bacnetSpecialEvt.bacnetSpecialEventUnion.stDate.year;
                                bacnetDateModel.WeekDay = listOfSpecialEvent.bacnetSpecialEvt.bacnetSpecialEventUnion.stDate.weekDay;
                                bacnetSpecialEventModel.CalenderReference = bacnetDateModel;
                            }
                            else if (listOfSpecialEvent.bacnetSpecialEvt.statusCalendar == CalendarEntryStatus.StatusDateRange)
                            {
                                BacnetDateRangeModel bacnetDateRangeModel = new BacnetDateRangeModel();
                                bacnetDateRangeModel.StartDate.Day = listOfSpecialEvent.bacnetSpecialEvt.bacnetSpecialEventUnion.stDateRange.stStartDate.day;
                                bacnetDateRangeModel.StartDate.Month = listOfSpecialEvent.bacnetSpecialEvt.bacnetSpecialEventUnion.stDateRange.stStartDate.month;
                                bacnetDateRangeModel.StartDate.Year = listOfSpecialEvent.bacnetSpecialEvt.bacnetSpecialEventUnion.stDateRange.stStartDate.year;
                                bacnetDateRangeModel.StartDate.WeekDay = listOfSpecialEvent.bacnetSpecialEvt.bacnetSpecialEventUnion.stDateRange.stStartDate.weekDay;

                                bacnetDateRangeModel.EndDate.Day = listOfSpecialEvent.bacnetSpecialEvt.bacnetSpecialEventUnion.stDateRange.stEndDate.day;
                                bacnetDateRangeModel.EndDate.Month = listOfSpecialEvent.bacnetSpecialEvt.bacnetSpecialEventUnion.stDateRange.stEndDate.month;
                                bacnetDateRangeModel.EndDate.Year = listOfSpecialEvent.bacnetSpecialEvt.bacnetSpecialEventUnion.stDateRange.stEndDate.year;
                                bacnetDateRangeModel.EndDate.WeekDay = listOfSpecialEvent.bacnetSpecialEvt.bacnetSpecialEventUnion.stDateRange.stEndDate.weekDay;

                                bacnetSpecialEventModel.CalenderReference = bacnetDateRangeModel;
                            }
                            else if (listOfSpecialEvent.bacnetSpecialEvt.statusCalendar == CalendarEntryStatus.StatusWeekNDay)
                            {
                                WeekNDayModel weekNDayModel = new WeekNDayModel();
                                weekNDayModel.Month = listOfSpecialEvent.bacnetSpecialEvt.bacnetSpecialEventUnion.stWeekNDay.month;
                                weekNDayModel.WeekNDay = listOfSpecialEvent.bacnetSpecialEvt.bacnetSpecialEventUnion.stWeekNDay.weekNDay;
                                weekNDayModel.WeekOfMonth = listOfSpecialEvent.bacnetSpecialEvt.bacnetSpecialEventUnion.stWeekNDay.weekOfMonth;

                                bacnetSpecialEventModel.CalenderReference = weekNDayModel;
                            }
                        }


                        IntPtr bacnetTimeValuePtr = IntPtr.Zero;
                        List<BacnetTimeValueModel> BacnetTimeValueList = new List<BacnetTimeValueModel>();

                        if (bacnetTimeVal.isUsed)
                        {
                            do
                            {
                                BacnetTimeValueModel bacnetTimeValueModel = new BacnetTimeValueModel();
                                bacnetTimeValueModel.Time = new BacnetTimeModel();
                                bacnetTimeValueModel.Time.Hour = bacnetTimeVal.stTime.hour;
                                bacnetTimeValueModel.Time.Min = bacnetTimeVal.stTime.min;
                                bacnetTimeValueModel.Time.Sec = bacnetTimeVal.stTime.sec;
                                bacnetTimeValueModel.Time.Hundredths = bacnetTimeVal.stTime.hundredths;

                                IntPtr bacnetValPointer = IntPtr.Zero;
                                BacnetPropertyValue bacnetPropertyValue = bacnetTimeVal.bacnetPropVal;
                                List<BacnetValueModel> BacnetValueModelList = new List<BacnetValueModel>();

                                do
                                {
                                    BacnetValueModel bacnetValueModel = new BacnetValueModel();
                                    bacnetValueModel.TagType = bacnetPropertyValue.tagType;
                                    bacnetValueModel.Value = BacnetTimeValueDecoding(bacnetPropertyValue);

                                    BacnetValueModelList.Add(bacnetValueModel);
                                    bacnetValPointer = bacnetPropertyValue.nextPropVal;

                                    if (bacnetValPointer != IntPtr.Zero)
                                        bacnetPropertyValue = (BacnetPropertyValue)Marshal.PtrToStructure(bacnetValPointer, typeof(BacnetPropertyValue));

                                } while (bacnetValPointer != IntPtr.Zero);

                                bacnetTimeValueModel.Values = BacnetValueModelList;

                                BacnetTimeValueList.Add(bacnetTimeValueModel);
                                bacnetTimeValuePtr = bacnetTimeVal.next;

                                if (bacnetTimeValuePtr != IntPtr.Zero)
                                    bacnetTimeVal = (BacnetTimeValue)Marshal.PtrToStructure(bacnetTimeValuePtr, typeof(BacnetTimeValue));

                            } while (bacnetTimeValuePtr != IntPtr.Zero);
                        }

                        bacnetSpecialEventModel.BacnetTimeValue = BacnetTimeValueList;
                        bacnetSpecialEventModelList.Add(bacnetSpecialEventModel);

                        specialEventPointer = listOfSpecialEvent.next;
                    }

                    responseConverted = bacnetSpecialEventModelList;
                    break;
                case BacnetDataType.BacnetDTUnsignedList:
                case BacnetDataType.BacnetDTUnsignedArray:
                    PrListOfUnsigned prListOfUnsigned = (PrListOfUnsigned)Marshal.PtrToStructure(pvPropVal, typeof(PrListOfUnsigned));

                    List<uint> UnsignedList = new List<uint>();
                    IntPtr unsignedPointer = prListOfUnsigned.unsignVal;

                    while (unsignedPointer != IntPtr.Zero)
                    {
                        ListOfUnsigned listOfUnsigned = (ListOfUnsigned)Marshal.PtrToStructure(unsignedPointer, typeof(ListOfUnsigned));
                        UnsignedList.Add(listOfUnsigned.value);

                        unsignedPointer = listOfUnsigned.next;
                    }

                    responseConverted = UnsignedList;
                    break;
                case BacnetDataType.BacnetDTDestinationList:
                    PrListOfBacnetDestination prListOfBacnetDestination = (PrListOfBacnetDestination)Marshal.PtrToStructure(pvPropVal, typeof(PrListOfBacnetDestination));

                    List<BacnetDestinationModel> bacnetDestinationList = new List<BacnetDestinationModel>();
                    IntPtr nxtPointer = prListOfBacnetDestination.ncRecepient;

                    while (nxtPointer != IntPtr.Zero)
                    {
                        ListOfBacnetDestination listOfBacnetDestination = StructureConverter.PtrToStructure<ListOfBacnetDestination>(nxtPointer);

                        BacnetDestinationModel bacnetDestinationModel = new BacnetDestinationModel();
                        bacnetDestinationModel.IssueConfirmedNotification = listOfBacnetDestination.issueConfirmedNotification;
                        bacnetDestinationModel.ProcessID = listOfBacnetDestination.stProcessID;

                        bacnetDestinationModel.FromTime = new BacnetTimeModel();
                        bacnetDestinationModel.FromTime.Hour = listOfBacnetDestination.stFromTime.hour;
                        bacnetDestinationModel.FromTime.Min = listOfBacnetDestination.stFromTime.min;
                        bacnetDestinationModel.FromTime.Sec = listOfBacnetDestination.stFromTime.sec;
                        bacnetDestinationModel.FromTime.Hundredths = listOfBacnetDestination.stFromTime.hundredths;

                        bacnetDestinationModel.ToTime = new BacnetTimeModel();
                        bacnetDestinationModel.ToTime.Hour = listOfBacnetDestination.stToTime.hour;
                        bacnetDestinationModel.ToTime.Min = listOfBacnetDestination.stToTime.min;
                        bacnetDestinationModel.ToTime.Sec = listOfBacnetDestination.stToTime.sec;
                        bacnetDestinationModel.ToTime.Hundredths = listOfBacnetDestination.stToTime.hundredths;

                        bacnetDestinationModel.EventTransitions = GetEventTransitionBits(listOfBacnetDestination.stTransitions);

                        bacnetDestinationModel.DaysOfWeek = GetDaysOfWeekBits(listOfBacnetDestination.stDaysOfWeek);

                        bacnetDestinationModel.BacnetRecipient = new BacnetRecipientModel();
                        bacnetDestinationModel.BacnetRecipient.DestinationType = listOfBacnetDestination.bacnetRecipient.destinationType;

                        if (listOfBacnetDestination.bacnetRecipient.destinationType == DestinationType.DestinationIsDeviceID)
                        {
                            ObjID objID = StructureConverter.ByteArrayToStructure<ObjID>(listOfBacnetDestination.bacnetRecipient.bacnetRecipientUnion);

                            bacnetDestinationModel.BacnetRecipient.ObjectType = objID.ObjectType;
                            bacnetDestinationModel.BacnetRecipient.ObjId = objID.ObjId;
                        }
                        else
                        {
                            BacnetAddress bacnetAddress = StructureConverter.ByteArrayToStructure<BacnetAddress>(listOfBacnetDestination.bacnetRecipient.bacnetRecipientUnion);

                            bacnetDestinationModel.BacnetRecipient.BacnetAddress = new BacnetAddressModel();
                            bacnetDestinationModel.BacnetRecipient.BacnetAddress.NetworkNumber = bacnetAddress.net;

                            if (bacnetAddress.net == 0)
                            {
                                bacnetDestinationModel.BacnetRecipient.BacnetAddress.IPAddress = IpAddressHelper.ConvertBinaryIptoString(bacnetAddress.ipAddrs);
                                bacnetDestinationModel.BacnetRecipient.BacnetAddress.Port = (uint)IpAddressHelper.GetPortFromBinaryIp(bacnetAddress.ipAddrs);
                                bacnetDestinationModel.BacnetRecipient.BacnetAddress.IPAddressLength = bacnetAddress.macLen;
                            }
                            else
                            {
                                bacnetDestinationModel.BacnetRecipient.BacnetAddress.MacAddress = IpAddressHelper.GetMacAddressStringFromByteArray(bacnetAddress.macLen, bacnetAddress.ipAddrs);
                                bacnetDestinationModel.BacnetRecipient.BacnetAddress.MacLength = bacnetAddress.macLen;
                            }
                        }

                        bacnetDestinationList.Add(bacnetDestinationModel);
                        nxtPointer = listOfBacnetDestination.next;
                    }

                    responseConverted = bacnetDestinationList;
                    break;
                case BacnetDataType.BacnetDTClientCOVIncrement:
                    PrBacnetClientCOV prBacnetClientCOV = (PrBacnetClientCOV)Marshal.PtrToStructure(pvPropVal, typeof(PrBacnetClientCOV));
                    BacnetClientCOVIncrementModel bacnetClientCOVIncrementModel = new BacnetClientCOVIncrementModel();

                    if (prBacnetClientCOV.stClientCOV.appTagtype == BacnetApplicationTag.BacnetApplicationTagReal)
                    {
                        bacnetClientCOVIncrementModel.Value = prBacnetClientCOV.stClientCOV.value;
                        bacnetClientCOVIncrementModel.TagType = "Real";
                    }
                    else
                    {
                        bacnetClientCOVIncrementModel.TagType = "Null";
                    }

                    responseConverted = bacnetClientCOVIncrementModel;
                    break;
                case BacnetDataType.BacnetDTDateRange:
                    PrBacnetDateRange prBacnetDateRange = (PrBacnetDateRange)Marshal.PtrToStructure(pvPropVal, typeof(PrBacnetDateRange));
                    BacnetDateRange bacnetDateRange = prBacnetDateRange.stDateRange;

                    BacnetDateRangeModel bacnetDateRangModel = new BacnetDateRangeModel();
                    bacnetDateRangModel.StartDate = new BacnetDateModel();
                    bacnetDateRangModel.StartDate.Day = bacnetDateRange.stStartDate.day;
                    bacnetDateRangModel.StartDate.Month = bacnetDateRange.stStartDate.month;
                    bacnetDateRangModel.StartDate.WeekDay = bacnetDateRange.stStartDate.weekDay;
                    bacnetDateRangModel.StartDate.Year = bacnetDateRange.stStartDate.year;

                    bacnetDateRangModel.EndDate = new BacnetDateModel();
                    bacnetDateRangModel.EndDate.Day = bacnetDateRange.stEndDate.day;
                    bacnetDateRangModel.EndDate.Month = bacnetDateRange.stEndDate.month;
                    bacnetDateRangModel.EndDate.WeekDay = bacnetDateRange.stEndDate.weekDay;
                    bacnetDateRangModel.EndDate.Year = bacnetDateRange.stEndDate.year;

                    responseConverted = bacnetDateRangModel;
                    break;
                case BacnetDataType.BacnetDTSchedulePresentDefault:
                    AnyValue anyValue = (AnyValue)Marshal.PtrToStructure(pvPropVal, typeof(AnyValue));
                    BacnetPropertyValue bacnetPropValue = anyValue.propValue;
                    BacnetValueModel bacnetPropertyValueModel = new BacnetValueModel();

                    bacnetPropertyValueModel = BacnetPropertyValueDecoding(bacnetPropValue);

                    responseConverted = bacnetPropertyValueModel;
                    break;
                case BacnetDataType.BacnetDTDateTime:
                    PrBacnetDateTime prBacnetDateTime = (PrBacnetDateTime)Marshal.PtrToStructure(pvPropVal, typeof(PrBacnetDateTime));
                    BacnetDateTimeModel bacnetDateTimeModel = new BacnetDateTimeModel();
                    bacnetDateTimeModel.Date = new BacnetDateModel();
                    bacnetDateTimeModel.Date.Day = prBacnetDateTime.stDateTime.date.day;
                    bacnetDateTimeModel.Date.Month = prBacnetDateTime.stDateTime.date.month;
                    bacnetDateTimeModel.Date.Year = prBacnetDateTime.stDateTime.date.year;
                    bacnetDateTimeModel.Date.WeekDay = prBacnetDateTime.stDateTime.date.weekDay;

                    bacnetDateTimeModel.Time = new BacnetTimeModel();
                    bacnetDateTimeModel.Time.Hour = prBacnetDateTime.stDateTime.time.hour;
                    bacnetDateTimeModel.Time.Min = prBacnetDateTime.stDateTime.time.min;
                    bacnetDateTimeModel.Time.Sec = prBacnetDateTime.stDateTime.time.sec;
                    bacnetDateTimeModel.Time.Hundredths = prBacnetDateTime.stDateTime.time.hundredths;

                    responseConverted = bacnetDateTimeModel;
                    break;
                case BacnetDataType.BacnetDTNotificationPriority:
                    PrBacnetNotifyPriority prBacnetNotifyPriority = (PrBacnetNotifyPriority)Marshal.PtrToStructure(pvPropVal, typeof(PrBacnetNotifyPriority));

                    uint[] valueArray = new uint[3];
                    valueArray[0] = prBacnetNotifyPriority.value[0];
                    valueArray[1] = prBacnetNotifyPriority.value[1];
                    valueArray[2] = prBacnetNotifyPriority.value[2];

                    responseConverted = valueArray;
                    break;
                case BacnetDataType.BacnetDTUnsigned8:
                    PrBacnetUnsigned8 prBacnetUnsigned8 = (PrBacnetUnsigned8)Marshal.PtrToStructure(pvPropVal, typeof(PrBacnetUnsigned8));
                    responseConverted = prBacnetUnsigned8.val;
                    break;
                case BacnetDataType.BacnetDTMax:
                case BacnetDataType.BacnetDTNull:
                case BacnetDataType.BacnetDTEmpty:
                    responseConverted = null;
                    break;
                case BacnetDataType.BacnetDTBacnetDevstat:
                    PrBacnetDevStatus prBacnetDevStatus = (PrBacnetDevStatus)Marshal.PtrToStructure(pvPropVal, typeof(PrBacnetDevStatus));
                    responseConverted = prBacnetDevStatus.deviceStatus;
                    break;
                case BacnetDataType.BacnetDTTime:
                    PrBacnetTime prBacnetTime = (PrBacnetTime)Marshal.PtrToStructure(pvPropVal, typeof(PrBacnetTime));
                    BacnetTimeModel bacnetTimeModel = new BacnetTimeModel();
                    bacnetTimeModel.Hour = prBacnetTime.timeVal.hour;
                    bacnetTimeModel.Min = prBacnetTime.timeVal.min;
                    bacnetTimeModel.Sec = prBacnetTime.timeVal.sec;
                    bacnetTimeModel.Hundredths = prBacnetTime.timeVal.hundredths;

                    responseConverted = bacnetTimeModel;
                    break;
                case BacnetDataType.BacnetDTDate:
                    PrBacnetDate prBacnetDate = (PrBacnetDate)Marshal.PtrToStructure(pvPropVal, typeof(PrBacnetDate));
                    BacnetDateModel bacnetDateMod = new BacnetDateModel();
                    bacnetDateMod.Year = prBacnetDate.dateVal.year;
                    bacnetDateMod.Month = prBacnetDate.dateVal.month;
                    bacnetDateMod.Day = prBacnetDate.dateVal.day;
                    bacnetDateMod.WeekDay = prBacnetDate.dateVal.weekDay;

                    responseConverted = bacnetDateMod;
                    break;
                case BacnetDataType.BacnetDTOctetString:
                    PrBacnetOctetStr prBacnetOctetStr = (PrBacnetOctetStr)Marshal.PtrToStructure(pvPropVal, typeof(PrBacnetOctetStr));
                    string octetString = GetOctetString(prBacnetOctetStr.stOctetString);

                    responseConverted = octetString;
                    break;
                case BacnetDataType.BacnetDTBitStringNew:
                    PrBacnetBITStr prBacnetBITStr = (PrBacnetBITStr)Marshal.PtrToStructure(pvPropVal, typeof(PrBacnetBITStr));
                    string bitString = GetBitStringNew(prBacnetBITStr.stBitString);

                    responseConverted = bitString;
                    break;
                case BacnetDataType.BacnetDTChannelValue:
                    PrBACnetChannelValue prBACnetChannelValue = (PrBACnetChannelValue)Marshal.PtrToStructure(pvPropVal, typeof(PrBACnetChannelValue));
                    ChannelValueModel channelValueModel = new ChannelValueModel();

                    channelValueModel.DataType = prBACnetChannelValue.channelValue.dataType;
                    channelValueModel.TagType = prBACnetChannelValue.channelValue.appTag;

                    if (prBACnetChannelValue.channelValue.dataType != BacnetDataType.BacnetDTLightingCommand)
                    {
                        BacnetValueModel bacnetPropertyValModel = new BacnetValueModel();
                        BacnetPropertyValue bacnetPropertyValue = StructureConverter.ByteArrayToStructure<BacnetPropertyValue>(prBACnetChannelValue.channelValue.channelValueUnion);
                        bacnetPropertyValModel = BacnetPropertyValueDecoding(bacnetPropertyValue);
                        channelValueModel.Value = bacnetPropertyValModel;
                    }

                    responseConverted = channelValueModel;
                    break;
                case BacnetDataType.BacnetDTEventParameters:
                    PrBacnetEventParameter prBacnetEventParameter = (PrBacnetEventParameter)Marshal.PtrToStructure(pvPropVal, typeof(PrBacnetEventParameter));
                    responseConverted = GetEventParameters(prBacnetEventParameter.bacnetEventParam);
                    break;
                case BacnetDataType.BacnetDTDateList:
                    PrListOfBacnetCalendarEntry bacnetCalendarEntry = (PrListOfBacnetCalendarEntry)Marshal.PtrToStructure(pvPropVal, typeof(PrListOfBacnetCalendarEntry));
                    IntPtr nxtPtr = bacnetCalendarEntry.listOfCalendar;
                    List<BacnetCalendarEntryModel> dateList = new List<BacnetCalendarEntryModel>();
                    while (nxtPtr != IntPtr.Zero)
                    {
                        ListOfBacnetCalendarEntry listOfBacnetDestination = StructureConverter.PtrToStructure<ListOfBacnetCalendarEntry>(nxtPtr);
                        BacnetCalendarEntryModel calendarEntryModel = new BacnetCalendarEntryModel();
                        calendarEntryModel.StatusCalendar = listOfBacnetDestination.statusCalendar;
                        if (listOfBacnetDestination.statusCalendar == CalendarEntryStatus.StatusDate)
                        {
                            BacnetDateModel dateModel = new BacnetDateModel();
                            dateModel.Day = listOfBacnetDestination.bacnetCalEntruUnion.stDate.day;
                            dateModel.Month = listOfBacnetDestination.bacnetCalEntruUnion.stDate.month;
                            dateModel.WeekDay = listOfBacnetDestination.bacnetCalEntruUnion.stDate.weekDay;
                            dateModel.Year = listOfBacnetDestination.bacnetCalEntruUnion.stDate.year;
                            calendarEntryModel.BacnetCalenderEntry = dateModel;
                        }
                        else if( listOfBacnetDestination.statusCalendar == CalendarEntryStatus.StatusDateRange)
                        {
                            BacnetDateRangeModel dateRangeModel = new BacnetDateRangeModel();
                            dateRangeModel.StartDate.Day = listOfBacnetDestination.bacnetCalEntruUnion.stDateRange.stStartDate.day;
                            dateRangeModel.StartDate.Month = listOfBacnetDestination.bacnetCalEntruUnion.stDateRange.stStartDate.month;
                            dateRangeModel.StartDate.WeekDay = listOfBacnetDestination.bacnetCalEntruUnion.stDateRange.stStartDate.weekDay;
                            dateRangeModel.StartDate.Year = listOfBacnetDestination.bacnetCalEntruUnion.stDateRange.stStartDate.year;

                            dateRangeModel.EndDate.Day = listOfBacnetDestination.bacnetCalEntruUnion.stDateRange.stEndDate.day;
                            dateRangeModel.EndDate.Month = listOfBacnetDestination.bacnetCalEntruUnion.stDateRange.stEndDate.month;
                            dateRangeModel.EndDate.WeekDay = listOfBacnetDestination.bacnetCalEntruUnion.stDateRange.stEndDate.weekDay;
                            dateRangeModel.EndDate.Year = listOfBacnetDestination.bacnetCalEntruUnion.stDateRange.stEndDate.year;
                            calendarEntryModel.BacnetCalenderEntry = dateRangeModel;
                        }
                        else if(listOfBacnetDestination.statusCalendar == CalendarEntryStatus.StatusWeekNDay)
                        {
                            WeekNDayModel weekNDayModel = new WeekNDayModel();
                            weekNDayModel.Month = listOfBacnetDestination.bacnetCalEntruUnion.stWeekNDay.month;
                            weekNDayModel.WeekNDay = listOfBacnetDestination.bacnetCalEntruUnion.stWeekNDay.weekNDay;
                            weekNDayModel.WeekOfMonth = listOfBacnetDestination.bacnetCalEntruUnion.stWeekNDay.weekOfMonth;

                            calendarEntryModel.BacnetCalenderEntry = weekNDayModel;
                        }
                        dateList.Add(calendarEntryModel);
                        nxtPtr = listOfBacnetDestination.next;
                    }
                    responseConverted = dateList;
                    break;
                default:
                    break;
            }

            return responseConverted;
        }

        private static object BacnetTimeValueDecoding(BacnetPropertyValue bacnetPropertyValue)
        {
            object _value = null;

            switch (bacnetPropertyValue.tagType)
            {
                case (byte)BacnetApplicationTag.BacnetApplicationTagBoolean:
                    _value = StructureConverter.ByteArrayToStructure<byte>(bacnetPropertyValue.BacnetPropertyValueUnion);
                    break;
                case (byte)BacnetApplicationTag.BacnetApplicationTagUnsignedInt:
                    _value = StructureConverter.ByteArrayToStructure<UInt32>(bacnetPropertyValue.BacnetPropertyValueUnion);
                    break;
                case (byte)BacnetApplicationTag.BacnetApplicationTagSignedInt:
                    _value = StructureConverter.ByteArrayToStructure<Int32>(bacnetPropertyValue.BacnetPropertyValueUnion);
                    break;
                case (byte)BacnetApplicationTag.BacnetApplicationTagReal:
                    _value = StructureConverter.ByteArrayToStructure<float>(bacnetPropertyValue.BacnetPropertyValueUnion);
                    break;
                case (byte)BacnetApplicationTag.BacnetApplicationTagDouble:
                    _value = StructureConverter.ByteArrayToStructure<Double>(bacnetPropertyValue.BacnetPropertyValueUnion);
                    break;
                case (byte)BacnetApplicationTag.BacnetApplicationTagOctetString:
                    BacnetOctetStr bacnetOctetStr = StructureConverter.ByteArrayToStructure<BacnetOctetStr>(bacnetPropertyValue.BacnetPropertyValueUnion);
                    string octetString = null;

                    for (int count = 0; count < bacnetOctetStr.OctetCount; count++)
                    {
                        octetString += bacnetOctetStr.octetStr[count];
                    }

                    _value = octetString;
                    break;
                case (byte)BacnetApplicationTag.BacnetApplicationTagCharacterString:
                    BacnetCharStr bacnetCharStr = StructureConverter.ByteArrayToStructure<BacnetCharStr>(bacnetPropertyValue.BacnetPropertyValueUnion);

                    string charString = null;

                    int strLength = (int)(bacnetCharStr.strLen);
                    byte[] charStringArray = new byte[strLength];
                    Buffer.BlockCopy(bacnetCharStr.charStr, 0, charStringArray, 0, strLength);

                    switch (bacnetCharStr.encoding)
                    {
                        case (byte)BacnetCharacterStringEncoding.CharacterUCS2:
                            charString = Encoding.BigEndianUnicode.GetString(charStringArray);
                            break;
                        default:
                            charString = Encoding.UTF8.GetString(charStringArray);
                            break;
                    }

                    _value = charString;
                    break;
                case (byte)BacnetApplicationTag.BacnetApplicationTagBitString:
                    BacnetBitStr bacnetBitStr = StructureConverter.ByteArrayToStructure<BacnetBitStr>(bacnetPropertyValue.BacnetPropertyValueUnion);
                    BacnetBitStrModel bacnetBitStrModel = new BacnetBitStrModel();
                    bacnetBitStrModel.ByteCnt = bacnetBitStr.byteCnt;
                    bacnetBitStrModel.UnusedBits = bacnetBitStr.unusedBits;
                    bacnetBitStrModel.TransBits = bacnetBitStr.transBits;

                    _value = bacnetBitStrModel;
                    break;
                case (byte)BacnetApplicationTag.BacnetApplicationTagEnumerated:
                    UInt32 enumeratedValue = StructureConverter.ByteArrayToStructure<UInt32>(bacnetPropertyValue.BacnetPropertyValueUnion);
                    _value = enumeratedValue;
                    break;
                case (byte)BacnetApplicationTag.BacnetApplicationTagDate:
                    BacnetDate bacnetDate = StructureConverter.ByteArrayToStructure<BacnetDate>(bacnetPropertyValue.BacnetPropertyValueUnion);
                    BacnetDateModel bacnetDateModel = new BacnetDateModel();
                    bacnetDateModel.Day = bacnetDate.day;
                    bacnetDateModel.Month = bacnetDate.month;
                    bacnetDateModel.Year = bacnetDate.year;
                    bacnetDateModel.WeekDay = bacnetDate.weekDay;

                    _value = bacnetDateModel;
                    break;
                case (byte)BacnetApplicationTag.BacnetApplicationTagTime:
                    BacnetTime bacnetTime = StructureConverter.ByteArrayToStructure<BacnetTime>(bacnetPropertyValue.BacnetPropertyValueUnion);
                    BacnetTimeModel bacnetTimeModel = new BacnetTimeModel();
                    bacnetTimeModel.Hour = bacnetTime.hour;
                    bacnetTimeModel.Min = bacnetTime.min;
                    bacnetTimeModel.Sec = bacnetTime.sec;
                    bacnetTimeModel.Hundredths = bacnetTime.hundredths;

                    _value = bacnetTimeModel;
                    break;
                case (byte)BacnetApplicationTag.BacnetApplicationTagObjectID:
                    BacnetObjID bacnetObjID = StructureConverter.ByteArrayToStructure<BacnetObjID>(bacnetPropertyValue.BacnetPropertyValueUnion);

                    ObjectIdentifierModel objectIdentifierModel = new ObjectIdentifierModel();
                    objectIdentifierModel.ObjectType = bacnetObjID.objType;
                    objectIdentifierModel.InstanceNumber = bacnetObjID.objInstance;

                    _value = objectIdentifierModel;
                    break;
                default:
                    break;
            }

            return _value;
        }

        private static BacnetValueModel BacnetPropertyValueDecoding(BacnetPropertyValue bacnetPropertyValue)
        {
            BacnetValueModel valueModel = new BacnetValueModel();
            valueModel.TagType = bacnetPropertyValue.tagType;

            switch (bacnetPropertyValue.tagType)
            {
                case (byte)BacnetApplicationTag.BacnetApplicationTagBoolean:
                    valueModel.Value = StructureConverter.ByteArrayToStructure<bool>(bacnetPropertyValue.BacnetPropertyValueUnion);
                    break;
                case (byte)BacnetApplicationTag.BacnetApplicationTagUnsignedInt:
                    valueModel.Value = StructureConverter.ByteArrayToStructure<UInt32>(bacnetPropertyValue.BacnetPropertyValueUnion);
                    break;
                case (byte)BacnetApplicationTag.BacnetApplicationTagSignedInt:
                    valueModel.Value = StructureConverter.ByteArrayToStructure<int>(bacnetPropertyValue.BacnetPropertyValueUnion);
                    break;
                case (byte)BacnetApplicationTag.BacnetApplicationTagReal:
                    valueModel.Value = StructureConverter.ByteArrayToStructure<float>(bacnetPropertyValue.BacnetPropertyValueUnion);
                    break;
                case (byte)BacnetApplicationTag.BacnetApplicationTagDouble:
                    valueModel.Value = StructureConverter.ByteArrayToStructure<double>(bacnetPropertyValue.BacnetPropertyValueUnion);
                    break;
                case (byte)BacnetApplicationTag.BacnetApplicationTagCharacterString:
                    BacnetCharStr bacnetCharStr = StructureConverter.ByteArrayToStructure<BacnetCharStr>(bacnetPropertyValue.BacnetPropertyValueUnion);

                    string charString = null;
                    int length = (int)(bacnetCharStr.strLen);
                    byte[] unsigned = new byte[length];
                    Buffer.BlockCopy(bacnetCharStr.charStr, 0, unsigned, 0, length);

                    switch (bacnetCharStr.encoding)
                    {
                        case (byte)BacnetCharacterStringEncoding.CharacterUCS2:
                            charString = Encoding.BigEndianUnicode.GetString(unsigned);
                            break;
                        default:
                            charString = Encoding.UTF8.GetString(unsigned);
                            break;
                    }

                    valueModel.Value = charString;
                    break;
                case (byte)BacnetApplicationTag.BacnetApplicationTagEnumerated:
                    valueModel.Value = StructureConverter.ByteArrayToStructure<uint>(bacnetPropertyValue.BacnetPropertyValueUnion);
                    break;
                case (byte)BacnetApplicationTag.BacnetApplicationTagDate:
                    BacnetDate bacnetDate = StructureConverter.ByteArrayToStructure<BacnetDate>(bacnetPropertyValue.BacnetPropertyValueUnion);
                    BacnetDateModel bacnetDateModel = new BacnetDateModel();
                    bacnetDateModel.Day = bacnetDate.day;
                    bacnetDateModel.Month = bacnetDate.month;
                    bacnetDateModel.Year = bacnetDate.year;
                    bacnetDateModel.WeekDay = bacnetDate.weekDay;

                    valueModel.Value = bacnetDateModel;
                    break;
                case (byte)BacnetApplicationTag.BacnetApplicationTagTime:
                    BacnetTime bacnetTime = StructureConverter.ByteArrayToStructure<BacnetTime>(bacnetPropertyValue.BacnetPropertyValueUnion);
                    BacnetTimeModel bacnetTimeModel = new BacnetTimeModel();
                    bacnetTimeModel.Hour = bacnetTime.hour;
                    bacnetTimeModel.Min = bacnetTime.min;
                    bacnetTimeModel.Sec = bacnetTime.sec;
                    bacnetTimeModel.Hundredths = bacnetTime.hundredths;

                    valueModel.Value = bacnetTimeModel;
                    break;
                case (byte)BacnetApplicationTag.BacnetApplicationTagObjectID:
                    BacnetObjID bacnetObjID = StructureConverter.ByteArrayToStructure<BacnetObjID>(bacnetPropertyValue.BacnetPropertyValueUnion);

                    ObjectIdentifierModel objectIdentifierModel = new ObjectIdentifierModel();
                    objectIdentifierModel.InstanceNumber = bacnetObjID.objInstance;
                    objectIdentifierModel.ObjectType = bacnetObjID.objType;

                    valueModel.Value = objectIdentifierModel;
                    break;
                case (byte)BacnetApplicationTag.BacnetApplicationTagOctetString:
                    BacnetOctetStr bacnetOctetStr = StructureConverter.ByteArrayToStructure<BacnetOctetStr>(bacnetPropertyValue.BacnetPropertyValueUnion);
                    string octetString = GetOctetString(bacnetOctetStr);

                    valueModel.Value = octetString;
                    break;
                case (byte)BacnetApplicationTag.BacnetApplicationTagBitString:
                    BacnetBITStr bacnetBitStr = StructureConverter.ByteArrayToStructure<BacnetBITStr>(bacnetPropertyValue.BacnetPropertyValueUnion);
                    string bitString = GetBitStringNew(bacnetBitStr);

                    valueModel.Value = bitString;
                    break;
                case (byte)BacnetApplicationTag.BacnetApplicationTagNull:
                    valueModel.Value = null;
                    break;
                default:
                    break;
            }

            return valueModel;
        }

        private static ListOfBacnetLogRecord DecodeLogUsingTagType(ListOfBacnetLogRecord listOfBacnetLogRecord, BacnetLogRecordModel bacnetLogRecordModel)
        {
            switch (listOfBacnetLogRecord.bacnetLogRecord.logDatum.tagType)
            {
                case (byte)BacnetDataType.BacnetDTReal:
                    bacnetLogRecordModel.LogRecord.LogDatum.LogData = StructureConverter.ByteArrayToStructure<float>(listOfBacnetLogRecord.bacnetLogRecord.logDatum.LogDatumUnion);
                    break;
                case (byte)BacnetDataType.BacnetDTUnsigned:
                case (byte)BacnetDataType.BacnetDTUnsigned16:
                case (byte)BacnetDataType.BacnetDTUnsigned32:
                    bacnetLogRecordModel.LogRecord.LogDatum.LogData = StructureConverter.ByteArrayToStructure<uint>(listOfBacnetLogRecord.bacnetLogRecord.logDatum.LogDatumUnion);
                    break;
                case (byte)BacnetDataType.BacnetDTEnum:
                case (byte)BacnetDataType.BacnetDTEnumNew:
                    bacnetLogRecordModel.LogRecord.LogDatum.LogData = StructureConverter.ByteArrayToStructure<uint>(listOfBacnetLogRecord.bacnetLogRecord.logDatum.LogDatumUnion);
                    break;
                default:
                    break;
            }
            return listOfBacnetLogRecord;
        }

        private static EventTransitionBitsModel GetEventTransitionBits(BacnetBitStr bacnetBitString)
        {
            EventTransitionBitsModel eventTransitionBitsModel = new EventTransitionBitsModel();

            byte[] bitStringBytes = new byte[bacnetBitString.byteCnt];
            Buffer.BlockCopy(bacnetBitString.transBits, 0, bitStringBytes, 0, bacnetBitString.byteCnt);

            if (bitStringBytes.Length > 0)
            {
                byte eventBits = bitStringBytes[0];

                eventTransitionBitsModel.ToOffNormal = CommanHelper.GetBit(eventBits, 7);
                eventTransitionBitsModel.ToFault = CommanHelper.GetBit(eventBits, 6);
                eventTransitionBitsModel.ToNormal = CommanHelper.GetBit(eventBits, 5);
            }

            return eventTransitionBitsModel;
        }

        private static DaysOfWeekBitsModel GetDaysOfWeekBits(BacnetBitStr bacnetBitString)
        {
            DaysOfWeekBitsModel daysOfWeekBitsModel = new DaysOfWeekBitsModel();

            byte[] bitStringBytes = new byte[bacnetBitString.byteCnt];
            Buffer.BlockCopy(bacnetBitString.transBits, 0, bitStringBytes, 0, bacnetBitString.byteCnt);

            if (bitStringBytes.Length > 0)
            {
                byte eventBits = bitStringBytes[0];

                daysOfWeekBitsModel.Monday = CommanHelper.GetBit(eventBits, 7);
                daysOfWeekBitsModel.Tuesday = CommanHelper.GetBit(eventBits, 6);
                daysOfWeekBitsModel.Wednesday = CommanHelper.GetBit(eventBits, 5);
                daysOfWeekBitsModel.Thursday = CommanHelper.GetBit(eventBits, 4);
                daysOfWeekBitsModel.Friday = CommanHelper.GetBit(eventBits, 3);
                daysOfWeekBitsModel.Saturday = CommanHelper.GetBit(eventBits, 2);
                daysOfWeekBitsModel.Sunday = CommanHelper.GetBit(eventBits, 1);
            }

            return daysOfWeekBitsModel;
        }

        private static StatusFlagsModel GetStatusFlagsBits(BacnetBitStr bacnetBitString)
        {
            byte[] bitStringBytes = new byte[bacnetBitString.byteCnt];
            Buffer.BlockCopy(bacnetBitString.transBits, 0, bitStringBytes, 0, bacnetBitString.byteCnt);
            StatusFlagsModel statusFlagsModel = new StatusFlagsModel();

            if (bitStringBytes.Length > 0)
            {
                byte statusFlags = bitStringBytes[0];

                statusFlagsModel.InAlarm = CommanHelper.GetBit(statusFlags, 7);
                statusFlagsModel.Fault = CommanHelper.GetBit(statusFlags, 6);
                statusFlagsModel.Overridden = CommanHelper.GetBit(statusFlags, 5);
                statusFlagsModel.OutOfService = CommanHelper.GetBit(statusFlags, 4);
            }

            return statusFlagsModel;
        }

        private static LimitEnableBitsModel GetLimitEnableBits(BacnetBitStr bacnetBitString)
        {
            byte[] bitStringBytes = new byte[bacnetBitString.byteCnt];
            Buffer.BlockCopy(bacnetBitString.transBits, 0, bitStringBytes, 0, bacnetBitString.byteCnt);
            LimitEnableBitsModel limitEnableBitsModel = new LimitEnableBitsModel();

            if (bitStringBytes.Length > 0)
            {
                byte statusFlags = bitStringBytes[0];

                limitEnableBitsModel.LowLimit = CommanHelper.GetBit(statusFlags, 7);
                limitEnableBitsModel.HighLimit = CommanHelper.GetBit(statusFlags, 6);
            }

            return limitEnableBitsModel;
        }

        private static string GetBitString(BacnetBitStr bacnetBitString)
        {
            byte[] bitStringBytes = new byte[bacnetBitString.byteCnt];
            Buffer.BlockCopy(bacnetBitString.transBits, 0, bitStringBytes, 0, bacnetBitString.byteCnt);
            string bitString = null;

            if (bitStringBytes.Length > 0)
            {
                byte statusFlags = bitStringBytes[0];

                for (int bit = 7; bit >= bacnetBitString.unusedBits; bit--)
                {
                    if (bitString != null)
                        bitString = bitString + ",";

                    bitString += CommanHelper.GetBit(statusFlags, bit) ? 1 : 0;
                }

            }

            return bitString;
        }

        private static string GetBitStringNew(BacnetBITStr bacnetBITStr)
        {
            byte[] bitStringBytes = new byte[bacnetBITStr.byteCnt];
            Buffer.BlockCopy(bacnetBITStr.transBits, 0, bitStringBytes, 0, bacnetBITStr.byteCnt);
            string bitString = null;

            if (bitStringBytes.Length > 0)
            {
                for (int byteCount = 0; byteCount < bacnetBITStr.byteCnt; byteCount++)
                {
                    byte statusFlags = bitStringBytes[byteCount];

                    if (byteCount != bacnetBITStr.byteCnt - 1)
                    {
                        for (int bit = 7; bit >= 0; bit--)
                        {
                            if (bitString != null)
                                bitString = bitString + ",";

                            bitString += CommanHelper.GetBit(statusFlags, bit) ? 1 : 0;
                        }
                    }
                    else
                    {
                        for (int bit = 7; bit >= bacnetBITStr.unusedBits; bit--)
                        {
                            if (bitString != null)
                                bitString = bitString + ",";

                            bitString += CommanHelper.GetBit(statusFlags, bit) ? 1 : 0;
                        }
                    }
                }

            }

            return bitString;
        }

        private static string GetOctetString(BacnetOctetStr bacnetOctetStr)
        {
            byte[] octetArray = new byte[bacnetOctetStr.OctetCount];
            Buffer.BlockCopy(bacnetOctetStr.octetStr, 0, octetArray, 0, (int)bacnetOctetStr.OctetCount);
            string octetString = null;

            if (octetArray.Length > 0)
            {
                octetString = CommanHelper.ByteArrayToString(octetArray);
            }

            return octetString;
        }

        private static EventParametersBase GetEventParameters(BacnetEventParameter bacnetEventParameter)
        {
            EventParametersBase eventParameters = new EventParametersBase();
            eventParameters.EventType = bacnetEventParameter.eventType;

            switch (bacnetEventParameter.eventType)
            {
                case BacnetEventType.EventChangeOfState:
                    ChangeOfStateEventParametersModel changeOfStateEventParametersModel = new ChangeOfStateEventParametersModel();
                    ChangeOfState changeOfState = StructureConverter.ByteArrayToStructure<ChangeOfState>(bacnetEventParameter.bacnetEventParameterUnion);

                    changeOfStateEventParametersModel.EventType = bacnetEventParameter.eventType;
                    changeOfStateEventParametersModel.TimeDelay = changeOfState.timedelay;
                    IntPtr propStatesPointer = changeOfState.listOfValues;

                    while (propStatesPointer != IntPtr.Zero)
                    {
                        ListOfBacnetPropertyStates listOfBacnetPropertyStates = (ListOfBacnetPropertyStates)Marshal.PtrToStructure(propStatesPointer, typeof(ListOfBacnetPropertyStates));
                        PropertyStatesModel propertyStatesModel = new PropertyStatesModel();
                        propertyStatesModel.PropertyState = (BacnetPropertyStates)listOfBacnetPropertyStates.bacnetPropStates.propState;
                        propertyStatesModel.ApplicationTag = (BacnetApplicationTag)listOfBacnetPropertyStates.bacnetPropStates.appTag;

                        if (propertyStatesModel.PropertyState == BacnetPropertyStates.PropStateBooleanValue)
                        {
                            propertyStatesModel.Value = listOfBacnetPropertyStates.bacnetPropStates.bacnetPropState.boolVal;
                        }
                        else if (propertyStatesModel.PropertyState == BacnetPropertyStates.PropStateUnsignedValue)
                        {
                            propertyStatesModel.Value = listOfBacnetPropertyStates.bacnetPropStates.bacnetPropState.unsignedValue;
                        }
                        else
                        {
                            propertyStatesModel.Value = listOfBacnetPropertyStates.bacnetPropStates.bacnetPropState.eventType;
                        }

                        changeOfStateEventParametersModel.PropertyStates.Add(propertyStatesModel);
                        propStatesPointer = listOfBacnetPropertyStates.next;
                    }

                    eventParameters = changeOfStateEventParametersModel;
                    break;
                case BacnetEventType.EventCommandFailure:
                    CommandFailureEventParametersModel commandFailureEventParametersModel = new CommandFailureEventParametersModel();
                    CommandFailure commandFailure = StructureConverter.ByteArrayToStructure<CommandFailure>(bacnetEventParameter.bacnetEventParameterUnion);

                    commandFailureEventParametersModel.EventType = bacnetEventParameter.eventType;
                    commandFailureEventParametersModel.ApplicationTag = (BacnetApplicationTag)commandFailure.apptag;
                    commandFailureEventParametersModel.TimeDelay = commandFailure.timedelay;
                    commandFailureEventParametersModel.BacnetObjectPropertyReference.DeviceInstance = commandFailure.stFeedbackPropertyReference.deviceInstace;
                    commandFailureEventParametersModel.BacnetObjectPropertyReference.ObjId = commandFailure.stFeedbackPropertyReference.objID;
                    commandFailureEventParametersModel.BacnetObjectPropertyReference.ObjectType = commandFailure.stFeedbackPropertyReference.objectType;
                    commandFailureEventParametersModel.BacnetObjectPropertyReference.DeviceType = commandFailure.stFeedbackPropertyReference.deviceType;
                    commandFailureEventParametersModel.BacnetObjectPropertyReference.DeviceIdPresent = commandFailure.stFeedbackPropertyReference.deviceIDPresent;
                    commandFailureEventParametersModel.BacnetObjectPropertyReference.PropertyIdentifier = commandFailure.stFeedbackPropertyReference.propertyIdentifier;
                    commandFailureEventParametersModel.BacnetObjectPropertyReference.ArrayIndex = commandFailure.stFeedbackPropertyReference.arrayIndex;
                    commandFailureEventParametersModel.BacnetObjectPropertyReference.ArrIndxPresent = commandFailure.stFeedbackPropertyReference.arrIndxPresent;

                    commandFailureEventParametersModel.Value = GetPropertyValueFromBytearrayUsingTag((BacnetApplicationTag)commandFailure.apptag, commandFailure.propertyValueUnion);
                    eventParameters = commandFailureEventParametersModel;
                    break;
                case BacnetEventType.EventOutOfRange:
                    OutOfRangeEventParametersModel outOfRangeEventParametersModel = new OutOfRangeEventParametersModel();
                    OutOfRange outOfRange = StructureConverter.ByteArrayToStructure<OutOfRange>(bacnetEventParameter.bacnetEventParameterUnion);
                    outOfRangeEventParametersModel.EventType = bacnetEventParameter.eventType;
                    outOfRangeEventParametersModel.TimeDelay = outOfRange.timedelay;
                    outOfRangeEventParametersModel.HighLimit = outOfRange.highLimit;
                    outOfRangeEventParametersModel.LowLimit = outOfRange.lowLimit;
                    outOfRangeEventParametersModel.DeadBand = outOfRange.deadband;

                    eventParameters = outOfRangeEventParametersModel;
                    break;
                case BacnetEventType.EventChangeOfReliability:
                    break;
                case BacnetEventType.EventChangeOfBitString:
                    break;
                case BacnetEventType.EventChangeOfValue:
                    break;
                case BacnetEventType.EventFloatingLimit:
                    break;
                case BacnetEventType.EventComplex:
                    break;
                case BacnetEventType.EventUnassigned:
                    break;
                case BacnetEventType.EventChangeOfLifeSafety:
                    break;
                case BacnetEventType.EventExtended:
                    break;
                case BacnetEventType.EventBufferReady:
                    break;
                case BacnetEventType.EventUnsignedRange:
                    break;
                case BacnetEventType.EventReserved:
                    break;
                case BacnetEventType.EventAccessEvent:
                    break;
                case BacnetEventType.EventDoubleOutOfRange:
                    break;
                case BacnetEventType.EventSignedOutOfRange:
                    break;
                case BacnetEventType.EventUnsignedOutOfRange:
                    break;
                case BacnetEventType.EventChangeOfCharacterString:
                    break;
                case BacnetEventType.EventChangeOfStatusFlags:
                    break;
                case BacnetEventType.EventNone:
                    break;
                case BacnetEventType.MaxEventType:
                    break;
                default:
                    break;
            }

            return eventParameters;
        }

        public static object GetPropertyValueFromBytearrayUsingTag(BacnetApplicationTag tag, byte[] propertyValueUnion)
        {
            object value = null;

            switch (tag)
            {
                case BacnetApplicationTag.BacnetApplicationTagBoolean:
                    value = StructureConverter.ByteArrayToStructure<bool>(propertyValueUnion);
                    break;
                case BacnetApplicationTag.BacnetApplicationTagUnsignedInt:
                    value = StructureConverter.ByteArrayToStructure<UInt32>(propertyValueUnion);
                    break;
                case BacnetApplicationTag.BacnetApplicationTagSignedInt:
                    value = StructureConverter.ByteArrayToStructure<int>(propertyValueUnion);
                    break;
                case BacnetApplicationTag.BacnetApplicationTagReal:
                    value = StructureConverter.ByteArrayToStructure<float>(propertyValueUnion);
                    break;
                case BacnetApplicationTag.BacnetApplicationTagDouble:
                    value = StructureConverter.ByteArrayToStructure<double>(propertyValueUnion);
                    break;
                case BacnetApplicationTag.BacnetApplicationTagCharacterString:
                    BacnetCharStr bacnetCharStr = StructureConverter.ByteArrayToStructure<BacnetCharStr>(propertyValueUnion);

                    string charString = null;
                    int length = (int)(bacnetCharStr.strLen);
                    byte[] unsigned = new byte[length];
                    Buffer.BlockCopy(bacnetCharStr.charStr, 0, unsigned, 0, length);

                    switch (bacnetCharStr.encoding)
                    {
                        case (byte)BacnetCharacterStringEncoding.CharacterUCS2:
                            charString = Encoding.BigEndianUnicode.GetString(unsigned);
                            break;
                        default:
                            charString = Encoding.UTF8.GetString(unsigned);
                            break;
                    }

                    value = charString;
                    break;
                case BacnetApplicationTag.BacnetApplicationTagEnumerated:
                    value = StructureConverter.ByteArrayToStructure<uint>(propertyValueUnion);
                    break;
                case BacnetApplicationTag.BacnetApplicationTagDate:
                    BacnetDate bacnetDate = StructureConverter.ByteArrayToStructure<BacnetDate>(propertyValueUnion);
                    BacnetDateModel bacnetDateModel = new BacnetDateModel();
                    bacnetDateModel.Day = bacnetDate.day;
                    bacnetDateModel.Month = bacnetDate.month;
                    bacnetDateModel.Year = bacnetDate.year;
                    bacnetDateModel.WeekDay = bacnetDate.weekDay;

                    value = bacnetDateModel;
                    break;
                case BacnetApplicationTag.BacnetApplicationTagTime:
                    BacnetTime bacnetTime = StructureConverter.ByteArrayToStructure<BacnetTime>(propertyValueUnion);
                    BacnetTimeModel bacnetTimeModel = new BacnetTimeModel();
                    bacnetTimeModel.Hour = bacnetTime.hour;
                    bacnetTimeModel.Min = bacnetTime.min;
                    bacnetTimeModel.Sec = bacnetTime.sec;
                    bacnetTimeModel.Hundredths = bacnetTime.hundredths;

                    value = bacnetTimeModel;
                    break;
                case BacnetApplicationTag.BacnetApplicationTagObjectID:
                    BacnetObjID bacnetObjID = StructureConverter.ByteArrayToStructure<BacnetObjID>(propertyValueUnion);

                    ObjectIdentifierModel objectIdentifierModel = new ObjectIdentifierModel();
                    objectIdentifierModel.InstanceNumber = bacnetObjID.objInstance;
                    objectIdentifierModel.ObjectType = bacnetObjID.objType;

                    value = objectIdentifierModel;
                    break;
                case BacnetApplicationTag.BacnetApplicationTagNull:
                    value = null;
                    break;
                case BacnetApplicationTag.BacnetApplicationTagOctetString:
                    break;
                case BacnetApplicationTag.BacnetApplicationTagReserve1:
                    break;
                case BacnetApplicationTag.BacnetApplicationTagReserve2:
                    break;
                case BacnetApplicationTag.BacnetApplicationTagReserve3:
                    break;
                case BacnetApplicationTag.BacnetApplicationTagDateTime:
                    break;
                case BacnetApplicationTag.BacnetApplicationTagListUnsign:
                    break;
                case BacnetApplicationTag.BacnetApplicationTagListChar:
                    break;
                case BacnetApplicationTag.BacnetApplicationTagPriortyary:
                    break;
                case BacnetApplicationTag.BacnetApplicationTagTimestamp:
                    break;
                case BacnetApplicationTag.BacnetApplicationTagActiveCOV:
                    break;
                case BacnetApplicationTag.BacnetApplicationTagConstructed:
                    break;
                case BacnetApplicationTag.BacnetApplicationTagAny:
                    break;
                case BacnetApplicationTag.MaxBacnetApplicationTag:
                    break;
                case BacnetApplicationTag.TagNotSupported:
                    break;
                default:
                    break;
            }

            return value;
        }
    }
}
