﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.ManagedWrapper.Helper
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.APIModels;
using Molex.StackDataObjects.Request;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Molex.BACnetStackIntegration.StackStructures;

namespace Molex.BACnetStackIntegration.ManagedWrapper.Helper
{
    internal class BBMDHelper
    {
        static BBMDHelper()
        {
        }

        public static WriteBdtRequest GetWriteBDTStructure(List<BDTModel> BDTEntryList, ref Dictionary<IntPtr, Type> structurePointers)
        {
            int writeBDTRequestSize = Marshal.SizeOf(typeof(WriteBdtRequest));

            WriteBdtRequest serviceChoiseRequest = new WriteBdtRequest();

            if (BDTEntryList.Count > 0)
            {
                WriteBdtRequest writeBdtRequest = new WriteBdtRequest();
                BDTModel bdtModel = BDTEntryList[0];
                writeBdtRequest.bIpAddress = new BacnetIPAddress();
                string[] ipAddressArray = bdtModel.IPAddress.Split('.');
                byte[] ipAddresses = new byte[ipAddressArray.Length];

                for (int i = 0; i < ipAddressArray.Length; i++)
                {
                    ipAddresses[i] = Convert.ToByte(ipAddressArray[i]);
                }

                writeBdtRequest.bIpAddress.ipAddress = ipAddresses;
                writeBdtRequest.bIpAddress.portNo = bdtModel.Port;
                writeBdtRequest.bIpAddress.broadcastMask = bdtModel.BroadcastMask;

                serviceChoiseRequest = writeBdtRequest;

                IntPtr nextWriteBDTPtr = IntPtr.Zero;

                if (BDTEntryList.Count > 1)
                {
                    nextWriteBDTPtr = Marshal.AllocHGlobal(writeBDTRequestSize);
                    serviceChoiseRequest.bIpAddress.next = nextWriteBDTPtr;

                    structurePointers.Add(nextWriteBDTPtr, typeof(WriteBdtRequest));
                }

                for (int i = 1; i < BDTEntryList.Count; i++)
                {
                    writeBdtRequest = new WriteBdtRequest();

                    bdtModel = new BDTModel();
                    bdtModel = BDTEntryList[i];
                    writeBdtRequest.bIpAddress = new BacnetIPAddress();
                    string[] ipAddressArr = bdtModel.IPAddress.Split('.');
                    byte[] ipAddr = new byte[ipAddressArr.Length];

                    for (int j = 0; j < ipAddressArr.Length; j++)
                    {
                        ipAddr[j] = Convert.ToByte(ipAddressArr[j]);
                    }

                    writeBdtRequest.bIpAddress.ipAddress = ipAddr;
                    writeBdtRequest.bIpAddress.portNo = bdtModel.Port;
                    writeBdtRequest.bIpAddress.broadcastMask = bdtModel.BroadcastMask;

                    IntPtr current = nextWriteBDTPtr;

                    if (i < BDTEntryList.Count - 1)
                    {
                        nextWriteBDTPtr = Marshal.AllocHGlobal(writeBDTRequestSize);
                        writeBdtRequest.bIpAddress.next = nextWriteBDTPtr;

                        if (!structurePointers.ContainsKey(nextWriteBDTPtr))
                            structurePointers.Add(nextWriteBDTPtr, typeof(WriteBdtRequest));
                    }

                    Marshal.StructureToPtr(writeBdtRequest, current, false);
                }
            }

            return serviceChoiseRequest;
        }

        public static DeleteFdtRequest GetDeleteFDTStructure(DeleteFDEntryRequest deleteFDEntryRequest)
        {
            DeleteFdtRequest req = new DeleteFdtRequest();
            req.bIpAddress = new BacnetIPAddress();
            string[] ipAddressArray = deleteFDEntryRequest.IPAddress.Split('.');
            byte[] ipAddresses = new byte[ipAddressArray.Length];

            for (int i = 0; i < ipAddressArray.Length; i++)
            {
                ipAddresses[i] = Convert.ToByte(ipAddressArray[i]);
            }

            req.bIpAddress.ipAddress = ipAddresses;
            req.bIpAddress.portNo = deleteFDEntryRequest.Port;
            req.bIpAddress.broadcastMask = deleteFDEntryRequest.BroadcastMask;

            return req;
        }
    }
}
