﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.Helper
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using System;
using System.Runtime.InteropServices;

namespace Molex.BACnetStackIntegration.Helper
{
    /// <summary>
    /// Responsible structure conversion.
    /// </summary>
    public static class StructureConverter
    {
        /// <summary>
        /// Converts structure to Pointer.
        /// </summary>
        /// <param name="obj">structure object to be converted into pointer</param>
        public static IntPtr StructureToPtr<T>(T obj)
        {
            int length = Marshal.SizeOf(obj);
            byte[] byteArray = new byte[length];
            IntPtr ptr = Marshal.AllocHGlobal(length);
            Marshal.StructureToPtr(obj, ptr, true);
            return ptr;
        }

        /// <summary>
        /// Converts pointer to structure.
        /// </summary>
        /// <param name="bytearray">pointer be converted into structure</param>
        public static T PtrToStructure<T>(IntPtr ptr)
        {
            T obj = (T)Marshal.PtrToStructure(ptr, typeof(T));
            return obj;
        }

        /// <summary>
        /// Converts structure of byte array to Pointer.
        /// </summary>
        /// <param name="obj">structure object to be converted into pointer</param>
        public static IntPtr DataToPtr(byte[] data, int length)
        {
            IntPtr ptr = Marshal.AllocHGlobal(length);
            Marshal.Copy(data, 0, ptr, length);
            return ptr;
        }

        /// <summary>
        /// Copy strcuture in given pointer
        /// </summary>
        public static void getBytes<T>(T str, IntPtr ptrDestination)
        {
            int size = Marshal.SizeOf(str);
            byte[] arr = new byte[size];
            IntPtr ptr = Marshal.AllocHGlobal(size);

            Marshal.StructureToPtr(str, ptr, true);
            Marshal.Copy(ptr, arr, 0, size);
            Marshal.FreeHGlobal(ptr);
            ptr = IntPtr.Zero;
            Marshal.Copy(arr, 0, ptrDestination, size);
        }

        /// <summary>
        /// Convert structure to byte array.
        /// </summary>
        public static byte[] StructureToByteArray<T>(T str)
        {
            int size = Marshal.SizeOf(str);
            byte[] arr = new byte[size];
            IntPtr ptr = Marshal.AllocHGlobal(size);

            Marshal.StructureToPtr(str, ptr, true);
            Marshal.Copy(ptr, arr, 0, size);
            Marshal.FreeHGlobal(ptr);
            ptr = IntPtr.Zero;
            return arr;
        }

        /// <summary>
        /// Convert byte array to structure.
        /// </summary>
        public static T ByteArrayToStructure<T>(byte[] data)
        {
            IntPtr ptr = Marshal.AllocHGlobal(data.Length);
            Marshal.Copy(data, 0, ptr, data.Length);
            T obj = (T)Marshal.PtrToStructure(ptr, typeof(T));
            return obj;
        }
    }
}