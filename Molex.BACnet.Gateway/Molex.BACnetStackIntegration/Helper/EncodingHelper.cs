﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.Helper
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.BACnetStackIntegration.Log;
using Molex.BACnetStackIntegration.Stack.Wrapper;
using Molex.StackDataObjects.APIModels;
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;
using System.Text;

namespace Molex.BACnetStackIntegration.Helper
{
    public class EncodingHelper
    {
        public static IntPtr EncodePropertyValue(BacnetDataType datatype, BacnetPropertyID propertyID, object propertyValue, ref Dictionary<IntPtr, Type> structurePointers, out bool hasLazyPointer, out bool isError)
        {
            byte[] propertyVal = new byte[255];
            IntPtr ptrPropVal = IntPtr.Zero;
            hasLazyPointer = false;
            isError = false;

            try
            {
                switch (datatype)
                {
                    case BacnetDataType.BacnetDTDateList:
                        PrListOfBacnetCalendarEntry listOfBacnetCalendarEntry = new PrListOfBacnetCalendarEntry();
                        listOfBacnetCalendarEntry = GenerateListOfBacnetCalendarEntry(propertyValue, structurePointers);
                        ptrPropVal = StructureConverter.StructureToPtr<PrListOfBacnetCalendarEntry>(listOfBacnetCalendarEntry);
                        break;
                    case BacnetDataType.BacnetDTCharString:
                        string pvValue = Convert.ToString(propertyValue);

                        byte[] arrByte = Encoding.ASCII.GetBytes(pvValue);
                        int arrayLength = 0;

                        if (arrByte.Length <= 255)
                            arrayLength = arrByte.Length;
                        else
                            arrayLength = 255;

                        PrBacnetCharStr charString = new PrBacnetCharStr();
                        charString.stCharString.strLen = (uint)(arrayLength);

                        Buffer.BlockCopy(arrByte, 0, propertyVal, 0, arrayLength);
                        charString.stCharString.charStr = propertyVal;
                        ptrPropVal = StructureConverter.StructureToPtr<PrBacnetCharStr>(charString);
                        break;
                    case BacnetDataType.BacnetDTBoolean:
                        bool boolVal = Convert.ToBoolean(propertyValue);

                        PrBacnetBool bacnetBool = new PrBacnetBool();
                        bacnetBool.val = boolVal;
                        ptrPropVal = StructureConverter.StructureToPtr<PrBacnetBool>(bacnetBool);
                        break;
                    case BacnetDataType.BacnetDTEnum:
                        hasLazyPointer = true;
                        PrBinaryEnumPV binaryEnumPV = new PrBinaryEnumPV();
                        binaryEnumPV = GenerateStructureBinaryEnumPV(propertyValue, structurePointers);

                        ptrPropVal = StructureConverter.StructureToPtr<PrBinaryEnumPV>(binaryEnumPV);
                        break;
                    case BacnetDataType.BacnetDTEnumNew:
                        int enumVal = Convert.ToInt32(propertyValue);

                        PrBacnetEnum bacnetEnum = new PrBacnetEnum();
                        bacnetEnum.val = enumVal;
                        ptrPropVal = StructureConverter.StructureToPtr<PrBacnetEnum>(bacnetEnum);
                        break;
                    case BacnetDataType.BacnetDTUnsigned:
                    case BacnetDataType.BacnetDTUnsigned32:
                        uint unsignedVal = Convert.ToUInt32(propertyValue);

                        PrBacnetUnsigned32 bacnetUnsigned32 = new PrBacnetUnsigned32();
                        bacnetUnsigned32.val = unsignedVal;
                        ptrPropVal = StructureConverter.StructureToPtr<PrBacnetUnsigned32>(bacnetUnsigned32);
                        break;
                    case BacnetDataType.BacnetDTUnsigned16:
                        ushort unsigned16Val = Convert.ToUInt16(propertyValue);

                        PrBacnetUnsigned16 bacnetUnsigned16 = new PrBacnetUnsigned16();
                        bacnetUnsigned16.val = unsigned16Val;
                        ptrPropVal = StructureConverter.StructureToPtr<PrBacnetUnsigned16>(bacnetUnsigned16);
                        break;
                    case BacnetDataType.BacnetDTInteger:
                        int signedVal = Convert.ToInt32(propertyValue);

                        PrBacnetSigned32 bacnetSigned32 = new PrBacnetSigned32();
                        bacnetSigned32.val = signedVal;
                        ptrPropVal = StructureConverter.StructureToPtr<PrBacnetSigned32>(bacnetSigned32);
                        break;
                    case BacnetDataType.BacnetDTReal:
                        float realVal = Convert.ToSingle(propertyValue);

                        PrBacnetReal bacnetReal = new PrBacnetReal();
                        bacnetReal.val = realVal;
                        ptrPropVal = StructureConverter.StructureToPtr<PrBacnetReal>(bacnetReal);
                        break;
                    case BacnetDataType.BacnetDTDouble:
                        double doubleVal = Convert.ToDouble(propertyValue);

                        PrBacnetDouble bacnetDouble = new PrBacnetDouble();
                        bacnetDouble.val = doubleVal;
                        ptrPropVal = StructureConverter.StructureToPtr<PrBacnetDouble>(bacnetDouble);
                        break;
                    case BacnetDataType.BacnetDTBitString:
                        PrBacnetBitStr prBacnetBitStr = new PrBacnetBitStr();
                        BacnetBitStr bacnetBitStr = new BacnetBitStr();

                        switch (propertyID)
                        {
                            case BacnetPropertyID.PropStatusFlags:
                            case BacnetPropertyID.PropMemberStatusFlags:
                                StatusFlagsModel statusFlagsModel = propertyValue as StatusFlagsModel;
                                bacnetBitStr = SetStatusFlagBits(statusFlagsModel);
                                break;
                            case BacnetPropertyID.PropEventEnable:
                            case BacnetPropertyID.PropAckedTransitions:
                            case BacnetPropertyID.PropAckRequired:
                                EventTransitionBitsModel eventTransitionBitsModel = propertyValue as EventTransitionBitsModel;
                                bacnetBitStr = SetEventTransitionBits(eventTransitionBitsModel);
                                break;
                            case BacnetPropertyID.PropLimitEnable:
                                LimitEnableBitsModel limitEnableBitsModel = propertyValue as LimitEnableBitsModel;
                                bacnetBitStr = SetLimitEnableBits(limitEnableBitsModel);
                                break;
                            default:
                                string bitString = propertyValue as string;
                                bacnetBitStr = SetBitString(bitString);
                                break;
                        }

                        prBacnetBitStr.stBitStr = bacnetBitStr;
                        ptrPropVal = StructureConverter.StructureToPtr<PrBacnetBitStr>(prBacnetBitStr);
                        break;
                    case BacnetDataType.BacnetDTUnsignedList:
                    case BacnetDataType.BacnetDTUnsignedArray:
                        hasLazyPointer = true;
                        PrListOfUnsigned listOfUnsigned = new PrListOfUnsigned();
                        listOfUnsigned = GenerateStructureListOfUnsigned(propertyValue, structurePointers);

                        ptrPropVal = StructureConverter.StructureToPtr<PrListOfUnsigned>(listOfUnsigned);
                        break;
                    case BacnetDataType.BacnetDTDateTime:
                        BacnetDateTimeModel bacnetDateTimeModel = propertyValue as BacnetDateTimeModel;
                        PrBacnetDateTime prBacnetDateTime = new PrBacnetDateTime();

                        if (bacnetDateTimeModel != null)
                        {
                            prBacnetDateTime.stDateTime.date.day = bacnetDateTimeModel.Date.Day;
                            prBacnetDateTime.stDateTime.date.month = bacnetDateTimeModel.Date.Month;
                            prBacnetDateTime.stDateTime.date.year = bacnetDateTimeModel.Date.Year;
                            prBacnetDateTime.stDateTime.date.weekDay = bacnetDateTimeModel.Date.WeekDay;

                            prBacnetDateTime.stDateTime.time.hour = bacnetDateTimeModel.Time.Hour;
                            prBacnetDateTime.stDateTime.time.min = bacnetDateTimeModel.Time.Min;
                            prBacnetDateTime.stDateTime.time.sec = bacnetDateTimeModel.Time.Sec;
                            prBacnetDateTime.stDateTime.time.hundredths = bacnetDateTimeModel.Time.Hundredths;
                        }

                        ptrPropVal = StructureConverter.StructureToPtr<PrBacnetDateTime>(prBacnetDateTime);

                        break;
                    case BacnetDataType.BacnetDTDateRange:
                        BacnetDateRangeModel bacnetDateRangeModel = propertyValue as BacnetDateRangeModel;
                        PrBacnetDateRange prBacnetDateRange = new PrBacnetDateRange();

                        if (bacnetDateRangeModel != null)
                        {
                            prBacnetDateRange.stDateRange.stStartDate.day = bacnetDateRangeModel.StartDate.Day;
                            prBacnetDateRange.stDateRange.stStartDate.month = bacnetDateRangeModel.StartDate.Month;
                            prBacnetDateRange.stDateRange.stStartDate.year = bacnetDateRangeModel.StartDate.Year;
                            prBacnetDateRange.stDateRange.stStartDate.weekDay = bacnetDateRangeModel.StartDate.WeekDay;

                            prBacnetDateRange.stDateRange.stEndDate.day = bacnetDateRangeModel.EndDate.Day;
                            prBacnetDateRange.stDateRange.stEndDate.month = bacnetDateRangeModel.EndDate.Month;
                            prBacnetDateRange.stDateRange.stEndDate.year = bacnetDateRangeModel.EndDate.Year;
                            prBacnetDateRange.stDateRange.stEndDate.weekDay = bacnetDateRangeModel.EndDate.WeekDay;
                        }

                        ptrPropVal = StructureConverter.StructureToPtr<PrBacnetDateRange>(prBacnetDateRange);

                        break;
                    case BacnetDataType.BacnetDTClientCOVIncrement:
                        BacnetClientCOVIncrementModel bacnetClientCOVIncrementModel = propertyValue as BacnetClientCOVIncrementModel;

                        PrBacnetClientCOV prBacnetClientCOV = new PrBacnetClientCOV();

                        if (bacnetClientCOVIncrementModel != null)
                        {
                            prBacnetClientCOV.stClientCOV.value = bacnetClientCOVIncrementModel.Value;

                            switch (bacnetClientCOVIncrementModel.TagType)
                            {
                                case "Real":
                                    prBacnetClientCOV.stClientCOV.appTagtype = BacnetApplicationTag.BacnetApplicationTagReal;
                                    break;
                                default:
                                    prBacnetClientCOV.stClientCOV.appTagtype = BacnetApplicationTag.BacnetApplicationTagNull;
                                    break;
                            }
                        }
                        else
                        {
                            prBacnetClientCOV.stClientCOV.appTagtype = BacnetApplicationTag.BacnetApplicationTagNull;
                        }


                        ptrPropVal = StructureConverter.StructureToPtr<PrBacnetClientCOV>(prBacnetClientCOV);
                        break;
                    case BacnetDataType.BacnetDTNotificationPriority:
                        PrBacnetNotifyPriority prBacnetNotifyPriority = new PrBacnetNotifyPriority();

                        if (propertyValue is ICollection)
                        {
                            uint[] valueArray = propertyValue as uint[];
                            uint[] priorityArray = new uint[3];

                            if (valueArray.Length >= 3)
                                Array.Copy(valueArray, priorityArray, 3);
                            else
                                priorityArray = valueArray;

                            prBacnetNotifyPriority.value = priorityArray;
                        }

                        ptrPropVal = StructureConverter.StructureToPtr<PrBacnetNotifyPriority>(prBacnetNotifyPriority);
                        break;
                    case BacnetDataType.BacnetDTDestinationList:
                        hasLazyPointer = true;
                        PrListOfBacnetDestination prListOfBacnetDestination = new PrListOfBacnetDestination();
                        prListOfBacnetDestination = GenerateStructureBacnetDestination(propertyValue, structurePointers);

                        ptrPropVal = StructureConverter.StructureToPtr<PrListOfBacnetDestination>(prListOfBacnetDestination);
                        break;
                    case BacnetDataType.BacnetDTDailySchedule:
                        hasLazyPointer = true;
                        PrBacnetDailySchedule prBacnetDailySchedule = new PrBacnetDailySchedule();
                        prBacnetDailySchedule = GenerateStructureDailySchedule(propertyValue, structurePointers);

                        ptrPropVal = StructureConverter.StructureToPtr<PrBacnetDailySchedule>(prBacnetDailySchedule);
                        break;
                    case BacnetDataType.BacnetDTDailyScheduleArray:
                        hasLazyPointer = true;
                        PrListOfBacnetDailySchedule prListOfBacnetDailySchedule = new PrListOfBacnetDailySchedule();
                        prListOfBacnetDailySchedule = GenerateStructureDailyScheduleArray(propertyValue, structurePointers);

                        ptrPropVal = StructureConverter.StructureToPtr<PrListOfBacnetDailySchedule>(prListOfBacnetDailySchedule);
                        break;
                    case BacnetDataType.BacnetDTSpecialEventArray:
                    case BacnetDataType.BacnetDTSpecialEvent:
                        hasLazyPointer = true;
                        PrListOfBacnetSpecialEvent prListOfBacnetSpecialEvent = new PrListOfBacnetSpecialEvent();
                        prListOfBacnetSpecialEvent = GenerateStructureSpecialEvent(propertyValue, structurePointers);

                        ptrPropVal = StructureConverter.StructureToPtr<PrListOfBacnetSpecialEvent>(prListOfBacnetSpecialEvent);
                        break;
                    case BacnetDataType.BacnetDTSchedulePresentDefault:
                        AnyValue anyValue = new AnyValue();
                        anyValue = GenerateStructureAnyValue(propertyValue);

                        ptrPropVal = StructureConverter.StructureToPtr<AnyValue>(anyValue);
                        break;
                    case BacnetDataType.BacnetDTDevObjPropReff:
                        BACnetDevObjPropRefModel bacnetDevObjPropRefModel = propertyValue as BACnetDevObjPropRefModel;

                        PrBacnetDevObjPropRef prBacnetDevObjPropRef = new PrBacnetDevObjPropRef();
                        BacnetDevObjPropRef bacnetDevObjPropRef = new BacnetDevObjPropRef();

                        if (bacnetDevObjPropRefModel != null)
                        {
                            bacnetDevObjPropRef.propertyIdentifier = bacnetDevObjPropRefModel.PropertyIdentifier;
                            bacnetDevObjPropRef.objID = bacnetDevObjPropRefModel.ObjId;
                            bacnetDevObjPropRef.objectType = bacnetDevObjPropRefModel.ObjectType;
                            bacnetDevObjPropRef.deviceIDPresent = bacnetDevObjPropRefModel.DeviceIdPresent;
                            bacnetDevObjPropRef.arrIndxPresent = bacnetDevObjPropRefModel.ArrIndxPresent;

                            if (bacnetDevObjPropRefModel.DeviceIdPresent)
                            {
                                bacnetDevObjPropRef.deviceType = bacnetDevObjPropRefModel.DeviceType;
                                bacnetDevObjPropRef.deviceInstace = bacnetDevObjPropRefModel.DeviceInstance;
                            }
                            if (bacnetDevObjPropRefModel.ArrIndxPresent)
                            {
                                bacnetDevObjPropRef.arrayIndex = bacnetDevObjPropRefModel.ArrayIndex;
                            }
                        }

                        prBacnetDevObjPropRef.stDevObjPropReff = bacnetDevObjPropRef;
                        ptrPropVal = StructureConverter.StructureToPtr<PrBacnetDevObjPropRef>(prBacnetDevObjPropRef);
                        break;
                    case BacnetDataType.BacnetDTDevObjPropReffArray:
                        hasLazyPointer = true;
                        PrListOfBacnetDevObjPropRef prListOfBacnetDevObjPropRefArray = new PrListOfBacnetDevObjPropRef();

                        prListOfBacnetDevObjPropRefArray = GenerateStructureDevObjPropRefArray(propertyValue, structurePointers);

                        ptrPropVal = StructureConverter.StructureToPtr<PrListOfBacnetDevObjPropRef>(prListOfBacnetDevObjPropRefArray);
                        break;
                    case BacnetDataType.BacnetDTDevObjPropReffList:
                        hasLazyPointer = true;
                        PrListOfBacnetDevObjPropRef prListOfBacnetDevObjPropRef = new PrListOfBacnetDevObjPropRef();
                        prListOfBacnetDevObjPropRef = GenerateStructureDevObjPropRef(propertyValue, structurePointers);

                        ptrPropVal = StructureConverter.StructureToPtr<PrListOfBacnetDevObjPropRef>(prListOfBacnetDevObjPropRef);

                        break;
                    case BacnetDataType.BacnetDTTime:
                        BacnetTimeModel bacnetTimeModel = propertyValue as BacnetTimeModel;
                        PrBacnetTime prBacnetTime = new PrBacnetTime();

                        if (bacnetTimeModel != null)
                        {
                            prBacnetTime.timeVal.hour = bacnetTimeModel.Hour;
                            prBacnetTime.timeVal.min = bacnetTimeModel.Min;
                            prBacnetTime.timeVal.sec = bacnetTimeModel.Sec;
                            prBacnetTime.timeVal.hundredths = bacnetTimeModel.Hundredths;
                        }

                        ptrPropVal = StructureConverter.StructureToPtr<PrBacnetTime>(prBacnetTime);

                        break;
                    case BacnetDataType.BacnetDTDate:
                        BacnetDateModel bacnetDateModel = propertyValue as BacnetDateModel;
                        PrBacnetDate prBacnetDate = new PrBacnetDate();

                        if (bacnetDateModel != null)
                        {
                            prBacnetDate.dateVal.year = bacnetDateModel.Year;
                            prBacnetDate.dateVal.month = bacnetDateModel.Month;
                            prBacnetDate.dateVal.day = bacnetDateModel.Day;
                            prBacnetDate.dateVal.weekDay = bacnetDateModel.WeekDay;
                        }

                        ptrPropVal = StructureConverter.StructureToPtr<PrBacnetDate>(prBacnetDate);
                        break;
                    case BacnetDataType.BacnetDTOctetString:
                        string octetStringVal = propertyValue as string;
                        PrBacnetOctetStr prBacnetOctetStr = new PrBacnetOctetStr();
                        prBacnetOctetStr.stOctetString = SetBacnetOctetStr(octetStringVal);

                        ptrPropVal = StructureConverter.StructureToPtr<PrBacnetOctetStr>(prBacnetOctetStr);
                        break;
                    case BacnetDataType.BacnetDTBitStringNew:
                        string bitStringVal = propertyValue as string;
                        PrBacnetBITStr prBacnetBITStr = new PrBacnetBITStr();
                        prBacnetBITStr.stBitString = SetBitStringNew(bitStringVal);

                        ptrPropVal = StructureConverter.StructureToPtr<PrBacnetBITStr>(prBacnetBITStr);
                        break;
                    case BacnetDataType.BacnetDTMax:
                    case BacnetDataType.BacnetDTNull:
                    case BacnetDataType.BacnetDTEmpty:
                        break;
                    case BacnetDataType.BacnetDTChannelValue:
                        ChannelValueModel channelValueModel = propertyValue as ChannelValueModel;
                        PrBACnetChannelValue prBACnetChannelValue = new PrBACnetChannelValue();
                        prBACnetChannelValue.channelValue = new BACnetChannelValue();
                        prBACnetChannelValue.channelValue.dataType = channelValueModel.DataType;
                        prBACnetChannelValue.channelValue.appTag = channelValueModel.TagType;
                        BacnetPropertyValue bacnetPropertyValue = new BacnetPropertyValue();
                        bacnetPropertyValue.tagType = (byte)channelValueModel.TagType;
                        bacnetPropertyValue.BacnetPropertyValueUnion = new byte[BacDelPropertyDef.BacnetPropertyValueUnionSize];
                        byte[] bacnetPropertyValueUnionByte = SetBacnetPropertyValueUnion(channelValueModel.Value as BacnetValueModel);
                        Buffer.BlockCopy(bacnetPropertyValueUnionByte, 0, bacnetPropertyValue.BacnetPropertyValueUnion, 0, bacnetPropertyValueUnionByte.Length);
                        byte[] propValBytes = StructureConverter.StructureToByteArray<BacnetPropertyValue>(bacnetPropertyValue);
                        prBACnetChannelValue.channelValue.channelValueUnion = new byte[BacDelPropertyDef.ChannelValueUnionSize];
                        Buffer.BlockCopy(propValBytes, 0, prBACnetChannelValue.channelValue.channelValueUnion, 0, propValBytes.Length);

                        ptrPropVal = StructureConverter.StructureToPtr<PrBACnetChannelValue>(prBACnetChannelValue);
                        break;
                    case BacnetDataType.BacnetDTEventParameters:
                        EventParametersBase eventParametersBase = propertyValue as EventParametersBase;
                        PrBacnetEventParameter prBacnetEventParameter = new PrBacnetEventParameter();

                        prBacnetEventParameter = GenerateStructureBacnetEventParameter(eventParametersBase, structurePointers);
                        ptrPropVal = StructureConverter.StructureToPtr<PrBacnetEventParameter>(prBacnetEventParameter);
                        break;
                    case BacnetDataType.BacnetDTObjectTypeSupported:
                        byte[] posArray = propertyValue as byte[];
                        PrBacnetObjectTypesSupported prBacnetObjectTypesSupported = new PrBacnetObjectTypesSupported();

                        prBacnetObjectTypesSupported.objectTypesSupported.value = posArray;
                        ptrPropVal = StructureConverter.StructureToPtr<PrBacnetObjectTypesSupported>(prBacnetObjectTypesSupported);
                        break;
                    case BacnetDataType.BacnetDTServicesSupported:
                        byte[] pssArray = propertyValue as byte[];
                        PrBacnetServicesSupported prBacnetServicesSupported = new PrBacnetServicesSupported();
                        prBacnetServicesSupported.serviceSupported.value = pssArray;
                        ptrPropVal = StructureConverter.StructureToPtr<PrBacnetServicesSupported>(prBacnetServicesSupported);
                        break;
                    case BacnetDataType.BacnetDTDevObjReffArray:
                        hasLazyPointer = true;
                        PrListOfBacnetDevObjRef prListOfBacnetDevObjRefArray = GenerateStructureDevObjRef(propertyValue, structurePointers);
                        ptrPropVal = StructureConverter.StructureToPtr<PrListOfBacnetDevObjRef>(prListOfBacnetDevObjRefArray);
                        break;
                    case BacnetDataType.BacnetDTObjectIDArray:
                        hasLazyPointer = true;
                        PrListOfObjId prListOfObjId = GenerateObjIDRefList(propertyValue, structurePointers);
                        ptrPropVal = StructureConverter.StructureToPtr<PrListOfObjId>(prListOfObjId);
                        break;
                    case BacnetDataType.BacnetDTCharStringArray:
                        hasLazyPointer = true;
                        PrListOfCharStr prListOfCharStr = GenerateStructureListOfCharString(propertyValue, structurePointers);
                        ptrPropVal = StructureConverter.StructureToPtr<PrListOfCharStr>(prListOfCharStr);
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex);
                isError = true;
            }

            return ptrPropVal;
        }

        private static PrListOfBacnetCalendarEntry GenerateListOfBacnetCalendarEntry(object propertyValue, Dictionary<IntPtr, Type> structurePointers)
        {
            PrListOfBacnetCalendarEntry prListOfBacnetCalendarEntry = new PrListOfBacnetCalendarEntry();

            if (propertyValue is ICollection)
            {
                List<BacnetCalendarEntryModel> bacnetCalendarModel = propertyValue as List<BacnetCalendarEntryModel>;
                prListOfBacnetCalendarEntry.count = Convert.ToUInt32(bacnetCalendarModel.Count);

                if (bacnetCalendarModel.Count > 0)
                {
                    IntPtr bacnetCalendarModelPtr = IntPtr.Zero;
                    int bacnetCalendarModelSize = Marshal.SizeOf(typeof(ListOfBacnetCalendarEntry));

                    for (int index = 0; index < bacnetCalendarModel.Count; index++)
                    {
                        ListOfBacnetCalendarEntry listOfBacnetCalendarEntry = new ListOfBacnetCalendarEntry();
                        BacnetCalendarEntryModel calendarModel = new BacnetCalendarEntryModel();
                        calendarModel = bacnetCalendarModel[index];

                        listOfBacnetCalendarEntry.statusCalendar = calendarModel.StatusCalendar;

                        switch (calendarModel.StatusCalendar)
                        {
                            case CalendarEntryStatus.StatusDate:
                                BacnetDateModel bacnetDateModel = calendarModel.BacnetCalenderEntry as BacnetDateModel;
                                BacnetDate bacnetDate = new BacnetDate();
                                bacnetDate.day = bacnetDateModel.Day;
                                bacnetDate.month = bacnetDateModel.Month;
                                bacnetDate.year = bacnetDateModel.Year;
                                bacnetDate.weekDay = bacnetDateModel.WeekDay;
                                listOfBacnetCalendarEntry.bacnetCalEntruUnion.stDate = bacnetDate;
                                break;
                            case CalendarEntryStatus.StatusDateRange:
                                BacnetDateRangeModel bacnetDateRangeModel = calendarModel.BacnetCalenderEntry as BacnetDateRangeModel;
                                BacnetDateRange bacnetDateRange = new BacnetDateRange();
                                bacnetDateRange.stStartDate.day = bacnetDateRangeModel.StartDate.Day;
                                bacnetDateRange.stStartDate.month = bacnetDateRangeModel.StartDate.Month;
                                bacnetDateRange.stStartDate.year = bacnetDateRangeModel.StartDate.Year;
                                bacnetDateRange.stStartDate.weekDay = bacnetDateRangeModel.StartDate.WeekDay;

                                bacnetDateRange.stEndDate.day = bacnetDateRangeModel.EndDate.Day;
                                bacnetDateRange.stEndDate.month = bacnetDateRangeModel.EndDate.Month;
                                bacnetDateRange.stEndDate.year = bacnetDateRangeModel.EndDate.Year;
                                bacnetDateRange.stEndDate.weekDay = bacnetDateRangeModel.EndDate.WeekDay;
                                listOfBacnetCalendarEntry.bacnetCalEntruUnion.stDateRange = bacnetDateRange;
                                break;
                            case CalendarEntryStatus.StatusWeekNDay:
                                WeekNDayModel weekNDayModel = calendarModel.BacnetCalenderEntry as WeekNDayModel;
                                WeekNDay weekNDay = new WeekNDay();
                                weekNDay.month = weekNDayModel.Month;
                                weekNDay.weekNDay = weekNDayModel.WeekNDay;
                                weekNDay.weekOfMonth = weekNDayModel.WeekOfMonth;
                                listOfBacnetCalendarEntry.bacnetCalEntruUnion.stWeekNDay = weekNDay;
                                break;
                            case CalendarEntryStatus.StatusCalReff:
                                break;
                            default:
                                break;
                        }

                        if (index == 0)
                        {
                            if (bacnetCalendarModel.Count > 1)
                            {
                                bacnetCalendarModelPtr = Marshal.AllocHGlobal(bacnetCalendarModelSize);
                                listOfBacnetCalendarEntry.next = bacnetCalendarModelPtr;
                            }

                            prListOfBacnetCalendarEntry.listOfCalendar = StructureConverter.StructureToPtr<ListOfBacnetCalendarEntry>(listOfBacnetCalendarEntry);
                        }
                        else
                        {
                            IntPtr currentPropVal = bacnetCalendarModelPtr;
                            if (index < bacnetCalendarModel.Count - 1)
                            {
                                bacnetCalendarModelPtr = Marshal.AllocHGlobal(bacnetCalendarModelSize);
                                listOfBacnetCalendarEntry.next = bacnetCalendarModelPtr;

                                if (!structurePointers.ContainsKey(bacnetCalendarModelPtr))
                                    structurePointers.Add(bacnetCalendarModelPtr, typeof(ListOfBacnetCalendarEntry));
                            }

                            Marshal.StructureToPtr(listOfBacnetCalendarEntry, currentPropVal, false);
                        }
                    }
                }
            }

            return prListOfBacnetCalendarEntry;
        }

        private static PrListOfBacnetDevObjPropRef GenerateStructureDevObjPropRef(object propertyValue, Dictionary<IntPtr, Type> structurePointers)
        {
            PrListOfBacnetDevObjPropRef prListOfBacnetDevObjPropRef = new PrListOfBacnetDevObjPropRef();

            if (propertyValue is ICollection)
            {
                List<BACnetDevObjPropRefModel> bacnetDevObjPropRefList = propertyValue as List<BACnetDevObjPropRefModel>;

                prListOfBacnetDevObjPropRef.arraySize = (uint)bacnetDevObjPropRefList.Count;

                if (bacnetDevObjPropRefList.Count > 0)
                {
                    IntPtr devObjPropRefPtr = IntPtr.Zero;
                    int devObjPropRefSize = Marshal.SizeOf(typeof(ListOfBacnetDevObjPropRef));

                    ListOfBacnetDevObjPropRef listOfBacnetDevObjPropReff = new ListOfBacnetDevObjPropRef();
                    BACnetDevObjPropRefModel bACnetDevObjPropRefModel = new BACnetDevObjPropRefModel();
                    bACnetDevObjPropRefModel = bacnetDevObjPropRefList[0];
                    BacnetDevObjPropRef devObjPropRef = new BacnetDevObjPropRef();

                    devObjPropRef.propertyIdentifier = bACnetDevObjPropRefModel.PropertyIdentifier;
                    devObjPropRef.objID = bACnetDevObjPropRefModel.ObjId;
                    devObjPropRef.objectType = bACnetDevObjPropRefModel.ObjectType;
                    devObjPropRef.deviceIDPresent = bACnetDevObjPropRefModel.DeviceIdPresent;
                    devObjPropRef.arrIndxPresent = bACnetDevObjPropRefModel.ArrIndxPresent;

                    if (bACnetDevObjPropRefModel.DeviceIdPresent)
                    {
                        devObjPropRef.deviceType = bACnetDevObjPropRefModel.DeviceType;
                        devObjPropRef.deviceInstace = bACnetDevObjPropRefModel.DeviceInstance;
                    }
                    if (bACnetDevObjPropRefModel.ArrIndxPresent)
                    {
                        devObjPropRef.arrayIndex = bACnetDevObjPropRefModel.ArrayIndex;
                    }

                    listOfBacnetDevObjPropReff.stDevObjPropRef = devObjPropRef;

                    if (bacnetDevObjPropRefList.Count > 1)
                    {
                        devObjPropRefPtr = Marshal.AllocHGlobal(devObjPropRefSize);
                        listOfBacnetDevObjPropReff.next = devObjPropRefPtr;

                        if (!structurePointers.ContainsKey(devObjPropRefPtr))
                            structurePointers.Add(devObjPropRefPtr, typeof(ListOfBacnetDevObjPropRef));
                    }

                    IntPtr listOfBacnetDevObjPropRefptr = StructureConverter.StructureToPtr<ListOfBacnetDevObjPropRef>(listOfBacnetDevObjPropReff);
                    prListOfBacnetDevObjPropRef.listOfBACnetDevObjPropReff = listOfBacnetDevObjPropRefptr;

                    if (!structurePointers.ContainsKey(listOfBacnetDevObjPropRefptr))
                        structurePointers.Add(listOfBacnetDevObjPropRefptr, typeof(ListOfBacnetDevObjPropRef));

                    for (int cnt = 1; cnt < bacnetDevObjPropRefList.Count; cnt++)
                    {
                        listOfBacnetDevObjPropReff = new ListOfBacnetDevObjPropRef();

                        bACnetDevObjPropRefModel = new BACnetDevObjPropRefModel();
                        bACnetDevObjPropRefModel = bacnetDevObjPropRefList[cnt];

                        devObjPropRef = new BacnetDevObjPropRef();

                        devObjPropRef.propertyIdentifier = bACnetDevObjPropRefModel.PropertyIdentifier;
                        devObjPropRef.objID = bACnetDevObjPropRefModel.ObjId;
                        devObjPropRef.objectType = bACnetDevObjPropRefModel.ObjectType;
                        devObjPropRef.deviceIDPresent = bACnetDevObjPropRefModel.DeviceIdPresent;
                        devObjPropRef.arrIndxPresent = bACnetDevObjPropRefModel.ArrIndxPresent;

                        if (bACnetDevObjPropRefModel.DeviceIdPresent)
                        {
                            devObjPropRef.deviceType = bACnetDevObjPropRefModel.DeviceType;
                            devObjPropRef.deviceInstace = bACnetDevObjPropRefModel.DeviceInstance;
                        }
                        if (bACnetDevObjPropRefModel.ArrIndxPresent)
                        {
                            devObjPropRef.arrayIndex = bACnetDevObjPropRefModel.ArrayIndex;
                        }

                        listOfBacnetDevObjPropReff.stDevObjPropRef = devObjPropRef;

                        IntPtr currentPropVal = devObjPropRefPtr;

                        if (cnt < bacnetDevObjPropRefList.Count - 1)
                        {
                            devObjPropRefPtr = Marshal.AllocHGlobal(devObjPropRefSize);
                            listOfBacnetDevObjPropReff.next = devObjPropRefPtr;

                            if (!structurePointers.ContainsKey(devObjPropRefPtr))
                                structurePointers.Add(devObjPropRefPtr, typeof(ListOfBacnetDevObjPropRef));
                        }

                        Marshal.StructureToPtr(listOfBacnetDevObjPropReff, currentPropVal, false);
                    }
                }

            }
            return prListOfBacnetDevObjPropRef;
        }

        private static PrListOfBacnetDevObjPropRef GenerateStructureDevObjPropRefArray(object propertyValue, Dictionary<IntPtr, Type> structurePointers)
        {
            PrListOfBacnetDevObjPropRef prListOfBacnetDevObjPropRefArray = new PrListOfBacnetDevObjPropRef();

            if (propertyValue is ICollection)
            {
                List<BACnetDevObjPropRefModel> bacnetDevObjPropRefArray = propertyValue as List<BACnetDevObjPropRefModel>;
                prListOfBacnetDevObjPropRefArray.arraySize = (uint)bacnetDevObjPropRefArray.Count;

                if (bacnetDevObjPropRefArray.Count > 0)
                {
                    IntPtr devObjPropRefPtr = IntPtr.Zero;
                    int devObjPropRefSize = Marshal.SizeOf(typeof(ListOfBacnetDevObjPropRef));

                    ListOfBacnetDevObjPropRef listOfBacnetDevObjPropRef = new ListOfBacnetDevObjPropRef();

                    BACnetDevObjPropRefModel bACnetDevObjPropRefModel = new BACnetDevObjPropRefModel();
                    bACnetDevObjPropRefModel = bacnetDevObjPropRefArray[0];

                    BacnetDevObjPropRef devObjPropRef = new BacnetDevObjPropRef();

                    devObjPropRef.propertyIdentifier = bACnetDevObjPropRefModel.PropertyIdentifier;
                    devObjPropRef.objID = bACnetDevObjPropRefModel.ObjId;
                    devObjPropRef.objectType = bACnetDevObjPropRefModel.ObjectType;
                    devObjPropRef.deviceIDPresent = bACnetDevObjPropRefModel.DeviceIdPresent;
                    devObjPropRef.arrIndxPresent = bACnetDevObjPropRefModel.ArrIndxPresent;

                    if (bACnetDevObjPropRefModel.DeviceIdPresent)
                    {
                        devObjPropRef.deviceType = bACnetDevObjPropRefModel.DeviceType;
                        devObjPropRef.deviceInstace = bACnetDevObjPropRefModel.DeviceInstance;
                    }
                    if (bACnetDevObjPropRefModel.ArrIndxPresent)
                    {
                        devObjPropRef.arrayIndex = bACnetDevObjPropRefModel.ArrayIndex;
                    }

                    listOfBacnetDevObjPropRef.stDevObjPropRef = devObjPropRef;

                    if (bacnetDevObjPropRefArray.Count > 1)
                    {
                        devObjPropRefPtr = Marshal.AllocHGlobal(devObjPropRefSize);
                        listOfBacnetDevObjPropRef.next = devObjPropRefPtr;

                        if (!structurePointers.ContainsKey(devObjPropRefPtr))
                            structurePointers.Add(devObjPropRefPtr, typeof(ListOfBacnetDevObjPropRef));
                    }

                    IntPtr listOfBacnetDevObjPropRefptrr = StructureConverter.StructureToPtr<ListOfBacnetDevObjPropRef>(listOfBacnetDevObjPropRef);
                    prListOfBacnetDevObjPropRefArray.listOfBACnetDevObjPropReff = listOfBacnetDevObjPropRefptrr;

                    if (!structurePointers.ContainsKey(listOfBacnetDevObjPropRefptrr))
                        structurePointers.Add(listOfBacnetDevObjPropRefptrr, typeof(ListOfBacnetDevObjPropRef));

                    for (int cnt = 1; cnt < bacnetDevObjPropRefArray.Count; cnt++)
                    {
                        listOfBacnetDevObjPropRef = new ListOfBacnetDevObjPropRef();

                        bACnetDevObjPropRefModel = new BACnetDevObjPropRefModel();
                        bACnetDevObjPropRefModel = bacnetDevObjPropRefArray[cnt];

                        devObjPropRef = new BacnetDevObjPropRef();

                        devObjPropRef.propertyIdentifier = bACnetDevObjPropRefModel.PropertyIdentifier;
                        devObjPropRef.objID = bACnetDevObjPropRefModel.ObjId;
                        devObjPropRef.objectType = bACnetDevObjPropRefModel.ObjectType;
                        devObjPropRef.deviceIDPresent = bACnetDevObjPropRefModel.DeviceIdPresent;
                        devObjPropRef.arrIndxPresent = bACnetDevObjPropRefModel.ArrIndxPresent;

                        if (bACnetDevObjPropRefModel.DeviceIdPresent)
                        {
                            devObjPropRef.deviceType = bACnetDevObjPropRefModel.DeviceType;
                            devObjPropRef.deviceInstace = bACnetDevObjPropRefModel.DeviceInstance;
                        }
                        if (bACnetDevObjPropRefModel.ArrIndxPresent)
                        {
                            devObjPropRef.arrayIndex = bACnetDevObjPropRefModel.ArrayIndex;
                        }

                        listOfBacnetDevObjPropRef.stDevObjPropRef = devObjPropRef;

                        IntPtr currentPropVal = devObjPropRefPtr;

                        if (cnt < bacnetDevObjPropRefArray.Count - 1)
                        {
                            devObjPropRefPtr = Marshal.AllocHGlobal(devObjPropRefSize);
                            listOfBacnetDevObjPropRef.next = devObjPropRefPtr;

                            if (!structurePointers.ContainsKey(devObjPropRefPtr))
                                structurePointers.Add(devObjPropRefPtr, typeof(ListOfBacnetDevObjPropRef));
                        }

                        Marshal.StructureToPtr(listOfBacnetDevObjPropRef, currentPropVal, false);
                    }
                }
            }
            return prListOfBacnetDevObjPropRefArray;
        }

        private static AnyValue GenerateStructureAnyValue(object propertyValue)
        {
            AnyValue anyValue = new AnyValue();

            BacnetValueModel bacnetPropertyValueModel = propertyValue as BacnetValueModel;
            BacnetPropertyValue bacnetPropertyValue = new BacnetPropertyValue();
            bacnetPropertyValue.BacnetPropertyValueUnion = new byte[BacDelPropertyDef.BacnetPropertyValueUnionSize];

            if (bacnetPropertyValueModel != null)
            {
                bacnetPropertyValue.tagType = bacnetPropertyValueModel.TagType;

                if (bacnetPropertyValueModel.TagType != 0)
                {
                    byte[] bacnetPropertyValueUnionBytes = SetBacnetPropertyValueUnion(bacnetPropertyValueModel);
                    Buffer.BlockCopy(bacnetPropertyValueUnionBytes, 0, bacnetPropertyValue.BacnetPropertyValueUnion, 0, bacnetPropertyValueUnionBytes.Length);
                }
            }

            anyValue.propValue = bacnetPropertyValue;

            return anyValue;
        }

        private static PrBinaryEnumPV GenerateStructureBinaryEnumPV(object propertyValue, Dictionary<IntPtr, Type> structurePointers)
        {
            PrBinaryEnumPV prBinaryEnumPV = new PrBinaryEnumPV();
            BacnetBinaryPV bacnetBinaryPV;

            if (propertyValue is ICollection)
            {
                List<object> prVal = propertyValue as List<object>;

                if (prVal.Count > 0)
                {
                    int prBinaryEnumPVSize = Marshal.SizeOf(typeof(PrBinaryEnumPV));
                    PrBinaryEnumPV prBinaryEnumPVTemp = new PrBinaryEnumPV();
                    Enum.TryParse<BacnetBinaryPV>(prVal[0].ToString(), out bacnetBinaryPV);
                    prBinaryEnumPVTemp.val = bacnetBinaryPV;

                    IntPtr nextBinaryEnumPVPtr = IntPtr.Zero;

                    if (prVal.Count > 1)
                    {
                        nextBinaryEnumPVPtr = Marshal.AllocHGlobal(prBinaryEnumPVSize);
                        prBinaryEnumPVTemp.next = nextBinaryEnumPVPtr;

                        if (!structurePointers.ContainsKey(nextBinaryEnumPVPtr))
                            structurePointers.Add(nextBinaryEnumPVPtr, typeof(PrBinaryEnumPV));
                    }

                    prBinaryEnumPV = prBinaryEnumPVTemp;

                    for (int i = 1; i < prVal.Count; i++)
                    {
                        prBinaryEnumPVTemp = new PrBinaryEnumPV();
                        bacnetBinaryPV = new BacnetBinaryPV();
                        Enum.TryParse<BacnetBinaryPV>(prVal[i].ToString(), out bacnetBinaryPV);

                        IntPtr current = nextBinaryEnumPVPtr;

                        if (i < prVal.Count - 1)
                        {
                            nextBinaryEnumPVPtr = Marshal.AllocHGlobal(prBinaryEnumPVSize);
                            prBinaryEnumPVTemp.next = nextBinaryEnumPVPtr;

                            if (!structurePointers.ContainsKey(nextBinaryEnumPVPtr))
                                structurePointers.Add(nextBinaryEnumPVPtr, typeof(PrBinaryEnumPV));
                        }

                        Marshal.StructureToPtr(prBinaryEnumPVTemp, current, false);
                    }
                }
            }
            else
            {
                Enum.TryParse<BacnetBinaryPV>(propertyValue.ToString(), out bacnetBinaryPV);
                prBinaryEnumPV.val = bacnetBinaryPV;
            }

            return prBinaryEnumPV;
        }

        private static BacnetBitStr SetStatusFlagBits(StatusFlagsModel statusFlagsModel)
        {
            BacnetBitStr bacnetBitStr = new BacnetBitStr();
            byte statusFlagByte = 0x00;

            if (statusFlagsModel != null)
            {
                if (statusFlagsModel.InAlarm)
                {
                    statusFlagByte = statusFlagByte.SetBit(7);
                }

                if (statusFlagsModel.Fault)
                {
                    statusFlagByte = statusFlagByte.SetBit(6);
                }

                if (statusFlagsModel.Overridden)
                {
                    statusFlagByte = statusFlagByte.SetBit(5);
                }

                if (statusFlagsModel.OutOfService)
                {
                    statusFlagByte = statusFlagByte.SetBit(4);
                }
            }

            byte[] bitStringBytes = new byte[1];
            bitStringBytes[0] = statusFlagByte;

            bacnetBitStr.byteCnt = 1;
            bacnetBitStr.unusedBits = 4;
            bacnetBitStr.transBits = bitStringBytes;

            return bacnetBitStr;
        }

        private static BacnetBitStr SetEventTransitionBits(EventTransitionBitsModel eventTransitionBitsModel)
        {
            BacnetBitStr bacnetBitStr = new BacnetBitStr();
            byte statusFlagByte = 0x00;

            if (eventTransitionBitsModel != null)
            {
                if (eventTransitionBitsModel.ToOffNormal)
                {
                    statusFlagByte = statusFlagByte.SetBit(7);
                }

                if (eventTransitionBitsModel.ToFault)
                {
                    statusFlagByte = statusFlagByte.SetBit(6);
                }

                if (eventTransitionBitsModel.ToNormal)
                {
                    statusFlagByte = statusFlagByte.SetBit(5);
                }
            }

            byte[] bitStringBytes = new byte[1];
            bitStringBytes[0] = statusFlagByte;

            bacnetBitStr.byteCnt = 1;
            bacnetBitStr.unusedBits = 5;
            bacnetBitStr.transBits = bitStringBytes;

            return bacnetBitStr;
        }

        private static BacnetBitStr SetLimitEnableBits(LimitEnableBitsModel limitEnableBitsModel)
        {
            BacnetBitStr bacnetBitStr = new BacnetBitStr();

            byte statusFlagByte = 0x00;

            if (limitEnableBitsModel != null)
            {
                if (limitEnableBitsModel.LowLimit)
                {
                    statusFlagByte = statusFlagByte.SetBit(7);
                }

                if (limitEnableBitsModel.HighLimit)
                {
                    statusFlagByte = statusFlagByte.SetBit(6);
                }
            }

            byte[] bitStringBytes = new byte[1];
            bitStringBytes[0] = statusFlagByte;

            bacnetBitStr.byteCnt = 1;
            bacnetBitStr.unusedBits = 6;
            bacnetBitStr.transBits = bitStringBytes;

            return bacnetBitStr;
        }

        private static BacnetBitStr SetDaysOfWeekBits(DaysOfWeekBitsModel daysOfWeekBitsModel)
        {
            BacnetBitStr bacnetBitStr = new BacnetBitStr();
            byte statusFlagByte = 0x00;

            if (daysOfWeekBitsModel.Monday)
            {
                statusFlagByte = statusFlagByte.SetBit(7);
            }

            if (daysOfWeekBitsModel.Tuesday)
            {
                statusFlagByte = statusFlagByte.SetBit(6);
            }

            if (daysOfWeekBitsModel.Wednesday)
            {
                statusFlagByte = statusFlagByte.SetBit(5);
            }

            if (daysOfWeekBitsModel.Thursday)
            {
                statusFlagByte = statusFlagByte.SetBit(4);
            }

            if (daysOfWeekBitsModel.Friday)
            {
                statusFlagByte = statusFlagByte.SetBit(3);
            }

            if (daysOfWeekBitsModel.Saturday)
            {
                statusFlagByte = statusFlagByte.SetBit(2);
            }

            if (daysOfWeekBitsModel.Sunday)
            {
                statusFlagByte = statusFlagByte.SetBit(1);
            }

            byte[] bitStringBytes = new byte[1];
            bitStringBytes[0] = statusFlagByte;

            bacnetBitStr.byteCnt = 1;
            bacnetBitStr.unusedBits = 1;
            bacnetBitStr.transBits = bitStringBytes;

            return bacnetBitStr;
        }

        private static PrListOfBacnetSpecialEvent GenerateStructureSpecialEvent(object propertyValue, Dictionary<IntPtr, Type> structurePointers)
        {
            PrListOfBacnetSpecialEvent prListOfBacnetSpecialEvent = new PrListOfBacnetSpecialEvent();

            if (propertyValue is ICollection)
            {
                List<BacnetSpecialEventModel> bacnetSpecialEventModelList = propertyValue as List<BacnetSpecialEventModel>;

                prListOfBacnetSpecialEvent.count = (uint)bacnetSpecialEventModelList.Count;

                if (bacnetSpecialEventModelList.Count > 0)
                {
                    IntPtr nextSpecialEventPtr = IntPtr.Zero;
                    int listOfSpecialEventSize = Marshal.SizeOf(typeof(ListOfSpecialEvent));

                    ListOfSpecialEvent listOfSpecialEvent = new ListOfSpecialEvent();
                    BacnetSpecialEventModel bacnetSpecialEventModel = bacnetSpecialEventModelList[0];

                    BacnetSpecialEvent bacnetSpecialEvent = new BacnetSpecialEvent();
                    bacnetSpecialEvent.eventPriority = bacnetSpecialEventModel.EventPriority;
                    bacnetSpecialEvent.statusCalendar = bacnetSpecialEventModel.StatusCalendar;

                    if (bacnetSpecialEventModel.StatusCalendar == CalendarEntryStatus.StatusCalReff)
                    {
                        ObjectIdentifierModel objectIdentifierMod = new ObjectIdentifierModel();
                        objectIdentifierMod = bacnetSpecialEventModel.CalenderReference as ObjectIdentifierModel;

                        bacnetSpecialEvent.bacnetSpecialEventUnion.calReff.objectType = objectIdentifierMod.ObjectType;
                        bacnetSpecialEvent.bacnetSpecialEventUnion.calReff.objId = objectIdentifierMod.InstanceNumber;
                    }
                    else
                    {
                        if (bacnetSpecialEventModel.StatusCalendar == CalendarEntryStatus.StatusDate)
                        {
                            BacnetDateModel bacnetDateModel = new BacnetDateModel();
                            bacnetDateModel = bacnetSpecialEventModel.CalenderReference as BacnetDateModel;

                            if (bacnetDateModel != null)
                            {
                                bacnetSpecialEvent.bacnetSpecialEventUnion.stDate.day = bacnetDateModel.Day;
                                bacnetSpecialEvent.bacnetSpecialEventUnion.stDate.month = bacnetDateModel.Month;
                                bacnetSpecialEvent.bacnetSpecialEventUnion.stDate.year = bacnetDateModel.Year;
                                bacnetSpecialEvent.bacnetSpecialEventUnion.stDate.weekDay = bacnetDateModel.WeekDay;
                            }
                        }
                        else if (bacnetSpecialEventModel.StatusCalendar == CalendarEntryStatus.StatusDateRange)
                        {
                            BacnetDateRangeModel bacnetDateRangeModel = new BacnetDateRangeModel();
                            bacnetDateRangeModel = bacnetSpecialEventModel.CalenderReference as BacnetDateRangeModel;

                            if (bacnetDateRangeModel != null)
                            {
                                bacnetSpecialEvent.bacnetSpecialEventUnion.stDateRange.stStartDate.day = bacnetDateRangeModel.StartDate.Day;
                                bacnetSpecialEvent.bacnetSpecialEventUnion.stDateRange.stStartDate.month = bacnetDateRangeModel.StartDate.Month;
                                bacnetSpecialEvent.bacnetSpecialEventUnion.stDateRange.stStartDate.year = bacnetDateRangeModel.StartDate.Year;
                                bacnetSpecialEvent.bacnetSpecialEventUnion.stDateRange.stStartDate.weekDay = bacnetDateRangeModel.StartDate.WeekDay;

                                bacnetSpecialEvent.bacnetSpecialEventUnion.stDateRange.stEndDate.day = bacnetDateRangeModel.EndDate.Day;
                                bacnetSpecialEvent.bacnetSpecialEventUnion.stDateRange.stEndDate.month = bacnetDateRangeModel.EndDate.Month;
                                bacnetSpecialEvent.bacnetSpecialEventUnion.stDateRange.stEndDate.year = bacnetDateRangeModel.EndDate.Year;
                                bacnetSpecialEvent.bacnetSpecialEventUnion.stDateRange.stEndDate.weekDay = bacnetDateRangeModel.EndDate.WeekDay;
                            }
                        }
                        else if (bacnetSpecialEventModel.StatusCalendar == CalendarEntryStatus.StatusWeekNDay)
                        {
                            WeekNDayModel weekNDayModel = new WeekNDayModel();
                            weekNDayModel = bacnetSpecialEventModel.CalenderReference as WeekNDayModel;

                            if (weekNDayModel != null)
                            {
                                bacnetSpecialEvent.bacnetSpecialEventUnion.stWeekNDay.month = weekNDayModel.Month;
                                bacnetSpecialEvent.bacnetSpecialEventUnion.stWeekNDay.weekNDay = weekNDayModel.WeekNDay;
                                bacnetSpecialEvent.bacnetSpecialEventUnion.stWeekNDay.weekOfMonth = weekNDayModel.WeekOfMonth;
                            }
                        }
                    }

                    BacnetTimeValue bacnetTimeValue = new BacnetTimeValue();
                    BacnetTimeValueModel bacnetTimeValueModel = new BacnetTimeValueModel();
                    BacnetPropertyValue bacnetPropertyValue = new BacnetPropertyValue();
                    BacnetValueModel bacnetValueModel = new BacnetValueModel();
                    byte[] bacPropertyValueUnionByte = new byte[264];

                    if (bacnetSpecialEventModel.BacnetTimeValue.Count > 0)
                    {
                        bacnetTimeValue.isUsed = true;

                        bacnetTimeValueModel = bacnetSpecialEventModel.BacnetTimeValue[0];

                        bacnetTimeValue.stTime = new BacnetTime();
                        bacnetTimeValue.stTime.hour = bacnetTimeValueModel.Time.Hour;
                        bacnetTimeValue.stTime.min = bacnetTimeValueModel.Time.Min;
                        bacnetTimeValue.stTime.sec = bacnetTimeValueModel.Time.Sec;
                        bacnetTimeValue.stTime.hundredths = bacnetTimeValueModel.Time.Hundredths;

                        if (bacnetTimeValueModel.Values.Count > 0)
                        {
                            bacnetValueModel = bacnetTimeValueModel.Values[0];
                            bacnetPropertyValue.tagType = bacnetValueModel.TagType;

                            if (bacnetValueModel.TagType != 0)
                                bacPropertyValueUnionByte = SetBacnetPropertyValueUnion(bacnetValueModel);
                        }
                    }

                    bacnetPropertyValue.BacnetPropertyValueUnion = new byte[BacDelPropertyDef.BacnetPropertyValueUnionSize];
                    Buffer.BlockCopy(bacPropertyValueUnionByte, 0, bacnetPropertyValue.BacnetPropertyValueUnion, 0, bacPropertyValueUnionByte.Length);

                    bacnetTimeValue.bacnetPropVal = bacnetPropertyValue;

                    IntPtr bacnetValuePtr = IntPtr.Zero;
                    int bacnetPropValueSize = Marshal.SizeOf(typeof(BacnetPropertyValue));

                    if (bacnetTimeValueModel.Values.Count > 1)
                    {
                        bacnetValuePtr = Marshal.AllocHGlobal(bacnetPropValueSize);
                        bacnetTimeValue.bacnetPropVal.nextPropVal = bacnetValuePtr;

                        if (!structurePointers.ContainsKey(bacnetValuePtr))
                            structurePointers.Add(bacnetValuePtr, typeof(BacnetPropertyValue));
                    }

                    for (int valcnt = 1; valcnt < bacnetTimeValueModel.Values.Count; valcnt++)
                    {
                        bacnetPropertyValue = new BacnetPropertyValue();

                        bacnetValueModel = new BacnetValueModel();
                        bacnetValueModel = bacnetTimeValueModel.Values[valcnt];

                        bacnetPropertyValue.tagType = bacnetValueModel.TagType;

                        bacnetPropertyValue.BacnetPropertyValueUnion = new byte[BacDelPropertyDef.BacnetPropertyValueUnionSize];

                        if (bacnetValueModel.TagType != 0)
                        {
                            byte[] bacPropertyValueUnionByt = SetBacnetPropertyValueUnion(bacnetValueModel);
                            Buffer.BlockCopy(bacPropertyValueUnionByt, 0, bacnetPropertyValue.BacnetPropertyValueUnion, 0, bacPropertyValueUnionByt.Length);
                        }

                        IntPtr currentPropVal = bacnetValuePtr;

                        if (valcnt < bacnetTimeValueModel.Values.Count - 1)
                        {
                            bacnetValuePtr = Marshal.AllocHGlobal(bacnetPropValueSize);
                            bacnetPropertyValue.nextPropVal = bacnetValuePtr;

                            if (!structurePointers.ContainsKey(bacnetValuePtr))
                                structurePointers.Add(bacnetValuePtr, typeof(BacnetPropertyValue));
                        }

                        Marshal.StructureToPtr(bacnetPropertyValue, currentPropVal, false);
                    }

                    IntPtr bacnetTimeValuePtr = IntPtr.Zero;
                    int bacnetTimeValueSize = Marshal.SizeOf(typeof(BacnetTimeValue));

                    if (bacnetSpecialEventModel.BacnetTimeValue.Count > 1)
                    {
                        bacnetTimeValuePtr = Marshal.AllocHGlobal(bacnetTimeValueSize);
                        bacnetTimeValue.next = bacnetTimeValuePtr;

                        if (!structurePointers.ContainsKey(bacnetTimeValuePtr))
                            structurePointers.Add(bacnetTimeValuePtr, typeof(BacnetTimeValue));
                    }

                    bacnetSpecialEvent.stListOfTimeValues = bacnetTimeValue;
                    listOfSpecialEvent.bacnetSpecialEvt = bacnetSpecialEvent;

                    for (int countTM = 1; countTM < bacnetSpecialEventModel.BacnetTimeValue.Count; countTM++)
                    {
                        bacnetTimeValue = new BacnetTimeValue();
                        bacnetTimeValueModel = new BacnetTimeValueModel();
                        bacnetTimeValueModel = bacnetSpecialEventModel.BacnetTimeValue[countTM];

                        bacnetTimeValue.stTime = new BacnetTime();
                        bacnetTimeValue.stTime.hour = bacnetTimeValueModel.Time.Hour;
                        bacnetTimeValue.stTime.min = bacnetTimeValueModel.Time.Min;
                        bacnetTimeValue.stTime.sec = bacnetTimeValueModel.Time.Sec;
                        bacnetTimeValue.stTime.hundredths = bacnetTimeValueModel.Time.Hundredths;

                        bacnetTimeValue.bacnetPropVal = new BacnetPropertyValue();
                        byte[] bacnetPropertyValueUnionBytes = new byte[264];

                        if (bacnetTimeValueModel.Values.Count > 0)
                        {
                            bacnetValueModel = bacnetTimeValueModel.Values[0];
                            bacnetPropertyValue = new BacnetPropertyValue();
                            bacnetPropertyValue.tagType = bacnetValueModel.TagType;

                            if (bacnetValueModel.TagType != 0)
                                bacnetPropertyValueUnionBytes = SetBacnetPropertyValueUnion(bacnetValueModel);
                        }

                        bacnetPropertyValue.BacnetPropertyValueUnion = new byte[BacDelPropertyDef.BacnetPropertyValueUnionSize];
                        Buffer.BlockCopy(bacnetPropertyValueUnionBytes, 0, bacnetPropertyValue.BacnetPropertyValueUnion, 0, bacnetPropertyValueUnionBytes.Length);

                        bacnetTimeValue.bacnetPropVal = bacnetPropertyValue;
                        IntPtr nextpropValuePtr = IntPtr.Zero;

                        if (bacnetTimeValueModel.Values.Count > 1)
                        {
                            nextpropValuePtr = Marshal.AllocHGlobal(bacnetPropValueSize);
                            bacnetTimeValue.bacnetPropVal.nextPropVal = nextpropValuePtr;

                            if (!structurePointers.ContainsKey(nextpropValuePtr))
                                structurePointers.Add(nextpropValuePtr, typeof(BacnetPropertyValue));
                        }

                        for (int cnt = 1; cnt < bacnetTimeValueModel.Values.Count; cnt++)
                        {
                            bacnetPropertyValue = new BacnetPropertyValue();

                            bacnetValueModel = new BacnetValueModel();
                            bacnetValueModel = bacnetTimeValueModel.Values[cnt];

                            bacnetPropertyValue.tagType = bacnetValueModel.TagType;
                            bacnetPropertyValue.BacnetPropertyValueUnion = new byte[BacDelPropertyDef.BacnetPropertyValueUnionSize];

                            if (bacnetValueModel.TagType != 0)
                            {
                                byte[] bacnetPropertyValueUnionByte = SetBacnetPropertyValueUnion(bacnetValueModel);
                                Buffer.BlockCopy(bacnetPropertyValueUnionByte, 0, bacnetPropertyValue.BacnetPropertyValueUnion, 0, bacnetPropertyValueUnionByte.Length);
                            }

                            IntPtr currentProppertyVal = nextpropValuePtr;

                            if (cnt < bacnetTimeValueModel.Values.Count - 1)
                            {
                                nextpropValuePtr = Marshal.AllocHGlobal(bacnetPropValueSize);
                                bacnetPropertyValue.nextPropVal = nextpropValuePtr;

                                if (!structurePointers.ContainsKey(nextpropValuePtr))
                                    structurePointers.Add(nextpropValuePtr, typeof(BacnetPropertyValue));
                            }

                            Marshal.StructureToPtr(bacnetPropertyValue, currentProppertyVal, false);
                        }

                        IntPtr currentTimeValue = bacnetTimeValuePtr;

                        if (countTM < bacnetSpecialEventModel.BacnetTimeValue.Count - 1)
                        {
                            bacnetTimeValuePtr = Marshal.AllocHGlobal(bacnetTimeValueSize);
                            bacnetTimeValue.next = bacnetTimeValuePtr;

                            if (!structurePointers.ContainsKey(bacnetTimeValuePtr))
                                structurePointers.Add(bacnetTimeValuePtr, typeof(BacnetTimeValue));
                        }

                        Marshal.StructureToPtr(bacnetTimeValue, currentTimeValue, false);
                    }

                    if (bacnetSpecialEventModelList.Count > 1)
                    {
                        nextSpecialEventPtr = Marshal.AllocHGlobal(listOfSpecialEventSize);
                        listOfSpecialEvent.next = nextSpecialEventPtr;

                        if (!structurePointers.ContainsKey(nextSpecialEventPtr))
                            structurePointers.Add(nextSpecialEventPtr, typeof(ListOfSpecialEvent));
                    }

                    prListOfBacnetSpecialEvent.splEvent = StructureConverter.StructureToPtr<ListOfSpecialEvent>(listOfSpecialEvent);

                    for (int count = 1; count < bacnetSpecialEventModelList.Count; count++)
                    {
                        listOfSpecialEvent = new ListOfSpecialEvent();
                        bacnetSpecialEventModel = bacnetSpecialEventModelList[count];

                        listOfSpecialEvent.bacnetSpecialEvt = new BacnetSpecialEvent();
                        listOfSpecialEvent.bacnetSpecialEvt.eventPriority = bacnetSpecialEventModel.EventPriority;
                        listOfSpecialEvent.bacnetSpecialEvt.statusCalendar = bacnetSpecialEventModel.StatusCalendar;

                        if (bacnetSpecialEventModel.StatusCalendar == CalendarEntryStatus.StatusCalReff)
                        {
                            ObjectIdentifierModel objectIdentifierMod = new ObjectIdentifierModel();
                            objectIdentifierMod = bacnetSpecialEventModel.CalenderReference as ObjectIdentifierModel;
                            listOfSpecialEvent.bacnetSpecialEvt.bacnetSpecialEventUnion.calReff.objectType = objectIdentifierMod.ObjectType;
                            listOfSpecialEvent.bacnetSpecialEvt.bacnetSpecialEventUnion.calReff.objId = objectIdentifierMod.InstanceNumber;
                        }
                        else
                        {
                            if (bacnetSpecialEventModel.StatusCalendar == CalendarEntryStatus.StatusDate)
                            {
                                BacnetDateModel bacnetDateModel = new BacnetDateModel();
                                bacnetDateModel = bacnetSpecialEventModel.CalenderReference as BacnetDateModel;

                                if (bacnetDateModel != null)
                                {
                                    listOfSpecialEvent.bacnetSpecialEvt.bacnetSpecialEventUnion.stDate.day = bacnetDateModel.Day;
                                    listOfSpecialEvent.bacnetSpecialEvt.bacnetSpecialEventUnion.stDate.month = bacnetDateModel.Month;
                                    listOfSpecialEvent.bacnetSpecialEvt.bacnetSpecialEventUnion.stDate.year = bacnetDateModel.Year;
                                    listOfSpecialEvent.bacnetSpecialEvt.bacnetSpecialEventUnion.stDate.weekDay = bacnetDateModel.WeekDay;
                                }
                            }
                            else if (bacnetSpecialEventModel.StatusCalendar == CalendarEntryStatus.StatusDateRange)
                            {
                                BacnetDateRangeModel bacnetDateRangeModel = new BacnetDateRangeModel();
                                bacnetDateRangeModel = bacnetSpecialEventModel.CalenderReference as BacnetDateRangeModel;

                                if (bacnetDateRangeModel != null)
                                {
                                    listOfSpecialEvent.bacnetSpecialEvt.bacnetSpecialEventUnion.stDateRange.stStartDate.day = bacnetDateRangeModel.StartDate.Day;
                                    listOfSpecialEvent.bacnetSpecialEvt.bacnetSpecialEventUnion.stDateRange.stStartDate.month = bacnetDateRangeModel.StartDate.Month;
                                    listOfSpecialEvent.bacnetSpecialEvt.bacnetSpecialEventUnion.stDateRange.stStartDate.year = bacnetDateRangeModel.StartDate.Year;
                                    listOfSpecialEvent.bacnetSpecialEvt.bacnetSpecialEventUnion.stDateRange.stStartDate.weekDay = bacnetDateRangeModel.StartDate.WeekDay;

                                    listOfSpecialEvent.bacnetSpecialEvt.bacnetSpecialEventUnion.stDateRange.stEndDate.day = bacnetDateRangeModel.EndDate.Day;
                                    listOfSpecialEvent.bacnetSpecialEvt.bacnetSpecialEventUnion.stDateRange.stEndDate.month = bacnetDateRangeModel.EndDate.Month;
                                    listOfSpecialEvent.bacnetSpecialEvt.bacnetSpecialEventUnion.stDateRange.stEndDate.year = bacnetDateRangeModel.EndDate.Year;
                                    listOfSpecialEvent.bacnetSpecialEvt.bacnetSpecialEventUnion.stDateRange.stEndDate.weekDay = bacnetDateRangeModel.EndDate.WeekDay;
                                }
                            }
                            else if (bacnetSpecialEventModel.StatusCalendar == CalendarEntryStatus.StatusWeekNDay)
                            {
                                WeekNDayModel weekNDayModel = new WeekNDayModel();
                                weekNDayModel = bacnetSpecialEventModel.CalenderReference as WeekNDayModel;

                                if (weekNDayModel != null)
                                {
                                    listOfSpecialEvent.bacnetSpecialEvt.bacnetSpecialEventUnion.stWeekNDay.month = weekNDayModel.Month;
                                    listOfSpecialEvent.bacnetSpecialEvt.bacnetSpecialEventUnion.stWeekNDay.weekNDay = weekNDayModel.WeekNDay;
                                    listOfSpecialEvent.bacnetSpecialEvt.bacnetSpecialEventUnion.stWeekNDay.weekOfMonth = weekNDayModel.WeekOfMonth;
                                }
                            }
                        }

                        bacnetTimeValue = new BacnetTimeValue();
                        byte[] bacPropertyValueUnionBytes = new byte[264];

                        if (bacnetSpecialEventModel.BacnetTimeValue.Count > 0)
                        {
                            bacnetTimeValue.isUsed = true;

                            bacnetTimeValueModel = new BacnetTimeValueModel();
                            bacnetTimeValueModel = bacnetSpecialEventModel.BacnetTimeValue[0];

                            bacnetTimeValue.stTime = new BacnetTime();
                            bacnetTimeValue.stTime.hour = bacnetTimeValueModel.Time.Hour;
                            bacnetTimeValue.stTime.min = bacnetTimeValueModel.Time.Min;
                            bacnetTimeValue.stTime.sec = bacnetTimeValueModel.Time.Sec;
                            bacnetTimeValue.stTime.hundredths = bacnetTimeValueModel.Time.Hundredths;

                            bacnetTimeValue.bacnetPropVal = new BacnetPropertyValue();

                            if (bacnetTimeValueModel.Values.Count > 0)
                            {
                                bacnetValueModel = bacnetTimeValueModel.Values[0];
                                bacnetPropertyValue = new BacnetPropertyValue();
                                bacnetPropertyValue.tagType = bacnetValueModel.TagType;

                                if (bacnetValueModel.TagType != 0)
                                    bacPropertyValueUnionBytes = SetBacnetPropertyValueUnion(bacnetValueModel);
                            }
                        }

                        bacnetPropertyValue.BacnetPropertyValueUnion = new byte[BacDelPropertyDef.BacnetPropertyValueUnionSize];
                        Buffer.BlockCopy(bacPropertyValueUnionBytes, 0, bacnetPropertyValue.BacnetPropertyValueUnion, 0, bacPropertyValueUnionBytes.Length);
                        bacnetTimeValue.bacnetPropVal = bacnetPropertyValue;

                        IntPtr bacnetValPtr = IntPtr.Zero;
                        int bacnetPropValSize = Marshal.SizeOf(typeof(BacnetPropertyValue));

                        if (bacnetTimeValueModel.Values.Count > 1)
                        {
                            bacnetValPtr = Marshal.AllocHGlobal(bacnetPropValSize);
                            bacnetTimeValue.bacnetPropVal.nextPropVal = bacnetValPtr;

                            if (!structurePointers.ContainsKey(bacnetValPtr))
                                structurePointers.Add(bacnetValPtr, typeof(BacnetPropertyValue));
                        }

                        for (int valcnt = 1; valcnt < bacnetTimeValueModel.Values.Count; valcnt++)
                        {
                            bacnetPropertyValue = new BacnetPropertyValue();

                            bacnetValueModel = new BacnetValueModel();
                            bacnetValueModel = bacnetTimeValueModel.Values[valcnt];

                            bacnetPropertyValue.tagType = bacnetValueModel.TagType;
                            bacnetPropertyValue.BacnetPropertyValueUnion = new byte[BacDelPropertyDef.BacnetPropertyValueUnionSize];

                            if (bacnetValueModel.TagType != 0)
                            {
                                byte[] bacPropertyValueUnionByt = SetBacnetPropertyValueUnion(bacnetValueModel);
                                Buffer.BlockCopy(bacPropertyValueUnionByt, 0, bacnetPropertyValue.BacnetPropertyValueUnion, 0, bacPropertyValueUnionByt.Length);
                            }

                            IntPtr currentPropVal = bacnetValPtr;

                            if (valcnt < bacnetTimeValueModel.Values.Count - 1)
                            {
                                bacnetValPtr = Marshal.AllocHGlobal(bacnetPropValSize);
                                bacnetPropertyValue.nextPropVal = bacnetValPtr;

                                if (!structurePointers.ContainsKey(bacnetValPtr))
                                    structurePointers.Add(bacnetValPtr, typeof(BacnetPropertyValue));
                            }

                            Marshal.StructureToPtr(bacnetPropertyValue, currentPropVal, false);
                        }

                        IntPtr bacnetTimeValPtr = IntPtr.Zero;
                        int bacnetTimeValSize = Marshal.SizeOf(typeof(BacnetTimeValue));

                        if (bacnetSpecialEventModel.BacnetTimeValue.Count > 1)
                        {
                            bacnetTimeValPtr = Marshal.AllocHGlobal(bacnetTimeValSize);
                            bacnetTimeValue.next = bacnetTimeValPtr;

                            if (!structurePointers.ContainsKey(bacnetTimeValPtr))
                                structurePointers.Add(bacnetTimeValPtr, typeof(BacnetTimeValue));
                        }

                        listOfSpecialEvent.bacnetSpecialEvt.stListOfTimeValues = bacnetTimeValue;

                        for (int countTM = 1; countTM < bacnetSpecialEventModel.BacnetTimeValue.Count; countTM++)
                        {
                            bacnetTimeValue = new BacnetTimeValue();
                            bacnetTimeValueModel = new BacnetTimeValueModel();
                            bacnetTimeValueModel = bacnetSpecialEventModel.BacnetTimeValue[count];

                            bacnetTimeValue.stTime = new BacnetTime();
                            bacnetTimeValue.stTime.hour = bacnetTimeValueModel.Time.Hour;
                            bacnetTimeValue.stTime.min = bacnetTimeValueModel.Time.Min;
                            bacnetTimeValue.stTime.sec = bacnetTimeValueModel.Time.Sec;
                            bacnetTimeValue.stTime.hundredths = bacnetTimeValueModel.Time.Hundredths;

                            bacnetTimeValue.bacnetPropVal = new BacnetPropertyValue();
                            byte[] bacnetPropertyValueUnionBytes = new byte[264];

                            if (bacnetTimeValueModel.Values.Count > 0)
                            {
                                bacnetValueModel = bacnetTimeValueModel.Values[0];
                                bacnetPropertyValue = new BacnetPropertyValue();
                                bacnetPropertyValue.tagType = bacnetValueModel.TagType;

                                if (bacnetValueModel.TagType != 0)
                                    bacnetPropertyValueUnionBytes = SetBacnetPropertyValueUnion(bacnetValueModel);
                            }

                            bacnetPropertyValue.BacnetPropertyValueUnion = new byte[BacDelPropertyDef.BacnetPropertyValueUnionSize];
                            Buffer.BlockCopy(bacnetPropertyValueUnionBytes, 0, bacnetPropertyValue.BacnetPropertyValueUnion, 0, bacnetPropertyValueUnionBytes.Length);

                            bacnetTimeValue.bacnetPropVal = bacnetPropertyValue;
                            IntPtr nextpropValuePtr = IntPtr.Zero;

                            if (bacnetTimeValueModel.Values.Count > 1)
                            {
                                nextpropValuePtr = Marshal.AllocHGlobal(bacnetPropValueSize);
                                bacnetTimeValue.bacnetPropVal.nextPropVal = nextpropValuePtr;

                                if (!structurePointers.ContainsKey(nextpropValuePtr))
                                    structurePointers.Add(nextpropValuePtr, typeof(BacnetPropertyValue));
                            }

                            for (int cnt = 1; cnt < bacnetTimeValueModel.Values.Count; cnt++)
                            {
                                bacnetPropertyValue = new BacnetPropertyValue();

                                bacnetValueModel = new BacnetValueModel();
                                bacnetValueModel = bacnetTimeValueModel.Values[cnt];

                                bacnetPropertyValue.tagType = bacnetValueModel.TagType;
                                bacnetPropertyValue.BacnetPropertyValueUnion = new byte[BacDelPropertyDef.BacnetPropertyValueUnionSize];

                                if (bacnetValueModel.TagType != 0)
                                {
                                    byte[] bacnetPropertyValueUnionByte = SetBacnetPropertyValueUnion(bacnetValueModel);
                                    Buffer.BlockCopy(bacnetPropertyValueUnionByte, 0, bacnetPropertyValue.BacnetPropertyValueUnion, 0, bacnetPropertyValueUnionByte.Length);
                                }

                                IntPtr currentProppertyVal = nextpropValuePtr;

                                if (cnt < bacnetTimeValueModel.Values.Count - 1)
                                {
                                    nextpropValuePtr = Marshal.AllocHGlobal(bacnetPropValueSize);
                                    bacnetPropertyValue.nextPropVal = nextpropValuePtr;

                                    if (!structurePointers.ContainsKey(nextpropValuePtr))
                                        structurePointers.Add(nextpropValuePtr, typeof(BacnetPropertyValue));
                                }

                                Marshal.StructureToPtr(bacnetPropertyValue, currentProppertyVal, false);
                            }

                            IntPtr currentTimeValue = bacnetTimeValPtr;

                            if (count < bacnetSpecialEventModel.BacnetTimeValue.Count - 1)
                            {
                                bacnetTimeValPtr = Marshal.AllocHGlobal(bacnetTimeValueSize);
                                bacnetTimeValue.next = bacnetTimeValPtr;

                                if (!structurePointers.ContainsKey(bacnetTimeValPtr))
                                    structurePointers.Add(bacnetTimeValPtr, typeof(BacnetTimeValue));
                            }

                            Marshal.StructureToPtr(bacnetTimeValue, currentTimeValue, false);
                        }

                        IntPtr current = nextSpecialEventPtr;

                        if (count < bacnetSpecialEventModelList.Count - 1)
                        {
                            nextSpecialEventPtr = Marshal.AllocHGlobal(listOfSpecialEventSize);
                            listOfSpecialEvent.next = nextSpecialEventPtr;

                            if (!structurePointers.ContainsKey(nextSpecialEventPtr))
                                structurePointers.Add(nextSpecialEventPtr, typeof(ListOfSpecialEvent));
                        }

                        Marshal.StructureToPtr(listOfSpecialEvent, current, false);
                    }
                }
            }

            return prListOfBacnetSpecialEvent;
        }

        public static byte[] SetBacnetPropertyValueUnion(BacnetValueModel bacnetValueModel)
        {
            byte[] propertyValueArray = new byte[264];

            switch (bacnetValueModel.TagType)
            {
                case (byte)BacnetApplicationTag.BacnetApplicationTagReal:
                    float floatVal = Convert.ToSingle(bacnetValueModel.Value);
                    propertyValueArray = StructureConverter.StructureToByteArray<float>(floatVal);
                    break;
                case (byte)BacnetApplicationTag.BacnetApplicationTagUnsignedInt:
                    uint unsignedVal = Convert.ToUInt32(bacnetValueModel.Value);
                    propertyValueArray = StructureConverter.StructureToByteArray<uint>(unsignedVal);
                    break;
                case (byte)BacnetApplicationTag.BacnetApplicationTagEnumerated:
                    uint enumVal = Convert.ToUInt32(bacnetValueModel.Value);
                    propertyValueArray = StructureConverter.StructureToByteArray<uint>(enumVal);
                    break;
                case (byte)BacnetApplicationTag.BacnetApplicationTagBoolean:
                    byte boolVal = Convert.ToByte(Convert.ToBoolean(bacnetValueModel.Value));
                    propertyValueArray = StructureConverter.StructureToByteArray<byte>(boolVal);
                    break;
                case (byte)BacnetApplicationTag.BacnetApplicationTagSignedInt:
                    int signedVal = Convert.ToInt32(bacnetValueModel.Value);
                    propertyValueArray = StructureConverter.StructureToByteArray<int>(signedVal);
                    break;
                case (byte)BacnetApplicationTag.BacnetApplicationTagDouble:
                    double doubleVal = Convert.ToDouble(bacnetValueModel.Value);
                    propertyValueArray = StructureConverter.StructureToByteArray<double>(doubleVal);
                    break;
                case (byte)BacnetApplicationTag.BacnetApplicationTagOctetString:
                    string octetStringVal = Convert.ToString(bacnetValueModel.Value);
                    BacnetOctetStr prBacnetOctetStr = new BacnetOctetStr();
                    prBacnetOctetStr = SetBacnetOctetStr(octetStringVal);
                    propertyValueArray = StructureConverter.StructureToByteArray<BacnetOctetStr>(prBacnetOctetStr);
                    break;
                case (byte)BacnetApplicationTag.BacnetApplicationTagCharacterString:
                    string charStringVal = Convert.ToString(bacnetValueModel.Value);
                    byte[] propertyVal = new byte[255];

                    byte[] arrByte = Encoding.ASCII.GetBytes(charStringVal);
                    int arrayLength = 0;

                    if (arrByte.Length <= 255)
                        arrayLength = arrByte.Length;
                    else
                        arrayLength = 255;

                    BacnetCharStr charString = new BacnetCharStr();
                    charString.strLen = (uint)(arrayLength);

                    Buffer.BlockCopy(arrByte, 0, propertyVal, 0, arrayLength);
                    charString.charStr = propertyVal;
                    propertyValueArray = StructureConverter.StructureToByteArray<BacnetCharStr>(charString);
                    break;
                case (byte)BacnetApplicationTag.BacnetApplicationTagBitString:
                    BacnetBITStr bacnetBitStr = new BacnetBITStr();
                    string bitString = Convert.ToString(bacnetValueModel.Value);
                    bacnetBitStr = SetBitStringNew(bitString);

                    propertyValueArray = StructureConverter.StructureToByteArray<BacnetBITStr>(bacnetBitStr);
                    break;
                case (byte)BacnetApplicationTag.BacnetApplicationTagDate:
                    BacnetDateModel bacnetDateModel = bacnetValueModel.Value as BacnetDateModel;
                    BacnetDate prBacnetDate = new BacnetDate();

                    if (bacnetDateModel != null)
                    {
                        prBacnetDate.year = bacnetDateModel.Year;
                        prBacnetDate.month = bacnetDateModel.Month;
                        prBacnetDate.day = bacnetDateModel.Day;
                        prBacnetDate.weekDay = bacnetDateModel.WeekDay;
                    }

                    propertyValueArray = StructureConverter.StructureToByteArray<BacnetDate>(prBacnetDate);
                    break;
                case (byte)BacnetApplicationTag.BacnetApplicationTagTime:
                    BacnetTimeModel bacnetTimeModel = bacnetValueModel.Value as BacnetTimeModel;
                    BacnetTime prBacnetTime = new BacnetTime();

                    if (bacnetTimeModel != null)
                    {
                        prBacnetTime.hour = bacnetTimeModel.Hour;
                        prBacnetTime.min = bacnetTimeModel.Min;
                        prBacnetTime.sec = bacnetTimeModel.Sec;
                        prBacnetTime.hundredths = bacnetTimeModel.Hundredths;
                    }

                    propertyValueArray = StructureConverter.StructureToByteArray<BacnetTime>(prBacnetTime);
                    break;
                case (byte)BacnetApplicationTag.BacnetApplicationTagObjectID:
                    ObjectIdentifierModel objectIdentifierModel = bacnetValueModel.Value as ObjectIdentifierModel;
                    BacnetObjID prBacnetObjID = new BacnetObjID();

                    if (objectIdentifierModel != null)
                    {
                        prBacnetObjID.objType = objectIdentifierModel.ObjectType;
                        prBacnetObjID.objInstance = objectIdentifierModel.InstanceNumber;
                    }

                    propertyValueArray = StructureConverter.StructureToByteArray<BacnetObjID>(prBacnetObjID);
                    break;
                case (byte)BacnetApplicationTag.BacnetApplicationTagNull:
                    break;
            }

            return propertyValueArray;
        }

        private static PrListOfUnsigned GenerateStructureListOfUnsigned(object propertyValue, Dictionary<IntPtr, Type> structurePointers)
        {
            int listOfUnsignedSize = Marshal.SizeOf(typeof(ListOfUnsigned));
            PrListOfUnsigned prListOfUnsigned = new PrListOfUnsigned();

            if (propertyValue is ICollection)
            {
                List<uint> UnsignedList = propertyValue as List<uint>;
                prListOfUnsigned.count = (uint)UnsignedList.Count;

                if (UnsignedList.Count > 0)
                {
                    ListOfUnsigned listOfUnsigned = new ListOfUnsigned();
                    listOfUnsigned.value = UnsignedList[0];

                    IntPtr nextUnsignedPtr = IntPtr.Zero;

                    if (UnsignedList.Count > 1)
                    {
                        nextUnsignedPtr = Marshal.AllocHGlobal(listOfUnsignedSize);
                        listOfUnsigned.next = nextUnsignedPtr;

                        if (!structurePointers.ContainsKey(nextUnsignedPtr))
                            structurePointers.Add(nextUnsignedPtr, typeof(ListOfUnsigned));
                    }

                    IntPtr listOfUnsignedPtr = StructureConverter.StructureToPtr<ListOfUnsigned>(listOfUnsigned);

                    if (!structurePointers.ContainsKey(listOfUnsignedPtr))
                        structurePointers.Add(listOfUnsignedPtr, typeof(ListOfUnsigned));

                    prListOfUnsigned.unsignVal = listOfUnsignedPtr;

                    for (int count = 1; count < UnsignedList.Count; count++)
                    {
                        listOfUnsigned = new ListOfUnsigned();
                        listOfUnsigned.value = UnsignedList[count];

                        IntPtr current = nextUnsignedPtr;

                        if (count < UnsignedList.Count - 1)
                        {
                            nextUnsignedPtr = Marshal.AllocHGlobal(listOfUnsignedSize);
                            listOfUnsigned.next = nextUnsignedPtr;

                            if (!structurePointers.ContainsKey(nextUnsignedPtr))
                                structurePointers.Add(nextUnsignedPtr, typeof(ListOfUnsigned));
                        }

                        Marshal.StructureToPtr(listOfUnsigned, current, false);
                    }
                }
            }

            return prListOfUnsigned;
        }

        private static PrListOfBacnetDestination GenerateStructureBacnetDestination(object propertyValue, Dictionary<IntPtr, Type> structurePointers)
        {
            int listOfBacnetDestinationSize = Marshal.SizeOf(typeof(ListOfBacnetDestination));
            PrListOfBacnetDestination prListOfBacnetDestination = new PrListOfBacnetDestination();

            if (propertyValue is ICollection)
            {
                List<BacnetDestinationModel> BacnetDestinationList = propertyValue as List<BacnetDestinationModel>;
                prListOfBacnetDestination.count = (uint)BacnetDestinationList.Count;

                if (BacnetDestinationList.Count > 0)
                {
                    ListOfBacnetDestination listOfBacnetDestination = new ListOfBacnetDestination();
                    BacnetDestinationModel bacnetDestinationModel = new BacnetDestinationModel();
                    bacnetDestinationModel = BacnetDestinationList[0];

                    listOfBacnetDestination.issueConfirmedNotification = bacnetDestinationModel.IssueConfirmedNotification;
                    listOfBacnetDestination.stProcessID = bacnetDestinationModel.ProcessID;
                    listOfBacnetDestination.stFromTime.hour = bacnetDestinationModel.FromTime.Hour;
                    listOfBacnetDestination.stFromTime.min = bacnetDestinationModel.FromTime.Min;
                    listOfBacnetDestination.stFromTime.sec = bacnetDestinationModel.FromTime.Sec;
                    listOfBacnetDestination.stFromTime.hundredths = bacnetDestinationModel.FromTime.Hundredths;
                    listOfBacnetDestination.stToTime.hour = bacnetDestinationModel.ToTime.Hour;
                    listOfBacnetDestination.stToTime.min = bacnetDestinationModel.ToTime.Min;
                    listOfBacnetDestination.stToTime.sec = bacnetDestinationModel.ToTime.Sec;
                    listOfBacnetDestination.stToTime.hundredths = bacnetDestinationModel.ToTime.Hundredths;
                    listOfBacnetDestination.stDaysOfWeek = SetDaysOfWeekBits(bacnetDestinationModel.DaysOfWeek);
                    listOfBacnetDestination.stTransitions = SetEventTransitionBits(bacnetDestinationModel.EventTransitions);
                    listOfBacnetDestination.bacnetRecipient.destinationType = bacnetDestinationModel.BacnetRecipient.DestinationType;
                    listOfBacnetDestination.bacnetRecipient.bacnetRecipientUnion = new byte[BacDelPropertyDef.BacnetRecipientUnionSize];
                    byte[] recipientBytes = SetBacnetRecipientUnion(bacnetDestinationModel.BacnetRecipient);

                    Buffer.BlockCopy(recipientBytes, 0, listOfBacnetDestination.bacnetRecipient.bacnetRecipientUnion, 0, recipientBytes.Length);

                    IntPtr nextDestinationPtr = IntPtr.Zero;

                    if (BacnetDestinationList.Count > 1)
                    {
                        nextDestinationPtr = Marshal.AllocHGlobal(listOfBacnetDestinationSize);
                        listOfBacnetDestination.next = nextDestinationPtr;

                        if (!structurePointers.ContainsKey(nextDestinationPtr))
                            structurePointers.Add(nextDestinationPtr, typeof(ListOfBacnetDestination));
                    }

                    IntPtr bacnetDestinationPointer = StructureConverter.StructureToPtr<ListOfBacnetDestination>(listOfBacnetDestination);
                    prListOfBacnetDestination.ncRecepient = bacnetDestinationPointer;

                    if (!structurePointers.ContainsKey(bacnetDestinationPointer))
                        structurePointers.Add(bacnetDestinationPointer, typeof(ListOfBacnetDestination));

                    for (int count = 1; count < BacnetDestinationList.Count; count++)
                    {
                        listOfBacnetDestination = new ListOfBacnetDestination();
                        bacnetDestinationModel = new BacnetDestinationModel();
                        bacnetDestinationModel = BacnetDestinationList[count];

                        listOfBacnetDestination.issueConfirmedNotification = bacnetDestinationModel.IssueConfirmedNotification;
                        listOfBacnetDestination.stProcessID = bacnetDestinationModel.ProcessID;
                        listOfBacnetDestination.stFromTime.hour = bacnetDestinationModel.FromTime.Hour;
                        listOfBacnetDestination.stFromTime.min = bacnetDestinationModel.FromTime.Min;
                        listOfBacnetDestination.stFromTime.sec = bacnetDestinationModel.FromTime.Sec;
                        listOfBacnetDestination.stFromTime.hundredths = bacnetDestinationModel.FromTime.Hundredths;
                        listOfBacnetDestination.stToTime.hour = bacnetDestinationModel.ToTime.Hour;
                        listOfBacnetDestination.stToTime.min = bacnetDestinationModel.ToTime.Min;
                        listOfBacnetDestination.stToTime.sec = bacnetDestinationModel.ToTime.Sec;
                        listOfBacnetDestination.stToTime.hundredths = bacnetDestinationModel.ToTime.Hundredths;
                        listOfBacnetDestination.stDaysOfWeek = SetDaysOfWeekBits(bacnetDestinationModel.DaysOfWeek);
                        listOfBacnetDestination.stTransitions = SetEventTransitionBits(bacnetDestinationModel.EventTransitions);
                        listOfBacnetDestination.bacnetRecipient.destinationType = bacnetDestinationModel.BacnetRecipient.DestinationType;
                        listOfBacnetDestination.bacnetRecipient.bacnetRecipientUnion = new byte[BacDelPropertyDef.BacnetRecipientUnionSize];
                        byte[] recipientByte = SetBacnetRecipientUnion(bacnetDestinationModel.BacnetRecipient);

                        Buffer.BlockCopy(recipientByte, 0, listOfBacnetDestination.bacnetRecipient.bacnetRecipientUnion, 0, recipientByte.Length);
                        IntPtr current = nextDestinationPtr;

                        if (count < BacnetDestinationList.Count - 1)
                        {
                            nextDestinationPtr = Marshal.AllocHGlobal(listOfBacnetDestinationSize);
                            listOfBacnetDestination.next = nextDestinationPtr;

                            if (!structurePointers.ContainsKey(nextDestinationPtr))
                                structurePointers.Add(nextDestinationPtr, typeof(ListOfBacnetDestination));
                        }

                        Marshal.StructureToPtr(listOfBacnetDestination, current, false);
                    }
                }
            }

            return prListOfBacnetDestination;
        }

        private static byte[] SetBacnetRecipientUnion(BacnetRecipientModel bacnetRecipientModel)
        {
            byte[] recipientArray = new byte[16];

            if (bacnetRecipientModel.DestinationType == DestinationType.DestinationIsDeviceID)
            {
                BacnetRecipientUnionObjID bacnetRecipientUnionObjID = new BacnetRecipientUnionObjID();
                bacnetRecipientUnionObjID.objectID = new ObjID();
                bacnetRecipientUnionObjID.objectID.ObjectType = bacnetRecipientModel.ObjectType;
                bacnetRecipientUnionObjID.objectID.ObjId = bacnetRecipientModel.ObjId;

                recipientArray = StructureConverter.StructureToByteArray<BacnetRecipientUnionObjID>(bacnetRecipientUnionObjID);
            }
            else if (bacnetRecipientModel.DestinationType == DestinationType.DestinationIsIpAddr)
            {
                BacnetRecipientUnionAddress bacnetRecipientUnionAddress = new BacnetRecipientUnionAddress();
                bacnetRecipientUnionAddress.stAddress = new BacnetAddress();
                bacnetRecipientUnionAddress.stAddress = GetDestinationListBacnetAddress(bacnetRecipientModel.BacnetAddress);

                recipientArray = StructureConverter.StructureToByteArray<BacnetRecipientUnionAddress>(bacnetRecipientUnionAddress);
            }

            return recipientArray;
        }

        private static PrListOfBacnetDailySchedule GenerateStructureDailyScheduleArray(object propertyValue, Dictionary<IntPtr, Type> structurePointers)
        {
            PrListOfBacnetDailySchedule prListOfBacnetDailySchedule = new PrListOfBacnetDailySchedule();
            BacnetTimeValue[] bacnetTimeValueArray = new BacnetTimeValue[7];

            if (propertyValue is ICollection)
            {
                List<BacnetTimeValueArrayModel> bacnetTimeValueListArray = propertyValue as List<BacnetTimeValueArrayModel>;

                for (int daycount = 0; daycount < bacnetTimeValueListArray.Count; daycount++)
                {
                    BacnetTimeValue bacnetTimeValueSource = new BacnetTimeValue();
                    int bacnetTimeValueSize = Marshal.SizeOf(typeof(BacnetTimeValue));
                    int bacnetPropertyValueSize = Marshal.SizeOf(typeof(BacnetPropertyValue));
                    BacnetTimeValue bacnetTimeValue = new BacnetTimeValue();

                    BacnetTimeValueArrayModel bacnetTimeValueArrayModel = bacnetTimeValueListArray[daycount];
                    BacnetTimeValueModel bacnetTimeValueModel = new BacnetTimeValueModel();
                    BacnetPropertyValue bacnetPropertyValue = new BacnetPropertyValue();
                    BacnetValueModel bacnetValueModel = new BacnetValueModel();
                    byte[] bacnetPropValueUnionByte = new byte[264];

                    if (bacnetTimeValueArrayModel.BacnetTimeValueList.Count > 0)
                    {
                        bacnetTimeValueModel = bacnetTimeValueArrayModel.BacnetTimeValueList[0];
                        bacnetTimeValue.isUsed = true;
                        bacnetTimeValue.stTime = new BacnetTime();
                        bacnetTimeValue.stTime.hour = bacnetTimeValueModel.Time.Hour;
                        bacnetTimeValue.stTime.min = bacnetTimeValueModel.Time.Min;
                        bacnetTimeValue.stTime.sec = bacnetTimeValueModel.Time.Sec;
                        bacnetTimeValue.stTime.hundredths = bacnetTimeValueModel.Time.Hundredths;

                        bacnetTimeValue.bacnetPropVal = new BacnetPropertyValue();
                        if (bacnetTimeValueModel.Values.Count > 0)
                        {
                            bacnetValueModel = bacnetTimeValueModel.Values[0];
                            bacnetPropertyValue.tagType = bacnetValueModel.TagType;

                            if (bacnetValueModel.TagType != 0)
                                bacnetPropValueUnionByte = SetBacnetPropertyValueUnion(bacnetValueModel);
                        }
                    }

                    bacnetPropertyValue.BacnetPropertyValueUnion = new byte[BacDelPropertyDef.BacnetPropertyValueUnionSize];
                    Buffer.BlockCopy(bacnetPropValueUnionByte, 0, bacnetPropertyValue.BacnetPropertyValueUnion, 0, bacnetPropValueUnionByte.Length);

                    bacnetTimeValue.bacnetPropVal = bacnetPropertyValue;
                    IntPtr nextpropertyValuePtr = IntPtr.Zero;

                    if (bacnetTimeValueModel.Values.Count > 1)
                    {
                        nextpropertyValuePtr = Marshal.AllocHGlobal(bacnetPropertyValueSize);
                        bacnetTimeValue.bacnetPropVal.nextPropVal = nextpropertyValuePtr;

                        if (!structurePointers.ContainsKey(nextpropertyValuePtr))
                            structurePointers.Add(nextpropertyValuePtr, typeof(BacnetPropertyValue));
                    }

                    for (int cnt = 1; cnt < bacnetTimeValueModel.Values.Count; cnt++)
                    {
                        bacnetPropertyValue = new BacnetPropertyValue();

                        bacnetValueModel = new BacnetValueModel();
                        bacnetValueModel = bacnetTimeValueModel.Values[cnt];

                        bacnetPropertyValue.tagType = bacnetValueModel.TagType;
                        bacnetPropertyValue.BacnetPropertyValueUnion = new byte[BacDelPropertyDef.BacnetPropertyValueUnionSize];

                        if (bacnetValueModel.TagType != 0)
                        {
                            byte[] bacnetPropValueUnionBytes = SetBacnetPropertyValueUnion(bacnetValueModel);
                            Buffer.BlockCopy(bacnetPropValueUnionBytes, 0, bacnetPropertyValue.BacnetPropertyValueUnion, 0, bacnetPropValueUnionBytes.Length);
                        }

                        IntPtr currentPropVal = nextpropertyValuePtr;

                        if (cnt < bacnetTimeValueModel.Values.Count - 1)
                        {
                            nextpropertyValuePtr = Marshal.AllocHGlobal(bacnetPropertyValueSize);
                            bacnetPropertyValue.nextPropVal = nextpropertyValuePtr;

                            if (!structurePointers.ContainsKey(nextpropertyValuePtr))
                                structurePointers.Add(nextpropertyValuePtr, typeof(BacnetPropertyValue));
                        }

                        Marshal.StructureToPtr(bacnetPropertyValue, currentPropVal, false);
                    }

                    bacnetTimeValueSource = bacnetTimeValue;

                    IntPtr nextTimeValptr = IntPtr.Zero;

                    if (bacnetTimeValueArrayModel.BacnetTimeValueList.Count > 1)
                    {
                        nextTimeValptr = Marshal.AllocHGlobal(bacnetTimeValueSize);
                        bacnetTimeValueSource.next = nextTimeValptr;

                        if (!structurePointers.ContainsKey(nextTimeValptr))
                            structurePointers.Add(nextTimeValptr, typeof(BacnetTimeValue));
                    }

                    for (int count = 1; count < bacnetTimeValueArrayModel.BacnetTimeValueList.Count; count++)
                    {
                        bacnetTimeValue = new BacnetTimeValue();
                        bacnetTimeValueModel = new BacnetTimeValueModel();
                        bacnetTimeValueModel = bacnetTimeValueArrayModel.BacnetTimeValueList[count];

                        bacnetTimeValue.stTime = new BacnetTime();
                        bacnetTimeValue.stTime.hour = bacnetTimeValueModel.Time.Hour;
                        bacnetTimeValue.stTime.min = bacnetTimeValueModel.Time.Min;
                        bacnetTimeValue.stTime.sec = bacnetTimeValueModel.Time.Sec;
                        bacnetTimeValue.stTime.hundredths = bacnetTimeValueModel.Time.Hundredths;

                        bacnetTimeValue.bacnetPropVal = new BacnetPropertyValue();
                        byte[] bacnetPropValueUnionBytes = new byte[264];

                        if (bacnetTimeValueModel.Values.Count > 0)
                        {
                            bacnetValueModel = bacnetTimeValueModel.Values[0];
                            bacnetPropertyValue = new BacnetPropertyValue();
                            bacnetPropertyValue.tagType = bacnetValueModel.TagType;

                            if (bacnetValueModel.TagType != 0)
                                bacnetPropValueUnionBytes = SetBacnetPropertyValueUnion(bacnetValueModel);
                        }

                        bacnetPropertyValue.BacnetPropertyValueUnion = new byte[BacDelPropertyDef.BacnetPropertyValueUnionSize];
                        Buffer.BlockCopy(bacnetPropValueUnionBytes, 0, bacnetPropertyValue.BacnetPropertyValueUnion, 0, bacnetPropValueUnionBytes.Length);

                        bacnetTimeValue.bacnetPropVal = bacnetPropertyValue;
                        IntPtr nextpropValuePtr = IntPtr.Zero;

                        if (bacnetTimeValueModel.Values.Count > 1)
                        {
                            nextpropValuePtr = Marshal.AllocHGlobal(bacnetPropertyValueSize);
                            bacnetTimeValue.bacnetPropVal.nextPropVal = nextpropValuePtr;

                            if (!structurePointers.ContainsKey(nextpropValuePtr))
                                structurePointers.Add(nextpropValuePtr, typeof(BacnetPropertyValue));
                        }

                        for (int cnt = 1; cnt < bacnetTimeValueModel.Values.Count; cnt++)
                        {
                            bacnetPropertyValue = new BacnetPropertyValue();

                            bacnetValueModel = new BacnetValueModel();
                            bacnetValueModel = bacnetTimeValueModel.Values[cnt];

                            bacnetPropertyValue.tagType = bacnetValueModel.TagType;
                            bacnetPropertyValue.BacnetPropertyValueUnion = new byte[BacDelPropertyDef.BacnetPropertyValueUnionSize];

                            if (bacnetValueModel.TagType != 0)
                            {
                                byte[] bacnetPropValueUnionByt = SetBacnetPropertyValueUnion(bacnetValueModel);
                                Buffer.BlockCopy(bacnetPropValueUnionByt, 0, bacnetPropertyValue.BacnetPropertyValueUnion, 0, bacnetPropValueUnionByt.Length);
                            }

                            IntPtr currentProppertyVal = nextpropValuePtr;

                            if (cnt < bacnetTimeValueModel.Values.Count - 1)
                            {
                                nextpropValuePtr = Marshal.AllocHGlobal(bacnetPropertyValueSize);
                                bacnetPropertyValue.nextPropVal = nextpropValuePtr;

                                if (!structurePointers.ContainsKey(nextpropValuePtr))
                                    structurePointers.Add(nextpropValuePtr, typeof(BacnetPropertyValue));
                            }

                            Marshal.StructureToPtr(bacnetPropertyValue, currentProppertyVal, false);
                        }

                        IntPtr currentTimeValue = nextTimeValptr;

                        if (count < bacnetTimeValueArrayModel.BacnetTimeValueList.Count - 1)
                        {
                            nextTimeValptr = Marshal.AllocHGlobal(bacnetTimeValueSize);
                            bacnetTimeValue.next = nextTimeValptr;

                            if (!structurePointers.ContainsKey(nextTimeValptr))
                                structurePointers.Add(nextTimeValptr, typeof(BacnetTimeValue));
                        }

                        Marshal.StructureToPtr(bacnetTimeValue, currentTimeValue, false);
                    }

                    bacnetTimeValueArray[daycount] = bacnetTimeValueSource;
                }
            }

            prListOfBacnetDailySchedule.timeValue = bacnetTimeValueArray;
            return prListOfBacnetDailySchedule;
        }

        private static PrBacnetDailySchedule GenerateStructureDailySchedule(object propertyValue, Dictionary<IntPtr, Type> structurePointers)
        {
            PrBacnetDailySchedule prBacnetDailySchedule = new PrBacnetDailySchedule();

            if (propertyValue is ICollection)
            {
                List<BacnetTimeValueModel> bacnetTimeValueModelList = propertyValue as List<BacnetTimeValueModel>;

                if (bacnetTimeValueModelList.Count > 0)
                {
                    int bacnetTimeValueSize = Marshal.SizeOf(typeof(BacnetTimeValue));
                    int bacnetPropertyValueSize = Marshal.SizeOf(typeof(BacnetPropertyValue));
                    BacnetTimeValue bacnetTimeValue = new BacnetTimeValue();

                    BacnetTimeValueModel BacnetTimeValueModel = bacnetTimeValueModelList[0];

                    if (BacnetTimeValueModel != null)
                    {
                        bacnetTimeValue.isUsed = true;
                    }

                    bacnetTimeValue.stTime = new BacnetTime();
                    bacnetTimeValue.stTime.hour = BacnetTimeValueModel.Time.Hour;
                    bacnetTimeValue.stTime.min = BacnetTimeValueModel.Time.Min;
                    bacnetTimeValue.stTime.sec = BacnetTimeValueModel.Time.Sec;
                    bacnetTimeValue.stTime.hundredths = BacnetTimeValueModel.Time.Hundredths;

                    bacnetTimeValue.bacnetPropVal = new BacnetPropertyValue();
                    BacnetPropertyValue bacnetPropertyValue = new BacnetPropertyValue();
                    BacnetValueModel bacnetValueModel = new BacnetValueModel();
                    byte[] propValueUnoinBytes = new byte[264];

                    if (BacnetTimeValueModel.Values.Count > 0)
                    {
                        bacnetValueModel = BacnetTimeValueModel.Values[0];

                        bacnetPropertyValue.tagType = bacnetValueModel.TagType;

                        if (bacnetValueModel.TagType != 0)
                            propValueUnoinBytes = SetBacnetPropertyValueUnion(bacnetValueModel);
                    }

                    bacnetPropertyValue.BacnetPropertyValueUnion = new byte[BacDelPropertyDef.BacnetPropertyValueUnionSize];
                    Buffer.BlockCopy(propValueUnoinBytes, 0, bacnetPropertyValue.BacnetPropertyValueUnion, 0, propValueUnoinBytes.Length);

                    bacnetTimeValue.bacnetPropVal = bacnetPropertyValue;
                    IntPtr nextpropertyValuePtr = IntPtr.Zero;

                    if (BacnetTimeValueModel.Values.Count > 1)
                    {
                        nextpropertyValuePtr = Marshal.AllocHGlobal(bacnetPropertyValueSize);
                        bacnetTimeValue.bacnetPropVal.nextPropVal = nextpropertyValuePtr;

                        if (!structurePointers.ContainsKey(nextpropertyValuePtr))
                            structurePointers.Add(nextpropertyValuePtr, typeof(BacnetPropertyValue));
                    }

                    for (int cnt = 1; cnt < BacnetTimeValueModel.Values.Count; cnt++)
                    {
                        bacnetPropertyValue = new BacnetPropertyValue();

                        bacnetValueModel = new BacnetValueModel();
                        bacnetValueModel = BacnetTimeValueModel.Values[cnt];

                        bacnetPropertyValue.tagType = bacnetValueModel.TagType;
                        bacnetPropertyValue.BacnetPropertyValueUnion = new byte[BacDelPropertyDef.BacnetPropertyValueUnionSize];

                        if (bacnetValueModel.TagType != 0)
                        {
                            byte[] propValueUnoinByte = SetBacnetPropertyValueUnion(bacnetValueModel);
                            Buffer.BlockCopy(propValueUnoinByte, 0, bacnetPropertyValue.BacnetPropertyValueUnion, 0, propValueUnoinByte.Length);
                        }

                        IntPtr currentPropVal = nextpropertyValuePtr;

                        if (cnt < BacnetTimeValueModel.Values.Count - 1)
                        {
                            nextpropertyValuePtr = Marshal.AllocHGlobal(bacnetPropertyValueSize);
                            bacnetPropertyValue.nextPropVal = nextpropertyValuePtr;

                            if (!structurePointers.ContainsKey(nextpropertyValuePtr))
                                structurePointers.Add(nextpropertyValuePtr, typeof(BacnetPropertyValue));
                        }

                        Marshal.StructureToPtr(bacnetPropertyValue, currentPropVal, false);
                    }

                    prBacnetDailySchedule.bacnetTimeVal = bacnetTimeValue;

                    IntPtr nextTimeValptr = IntPtr.Zero;

                    if (bacnetTimeValueModelList.Count > 1)
                    {
                        nextTimeValptr = Marshal.AllocHGlobal(bacnetTimeValueSize);
                        prBacnetDailySchedule.bacnetTimeVal.next = nextTimeValptr;

                        if (!structurePointers.ContainsKey(nextTimeValptr))
                            structurePointers.Add(nextTimeValptr, typeof(BacnetTimeValue));
                    }

                    for (int count = 1; count < bacnetTimeValueModelList.Count; count++)
                    {
                        bacnetTimeValue = new BacnetTimeValue();
                        BacnetTimeValueModel = new BacnetTimeValueModel();
                        BacnetTimeValueModel = bacnetTimeValueModelList[count];

                        if (BacnetTimeValueModel != null)
                        {
                            bacnetTimeValue.isUsed = true;
                        }

                        bacnetTimeValue.stTime = new BacnetTime();
                        bacnetTimeValue.stTime.hour = BacnetTimeValueModel.Time.Hour;
                        bacnetTimeValue.stTime.min = BacnetTimeValueModel.Time.Min;
                        bacnetTimeValue.stTime.sec = BacnetTimeValueModel.Time.Sec;
                        bacnetTimeValue.stTime.hundredths = BacnetTimeValueModel.Time.Hundredths;

                        bacnetTimeValue.bacnetPropVal = new BacnetPropertyValue();
                        byte[] propValueUnoinByte = new byte[264];

                        if (BacnetTimeValueModel.Values.Count > 0)
                        {
                            bacnetValueModel = BacnetTimeValueModel.Values[0];
                            bacnetPropertyValue = new BacnetPropertyValue();
                            bacnetPropertyValue.tagType = bacnetValueModel.TagType;

                            if (bacnetValueModel.TagType != 0)
                                propValueUnoinByte = SetBacnetPropertyValueUnion(bacnetValueModel);
                        }

                        bacnetPropertyValue.BacnetPropertyValueUnion = new byte[BacDelPropertyDef.BacnetPropertyValueUnionSize];
                        Buffer.BlockCopy(propValueUnoinByte, 0, bacnetPropertyValue.BacnetPropertyValueUnion, 0, propValueUnoinByte.Length);

                        bacnetTimeValue.bacnetPropVal = bacnetPropertyValue;
                        IntPtr nextpropValuePtr = IntPtr.Zero;

                        if (BacnetTimeValueModel.Values.Count > 1)
                        {
                            nextpropValuePtr = Marshal.AllocHGlobal(bacnetPropertyValueSize);
                            bacnetTimeValue.bacnetPropVal.nextPropVal = nextpropValuePtr;

                            if (!structurePointers.ContainsKey(nextpropValuePtr))
                                structurePointers.Add(nextpropValuePtr, typeof(BacnetPropertyValue));
                        }

                        for (int cnt = 1; cnt < BacnetTimeValueModel.Values.Count; cnt++)
                        {
                            bacnetPropertyValue = new BacnetPropertyValue();

                            bacnetValueModel = new BacnetValueModel();
                            bacnetValueModel = BacnetTimeValueModel.Values[cnt];

                            bacnetPropertyValue.tagType = bacnetValueModel.TagType;
                            bacnetPropertyValue.BacnetPropertyValueUnion = new byte[BacDelPropertyDef.BacnetPropertyValueUnionSize];

                            if (bacnetValueModel.TagType != 0)
                            {
                                byte[] propValUnoinByte = SetBacnetPropertyValueUnion(bacnetValueModel);
                                Buffer.BlockCopy(propValUnoinByte, 0, bacnetPropertyValue.BacnetPropertyValueUnion, 0, propValUnoinByte.Length);
                            }

                            IntPtr currentProppertyVal = nextpropValuePtr;

                            if (cnt < BacnetTimeValueModel.Values.Count - 1)
                            {
                                nextpropValuePtr = Marshal.AllocHGlobal(bacnetPropertyValueSize);
                                bacnetPropertyValue.nextPropVal = nextpropValuePtr;

                                if (!structurePointers.ContainsKey(nextpropValuePtr))
                                    structurePointers.Add(nextpropValuePtr, typeof(BacnetPropertyValue));
                            }

                            Marshal.StructureToPtr(bacnetPropertyValue, currentProppertyVal, false);
                        }

                        IntPtr currentTimeValue = nextTimeValptr;

                        if (count < bacnetTimeValueModelList.Count - 1)
                        {
                            nextTimeValptr = Marshal.AllocHGlobal(bacnetTimeValueSize);
                            bacnetTimeValue.next = nextTimeValptr;

                            if (!structurePointers.ContainsKey(nextTimeValptr))
                                structurePointers.Add(nextTimeValptr, typeof(BacnetTimeValue));
                        }

                        Marshal.StructureToPtr(bacnetTimeValue, currentTimeValue, false);
                    }
                }
            }

            return prBacnetDailySchedule;
        }

        private static BacnetAddress GetDestinationListBacnetAddress(BacnetAddressModel bacnetAddressModel)
        {
            BacnetAddress destinationAddress = new BacnetAddress();

            try
            {
                byte[] IpArray = new byte[6];
                destinationAddress.net = bacnetAddressModel.NetworkNumber;

                if (bacnetAddressModel.IPAddress != null)
                {
                    IPAddress ipAddress = IPAddress.Parse(bacnetAddressModel.IPAddress);

                    Buffer.BlockCopy(ipAddress.GetAddressBytes(), 0, IpArray, 0, 4);
                    byte[] port = BitConverter.GetBytes(bacnetAddressModel.Port);
                    Array.Reverse(port);
                    Buffer.BlockCopy(port, 2, IpArray, 4, 2);

                    destinationAddress.macLen = bacnetAddressModel.IPAddressLength;
                    destinationAddress.ipAddrs = IpArray;
                }

                if (bacnetAddressModel.MacAddress != null)
                {
                    PhysicalAddress mac = PhysicalAddress.Parse(bacnetAddressModel.MacAddress);
                    byte[] macArray = new byte[6];

                    Buffer.BlockCopy(mac.GetAddressBytes(), 0, macArray, 0, mac.GetAddressBytes().Length);
                    destinationAddress.macLen = bacnetAddressModel.MacLength;

                    destinationAddress.ipAddrs = macArray;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, moreInfo: "IL Error: Error occurd in GetDestinationListBacnetAddress method.");
            }

            return destinationAddress;
        }

        private static BacnetBitStr SetBitString(string bitString)
        {
            BacnetBitStr bacnetBitStr = new BacnetBitStr();

            byte statusFlagByte = 0x00;

            string[] bitStringElement = bitString.Split(',');

            if (bitStringElement.Length > 0)
            {
                if (bitStringElement.Length <= 8)
                {
                    for (int index = 0; index < bitStringElement.Length; index++)
                    {
                        if (bitStringElement[index] == 1.ToString() || bitStringElement[index] == "true")
                            statusFlagByte = statusFlagByte.SetBit(7 - index);
                    }
                }
                else
                {
                    for (int index = 0; index < 8; index++)
                    {
                        if (bitStringElement[index] == 1.ToString() || bitStringElement[index] == "true")
                            statusFlagByte = statusFlagByte.SetBit(7 - index);
                    }
                }
            }

            byte[] bitStringBytes = new byte[1];
            bitStringBytes[0] = statusFlagByte;

            bacnetBitStr.byteCnt = 1;
            if (bitStringElement.Length <= 8)
            {
                bacnetBitStr.unusedBits = (byte)(8 - bitStringElement.Length);
            }
            else
            {
                bacnetBitStr.unusedBits = 0;
            }
            bacnetBitStr.transBits = bitStringBytes;

            return bacnetBitStr;
        }

        private static BacnetBITStr SetBitStringNew(string bitString)
        {
            BacnetBITStr bacnetBITStr = new BacnetBITStr();

            string[] bitStringElement = bitString.Split(',');

            if (bitStringElement.Length > 0)
            {
                byte statusFlagByte = 0x00;
                int startIndex = 0, endIndex = 0;
                int totalByte = 0, extraByte = 0, unusedBits = 0;
                byte[] bitStringBytes;

                if (bitStringElement.Length <= 80)
                {
                    totalByte = bitStringElement.Length / 8;
                    extraByte = bitStringElement.Length % 8;

                    if (extraByte != 0)
                    {
                        totalByte = totalByte + 1;
                        unusedBits = 8 - extraByte;
                    }

                    if (totalByte > 1)
                    {
                        endIndex = 8;
                    }
                    else
                    {
                        if (extraByte > 0)
                            endIndex = extraByte;
                        else
                            endIndex = 8;
                    }

                    bitStringBytes = new byte[totalByte];

                    for (int index = 0; index < totalByte; index++)
                    {
                        statusFlagByte = 0x00;
                        int position = 0;

                        for (int bitPosition = startIndex; bitPosition < endIndex; bitPosition++)
                        {
                            if (bitStringElement[bitPosition] == 1.ToString() || bitStringElement[bitPosition] == "true")
                                statusFlagByte = statusFlagByte.SetBit(7 - position);
                            position++;
                        }

                        bitStringBytes[index] = statusFlagByte;
                        startIndex = startIndex + 8;

                        if (index == totalByte - 2)
                        {
                            if (unusedBits > 0)
                            {
                                endIndex = endIndex + extraByte;
                            }
                            else
                            {
                                endIndex = endIndex + 8;
                            }
                        }
                        else
                        {
                            endIndex = endIndex + 8;
                        }
                    }
                }
                else
                {
                    bitStringBytes = new byte[10];
                    totalByte = 10;
                    unusedBits = 0;
                    endIndex = 8;

                    for (int index = 0; index < 10; index++)
                    {
                        statusFlagByte = 0x00;
                        int position = 0;

                        for (int bitPosition = startIndex; bitPosition < endIndex; bitPosition++)
                        {
                            if (bitStringElement[bitPosition] == 1.ToString() || bitStringElement[bitPosition] == "true")
                                statusFlagByte = statusFlagByte.SetBit(7 - position);
                            position++;
                        }

                        bitStringBytes[index] = statusFlagByte;
                        startIndex = startIndex + 8;
                        endIndex = endIndex + 8;
                    }
                }

                bacnetBITStr.byteCnt = (byte)totalByte;
                bacnetBITStr.unusedBits = (byte)unusedBits;

                bacnetBITStr.transBits = new byte[BacDelStackConfig.MaxBitstringBytes];
                Buffer.BlockCopy(bitStringBytes, 0, bacnetBITStr.transBits, 0, bitStringBytes.Length);
            }

            return bacnetBITStr;
        }

        private static BacnetOctetStr SetBacnetOctetStr(string bitString)
        {
            BacnetOctetStr bacnetOctetStr = new BacnetOctetStr();
            byte[] byteArray = CommanHelper.StringToByteArray(bitString);
            uint octetcount = 0;

            if (byteArray.Length <= 50)
            {
                octetcount = (uint)byteArray.Length;
            }
            else
            {
                octetcount = 50;
            }

            bacnetOctetStr.OctetCount = octetcount;
            bacnetOctetStr.octetStr = new byte[BacDelStackConfig.MaxOctetStringBytes];
            Buffer.BlockCopy(byteArray, 0, bacnetOctetStr.octetStr, 0, (int)octetcount);

            return bacnetOctetStr;
        }

        private static PrBacnetEventParameter GenerateStructureBacnetEventParameter(EventParametersBase eventParametersBase, Dictionary<IntPtr, Type> structurePointers)
        {
            PrBacnetEventParameter prBacnetEventParameter = new PrBacnetEventParameter();
            BacnetEventParameter bacnetEventParameter = new BacnetEventParameter();
            bacnetEventParameter.eventType = eventParametersBase.EventType;

            switch (eventParametersBase.EventType)
            {
                case BacnetEventType.EventChangeOfState:
                    ChangeOfStateEventParametersModel changeOfStateEventParametersModel = eventParametersBase as ChangeOfStateEventParametersModel;

                    if (changeOfStateEventParametersModel != null)
                    {
                        ChangeOfState changeOfState = new ChangeOfState();
                        changeOfState.timedelay = changeOfStateEventParametersModel.TimeDelay;

                        IntPtr listOfBacnetPropertyStatesPtr = IntPtr.Zero;
                        int listOfBacnetPropertyStatesSize = Marshal.SizeOf(typeof(ListOfBacnetPropertyStates));
                        ListOfBacnetPropertyStates listOfBacnetPropertyStates = new ListOfBacnetPropertyStates();
                        PropertyStatesModel propertyStatesModel = new PropertyStatesModel();

                        if (changeOfStateEventParametersModel.PropertyStates.Count > 0)
                        {
                            propertyStatesModel = changeOfStateEventParametersModel.PropertyStates[0];
                            BacnetPropertyState bacnetPropertyState = new BacnetPropertyState();
                            bacnetPropertyState.appTag = (uint)propertyStatesModel.ApplicationTag;
                            bacnetPropertyState.propState = (uint)propertyStatesModel.PropertyState;

                            if (propertyStatesModel.PropertyState == BacnetPropertyStates.PropStateBooleanValue)
                            {
                                bacnetPropertyState.bacnetPropState.boolVal = Convert.ToBoolean(propertyStatesModel.Value);
                            }
                            else
                            {
                                bacnetPropertyState.bacnetPropState.eventType = (BacnetEventType)Convert.ToUInt32(propertyStatesModel.Value);
                            }

                            listOfBacnetPropertyStates.bacnetPropStates = bacnetPropertyState;

                            if (changeOfStateEventParametersModel.PropertyStates.Count > 1)
                            {
                                listOfBacnetPropertyStatesPtr = Marshal.AllocHGlobal(listOfBacnetPropertyStatesSize);
                                listOfBacnetPropertyStates.next = listOfBacnetPropertyStatesPtr;

                                if (!structurePointers.ContainsKey(listOfBacnetPropertyStatesPtr))
                                    structurePointers.Add(listOfBacnetPropertyStatesPtr, typeof(ListOfBacnetPropertyStates));
                            }

                            IntPtr listOfBacnetPropStatesPtr = StructureConverter.StructureToPtr<ListOfBacnetPropertyStates>(listOfBacnetPropertyStates);
                            changeOfState.listOfValues = listOfBacnetPropStatesPtr;

                            if (!structurePointers.ContainsKey(listOfBacnetPropStatesPtr))
                                structurePointers.Add(listOfBacnetPropStatesPtr, typeof(ListOfBacnetPropertyStates));


                            for (int cnt = 1; cnt < changeOfStateEventParametersModel.PropertyStates.Count; cnt++)
                            {
                                listOfBacnetPropertyStates = new ListOfBacnetPropertyStates();
                                propertyStatesModel = new PropertyStatesModel();
                                propertyStatesModel = changeOfStateEventParametersModel.PropertyStates[cnt];
                                bacnetPropertyState = new BacnetPropertyState();

                                bacnetPropertyState.appTag = (uint)propertyStatesModel.ApplicationTag;
                                bacnetPropertyState.propState = (uint)propertyStatesModel.PropertyState;

                                if (propertyStatesModel.PropertyState == BacnetPropertyStates.PropStateBooleanValue)
                                {
                                    bacnetPropertyState.bacnetPropState.boolVal = Convert.ToBoolean(propertyStatesModel.Value);
                                }
                                else
                                {
                                    bacnetPropertyState.bacnetPropState.eventType = (BacnetEventType)Convert.ToUInt32(propertyStatesModel.Value);
                                }

                                listOfBacnetPropertyStates.bacnetPropStates = bacnetPropertyState;

                                IntPtr currentPropVal = listOfBacnetPropertyStatesPtr;

                                if (cnt < changeOfStateEventParametersModel.PropertyStates.Count - 1)
                                {
                                    listOfBacnetPropertyStatesPtr = Marshal.AllocHGlobal(listOfBacnetPropertyStatesSize);
                                    listOfBacnetPropertyStates.next = listOfBacnetPropertyStatesPtr;

                                    if (!structurePointers.ContainsKey(listOfBacnetPropertyStatesPtr))
                                        structurePointers.Add(listOfBacnetPropertyStatesPtr, typeof(ListOfBacnetDevObjPropRef));
                                }

                                Marshal.StructureToPtr(listOfBacnetPropertyStates, currentPropVal, false);
                            }
                        }

                        bacnetEventParameter.bacnetEventParameterUnion = new byte[BacDelPropertyDef.BacnetEventParameterUnionSize];
                        byte[] changeOfStateByte = StructureConverter.StructureToByteArray<ChangeOfState>(changeOfState);

                        Buffer.BlockCopy(changeOfStateByte, 0, bacnetEventParameter.bacnetEventParameterUnion, 0, changeOfStateByte.Length);
                    }
                    break;
                case BacnetEventType.EventCommandFailure:
                    CommandFailureEventParametersModel commandFailureEventParametersModel = eventParametersBase as CommandFailureEventParametersModel;

                    if (commandFailureEventParametersModel != null)
                    {
                        CommandFailure commandFailure = new CommandFailure();
                        commandFailure.apptag = (uint)commandFailureEventParametersModel.ApplicationTag;
                        commandFailure.timedelay = commandFailureEventParametersModel.TimeDelay;
                        commandFailure.stFeedbackPropertyReference = new BacnetDevObjPropRef();
                        commandFailure.stFeedbackPropertyReference.deviceInstace = commandFailureEventParametersModel.BacnetObjectPropertyReference.DeviceInstance;
                        commandFailure.stFeedbackPropertyReference.objID = commandFailureEventParametersModel.BacnetObjectPropertyReference.ObjId;
                        commandFailure.stFeedbackPropertyReference.objectType = commandFailureEventParametersModel.BacnetObjectPropertyReference.ObjectType;
                        commandFailure.stFeedbackPropertyReference.deviceType = commandFailureEventParametersModel.BacnetObjectPropertyReference.DeviceType;
                        commandFailure.stFeedbackPropertyReference.deviceIDPresent = commandFailureEventParametersModel.BacnetObjectPropertyReference.DeviceIdPresent;
                        commandFailure.stFeedbackPropertyReference.propertyIdentifier = commandFailureEventParametersModel.BacnetObjectPropertyReference.PropertyIdentifier;
                        commandFailure.stFeedbackPropertyReference.arrayIndex = commandFailureEventParametersModel.BacnetObjectPropertyReference.ArrayIndex;
                        commandFailure.stFeedbackPropertyReference.arrIndxPresent = commandFailureEventParametersModel.BacnetObjectPropertyReference.ArrIndxPresent;

                        commandFailure.propertyValueUnion = new byte[BacDelPropertyDef.PropertyValueUnionSize];
                        byte[] propertyValueByte = GetByteArrayFromPropertyValueUsingTag(commandFailureEventParametersModel.ApplicationTag, commandFailureEventParametersModel.Value);
                        Buffer.BlockCopy(propertyValueByte, 0, commandFailure.propertyValueUnion, 0, propertyValueByte.Length);

                        bacnetEventParameter.bacnetEventParameterUnion = new byte[BacDelPropertyDef.BacnetEventParameterUnionSize];
                        byte[] commandFailureByte = StructureConverter.StructureToByteArray<CommandFailure>(commandFailure);

                        Buffer.BlockCopy(commandFailureByte, 0, bacnetEventParameter.bacnetEventParameterUnion, 0, commandFailureByte.Length);
                    }
                    break;
                case BacnetEventType.EventOutOfRange:
                    OutOfRangeEventParametersModel outOfRangeEventParametersModel = eventParametersBase as OutOfRangeEventParametersModel;

                    if (outOfRangeEventParametersModel != null)
                    {
                        OutOfRange outOfRange = new OutOfRange();
                        outOfRange.timedelay = outOfRangeEventParametersModel.TimeDelay;
                        outOfRange.highLimit = outOfRangeEventParametersModel.HighLimit;
                        outOfRange.lowLimit = outOfRangeEventParametersModel.LowLimit;
                        outOfRange.deadband = outOfRangeEventParametersModel.DeadBand;

                        bacnetEventParameter.bacnetEventParameterUnion = new byte[BacDelPropertyDef.BacnetEventParameterUnionSize];
                        byte[] outOfRangeByte = StructureConverter.StructureToByteArray<OutOfRange>(outOfRange);

                        Buffer.BlockCopy(outOfRangeByte, 0, bacnetEventParameter.bacnetEventParameterUnion, 0, outOfRangeByte.Length);
                    }
                    break;
                case BacnetEventType.EventChangeOfReliability:
                    break;
                case BacnetEventType.EventChangeOfBitString:
                    break;
                case BacnetEventType.EventChangeOfValue:
                    break;
                case BacnetEventType.EventFloatingLimit:
                    break;
                case BacnetEventType.EventComplex:
                    break;
                case BacnetEventType.EventUnassigned:
                    break;
                case BacnetEventType.EventChangeOfLifeSafety:
                    break;
                case BacnetEventType.EventExtended:
                    break;
                case BacnetEventType.EventBufferReady:
                    break;
                case BacnetEventType.EventUnsignedRange:
                    break;
                case BacnetEventType.EventReserved:
                    break;
                case BacnetEventType.EventAccessEvent:
                    break;
                case BacnetEventType.EventDoubleOutOfRange:
                    break;
                case BacnetEventType.EventSignedOutOfRange:
                    break;
                case BacnetEventType.EventUnsignedOutOfRange:
                    break;
                case BacnetEventType.EventChangeOfCharacterString:
                    break;
                case BacnetEventType.EventChangeOfStatusFlags:
                    break;
                case BacnetEventType.EventNone:
                    break;
                case BacnetEventType.MaxEventType:
                    break;
                default:
                    break;
            }

            prBacnetEventParameter.bacnetEventParam = bacnetEventParameter;
            return prBacnetEventParameter;
        }

        private static byte[] GetByteArrayFromPropertyValueUsingTag(BacnetApplicationTag tag, object value)
        {
            byte[] propValueArray = new byte[BacDelPropertyDef.PropertyValueUnionSize];

            switch (tag)
            {
                case BacnetApplicationTag.BacnetApplicationTagReal:
                    float floatVal = Convert.ToSingle(value);
                    propValueArray = StructureConverter.StructureToByteArray<float>(floatVal);
                    break;
                case BacnetApplicationTag.BacnetApplicationTagUnsignedInt:
                    uint unsignedVal = Convert.ToUInt32(value);
                    propValueArray = StructureConverter.StructureToByteArray<uint>(unsignedVal);
                    break;
                case BacnetApplicationTag.BacnetApplicationTagEnumerated:
                    uint enumVal = Convert.ToUInt32(value);
                    propValueArray = StructureConverter.StructureToByteArray<uint>(enumVal);
                    break;
                case (byte)BacnetApplicationTag.BacnetApplicationTagNull:
                    break;
                case BacnetApplicationTag.BacnetApplicationTagBoolean:
                    break;
                case BacnetApplicationTag.BacnetApplicationTagDouble:
                    break;
                case BacnetApplicationTag.BacnetApplicationTagOctetString:
                    break;
                case BacnetApplicationTag.BacnetApplicationTagCharacterString:
                    break;
                case BacnetApplicationTag.BacnetApplicationTagBitString:
                    break;
                case BacnetApplicationTag.BacnetApplicationTagDate:
                    break;
                case BacnetApplicationTag.BacnetApplicationTagTime:
                    break;
                case BacnetApplicationTag.BacnetApplicationTagObjectID:
                    break;
                case BacnetApplicationTag.BacnetApplicationTagReserve1:
                    break;
                case BacnetApplicationTag.BacnetApplicationTagReserve2:
                    break;
                case BacnetApplicationTag.BacnetApplicationTagReserve3:
                    break;
                case BacnetApplicationTag.BacnetApplicationTagDateTime:
                    break;
                case BacnetApplicationTag.BacnetApplicationTagListUnsign:
                    break;
                case BacnetApplicationTag.BacnetApplicationTagListChar:
                    break;
                case BacnetApplicationTag.BacnetApplicationTagPriortyary:
                    break;
                case BacnetApplicationTag.BacnetApplicationTagTimestamp:
                    break;
                case BacnetApplicationTag.BacnetApplicationTagActiveCOV:
                    break;
                case BacnetApplicationTag.BacnetApplicationTagConstructed:
                    break;
                case BacnetApplicationTag.BacnetApplicationTagAny:
                    break;
                case BacnetApplicationTag.MaxBacnetApplicationTag:
                    break;
                case BacnetApplicationTag.TagNotSupported:
                    break;
                default:
                    break;
            }

            return propValueArray;
        }

        private static PrListOfBacnetDevObjRef GenerateStructureDevObjRef(object propertyValue, Dictionary<IntPtr, Type> structurePointers)
        {
            PrListOfBacnetDevObjRef prListOfBacnetDevObjRef = new PrListOfBacnetDevObjRef();

            if (propertyValue is ICollection)
            {
                List<BacnetDevObjRefModel> bacnetDevObjRefList = propertyValue as List<BacnetDevObjRefModel>;

                prListOfBacnetDevObjRef.arraySize = (uint)bacnetDevObjRefList.Count;

                if (bacnetDevObjRefList.Count > 0)
                {
                    IntPtr devObjRefPtr = IntPtr.Zero;
                    int devObjRefSize = Marshal.SizeOf(typeof(ListOfBacnetDevObjRef));

                    ListOfBacnetDevObjRef listOfBacnetDevObjReff = new ListOfBacnetDevObjRef();
                    BacnetDevObjRefModel bACnetDevObjRefModel = new BacnetDevObjRefModel();
                    bACnetDevObjRefModel = bacnetDevObjRefList[0];
                    BacnetDevObjRef devObjRef = new BacnetDevObjRef();

                    devObjRef.objID = bACnetDevObjRefModel.ObjectID;
                    devObjRef.objectType = bACnetDevObjRefModel.ObjectType;
                    devObjRef.deviceIDPresent = bACnetDevObjRefModel.DeviceIDPresent;

                    if (bACnetDevObjRefModel.DeviceIDPresent)
                    {
                        devObjRef.deviceType = bACnetDevObjRefModel.DeviceType;
                        devObjRef.deviceID = bACnetDevObjRefModel.DeviceID;
                    }

                    listOfBacnetDevObjReff.stDevObjRef = devObjRef;

                    if (bacnetDevObjRefList.Count > 1)
                    {
                        devObjRefPtr = Marshal.AllocHGlobal(devObjRefSize);
                        listOfBacnetDevObjReff.next = devObjRefPtr;

                        if (!structurePointers.ContainsKey(devObjRefPtr))
                            structurePointers.Add(devObjRefPtr, typeof(ListOfBacnetDevObjRef));
                    }

                    IntPtr listOfBacnetDevObjRefptr = StructureConverter.StructureToPtr<ListOfBacnetDevObjRef>(listOfBacnetDevObjReff);
                    prListOfBacnetDevObjRef.listOfDevObjReff = listOfBacnetDevObjRefptr;

                    if (!structurePointers.ContainsKey(listOfBacnetDevObjRefptr))
                        structurePointers.Add(listOfBacnetDevObjRefptr, typeof(ListOfBacnetDevObjRef));

                    for (int cnt = 1; cnt < bacnetDevObjRefList.Count; cnt++)
                    {
                        listOfBacnetDevObjReff = new ListOfBacnetDevObjRef();

                        bACnetDevObjRefModel = new BacnetDevObjRefModel();
                        bACnetDevObjRefModel = bacnetDevObjRefList[cnt];

                        devObjRef = new BacnetDevObjRef();

                        devObjRef.deviceIDPresent = bACnetDevObjRefModel.DeviceIDPresent;
                        devObjRef.objectType = bACnetDevObjRefModel.ObjectType;
                        devObjRef.objID = bACnetDevObjRefModel.ObjectID;

                        if (bACnetDevObjRefModel.DeviceIDPresent)
                        {
                            devObjRef.deviceType = bACnetDevObjRefModel.DeviceType;
                            devObjRef.deviceID = bACnetDevObjRefModel.DeviceID;
                        }

                        listOfBacnetDevObjReff.stDevObjRef = devObjRef;

                        IntPtr currentVal = devObjRefPtr;

                        if (cnt < bacnetDevObjRefList.Count - 1)
                        {
                            devObjRefPtr = Marshal.AllocHGlobal(devObjRefSize);
                            listOfBacnetDevObjReff.next = devObjRefPtr;

                            if (!structurePointers.ContainsKey(devObjRefPtr))
                                structurePointers.Add(devObjRefPtr, typeof(ListOfBacnetDevObjRef));
                        }

                        Marshal.StructureToPtr(listOfBacnetDevObjReff, currentVal, false);
                    }
                }
            }

            return prListOfBacnetDevObjRef;
        }

        private static PrListOfObjId GenerateObjIDRefList(object propertyValue, Dictionary<IntPtr, Type> structurePointers)
        {
            PrListOfObjId prListOfObjId = new PrListOfObjId();

            if (propertyValue is ICollection)
            {
                List<ObjectIdentifierModel> bacnetObjRefList = propertyValue as List<ObjectIdentifierModel>;
                prListOfObjId.objCount = (uint)bacnetObjRefList.Count;

                if (bacnetObjRefList.Count > 0)
                {
                    IntPtr devObjRefPtr = IntPtr.Zero;
                    int devObjRefSize = Marshal.SizeOf(typeof(ListOfObjId));

                    ListOfObjId listOfObjId = new ListOfObjId();
                    ObjectIdentifierModel objectIdentifierModel = new ObjectIdentifierModel();
                    objectIdentifierModel = bacnetObjRefList[0];

                    listOfObjId.stObjectID.objectType = objectIdentifierModel.ObjectType;
                    listOfObjId.stObjectID.objId = objectIdentifierModel.InstanceNumber;

                    if (bacnetObjRefList.Count > 1)
                    {
                        devObjRefPtr = Marshal.AllocHGlobal(devObjRefSize);
                        listOfObjId.next = devObjRefPtr;

                        if (!structurePointers.ContainsKey(devObjRefPtr))
                            structurePointers.Add(devObjRefPtr, typeof(ListOfObjId));
                    }

                    IntPtr listOfBacnetDevObjRefptr = StructureConverter.StructureToPtr<ListOfObjId>(listOfObjId);
                    prListOfObjId.arrayObjID = listOfBacnetDevObjRefptr;

                    if (!structurePointers.ContainsKey(listOfBacnetDevObjRefptr))
                        structurePointers.Add(listOfBacnetDevObjRefptr, typeof(ListOfObjId));

                    if (bacnetObjRefList.Count > 1)
                    {
                        for (int cnt = 1; cnt < bacnetObjRefList.Count; cnt++)
                        {
                            listOfObjId = new ListOfObjId();

                            objectIdentifierModel = new ObjectIdentifierModel();
                            objectIdentifierModel = bacnetObjRefList[cnt];

                            listOfObjId.stObjectID.objId = objectIdentifierModel.InstanceNumber;
                            listOfObjId.stObjectID.objectType = objectIdentifierModel.ObjectType;

                            IntPtr currentVal = devObjRefPtr;

                            if (cnt < bacnetObjRefList.Count - 1)
                            {
                                devObjRefPtr = Marshal.AllocHGlobal(devObjRefSize);
                                listOfObjId.next = devObjRefPtr;

                                if (!structurePointers.ContainsKey(devObjRefPtr))
                                    structurePointers.Add(devObjRefPtr, typeof(ListOfBacnetDevObjRef));
                            }

                            Marshal.StructureToPtr(listOfObjId, currentVal, false);
                        }
                    }
                }
            }

            return prListOfObjId;
        }

        private static PrListOfCharStr GenerateStructureListOfCharString(object propertyValue, Dictionary<IntPtr, Type> structurePointers)
        {
            PrListOfCharStr prListOfCharStr = new PrListOfCharStr();

            if (propertyValue is ICollection)
            {
                string[] resultArray = null;
                resultArray = propertyValue as string[];

                prListOfCharStr.count = Convert.ToUInt32(resultArray.Length);
                int listOfCharStrSize = Marshal.SizeOf(typeof(ListOfCharStr));
                IntPtr nextCharStringPtr = IntPtr.Zero;
                int arrLength = 0;

                if (resultArray.Length > 0)
                {
                    ListOfCharStr listOfCharStr = new ListOfCharStr();
                    BacnetCharStr bacnetCharStr = new BacnetCharStr();
                    bacnetCharStr.charStr = new byte[BacDelStackConfig.MaxCharacterStringBytes];

                    byte[] charArrByte = Encoding.ASCII.GetBytes(resultArray[0]);

                    if (charArrByte.Length <= 255)
                        arrLength = charArrByte.Length;
                    else
                        arrLength = 255;

                    bacnetCharStr.strLen = Convert.ToUInt32(arrLength);
                    Buffer.BlockCopy(charArrByte, 0, bacnetCharStr.charStr, 0, arrLength);
                    listOfCharStr.charStr = bacnetCharStr;

                    if (resultArray.Length > 1)
                    {
                        nextCharStringPtr = Marshal.AllocHGlobal(listOfCharStrSize);
                        listOfCharStr.next = nextCharStringPtr;

                        if (!structurePointers.ContainsKey(nextCharStringPtr))
                            structurePointers.Add(nextCharStringPtr, typeof(ListOfCharStr));
                    }

                    prListOfCharStr.stStringVal = listOfCharStr;

                    if (resultArray.Length > 1)
                    {
                        for (int cnt = 1; cnt < resultArray.Length; cnt++)
                        {
                            listOfCharStr = new ListOfCharStr();
                            bacnetCharStr = new BacnetCharStr();
                            bacnetCharStr.charStr = new byte[BacDelStackConfig.MaxCharacterStringBytes];

                            charArrByte = Encoding.ASCII.GetBytes(resultArray[cnt]);

                            if (charArrByte.Length <= 255)
                                arrLength = charArrByte.Length;
                            else
                                arrLength = 255;

                            bacnetCharStr.strLen = Convert.ToUInt32(arrLength);

                            Buffer.BlockCopy(charArrByte, 0, bacnetCharStr.charStr, 0, arrLength);
                            listOfCharStr.charStr = bacnetCharStr;

                            IntPtr current = nextCharStringPtr;

                            if (cnt < resultArray.Length - 1)
                            {
                                nextCharStringPtr = Marshal.AllocHGlobal(listOfCharStrSize);
                                listOfCharStr.next = nextCharStringPtr;

                                if (!structurePointers.ContainsKey(nextCharStringPtr))
                                    structurePointers.Add(nextCharStringPtr, typeof(ListOfCharStr));
                            }

                            Marshal.StructureToPtr(listOfCharStr, current, false);
                        }
                    }
                }
            }

            return prListOfCharStr;
        }
    }
}
