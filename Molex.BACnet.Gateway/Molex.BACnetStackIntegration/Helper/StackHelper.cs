﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.Helpers
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Molex.BACnetStackIntegration.Helpers
{
    internal static class StackHelper
    {
        public static string FormatIPAddress(uint ip1, uint ip2, uint ip3, uint ip4)
        {
            return ip1 + "." + ip2 + "." + ip3 + "." + ip4;
        }

        public static T ByteArrayToStructure<T>(byte[] bytearray, int startIndex, out int byteRead)
        {
            int length = Marshal.SizeOf(typeof(T));
            byteRead = length;
            IntPtr intPt = Marshal.AllocHGlobal(length);
            Marshal.Copy(bytearray, startIndex, intPt, length);
            T obj = (T)Marshal.PtrToStructure(intPt, typeof(T));
            Marshal.FreeHGlobal(intPt);
            intPt = IntPtr.Zero;
            return obj;
        }

        public static byte[] StructureToByteArray(object obj, int size = 0)
        {
            size = (size == 0) ? Marshal.SizeOf(obj) : size;
            byte[] byteArray = new byte[size];
            IntPtr ptr = Marshal.AllocHGlobal(size);
            Marshal.StructureToPtr(obj, ptr, true);
            Marshal.Copy(ptr, byteArray, 0, size);
            Marshal.DestroyStructure(ptr, obj.GetType());
            Marshal.FreeHGlobal(ptr);
            ptr = IntPtr.Zero;
            return byteArray;
        }

        public static List<byte> GetByteFormMACAddress(string macAdress)
        {
            List<byte> _valueBytes = null;
            try
            {
                if (macAdress == null || macAdress.Length == 0)
                {
                    _valueBytes = null;
                }
                else
                {
                    string[] parts = macAdress.Split(':');
                    if (parts.Length > 1)
                    {
                        _valueBytes = new List<byte>();
                        for (int i = 0; i < parts.Length; i++)
                        {
                            _valueBytes.Add(byte.Parse(parts[i], System.Globalization.NumberStyles.HexNumber));
                        }
                    }
                    else
                    {
                        macAdress = MACAdressFormat(macAdress);
                        _valueBytes = GetByteFormMACAddress(macAdress);
                        //char[] stringparts = macAdress.ToArray();
                        //_valueBytes = new List<byte>();
                        //_valueBytes = System.Text.Encoding.UTF8.GetBytes(stringparts).ToList();
                    }
                }
            }
            catch (Exception)
            {
                return _valueBytes;
            }

            return _valueBytes;
        }



        public static string MACAdressFormat(string str)
        {
            string format = "000000000000";
            if (!string.IsNullOrEmpty(str))
            {
                int length = str.Length;
                int tempLength = format.Length - length;
                format = format.Substring(0, tempLength);
                format = format.Insert(tempLength, str);

                for (int i = 2; i < format.Length; )
                {
                    format = format.Insert(i, ":");
                    i = i + 3;
                }
            }
            return format;
        }
    }
}