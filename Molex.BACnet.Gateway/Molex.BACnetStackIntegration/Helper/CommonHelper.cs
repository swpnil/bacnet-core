﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.Helper
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------

using Molex.BACnetStackIntegration.Stack.Wrapper;
using Molex.StackDataObjects.APIModels;
using Molex.StackDataObjects.Constants;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Molex.BACnetStackIntegration.Helper
{
    internal static class CommanHelper
    {
        static CommanHelper()
        {
        }

        public static PropertyValues GetPropertyValuesStructure(List<PropertyValueModel> PropertyValueList, ref Dictionary<IntPtr, Type> structurePointers)
        {
            int propertyValuesSize = Marshal.SizeOf(typeof(PropertyValues));
            PropertyValues propertyValueStructure = new PropertyValues();

            if (PropertyValueList.Count > 0)
            {
                PropertyValues propertyValue = new PropertyValues();

                PropertyValueModel propertyValueModel = PropertyValueList[0];
                propertyValue.dataType = propertyValueModel.DataType;
                propertyValue.isArrayIndxPresent = propertyValueModel.IsArrayIndxPresent;
                propertyValue.objectProperty = propertyValueModel.ObjectProperty;
                propertyValue.priority = propertyValueModel.Priority;
                propertyValue.propertyArrayIndex = propertyValueModel.PropertyArrayIndex;
                IntPtr propValPointer = StructureConverter.StructureToPtr(propertyValueModel.PropertyValue);
                propertyValue.propVal = propValPointer;

                propertyValueStructure = propertyValue;

                IntPtr nextPropertyValuePtr = IntPtr.Zero;

                if (PropertyValueList.Count > 1)
                {
                    nextPropertyValuePtr = Marshal.AllocHGlobal(propertyValuesSize);
                    propertyValueStructure.nextVal = nextPropertyValuePtr;

                    if (!structurePointers.ContainsKey(nextPropertyValuePtr))
                        structurePointers.Add(nextPropertyValuePtr, typeof(PropertyValues));
                }

                for (int i = 1; i < PropertyValueList.Count; i++)
                {
                    propertyValue = new PropertyValues();

                    propertyValueModel = new PropertyValueModel();
                    propertyValueModel = PropertyValueList[i];
                    propertyValue.dataType = propertyValueModel.DataType;
                    propertyValue.isArrayIndxPresent = propertyValueModel.IsArrayIndxPresent;
                    propertyValue.objectProperty = propertyValueModel.ObjectProperty;
                    propertyValue.priority = propertyValueModel.Priority;
                    propertyValue.propertyArrayIndex = propertyValueModel.PropertyArrayIndex;
                    IntPtr propValPtr = StructureConverter.StructureToPtr(propertyValueModel.PropertyValue);
                    propertyValue.propVal = propValPointer;

                    IntPtr current = nextPropertyValuePtr;

                    if (i < PropertyValueList.Count - 1)
                    {
                        nextPropertyValuePtr = Marshal.AllocHGlobal(propertyValuesSize);
                        propertyValue.nextVal = nextPropertyValuePtr;

                        if (!structurePointers.ContainsKey(nextPropertyValuePtr))
                            structurePointers.Add(nextPropertyValuePtr, typeof(PropertyValues));
                    }

                    Marshal.StructureToPtr(propertyValue, current, false);
                }
            }

            return propertyValueStructure;
        }

        public static bool GetBit(byte statusFlags, int bitNumber)
        {
            return (statusFlags & (1 << bitNumber)) != 0;
        }

        public static byte SetBit(this byte b, int pos)
        {
            if (pos < 0 || pos > 7)
                throw new ArgumentOutOfRangeException("pos", "Index must be in the range of 0-7.");

            return (byte)(b | (1 << pos));
        }

        public static byte UnsetBit(this byte b, int pos)
        {
            if (pos < 0 || pos > 7)
                throw new ArgumentOutOfRangeException("pos", "Index must be in the range of 0-7.");

            return (byte)(b & ~(1 << pos));
        }

        public static string ByteArrayToString(byte[] ba)
        {
            string hex = BitConverter.ToString(ba);
            return hex.Replace("-", ":");
        }

        public static byte[] StringToByteArray(string str)
        {
            string[] hexItem = str.Split(':');
            byte[] bytes = new byte[hexItem.Length];

            for (int i = 0; i < hexItem.Length; i++)
            {

                if (hexItem[i].Length < 2)
                {
                    hexItem[i] = "0" + hexItem[i];
                }

                bytes[i] = Convert.ToByte(hexItem[i], 16);
            }

            return bytes;
        }

        public static byte[] HexStringToByteArray(string str)
        {
            int NumberChars = str.Length;
            byte[] bytes = new byte[NumberChars / 2];

            for (int i = 0; i < NumberChars; i += 2)
            {
                bytes[i / 2] = Convert.ToByte(str.Substring(i, 2), 16);
            }

            return bytes;
        }

        public static BacnetDataType GetPropertyDataType(BacnetPropertyID propertyID, BacnetObjectType bacnetObjectType, BacnetDataType dataType)
        {
            BacnetDataType bacnetDataType = dataType;

            if (dataType == BacnetDataType.BacnetDTNull)
                return dataType;

            switch (propertyID)
            {
                case BacnetPropertyID.PropDateList:
                    bacnetDataType = BacnetDataType.BacnetDTDateList;
                    break;
                case BacnetPropertyID.PropNotifyType:
                    bacnetDataType = BacnetDataType.BacnetDTEnumNew;
                    break;
                case BacnetPropertyID.PropLogDeviceObjectProperty:
                    bacnetDataType = BacnetDataType.BacnetDTDevObjPropReff;
                    break;
                case BacnetPropertyID.PropDescription:
                case BacnetPropertyID.PropObjectName:
                case BacnetPropertyID.PropStateDescription:
                    bacnetDataType = BacnetDataType.BacnetDTCharString;
                    break;
                case BacnetPropertyID.PropObjectType:
                    bacnetDataType = BacnetDataType.BacnetDTObjType;
                    break;
                case BacnetPropertyID.PropObjectIdentifier:
                    bacnetDataType = BacnetDataType.BacnetDTObjectID;
                    break;
                case BacnetPropertyID.PropPresentValue:
                    switch (bacnetObjectType)
                    {
                        case BacnetObjectType.ObjectAnalogInput:
                        case BacnetObjectType.ObjectAnalogOutput:
                        case BacnetObjectType.ObjectAnalogValue:
                        case BacnetObjectType.ObjectPulseConverter:
                        case BacnetObjectType.ObjectLoop:
                        case BacnetObjectType.ObjectLightingOutput:
                            bacnetDataType = BacnetDataType.BacnetDTReal;
                            break;
                        case BacnetObjectType.ObjectBinaryInput:
                            bacnetDataType = BacnetDataType.BacnetDTEnum;
                            break;
                        case BacnetObjectType.ObjectBinaryOutput:
                        case BacnetObjectType.ObjectBinaryValue:
                            bacnetDataType = BacnetDataType.BacnetDTEnum;
                            break;
                        case BacnetObjectType.ObjectCalendar:
                            bacnetDataType = BacnetDataType.BacnetDTBoolean;
                            break;
                        case BacnetObjectType.ObjectCommand:
                        case BacnetObjectType.ObjectPositiveIntegerValue:
                        case BacnetObjectType.ObjectMultiStateInput:
                        case BacnetObjectType.ObjectMultiStateOutput:
                        case BacnetObjectType.ObjectMultiStateValue:
                            bacnetDataType = BacnetDataType.BacnetDTUnsigned;
                            break;
                        case BacnetObjectType.ObjectBitStringValue:
                            bacnetDataType = BacnetDataType.BacnetDTBitStringNew;
                            break;
                        case BacnetObjectType.ObjectCharacterStringValue:
                            bacnetDataType = BacnetDataType.BacnetDTCharString;
                            break;
                        case BacnetObjectType.ObjectOctetStringValue:
                            bacnetDataType = BacnetDataType.BacnetDTOctetString;
                            break;
                        case BacnetObjectType.ObjectDatePatternValue:
                        case BacnetObjectType.ObjectDateValue:
                            bacnetDataType = BacnetDataType.BacnetDTDate;
                            break;
                        case BacnetObjectType.ObjectDateTimePatternValue:
                        case BacnetObjectType.ObjectDateTimeValue:
                            bacnetDataType = BacnetDataType.BacnetDTDateTime;
                            break;
                        case BacnetObjectType.ObjectIntegerValue:
                            bacnetDataType = BacnetDataType.BacnetDTInteger;
                            break;
                        case BacnetObjectType.ObjectLargeAnalogValue:
                            bacnetDataType = BacnetDataType.BacnetDTDouble;
                            break;
                        case BacnetObjectType.ObjectTimePatternValue:
                        case BacnetObjectType.ObjectTimeValue:
                            bacnetDataType = BacnetDataType.BacnetDTTime;
                            break;
                        case BacnetObjectType.ObjectChannel:
                            bacnetDataType = BacnetDataType.BacnetDTChannelValue;
                            break;
                        default:
                            break;
                    }
                    break;
                case BacnetPropertyID.PropUnits:
                    //case BacnetPropertyID.PropProprietaryColorTemperatureUnit:
                    bacnetDataType = BacnetDataType.BacnetDTEnumNew;
                    break;
                case BacnetPropertyID.PropOutOfService:
                    bacnetDataType = BacnetDataType.BacnetDTBoolean;
                    break;
                case BacnetPropertyID.PropStatusFlags:
                    bacnetDataType = BacnetDataType.BacnetDTBitString;
                    break;
                case BacnetPropertyID.PropObjectPropertyReference:
                    bacnetDataType = BacnetDataType.BacnetDTDevObjPropReff;
                    break;
                case BacnetPropertyID.PropListOfObjectPropertyReferences:
                    bacnetDataType = BacnetDataType.BacnetDTDevObjPropReffList;
                    break;
                case BacnetPropertyID.PropAllWritesSuccessful:
                case BacnetPropertyID.PropInProcess:
                case BacnetPropertyID.PropIsUtc:
                    bacnetDataType = BacnetDataType.BacnetDTBoolean;
                    break;
                case BacnetPropertyID.PropAdjustValue:
                    bacnetDataType = BacnetDataType.BacnetDTReal;
                    break;
                case BacnetPropertyID.PropValueBeforeChange:
                case BacnetPropertyID.PropValueSet:
                case BacnetPropertyID.PropPulseRate:
                    bacnetDataType = BacnetDataType.BacnetDTUnsigned32;
                    break;
                case BacnetPropertyID.PropSetPoint:
                    bacnetDataType = BacnetDataType.BacnetDTReal;
                    break;
                case BacnetPropertyID.PropProgramChange:
                case BacnetPropertyID.PropProgramState:
                case BacnetPropertyID.PropEventState:
                    bacnetDataType = BacnetDataType.BacnetDTEnumNew;
                    break;
                case BacnetPropertyID.PropSystemStatus:
                    bacnetDataType = BacnetDataType.BacnetDTEnumNew;
                    break;
                case BacnetPropertyID.PropMinimumValue:
                case BacnetPropertyID.PropAverageValue:
                case BacnetPropertyID.PropMaximumValue:
                    bacnetDataType = BacnetDataType.BacnetDTReal;
                    break;
                case BacnetPropertyID.PropRelinquishDefault:
                    switch (bacnetObjectType)
                    {
                        case BacnetObjectType.ObjectAnalogInput:
                        case BacnetObjectType.ObjectAnalogOutput:
                        case BacnetObjectType.ObjectAnalogValue:
                        case BacnetObjectType.ObjectPulseConverter:
                        case BacnetObjectType.ObjectLoop:
                            bacnetDataType = BacnetDataType.BacnetDTReal;
                            break;
                        case BacnetObjectType.ObjectBinaryInput:
                        case BacnetObjectType.ObjectBinaryOutput:
                        case BacnetObjectType.ObjectBinaryValue:
                            bacnetDataType = BacnetDataType.BacnetDTEnum;
                            break;
                        case BacnetObjectType.ObjectCalendar:
                            bacnetDataType = BacnetDataType.BacnetDTBoolean;
                            break;
                        case BacnetObjectType.ObjectCommand:
                        case BacnetObjectType.ObjectPositiveIntegerValue:
                        case BacnetObjectType.ObjectMultiStateInput:
                        case BacnetObjectType.ObjectMultiStateOutput:
                        case BacnetObjectType.ObjectMultiStateValue:
                            bacnetDataType = BacnetDataType.BacnetDTUnsigned;
                            break;
                        case BacnetObjectType.ObjectBitStringValue:
                            bacnetDataType = BacnetDataType.BacnetDTBitStringNew;
                            break;
                        case BacnetObjectType.ObjectCharacterStringValue:
                            bacnetDataType = BacnetDataType.BacnetDTCharString;
                            break;
                        case BacnetObjectType.ObjectOctetStringValue:
                            bacnetDataType = BacnetDataType.BacnetDTOctetString;
                            break;
                        case BacnetObjectType.ObjectDatePatternValue:
                        case BacnetObjectType.ObjectDateValue:
                            bacnetDataType = BacnetDataType.BacnetDTDate;
                            break;
                        case BacnetObjectType.ObjectDateTimePatternValue:
                        case BacnetObjectType.ObjectDateTimeValue:
                            bacnetDataType = BacnetDataType.BacnetDTDateTime;
                            break;
                        case BacnetObjectType.ObjectIntegerValue:
                            bacnetDataType = BacnetDataType.BacnetDTInteger;
                            break;
                        case BacnetObjectType.ObjectLargeAnalogValue:
                            bacnetDataType = BacnetDataType.BacnetDTDouble;
                            break;
                        case BacnetObjectType.ObjectTimePatternValue:
                        case BacnetObjectType.ObjectTimeValue:
                            bacnetDataType = BacnetDataType.BacnetDTTime;
                            break;
                        default:
                            break;
                    }
                    break;
                case BacnetPropertyID.PropEventParameters:
                    bacnetDataType = BacnetDataType.BacnetDTEventParameters;
                    break;
                case BacnetPropertyID.PropClientCOVIncrement:
                    bacnetDataType = BacnetDataType.BacnetDTClientCOVIncrement;
                    break;
                case BacnetPropertyID.PropLoggingType:
                    bacnetDataType = BacnetDataType.BacnetDTEnumNew;
                    break;
                case BacnetPropertyID.PropLogInterval:
                    bacnetDataType = BacnetDataType.BacnetDTUnsigned32;
                    break;
                case BacnetPropertyID.PropStartTime:
                case BacnetPropertyID.PropStopTime:
                    bacnetDataType = BacnetDataType.BacnetDTDateTime;
                    break;
                case BacnetPropertyID.PropStopWhenFull:
                case BacnetPropertyID.PropEnable:
                    bacnetDataType = BacnetDataType.BacnetDTBoolean;
                    break;
                case BacnetPropertyID.PropAckRequired:
                case BacnetPropertyID.PropEventEnable:
                    bacnetDataType = BacnetDataType.BacnetDTBitString;
                    break;
                case BacnetPropertyID.PropPriority:
                    bacnetDataType = BacnetDataType.BacnetDTNotificationPriority;
                    break;
                case BacnetPropertyID.PropRecipientList:
                    bacnetDataType = BacnetDataType.BacnetDTDestinationList;
                    break;
                case BacnetPropertyID.PropExceptionSchedule:
                    bacnetDataType = BacnetDataType.BacnetDTSpecialEventArray;
                    break;
                case BacnetPropertyID.PropWeeklySchedule:
                    bacnetDataType = BacnetDataType.BacnetDTDailyScheduleArray;
                    break;
                case BacnetPropertyID.PropEffectivePeriod:
                    bacnetDataType = BacnetDataType.BacnetDTDateRange; ;
                    break;
                case BacnetPropertyID.PropScheduleDefault:
                    bacnetDataType = BacnetDataType.BacnetDTSchedulePresentDefault;
                    break;
                case BacnetPropertyID.PropPriorityForWriting:
                    bacnetDataType = BacnetDataType.BacnetDTUnsigned32;
                    break;
                case BacnetPropertyID.PropInProgress:
                case BacnetPropertyID.PropWriteStatus:
                    bacnetDataType = BacnetDataType.BacnetDTEnumNew;
                    break;
                case BacnetPropertyID.PropNotificationClass:
                    bacnetDataType = BacnetDataType.BacnetDTUnsigned;
                    break;
                case BacnetPropertyID.PropProtocolObjectTypesSupported:
                    bacnetDataType = BacnetDataType.BacnetDTObjectTypeSupported;
                    break;
                case BacnetPropertyID.PropProtocolServicesSupported:
                    bacnetDataType = BacnetDataType.BacnetDTServicesSupported;
                    break;
                case BacnetPropertyID.PropStructuredObjectList:
                    bacnetDataType = BacnetDataType.BacnetDTObjectIDArray;
                    break;
                case BacnetPropertyID.PropSubordinateList:
                    bacnetDataType = BacnetDataType.BacnetDTDevObjReffArray;
                    break;
                case BacnetPropertyID.PropSubordinateAnnotations:
                    bacnetDataType = BacnetDataType.BacnetDTCharStringArray;
                    break;
                default:
                    break;
            }

            return bacnetDataType;
        }

        public static BacnetAddressModel GetBacnetAddressModel(BacnetAddress bacnetAddress)
        {
            BacnetAddressModel bacnetAddressModel = new BacnetAddressModel();
            bacnetAddressModel.IPAddress = IpAddressHelper.ConvertBinaryIptoString(bacnetAddress.ipAddrs);
            bacnetAddressModel.Port = (uint)IpAddressHelper.GetPortFromBinaryIp(bacnetAddress.ipAddrs);
            bacnetAddressModel.MacAddress = IpAddressHelper.GetMacAddressStringFromByteArray(bacnetAddress.dLen, bacnetAddress.dvDadr);
            bacnetAddressModel.MacLength = bacnetAddress.dLen;
            bacnetAddressModel.IPAddressLength = bacnetAddress.macLen;
            bacnetAddressModel.NetworkNumber = bacnetAddress.net;

            return bacnetAddressModel;
        }

        public static byte[] GetByteArrayFromBitString(string bitString)
        {
            int numOfBytes = bitString.Length / 8;
            byte[] arrayBytes = new byte[numOfBytes];

            for (int i = 0; i < numOfBytes; ++i)
            {
                arrayBytes[i] = Convert.ToByte(bitString.Substring(8 * i, 8), 2);
            }

            return arrayBytes;
        }

        public static bool IsCommandableObject(uint devID, uint objID, BacnetObjectType objectType, BacnetPropertyID propID)
        {
            bool isCommandable = false;

            if (propID == BacnetPropertyID.PropPresentValue ||
                propID == BacnetPropertyID.PropRelinquishDefault)
            {
                switch (objectType)
                {
                    case BacnetObjectType.ObjectAnalogOutput:
                    case BacnetObjectType.ObjectAnalogValue:
                    case BacnetObjectType.ObjectBinaryOutput:
                    case BacnetObjectType.ObjectBinaryValue:
                    case BacnetObjectType.ObjectMultiStateOutput:
                    case BacnetObjectType.ObjectMultiStateValue:
                    case BacnetObjectType.ObjectPositiveIntegerValue:
                        isCommandable = true;
                        break;
                    default:
                        isCommandable = false;
                        break;
                }
            }

            return isCommandable;
        }
    }
}
