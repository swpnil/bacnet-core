﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.Helper
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using System.Linq;
using System.Net;
using System.Net.Sockets;

namespace Molex.BACnetStackIntegration.Helper
{
    public static class IpAddressHelper
    {
        public static string ConvertBinaryIptoString(byte[] ipAddrs)
        {
            return string.Format("{0}.{1}.{2}.{3}", ipAddrs[0], ipAddrs[1], ipAddrs[2], ipAddrs[3]);
        }

        public static int GetPortFromBinaryIp(byte[] ipAddrs)
        {
            int portNumber = 0;
            portNumber = ipAddrs[4] << 8;
            portNumber |= ipAddrs[5];

            return portNumber;
        }

        private static string GetLocalIpAddress()
        {
            string localIpAddress = null;

            var host = Dns.GetHostEntry(Dns.GetHostName());

            IPAddress ipAddress1 = null;
            foreach (var ip in host.AddressList.Where(ip => ip.AddressFamily == AddressFamily.InterNetwork))
                ipAddress1 = ip;

            if (ipAddress1 != null)
            {
                var tempAdr1 = ipAddress1.GetAddressBytes();
                localIpAddress = ConvertBinaryIptoString(new byte[] { tempAdr1[0], tempAdr1[1], tempAdr1[2], tempAdr1[3], 0xba, 0xc0 });
            }

            return localIpAddress;
        }

        private static string _localIpAddress;

        public static string LocalIpAddress
        {
            get
            {
                if (string.IsNullOrEmpty(_localIpAddress))
                    _localIpAddress = GetLocalIpAddress();
                return _localIpAddress;
            }
        }

        public static string GetMacAddressStringFromByteArray(int len, byte[] macAddress)
        {
            string mac = null;
            for (int i = 0; i < len; i++)
            {
                string hexValue = macAddress[i].ToString("X");
                if (hexValue.Length == 1)
                    hexValue = "0" + hexValue;
                mac += hexValue;
            }

            return mac;
        }

        public static byte[] ConvertStringIpToByteArray(string ipAddress)
        {
            IPAddress ip = IPAddress.Parse(ipAddress);
            byte[] ipAddrArray = ip.GetAddressBytes();

            return ipAddrArray;
        }
    }
}
