﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.ManagedWrapper.Helper
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.APIModels;
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Request;

namespace Molex.BACnetStackIntegration.ManagedWrapper.Helper
{
    internal static class FallbackHelper
    {
        internal static ReadPropertyRequest CreateReadPropertyRequest(ReadPropertyRequest readPropertyRequest, uint arrayIndex)
        {
            ReadPropertyRequest rpRequest = new ReadPropertyRequest();
            rpRequest.DestinationAddress = readPropertyRequest.DestinationAddress;
            rpRequest.SourceDeviceId = readPropertyRequest.SourceDeviceId;
            rpRequest.DestinationDeviceId = readPropertyRequest.DestinationDeviceId;
            rpRequest.DestinationTypeFlag = readPropertyRequest.DestinationTypeFlag;

            rpRequest.RpRequest = new ReadPropertyRequestModel();
            rpRequest.RpRequest.IsArrayIndexPresent = 1;
            rpRequest.RpRequest.ArrayIndex = arrayIndex;
            rpRequest.RpRequest.ObjectInstanceNumber = readPropertyRequest.RpRequest.ObjectInstanceNumber;
            rpRequest.RpRequest.ObjectProperty = readPropertyRequest.RpRequest.ObjectProperty;
            rpRequest.RpRequest.ObjectType = readPropertyRequest.RpRequest.ObjectType;

            return rpRequest;
        }

        internal static bool IsArrayProperty(BacnetObjectType objectType, BacnetPropertyID propId)
        {
            bool isArrayProp = false;
            switch (objectType)
            {
                case BacnetObjectType.ObjectAnalogInput:
                case BacnetObjectType.ObjectBinaryInput:
                case BacnetObjectType.ObjectTrendlog:
                case BacnetObjectType.ObjectAccumulator:
                case BacnetObjectType.ObjectEventEnrollment:
                case BacnetObjectType.ObjectLifeSafetyPoint:
                case BacnetObjectType.ObjectLifeSafetyZone:
                case BacnetObjectType.ObjectLoop:
                case BacnetObjectType.ObjectPulseConverter:
                case BacnetObjectType.ObjectEventLog:
                    switch (propId)
                    {
                        case BacnetPropertyID.PropEventTimestamps:
                        case BacnetPropertyID.PropEventMessageTexts:
                            isArrayProp = true;
                            break;
                        default:
                            isArrayProp = false;
                            break;
                    }
                    break;
                case BacnetObjectType.ObjectDevice:
                    switch (propId)
                    {
                        case BacnetPropertyID.PropObjectList:
                        case BacnetPropertyID.PropStructuredObjectList:
                        case BacnetPropertyID.PropConfigurationFiles:
                        case BacnetPropertyID.PropSlaveProxyEnabled:
                        case BacnetPropertyID.PropAutoSlaveDiscovery:
                            isArrayProp = true;
                            break;
                        default:
                            isArrayProp = false;
                            break;
                    }
                    break;
                case BacnetObjectType.ObjectMultiStateInput:
                    switch (propId)
                    {
                        case BacnetPropertyID.PropEventTimestamps:
                        case BacnetPropertyID.PropEventMessageTexts:
                        case BacnetPropertyID.PropStateText:
                            isArrayProp = true;
                            break;
                        default:
                            isArrayProp = false;
                            break;
                    }
                    break;
                case BacnetObjectType.ObjectMultiStateOutput:
                case BacnetObjectType.ObjectMultiStateValue:
                    switch (propId)
                    {
                        case BacnetPropertyID.PropEventTimestamps:
                        case BacnetPropertyID.PropEventMessageTexts:
                        case BacnetPropertyID.PropStateText:
                        case BacnetPropertyID.PropPriorityArray:
                            isArrayProp = true;
                            break;
                        default:
                            isArrayProp = false;
                            break;
                    }
                    break;
                case BacnetObjectType.ObjectNotificationClass:
                    switch (propId)
                    {
                        case BacnetPropertyID.PropPriority:
                            isArrayProp = true;
                            break;
                        default:
                            isArrayProp = false;
                            break;
                    }
                    break;
                case BacnetObjectType.ObjectSchedule:
                    switch (propId)
                    {
                        case BacnetPropertyID.PropWeeklySchedule:
                        case BacnetPropertyID.PropExceptionSchedule:
                            isArrayProp = true;
                            break;
                        default:
                            isArrayProp = false;
                            break;
                    }
                    break;
                case BacnetObjectType.ObjectTrendLogMultiple:
                    switch (propId)
                    {
                        case BacnetPropertyID.PropLogDeviceObjectProperty:
                        case BacnetPropertyID.PropEventTimestamps:
                        case BacnetPropertyID.PropEventMessageTexts:
                            isArrayProp = true;
                            break;
                        default:
                            isArrayProp = false;
                            break;
                    }
                    break;
                case BacnetObjectType.ObjectAnalogOutput:
                case BacnetObjectType.ObjectAnalogValue:
                case BacnetObjectType.ObjectBinaryOutput:
                case BacnetObjectType.ObjectBinaryValue:
                case BacnetObjectType.ObjectLargeAnalogValue:
                case BacnetObjectType.ObjectIntegerValue:
                case BacnetObjectType.ObjectPositiveIntegerValue:
                    switch (propId)
                    {
                        case BacnetPropertyID.PropEventTimestamps:
                        case BacnetPropertyID.PropEventMessageTexts:
                        case BacnetPropertyID.PropPriorityArray:
                            isArrayProp = true;
                            break;
                        default:
                            isArrayProp = false;
                            break;
                    }
                    break;
                case BacnetObjectType.ObjectAccessDoor:
                    switch (propId)
                    {
                        case BacnetPropertyID.PropDoorMembers:
                        case BacnetPropertyID.PropEventTimestamps:
                        case BacnetPropertyID.PropEventMessageTexts:
                        case BacnetPropertyID.PropPriorityArray:
                            isArrayProp = true;
                            break;
                        default:
                            isArrayProp = false;
                            break;
                    }
                    break;
                case BacnetObjectType.ObjectCommand:
                    switch (propId)
                    {
                        case BacnetPropertyID.PropAction:
                        case BacnetPropertyID.PropActionText:
                            isArrayProp = true;
                            break;
                        default:
                            isArrayProp = false;
                            break;
                    }
                    break;
                case BacnetObjectType.ObjectLoadControl:
                    switch (propId)
                    {
                        case BacnetPropertyID.PropShedLevels:
                        case BacnetPropertyID.PropShedLevelDescriptions:
                        case BacnetPropertyID.PropEventTimestamps:
                        case BacnetPropertyID.PropEventMessageTexts:
                            isArrayProp = true;
                            break;
                        default:
                            isArrayProp = false;
                            break;
                    }
                    break;
                case BacnetObjectType.ObjectStructuredView:
                    switch (propId)
                    {
                        case BacnetPropertyID.PropSubordinateList:
                        case BacnetPropertyID.PropSubordinateAnnotations:
                            isArrayProp = true;
                            break;
                        default:
                            isArrayProp = false;
                            break;
                    }
                    break;
                case BacnetObjectType.ObjectAccessPoint:
                    switch (propId)
                    {
                        case BacnetPropertyID.PropAuthenticationPolicyList:
                        case BacnetPropertyID.PropAuthenticationPolicyNames:
                        case BacnetPropertyID.PropAccessDoors:
                        case BacnetPropertyID.PropEventTimestamps:
                        case BacnetPropertyID.PropEventMessageTexts:
                            isArrayProp = true;
                            break;
                        default:
                            isArrayProp = false;
                            break;
                    }
                    break;
                case BacnetObjectType.ObjectCredentialDataInput:
                    switch (propId)
                    {
                        case BacnetPropertyID.PropSupportedFormats:
                        case BacnetPropertyID.PropSupportedFormatClasses:
                            isArrayProp = true;
                            break;
                        default:
                            isArrayProp = false;
                            break;
                    }
                    break;
                case BacnetObjectType.ObjectCharacterStringValue:
                    switch (propId)
                    {
                        case BacnetPropertyID.PropAlarmValues:
                        case BacnetPropertyID.PropFaultValues:
                        case BacnetPropertyID.PropEventTimestamps:
                        case BacnetPropertyID.PropEventMessageTexts:
                        case BacnetPropertyID.PropPriorityArray:
                            isArrayProp = true;
                            break;
                        default:
                            isArrayProp = false;
                            break;
                    }
                    break;
                case BacnetObjectType.ObjectBitStringValue:
                    switch (propId)
                    {
                        case BacnetPropertyID.PropBitText:
                        case BacnetPropertyID.PropAlarmValues:
                        case BacnetPropertyID.PropEventTimestamps:
                        case BacnetPropertyID.PropEventMessageTexts:
                        case BacnetPropertyID.PropPriorityArray:
                            isArrayProp = true;
                            break;
                        default:
                            isArrayProp = false;
                            break;
                    }
                    break;
                case BacnetObjectType.ObjectNetworkSecurity:
                    switch (propId)
                    {
                        case BacnetPropertyID.PropNetworkAccessSecurityPolicies:
                        case BacnetPropertyID.PropKeySets:
                            isArrayProp = true;
                            break;
                        default:
                            isArrayProp = false;
                            break;
                    }
                    break;
                case BacnetObjectType.ObjectDateTimeValue:
                case BacnetObjectType.ObjectOctetStringValue:
                case BacnetObjectType.ObjectTimeValue:
                case BacnetObjectType.ObjectDateValue:
                case BacnetObjectType.ObjectDateTimePatternValue:
                case BacnetObjectType.ObjectTimePatternValue:
                case BacnetObjectType.ObjectDatePatternValue:
                    switch (propId)
                    {
                        case BacnetPropertyID.PropPriorityArray:
                            isArrayProp = true;
                            break;
                        default:
                            isArrayProp = false;
                            break;
                    }
                    break;
                case BacnetObjectType.ObjectGlobalGroup:
                    switch (propId)
                    {
                        case BacnetPropertyID.PropGroupMembers:
                        case BacnetPropertyID.PropGroupMemberNames:
                        case BacnetPropertyID.PropPresentValue:
                        case BacnetPropertyID.PropEventTimestamps:
                        case BacnetPropertyID.PropEventMessageTexts:
                            isArrayProp = true;
                            break;
                        default:
                            isArrayProp = false;
                            break;
                    }
                    break;
                default:
                    isArrayProp = false;
                    break;
            }
            return isArrayProp;

        }
    }
}
