﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.Manager
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Request;
using System;
using System.Collections.Generic;
using Molex.BACnetStackIntegration.Stack.Wrapper;
using Molex.BACnetStackIntegration.Processor;
using Molex.BACnetStackIntegration.Log;

namespace Molex.BACnetStackIntegration.Manager
{
    internal class CallbackManager
    {
        #region Constructor and Singleton Implementation

        /// <summary>
        /// Volatile instance of class.
        /// </summary>
        private static volatile CallbackManager _instance;

        /// <summary>
        /// Object for acquiring lock while creating singleton instance
        /// to ensure only one instance is created in multi threading environment.
        /// </summary>
        private static object _syncRoot = new Object();

        /// <summary>
        /// Get's the singleton instance of the class.
        /// </summary>
        public static CallbackManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_syncRoot)
                    {
                        if (_instance == null)
                            _instance = new CallbackManager();
                    }
                }

                return _instance;
            }
        }

        private CallbackManager()
        {
        }

        #endregion Constructor and Singleton Implementation

        #region Private Veriables

        private static volatile object _syncLock = new object();
        private Dictionary<RequestBase, App_Callback_Interface_t> _callbackDictionary = new Dictionary<RequestBase, App_Callback_Interface_t>();
        private Dictionary<RequestBase, MessageProcessorBase> _callbackProcessorDictionary = new Dictionary<RequestBase, MessageProcessorBase>();

        #endregion Private Veriables

        public void AddDelegateToCallbackDictionary(MessageProcessorBase callback, RequestBase request)
        {
            Logger.Instance.Log("IL Callback Count: " + _callbackProcessorDictionary.Count);
            lock (_syncLock)
            {
                if (!_callbackProcessorDictionary.ContainsKey(request))
                {
                    _callbackProcessorDictionary[request] = callback;
                }
            }
        }

        public void AddToCallbackDictionary(App_Callback_Interface_t callback, RequestBase request)
        {
            lock (_syncLock)
            {
                if (!_callbackDictionary.ContainsKey(request))
                {
                    _callbackDictionary[request] = callback;
                }
            }
        }

        public void RemoveFromCallbackDictionary(RequestBase request)
        {
            lock (_syncLock)
            {
                if (_callbackDictionary.ContainsKey(request))
                {
                    _callbackDictionary.Remove(request);
                }

            }
        }

        public void RemoveDelegateFromCallbackDictionary(RequestBase request)
        {
            Logger.Instance.Log("IL Callback Count: " + _callbackProcessorDictionary.Count);
            Logger.Instance.Log("IL Callback Removed: ");
            lock (_syncLock)
            {
                if (_callbackProcessorDictionary.ContainsKey(request))
                {
                    _callbackProcessorDictionary.Remove(request);
                }

            }
        }
    }
}
