﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.Manager
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.APIModels;
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Request;
using Molex.StackDataObjects.Response;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Molex.BACnetStackIntegration.ManagedWrapper.Helper;
using Molex.BACnetStackIntegration.Model.Request;
using Molex.BACnetStackIntegration.Model.Response;

namespace Molex.BACnetStackIntegration.Manager
{
    internal class RPFallbackManager
    {
        uint _currentArrayIndex = 0;
        uint _totalArrayIndex;
        StackIntegrationResponseResult _originalStackIntegrationResponseResult;
        StackIntegrationRequestBase _originalStackIntegrationRequestBase;
        ReadPropertyResponse _originalRequestResponse;
        List<ResponseResult> _fallbackResponses;
        uint _currentResponseCount = 0;
        object _response = null;

        internal RPFallbackManager()
        {
            _fallbackResponses = new List<ResponseResult>();
        }

        internal void HandleFallback(StackIntegrationResponseResult result)
        {
            _originalStackIntegrationResponseResult = result;
            ReadPropertyRequest rpReq = result.Request.Request as ReadPropertyRequest;

            ReadPropertyRequest readPropertyRequest = FallbackHelper.CreateReadPropertyRequest(rpReq, _currentArrayIndex);
            readPropertyRequest.ActionCompletedHandler = BasicInfoReponseHandler;

            _originalStackIntegrationRequestBase = new StackIntegrationRequestBase();
            _originalStackIntegrationRequestBase.Request = readPropertyRequest;

            MessageSenderManager.Instance.SendData(_originalStackIntegrationRequestBase);
        }

        void BasicInfoReponseHandler(ResponseResult result)
        {
            if (result.IsSucceed)
            {
                _totalArrayIndex = (uint)(result.Response as ReadPropertyResponse).PropertyValue;

                if (_totalArrayIndex == 0)
                {
                    _originalStackIntegrationResponseResult.IsSucceed = true;
                    MessageReceiverManager.Instance.ProcessResponse(_originalStackIntegrationResponseResult);
                }

                for (int count = 1; count <= _totalArrayIndex; count++)
                    ReadData();
            }
            else
            {
                _originalStackIntegrationResponseResult.IsSucceed = false;
                MessageReceiverManager.Instance.ProcessResponse(_originalStackIntegrationResponseResult);
            }
        }

        void ReadData()
        {
            _currentArrayIndex++;
            uint _arrayIndex = _currentArrayIndex;

            ReadPropertyRequest readPropertyRequest = FallbackHelper.CreateReadPropertyRequest(_originalStackIntegrationRequestBase.Request as ReadPropertyRequest, _arrayIndex);
            readPropertyRequest.ActionCompletedHandler = ReadDataHandler;

            StackIntegrationRequestBase request = new StackIntegrationRequestBase();
            request.SyncHandler = new AsyncToSyncHandler(500);//TODO: Make it configurable
            request.Request = readPropertyRequest;

            MessageSenderManager.Instance.SendData(request);
            request.SyncHandler.Sync();
        }

        void ReadDataHandler(ResponseResult result)
        {
            if (_currentResponseCount == _totalArrayIndex - 1)
            {
                _fallbackResponses.Add(result);

                ReadPropertyRequest req = _originalStackIntegrationRequestBase.Request as ReadPropertyRequest;
                BacnetPropertyID bacnetPropertyID = req.RpRequest.ObjectProperty;
                BacnetDataType bacnetDataType;

                switch (bacnetPropertyID)
                {
                    case BacnetPropertyID.PropObjectList:
                        List<ObjectIdentifierModel> bacnetObjIDList = new List<ObjectIdentifierModel>();

                        foreach (var response in _fallbackResponses)
                        {
                            ReadPropertyResponse readPropertyResponse = response.Response as ReadPropertyResponse;
                            Enum.TryParse<BacnetDataType>(readPropertyResponse.Type, out bacnetDataType);

                            if (bacnetDataType == BacnetDataType.BacnetDTObjectIDArray)
                            {
                                List<ObjectIdentifierModel> objectIdentifierModelList = new List<ObjectIdentifierModel>();
                                objectIdentifierModelList = readPropertyResponse.PropertyValue as List<ObjectIdentifierModel>;

                                if (objectIdentifierModelList.Count > 0)
                                {
                                    ObjectIdentifierModel objectIdentifierModel = objectIdentifierModelList[0];
                                    bacnetObjIDList.Add(objectIdentifierModel);
                                }
                            }
                        }

                        _response = bacnetObjIDList;
                        break;
                    case BacnetPropertyID.PropWeeklySchedule:
                        List<BacnetTimeValueArrayModel> bacnetTimeValueListArray = new List<BacnetTimeValueArrayModel>();

                        foreach (var response in _fallbackResponses)
                        {
                            ReadPropertyResponse readPropertyResponse = response.Response as ReadPropertyResponse;
                            ReadPropertyRequest rpReq = response.Request as ReadPropertyRequest;
                            BacnetTimeValueArrayModel bacnetTimeValueArrayModel = new BacnetTimeValueArrayModel();

                            if (readPropertyResponse != null)
                            {
                                Enum.TryParse<BacnetDataType>(readPropertyResponse.Type, out bacnetDataType);
                                List<BacnetTimeValueArrayModel> bacnetTimeValueArrayModelList = new List<BacnetTimeValueArrayModel>();
                                bacnetTimeValueArrayModelList = readPropertyResponse.PropertyValue as List<BacnetTimeValueArrayModel>;

                                if (bacnetTimeValueArrayModelList.Count > 0)
                                {
                                    if (bacnetDataType == BacnetDataType.BacnetDTDailyScheduleArray)
                                    {
                                        bacnetTimeValueArrayModel = bacnetTimeValueArrayModelList[0];
                                    }
                                }
                            }

                            bacnetTimeValueArrayModel.DayCount = (int)rpReq.RpRequest.ArrayIndex;
                            bacnetTimeValueListArray.Add(bacnetTimeValueArrayModel);
                        }

                        _response = bacnetTimeValueListArray;
                        break;
                    case BacnetPropertyID.PropExceptionSchedule:
                        List<BacnetSpecialEventModel> bacnetSpecialEventModelList = new List<BacnetSpecialEventModel>();

                        foreach (var response in _fallbackResponses)
                        {
                            ReadPropertyResponse readPropertyResponse = response.Response as ReadPropertyResponse;
                            BacnetSpecialEventModel bacnetSpecialEventModel = new BacnetSpecialEventModel();

                            if (readPropertyResponse != null)
                            {
                                Enum.TryParse<BacnetDataType>(readPropertyResponse.Type, out bacnetDataType);
                                List<BacnetSpecialEventModel> bacnetSpecialEventModList = new List<BacnetSpecialEventModel>();
                                bacnetSpecialEventModList = readPropertyResponse.PropertyValue as List<BacnetSpecialEventModel>;

                                if (bacnetSpecialEventModList.Count > 0)
                                {
                                    if (bacnetDataType == BacnetDataType.BacnetDTSpecialEventArray)
                                    {
                                        bacnetSpecialEventModel = bacnetSpecialEventModList[0];
                                    }
                                }
                            }

                            bacnetSpecialEventModelList.Add(bacnetSpecialEventModel);
                        }

                        _response = bacnetSpecialEventModelList;
                        break;
                    default: break;
                }

                ReadPropertyResponse readPropertyResp = result.Response as ReadPropertyResponse;

                _originalRequestResponse = new ReadPropertyResponse();
                _originalRequestResponse.PropertyValue = _response;
                if (readPropertyResp != null)
                    _originalRequestResponse.Type = readPropertyResp.Type;
                _originalStackIntegrationResponseResult.Response = _originalRequestResponse;
                _originalStackIntegrationResponseResult.IsSucceed = true;

                MessageReceiverManager.Instance.ProcessResponse(_originalStackIntegrationResponseResult);
            }
            else
            {
                _fallbackResponses.Add(result);
            }

            _currentResponseCount++;
        }

    }
}
