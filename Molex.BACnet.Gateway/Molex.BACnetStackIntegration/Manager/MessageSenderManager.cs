﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.Manager
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.BACnetStackIntegration.Core;
using Molex.BACnetStackIntegration.Helper;
using Molex.BACnetStackIntegration.Log;
using Molex.BACnetStackIntegration.ManagedWrapper.Helper;
using Molex.BACnetStackIntegration.Model.Request;
using Molex.BACnetStackIntegration.Model.Response;
using Molex.BACnetStackIntegration.Processor;
using Molex.BACnetStackIntegration.Repository;
using Molex.BACnetStackIntegration.Stack.Wrapper;
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Request;
using Molex.StackDataObjects.Response;
using Molex.StackDataObjects.StackInititializationModels;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Molex.BACnetStackIntegration.Manager
{
    internal class MessageSenderManager : SingletonQueueBase<MessageSenderManager, StackIntegrationRequestBase>
    {
        private MessageSenderManager()
        {
            base.ThreadName = "MessageSenderManagerThread";
        }

        #region Global Parameters
        static App_AutoResp_Interface_t _autoCallback;
        static App_AutoResp_Interface_t _covCallback;
        static App_AutoResp_Interface_t _iAmCallback;
        static App_AutoResp_Interface_t _writePropertyCallback;
        static App_AutoResp_Interface_t _createObjectCallback;
        static App_AutoResp_Interface_t _deleteObjectCallback;
        static App_AutoResp_Interface_t _writePropertyMultipleCallback;
        static App_AutoResp_Interface_t _reinitializeDevice;

        static App_AutoResp_Interface_t _timeSync;
        static App_AutoResp_Interface_t _utctimeSync;
        static App_AutoResp_Interface_t _dcc;
        static App_AutoResp_Interface_t _addListElement;
        static App_AutoResp_Interface_t _removeListElement;

        static App_Interface_t _listElementInternalCallBack;
        #endregion

        #region Public Properties

        public bool IsServerOverLoaded { get; set; }

        public bool IsStackInit
        {
            get;
            private set;
        }

        public bool IsStackPortOpened
        {
            get;
            set;
        }

        #endregion Public Properties

        #region Internal Methods

        internal void SendData(StackIntegrationRequestBase request)
        {
            base.Enque(request);
        }

        internal ResponseResult CallSyncApi(StackIntegrationRequestBase request)
        {
            ResponseResult result = null;

            switch (request.Request.ReqType)
            {
                case StackConstants.RequestType.BACDEL_BBMD_REQ_WRITE_BDT:
                    result = new ResponseResult();
                    WriteBDTRequest writeBDTRequest = (WriteBDTRequest)request.Request;
                    BacnetReturnType writeBDTListResult = StackAPIHelper.BACDELWriteBDTEntry(writeBDTRequest.BDTEntries);
                    if (writeBDTListResult == BacnetReturnType.BacdelSuccess)
                        result.IsSucceed = true;
                    else
                    {
                        result.IsSucceed = false;
                        result.ErrorModel = new ErrorResponseModel();
                        result.ErrorModel.Type = ErrorType.StackError;
                        result.ErrorModel.ErrorCode = (int)writeBDTListResult;
                    }
                    break;
                case StackConstants.RequestType.BACDEL_Get_Dynamic_Device_Address_Binding:
                    result = new ResponseResult();
                    result = StackAPIHelper.BACDELSendGetAddressBinding();
                    break;
                case StackConstants.RequestType.BACDEL_Stack_Init:
                    result = new ResponseResult();
                    StackInitRequest stackInitRequest = request.Request as StackInitRequest;
                    BacnetReturnType stackInitResult = StackInit(stackInitRequest.StackInitConfiguration);

                    if (stackInitResult == BacnetReturnType.BacdelSuccess)
                        result.IsSucceed = true;
                    else
                    {
                        result.IsSucceed = false;
                        result.ErrorModel = new ErrorResponseModel();
                        result.ErrorModel.Type = ErrorType.StackError;
                        result.ErrorModel.ErrorCode = (int)stackInitResult;
                    }
                    break;
                case StackConstants.RequestType.BACDEL_Stack_Deinit:
                    result = new ResponseResult();
                    BacnetReturnType bacnetReturnType = StackAPIHelper.BACDELSendStackDeInit();
                    if (bacnetReturnType == BacnetReturnType.BacdelSuccess)
                    {
                        result.IsSucceed = true;
                        IsStackInit = false;
                    }
                    else
                    {
                        result.IsSucceed = false;
                        result.ErrorModel = new ErrorResponseModel();
                        result.ErrorModel.Type = ErrorType.StackError;
                        result.ErrorModel.ErrorCode = (int)bacnetReturnType;
                    }
                    break;
                case StackConstants.RequestType.BACDEL_Delete_Dynamic_Device_Address_Binding:
                    result = new ResponseResult();
                    DeleteAddressBindingRequest deleteAddressBindingRequest = (DeleteAddressBindingRequest)request.Request;

                    BacnetReturnType bacnetReturnTypeDelAddrBinding = StackAPIHelper.BACDELSendDeleteAddressBinding(deleteAddressBindingRequest.DeviceId, deleteAddressBindingRequest.DestinationAddress);

                    if (bacnetReturnTypeDelAddrBinding == BacnetReturnType.BacdelSuccess)
                        result.IsSucceed = true;
                    else
                    {
                        result.IsSucceed = false;
                        result.ErrorModel = new ErrorResponseModel();
                        result.ErrorModel.Type = ErrorType.StackError;
                        result.ErrorModel.ErrorCode = (int)bacnetReturnTypeDelAddrBinding;
                    }
                    break;
                case StackConstants.RequestType.BACDEL_Enable_BBMD:
                    result = new ResponseResult();
                    BacnetReturnType enbleBBMDResult = StackAPIHelper.BACDELSendEnableBBMDRequest();

                    if (enbleBBMDResult == BacnetReturnType.BacdelSuccess)
                        result.IsSucceed = true;
                    else
                    {
                        result.IsSucceed = false;
                        result.ErrorModel = new ErrorResponseModel();
                        result.ErrorModel.Type = ErrorType.StackError;
                        result.ErrorModel.ErrorCode = (int)enbleBBMDResult;
                    }
                    break;
                case StackConstants.RequestType.BACDEL_Disable_BBMD:
                    result = new ResponseResult();
                    BacnetReturnType disableBBMDResult = StackAPIHelper.BACDELSendDisableBBMDRequest();

                    if (disableBBMDResult == BacnetReturnType.BacdelSuccess)
                        result.IsSucceed = true;
                    else
                    {
                        result.IsSucceed = false;
                        result.ErrorModel = new ErrorResponseModel();
                        result.ErrorModel.Type = ErrorType.StackError;
                        result.ErrorModel.ErrorCode = (int)disableBBMDResult;
                    }
                    break;
                case StackConstants.RequestType.BACDEL_Change_IpAddress_PortNo:
                    result = new ResponseResult();
                    ChangeIpAddressPortNoRequest changeIpAddressPortNoRequest = (ChangeIpAddressPortNoRequest)request.Request;

                    BacnetReturnType changeIpAddressPortNoResult = StackAPIHelper.BACDELSendChangeIpAddressPortNoRequest(changeIpAddressPortNoRequest.Port, changeIpAddressPortNoRequest.IPAddress);

                    if (changeIpAddressPortNoResult == BacnetReturnType.BacdelSuccess)
                        result.IsSucceed = true;
                    else
                    {
                        result.IsSucceed = false;
                        result.ErrorModel = new ErrorResponseModel();
                        result.ErrorModel.Type = ErrorType.StackError;
                        result.ErrorModel.ErrorCode = (int)changeIpAddressPortNoResult;
                    }
                    break;
                case StackConstants.RequestType.BACDEL_Set_Device_Apdu_Retries:
                    result = new ResponseResult();
                    SetDeviceApduRetriesRequest setDeviceApduRetriesRequest = (SetDeviceApduRetriesRequest)request.Request;

                    BacnetReturnType setDeviceApduRetriesResult = StackAPIHelper.BACDELSendSetDeviceApduRetriesRequest(setDeviceApduRetriesRequest.DeviceId, setDeviceApduRetriesRequest.ApduRetries, setDeviceApduRetriesRequest.IsAllDevice);

                    if (setDeviceApduRetriesResult == BacnetReturnType.BacdelSuccess)
                        result.IsSucceed = true;
                    else
                    {
                        result.IsSucceed = false;
                        result.ErrorModel = new ErrorResponseModel();
                        result.ErrorModel.Type = ErrorType.StackError;
                        result.ErrorModel.ErrorCode = (int)setDeviceApduRetriesResult;
                    }
                    break;
                case StackConstants.RequestType.BACDEL_Set_Device_Apdu_Timeout:
                    result = new ResponseResult();
                    SetDeviceApduTimeoutRequest setDeviceApduTimeoutRequest = (SetDeviceApduTimeoutRequest)request.Request;
                    BacnetReturnType setDeviceApduTimeoutResult = StackAPIHelper.BACDELSendSetDeviceApduTimeoutRequest(setDeviceApduTimeoutRequest.DeviceId, setDeviceApduTimeoutRequest.ApduTimeout, setDeviceApduTimeoutRequest.IsAllDevice);

                    if (setDeviceApduTimeoutResult == BacnetReturnType.BacdelSuccess)
                        result.IsSucceed = true;
                    else
                    {
                        result.IsSucceed = false;
                        result.ErrorModel = new ErrorResponseModel();
                        result.ErrorModel.Type = ErrorType.StackError;
                        result.ErrorModel.ErrorCode = (int)setDeviceApduTimeoutResult;
                    }
                    break;
                case StackConstants.RequestType.BACDEL_Change_Device_Id:
                    result = new ResponseResult();
                    ChangeDeviceIdRequest changeDeviceIdRequest = (ChangeDeviceIdRequest)request.Request;

                    BacnetReturnType changeDeviceIdResult = StackAPIHelper.BacdelSendChangeDeviceIdRequest(changeDeviceIdRequest.OldDeviceId, changeDeviceIdRequest.NewDeviceId, changeDeviceIdRequest.NewDADR);
                    if (changeDeviceIdResult == BacnetReturnType.BacdelSuccess)
                        result.IsSucceed = true;
                    else
                    {
                        result.IsSucceed = false;
                        result.ErrorModel = new ErrorResponseModel();
                        result.ErrorModel.Type = ErrorType.StackError;
                        result.ErrorModel.ErrorCode = (int)changeDeviceIdResult;
                    }
                    break;
                case StackConstants.RequestType.BACDEL_BBMD_READ_REG_FD_LIST:
                    result = new ResponseResult();
                    result = StackAPIHelper.BacdelSendReadRegisterFDListRequest();
                    break;
                case StackConstants.RequestType.BACDEL_Get_BBMD_Status:
                    result = new ResponseResult();
                    bool getBBMDStatusResult = StackAPIHelper.BACDELSendGetBBMDStatusRequest();

                    result.IsSucceed = true;
                    GetBBMDStatusResponse getBBMDStatusResponse = new GetBBMDStatusResponse();
                    getBBMDStatusResponse.IsBBMDEnabled = getBBMDStatusResult;
                    result.Response = getBBMDStatusResponse;
                    break;
                case StackConstants.RequestType.BACDEL_Get_FD_Status:
                    result = new ResponseResult();
                    bool getFDStatusResult = StackAPIHelper.BACDELSendGetFDStatusRequest();

                    result.IsSucceed = true;
                    GetFDStatusResponse getFDStatusResponse = new GetFDStatusResponse();
                    getFDStatusResponse.IsFDEnabled = getFDStatusResult;
                    result.Response = getFDStatusResponse;
                    break;
                case StackConstants.RequestType.BACDEL_Clear_Device_Objects:
                    result = new ResponseResult();
                    BacnetReturnType clearDeviceObjectsResult = StackAPIHelper.BACDELClearDeviceObject();

                    if (clearDeviceObjectsResult == BacnetReturnType.BacdelSuccess)
                        result.IsSucceed = true;
                    else
                    {
                        result.IsSucceed = false;
                        result.ErrorModel = new ErrorResponseModel();
                        result.ErrorModel.Type = ErrorType.StackError;
                        result.ErrorModel.ErrorCode = (int)clearDeviceObjectsResult;
                    }
                    break;
                case StackConstants.RequestType.BACDEL_Get_PresentValue_From_BACapp:
                    result = new ResponseResult();

                    GetPresentValueFromBACAppRequest getPresentValueFromBACAppRequest = (GetPresentValueFromBACAppRequest)request.Request;
                    BacnetReturnType getPresentValueFromBACappResult = StackAPIHelper.BacdelSetPresentvalueForExternalFunctionality(getPresentValueFromBACAppRequest.SourceDeviceId, getPresentValueFromBACAppRequest.PVTriggerModel, getPresentValueFromBACAppRequest.BacnetValueModel);

                    if (getPresentValueFromBACappResult == BacnetReturnType.BacdelSuccess)
                        result.IsSucceed = true;
                    else
                    {
                        result.IsSucceed = false;
                        result.ErrorModel = new ErrorResponseModel();
                        result.ErrorModel.Type = ErrorType.StackError;
                        result.ErrorModel.ErrorCode = (int)getPresentValueFromBACappResult;
                    }
                    break;
                case StackConstants.RequestType.BACDEL_Add_Object:
                    result = new ResponseResult();

                    AddObjectRequest addObjectRequest = (AddObjectRequest)request.Request;
                    BacnetReturnType addObjectRequestResult = StackAPIHelper.BacdelAddObject(addObjectRequest.SourceDeviceId, addObjectRequest.ObjectType, addObjectRequest.ObjectID, addObjectRequest.ObjectName);

                    if (addObjectRequestResult == BacnetReturnType.BacdelSuccess)
                        result.IsSucceed = true;
                    else
                    {
                        result.IsSucceed = false;
                        result.ErrorModel = new ErrorResponseModel();
                        result.ErrorModel.Type = ErrorType.StackError;
                        result.ErrorModel.ErrorCode = (int)addObjectRequestResult;
                    }
                    break;
                case StackConstants.RequestType.BACDEL_Delete_Object:
                    result = new ResponseResult();

                    RemoveObjectRequest removeObjectRequest = (RemoveObjectRequest)request.Request;
                    BacnetReturnType removeObjectRequestResult = StackAPIHelper.BacdelRemoveObject(removeObjectRequest.SourceDeviceId, removeObjectRequest.ObjectType, removeObjectRequest.ObjectID);

                    if (removeObjectRequestResult == BacnetReturnType.BacdelSuccess)
                        result.IsSucceed = true;
                    else
                    {
                        result.IsSucceed = false;
                        result.ErrorModel = new ErrorResponseModel();
                        result.ErrorModel.Type = ErrorType.StackError;
                        result.ErrorModel.ErrorCode = (int)removeObjectRequestResult;
                    }
                    break;
                case StackConstants.RequestType.BACDEL_Get_Object_Property:
                    result = new ResponseResult();

                    GetObjectPropertyValueRequest getObjectPropertyValueRequest = (GetObjectPropertyValueRequest)request.Request;
                    result = StackAPIHelper.BacdelGetObjectPropertyValue(getObjectPropertyValueRequest.SourceDeviceId, getObjectPropertyValueRequest.ObjectID, getObjectPropertyValueRequest.ObjectType, getObjectPropertyValueRequest.Property, getObjectPropertyValueRequest.ArrayIndex, getObjectPropertyValueRequest.IsArrayIndexPresent);
                    break;
                case StackConstants.RequestType.BACDEL_Add_Device:
                    result = new ResponseResult();

                    AddDeviceRequest addDeviceRequest = (AddDeviceRequest)request.Request;
                    BacdelDeviceModel device = new BacdelDeviceModel();
                    device.DAdr = addDeviceRequest.DeviceAdr;
                    device.DeviceID = addDeviceRequest.DeviceID;
                    device.SNet = addDeviceRequest.SNet;
                    BacnetReturnType addDeviceRequestResult = StackAPIHelper.BACDELAddDevice(device);

                    if (addDeviceRequestResult == BacnetReturnType.BacdelSuccess)
                        result.IsSucceed = true;
                    else
                    {
                        result.IsSucceed = false;
                        result.ErrorModel = new ErrorResponseModel();
                        result.ErrorModel.Type = ErrorType.StackError;
                        result.ErrorModel.ErrorCode = (int)addDeviceRequestResult;
                    }
                    break;
                case StackConstants.RequestType.BACDEL_Set_Object_Property:
                    result = new ResponseResult();

                    SetObjectPropertyValueRequest setObjectPropertyValueRequest = (SetObjectPropertyValueRequest)request.Request;
                    result = StackAPIHelper.BacdelSetObjectPropertyValue(setObjectPropertyValueRequest.SourceDeviceId, setObjectPropertyValueRequest.ObjectID, setObjectPropertyValueRequest.ObjectType, setObjectPropertyValueRequest.ObjectProperty, setObjectPropertyValueRequest.ArrayIndex, setObjectPropertyValueRequest.PropertyValue, setObjectPropertyValueRequest.Priority, setObjectPropertyValueRequest.IsArrayIndexPresent, setObjectPropertyValueRequest.DataType);

                    int logDeviceId = Logger.Instance.GetLogFlag(1);
                    if (logDeviceId != -1 && logDeviceId == (int)(setObjectPropertyValueRequest.SourceDeviceId))
                    {
                        Logger.Instance.Log(string.Format("WriteProperty => Res: '{0}' agnst SrcDevId: '{1}', ObjId: '{2}', ObjTyp: '{3}', Prop: '{4}', Val: '{5}', DT: '{6}' ",
                                                                               result.IsSucceed,
                                                                               setObjectPropertyValueRequest.SourceDeviceId,
                                                                               setObjectPropertyValueRequest.ObjectID,
                                                                               setObjectPropertyValueRequest.ObjectType,
                                                                               setObjectPropertyValueRequest.ObjectProperty,
                                                                               setObjectPropertyValueRequest.PropertyValue,
                                                                               setObjectPropertyValueRequest.DataType));
                        if (!result.IsSucceed)
                        {
                            Logger.Instance.Log(string.Format("WriteProperty => Err: '{0}' agnst SrcDevId: '{1}', ObjId: '{2}', ObjTyp: '{3}', Prop: '{4}'",
                                                                               result.ErrorModel.ToString(),
                                                                               setObjectPropertyValueRequest.SourceDeviceId,
                                                                               setObjectPropertyValueRequest.ObjectID,
                                                                               setObjectPropertyValueRequest.ObjectType,
                                                                               setObjectPropertyValueRequest.ObjectProperty));
                        }
                    }
                    break;
                case StackConstants.RequestType.BACDEL_Update_COVA_Subscription_LifeTime:
                    result = new ResponseResult();

                    UpdateCovASubscriptionLifeTimeRequest updateCovASubscriptionLifeTimeRequest = (UpdateCovASubscriptionLifeTimeRequest)request.Request;
                    BacnetReturnType updateCovASubscriptionLifeTimeRequestResult = StackAPIHelper.BacdelUpdateCovASubscriptionLifeTime(updateCovASubscriptionLifeTimeRequest.LifeTime);

                    if (updateCovASubscriptionLifeTimeRequestResult == BacnetReturnType.BacdelSuccess)
                        result.IsSucceed = true;
                    else
                    {
                        result.IsSucceed = false;
                        result.ErrorModel = new ErrorResponseModel();
                        result.ErrorModel.Type = ErrorType.StackError;
                        result.ErrorModel.ErrorCode = (int)updateCovASubscriptionLifeTimeRequestResult;
                    }
                    break;
                case StackConstants.RequestType.BACDEL_Set_Vendor_Specific_data:
                    result = new ResponseResult();

                    SetVendorSpecificDataRequest setVendorSpecificDataRequest = request.Request as SetVendorSpecificDataRequest;
                    StackAPIHelper.SetVendorSpecificdata(setVendorSpecificDataRequest.VendorData);

                    result.IsSucceed = true;
                    break;
                case StackConstants.RequestType.BACDEL_Set_Device_Password:
                    result = new ResponseResult();

                    SetDevicePasswordRequest setDevicePasswordRequest = request.Request as SetDevicePasswordRequest;
                    StackAPIHelper.BACDELSetDevicePassword(setDevicePasswordRequest.DeviceId, setDevicePasswordRequest.BACnetPassword);

                    result.IsSucceed = true;
                    break;
                case StackConstants.RequestType.BACDEL_Create_Directory:
                    result = new ResponseResult();
                    BacnetReturnType createDirectoryResult = StackAPIHelper.BACDELCreateDirectory();

                    if (createDirectoryResult == BacnetReturnType.BacdelSuccess)
                        result.IsSucceed = true;
                    else
                    {
                        result.IsSucceed = false;
                        result.ErrorModel = new ErrorResponseModel();
                        result.ErrorModel.Type = ErrorType.StackError;
                        result.ErrorModel.ErrorCode = (int)createDirectoryResult;
                    }
                    break;
                case StackConstants.RequestType.BACDEL_Set_Max_Limits:
                    result = new ResponseResult();

                    SetMaxLimitsRequest setMaxLimitsRequest = request.Request as SetMaxLimitsRequest;
                    BacnetReturnType setMaxLimitResult = StackAPIHelper.BACDELSetMaxLimits(setMaxLimitsRequest.BACAppMaxLimits, setMaxLimitsRequest.FirstFailedLimit, setMaxLimitsRequest.LimitType, setMaxLimitsRequest.SetSpecifiedLimit);

                    if (setMaxLimitResult == BacnetReturnType.BacdelSuccess)
                        result.IsSucceed = true;
                    else
                    {
                        result.IsSucceed = false;
                        result.ErrorModel = new ErrorResponseModel();
                        result.ErrorModel.Type = ErrorType.StackError;
                        result.ErrorModel.ErrorCode = (int)setMaxLimitResult;
                    }
                    break;
                case StackConstants.RequestType.BACDEL_Set_Get_Property_AccessType:
                    SetGetPropertyAccessType setGetPropertyAccessType = request.Request as SetGetPropertyAccessType;
                    StackAPIHelper.BACDELSetGetPropertyAccessType(setGetPropertyAccessType.DeviceId, setGetPropertyAccessType.ObjectId, setGetPropertyAccessType.ObjectType, setGetPropertyAccessType.PropertyID, setGetPropertyAccessType.AccessType, setGetPropertyAccessType.ReadOrWrite);
                    break;
                default:
                    result = new ResponseResult();
                    break;
            }

            return result;
        }

        internal BacnetReturnType StackInit(StackConfigModel stackInitConfig)
        {
            if (IsStackInit)
                return BacnetReturnType.BacdelStackAlreadyInitialized;

            IsStackInit = false;
            BacnetReturnType bacdelResult = StackAPIHelper.BACDELStackInit(stackInitConfig);

            if (bacdelResult == BacnetReturnType.BacdelSuccess)
            {
                StackRepository.Instance.StackBasicInfo.IPAddress = stackInitConfig.IpAddress;
                StackRepository.Instance.StackBasicInfo.Port = stackInitConfig.Port;
                IsStackInit = true;
            }

            return bacdelResult;
        }

        internal ResponseResult RegisterCallbacks(Dictionary<BacAppCallBackFunChoice, List<CallBackModel>> callbackmodel)
        {
            ResponseResult result = new ResponseResult();
            foreach (BacAppCallBackFunChoice CallBackType in callbackmodel.Keys)
            {
                switch (CallBackType)
                {
                    case BacAppCallBackFunChoice.AppCbEventNotification:
                        _autoCallback = AutoResponseProcessor.ProcessAutoResponse;
                        StackAPIHelper.RegisterAutoCallBack(BacAppCallBackFunChoice.AppCbEventNotification, _autoCallback);
                        break;
                    case BacAppCallBackFunChoice.AppCbCOVNotification:
                        _covCallback = CovNotificationProcessor.ProcessCovNotification;
                        StackAPIHelper.RegisterAutoCallBack(BacAppCallBackFunChoice.AppCbCOVNotification, _covCallback);
                        break;
                    case BacAppCallBackFunChoice.AppCbIAm:
                        _iAmCallback = IAmResponseProcessor.ProcessIAmResponse;
                        StackAPIHelper.RegisterAutoCallBack(BacAppCallBackFunChoice.AppCbIAm, _iAmCallback);
                        break;
                    case BacAppCallBackFunChoice.AppCbWriteProperty:
                        _writePropertyCallback = WritePropertyCallbackProcessor.WritePropertyCallback;
                        StackAPIHelper.RegisterAutoCallBack(BacAppCallBackFunChoice.AppCbWriteProperty, _writePropertyCallback);

                        List<CallBackModel> callbackList = callbackmodel[CallBackType];

                        foreach (var type in callbackList)
                        {
                            foreach (var property in type.PropertyIds)
                            {
                                IntPtr ptr = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(uint)));
                                uint intType = (uint)BacnetCallbackConfigType.CallbackConfigRequired;
                                ptr = StructureConverter.StructureToPtr<uint>(intType);
                                StackAPIHelper.RegisterSetGetPropertyWPCallback(type.ObjectType, property, ptr, true);
                            }
                        }
                        break;
                    case BacAppCallBackFunChoice.AppCbCreateObject:
                        _createObjectCallback = CreateObjectCallbackProcessor.CreateObjectCallback;
                        StackAPIHelper.RegisterAutoCallBack(BacAppCallBackFunChoice.AppCbCreateObject, _createObjectCallback);
                        break;
                    case BacAppCallBackFunChoice.AppCbDeleteObject:
                        _deleteObjectCallback = DeleteObjectCallbackProcessor.DeleteObjectCallback;
                        StackAPIHelper.RegisterAutoCallBack(BacAppCallBackFunChoice.AppCbDeleteObject, _deleteObjectCallback);
                        break;
                    case BacAppCallBackFunChoice.AppCbReinitializeDevice:
                        _reinitializeDevice = ReInitDeviceCallbackProcessor.ProcessReInitDevice;
                        StackAPIHelper.RegisterAutoCallBack(BacAppCallBackFunChoice.AppCbReinitializeDevice, _reinitializeDevice);
                        break;

                    case BacAppCallBackFunChoice.AppCbTimeSync:
                        _timeSync = TimeSyncCallbackProcessor.ProcessTimeSync;
                        StackAPIHelper.RegisterAutoCallBack(BacAppCallBackFunChoice.AppCbTimeSync, _timeSync);
                        break;
                    case BacAppCallBackFunChoice.AppCbUtcTimeSync:
                        _utctimeSync = UTCTimeSyncCallbackProcessor.ProcessUTCTimeSync;
                        StackAPIHelper.RegisterAutoCallBack(BacAppCallBackFunChoice.AppCbUtcTimeSync, _utctimeSync);
                        break;
                    case BacAppCallBackFunChoice.AppCbDeviceCommunicationControl:
                        _dcc = DeviceCommunicationControlCallbackProcessor.ProcessDeviceCommunicationControl;
                        StackAPIHelper.RegisterAutoCallBack(BacAppCallBackFunChoice.AppCbDeviceCommunicationControl, _dcc);
                        break;
                    case BacAppCallBackFunChoice.AppCbAddListElement:
                        _addListElement = AddListElementProcessor.AddListElementCallback;
                        StackAPIHelper.RegisterAutoCallBack(BacAppCallBackFunChoice.AppCbAddListElement, _addListElement);
                        break;
                    case BacAppCallBackFunChoice.AppCbRemoveListElement:
                        _removeListElement = RemoveListElementProcessor.RemoveListElementCallback;
                        StackAPIHelper.RegisterAutoCallBack(BacAppCallBackFunChoice.AppCbRemoveListElement, _removeListElement);
                        break;
                    default:
                        break;
                }
            }


            result.IsSucceed = true;
            return result;
        }

        internal void RegisterInternalCallBack()
        {
            _listElementInternalCallBack = ListElementCallbackProcessor.ListElementInternalCallback;
            StackAPIHelper.RegisterAppLayerCallback(_listElementInternalCallBack);
        }

        internal void RegisterPacketDelay(uint delayTime)
        {
            StackAPIHelper.RegisterPacketDelay(delayTime);
        }

        internal BacnetReturnType SetVirtualNetworkNumber(uint virtualNetworkNumber)
        {
            BacnetReturnType bacdelResult = StackAPIHelper.SetVirtualNetworkNumber(virtualNetworkNumber);
            return bacdelResult;
        }

        internal BacnetReturnType SetLocalNetworkNumber(uint localNetworkNumber)
        {
            BacnetReturnType bacdelResult = StackAPIHelper.SetLocalNetworkNumber(localNetworkNumber);
            return bacdelResult;
        }

        internal ResponseResult GetBDTList()
        {
            ResponseResult bacdelResult = StackAPIHelper.GetBDTList();
            return bacdelResult;
        }

        internal ResponseResult GetFDTList()
        {
            ResponseResult bacdelResult = StackAPIHelper.GetFDTList();
            return bacdelResult;
        }

        #endregion Internal Methods

        #region Overriden Methods

        protected override void Process(StackIntegrationRequestBase req)
        {
            bool isRequestSendSuccess;

            try
            {
                do
                {
                    int packetId = 0;

                    BacnetReturnType bacnetReturnType = StackAPIHelper.ExecuteStackApi(req, out packetId);

                    if (packetId > 0)
                    {
                        RequestManager.Instance.AddRequestId(req, packetId);
                    }

                    if (bacnetReturnType == BacnetReturnType.BacdelMallocError ||
                        bacnetReturnType == BacnetReturnType.BacdelSemaphoreError ||
                        bacnetReturnType == BacnetReturnType.BacdelInitiatorQueueFull)
                    {
                        isRequestSendSuccess = false;
                        IsServerOverLoaded = true;
                        Logger.Instance.Log(new LogData
                        {
                            MoreInfo = "IL request failed. Code : " + bacnetReturnType + ". Packet Id : " + req.Request.PacketId
                        });
                    }
                    else if (bacnetReturnType == BacnetReturnType.BacdelSuccess || bacnetReturnType == BacnetReturnType.BacdelInitiatorSuccess)
                    {
                        isRequestSendSuccess = true;
                        IsServerOverLoaded = false;
                    }
                    else
                    {
                        isRequestSendSuccess = true;
                        IsServerOverLoaded = false;

                        StackIntegrationResponseResult stackIntegrationResponseResult = new StackIntegrationResponseResult();
                        stackIntegrationResponseResult.TokenId = packetId;
                        stackIntegrationResponseResult.IsSucceed = false;
                        stackIntegrationResponseResult.ErrorModel = new ErrorResponseModel();
                        stackIntegrationResponseResult.ErrorModel.Type = ErrorType.StackError;
                        stackIntegrationResponseResult.ErrorModel.ErrorCode = (int)bacnetReturnType;

                        MessageReceiverManager.Instance.ProcessResponse(stackIntegrationResponseResult);
                    }
                }
                while (!isRequestSendSuccess);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex, "IL Error: Error occured in MessageSenderManager while processing request.");
            }
        }

        #endregion
    }
}