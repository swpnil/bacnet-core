﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.Manager
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.BACnetStackIntegration.Model.Request;
using Molex.BACnetStackIntegration.Model.Response;
using Molex.StackDataObjects.APIModels;
using Molex.StackDataObjects.Constants;
using Molex.StackDataObjects.Request;
using Molex.StackDataObjects.Response;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Molex.BACnetStackIntegration.Manager
{
    internal class RPMFallbackManager
    {
        int _totalRequestCount;
        StackIntegrationResponseResult _originalStackIntegrationResponseResult;
        ReadPropertyMultipleResponse _originalRequestResponse;
        List<ResponseResult> _fallbackResponses;
        int _currentResponseCount = 0;
        List<ReadPropertyMultipleDataModel> _response = null;

        internal RPMFallbackManager()
        {
            _fallbackResponses = new List<ResponseResult>();
            _response = new List<ReadPropertyMultipleDataModel>();
        }

        internal void HandleRPMFallback(StackIntegrationResponseResult result)
        {
            _originalStackIntegrationResponseResult = result;
            ReadPropertyMultipleRequest rpmReq = result.Request.Request as ReadPropertyMultipleRequest;
            _totalRequestCount = rpmReq.RpmRequest.Count;

            foreach (var request in rpmReq.RpmRequest)
            {
                ReadPropertyRequest rpRequest = new ReadPropertyRequest();
                rpRequest.SourceDeviceId = rpmReq.SourceDeviceId;
                rpRequest.DestinationAddress = rpmReq.DestinationAddress;
                rpRequest.DestinationDeviceId = rpmReq.DestinationDeviceId;
                rpRequest.DestinationTypeFlag = rpmReq.DestinationTypeFlag;
                rpRequest.RpRequest = new ReadPropertyRequestModel();
                rpRequest.RpRequest.IsArrayIndexPresent = request.ArrayIndexPresent;
                rpRequest.RpRequest.ArrayIndex = request.ArrayIndex;
                rpRequest.RpRequest.ObjectInstanceNumber = request.ObjectInstance;
                rpRequest.RpRequest.ObjectProperty = request.ObjectProperty;
                rpRequest.RpRequest.ObjectType = request.ObjectType;
                rpRequest.ActionCompletedHandler = RPResponseHandler;

                StackIntegrationRequestBase stackIntegrationRequestBase = new StackIntegrationRequestBase();
                stackIntegrationRequestBase.SyncHandler = new AsyncToSyncHandler(500);
                stackIntegrationRequestBase.Request = rpRequest;

                MessageSenderManager.Instance.SendData(stackIntegrationRequestBase);
                stackIntegrationRequestBase.SyncHandler.Sync();
            }
        }

        void RPResponseHandler(ResponseResult result)
        {
            if (_currentResponseCount == _totalRequestCount - 1)
            {
                _fallbackResponses.Add(result);

                foreach (var response in _fallbackResponses)
                {
                    ReadPropertyResponse readPropertyResponse = response.Response as ReadPropertyResponse;
                    ReadPropertyRequestModel requestModel = (response.Request as ReadPropertyRequest).RpRequest;

                    ReadPropertyMultipleDataModel readPropertyMultipleDataModel = new ReadPropertyMultipleDataModel();
                    readPropertyMultipleDataModel.ReadPropertyRequest = new ReadPropertyMultipleRequestModel();

                    readPropertyMultipleDataModel.ReadPropertyRequest.ArrayIndexPresent = requestModel.IsArrayIndexPresent;
                    readPropertyMultipleDataModel.ReadPropertyRequest.ArrayIndex = requestModel.ArrayIndex;
                    readPropertyMultipleDataModel.ReadPropertyRequest.ObjectInstance = requestModel.ObjectInstanceNumber;
                    readPropertyMultipleDataModel.ReadPropertyRequest.ObjectProperty = requestModel.ObjectProperty;
                    readPropertyMultipleDataModel.ReadPropertyRequest.ObjectType = requestModel.ObjectType;

                    if (!response.IsSucceed)
                    {
                        if (response.ErrorModel != null)
                        {
                            readPropertyMultipleDataModel.IsSucceed = false;
                            readPropertyMultipleDataModel.ErrorClass = (BacnetErrorClass)response.ErrorModel.ErrorClass;
                            readPropertyMultipleDataModel.ErrorCode = (BacnetErrorCode)response.ErrorModel.ErrorCode;
                        }
                    }
                    else
                    {
                        if (readPropertyResponse != null)
                        {
                            readPropertyMultipleDataModel.PropertyValue = readPropertyResponse.PropertyValue;
                            readPropertyMultipleDataModel.Type = readPropertyResponse.Type;
                        }

                        readPropertyMultipleDataModel.IsSucceed = true;
                    }

                    _response.Add(readPropertyMultipleDataModel);
                }

                _originalRequestResponse = new ReadPropertyMultipleResponse();
                _originalRequestResponse.ReadPropertyMultipleResponseData = _response;
                _originalStackIntegrationResponseResult.Response = _originalRequestResponse;
                _originalStackIntegrationResponseResult.IsSucceed = true;

                MessageReceiverManager.Instance.ProcessResponse(_originalStackIntegrationResponseResult);
            }
            else
            {
                _fallbackResponses.Add(result);
            }

            _currentResponseCount++;
        }
    }
}
