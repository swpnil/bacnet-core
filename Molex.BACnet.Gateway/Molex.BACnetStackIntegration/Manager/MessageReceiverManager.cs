﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.Manager
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.BACnetStackIntegration.Log;
using Molex.StackDataObjects.Response;
using System.Threading;
using Molex.BACnetStackIntegration.Core;
using Molex.BACnetStackIntegration.Model.Response;
using Molex.BACnetStackIntegration.ManagedWrapper.Helper;

namespace Molex.BACnetStackIntegration.Manager
{
    internal class MessageReceiverManager : SingletonQueueBase<MessageReceiverManager, StackIntegrationResponseResult>
    {
        private MessageReceiverManager()
        {
            base.ThreadName = "MessageReceiverManagerThread";
        }

        public void ProcessResponse(StackIntegrationResponseResult result)
        {
            base.Enque(result);
        }

        protected override void Process(StackIntegrationResponseResult result)
        {
            ThreadPool.QueueUserWorkItem(
           execute =>
           {
               StackIntegrationResponseResult stackIntegrationResponseResult = result as StackIntegrationResponseResult;
               stackIntegrationResponseResult.Request = RequestManager.Instance.GetRequest(stackIntegrationResponseResult.TokenId);

               LazyPointerManager.Instance.FreeMemory(stackIntegrationResponseResult.TokenId);

               ResponseResult responseResult = new ResponseResult();
               responseResult.ErrorModel = stackIntegrationResponseResult.ErrorModel;
               responseResult.IsSucceed = stackIntegrationResponseResult.IsSucceed;
               responseResult.PacketReceivedTime = stackIntegrationResponseResult.PacketReceivedTime;
               responseResult.ReqType = stackIntegrationResponseResult.ReqType;
               if (stackIntegrationResponseResult.Request != null)
                   responseResult.Request = stackIntegrationResponseResult.Request.Request;

               responseResult.Response = stackIntegrationResponseResult.Response;
               responseResult.StopProcessing = stackIntegrationResponseResult.StopProcessing;
               responseResult.TokenId = stackIntegrationResponseResult.TokenId;

               if (stackIntegrationResponseResult.Request != null)
               {
                   CallbackManager.Instance.RemoveFromCallbackDictionary(stackIntegrationResponseResult.Request.Request);
               }

               if (stackIntegrationResponseResult.StopProcessing == false &&
                   stackIntegrationResponseResult.Request != null &&
                   stackIntegrationResponseResult.Request.Request.ActionCompletedHandler != null)
                   stackIntegrationResponseResult.Request.Request.ActionCompletedHandler(responseResult);

               if (stackIntegrationResponseResult.Request != null &&
                   stackIntegrationResponseResult.Request.SyncHandler != null)
                   stackIntegrationResponseResult.Request.SyncHandler.Complete();

               RequestManager.Instance.RequestCompleted(stackIntegrationResponseResult.TokenId);
           }, result);

        }
    }
}
