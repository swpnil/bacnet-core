﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.Manager
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Molex.BACnetStackIntegration.Core;

namespace Molex.BACnetStackIntegration.Manager
{
    internal class LazyPointerManager : SingletonQueueBase<LazyPointerManager, object>
    {
        private LazyPointerManager() { }

        Dictionary<int, Dictionary<IntPtr, Type>> _pointerDictionary;
        static volatile object _sysLock = new object();

        protected override void ConstructorInit()
        {
            base.ConstructorInit();
            _pointerDictionary = new Dictionary<int, Dictionary<IntPtr, Type>>();
        }

        protected override void Process(object queueItem)
        {
            int tokenId = Convert.ToInt32(queueItem);
            ReleaseMemory(tokenId);
        }

        internal void FreeMemory(int tokenID)
        {
            base.Enque(tokenID);
        }

        internal void AddPointer(int tokenID, Dictionary<IntPtr, Type> structurePointers)
        {
            if (_pointerDictionary.ContainsKey(tokenID))
                ReleaseMemory(tokenID);

            lock (_sysLock)
            {
                _pointerDictionary.Add(tokenID, structurePointers);
            }
        }

        void ReleasePointer(IntPtr val)
        {
            if (val != IntPtr.Zero)
                val = IntPtr.Zero;
        }

        void ReleaseMemory(int tokenID)
        {
            if (_pointerDictionary.ContainsKey(tokenID))
            {
                Dictionary<IntPtr, Type> structDictionary = _pointerDictionary[tokenID];

                foreach (var item in structDictionary)
                {
                    Marshal.DestroyStructure(item.Key, item.Value);
                    Marshal.FreeHGlobal(item.Key);
                    ReleasePointer(item.Key);
                }

                structDictionary.Clear();

                lock (_sysLock)
                {
                    _pointerDictionary.Remove(tokenID);
                }
            }
        }
    }
}
