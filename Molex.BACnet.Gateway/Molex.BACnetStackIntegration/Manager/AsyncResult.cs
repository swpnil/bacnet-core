﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.Manager
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using System;
using System.Threading;

namespace Molex.BACnetStackIntegration.Manager
{
    internal class AsyncResult : IAsyncResult
    {
        private object state;
        private ManualResetEvent manualResentEvent;

        public AsyncResult(object state)
        {
            this.state = state;
            this.manualResentEvent = new ManualResetEvent(false);
        }

        object IAsyncResult.AsyncState
        {
            get { return state; }
        }

        WaitHandle IAsyncResult.AsyncWaitHandle
        {
            get { return this.manualResentEvent; }
        }

        public WaitHandle AsyncWaitHandle
        {
            get { return this.manualResentEvent; }
        }

        bool IAsyncResult.CompletedSynchronously
        {
            get { return false; }
        }

        bool IAsyncResult.IsCompleted
        {
            get { return manualResentEvent.WaitOne(0, false); }
        }

        public void Complete()
        {
            manualResentEvent.Set();
        }
    }
}