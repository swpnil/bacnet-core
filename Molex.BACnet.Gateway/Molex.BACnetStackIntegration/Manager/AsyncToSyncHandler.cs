﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.Manager
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using System;

namespace Molex.BACnetStackIntegration.Manager
{
    internal class AsyncToSyncHandler
    {
        #region Private Variables

        private AsyncResult _asyncResult;

        #endregion Private Variables

        #region Properties

        public int MillisecondsTimeout { get; set; }
        public bool IsTimeout { get; private set; }
        public bool IsSucceed { get; private set; }

        public Action<AsyncToSyncHandler> OnResponseReceived { get; set; }

        #endregion Properties

        public AsyncToSyncHandler(int timeout = -1)
        {
            MillisecondsTimeout = timeout;
        }

        public void Sync()
        {
            _asyncResult = new AsyncResult(null);
            bool isComplete = _asyncResult.AsyncWaitHandle.WaitOne(MillisecondsTimeout);
            IsTimeout = !isComplete;
        }

        public void Complete()
        {
            if (!IsTimeout)
            {
                IsSucceed = true;

                if (_asyncResult != null)
                    _asyncResult.Complete();
            }
        }
    }
}