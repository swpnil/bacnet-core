﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.Manager
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Threading;
using Molex.BACnetStackIntegration.Model.Request;


namespace Molex.BACnetStackIntegration.Manager
{
    internal class RequestManager
    {
        #region Constructor and Singleton Implementation

        /// <summary>
        /// Volatile instance of class.
        /// </summary>
        private static volatile RequestManager _instance;

        /// <summary>
        /// Object for acquiring lock while creating singleton instance
        /// to ensure only one instance is created in multi threading environment.
        /// </summary>
        private static object _syncRoot = new Object();

        /// <summary>
        /// Get's the singleton instance of the class.
        /// </summary>
        public static RequestManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_syncRoot)
                    {
                        if (_instance == null)
                            _instance = new RequestManager();
                    }
                }

                return _instance;
            }
        }

        private RequestManager()
        {
        }

        #endregion Constructor and Singleton Implementation

        #region Private Veriables

        private static volatile object _reqSync = new object();
        private int _currentId = 0;
        private Dictionary<int, StackIntegrationRequestBase> _requestDictionary = new Dictionary<int, StackIntegrationRequestBase>();

        #endregion Private Veriables

        #region Public Methods

        public int GenerateRequestId(StackIntegrationRequestBase request)
        {
            lock (_reqSync)
            {
                _currentId++;
                bool isRequestIdGenerated = false;
                do
                {
                    if (!_requestDictionary.ContainsKey(_currentId))
                    {
                        isRequestIdGenerated = true;
                        _requestDictionary[_currentId] = request;
                    }
                    else
                    {
                        if (_currentId == int.MaxValue)
                            _currentId = 1;
                        else
                            _currentId++;
                    }
                }
                while (!isRequestIdGenerated);

                return _currentId;
            }
        }

        public int AddRequestId(StackIntegrationRequestBase request, int tokenId)
        {
            lock (_reqSync)
            {
                if (!_requestDictionary.ContainsKey(tokenId))
                {
                    _requestDictionary[tokenId] = request;
                }

                return tokenId;
            }
        }

        public void RequestCompleted(int requestId)
        {
            lock (_reqSync)
            {
                StackIntegrationRequestBase request;

                if (_requestDictionary.TryGetValue(requestId, out request))
                {
                    _requestDictionary.Remove(requestId);
                }
            }
        }

        public StackIntegrationRequestBase GetRequest(int requestId)
        {
            int i = 0;
            StackIntegrationRequestBase request;
            do
            {
                request = GetRequestFromDictionary(requestId);
                Thread.Sleep(10);
                i++;
            }
            while (i < 20);

            return request;
        }

        private StackIntegrationRequestBase GetRequestFromDictionary(int requestId)
        {
            lock (_reqSync)
            {
                StackIntegrationRequestBase request;

                _requestDictionary.TryGetValue(requestId, out request);
                return request;
            }
        }

        #endregion Public Methods
    }
}