﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.Stack.Wrapper
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
///

#define BACDEL_PR14


namespace Molex.BACnetStackIntegration.Stack.Wrapper
{
    internal class BacDelDef
    {
        internal const int BacnetArrayOfThree = 3;
        internal const int BacnetArrayOfSeven = 7;
    }

    /// <summary>
    /// Name in Stack Application: BACNET_ACTION
    /// </summary>
    internal enum BacnetAction
    {
        ActionDirect = 0,
        ActionReverse = 1,
        MaxBacnetAction
    }

    /// <summary>
    /// Name in Stack Application: BACNET_BACKUP_STATE
    /// </summary>
    internal enum BacnetBackupState
    {
        backUpStateIdle = 0,
        backUpStatePreparingForBackUp = 1,
        backUpStatePreparingForRestore = 2,
        backUpStatePerformingABackUp = 3,
        backUpStatePerformingARestore = 4,
        backUpStateBackUpFailure = 5,
        backUpStateRestoreFailure = 6,
        maxBackUpState = 7
    }

    /// <summary>
    /// Name in Stack Application: BACNET_POLARITY
    /// </summary>
    internal enum BacnetPolarity
    {
        PolarityNormal = 0,
        PolarityReverse = 1,
        MaxPolarity = 2
    }

    /// <summary>
    /// Name in Stack Application: BACNET_PROGRAM_ERROR
    /// </summary>
    internal enum BacnetProgramError
    {
        ProgramErrorNormal = 0,
        ProgramErrorLoadFailed = 1,
        ProgramErrorInternal = 2,
        ProgramErrorProgram = 3,
        ProgramErrorOther = 4,

        ///<summary> Enumerated values 0-63 are reserved for definition by ASHRAE.  </summary>
        ///<summary> Enumerated values 64-65535 may be used by others subject to  </summary>
        ///<summary> the procedures and constraints described in Clause 23. </summary>
        MaxProgramError = 5
    }

    /// <summary>
    /// Name in Stack Application: BACNET_FILE_ACCESS_METHOD
    /// </summary>
    internal enum BacnetFileAccessMethod
    {
        FileRecordAccess = 0,
        FileStreamAccess = 1,
        MaxFileAccessMethod = 2
    }

    /// <summary>
    /// Name in Stack Application: BACNET_LIFE_SAFETY_MODE
    /// </summary>
    internal enum BacnetLifeSafetyMode
    {
        LifeSafetyModeOff = 0,
        LifeSafetyModeOn = 1,
        LifeSafetyModeTest = 2,
        LifeSafetyModeManned = 3,
        LifeSafetyModeUnmanned = 4,
        LifeSafetyModeArmed = 5,
        LifeSafetyModeDisarmed = 6,
        LifeSafetyModePreArmed = 7,
        LifeSafetyModeSlow = 8,
        LifeSafetyModeFast = 9,
        LifeSafetyModeDisconnected = 10,
        LifeSafetyModeEnabled = 11,
        LifeSafetyModeDisabled = 12,
        LifeSafetyModeAutomaticReleaseDisabled = 13,
        LifeSafetyModeDefault = 14,
        MaxLifeSafetyMode = 15
        ///<summary> Enumerated values 0-255 are reserved for definition by ASHRAE.  </summary>
        ///<summary> Enumerated values 256-65535 may be used by others subject to  </summary>
        ///<summary> procedures and constraints described in Clause 23. </summary>
    }

    /// <summary>
    /// Name in Stack Application: BACNET_LIFE_SAFETY_OPERATION
    /// </summary>
    internal enum BacnetLifeSafetyOperation
    {
        LifeSafetyOpNone = 0,
        LifeSafetyOpSilence = 1,
        LifeSafetyOpAudible = 2,
        LifeSafetyOpVisual = 3,
        LifeSafetyOpReset = 4,
        LifeSafetyOpResetAlarm = 5,
        LifeSafetyOpResetFault = 6,
        LifeSafetyOpUnsilence = 7,
        LifeSafetyOpUnsilenceAudible = 8,
        LifeSafetyOpUnsilenceVisual = 9,

        ///<summary> Enumerated values 0-63 are reserved for definition by ASHRAE.  </summary>
        ///<summary> Enumerated values 64-65535 may be used by others subject to  </summary>
        ///<summary> procedures and constraints described in Clause 23. </summary>
        MaxLifeSafetyOp = 10
    }

    /// <summary>
    /// Name in Stack Application: BACNET_LIFE_SAFETY_STATE
    /// </summary>
    internal enum BacnetLifeSafetyState
    {
        LifeSafetyStateQuiet = 0,
        LifeSafetyStatePreAlarm = 1,
        LifeSafetyStateAlarm = 2,
        LifeSafetyStateFault = 3,
        LifeSafetyStateFaultPreAlarm = 4,
        LifeSafetyStateFaultAlarm = 5,
        LifeSafetyStateNotReady = 6,
        LifeSafetyStateActive = 7,
        LifeSafetyStateTamper = 8,
        LifeSafetyStateTestAlarm = 9,
        LifeSafetyStateTestActive = 10,
        LifeSafetyStateTestFault = 11,
        LifeSafetyStateTestFaultAlarm = 12,
        LifeSafetyStateHoldUp = 13,
        LifeSafetyStateDuress = 14,
        LifeSafetyStateTamperAlarm = 15,
        LifeSafetyStateAbnormal = 16,
        LifeSafetyStateEmergencyPower = 17,
        LifeSafetyStateDelayed = 18,
        LifeSafetyStateBlocked = 19,
        LifeSafetyStateLocalAlarm = 20,
        LifeSafetyStateGeneralAlarm = 21,
        LifeSafetyStateSupervisory = 22,
        LifeSafetyStateTestSupervisory = 23,
        MaxLifeSafetyState = 24
        ///<summary> Enumerated values 0-255 are reserved for definition by ASHRAE.  </summary>
        ///<summary> Enumerated values 256-65535 may be used by others subject to  </summary>
        ///<summary> procedures and constraints described in Clause 23. </summary>
    }

    /// <summary>
    /// Name in Stack Application: BACNET_SILENCED_STATE
    /// </summary>
    internal enum BacnetSilencedState
    {
        SilencedStateUnsilenced = 0,
        SilencedStateAudibleSilenced = 1,
        SilencedStateVisibleSilenced = 2,
        SilencedStateAllSilenced = 3,
        /* Enumerated values 0-63 are reserved for definition by ASHRAE. */
        /* Enumerated values 64-65535 may be used by others subject to */
        /* procedures and constraints described in Clause 23. */
        MaxSilencedState = 4
    }

    /// <summary>
    /// Name in Stack Application: BACNET_MAINTENANCE
    /// </summary>
    internal enum BacnetMaintenance
    {
        MaintenanceNone = 0,
        MaintenancePeriodicTest = 1,
        MaintenanceNeedServiceOperational = 2,
        MaintenanceNeedServiceInOperative = 3,
        /* Enumerated values 0-255 are reserved for definition by ASHRAE.  */
        /* Enumerated values 256-65535 may be used by others subject to  */
        /* procedures and constraints described in Clause 23. */
        MaxMaintenance = 4
    }

    /// <summary>
    /// Name in Stack Application: BACNET_VT_CLASS
    /// </summary>
    internal enum BacnetVTClass
    {
        VtClassDefault = 0,
        VtClassANSIX34 = 1,      /* real name is ANSI X3.64 */
        VtClassDECVT52 = 2,
        VtClassDECVT100 = 3,
        VtClassDECVT220 = 4,
        VtClassHP70094 = 5,     /* real name is HP 700/94 */
        VtClassIBM3130 = 6,
        /* Enumerated values 0-63 are reserved for definition by ASHRAE.  */
        /* Enumerated values 64-65535 may be used by others subject to  */
        /* the procedures and constraints described in Clause 23. */
        MaxVtClass = 7
    }

    /// <summary>
    /// Name in Stack Application: BACNET_CHARACTER_STRING_ENCODING
    /// </summary>
    internal enum BacnetCharacterStringEncoding
    {
        CharacterANSIX34 = 0,
        CharacterIBMMsDbcs = 1,
        CharacterJISX0208 = 2,
        CharacterUCS4 = 3,
        CharacterUCS2 = 4,
        CharacterISO88591 = 5,
        MaxCharacterStringEncoding = 6
    }

    ///<summary>
    ///note: these are not the real values,
    ///but are shifted left for easy encoding
    ///Name in Stack Application: BACNET_PDU_TYPE
    ///</summary>
    internal enum BacnetPduType
    {
        PduTypeConfirmedServiceRequest = 0,
        PduTypeUnconfirmedServiceRequest = 0x10,
        PduTypeSimpleAck = 0x20,
        PduTypeComplexAck = 0x30,
        PduTypeSegmentAck = 0x40,
        PduTypeError = 0x50,
        PduTypeReject = 0x60,
        PduTypeAbort = 0x70
    }

    ///<summary>
    ///Bit String Enumerations
    ///Name in Stack Application: BACNET_SERVICES_SUPPORTED
    ///</summary>
    internal enum BacnetServicesSupported
    {
        ///<summary> Alarm and Event Services </summary>
        ServiceSupportedAcknowledgeAlarm = 0,

        ServiceSupportedConfirmedCOVNotification = 1,
        ServiceSupportedConfirmedEventNotification = 2,
        ServiceSupportedGetAlarmSummary = 3,
        ServiceSupportedGetEnrollmentSummary = 4,
        ServiceSupportedSubscribeCOV = 5,

        ///<summary> File Access Services </summary>
        ServiceSupportedAtomicReadFile = 6,

        ServiceSupportedAtomicWriteFile = 7,

        ///<summary> Object Access Services </summary>
        ServiceSupportedAddListElement = 8,

        ServiceSupportedRemoveListElement = 9,
        ServiceSupportedCreateObject = 10,
        ServiceSupportedDeleteObject = 11,
        ServiceSupportedReadProperty = 12,
        ServiceSupportedReadPropConditional = 13,
        ServiceSupportedReadPropMultiple = 14,
        ServiceSupportedReadRange = 35,
        ServiceSupportedWriteProperty = 15,
        ServiceSupportedWritePropMultiple = 16,

        ///<summary> Remote Device Management Services </summary>
        ServiceSupportedDeviceCommunicationControl = 17,

        ServiceSupportedConfirmedPrivateTransfer = 18,
        ServiceSupportedConfirmedTextMessage = 19,
        ServiceSupportedReinitializeDevice = 20,

        ///<summary> Virtual Terminal Services </summary>
        ServiceSupportedVtOpen = 21,

        ServiceSupportedVtClose = 22,
        ServiceSupportedVtData = 23,

        ///<summary> Security Services </summary>
        ServiceSupportedAuthenticate = 24,

        ServiceSupportedRequestKey = 25,
        ServiceSupportedIAm = 26,
        ServiceSupportedIHave = 27,
        ServiceSupportedUnconfirmedCOVNotification = 28,
        ServiceSupportedUnconfirmedEventNotification = 29,
        ServiceSupportedUnconfirmedPrivateTransfer = 30,
        ServiceSupportedUnconfirmedTextMessage = 31,
        ServiceSupportedTimeSynchronization = 32,
        ServiceSupportedUtcTimeSynchronization = 36,
        ServiceSupportedWhoHas = 33,
        ServiceSupportedWhoIs = 34,
        ServiceSupportedLifeSafetyOperation = 37,
        ServiceSupportedSubscribeCOVProperty = 38,
        ServiceSupportedGetEventInformation = 39,

        ///<summary> Other services to be added as they are defined. </summary>
        ///<summary> All values in this production are reserved </summary>
        ///<summary> for definition by ASHRAE. </summary>
        MaxBacnetServicesSupported = 40
    }

    /// <summary>
    /// Name in Stack Application: BACNET_BVLC_FUNCTION
    /// </summary>
    internal enum BacnetBvlcFunction
    {
        BvlcResult = 0,
        BvlcWriteBroadcastDistributionTable = 1,
        BvlcReadBroadcastDistTable = 2,
        BvlcReadBroadcastDistTableAck = 3,
        BvlcForwardedNpdu = 4,
        BvlcRegisterForeignDevice = 5,
        BvlcReadForeignDeviceTable = 6,
        BvlcReadForeignDeviceTableAck = 7,
        BvlcDeleteForeignDeviceTableEntry = 8,
        BvlcDistributeBroadcastToNetwork = 9,
        BvlcOriginalUnicastNpdu = 10,
        BvlcOriginalBroadcastNpdu = 11,
        MaxBvlcFunction = 12
    }

    /// <summary>
    /// Name in Stack Application: BACNET_BVLC_RESULT
    /// </summary>
    internal enum BacnetBvlcResult
    {
        BvlcResultSuccessfulCompletion = 0x0000,
        BvlcResultWriteBroadcastDistributionTableNak = 0x0010,
        BvlcResultReadBroadcastDistributionTableNak = 0x0020,
        BvlcResultRegisterForeignDeviceNak = 0X0030,
        BvlcResultReadForeignDeviceTableNak = 0x0040,
        BvlcResultDeleteForeignDeviceTableEntryNak = 0x0050,
        BvlcResultDistributeBroadcastToNetworkNak = 0x0060,
        MaxBvlcResult
    }

    /// <summary>
    /// Name in Stack Application: BACNET_ACKNOWLEDGMENT_FILTER
    /// </summary>
    internal enum BacnetAcknowledgmentFilter
    {
        AcknowledgmentFilterAll = 0,
        AcknowledgmentFilterAcked = 1,
        AcknowledgmentFilterNotAcked = 2,
        MaxAcknowledgmentFilter = 3
    }

    /// <summary>
    /// Name in Stack Application: BACNET_EVENT_STATE_FILTER
    /// </summary>
    internal enum BacnetEventStateFilter
    {
        EventStateFilterOffNormal = 0,
        EventStateFilterFault = 1,
        EventStateFilterNormal = 2,
        EventStateFilterAll = 3,
        EventStateFilterActive = 4,
        MaxEventStateFilter = 5
    }

    /// <summary>
    /// Name in Stack Application: BACNET_MESSAGE_PRIORITY
    /// </summary>
    internal enum BacnetMessagePriority
    {
        MessagePriorityNormal = 0,
        MessagePriorityUrgent = 1,
        MessagePriorityCriticalEquipment = 2,
        MessagePriorityLifeSafety = 3,
        MaxMessagePriority = 4
    }

    ///<summary> Network Layer Message Type
    ///If Bit 7 of the control octet described in 6.2.2 is 1,
    ///a message type octet shall be Present as shown in Figure 6-1.
    ///The following message types are indicated:
    ///Name in Stack Application: BACNET_NETWORK_MESSAGE_TYPE
    ///</summary>
    internal enum BacnetNetworkMessageType
    {
        NetworkMessageWhoIsRouterToNetwork = 0,
        NetworkMessageIAmRouterToNetwork = 1,
        NetworkMessageICouldBeRouterToNetwork = 2,
        NetworkMessageRejectMessageToNetwork = 3,
        NetworkMessageRouterBusyToNetwork = 4,
        NetworkMessageRouterAvailableToNetwork = 5,
        NetworkMessageInitRtTable = 6,
        NetworkMessageInitRtTableAck = 7,
        NetworkMessageEstablishConnectionToNetwork = 8,
        NetworkMessageDisconnectConnectionToNetwork = 9,

        ///<summary> X'0A' to X'7F': Reserved for use by ASHRAE, </summary>
        ///<summary> X'80' to X'FF': Available for Vendor proprietary messages </summary>
        NetworkMessageInvalid = 0x100
    }

    internal enum BacnetAbortReason
    {
        AbortReasonOther = 0,
        AbortReasonBufferOverflow = 1,
        AbortReasonInvalidApduInThisState = 2,
        AbortReasonPreemtedByHigherPriorityTask = 3,
        AbortReasonSegmentationNotSupported = 4,
        AbortReasonSecurityError = 5,
        AbortReasonInSufficientSecurity = 6,
        AbortReasonWindowSizeOutOfRange = 7,
        AbortReasonApplicationExceededReplyTime = 8,
        AbortReasonOutOfResources = 9,
        AbortReasonTsmTimeOut = 10,
        AbortReasonApduTooLong = 11,
        /* Enumerated values 0-63 are reserved for definition by ASHRAE. */
        /* Enumerated values 64-65535 may be used by others subject to */
        /* the procedures and constraints described in Clause 23. */
        MaxBacnetAbortReason = 12,
        FirstProprietaryAbortReason = 64,
        LastProprietaryAbortReason = 65535
    }

    internal enum BacnetRejectReason
    {
        RejectReasonOther = 0,
        RejectReasonBufferOverflow = 1,
        RejectReasonInconsistentParameters = 2,
        RejectReasonInvalidParameterDataType = 3,
        RejectReasonInvalidTag = 4,
        RejectReasonMissingRequiredParameter = 5,
        RejectReasonParameterOutOfRange = 6,
        RejectReasonTooManyArguments = 7,
        RejectReasonUndefinedEnumeration = 8,
        RejectReasonUnrecognisedService = 9,
        /* Enumerated values 0-63 are reserved for definition by ASHRAE. */
        /* Enumerated values 64-65535 may be used by others subject to */
        /* the procedures and constraints described in Clause 23. */
        MaxBacnetRejectReason = 10,
        FirstProprietaryRejectReason = 64,
        LastProprietaryRejectReason = 65535
    }



    /// <summary>
    /// Name in Stack Application: BACNET_REINITIALIZED_STATE
    /// </summary>
    internal enum BacnetReInitializedState
    {
        BacnetReInitColdStart = 0,
        BacnetReInitWarmStart = 1,
        BacnetReInitStartBackup = 2,
        BacnetReInitEndBackup = 3,
        BacnetReInitStartRestore = 4,
        BacnetReInitEndRestore = 5,
        BacnetReInitAbortRestore = 6,
        MaxBacnetReinitializedState = 7,
        BacnetReInitIdle = 255
    }

    /// <summary>
    /// Name in Stack Application: BACNET_NODE_TYPE
    /// </summary>
    internal enum BacnetNodeType
    {
        BacnetNodeUnknown = 0,
        BacnetNodeSystem = 1,
        BacnetNodeNetwork = 2,
        BacnetNodeDevice = 3,
        BacnetNodeOrganizational = 4,
        BacnetNodeArea = 5,
        BacnetNodeEquipment = 6,
        BacnetNodePoint = 7,
        BacnetNodeCollection = 8,
        BacnetNodeProperty = 9,
        BacnetNodeFunctional = 10,
        BacnetNodeOther = 11,
        MaxNodeType = 12
    }

    /// <summary>
    /// Name in Stack Application: BACNET_ACC_STATUS
    /// </summary>
    internal enum BacnetAccStatus
    {
        BacnetAccStatusNormal = 0,
        BacnetAccStatusStarting = 1,
        BacnetAccStatusRecovered = 2,
        BacnetAccStatusAbnormal = 3,
        BacnetAccStatusFailed = 4,
        MaxBacnetAccStatus
    }

    /// <summary>
    /// Name in Stack Application: BACNET_AUTHENTICATION_FACTOR_TYPE
    /// </summary>
    internal enum BacnetAuthenticateFactorType
    {
        AuFactorTypeUndefined = 0,
        AuFactorTypeError = 1,
        AuFactorTypeCustom = 2,
        AuFactorTypeSimpleNumber16 = 3,
        AuFactorTypeSimpleNumber32 = 4,
        AuFactorTypeSimpleNumber56 = 5,
        AuFactorTypeSimpleAlphaNumeric = 6,
        AuFactorTypeAbaTrack2 = 7,
        AuFactorTypeWiegand26 = 8,
        AuFactorTypeWiegand37 = 9,
        AuFactorTypeWiegand37Facility = 10,
        AuFactorTypeFacility16Card32 = 11,
        AuFactorTypeFacility32Card32 = 12,
        AuFactorTypeFascN = 13,
        AuFactorTypeFascNBcd = 14,
        AuFactorTypeFascNLarge = 15,
        AuFactorTypeFascNLargeBcd = 16,
        AuFactorTypeGsa75 = 17,
        AuFactorTypeChuid = 18,
        AuFactorTypeChuidFull = 19,
        AuFactorTypeGuid = 20,
        AuFactorTypeCbeffA = 21,
        AuFactorTypeCbeffB = 22,
        AuFactorTypeCbeffC = 23,
        AuFactorTypeUserPassword = 24,
        MAXAuthenticationFactorType = 25
    }

    /// <summary>
    /// Name in Stack Application: BACNET_SECURITY_LEVEL
    /// </summary>
    internal enum BacnetSecurityLevel
    {
        SecurityLevelInCapable = 0, //INDICATES THAT THE DEVICE IS CONFIGURED TO NOT USE SECURITY
        SecurityLevelPlain = 1,
        SecurityLevelSigned = 2,
        SecurityLevelEncrypted = 3,
        SecurityLevelSignedEndToEnd = 4,
        SecurityLevelEncryptedEndToEnd = 5,
        MaxSecurityLevel = 6
    }

    /// <summary>
    /// Name in Stack Application: BACNET_SECURITY_POLICY
    /// </summary>
    internal enum BacnetSecurityPolicy
    {
        SecurityPolicyPlainNonTrusted = 0,
        SecurityPolicyPlainTrusted = 1,
        SecurityPolicySignedTrusted = 2,
        SecurityPolicyEncryptedTrusted = 3,
        MaxSecurityPolicy = 4
    }

    /// <summary>
    /// Name in Stack Application: BACNET_DOOR_VALUE
    /// </summary>
    internal enum BacnetDoorValue
    {
        DoorValueLock = 0,
        DoorValueUnlock = 1,
        DoorValuePulseUnlock = 2,
        DoorValueExtendedPulseUnlock = 3,
        MaxDoorValue = 4
    }

    /// <summary>
    /// Name in Stack Application: BACNET_DOOR_STATUS
    /// </summary>
    internal enum BacnetDoorStatus
    {
        DoorStatusClosed = 0,
        DoorStatusOpened = 1,
        DoorStatusUnknown = 2,
        DoorStatusDoorFault = 3,
        DoorStatusUnused = 4,
        MaxDoorStatus = 5
    }

    /// <summary>
    /// Name in Stack Application: BACNET_LOCK_STATUS
    /// </summary>
    internal enum BacnetLockStatus
    {
        LockStatusLocked = 0,
        LockStatusUnlocked = 1,
        LockStatusLockFault = 2,
        LockStatusUnused = 3,
        LockStatusUnknown = 4,
        MaxLockStatus = 5
    }

    /// <summary>
    /// Name in Stack Application: BACNET_DOOR_SECURED_STATE
    /// </summary>
    internal enum BacnetDoorSecuredState
    {
        DoorSecuredStateSecured = 0,
        DoorSecuredStateUnsecured = 1,
        DoorSecuredStateUnknown = 2,
        MaxDoorSecuredState = 4
    }

    /// <summary>
    /// Name in Stack Application: BACNET_DOOR_ALARM_STATE
    /// </summary>
    internal enum BacnetDoorAlarmState
    {
        DoorAlarmStateNormal = 0,
        DoorAlarmStateAlarm = 1,
        DoorAlarmStateDoorOpenTooLong = 2,
        DoorAlarmStateForcedOpen = 3,
        DoorAlarmStateTamper = 4,
        DoorAlarmStateDoorFault = 5,
        DoorAlarmStateLockDown = 6,
        DoorAlarmStateFreeAccess = 7,
        DoorAlarmStateEgressOpen = 8,
        MaxDoorAlarmState = 9
    }

    /// <summary>
    /// Name in Stack Application: BACNET_ACCESS_EVENT
    /// </summary>
    internal enum BacnetAccessEvent
    {
        AccessEventNone = 0,
        AccessEventGranted = 1,
        AccessEventMuster = 2,
        AccessEventPassbackDetected = 3,
        AccessEventDuress = 4,
        AccessEventTrace = 5,
        AccessEventLockoutMaxAttempts = 6,
        AccessEventLockoutOther = 7,
        AccessEventLockoutRelinquished = 8,
        AccessEventLockedByHigherPriority = 9,
        AccessEventOutOfService = 10,
        AccessEventOutOfServiceRelinquished = 11,
        AccessEventAccompanimentBy = 12,
        AccessEventAuthenticationFactorRead = 13,
        AccessEventAuthorizationDelayed = 14,
        AccessEventVerificationRequired = 15,

        // Enumerated values 128-511 are used for events which indicate that access has been denied.
        AccessEventDeniedDenyAll = 128,

        AccessEventDeniedUnknownCredential = 129,
        AccessEventDeniedAuthenticationUnavailable = 130,
        AccessEventDeniedAuthenticationFactorTimeout = 131,
        AccessEventDeniedIncorrectAuthenticationFactor = 132,
        AccessEventDeniedZoneNoAccessRights = 133,
        AccessEventDeniedPointNoAccessRights = 134,
        AccessEventDeniedNoAccessRights = 135,
        AccessEventDeniedOutOfTimeRange = 136,
        AccessEventDeniedThreatLevel = 137,
        AccessEventDeniedPassback = 138,
        AccessEventDeniedUnexpectedLocationUsage = 139,
        AccessEventDeniedMaxAttempts = 140,
        AccessEventDeniedLowerOccupancyLimit = 141,
        AccessEventDeniedUpperOccupancyLimit = 142,
        AccessEventDeniedAuthenticationFactorLost = 143,
        AccessEventDeniedAuthenticationFactorStolen = 144,
        AccessEventDeniedAuthenticationFactorDamaged = 145,
        AccessEventDeniedAuthenticationFactorDestroyed = 146,
        AccessEventDeniedAuthenticationFactorDisabled = 147,
        AccessEventDeniedAuthenticationFactorError = 148,
        AccessEventDeniedCredentialUnassigned = 149,
        AccessEventDeniedCredentialNotProvisioned = 150,
        AccessEventDeniedCredentialNotYetActive = 151,
        AccessEventDeniedCredentialExpired = 152,
        AccessEventDeniedCredentialManualDisable = 153,
        AccessEventDeniedCredentialLockout = 154,
        AccessEventDeniedCredentialMaxDays = 155,
        AccessEventDeniedCredentialMaxUses = 156,
        AccessEventDeniedCredentialInactivity = 157,
        AccessEventDeniedCredentialDisabled = 158,
        AccessEventDeniedNoAccompaniment = 159,
        AccessEventDeniedIncorrectAccompaniment = 160,
        AccessEventDeniedLockout = 161,
        AccessEventDeniedVerificationFailed = 162,
        AccessEventDeniedVerificationTimeout = 163,
        AccessEventDeniedOther = 164,

        ///<summary> Enumerated values 0-511 are reserved for definition by ASHRAE. </summary>
        ///<summary> Enumerated values 512-65535 may be used by others subject to the
        /// procedures and constraints described in Clause 23.
        /// </summary>
        MaxBacnetAccessEvent
    }

    /// <summary>
    /// Name in Stack Application: BACNET_ACCESS_ZONE_OCCUPANCY_STATE
    /// </summary>
    internal enum BacnetAccessZoneOccupancyState
    {
        ZoneOccupancyStateNormal = 0,
        ZoneOccupancyStateBelowLowerLimit = 1,
        ZoneOccupancyStateAtLowerLimit = 2,
        ZoneOccupancyStateAtUpperLimit = 3,
        ZoneOccupancyStateAboveUpperLimit = 4,
        ZoneOccupancyStateDisabled = 5,
        ZoneOccupancyStateNotSupported = 6,
        /* Enumerated values 0-63 are reserved for definition by ASHRAE. */
        /* Enumerated values 64-65535 may be used by others subject to the
           procedures and constraints described in Clause 23. */
        MaxAccessZoneOccupancyState = 7
    }

    /// <summary>
    /// Name in Stack Application: BACNET_ACCESS_PASSBACK_MODE
    /// </summary>
    internal enum BacnetAccessPassbackMode
    {
        PassbackModePassbackOff = 0,
        PassbackModeHardPassback = 1,
        PassbackModeSoftPassback = 2,
        MaxPassbackMode = 3
    }

    /// <summary>
    /// Name in Stack Application: BACNET_ACCESS_USER_TYPE
    /// </summary>
    internal enum BacnetAccessUserType
    {
        AccessUserTypeAsset = 0,
        AccessUserTypeGroup = 1,
        AccessUserTypePerson = 2,
        /* Enumerated values 0-63 are reserved for definition by ASHRAE. */
        /* Enumerated values 64-65535 may be used by others subject to the
           procedures and constraints described in Clause 23. */
        MaxAccessUserType = 3
    }

    /// <summary>
    /// Name in Stack Application: BACNET_ACCESS_CREDENTIAL_DISABLE
    /// </summary>
    internal enum BacnetAccessCredentialDisable
    {
        ACRNone = 0,
        ACRDisable = 1,
        ACRDisableManual = 2,
        ACRDisableLockOut = 3,
        MaxAccessCredentialDisable = 4
    }

    /// <summary>
    /// Name in Stack Application: BACNET_SHED_LEVEL
    /// To resolve conlict with struct name, this enum named as BacnetShedLevelType
    /// </summary>
    internal enum BacnetShedLevelType
    {
        ShedLevelPercent = 0,
        ShedLevelLevel = 1,
        ShedLevelAmount = 2,
        MaxShedLevel = 3
    }

    /// <summary>
    /// Name in Stack Application: BACNET_TIME_RANGE_SPECIFIER
    /// </summary>
    internal enum BacnetTimeRangeSpecifier
    {
        TimeRangeSpecifierSpecified = 0,
        TimeRangeSpecifierAlways = 1,
        MaxTimeRangeSpecifier = 2
    }

    /// <summary>
    /// Name in Stack Application: BACNET_LOCATION_SPECIFIER
    /// </summary>
    internal enum BacnetLocationSpecifier
    {
        LocationSpecifierSpecified = 0,
        LocationSpecifierAll = 1,
        MaxLocationSpecifier = 2
    }

    /// <summary>
    /// Name in Stack Application: BACNET_ACCESS_AUTHENTICATION_FACTOR_DISABLE
    /// </summary>
    internal enum BacnetAccessAuthenticationFactorDisable
    {
        AAFDNone = 0,
        AAFDDisabled = 1,
        AAFDDisabledLost = 2,
        AAFDDisabledStolen = 3,
        AAFDDisabledDamaged = 4,
        AAFDDisabledDestroyed = 5,
        /* Enumerated values 0-63 are reserved for definition by ASHRAE. */
        /* Enumerated values 64-65535 may be used by others subject to the
           procedures and constraints described in Clause 23. */
        MaxAccessAuthenticationFactorDisable = 6
    }

    /// <summary>
    /// Name in Stack Application: BACNET_AUTHENTICATION_STATUS
    /// </summary>
    internal enum BacnetAuthenticationStatus
    {
        AuStatusNotReady = 0,
        AuStatusReady = 1,
        AuStatusDisabled = 2,
        AuStatusWaitingForAuthenticationFactor = 3,
        AuStatusWaitingForAccompaniment = 4,
        AuStatusWaitingForVerification = 5,
        AuStatusInProgress = 6,
        AuStatusMax = 7
    }

    /// <summary>
    /// Name in Stack Application: BACNET_AUTHORIZATION_MODE
    /// </summary>
    internal enum BacnetAuthorizationMode
    {
        AtrzModeAuthorize = 0,
        AtrzModeGrantActive = 1,
        AtrzModeDenyAll = 2,
        AtrzModeVerificationRequired = 3,
        AtrzModeAuthorizationDelayed = 4,
        AtrzModeNone = 5,
        AtrzModeMax = 6
    }

    ///<summary>
    ///enumerations for Fault type
    ///Name in Stack Application: BACNET_FAULT_TYPE
    ///</summary>
    internal enum BacnetFaultType
    {
        FAULT_TYPE_NONE = 0,
        FAULT_TYPE_FAULT_CHARACTERSTRING = 1,
        FAULT_TYPE_FAULT_EXTENDED = 2,
        FAULT_TYPE_FAULT_LIFE_SAFETY = 3,
        FAULT_TYPE_FAULT_STATE = 4,
        FAULT_TYPE_FAULT_STATUS_FLAGS = 5,
        MAX_FAULT_TYPE
    }




    ///<summary>
    ///Enum defining default configuration parameters
    ///Name in Stack Application: BACNET_STACK_CONFIG_PARAMETERS
    ///</summary>
    internal enum BacnetStackConfigParameters
    {
        BacnetStackconfigparamThreadpoolTimeout = 1,
        BacnetStackconfigparamClrInitiateQueTimeout = 2,
        BacnetStackconfigparamClrInitiatedReqTimeout = 3,
        BacnetStackconfigparamPxQueTimeout = 4,
        BacnetStackconfigparamClrInitiateQueDeletionCount = 5,
        BacnetStackconfigparamClrBBMDIinitiateQueTimeout = 6,
        /* Add new default configuration parameters as applicable */
        MaxStackConfigParameters
    }

    ///<summary>
    ///Enum defining bacnet lighting operations
    ///Name in Stack Application: BACNET_LIGHTING_OPERATION
    ///</summary>
    internal enum BACnetLightingOperation
    {
        BacnetLightingOperationNone = 0,
        BacnetLightingOperationFadeTo = 1,
        BacnetLightingOperationRampTo = 2,
        BacnetLightingOperationStepUp = 3,
        BacnetLightingOperationStepDown = 4,
        BacnetLightingOperationStepOn = 5,
        BacnetLightingOperationStepOff = 6,
        BacnetLightingOperationWarn = 7,
        BacnetLightingOperationWarnOff = 8,
        BacnetLightingOperationWarnRelinquish = 9,
        BacnetLightingOperationStop = 10,
        MaxLightingOperation = 11
    }

}