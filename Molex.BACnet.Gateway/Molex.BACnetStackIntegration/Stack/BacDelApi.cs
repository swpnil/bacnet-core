﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.Stack.Wrapper
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.BACnetStackIntegration.StackStructures;
using Molex.StackDataObjects.Constants;
using System;
using System.Runtime.InteropServices;

namespace Molex.BACnetStackIntegration.Stack.Wrapper
{

    /****************************************************************************
    **** Delegates (Function Pointers)
    ****************************************************************************/

    #region Delegates

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    internal delegate BacnetReturnType App_Callback_Interface_t(IntPtr serviceArgs, byte reason);

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    internal delegate BacnetReturnType App_AutoResp_Interface_t(uint devId, uint callbackId,
                                                              BacnetServicesSupported serviceType,
                                                              IntPtr errorClass,
                                                              IntPtr errorResponse,
                                                              IntPtr pduType,
                                                              IntPtr serviceData,
                                                              IntPtr serviceResp,
                                                              IntPtr otherData,
                                                              IntPtr rmtDvAddr,
                                                              IntPtr timeStamp
                                                              );

    /** Application interface callback function pointer defination for 
internal stack callbacks */
    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    internal delegate void App_Interface_t(uint deviceId,
    uint objectId,
    BacnetObjectType objType,
    BacnetCallbackType callbackType,
    BacnetCallbackReason callbackReason,
    uint dataValue,
    IntPtr pvDataValue
    );

    #endregion Delegates

    internal static class BacDelApi
    {
        private const string BACnetStackDllPath = @"libBACDEL_Stack.so";
        //private const string BACnetStackDllPath = @"BACnet_Stack.dll";
        /// <summary>
        /// API to get the IP address of specific device or to get the  device ID of a specific IP address.
        /// </summary>
        /// <param name="deviceID">Pointer to device id value.</param>
        /// <param name="deviceAdd">Pointer to IP address structure.</param>
        /// <param name="searchIPAdd">Search for IP address or device id.</param>
        /// <returns>success or suitable error code.</returns>
        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern BacnetReturnType BACDEL_Get_Device_ID_Or_Address(IntPtr deviceID, IntPtr deviceAdd, bool searchIPAdd);

        /// <summary>
        /// APi to get ip address of router from its network no.
        /// </summary>
        /// <param name="networkNo">Network No whose Ip address is required.</param>
        /// <param name="deviceAdd">Pointer to save IP address.</param>
        /// <returns>success or suitable error code.</returns>
        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern BacnetReturnType BACDEL_Get_Router_From_Network_No(ushort networkNo, IntPtr deviceAdd);

        /// <summary>
        /// API to register callback functions for auto-responses from stack.
        /// </summary>
        /// <param name="appChoice">Type of Function to be called.</param>
        /// <param name="fpFunction">The Callback function pointer of applications</param>
        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern void BACDEL_App_CallBack_Register(BacAppCallBackFunChoice appChoice, App_AutoResp_Interface_t fpFunction);

        /// <summary>
        /// API to Un-register callback functions for auto-responses from stack.
        /// </summary>
        /// <param name="eAppChoice">Type of Function to be called.</param>
        /// <returns>void/nothing.</returns>
        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern void BACDEL_App_CallBack_UnRegister(BacAppCallBackFunChoice appChoice);

        /// <sumary>
        /// API to log input character string into log file or display it on console
        /// </sumary>
        /// <param name="debugLogLevel">Debug level of message</param>
        /// <param name="format">Character string to be printed or logged</param>
        /// <param name="">Variable arguments</param>
        /// <returns>success or suitable error code.</returns>
        //internal static extern BacnetReturnType BACDEL_Print_DebugMsg(uint debugLogLevel, IntPtr format,..)  ;

        /// <summary>
        /// API to add pure ip device or virtual devices in BACnet stack.
        /// </summary>
        /// <param name="dADR">New Device DADR (required only for VD's).</param>
        /// <param name="deviceID">Device Id assigned to new device.</param>
        /// <param name="sNET">2-octet source network number (0 for pure IP device)</param>
        /// <returns>success or suitable error code.</returns>
        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern BacnetReturnType BACDEL_Add_Device(uint dADR, uint deviceID, ushort sNET);

        /// <summary>
        /// API to delete pure ip device or virtual devices present in stack.
        /// Note: 1st host device (pure ip device) can't be deleted with this api.
        /// </summary>
        /// <param name="deviceID">Device to be deleted.
        ///						   - if i32Device_Id == -1, then free all devices.
        ///         			   - else free only deviceID.</param>
        /// <returns>success or suitable error code.</returns>
        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern BacnetReturnType BACDEL_Delete_Device(int deviceID);

        /// <summary>
        /// API to add BACnet objects (other than device) in the created devices.
        /// </summary>
        /// <param name="deviceID">Device in which object is to be added.</param>
        /// <param name="objType">The type of object to be added.</param>
        /// <param name="objID">The Object Identifier for new object.</param>
        /// <param name="objName">Pointer to New Object Name.</param>
        /// <returns>success or suitable error code.</returns>
        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern BacnetReturnType BACDEL_Add_Object(uint deviceID, BacnetObjectType objType, uint objID, IntPtr objName);

        /// <summary>
        ///  API to delete BACnet objects (other than device) in the created devices.
        /// </summary>
        /// <param name="deviceID">Device in which object is to be deleted.</param>
        /// <param name="objType">The type of object to be deleted.</param>
        /// <param name="objID">Id of object to be deleted.</param>
        /// <returns>success or suitable error code.</returns>
        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern BacnetReturnType BACDEL_Delete_Object(uint deviceID, BacnetObjectType objType, uint objID);

        /// <summary>
        /// API to initialize the BACnet-IP stack on given network interface.
        /// It initializes-creates all the threads, mutex, handles, etc.
        /// </summary>
        /// <param name="ipAddrs">ip address of the interface to run the stack.</param>
        /// <param name="udpPortNum">udp port no to run the stack.</param>
        /// <param name="localNetworkNo">local network no for stack.</param>
        /// <param name="virtualNetworkNo">virtual network no for stack.</param>
        /// <returns>success or suitable error code.</returns>
        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern BacnetReturnType BACDEL_Stack_Init([MarshalAs(UnmanagedType.LPArray)] byte[] ipAddrs, ushort udpPortNum, ushort localNetworkNo, ushort virtualNetworkNo);

        /// <summary>
        /// Api to change IP address and/or UDP port no at runtime.
        /// The follow up process after IP change will be
        /// 1) Binding new IP address with the socket.
        /// 2) Sending an I-AM response.
        /// 3) Sending WHO-IS request.
        /// 4) Resending COV Subscribe Request to registered devices.
        /// </summary>
        /// <param name="udpPortNum">udp port no to run the stack.</param>
        /// <param name="ipAddr">ip address of the interface to run the stack.</param>
        /// <returns>success or suitable error code.</returns>
        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern BacnetReturnType BACDEL_Change_IpAddress_PortNo(ushort udpPortNum, [MarshalAs(UnmanagedType.LPArray)] byte[] ipAddr);

        /// <summary>
        ///  API to de-initialize the BACnet-IP stack.
        /// It delets all the threads, mutex, handles, etc and frees all dynamically
        /// allocated memories.
        /// </summary>
        /// <returns>success or suitable error code.</returns>
        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern BacnetReturnType BACDEL_Stack_Deinit();

        /// <summary>
        /// API to start the stack socket communication.
        /// </summary>
        /// <returns>success or suitable error code.</returns>
        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern BacnetReturnType BACDEL_Enable_Network_Communication();

        /// <summary>
        ///  API to stop the stack socket communication.
        /// </summary>
        /// <returns>success or suitable error code.</returns>
        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern BacnetReturnType BACDEL_Disable_Network_Communication();

        /// <summary>
        /// API to delete all host/virtual devices and remote devices created in stack.
        /// </summary>
        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern void BACDEL_Clear_Device_Objects();

        /// <summary>
        /// * API to read property value of any object of any device created within the stack.
        /// </summary>
        /// <param name="devId">Device to which object belongs.</param>
        /// <param name="objId">Object to which property belongs.</param>
        /// <param name="objType">Type of object.</param>
        /// <param name="devProp">Property that is to be read.</param>
        /// <param name="arrayIndex">Array index.</param>
        /// <param name="readPropVal">Double pointer to save property value pointer.</param>
        /// <param name="arrayIndexPresent">Indicates if array index in present or not.</param>
        /// <param name="dataType">Property data type.</param>
        /// <returns>MAX_BACNET_ERROR_CODE on success.</returns>
        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern BacnetErrorCode BACDEL_Get_Object_Property(uint devId,
                                                                        uint objId,
                                                                        BacnetObjectType objType,
                                                                        BacnetPropertyID devProp,
                                                                        uint arrayIndex,
                                                                        IntPtr readPropVal,
                                                                        bool arrayIndexPresent,
                                                                        IntPtr dataType);

        /// <summary>
        /// API to update the property value of specified object.
        ///Property value of only one property can be updated using this API.
        /// </summary>
        /// <param name="devID">Device Id.</param>
        /// <param name="objectID">Object Id.</param>
        /// <param name="objType">Type of Object.</param>
        /// <param name="devProp">Property ID.</param>
        /// <param name="arrayIndex">Array index for the property value.</param>
        /// <param name="pvWritePropVal">Pointer to property value.</param>
        /// <param name="priority">Priority.</param>
        /// <param name="arrayIndexPresent">Specifies if array index is present or not.</param>
        /// <param name="dataType">Property data type.</param>
        /// <returns>MAX_BACNET_ERROR_CODE on success.</returns>
        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern BacnetErrorCode BACDEL_Set_Object_Property(uint devID, uint objectID, BacnetObjectType objType,
                                                                        BacnetPropertyID devProp, uint arrayIndex, IntPtr pvWritePropVal,
                                                                        byte priority, bool arrayIndexPresent, BacnetDataType dataType);

        /// <summary>
        /// API to get the size (in bytes) required to create 1 instance of respective object type.
        /// This size does not include size of array or list type properties.
        /// </summary>
        /// <param name="objectType">type of bacnet object.</param>
        /// <returns> size in no of bytes.</returns>
        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern int BACDEL_Get_Sizeof_Object(BacnetObjectType objectType);

        /// <summary>
        ///  Api to reinit device. Reinit device mechanism is vendor specific.
        /// To use default stack implementation call this api from callback function.
        /// </summary>
        /// <param name="deviceId">Device id</param>
        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern void BACDEL_ReInitDevice(uint deviceId, BACnetRestartReason eRestartReason);

        /// <summary>
        /// API to get the property application tag type as per the object type.
        /// </summary>
        /// <param name="objectType">type of bacnet object.</param>
        /// <param name="objectProperty">property id.</param>
        /// <returns> type of application tag.</returns>
        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern BacnetApplicationTag BACDEL_Get_Property_Tag(BacnetObjectType objectType, BacnetPropertyID objectProperty);

        /// <summary>
        ///  API to get the property data-type as per the object type.
        /// </summary>
        /// <param name="objectType">type of bacnet object.</param>
        /// <param name="objectProperty">property id.</param>
        /// <returns></returns>
        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern BacnetDataType BACDEL_Get_Property_DataType(BacnetObjectType objectType, BacnetPropertyID objectProperty);

        /// <summary>
        ///  Api to set or get the read-write access of object properties.
        /// </summary>
        /// <param name="devID"> Device Id.</param>
        /// <param name="objID"> Object Id.</param>
        /// <param name="objectType">Type of object.</param>
        /// <param name="propID">Property Id.</param>
        /// <param name="accessType">Property AccessType to be set.</param>
        /// <param name="readOrWrite">Write access type if true else read & return access type.</param>
        /// <returns>NOT_SUPPORTED on error & read or written value on success.</returns>
        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern PropAccessType BACDEL_Set_Get_Property_AccessType(uint devID, uint objID, BacnetObjectType objectType,
                                                                               BacnetPropertyID propID, PropAccessType accessType,
                                                                               bool readOrWrite);

        /// <summary>
        ///  Api to set character encoding to be used by devices in stack.
        /// </summary>
        /// <param name="devID">Device Id.</param>
        /// <param name="eCharEncoding">Type of character encoding.</param>
        /// <param name="codePage">Code page for IBM_DBCS encoding.</param>
        /// <returns>success or suitable error code. </returns>
        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern BacnetReturnType BACDEL_Set_Character_Encoding(int devID, BacnetCharacterStringEncoding charEncoding, ushort codePage);

        /// <summary>
        /// API to set Device password common for DCC & Reinit Dv services.
        /// </summary>
        /// <param name="devID">Device Id.</param>
        /// <param name="password">Password for device.</param>
        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern void BACDEL_Set_Device_Password(int devID, [MarshalAs(UnmanagedType.LPArray)] byte[] password);

        /// <summary>
        /// API to set device object properties properties to default values.
        /// </summary>
        /// <param name="devId">Device Id.</param>
        /// <returns>success or suitable error code.</returns>
        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern BacnetReturnType BACDEL_Init_Device_Properties(uint devId, bool bClearObjectList);

        /// <summary>
        ///  API to change the device id and / or DADR of any host device at runtime.
        /// </summary>
        /// <param name="oldDevID"> current device id.</param>
        /// <param name="newDevID"> new device id.</param>
        /// <param name="newDADR">new device DADR value.</param>
        /// <returns>success or suitable error code.</returns>
        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern BacnetReturnType BACDEL_Change_Device_Id(uint oldDevID, uint newDevID, uint newDADR);

        /// <summary>
        /// * API to create directory/folder required to save files containing data for
        /// BBMD service and file object.
        /// </summary>
        /// <returns>success or suitable error code.</returns>
        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern BacnetReturnType BACDEL_Create_Directory();

        /// <summary>
        /// API to Check wheteher Object type is supported or not.
        /// If " i32DevId " is -1, check the support in stack else check in
        ///corresponding device.
        /// </summary>
        /// <param name="devID">indicates device id.</param>
        /// <param name="eObjectType">type of object.</param>
        /// <returns>true if supported else false.</returns>
        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern bool BACDEL_Check_Object_Support(int devID, BacnetObjectType objectType);

        /// <summary>
        /// API to set device APDU timeout property value.
        /// </summary>
        /// <param name="devID">Device Id.</param>
        /// <param name="apduTimeout">APDU timeout value for given device.</param>
        /// <param name="allDevice">Change timeout of all virtual devices if true.</param>
        /// <returns>success or suitable error code.</returns>
        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern BacnetReturnType BACDEL_Set_Device_Apdu_Timeout(uint devID, uint apduTimeout, bool allDevice);

        /// <summary>
        /// API to set number of APDU retries property value of device object.
        /// </summary>
        /// <param name="devID">Device Id.</param>
        /// <param name="apduRetries">APDU retries value for given device.</param>
        /// <param name="allDevice">Change retries of all virtual devices if true.</param>
        /// <returns>success or suitable error code.</returns>
        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern BacnetReturnType BACDEL_Set_Device_Apdu_Retries(uint devID, uint apduRetries, bool allDevice);

        /// <summary>
        ///  API to Get Vendor Specific Parameters for BACnet stack.
        /// </summary>
        /// <param name="vendorID">Vendor ID.</param>
        /// <param name="vendorName">Vendor Name.</param>
        /// <param name="modelName">Model Name.</param>
        /// <param name="firmwareRev">Firmware revision.</param>
        /// <param name="applSoftwareVer">Application Software version.</param>
        /// <param name="location">Location.</param>
        /// <param name="deviceDescription">Device Description.</param>
        /// <param name="objDescription">Object Description.</param>
        /// <param name="profileName">Profile Name.</param>
        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern void BACDEL_Set_Vendor_Specific_Data(ushort vendorID,
                                                                  [MarshalAs(UnmanagedType.LPArray)] byte[] vendorName, [MarshalAs(UnmanagedType.LPArray)] byte[] modelName,
                                                                  [MarshalAs(UnmanagedType.LPArray)] byte[] firmwareRev, [MarshalAs(UnmanagedType.LPArray)] byte[] applSoftwareVer,
                                                                  [MarshalAs(UnmanagedType.LPArray)] byte[] location, [MarshalAs(UnmanagedType.LPArray)] byte[] deviceDescription,
                                                                  [MarshalAs(UnmanagedType.LPArray)] byte[] objDescription, [MarshalAs(UnmanagedType.LPArray)] byte[] profileName,
                                                                  [MarshalAs(UnmanagedType.LPArray)] byte[] serialNumber);

        /// <summary>
        /// API is used to set Actual window size and
        /// proposed window size for segmentation.
        /// </summary>
        /// <param name="actWindowSize">Actual window size</param>
        /// <param name="propsdWindowSize">Proposed window size</param>
        /// <returns>success or suitable error code.</returns>
        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern BacnetReturnType BACDEL_Set_Segmentation_WindowSize(byte actWindowSize, byte propsdWindowSize);

        /// <summary>
        /// API to send Who Is service request.
        /// </summary>
        /// <param name="srcDevID">Source Device ID.</param>
        /// <param name="destDevID">Destination Device ID. </param>
        /// <param name="destinationAdd">Destination Device IP address.</param>
        /// <param name="destTypeFlag"> Flag to check request generated byDevice ID or Device address. </param>
        /// <param name="destType"> Type of destination.</param>
        /// <param name="isNwLayerMsg">Send either who_is or who_is_router_to_network</param>
        /// <param name="networkNo">Network no for who_is_router_to_network.</param>
        /// <param name="whoIsData">Structure pointer to access the data of Who-Is service request</param>
        /// <param name="tokenID">Token ID will be returned</param>
        /// <returns>success or suitable error code.</returns>
        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern BacnetReturnType BACDEL_Send_Who_Is(uint srcDevId, uint destDevId, IntPtr destinationAdd, bool destTypeFlag,
                                                                 DestinationType destType, bool isNwLayerMsg, ushort networkNo, IntPtr whoIsData,
                                                                 IntPtr tokenId);

        /// <summary>
        /// API to send I-Am service request.
        /// </summary>
        /// <param name="srcDevID">Source Device ID. </param>
        /// <param name="destDevID">Destination Device ID. </param>
        /// <param name="destinationAdd">Destination Device IP address.</param>
        /// <param name="destTypeFlag">Flag to check request generated by Device ID or Device address. </param>
        /// <param name="destType">Type of destination.</param>
        /// <param name="isNwLayerMsg">Send either i_am or i_am_router_to_network.</param>
        /// <param name="tokenID">Token ID will be returnedt</param>
        /// <returns>success or suitable error code.</returns>
        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern BacnetReturnType BACDEL_Send_I_Am(uint srcDevID, uint destDevID, IntPtr destinationAdd,
                                                               bool destTypeFlag, DestinationType destType, bool isNwLayerMsg, IntPtr tokenID);

        /// <summary>
        /// API to send WHO HAS request.
        /// </summary>
        /// <param name="srcDevID">Source Device ID. </param>
        /// <param name="destDevID">Destination Device ID. </param>
        /// <param name="destinationAdd">Destination Device IP address.</param>
        /// <param name="destTypeFlag">Flag to check request generated by Device ID or Device address.</param>
        /// <param name="destType">Type of destination.</param>
        /// <param name="whoHasData">Structure pointer to access the data of WHO-HAS service request</param>
        /// <param name="tokenID">Token ID will be returned</param>
        /// <returns>success or suitable error code.</returns>
        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern BacnetReturnType BACDEL_Send_Who_Has(uint srcDevID, uint destDevID, IntPtr destinationAdd,
                                                                  bool destTypeFlag, DestinationType destType, IntPtr whoHasData,
                                                                  IntPtr tokenID);

        /// <summary>
        /// API to send I HAVE request.
        /// </summary>
        /// <param name="srcDevID">Source Device ID. </param>
        /// <param name="destDevID">Destination Device ID. </param>
        /// <param name="destinationAdd">Destination Device IP address.</param>
        /// <param name="destTypeFlag">Flag to check request generated by Device ID or Device address.</param>
        /// <param name="destType">Type of destination.</param>
        /// <param name="iHaveData">Structure pointer to access the data of I HAVE service request</param>
        /// <param name="tokenId">Token ID will be returned</param>
        /// <returns>success or suitable error code.</returns>
        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern BacnetReturnType BACDEL_Send_I_Have(uint srcDevID, uint destDevID, IntPtr destinationAdd,
                                                                 bool destTypeFlag, DestinationType destType, IntPtr iHaveData,
                                                                 IntPtr tokenId);

        /// <summary>
        ///  API to send Read Property request.
        /// </summary>
        /// <param name="srcDevId">Source Device ID. </param>
        /// <param name="destDevId">Destination Device ID. </param>
        /// <param name="destinationAdd">Destination Device IP address.</param>
        /// <param name="destTypeFlag">Flag to check request generated by Device ID or Device address.</param>
        /// <param name="rpData">Structure pointer to access the data of Read Property service request</param>
        /// <param name="fpCallbackFun">Callback function pointer.</param>
        /// <param name="tokenId">Token ID will be returned</param>
        /// <returns> success or suitable error code.</returns>
        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern BacnetReturnType BACDEL_Send_RP(uint srcDevID,
                                                             uint destDevID,
                                                             IntPtr destinationAdd,
                                                             bool destTypeFlag,
                                                             IntPtr rpData,
                                                             App_Callback_Interface_t fpCallbackFun,
                                                             IntPtr tokenID);

        /// <summary>
        ///  API to send Read Property Multiple request.
        /// </summary>
        /// <param name="srcDevId">Source Device ID. </param>
        /// <param name="destDevId">Destination Device ID. </param>
        /// <param name="destinationAdd">Destination Device IP address.</param>
        /// <param name="destTypeFlag">Flag to check request generated by Device ID or Device address.</param>
        /// <param name="rpmData">Structure pointer to access the data of Read Property Multiple service request</param>
        /// <param name="fpCallbackFun">Callback function pointer.</param>
        /// <param name="tokenId">Token ID will be returned</param>
        /// <returns> success or suitable error code.</returns>
        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern BacnetReturnType BACDEL_Send_RPM(uint srcDevId, uint destDevId,
                                                              IntPtr destinationAdd,
                                                              bool destTypeFlag, IntPtr rpmData,
                                                              App_Callback_Interface_t fpCallbackFun, IntPtr tokenId
                                                              );

        /// <summary>
        ///  API to send Write Property request.
        /// </summary>
        /// <param name="srcDevId">Source Device ID. </param>
        /// <param name="destDevId">Destination Device ID. </param>
        /// <param name="destinationAdd">Destination Device IP address.</param>
        /// <param name="destTypeFlag">Flag to check request generated by Device ID or Device address. </param>
        /// <param name="wpData">Structure pointer to access the data of Write Property service request</param>
        /// <param name="fpCallbackFun">Callback function pointer.</param>
        /// <param name="tokenId">Token ID will be returned</param>
        /// <returns> success or suitable error code.</returns>
        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern BacnetReturnType BACDEL_Send_WP(uint srcDevID,
                                                             uint destDevID,
                                                             IntPtr destinationAdd,
                                                             bool destTypeFlag,
                                                             IntPtr wpData,
                                                             App_Callback_Interface_t fpCallbackFun,
                                                             IntPtr tokenID);

        /// <summary>
        /// API to send Write Property Multiple request.
        /// </summary>
        /// <param name="srcDevId">Source Device ID. </param>
        /// <param name="destDevId">Destination Device ID. </param>
        /// <param name="destinationAdd">Destination Device IP address.</param>
        /// <param name="destTypeFlag"></param>
        /// <param name="wpmData">Flag to check request generated by Device ID or Device address. </param>
        /// <param name="tokenId">Token ID will be returned</param>
        /// <returns>success or suitable error code.</returns>
        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern BacnetReturnType BACDEL_Send_WPM(uint srcDevID, uint destDevID, IntPtr destinationAdd, bool destTypeFlag,
                                                              IntPtr wpmData, App_Callback_Interface_t fpCallbackFun, IntPtr tokenID);

        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern BacnetReturnType BACDEL_Send_PT(uint srcDevId, uint destDevId, IntPtr destinationAdd,
                                                             bool bDestTypeFlag, DestinationType eDestType, IntPtr PTData,
                                                             App_Callback_Interface_t fpCallbackFun, IntPtr tokenID);

        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern BacnetReturnType BACDEL_Set_Max_Limits(IntPtr pstStackLimit, IntPtr peFirstFailedLimit, BacnetMaxlimitParameters eLimitType, bool bSetSpecifiedLimit);

        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern void BACDEL_Get_Dynamic_Device_Address_Binding(IntPtr bacnetAddrBinding);

        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern void BACDEL_Free_Dynamic_Device_Address_Binding(IntPtr bacnetAddrBinding);

        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern BacnetReturnType BACDEL_Delete_Dynamic_Device_Address_Binding(int deviceID, IntPtr devAddress);

        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern BacnetReturnType BACDEL_Enable_BBMD();

        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern BacnetReturnType BACDEL_Disable_BBMD();

        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern InitiatorResponse BACDEL_Generate_BBMD_Request(IntPtr pstServiceArgs, App_Callback_Interface_t fpFunctionPtr);

        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern void BACDEL_Get_Register_FD_List(IntPtr pstRegFdData);

        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern void BACDEL_Free_Register_FD_List(IntPtr pstRegFdData);

        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern BacnetReturnType BACDEL_Send_RR(uint srcDevId, uint destDevId, IntPtr destinationAdd, bool destTypeFlag, IntPtr rrData, App_Callback_Interface_t fpCallbackFun, IntPtr tokenId);

        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern BacnetReturnType BACDEL_Send_Create_Object(uint srcDevId, uint destDevId, IntPtr destinationAdd, bool destTypeFlag, IntPtr createObjData, App_Callback_Interface_t fpCallbackFun, IntPtr tokenId);

        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern BacnetReturnType BACDEL_Send_Delete_Object(uint srcDevId, uint destDevId, IntPtr destinationAdd, bool destTypeFlag, IntPtr deleteObjData, App_Callback_Interface_t fpCallbackFun, IntPtr tokenId);

        /**
        *
        * DESCRIPTION
        * API to check whether BBMD is enabled or disabled in stack.
        *
        * @param [in] void/nothing.
        * @return bool [out] true if enabled, else false.
        *
        */
        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern byte BACDEL_Get_BBMD_Status();

        /**
        *
        * DESCRIPTION
        * API to check whether FD is enabled or disabled in stack.
        *
        * @param [in] void/nothing.
        * @return bool [out] true if enabled, else false.
        *
        */
        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern byte BACDEL_Get_FD_Status();

        /// <summary>                                                          
        /// API to send Subscribe COV request.
        /// </summary>
        /// <param name= u32SrcDevId> Source Device ID.</param>			 
        /// <param name= u32DestDevId>Destination Device ID.</param>	 
        /// <param name= pstDestinationAdd>Destination Device IP address.</param>	
        /// <param name= bDestTypeFlag>Flag to check request generated by Device ID or Device address.</param>
        /// <param name= pstSubCOVData>Structure pointer to access the data of Subscribe COV service Request</param>  		
        /// <param name= fpCallbackFun>Callback function pointer.</param>		
        /// <param name= pi32TokenId>Token ID will be returned</param>		
        /// <returns>success or suitable error code.</returns>
        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern BacnetReturnType BACDEL_Send_Subscribe_Cov(uint u32SrcDevId, uint u32DestDevId, IntPtr pstDestinationAdd, bool bDestTypeFlag, IntPtr pstSubCOVData, App_Callback_Interface_t fpCallbackFun, IntPtr pi32TokenId);

        /// <summary>  
        ///  This API is used by application to provide data of monitored property valuefor external functionalities like alarms and trending.
        /// </summary>
        /// <param name= u32SourDevId> Source device ID.</param>
        /// <param name= pstMonitoredValue> Monitored property value.</param>
        ///<param name= pstExternalData> Indicates value is to be used for alarm or trending.</param>
        ///<returns> success or suitable error code.</returns>
        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern BacnetReturnType BACDEL_Set_MonitoredValues_For_External_Functionality(uint u32SrcDevId, IntPtr pstMonitoredValue, IntPtr pstExternalData);

        /// <summary>                                                          
        /// PI to set lifetime parameter for cov-a subscriptions sent internally from stack.
        /// </summary>
        /// <param name= u32LifeTime> a-side cov subscriptions lifetime.</param>			 
        /// <returns>success or suitable error code.</returns>
        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern BacnetReturnType BACDEL_Update_COVA_Subscription_LifeTime(uint lifeTime);

        /// <summary>  
        /// API to register internal callback functions from stack.
        /// </summary>
        /// <param name= fpFunction> The Callback function pointer of applications.</param>
        ///<returns> void/nothing.</returns>
        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern void BACDEL_AppLayer_CallBack_Register(App_Interface_t fpFunction);

        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern void BACDEL_Free_Object_Property(BacnetDataType eDataType, IntPtr pvData);

        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern void BACDEL_Set_Get_Property_Wp_Callback(BacnetObjectType objectType, BacnetPropertyID propertyId, IntPtr configType, bool readOrWrite);

        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern void BACDEL_Set_Time_Delay(bool bIsAsideDelay, bool bIsBsideDelay, bool bIsAllDelay, uint u32AsideTimeDelay, uint u32BsideTimeDelay, uint u32AllTimeDelay);

        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern BacnetReturnType BACDEL_Set_Virtual_Network_No(uint u16NetworkNo);

        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern BacnetReturnType BACDEL_Set_Local_Network_No(uint u16NetworkNo);

        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern void BACDEL_Get_BDT_List(IntPtr ptrBdtDataList);

        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern void BACDEL_Get_FDT_List(IntPtr ptrFdtDataList);

        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern void BACDEL_Free_BDT_List(IntPtr ptrBdtDataList);

        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern void BACDEL_Free_FDT_List(IntPtr ptrFdtDataList);

        [DllImport(BACnetStackDllPath, CallingConvention = CallingConvention.Cdecl)]
        internal static extern BacnetReturnType BACDEL_Set_BDT_List(IntPtr ptrWriteBdtList);
    }
}