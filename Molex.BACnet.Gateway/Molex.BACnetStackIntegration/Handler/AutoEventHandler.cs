﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.Handler
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Response.AutoResponses;
using System;

namespace Molex.BACnetStackIntegration.Handler
{
    public static class AutoEventHandler
    {
        public static Action<AutoResponse> OnMessage { get; set; }
    }
}
