﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.Handler
/// Class: <ClassName>
/// Description: <Description>
/// Date: 2017-02-03 22:30

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Response.AutoResponses;
using System;

namespace Molex.BACnetStackIntegration.Handler
{
    public static class StackInternalNotificationHandler
    {
        public static Action<AutoResponse> OnMessage { get; set; }
    }
}
