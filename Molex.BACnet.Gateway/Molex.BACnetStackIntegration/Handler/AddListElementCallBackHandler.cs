﻿using Molex.StackDataObjects.Response.AutoResponses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnetStackIntegration.Handler
{
    public class AddListElementCallBackHandler
    {
        public static Action<AutoResponse> OnMessage { get; set; }
    }
}
