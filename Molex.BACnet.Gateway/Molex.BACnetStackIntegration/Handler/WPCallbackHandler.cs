﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.Handler
/// Class: <ClassName>
/// Description: <Description>
/// Date: 2017-05-17 13.00

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Response.AutoResponses;
using System;

namespace Molex.BACnetStackIntegration.Handler
{
    public static class WPCallbackHandler
    {
        public static Action<AutoResponse> OnMessage { get; set; }
    }
}
