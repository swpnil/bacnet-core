﻿using Molex.StackDataObjects.Response.InternalResponses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molex.BACnetStackIntegration.Handler
{
    public class ListElementCallBackHandler
    {
        public static Action<InternalResponse> OnMessage { get; set; }
    }
}
