﻿using Molex.StackDataObjects.Response.AutoResponses;
using System;

namespace Molex.BACnetStackIntegration.Handler
{
    public static class DeviceCommunicationControlCallbackHandler
    {
        public static Action<AutoResponse> OnMessage { get; set; }
    }
}
