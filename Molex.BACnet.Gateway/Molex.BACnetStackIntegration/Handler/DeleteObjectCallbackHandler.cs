﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.Handler
/// Class: <ClassName>
/// Description: <Description>
/// Date: 2017-05-19 11:00 a.m

/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Response.AutoResponses;
using System;

namespace Molex.BACnetStackIntegration.Handler
{
    public static class DeleteObjectCallbackHandler
    {
        public static Action<AutoResponse> OnMessage { get; set; }
    }
}
