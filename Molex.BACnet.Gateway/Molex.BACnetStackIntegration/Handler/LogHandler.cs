﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.Handler
/// Class: <ClassName>
/// Description: <Description>


using Molex.BACnetStackIntegration.Log;
/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using System;

namespace Molex.BACnetStackIntegration.Handler
{
    public static class LogHandler
    {
        public static Action<string> OnMessage { get; set; }
        public static Action<bool, Exception, string, DateTime> OnLogDataMessage { get; set; }
    }
}
