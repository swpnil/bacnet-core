﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.StackStructures
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.BACnetStackIntegration.Stack.Wrapper;
using Molex.StackDataObjects.Constants;
using System;
using System.Runtime.InteropServices;

namespace Molex.BACnetStackIntegration.StackStructures
{
    /****************************************************************************
     **** BACnet Service Structures
     ****************************************************************************/

    /// <summary>
    /// /** Structure to save Initiator response */
    /// Name in Stack Application: initiator_response_t
    /// </summary>
    internal struct InitiatorResponse
    {
        public BacnetReturnType m_eRetType;
        public BacnetReturnType m_eErrorCode;
        public int m_i32TokenID;
    }

    /// <summary>
    /// /** Structure to access the data of Read Property Request */
    /// Name in Stack Application: rp_request_t
    /// </summary>
    internal struct RpRequest
    {
        public byte arrayIndexPresent;
        public BacnetObjectType objectType;
        public uint objectInstance;
        public BacnetPropertyID objectProperty;
        public uint arrayIndex;
    }

    /// <summary>
    /// /** Structure to access the data of Read Property Response */
    /// Name in Stack Application: rp_response_t
    /// </summary>
    internal struct RpResponse
    {
        public byte arrayIndexPresent;
        public BacnetObjectType objectType;
        public uint objectInstance;
        public BacnetPropertyID objectProperty;
        public uint arrayIndex;
        public BacnetDataType dataType;
        public IntPtr pvPropVal;
    }

    /// <summary>
    /// /** Structure to access the data of Write Property Request */
    /// Name in Stack Application: wp_request_t
    /// </summary>
    internal struct WpRequest
    {
        public byte arrayIndexPresent;
        public BacnetObjectType objectType;
        public uint objectInstance;
        public BacnetPropertyID objectProperty;
        public uint arrayIndex;
        public uint priority;
        public BacnetDataType dataType;
        public IntPtr pvPropVal;
    }

    /// <summary>
    /// /** Structure to access the data of Write Property Request */
    /// Name in Stack Application: BACDEL_AppLayer_CallBack_Register ;
    /// </summary>
    internal struct ListElementRequest
    {
        public uint arrayIndexPresent;
        public BacnetObjectType objectType;
        public uint objectInstance;
        public BacnetPropertyID objectProperty;
        public uint arrayIndex;
        //#ifdef OLD_RP_WP_INTERFACE
        //BACNET_PROPERTY_VALUE m_stPropertyValue;
        //#endif
        //#ifdef NEW_RP_WP_INTERFACE
        public BacnetDataType dataType;
        public IntPtr pvPropVal;
        //#endif
    }

    
    internal struct AddListElementResponse
    {
        public uint deviceId;
        public uint objectId;
        public BacnetObjectType objectType;
        public BacnetCallbackType callbackType;
        public BacnetCallbackReason callbackReason;
        public uint dataValue;
        public IntPtr pvPropVal;
    }
    /// <summary>
    /// /** Structure to access the data of Write Property Multiple Request */
    /// Name in Stack Application: wpm_request_t
    /// </summary>
    internal struct WpmRequest
    {
        public byte arrayIndexPresent;
        public BacnetObjectType objectType;
        public uint objectInstance;
        public BacnetPropertyID objectProperty;
        public uint arrayIndex;
        public uint priority;
        public BacnetDataType dataType;
        public IntPtr pvPropVal;
        public IntPtr WPMNextElem;
    }

    /// <summary>
    /// /** Structure to access the data of Write Property Multiple Error Response */
    /// Name in Stack Application: error_wpm_response_t
    /// </summary>
    internal struct ErrorWpmResponse
    {
        public BacnetErrorClass errorClass;
        public BacnetErrorCode errorCode;
        public BacnetObjectType objectType;
        public uint objectInstance;
        public BacnetPropertyID objectProperty;
        public uint arrayIndex;
        public byte arrayIndexPresent;
    }

    /// <summary>
    /// /** Structure to access the data of Read Property Multiple Request */
    /// Name in Stack Application: rpm_request_t
    /// </summary>
    internal struct RpmRequest
    {
        public byte arrayIndexPresent;
        public BacnetObjectType objectType;
        public uint objectInstance;
        public BacnetPropertyID objectProperty;
        public uint arrayIndex;
        public IntPtr rpmNextElem;
    }

    /// <summary>
    /// /** Structure to access the data of Read Property Multiple Response */
    /// Name in Stack Application: rpm_response_t
    /// </summary>
    internal struct RpmResponse
    {
        public byte arrayIndexPresent;
        public BacnetErrorClass errorClass;
        public BacnetErrorCode errorCode;
        public BacnetObjectType objectType;
        public uint objectInstance;
        public BacnetPropertyID objectProperty;
        public uint arrayIndex;
        public BacnetDataType dataType;
        public IntPtr pvPropVal;
        public IntPtr rpmNextElem;
    }

    /// <summary>
    /// /** Structure to access the data of I-HAVE service */
    /// Name in Stack Application: dob_i_have_t
    /// </summary>
    internal struct DobIHave
    {
        public uint deviceInstance;
        public BacnetObjectType objectType;
        public uint objectInstance;
        public BacnetCharStr stObjName;
        public IntPtr next;
    }

    /// <summary>
    /// /** Structure to access the data of WHO-HAS service */
    /// Name in Stack Application: dob_who_has_t
    /// </summary>
    internal struct DobWhoHas
    {
        public byte notificationType;

        public struct DevRange
        {
            public int devRangeLowLimit;
            public int devRangeHighLimit;
        }

        public DevRange devRange;

        /// <summary>
        ///  Union m_uObject in Stack has been declared as byte[] dobWhoHasUnion here. This field may contain any one of below structures.
        ///  DobWhoHasUnionObjectID, DobWhoHasUnionCharStr
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.U1, SizeConst = BacDelPropertyDef.DobWhoHasUnionSize)]
        public byte[] dobWhoHasUnion;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.DobWhoHasUnionSize)]
    internal struct DobWhoHasUnionObjectID
    {
        public struct ObjectId
        {
            public BacnetObjectType objectType;
            public uint objectInstance;
        }

        public ObjectId objectID;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.DobWhoHasUnionSize)]
    internal struct DobWhoHasUnionCharStr
    {
        public BacnetCharStr bacnetCharStr;
    }

    /// <summary>
    /// bacDELPropertyDef class file having same structure name as m_stObjectId.
    /// Renamed as BacDelApiObjectID to resolve conflict.
    /// Name in Stack Application: m_stObjectId
    /// </summary>
    internal struct BacDelApiObjectID
    {
        public BacnetObjectType objectType;
        public uint objectInstance;
    }

    /// <summary>
    /// /** Structure to access the data of I-AM service */
    /// Name in Stack Application: ddb_i_am_t
    /// </summary>
    internal struct DdbIam
    {
        public BacnetSegmentation segmentationSupported;
        public ushort maxAPDUlengthAccepted;
        public uint deviceInstance;
        public uint vendorID;
        public IntPtr next;
    }

    /// <summary>
    /// /** Structure to access the data of WHO-IS service */
    /// Name in Stack Application: ddb_who_is_t
    /// </summary>
    internal struct DdbWhoIs
    {
        public int devRangeLowLimit;
        public int devRangeHighLimit;
    }

    /// <summary>
    /// /** Structure to access data of private transfer request */
    /// Name in Stack App: pt_request_t
    /// </summary>

    internal struct PtRequest
    {
        public uint vendorID;
        public uint serviceNumber;
        public uint dataLength;

        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.I1, SizeConst = BacDelStackConfig.MaxPtSerData)]
        public sbyte[] serviceParameters;

        /* For bool datatype we have to explicitly mention to allocate 1 byte to avoid packing issue.*/

        [MarshalAs(UnmanagedType.I1)]
        public bool isConfirmedRequest;

        /* For bool datatype we have to explicitly mention to allocate 1 byte to avoid packing issue.*/

        [MarshalAs(UnmanagedType.I1)]
        public bool isSerParaPresent;
    }

    /// <summary>
    /// /** Structure to store data of private transfer response */
    /// Name in Stack App: pt_response_t
    /// </summary>
    internal struct PtResponse
    {
        public uint vendorID;
        public uint serviceNumber;
        public uint dataLength;
        public BacnetErrorClass errorClass;
        public BacnetErrorCode errorCode;
        public BacnetPduType pduType;

        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.I1, SizeConst = BacDelStackConfig.MaxPtSerData)]
        public sbyte[] serviceParameters;

        /* For bool datatype we have to explicitly mention to allocate 1 byte to avoid packing issue.*/

        [MarshalAs(UnmanagedType.I1)]
        public bool isSerParaPresent;
    }

    /// <summary>
    ///  /** structure to create linklist of B/IP Addresses. */
    /// Name in Stack Application: BIp_Address_list
    /// </summary> 
    internal struct BacnetIPAddress
    {
        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.I1, SizeConst = BacDelStackConfig.MaxIpLen)]
        public byte[] ipAddress;

        public ushort portNo;
        public byte broadcastMask;
        public IntPtr next;
    }

    /// <summary>
    ///  /** */
    /// Name in Stack Application: m_uPortNo
    /// </summary> 
    internal struct PortNumberUnion
    {
        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.I1, SizeConst = BacDelStackConfig.MaxPortLen)]
        public byte[] portNumberArray;
        public ushort portNumber;
    }
    /// <summary>
    /// /** Structure to access the data of Read BDT response */
    /// Name in Stack Application: readBdt_response_t
    /// </summary> 
    internal struct ReadBdtResponse
    {
        public IntPtr bIpAddress;
        public int stringLen;
        public byte propValue;
    }


    /// <summary>
    /// /** Structure to access the data of Read FDT response */
    /// Name in Stack Application: Fdt_Data_t
    /// </summary>
    internal struct FdtData
    {
        public ushort timeToLive;
        public ushort timeRemaining;
        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.I1, SizeConst = BacDelStackConfig.MaxIpLen)]
        public byte[] ipAddress;
        public ushort portNo;
        public IntPtr next;
    }

    /// <summary>
    /// /** Structure to access the data of Read FDT response */
    /// Name in Stack Application: readFdt_response_t
    /// </summary>
    internal struct ReadFdtResponse
    {
        public IntPtr fdtData;
        public int stringLen;
        public byte propValue;
    };

    /// <summary>
    /// /** Structure to access the data of Write BDT request */
    /// Name in Stack Application: writeBdt_request_t
    /// </summary>
    internal struct WriteBdtRequest
    {
        public BacnetIPAddress bIpAddress;
    };

    /// <summary>
    /// /** Structure to access the data of Delete FDT request */
    /// Name in Stack Application: deleteFdt_request_t
    /// </summary>
    internal struct DeleteFdtRequest
    {
        public BacnetIPAddress bIpAddress;
    };

    /// <summary>
    ///  /** Structure to access the data of Register FD request */
    /// Name in Stack Application: registerFD_request_t
    /// </summary>
    internal struct RegisterFdRequest
    {
        public ushort timeToLive;
    };

    /// <summary>
    /// /** Stores the BACnet data from APDU & beyond */
    /// Name in Stack Application: apdu_packet_t
    /// </summary>
    internal struct ApduPacket
    {
        // Flag to indicate error status i.e. whether error is received from server or client */
        // true if error is received from server
        /* For bool datatype we have to explicitly mention to allocate 1 byte to avoid packing issue.*/

        [MarshalAs(UnmanagedType.I1)]
        public bool errorFromServer;

        // States the service which is requested or for which the response is stored
        public BacnetServicesSupported serviceSupport;

        // PDU type of request
        public BacnetPduType pduType;

        // invoke id for confirmed request
        public int invokeID;

        /// <summary>
        /// Union service_choice_u in Stack has been declared as byte[] serviceChoiceUnion here. This field may contain any one of below structures.
        /// ServiceChoiceUnionErrorResponse, ServiceChoiceUnionErrorWpmResponse, ServiceChoiceUnionWpmRequest, ServiceChoiceUnionRpmResponse, ServiceChoiceUnionRpmRequest,
        /// ServiceChoiceUnionRpRequest, ServiceChoiceUnionRpResponse, ServiceChoiceUnionPtRequest, ServiceChoiceUnionPtResponse,
        /// ServiceChoiceUnionAbortResponse, ServiceChoiceUnionRejectResponse
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.U1, SizeConst = BacDelPropertyDef.serviceChoiceUnionSize)]
        public byte[] serviceChoiceUnion;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.serviceChoiceUnionSize)]
    internal struct ServiceChoiceUnionErrorResponse
    {
        public ErrorResponse errorResponse;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.serviceChoiceUnionSize)]
    internal struct ServiceChoiceUnionAbortResponse
    {
        public AbortResponse abortResponse;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.serviceChoiceUnionSize)]
    internal struct ServiceChoiceUnionRejectResponse
    {
        public RejectResponse rejectResponse;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.serviceChoiceUnionSize)]
    internal struct ServiceChoiceUnionErrorWpmResponse
    {
        public ErrorWpmResponse errorWpmResponse;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.serviceChoiceUnionSize)]
    internal struct ServiceChoiceUnionWpmRequest
    {
        public WpmRequest wpmRequest;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.serviceChoiceUnionSize)]
    internal struct ServiceChoiceUnionRpmResponse
    {
        public RpmResponse rpmResponse;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.serviceChoiceUnionSize)]
    internal struct ServiceChoiceUnionRpmRequest
    {
        public RpmRequest rpmRequest;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.serviceChoiceUnionSize)]
    internal struct ServiceChoiceUnionRpRequest
    {
        public RpRequest rpRequest;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.serviceChoiceUnionSize)]
    internal struct ServiceChoiceUnionRpResponse
    {
        public RpResponse rpResponse;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.serviceChoiceUnionSize)]
    internal struct ServiceChoiceUnionPtRequest
    {
        public PtRequest ptRequest;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.serviceChoiceUnionSize)]
    internal struct ServiceChoiceUnionPtResponse
    {
        public PtResponse ptResponse;
    }

    /// <summary>
    /// /** Stores the BACnet data from NPDU & beyond */
    /// Name in Stack Application: npdu_packet_t
    /// </summary>
    internal struct NpduPacket
    {
        /** Specifies if this is network layer message or contains apdu */
        /* For bool datatype we have to explicitly mention to allocate 1 byte to avoid packing issue.*/

        [MarshalAs(UnmanagedType.I1)]
        public bool isNwLayerMsg;

        /** Specifies the transmit type BROADCAST/UNICAST */
        public DestinationType destinationType;
        /** Specifies network message type */
        public BacnetNetworkMessageType msgType;
        /** Stores the BACnet data from APDU & beyond */
        public ApduPacket stAPDUData;
    }

    /// <summary>
    /// /** Structure to access the data of BACnet IP packet.
    /// This structure has been exposed to external applications
    /// Name in Stack Application: bacnetip_arguments_t
    /// </summary>
    internal struct BacnetIPArguments
    {
        /** Reference number for external application to retrieve response */
        public int tokenID;
        /* Store device-id */
        public uint deviceID;
        /** Determines the packet type UNICAST/BROADCAST/DISTRIBUTED/FORWARD */
        public BacnetBvlcFunction bvlcFunctionType;
        /** BVLC result value for BBMD services */
        public BacnetBvlcResult bvlcResult;
        /** States the priority level of the message NORMAL/URGENT/CRITICAL/LIFE_SAFETY */
        public BacnetMessagePriority priority;
        /** Stores the address from where the response is received */
        public BacnetAddress stDestBACnetAddr;
        /** Stores the BACnet data from NPDU & beyond */
        public NpduPacket stNPDUData;
    }

    /// <summary>
    /// /** Structure to access the data of received Event Notification
    /// Name in Stack Application: event_notification_t
    /// </summary>
    internal struct EventNotification
    {
        public byte m_u8SendSimpleAck;
        public byte m_u8IsConfirmedNotification;
        public byte m_u8Priority;
        public byte m_u8AckRequired;
        public BacnetObjectType m_eInitiatingObjectType;
        public uint m_u32InitiatingObjectId;
        public uint m_u32InitiatingDeviceId;
        public uint m_u32ProcessIdentifier;
        public uint m_u32NotificationClass;
        public BacnetEventType m_eEventType;
        public BacnetNotifyType m_eNotifyType;
        public BacnetEventState m_eFromState;
        public BacnetEventState m_eToState;
        public BacnetCharStr m_CharString;
        public BacnetTimeStamp m_stTimeStamp;
        public IntPtr m_pvEventValues;
        public BacnetNotificationParameters m_stNotifyParameters;
    }

    /// <summary>
    /// /** Structure to access the data of Read Range request
    /// Name in Stack Application: readrange_request_t
    /// </summary>
    internal struct ReadRangeReq
    {
        public byte arrayIndexPresent;
        public BacnetObjectType objectType;
        public uint objectId;
        public BacnetPropertyID propertyId;
        public uint arrayIndex;
        public BacnetReadRange rangeType;
        public RangeUnion rangeUnion;
    }

    /// <summary>
    ///   /** Structure to access the data of Read Range response */
    /// Name in Stack Application: readrange_response_t
    /// </summary>
    internal struct ReadRangeRes
    {
        public byte arrayIndexPresent;
        public BacnetObjectType objectType;
        public uint objectId;
        public BacnetPropertyID propertyId;
        public uint arrayIndex;
        public uint itemCount;
        public uint firstSeqNo;
        public BacnetBitStr resultFlags;
        public uint dataLen;
        public BacnetDataType dataType;
        public IntPtr pvPropVal;
    }


    /// <summary>
    /// /** Union to access the data of Read Range request
    /// Name in Stack Application: m_uRange
    /// </summary>
    [StructLayout(LayoutKind.Explicit)]
    internal struct RangeUnion
    {
        [FieldOffset(0)]
        public ByPosition byPosition;
        [FieldOffset(0)]
        public BySeqNo bySeqNo;
        [FieldOffset(0)]
        public ByTime byTime;
    }

    /// <summary>
    /// /** Structure to access the data of Read Range request By Position
    /// Name in Stack Application: m_stByPosition_t
    /// </summary>
    internal struct ByPosition
    {
        public int count;
        public uint referenceIndex;
    }

    /// <summary>
    /// /** Structure to access the data of Read Range request By Sequences No
    /// Name in Stack Application: m_stBySeqNo_t
    /// </summary>
    internal struct BySeqNo
    {
        public int count;
        public uint referenceIndex;
    }

    /// <summary>
    /// /** Structure to access the data of Read Range request By Time
    /// Name in Stack Application: m_stByTime
    /// </summary>
    internal struct ByTime
    {
        public int count;
        public BacnetDateTime dateTime;
    }

    /// <summary>
    /// /** Structure to access the data of Create Object request */
    /// Name in Stack Application: create_object_req_t
    /// </summary>
    internal struct CreateObjectReq
    {
        public BacnetObjectType objectType;
        public uint objectId;
        public uint devID;
        public bool isObjTypeSpecifier;
        public IntPtr initialValues;
    }

    /// <summary>
    ///  /** Structure to access the data of Create Object response */
    /// Name in Stack Application: create_object_res_t
    /// </summary>
    internal struct CreateObjResponse
    {
        public BacnetObjectType objectType;
        public uint objectId;
    }

    /// <summary>
    ///     /** Structure to access the data of Delete Object request */
    /// Name in Stack Application: delete_object_req_t
    /// </summary>
    internal struct DeleteObjectReq
    {
        public BacnetObjectType objectType;
        public uint deviceID;
        public uint objectId;
    }

    /// <summary>
    /// /** Structure to access the data of COV Subscribe Request */
    /// Name in Stack Application: cov_subscribe_t
    /// </summary>
    internal struct CovSubscribeRequest
    {
        public byte subscribe;
        public byte notificationType;
        public BacnetObjectType monitoredObjectType;
        public uint monitoredObjectInstance;
        public uint processIdentifier;
        public uint lifetime;
    }

    /// <summary>
    /// /** Structure to access the data of COV Notification Request */
    /// Name in Stack Application: cov_notification_t
    /// </summary>
    internal struct CovNotificationRequest
    {
        public byte sendSimpleAck;
        public byte notificationType;
        public BacnetObjectType monitoredObjectType;
        public uint monitoredObjectInstance;
        public uint deviceInstance;
        public uint processIdentifier;
        public uint lifetime;
        public IntPtr pvCOVValue;
        public PropertyValues stCOVValue;
        public float covIncrement;
    }

    /// <summary>
    /// /** structure to provide data for external functionalities */
    /// Name in Stack Application: Ext_Property_Data_t
    /// </summary>
    internal struct ExtPropertyData
    {
        /* flag to indicate PV is updated for alarm */
        [MarshalAs(UnmanagedType.I1)]
        public bool alarmFlag;
        /* flag to indicate PV is updated for trending */
        [MarshalAs(UnmanagedType.I1)]
        public bool trendFlag;
        /* store event entrollment object ID */
        public uint eeObjectID;
        /* store trend log object ID */
        public uint tlObjectID;
        /* external status flags for alarms */
        public BacnetBitStr alarmsStatusFlags;
        /* external status flags for trending */
        public BacnetBitStr trendsStatusFlags;

    }


    /// <summary>
    /// /** Structure to access the data of time synchronize request */
    /// Name in Stack Application: time_sync_request_t
    /// </summary>
    /// <CreatedBy>prasad.joshi</CreatedBy><TimeStamp>8/28/201712:56 PM</TimeStamp>
    internal struct TimeSyncRequest
    {
        public BacnetDate date;
        public BacnetTime time;
    }

    /// <summary>
    /// /** Structure to access the data of time synchronize request */
    /// Name in Stack Application: utc_time_sync_request_t
    /// </summary>
    /// <CreatedBy>prasad.joshi</CreatedBy><TimeStamp>8/28/201712:56 PM</TimeStamp>
    internal struct UTCTimeSyncRequest
    {
        public BacnetDate utcDate;
        public BacnetTime utcTime;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <CreatedBy>hp</CreatedBy>
    /// <TimeStamp>8/24/20171:12 PM</TimeStamp>
    internal struct ReInitDeviceRequest
    {
        public BacnetReinitializedState ReinitializeState;
        public BacnetCharStr devicePassword;
    }

    /// <summary>
    /// /** Structure to access the data of time synchronize request */
    /// Name in Stack Application: dcc_request_t
    /// </summary>
    /// <CreatedBy>Amol Kulkarni</CreatedBy><TimeStamp>8/31/20177:35 PM</TimeStamp>
    internal struct DCCRequest
    {
        public uint TimeDuration;
        public BacnetCommunicationState DccState;
        public BacnetCharStr Password;
    }
}
