﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.Stack.Wrapper
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Constants;
using System;
using System.Runtime.InteropServices;

namespace Molex.BACnetStackIntegration.Stack.Wrapper
{
    /// <summary>
    /// RPM property list with count
    /// Name in Stack Application: property_list_t
    /// </summary>
    internal struct PropertyList
    {
        public IntPtr list;
        public uint count;
    }

    /// <summary>
    /// RPM special property list
    /// Name in Stack Application: special_property_list_t
    /// </summary>
    internal struct SpecialPropertyList
    {
        public PropertyList required;
        public PropertyList optional;
        public PropertyList proprietary;
    }

    /// <summary>
    /// This structure defines the property datatype of type bool.
    /// Name in Stack Application: Pr_BACnetBool_t
    /// </summary>
    internal struct PrBacnetBool
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        public int activeCOV;
        /* For bool datatype we have to explicitly mention to allocate 1 byte to avoid packing issue.*/

        [MarshalAs(UnmanagedType.I1)]
        public bool val;
    }

    /// <summary>
    /// This structure defines the property datatype of type signed integer.
    /// Name in Stack Application: Pr_BACnetSigned32_t
    /// </summary>
    internal struct PrBacnetSigned32
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        public int activeCOV;
        /* rely on OS, if there is one */
        public int val;
    }

    /// <summary>
    /// This structure defines the property datatype of type Unsigned 8.
    /// Name in Stack Application: Pr_BACnetUnsigned8_t
    /// </summary>
    internal struct PrBacnetUnsigned8
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        // public int activeCOV;
        /** Value for varible of this type */
        public byte val;
    }

    /// <summary>
    /// This structure defines the property datatype of type Unsigned 16.
    /// Name in Stack Application: Pr_BACnetUnsigned16_t
    /// </summary>
    internal struct PrBacnetUnsigned16
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /** Value for data */
        public ushort val;
    }

    /// <summary>
    /// This structure defines the property datatype of type unsigned integer
    /// Name in Stack Application: Pr_BACnetUnsigned32_t
    /// </summary>
    internal struct PrBacnetUnsigned32
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        public int activeCOV;
        /** data value */
        public uint val;
    }

    /// <summary>
    /// This structure defines the property datatype of type enum.
    /// Name in Stack Application: Pr_BACnetEnum_t
    /// </summary>
    internal struct PrBacnetEnum
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        public int activeCOV;
        /** enum data value */
        public int val;
    }

    /// <summary>
    /// This structure defines the property datatype of type real / float.
    /// Name in Stack Application: Pr_BACnetReal_t
    /// </summary>
    internal struct PrBacnetReal
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        public int activeCOV;
        /* rely on OS, if there is one */
        public float val;
    }

    /// <summary>
    /// This structure defines the property datatype of type double.
    /// Name in Stack Application: Pr_BACnetDouble_t
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    internal struct PrBacnetDouble
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        public int activeCOV;
        /* rely on OS, if there is one */
        public double val;
    }

    /// <summary>
    /// This structure defines the property datatype of type Character String.
    /// Name in Stack Application: Pr_BACnetCharStr_t
    /// </summary>
    internal struct PrBacnetCharStr
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        public int m_i32ActiveCOV;
        /** struct to save string, its length & encoding */
        public BacnetCharStr stCharString;
    }

    /// <summary>
    /// This structure defines the property datatype of type BACnet Optional Character String.
    /// Name in Stack Application: Pr_BACnetOptCharStr_t
    /// </summary>
    internal struct PrBacnetOptCharStr
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /** tag type either null or char_string */
        /* For bool datatype we have to explicitly mention to allocate 1 byte to avoid packing issue.*/

        [MarshalAs(UnmanagedType.I1)]
        public bool tagValue;

        /** struct to save string, its length & encoding */
        public BacnetCharStr stCharString;
    }

    /// <summary>
    /// This structure defines the property datatype of type Octet String.
    /// Name in Stack Application: Pr_BACnetOctetStr_t
    /// </summary>
    internal struct PrBacnetOctetStr
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        public BacnetOctetStr stOctetString;
    }

    /// <summary>
    /// This structure defines the property datatype of type Bit String.- contains single byte.
    /// Name in Stack Application: Pr_BACnetBitStr_t
    /// </summary>
    internal struct PrBacnetBitStr
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        public int activeCOV;
        /* Bit string value */
        public BacnetBitStr stBitStr;
    }

    /// <summary>
    /// This structure defines the property datatype of type BITSTRING.- contains multiple bytes.
    /// Name in Stack Application: Pr_BACnetBITStr_t
    /// </summary>
    internal struct PrBacnetBITStr
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        public int activeCOV;
        /* bitstring value */
        public BacnetBITStr stBitString;
    }

    internal class BacDelPropertyDef
    {
        public const int PrListOfBacnetSecurityKeySetSize = 2;
        public const int PrBacnetEventTimeStampTypeSize = 3;
        public const int PrListOfBacnetDailyScheduleTimeValueSize = 7;
        public const int PrBacnetEventMsgTextCharStrSize = 3;
        public const int PrBacnetNotifyPriorityUintSize = 3;
        public const int BacnetNotifiParamsChngOfStateSize = 540;
        public const int DobWhoHasUnionSize = 264;
        //Changed value from 280 to 868 as stack accepting this value
        //public const int serviceChoiceUnionSize = 280;
        public const int serviceChoiceUnionSize = 868;
        public const int BacnetPropertyValueUnionSize = 264;
        public const int BacnetTimeStampUnionSize = 12;
        public const int PrBacnetPriorityArrayUnionSize = 64;
        public const int BacnetArrayIndexUnionSize = 256;
        public const int BacnetRecipientUnionSize = 16;
        public const int PropertyValueUnionSize = 264;
        public const int ParametersUnionSize = 64;
        public const int BacnetEventParameterUnionSize = 300;
        public const int ChangeOfValueNPUnionSize = 12;
        public const int ChangeOfStatusFlagsNPUnionSize = 264;
        public const int BacnetNotificationParametersUnionSize = 536;
        public const int LogDatumUnionSize = 272;
        public const int BacnetLogDataUnionSize = 272;
        public const int EventlogDatamUnionSize = 864;
        public const int CovcriteriaUnionSize = 20;
        public const int BacnetObjTypesSuppUnionSize = 7;
        public const int BacnetObjTypesSuppStructSize = 8;
        public const int BacnetServiceSupportedUnionSize = 6;
        public const int BacnetServiceSupportedStructSize = 8;
        public const int BacnetRecipientStructSize = 20;
        public const int BacnetPropertyStatesStructSize = 12;
        public const int BacnetSpecialEventUnionSize = 304;
        public const int MaxPriorityArrayCount = 16;
        public const int ChannelValueUnionSize = 272;
    }

    /// <summary>
    /// This structure defines the property datatype of type BACnetObjectIdentifier.
    /// Name in Stack Application: Pr_BACnetObjId_t
    /// </summary>
    internal struct PrBacnetObjID
    {
        ///<summary>Access Sepcifier </summary>
        public PropAccessType accessType;

        ///<summary> COV Status Specifier </summary>
        //public int activeCOV;

        ///<summary> Type of Object</summary>
        public BacnetObjectType objectType;

        ///<summary> Object Instance</summary>
        public uint objId;
    }

    /// <summary>
    /// This structure defines the property datatype of type time
    /// Name in Stack Application: Pr_BACnetTime_t
    /// </summary>
    internal struct PrBacnetTime
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /* rely on OS, if there is one */
        public BacnetTime timeVal;
    }

    /// <summary>
    /// This structure defines the property datatype of type date.
    /// Name in Stack Application: Pr_BACnetDate_t
    /// </summary>
    internal struct PrBacnetDate
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        // public int activeCOV;
        /* rely on OS, if there is one */
        public BacnetDate dateVal;
    }

    /// <summary>
    /// This structure defines the property datatype of type ABSTRACT-SYNTAX.
    /// -- any primitive datatype; complex types cannot be decoded
    /// Name in Stack Application: AnyValue_t
    /// </summary>
    internal struct AnyValue
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        public int activeCOV;
        public BacnetPropertyValue propValue;
    }

    ///<summary>
    ///   This structure defines the property datatype
    ///   of type DataTime
    ///   - without access type & active cov.
    ///   Name in Stack Application: BACnetDateTime_t
    ///</summary>
    internal struct BacnetDateTime
    {
        /*Time structure*/
        public BacnetTime time;
        /* Date structure */
        public BacnetDate date;
    }

    /// <summary>
    /// This structure defines the property datatype of type DataTime
    /// Name in Stack Application: Pr_BACnetDateTime_t
    /// </summary>
    internal struct PrBacnetDateTime
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        public int activeCOV;
        /** Date time value */
        public BacnetDateTime stDateTime;
    }

    ///<summary>
    ///  This structure defines the property datatype of type DataTime - without access type & active cov.
    ///  Name in Stack Application: BACnetTimeStamp_t
    ///</summary>
    internal struct BacnetTimeStamp
    {
        /*Indicates type of time-stamp*/
        public BacnetTimeStampType timeStampType;
        public TimeStampUnion timeStampUnion;
    }

    [StructLayout(LayoutKind.Explicit)]
    internal struct TimeStampUnion
    {
        /** Time Structure */

        [FieldOffset(0)]
        public BacnetTime time;

        /** Date and Time structure */

        [FieldOffset(0)]
        public BacnetDateTime dateTime;

        /** Sequence Number */

        [FieldOffset(0)]
        public uint seqNo;
    }

    /// <summary>
    /// This structure defines the property datatype of type TimeStamp.
    /// Name in Stack Application: Pr_BACnetTimeStamp_t
    /// </summary>
    internal struct PrBacnetTimeStamp
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        public BacnetTimeStamp bacnetTimeStamp;
    }

    /// <summary>
    /// This structure defines the property datatype of type priority array.
    /// Name in Stack Application: Pr_BACnetPriorityArray_t
    /// </summary>
    internal struct PrBacnetPriorityArray
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /** Priority Value type (Enum, Bool, Real..)
        Based on the data type App. Tag will be added */

        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.U1, SizeConst = BacDelStackConfig.MaxPriorityArrayCount)]
        public byte[] tagType;

        /// <summary>
        /// Union m_uValue in Stack has been declared as byte[] PrBacnetPriorityArrayUnion here. This field may contain any one of below structures.
        /// PrBacnetPriorityArrayUnionBool, PrBacnetPriorityArrayUnionBinaryPV, PrBacnetPriorityArrayUnionFloat, PrBacnetPriorityArrayUnionUint,
        /// PrBacnetPriorityArrayUnionInt
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.U1, SizeConst = BacDelPropertyDef.PrBacnetPriorityArrayUnionSize)]
        public byte[] PrBacnetPriorityArrayUnion;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.PrBacnetPriorityArrayUnionSize)]
    internal struct PrBacnetPriorityArrayUnionBool
    {
        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.I1, SizeConst = BacDelStackConfig.MaxPriorityArrayCount)]
        public bool[] Val;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.PrBacnetPriorityArrayUnionSize)]
    internal struct PrBacnetPriorityArrayUnionBinaryPV
    {
        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.U1, SizeConst = BacDelStackConfig.MaxPriorityArrayCount)]
        public BacnetBinaryPV[] Val;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.PrBacnetPriorityArrayUnionSize)]
    internal struct PrBacnetPriorityArrayUnionFloat
    {
        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.R4, SizeConst = BacDelStackConfig.MaxPriorityArrayCount)]
        public float[] Val;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.PrBacnetPriorityArrayUnionSize)]
    internal struct PrBacnetPriorityArrayUnionUint
    {
        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.U4, SizeConst = BacDelStackConfig.MaxPriorityArrayCount)]
        public uint[] Val;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.PrBacnetPriorityArrayUnionSize)]
    internal struct PrBacnetPriorityArrayUnionInt
    {
        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.I4, SizeConst = BacDelStackConfig.MaxPriorityArrayCount)]
        public int[] Val;
    }

    /// <summary>
    /// Linked list of Unsigned / signed data.
    /// Name in Stack Application: ListOfUnsigned_t
    /// </summary>
    internal struct ListOfUnsigned
    {
        public uint value;
        public IntPtr next;
    }

    /// <summary>
    /// This structure defines the property datatype of type list unsigned.
    /// Name in Stack Application: Pr_ListOfUnsigned_t
    /// </summary>
    internal struct PrListOfUnsigned
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        public uint count;
        /** data value */
        public IntPtr unsignVal;
    }

    /// <summary>
    /// Linked list of character string data.
    /// Name in Stack Application: ListOfCharStr_t
    /// </summary>
    internal struct ListOfCharStr
    {
        public BacnetCharStr charStr;
        public IntPtr next;
    }

    /// <summary>
    /// This structure defines the property datatype of type list character string.
    /// Name in Stack Application: Pr_ListOfCharStr_t
    /// </summary>
    internal struct PrListOfCharStr
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        public int activeCOV;
        /** Number of States */
        public uint count;
        /** Value for character string */
        public ListOfCharStr stStringVal;
    }

    /// <summary>
    /// Linked list of optional character string data.either null or char_string.
    /// Name in Stack Application: ListOfOptCharStr_t
    /// </summary>
    internal struct ListOfOptCharStr
    {
        public BacnetCharStr charStr;
        public sbyte appTag;
        public IntPtr next;
    }

    /// <summary>
    /// This structure defines the property datatype of type BACnet Optional Character String.
    /// Name in Stack Application: Pr_ListOfOptCharStr_t
    /// </summary>
    internal struct PrListOfOptCharStr
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /** no of nodes in array */
        public uint count;
        /** struct to save string, its length & encoding */
        public IntPtr optCharStr;
    }

    /// <summary>
    /// Linked list of bit string data.
    /// Name in Stack Application: ListOfBitStr_t
    /// </summary>
    internal struct ListOfBitStr
    {
        public BacnetBITStr stBitString;
        public IntPtr next;
    }

    /// <summary>
    /// This structure defines the property datatype for bit string value object - bitstring array
    /// Name in Stack Application: Pr_ListOfBitStr_t
    /// </summary>
    internal struct PrListOfBitStr
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /** no of bit string count */
        public uint count;
        /**/
        public IntPtr bitString;
    }

    /// <summary>
    /// This structure defines the property Array index, & union for all primitive data types.
    /// Name in Stack Application: bacnetArrayIndex_t
    /// </summary>
    internal struct BacnetArrayIndex
    {
        /** Array index */
        public int arrayIndex;

        /// <summary>
        /// Union m_uValue in Stack has been declared as byte[] bacnetArrayIndexUnion here. This field may contain any one of below structures.
        /// BacnetArrayIndexUnionUint, BacnetArrayIndexUnionInt, BacnetArrayIndexUnionFloat, BacnetArrayIndexUnionBinaryPV, BacnetArrayIndexForDouble,
        /// BacnetArrayIndexUnionCharStr, BacnetArrayIndexUnionBITStr, BacnetArrayIndexUnionOctetStr
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.U1, SizeConst = BacDelPropertyDef.BacnetArrayIndexUnionSize)]
        public byte[] bacnetArrayIndexUnion;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetArrayIndexUnionSize)]
    internal struct BacnetArrayIndexUnionUint
    {
        /** Unsigned value */
        public uint unsignVal;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetArrayIndexUnionSize)]
    internal struct BacnetArrayIndexUnionInt
    {
        /** Signed value */
        public int signVal;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetArrayIndexUnionSize)]
    internal struct BacnetArrayIndexUnionFloat
    {
        /** Real value */
        public float floatVal;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetArrayIndexUnionSize)]
    internal struct BacnetArrayIndexUnionBinaryPV
    {
        /** Enum value */
        public BacnetBinaryPV val;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetArrayIndexUnionSize)]
    internal struct BacnetArrayIndexForDouble
    {
        /** Double value */
        public double doubleVal;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetArrayIndexUnionSize)]
    internal struct BacnetArrayIndexUnionCharStr
    {
        /** Char string value */

        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.I1, SizeConst = BacDelStackConfig.MaxCharacterStringBytes)]
        public sbyte[] charString;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetArrayIndexUnionSize)]
    internal struct BacnetArrayIndexUnionBITStr
    {
        /** Bit string value */
        public BacnetBITStr bitStringVal;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetArrayIndexUnionSize)]
    internal struct BacnetArrayIndexUnionOctetStr
    {
        /** Octet string value */

        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.U1, SizeConst = BacDelStackConfig.MaxOctetStringBytes)]
        public byte[] octetString;
    }

    /// <summary>
    /// Linked list of boolean data.
    /// Name in Stack Application: ListOfBoolen_t
    /// </summary>
    internal struct ListOfBoolen
    {
        /* For bool datatype we have to explicitly mention to allocate 1 byte to avoid packing issue.*/

        [MarshalAs(UnmanagedType.I1)]
        public bool val;

        /** Next element */
        public IntPtr next;
    }

    /// <summary>
    /// This structure defines the property datatype type boolean array.
    /// Name in Stack Application: Pr_ListOfBoolen_t
    /// </summary>
    internal struct PrListOfBoolen
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /** Boolean List count */
        public uint boolCount;
        public IntPtr boolList;
    }

    /// <summary>
    /// This structure defines the property datatype of type enum & enum array with polarity.
    /// Name in Stack Application: Pr_BinaryEnumPV_t
    /// </summary>
    internal struct PrBinaryEnumPV
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        public int activeCOV;
        /** Bool Value */
        public BacnetBinaryPV val;
        /** Next element */
        public IntPtr next;
        /* polarity for Present value in Binary objects */
        public BacnetPolarity pvPolarity;
    }

    /// <summary>
    /// Linked list of enumerated data.
    /// Name in Stack Application: ListOfEnum_t
    /// </summary>
    internal struct ListOfEnum
    {
        /** Enumerated Value */
        public int val;
        /** Next element */
        public IntPtr next;
    }

    /// <summary>
    /// This structure defines the property datatype of type enum array/list.
    /// Name in Stack Application: Pr_ListOfEnum_t
    /// </summary>
    internal struct PrListOfEnum
    {
        public PropAccessType accessType;
        //public int activeCOV;
        /** Enum List count */
        public uint count;
        /** next value */
        public IntPtr enumList;
    }

    /// <summary>
    /// Structure to save ANY data type property values.
    /// Name in Stack Application: BACnetAbstractData_t
    /// </summary>
    internal struct BacnetAbstractData
    {
        /* pointer to save property value */
        public IntPtr constrProp;
        /* property data type */
        public BacnetDataType dataType;
    }

    /// <summary>
    /// DESCRIPTION
    /// Structure to save property values.
    /// Datatype as per standard is BACnetPropertyValue.
    /// Name in Stack Application: property_value_t
    /// </summary>
    internal struct PropertyValue
    {
        public BacnetPropertyID objectProperty;
        public int propertyArrayIndex;
        /* For bool datatype we have to explicitly mention to allocate 1 byte to avoid packing issue.*/

        [MarshalAs(UnmanagedType.I1)]
        public bool isArrayIndxPresent;

        public BacnetDataType dataType;
        public byte priority;
        public IntPtr nextVal;
        public IntPtr propVal;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet object type.
    /// Name in Stack Application: Pr_BACnetObjType_t
    /// </summary>
    internal struct PrBacnetObjType
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /** Bacnet Object type */
        public BacnetObjectType objectType;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet device status.
    /// Name in Stack Application: Pr_BACnetDevStatus_t
    /// </summary>
    internal struct PrBacnetDevStatus
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        public int activeCOV;
        /** Status of device*/
        public BacnetDeviceStatus deviceStatus;
    }

    /// <summary>
    /// This structure defines the property datatype of type backup & restore state.
    /// Name in Stack Application: Pr_BACnetBackupState_t
    /// </summary>
    internal struct PrBacnetBackupState
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /** Backup & Restore state of device */
        public BacnetBackupState eBackupState;
    }

    /// <summary>
    /// This structure defines the property datatype of type engineering units.
    /// Name in Stack Application: Pr_BACnetEnggUnits_t
    /// </summary>
    internal struct PrBacnetEnggUnits
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /** Measurement Unit */
        public BacnetEngineeringUnits enggUnits;
    }

    /// <summary>
    /// This structure defines the property datatype of type BACnet Segementation.
    /// Name in Stack Application: Pr_BACnetSegmentation_t
    /// </summary>
    internal struct PrBacnetSegmentation
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /** Type of segmentation */
        public BacnetSegmentation segmentationSupport;
    }

    /// <summary>
    /// This structure defines the property datatype of type Event state.
    /// Name in Stack Application: Pr_BACnetEventState_t
    /// </summary>
    internal struct PrBacnetEventState
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /* Event state value */
        public BacnetEventState eventState;
    }

    /// <summary>
    /// This structure defines the property datatype of type Reliability.
    /// Name in Stack Application: Pr_BACnetReliability_t
    /// </summary>
    internal struct PrBacnetReliability
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /* Relaiblity value */
        public BacnetReliability reliabilty;
    }

    /// <summary>
    /// This structure defines the property datatype of type binary PV.
    /// Name in Stack Application: Pr_BACnetBinaryPV_t
    /// </summary>
    internal struct PrBacnetBinaryPV
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        public int activeCOV;
        /* Event state value */
        public BacnetBinaryPV binaryPv;
    }

    /// <summary>
    /// This structure defines the property datatype of type Polarity.
    /// Name in Stack Application: Pr_BACnetPolarity_t
    /// </summary>
    internal struct PrBacnetPolarity
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /** polarity value */
        public BacnetPolarity polarity;
    }

    /// <summary>
    /// This structure defines the property datatype of type Notify Type
    /// Name in Stack Application: Pr_BACnetNotifyType_t
    /// </summary>
    internal struct PrBacnetNotifyType
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /* type of notification */
        public BacnetNotifyType notifyType;
    }

    /// <summary>
    /// This structure defines the property datatype of type event type.
    /// Name in Stack Application: Pr_BACnetEventType_t
    /// </summary>
    internal struct PrBacnetEventType
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /** type of event */
        public BacnetEventType eventType;
    }

    /// <summary>
    /// This structure defines the property datatype of type file access method.
    /// Name in Stack Application: Pr_BACnetFileAccessMethod_t
    /// </summary>
    internal struct PrBacnetFileAccessMethod
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /** File Access Method */
        public BacnetFileAccessMethod accessMethod;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet logging type.
    /// Name in Stack Application: Pr_BACnetLoggingType_t
    /// </summary>
    internal struct PrBacnetLoggingType
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        // public int activeCOV;
        /* type of logging */
        public BacnetLoggingType loggingType;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet action.
    /// Name in Stack Application: Pr_BACnetAction_t
    /// </summary>
    internal struct PrBacnetAction
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        // public int activeCOV;
        /** type of action */
        public BacnetAction action;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet life safety mode.
    /// Name in Stack Application: Pr_BACnetLifeSafetyMode_t
    /// </summary>
    internal struct PrBacnetLifeSafetyMode
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /** type of life safety mode */
        public BacnetLifeSafetyMode safetyMode;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet life safety state.
    /// Name in Stack Application: Pr_BACnetLifeSafetyState_t
    /// </summary>
    internal struct PrBacnetLifeSafetyState
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        // public int activeCOV;
        /** type of life safety state */
        public BacnetLifeSafetyState safetyState;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet life safety operation.
    /// Name in Stack Application: Pr_BACnetLifeSafetyOperation_t
    /// </summary>
    internal struct PrBacnetLifeSafetyOperation
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /** type of life safety operation */
        public BacnetLifeSafetyOperation safetyOperation;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet silenced state.
    /// Name in Stack Application: Pr_BACnetSilencedState_t
    /// </summary>
    internal struct PrBacnetSilencedState
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        // public int activeCOV;
        /** type of silenced state */
        public BacnetSilencedState silencedState;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet maintenance.
    /// Name in Stack Application: Pr_BACnetMaintenance_t
    /// </summary>
    internal struct PrBacnetMaintenance
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /** type of maintenance */
        public BacnetMaintenance maintenance;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet program state.
    /// Name in Stack Application: Pr_BACnetProgramState_t
    /// </summary>
    internal struct PrBacnetProgramState
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        // public int activeCOV;
        /** type of program state */
        public BacnetProgramState programState;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet program request.
    /// Name in Stack Application: Pr_BACnetProgramRequest_t
    /// </summary>
    internal struct PrBacnetProgramRequest
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        // public int activeCOV;
        /** type of program request */
        public BacnetProgramRequest programRequest;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet program error.
    /// Name in Stack Application: Pr_BACnetProgramError_t
    /// </summary>
    internal struct PrBacnetProgramError
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        // public int activeCOV;
        /** type of program error */
        public BacnetProgramError programError;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet door value.\
    /// Name in Stack Application: Pr_BACnetDoorValue_t
    /// </summary>
    internal struct PrBacnetDoorValue
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /** type of door value */
        public BacnetDoorValue doorValue;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet door status.
    /// Name in Stack Application: Pr_BACnetDoorStatus_t
    /// </summary>
    internal struct PrBacnetDoorStatus
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /** type of door status */
        public BacnetDoorStatus doorStatus;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet door secured status.
    /// Name in Stack Application: Pr_BACnetDoorSecuredStatus_t
    /// </summary>
    internal struct PrBacnetDoorSecuredStatus
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        // public int activeCOV;
        /** type of door secured status */
        public BacnetDoorSecuredState doorSecuredState;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet lock status.
    /// Name in Stack Application: Pr_BACnetLockStatus_t
    /// </summary>
    internal struct PrBacnetLockStatus
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        // public int activeCOV;
        /** type of lock status */
        public BacnetLockStatus lockStatus;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet door alarm state.
    /// Name in Stack Application: Pr_BACnetDoorAlarmState_t
    /// </summary>
    internal struct PrBacnetDoorAlarmState
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        // public int activeCOV;
        /** type of door alarm state */
        public BacnetDoorAlarmState doorAlarmState;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet access event.
    /// Name in Stack Application: Pr_BACnetAccessEvent_t
    /// </summary>
    internal struct PrBacnetAccessEvent
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        // public int activeCOV;
        /** type of access event */
        public BacnetAccessEvent accessEvent;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet access zone occupancy state.
    /// Name in Stack Application: Pr_BACnetAccessZoneOccState_t
    /// </summary>
    internal struct PrBacnetAccessZoneOccState
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /** type of access zone occupancy state */
        public BacnetAccessZoneOccupancyState occupancyState;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet access passback mode.
    /// Name in Stack Application: Pr_BACnetAccessPassbackMode_t
    /// </summary>
    internal struct PrBacnetAccessPassbackMode
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        // public int activeCOV;
        /** type of access passback mode */
        public BacnetAccessPassbackMode passbackMode;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet access user type.
    /// Name in Stack Application: Pr_BACnetAccessUserType_t
    /// </summary>
    internal struct PrBacnetAccessUserType
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /** type of access user */
        public BacnetAccessUserType userType;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet access credential disable.
    /// Name in Stack Application: Pr_BACnetAccessCredDisable_t
    /// </summary>
    internal struct PrBacnetAccessCredDisable
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /** type of access credential disable value */
        public BacnetAccessCredentialDisable credentialDisable;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet node type.
    /// Name in Stack Application: Pr_BACnetNodeType_t
    /// </summary>
    internal struct PrBacnetNodeType
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /** type of node */
        public BacnetNodeType nodeType;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet security level.
    /// Name in Stack Application: Pr_BACnetSecurityLevel_t
    /// </summary>
    internal struct PrBacnetSecurityLevel
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        // public int activeCOV;
        /** type of security level */
        public BacnetSecurityLevel securityevel;
    }

    /// <summary>
    /// structure for BACnet Shed State
    /// Name in Stack Application: Pr_BACnetShedState_t
    /// </summary>
    internal struct PrBacnetShedState
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        // public int activeCOV;
        /** type of shed level */
        public BacnetShedLevelType shedLevelType;
    }

    /// <summary>
    /// This structure defines the property datatype of type Authorization Mode.
    /// Name in Stack Application: Pr_BACnetAuthorizationMode_t
    /// </summary>
    internal struct PrBacnetAuthorizationMode
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /* BACnet Authentication Status enum */
        public BacnetAuthorizationMode authorizationMode;
    }

    /// <summary>
    /// This structure defines the property datatype of type Authentication Status.
    /// Name in Stack Application: Pr_BACnetAuthenticationStatus_t
    /// </summary>
    internal struct PrBacnetAuthenticationStatus
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /* BACnet Authentication Status enum */
        public BacnetAuthenticationStatus authentiStatus;
    }

    /// <summary>
    /// This structure defines the property datatype of type Event Time Stamp array.
    /// Name in Stack Application: Pr_BACnetEventTimeStamp_t
    /// </summary>
    internal struct PrBacnetEventTimeStamp
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        // public int activeCOV;
        /** indicates type of time-stamp */

        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.LPStruct, SizeConst = BacDelPropertyDef.PrBacnetEventTimeStampTypeSize)]
        public BacnetTimeStampType[] timeStampType;

        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.LPStruct, SizeConst = BacDelPropertyDef.PrBacnetEventTimeStampTypeSize)]
        public TimeStampUnion[] timeStampUnion;
    }

    /// <summary>
    /// This structure defines the property datatype of type array of bacnet event message text.
    /// Name in Stack Application: Pr_BACnetEventMsgText_t
    /// </summary>
    internal struct PrBacnetEventMsgText
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        // public int activeCOV;
        /** array of 3 character strings */

        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.LPStruct, SizeConst = BacDelPropertyDef.PrBacnetEventMsgTextCharStrSize)]
        public BacnetCharStr[] eventMsgText;
    }

    /// <summary>
    /// Linked list of Object Id.
    /// Name in Stack Application: ListOfObjId_t
    /// </summary>
    internal struct ListOfObjId
    {
        /** flag that indicates if object is dynamically created or not */
        /* For bool datatype we have to explicitly mention to allocate 1 byte to avoid packing issue.*/

        [MarshalAs(UnmanagedType.I1)]
        public bool objDynamicallyCreated;

        /** Object Instance */

        public struct ObjectId
        {
            /** Type of Object*/
            public BacnetObjectType objectType;
            /** Object Instance*/
            public uint objId;
        }

        public ObjectId stObjectID;
        /** COV Status Specifier */
        public int activeCOV;
        /** Pointer to List */
        public IntPtr next;
    }

    /// <summary>
    /// This structure defines the property datatype of list / array of BACnet Object Id.
    /// Name in Stack Application: Pr_ListOfObjId_t
    /// </summary>
    internal struct PrListOfObjId
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        public int activeCOV;
        /* Object List count */
        public uint objCount;
        /* Array of object Ids */
        public IntPtr arrayObjID;
    }

    /// <summary>
    /// structure defines the types of services supported.
    /// Name in Stack Application: Bit (BACnetServicesSupported_t)
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetServiceSupportedStructSize)]
    internal struct BacnetServiceSupported
    {
        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.U1, SizeConst = BacDelPropertyDef.BacnetServiceSupportedUnionSize)]
        public byte[] value;
    }

    /// <summary>
    /// This structure defines the property datatype of type object services supported.
    /// Name in Stack Application: Pr_BACnetServicesSupported_t
    /// </summary>
    internal struct PrBacnetServicesSupported
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        public int activeCOV;
        public BacnetServiceSupported serviceSupported;
    }

    /// <summary>
    /// structure defines the types of object supported.
    /// Name in Stack Application: Bit (BACnetObjectTypesSupported_t)
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetObjTypesSuppStructSize)]
    internal struct BacnetObjectTypesSupported
    {
        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.U1, SizeConst = BacDelPropertyDef.BacnetObjTypesSuppUnionSize)]
        public byte[] value;
    }

    /// <summary>
    /// This structure defines the property datatype of type object types supported.
    /// Name in Stack Application: Pr_BACnetObjectTypesSupported_t
    /// </summary>
    internal struct PrBacnetObjectTypesSupported
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        public int activeCOV;
        public BacnetObjectTypesSupported objectTypesSupported;
    }

    /// <summary>
    /// This structure defines the property datatype of type BACnet Address Bind .
    /// Name in Stack Application: Pr_ListOfBACnetAddrBinding_t
    /// </summary>
    internal struct PrListOfBacnetAddrBinding
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /** count for no of nodes in list */
        public uint count;
        /** Address binding data for device */
        public BacnetAddrBinding addBinding;
    }

    /// <summary>
    /// recipient info for COV subscription.
    /// Name in Stack Application: CovRecipientInfo_t
    /// </summary>
    internal struct CovRecipientInfo
    {
        /** If Cancellation/Subscription request */
        public sbyte subscribe;
        /** The time remaining for the subscription to get cancelled */
        public uint timeRemaining;
        /** If Property type to be monitored is Real, COV notification is send
            if the value changes by COVIncrement */
        public float covIncrement;
        /** flag indicates if cov increment was received in subscription or copied
            from cov increment property
            true == from subscription else false */
        /* For bool datatype we have to explicitly mention to allocate 1 byte to avoid packing issue.*/

        [MarshalAs(UnmanagedType.I1)]
        public bool covIncFrmClient;

        /** Notification to be send will be of type confirmed or unconfirmed */
        /* For bool datatype we have to explicitly mention to allocate 1 byte to avoid packing issue.*/

        [MarshalAs(UnmanagedType.I1)]
        public bool issueConfirmedNotification;

        /** The process Id of device to which notification is to be send */
        public uint processId;
        /** Device Id */
        public ObjID objectID;
        /** Address of the registered device */
        public BacnetAddress stAddress;
        /** Next List element */
        public IntPtr next;
    }

    /// <summary>
    /// property info & value for COV subscription.
    /// Name in Stack Application: CovPropElement_t
    /// </summary>
    internal struct CovPropElement
    {
        /** Data Type of property */
        public BacnetDataType dataType;
        /** Monitored Property */
        public BacnetPropertyID propertyID;
        /** indicates if array index is present or absent */
        /* For bool datatype we have to explicitly mention to allocate 1 byte to avoid packing issue.*/

        [MarshalAs(UnmanagedType.I1)]
        public bool arrIndxPresent;

        /** Array index parameter if present */
        public uint propertyArrayIndex;
        /** Value that has to be compared */
        public IntPtr storedValue;
        /** Value that has to be compared */
        public IntPtr currentValue;
        /** Next Element in list */
        public IntPtr next;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet COV subscription.
    /// Name in Stack Application: ListOfBACnetCovSubs_t
    /// </summary>
    internal struct ListOfBacnetCovSubs
    {
        /** Index number */
        public int index;
        /** Object Identifier of monitored property */
        public ObjID objectID;
        /** Property that needs to be subscribed */
        public CovPropElement covPropertyElem;
        /** Stores relevent data of the subscriber */
        public IntPtr subscriberInfo;
        /** Next Element in list */
        public IntPtr next;
    }

    /// <summary>
    /// This structure defines the property datatype of type list of bacnet COV subscription.
    /// Name in Stack Application: Pr_ListOfBACnetCovSubs_t
    /// </summary>
    internal struct PrListOfBacnetCovSubs
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** For Mutex Lock */
        public IntPtr hcovsublistMtxLock; // void pointer
        /* list count */
        public uint count;
        /** COV subscription structure */
        public IntPtr covSubscribe;
    }

    /// <summary>
    ///  list of unsolicited COV subscription.
    ///  Name in Stack Application: covUnsolicitlist_t
    /// </summary>
    internal struct CovUnsolicitlist
    {
        public uint timeRemaining;
        /** pointer to COV subscription list */
        public IntPtr covSubscribe;
        /* Next monitored object */
        public IntPtr nextObject;
    }

    /// <summary>
    /// This structure defines the property datatype BACnet VT Class.
    /// Name in Stack Application: Pr_BACnetVTClass_t
    /// </summary>
    internal struct PrBacnetVTClass
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /** Type of VT class */
        public BacnetVTClass vtcClassSupport;
        /** Poiter to next list element */
        public IntPtr next;
    }

    /// <summary>
    /// This structure defines the property datatype BACnet VT sessions.
    /// Name in Stack Application: BACnetVTSession_t
    /// </summary>
    internal struct BacnetVTSession
    {
        /** Local Session Id */
        public byte localVTSessionID;
        /** Remote Session Id */
        public byte remoteVTSessionID;
        /** Remote Address */
        public BacnetAddress remoteVTAddress;
    }

    /// <summary>
    /// This structure defines the property datatype BACnet VT sessions list.
    /// Name in Stack Application: ListOfBACnetVTSession_t
    /// </summary>
    internal struct ListOfBacnetVTSession
    {
        public BacnetVTSession vtSession;
        public IntPtr next;
    }

    /// <summary>
    /// This structure defines the property datatype BACnet VT sessions.
    /// Name in Stack Application: Pr_ListOfBACnetVTSession_t
    /// </summary>
    internal struct PrListOfBacnetVTSession
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        // public int activeCOV;
        /* list count */
        public uint count;
        /* list of vt sessions */
        public IntPtr listOfVTSession;
    }

    /// <summary>
    /// This structure defines the property datatype of type session key.
    /// Name in Stack Application: Pr_BACnetSessKey_t
    /// </summary>
    internal struct PrBacnetSessKey
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        // public int activeCOV;
        /** Cryptographic Session Key */

        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.U1, SizeConst = 8)]
        public byte[] sessionKey;

        /** BACnet Address of the peer with which secure communications is requested*/
        public BacnetAddress peerAddress;
        /** Next list element pointer */
        public IntPtr next;
    }

    ///<summary>
    ///   This structure defines the property datatype of type bacnet recipient.
    ///   Name in Stack Application: BACnetRecipient_t
    ///</summary>
    internal struct BacnetRecipient
    {
        public DestinationType destinationType;

        /// <summary>
        /// Union BACnetRecipient_u in Stack has been declared as byte[] bacnetRecipientUnion here. This field may contain any one of below structures.
        /// BacnetRecipientUnionObjID, BacnetRecipientUnionAddress
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.U1, SizeConst = BacDelPropertyDef.BacnetRecipientUnionSize)]
        public byte[] bacnetRecipientUnion;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetRecipientUnionSize)]
    internal struct BacnetRecipientUnionObjID
    {
        public ObjID objectID;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetRecipientUnionSize)]
    internal struct BacnetRecipientUnionAddress
    {
        public BacnetAddress stAddress;
    }

    /// <summary>
    /// This structure defines the property datatype of type list of bacnet recipient.
    /// Name in Stack Application: ListOfBACnetRecipient_t
    /// </summary>
    internal struct ListOfBacnetRecipient
    {
        public BacnetRecipient bacnetRecipient;
        public IntPtr next;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet recipient.
    /// Name in Stack Application: Pr_ListOfBACnetRecipient_t
    /// </summary>
    internal struct PrListOfBacnetRecipient
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /* count for no of list elements */
        public uint count;
        /* list of recipients */
        public IntPtr listOfRecipient;
    }

    ///<summary>
    ///   This structure defines the property datatype of type bacnet recipient.
    ///   - stores the process ID & recipient.
    ///   Name in Stack Application: BACnetRecipientProcess_t
    ///</summary>
    internal struct BacnetRecipientProcess
    {
        public BacnetRecipient bacnetRecipient;
        public uint processID;
    }

    ///<summary>
    ///bacnet notification parameters - change of state
    ///Name in Stack Application: BACnetDevObjRef_t
    ///</summary>
    internal struct BacnetDevObjRef
    {
        /* Device Type flag */
        /* For bool datatype we have to explicitly mention to allocate 1 byte to avoid packing issue.*/

        [MarshalAs(UnmanagedType.I1)]
        public bool deviceIDPresent;

        /* Type of Object */
        public BacnetObjectType objectType;
        /* Device Type */
        public BacnetObjectType deviceType;
        /* Object Instance */
        public uint objID;
        /* Device Instance */
        public uint deviceID;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet device object reference.
    /// Name in Stack Application: Pr_BACnetDevObjRef_t
    /// </summary>
    internal struct PrBacnetDevObjRef
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        public int activeCOV;
        /** Device Object references data */
        public BacnetDevObjRef stDeviceObjReff;
    }

    /// <summary>
    /// Linked list of device object reference.
    /// Name in Stack Application: ListOfBACnetDevObjRef_t
    /// </summary>
    internal struct ListOfBacnetDevObjRef
    {
        public BacnetDevObjRef stDevObjRef;
        public IntPtr next;
    }

    /// <summary>
    /// This structure defines the property datatype of type array/list of bacnet device object reference.
    /// Name in Stack Application: Pr_ListOfBACnetDevObjRef_t
    /// </summary>
    internal struct PrListOfBacnetDevObjRef
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        public int activeCOV;
        /** array size */
        public uint arraySize;
        /** list of values */
        public IntPtr listOfDevObjReff; // pointer to structure ListOfBACnetDevObjRef_t
    }

    /// <summary>
    /// This is structure of bacnet object property reference.
    /// Name in Stack Application: BACnetObjPropRef_t
    /// </summary>
    internal struct BacnetObjPropRef
    {
        /** Object Instance*/
        public uint objID;
        /** Type of Object*/
        public BacnetObjectType objectType;
        /** Array Index & its flag */
        /* For bool datatype we have to explicitly mention to allocate 1 byte to avoid packing issue.*/

        [MarshalAs(UnmanagedType.I1)]
        public bool arrIndxPresent;

        public uint arrayIndex;
        /** Property Identifier*/
        public BacnetPropertyID propertyIdentifier;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet object property reference.
    /// Name in Stack Application: Pr_BACnetObjPropRef_t
    /// </summary>
    internal struct PrBacnetObjPropRef
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /* Object Property Reference data */
        public BacnetObjPropRef m_stObjPropRef;
    }

    /// <summary>
    /// Linked list of object property reference.
    /// Name in Stack Application: ListOfBACnetObjPropRef_t
    /// </summary>
    internal struct ListOfBacnetObjPropRef
    {
        public BacnetObjPropRef stObjPropRef;
        public IntPtr next;
    }

    /// <summary>
    /// This structure defines the property datatype
    /// of type array/list of bacnet object property reference.
    /// Name in Stack Application: Pr_ListOfBACnetObjPropRef_t
    /// </summary>
    internal struct PrListOfBacnetObjPropRef
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /** array size */
        public uint arraySize;
        /** list of values */
        public IntPtr listOfObjPropReff;
    }

    /// <summary>
    /// struct for bacnet device object property ref.
    /// Name in Stack Application: BACnetDevObjPropRef_t
    /// </summary>
    internal struct BacnetDevObjPropRef
    {
        /* Type of Object*/
        public BacnetObjectType objectType;
        public BacnetObjectType deviceType;
        /* Object Instance */
        public uint objID;
        /* Device Instance */
        public uint deviceInstace;
        /* Device Type & its flag */
        /* For bool datatype we have to explicitly mention to allocate 1 byte to avoid packing issue.*/

        [MarshalAs(UnmanagedType.I1)]
        public bool deviceIDPresent;

        /* Array Index & its flag */
        /* For bool datatype we have to explicitly mention to allocate 1 byte to avoid packing issue.*/

        [MarshalAs(UnmanagedType.I1)]
        public bool arrIndxPresent;

        public uint arrayIndex;
        /* Property Identifier*/
        public BacnetPropertyID propertyIdentifier;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet device object property reference.
    /// Name in Stack Application: Pr_BACnetDevObjPropRef_t
    /// </summary>
    internal struct PrBacnetDevObjPropRef
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        public int activeCOV;
        /** Device Object Property references data */
        public BacnetDevObjPropRef stDevObjPropReff;
    }

    /// <summary>
    /// Linked list of device object property reference.
    /// Name in Stack Application: ListOfBACnetDevObjPropRef_t
    /// </summary>
    internal struct ListOfBacnetDevObjPropRef
    {
        public BacnetDevObjPropRef stDevObjPropRef;
        public IntPtr next;
    }

    /// <summary>
    /// This structure defines the property datatype
    /// of type bacnet device object property reference.
    /// Name in Stack Application: Pr_ListOfBACnetDevObjPropRef_t
    /// </summary>
    internal struct PrListOfBacnetDevObjPropRef
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        public int activeCOV;
        /* hold array size only for
        sBACnetARRAY[N] of BACnetDeviceObjectPropertyReference */
        public uint arraySize;
        /** list of values */
        public IntPtr listOfBACnetDevObjPropReff;
    }

    /// <summary>
    /// This structure defines the property datatype
    /// of type bacnet device object property value.
    /// Name in Stack Application: BACnetDevObjPropVal_t
    /// </summary>
    internal struct BacnetDevObjPropVal
    {
        /** device id */
        public uint deviceInstace;
        /** object id */
        public uint objID;
        /** device type */
        public BacnetObjectType deviceType;
        /** object type */
        public BacnetObjectType objectType;
        /** array index */
        public uint arrayIndex;
        /** property id */
        public BacnetPropertyID propertyID;
        /** Any value */
        public BacnetPropertyValue value;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet property states.
    /// Name in Stack Application: BACnetPropertyStates_t
    /// </summary>
    internal struct BacnetPropertyState
    {
        /* tag for encoding & decoding, also denotes BACNET_PROPERTY_STATES value */
        public uint propState;
        /* tag to save application tag of prop data type */
        public uint appTag;
        public BacnetPropertyStatesUnion bacnetPropState;
    }

    [StructLayout(LayoutKind.Explicit)]
    internal struct BacnetPropertyStatesUnion
    {
        /*boolean-value*/

        [FieldOffset(0)]
        public bool boolVal;

        /*binary-value*/

        [FieldOffset(0)]
        public BacnetBinaryPV binaryVal;

        /*event-type*/

        [FieldOffset(0)]
        public BacnetEventType eventType;

        /*polarity*/

        [FieldOffset(0)]
        public BacnetPolarity polarity;

        /*program-change*/

        [FieldOffset(0)]
        public BacnetProgramRequest programChange;

        /*program-state*/

        [FieldOffset(0)]
        public BacnetProgramState programState;

        /*reason-for-halt*/

        [FieldOffset(0)]
        public BacnetProgramError reasonForHalt;

        /*reliability*/

        [FieldOffset(0)]
        public BacnetReliability reliability;

        /*state*/

        [FieldOffset(0)]
        public BacnetEventState eventstate;

        /*system-status*/

        [FieldOffset(0)]
        public BacnetDeviceStatus systemStatus;

        /*units*/

        [FieldOffset(0)]
        public BacnetEngineeringUnits units;

        /*unsigned-value*/

        [FieldOffset(0)]
        public uint unsignedValue;

        /*life-safety-mode*/

        [FieldOffset(0)]
        public BacnetLifeSafetyMode lifeSafetyMode;

        /*life-safety-state*/

        [FieldOffset(0)]
        public BacnetLifeSafetyState lifeSafetyState;
    }

    /// <summary>
    /// Linked list of bacnet property states.
    /// Name in Stack Application: ListOfBACnetPropertyStates_t
    /// </summary>

    internal struct ListOfBacnetPropertyStates
    {
        public BacnetPropertyState bacnetPropStates;
        public IntPtr next;
    }

    /// <summary>
    /// This structure defines the property datatype of type Elapsed Active Time.
    /// Name in Stack Application: bacnetActiveElapse_t
    /// </summary>
    internal struct BacnetActiveElapse
    {
        /* For storing Active state elapse info */
        public PrBacnetUnsigned32 stActiveElapseTime;
        /* For storing time of Active state start */
        public PrBacnetDateTime stActiveStateTime;
    }

    /// <summary>
    /// Linked list of bacnet life safety states.
    /// Name in Stack Application: ListOfBACnetLifeSafetyStates_t
    /// </summary>
    internal struct ListOfBacnetLifeSafetyStates
    {
        /** life safety state */
        public BacnetLifeSafetyState lifeSafetyState;
        /** pointer to create link list */
        public IntPtr next;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet Authentication Factor.
    /// Name in Stack Application: BACnetAuFactor_t
    /// </summary>
    internal struct BacnetAuFactor
    {
        /* type of authentication format */
        public BacnetAuthenticateFactorType formatType;
        /* format class */
        public uint formatClass;
        /* value of type - octetstring */
        public BacnetOctetStr octetStrValue;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet priority.- unsigned array of 3.
    /// Name in Stack Application: Pr_BACnetNotifyPriority_t
    /// </summary>
    internal struct PrBacnetNotifyPriority
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        public int activeCOV;
        /** data value - array of three */
        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.U4, SizeConst = BacDelPropertyDef.PrBacnetNotifyPriorityUintSize)]
        public uint[] value;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet destination.
    /// - linked list of destinations.
    /// Name in Stack Application: ListOfBACnetDestination_t
    /// </summary>
    internal struct ListOfBacnetDestination
    {
        /** Notification to be send will be of type confirmed or unconfirmed */
        /* For bool datatype we have to explicitly mention to allocate 1 byte to avoid packing issue.*/

        [MarshalAs(UnmanagedType.I1)]
        public bool issueConfirmedNotification;

        /** The process Id of device to which notification is to be send */
        public uint stProcessID;
        /** valid from time after which the notification can be sent to particular destination */
        public BacnetTime stFromTime;
        /** valid to time upto which the notification can be sent to particular destination */
        public BacnetTime stToTime;
        /**	valid day of week on which the notification is to be sent */
        public BacnetBitStr stDaysOfWeek;
        /**	event transitions bits	*/
        public BacnetBitStr stTransitions;
        public BacnetRecipient bacnetRecipient;
        /** pointer to create a link list for no of destinations */
        public IntPtr next;
    }

    /// <summary>
    /// This structure defines the property datatype of type list of bacnet destination.
    /// Name in Stack Application: Pr_ListOfBACnetDestination_t
    /// </summary>
    internal struct PrListOfBacnetDestination
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        public int activeCOV;
        /* no of array/list elements */
        public uint count;
        /* Recipient List */
        public IntPtr ncRecepient;
    }

    /// <summary>
    /// change-of-bitstring
    /// Name in Stack Application: ChangeOfBitstring_t
    /// </summary>
    internal struct ChangeOfBitstring
    {
        /* time-delay */
        public uint timedelay;
        /* bitmask */
        public PrBacnetBITStr stBitmask;
        /* count for no of list elements */
        public uint count;
        /* list-of-bitstring-values */
        public IntPtr bitStrList;
    }

    /// <summary>
    /// change-of-state
    /// Name in Stack Application: ChangeOfState_t
    /// </summary>
    internal struct ChangeOfState
    {
        /* time-delay */
        public uint timedelay;
        /* BACnetPropertyStates */
        public IntPtr listOfValues;
    }

    /// <summary>
    /// change-of-value
    /// Name in Stack Application: ChangeOfValue_t
    /// </summary>
    internal struct ChangeOfValue
    {
        /* time-delay */
        public uint timedelay;
        /* change-of-value union */
        /* type of value for union */
        public sbyte criteriaType;

        /// <summary>
        ///  Union covcriteria_U in Stack has been declared as byte[] covCriteriaUnion here.
        ///  This field may contain any one of below structures.
        ///  CovCriteriaUnionBITStr, CovCriteriaUnionFloat
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.U1, SizeConst = BacDelPropertyDef.CovcriteriaUnionSize)]
        public byte[] covCriteriaUnion;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.CovcriteriaUnionSize)]
    internal struct CovCriteriaUnionBITStr
    {
        /* bitmask */
        public PrBacnetBITStr stBitMask;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.CovcriteriaUnionSize)]
    internal struct CovCriteriaUnionFloat
    {
        /* referenced-property-increment */
        public float refPropIncr;
    }

    /// <summary>
    /// command-failure
    /// Name in Stack Application: CommandFailure_t
    /// </summary>
    internal struct CommandFailure
    {
        /* to save application tag */
        public uint apptag;
        /* time-delay */
        public uint timedelay;
        /* feedback-property-reference */
        public BacnetDevObjPropRef stFeedbackPropertyReference;

        /// <summary>
        /// Union PropertyValue_u in Stack has been declared as byte[] commandFailureUnion here.
        /// This field may contain any one of below structures.
        /// PropertyValueUnionBool, PropertyValueUnionUint, PropertyValueUnionInt, PropertyValueUnionBinaryPV,
        /// PropertyValueUnionFloat, PropertyValueUnionDouble, PropertyValueUnionBITStr, PropertyValueUnionCharStr,
        /// PropertyValueUnionOctetStr, PropertyValueUnionTime, PropertyValueUnionDate, PropertyValueUnionDateTime
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.U1, SizeConst = BacDelPropertyDef.PropertyValueUnionSize)]
        public byte[] propertyValueUnion;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.PropertyValueUnionSize)]
    internal struct PropertyValueUnionBool
    {
        /* For bool datatype we have to explicitly mention to allocate 1 byte to avoid packing issue.*/

        [MarshalAs(UnmanagedType.I1)]
        public bool value;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.PropertyValueUnionSize)]
    internal struct PropertyValueUnionUint
    {
        public uint value;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.PropertyValueUnionSize)]
    internal struct PropertyValueUnionInt
    {
        public int value;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.PropertyValueUnionSize)]
    internal struct PropertyValueUnionBinaryPV
    {
        public BacnetBinaryPV value;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.PropertyValueUnionSize)]
    internal struct PropertyValueUnionFloat
    {
        public float value;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.PropertyValueUnionSize)]
    internal struct PropertyValueUnionDouble
    {
        public double value;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.PropertyValueUnionSize)]
    internal struct PropertyValueUnionBITStr
    {
        public BacnetBITStr stBitStringVal;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.PropertyValueUnionSize)]
    internal struct PropertyValueUnionCharStr
    {
        public BacnetCharStr stCharStringVal;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.PropertyValueUnionSize)]
    internal struct PropertyValueUnionOctetStr
    {
        public BacnetOctetStr stOctetStringVal;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.PropertyValueUnionSize)]
    internal struct PropertyValueUnionTime
    {
        public BacnetTime timeValue;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.PropertyValueUnionSize)]
    internal struct PropertyValueUnionDate
    {
        public BacnetDate dateValue;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.PropertyValueUnionSize)]
    internal struct PropertyValueUnionDateTime
    {
        public BacnetDateTime dateTimeValue;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.PropertyValueUnionSize)]
    internal struct PropertyValueUnionObjId
    {
        public BacnetObjID objectId;
    }

    /// <summary>
    /// floating-limit
    /// Name in Stack Application: FloatingLimit_t
    /// </summary>
    internal struct FloatingLimit
    {
        /* time-delay */
        public uint timedelay;
        /* setpoint-reference */
        public BacnetDevObjPropRef stSetpointReference;
        /* low-diff-limit */
        public float lowDiffLimit;
        /* high-diff-limit */
        public float highDiffLimit;
        /* deadband */
        public float deadband;
    }

    /// <summary>
    /// out-of-range
    /// Name in Stack Application: OutOfRange_t
    /// </summary>
    internal struct OutOfRange
    {
        /* time-delay */
        public uint timedelay;
        /* low-limit */
        public float lowLimit;
        /* high-limit */
        public float highLimit;
        /* deadband */
        public float deadband;
    }

    /// <summary>
    /// change-of-life-safety
    /// Name in Stack Application: ChangeOfLifeSafety_t
    /// </summary>
    internal struct ChangeOfLifeSafety
    {
        /* time-delay */
        public uint timedelay;
        /* list-of-life-safety-alarm-values */
        public IntPtr listOfLifeSafetyAlarmValues;
        /* list-of-alarm-values */
        public IntPtr listOfAlarmValues;
        /* mode-property-reference */
        public BacnetDevObjPropRef stModePropReff;
    }

    /// <summary>
    /// extended-parameters-structure
    /// Name in Stack Application: Parameters_t
    /// </summary>
    internal struct Parameters
    {
        /* type of parameter */
        public byte parameterType;

        /// <summary>
        /// Union Parameters_u in Stack has been declared as byte[] parametersUnion here. This field may contain any one of below structures.
        /// ParametersUnionBool, ParametersUnionUint, ParametersUnionInt, ParametersUnionBinaryPV, ParametersUnionFloat, ParametersUnionDouble,
        /// ParametersUnionBITStr, ParametersUnionOctetStr, ParametersUnionDevObjPropRef
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.U1, SizeConst = BacDelPropertyDef.ParametersUnionSize)]
        public byte[] parametersUnion;

        public IntPtr next;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.ParametersUnionSize)]
    internal struct ParametersUnionBool
    {
        /* For bool datatype we have to explicitly mention to allocate 1 byte to avoid packing issue.*/

        [MarshalAs(UnmanagedType.I1)]
        public bool val;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.ParametersUnionSize)]
    internal struct ParametersUnionUint
    {
        public uint val;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.ParametersUnionSize)]
    internal struct ParametersUnionInt
    {
        public int val;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.ParametersUnionSize)]
    internal struct ParametersUnionBinaryPV
    {
        public BacnetBinaryPV val;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.ParametersUnionSize)]
    internal struct ParametersUnionFloat
    {
        public float val;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.ParametersUnionSize)]
    internal struct ParametersUnionDouble
    {
        public double val;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.ParametersUnionSize)]
    internal struct ParametersUnionBITStr
    {
        public BacnetBITStr stBitStrValue;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.ParametersUnionSize)]
    internal struct ParametersUnionOctetStr
    {
        public BacnetOctetStr stOctetStr;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.ParametersUnionSize)]
    internal struct ParametersUnionDevObjPropRef
    {
        public BacnetDevObjPropRef stDevObjPropRef;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.ParametersUnionSize)]
    internal struct ParametersUnionPRObjId
    {
        public PrBacnetObjID stObjectId;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.ParametersUnionSize)]
    internal struct ParametersUnionCharStr
    {
        public BacnetCharStr stCharStr;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.ParametersUnionSize)]
    internal struct ParametersUnionDate
    {
        public BacnetDate stDate;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.ParametersUnionSize)]
    internal struct ParametersUnionTime
    {
        public BacnetTime stTime;
    }

    /// <summary>
    /// extended
    /// Name in Stack Application: Extended_t
    /// </summary>
    internal struct Extended
    {
        /* vendorId */
        public uint vendorID;
        /* extendedEventType */
        public uint extendedEventType;
        /* sequence of parameters */
        public IntPtr parameters;
    }

    /// <summary>
    /// buffer-ready
    /// Name in Stack Application: BufferReady_t
    /// </summary>
    internal struct BufferReady
    {
        /* notification-threshold */
        public uint notificationThreshold;
        /* previous-notification-count */
        public uint previousNotificationCount;
    }

    /// <summary>
    /// unsigned-range
    /// Name in Stack Application: UnsignedRange_t
    /// </summary>
    internal struct UnsignedRange
    {
        /* time-delay */
        public uint timedelay;
        /* low-limit */
        public uint lowLimit;
        /* high-limit */
        public uint highLimit;
    }

    /// <summary>
    /// access-event
    /// Name in Stack Application: AccessEvent_t
    /// </summary>
    internal struct AccessEvent
    {
        /* no of elements in list */
        public uint count;
        /* list-of-access-events */
        public IntPtr accessEventList;
        /* access-event-time-reference */
        public BacnetDevObjPropRef stAccessEventTimeReff;
    }

    /// <summary>
    /// double-out-of-range
    /// Name in Stack Application: DoubleOutOfRange_t
    /// </summary>
    internal struct DoubleOutOfRange
    {
        /* time-delay */
        public uint timedelay;
        /* low-limit */
        public double lowLimit;
        /* high-limit */
        public double highLimit;
        /* deadband */
        public double deadband;
    }

    /// <summary>
    /// signed-out-of-range
    /// Name in Stack Application: SignedOutOfRange_t
    /// </summary>
    internal struct SignedOutOfRange
    {
        /* time-delay */
        public uint timedelay;
        /* low-limit */
        public int lowLimit;
        /* high-limit */
        public int highLimit;
        /* deadband */
        public uint deadband;
    }

    /// <summary>
    /// unsigned-out-of-range
    /// Name in Stack Application: UnSignedOutOfRange_t
    /// </summary>
    internal struct UnSignedOutOfRange
    {
        /* time-delay */
        public uint timedelay;
        /* low-limit */
        public uint lowLimit;
        /* high-limit */
        public uint highLimit;
        /* deadband */
        public uint deadband;
    }

    /// <summary>
    /// change-of-characterstring
    /// Name in Stack Application: ChangeOfCharString_t
    /// </summary>
    internal struct ChangeOfCharString
    {
        /* time-delay */
        public uint timedelay;
        /* no of list elements */
        public uint count;
        /* list-of-alarm-values */
        public IntPtr listOfAlarmValues;
    }

    /// <summary>
    /// change-of-status-flags
    /// Name in Stack Application: ChangeOfStatusFlag_t
    /// </summary>
    internal struct ChangeOfStatusFlag
    {
        /* time-delay */
        public uint timedelay;
        /* selected-flags */
        public BacnetBitStr stStatusFlag;
    }

    /// <summary>
    /// This structure defines the bacnet event parameters datatype.
    /// Name in Stack Application: BACnetEventParameter_t
    /// </summary>
    internal struct BacnetEventParameter
    {
        /** type of event */
        public BacnetEventType eventType;

        /// <summary>
        /// Union BACnetEventParameter_u in Stack has been declared as byte[] bacnetEventParameterUnion here. This field may contain any one of below structures.
        /// BacnetEventParameterUnionChangeOfBitstring, BacnetEventParameterUnionChangeOfState, BacnetEventParameterUnionChangeOfValue, BacnetEventParameterUnionCommandFailure,
        /// BacnetEventParameterUnionFloatingLimit, BacnetEventParameterUnionOutOfRange, BacnetEventParameterUnionChangeOfLifeSafety, BacnetEventParameterUnionExtended,
        /// BacnetEventParameterUnionBufferReady, BacnetEventParameterUnionUnsignedRange, BacnetEventParameterUnionAccessEvent, BacnetEventParameterUnionDoubleOutOfRange,
        /// BacnetEventParameterUnionSignedOutOfRange, BacnetEventParameterUnionUnSignedOutOfRange,
        /// BacnetEventParameterUnionChangeOfCharString, BacnetEventParameterUnionChangeOfStatusFlag
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.U1, SizeConst = BacDelPropertyDef.BacnetEventParameterUnionSize)]
        public byte[] bacnetEventParameterUnion;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetEventParameterUnionSize)]
    internal struct BacnetEventParameterUnionChangeOfBitstring
    {
        public ChangeOfBitstring stCngBitstring;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetEventParameterUnionSize)]
    internal struct BacnetEventParameterUnionChangeOfState
    {
        public ChangeOfState stCngState;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetEventParameterUnionSize)]
    internal struct BacnetEventParameterUnionChangeOfValue
    {
        public ChangeOfValue stCngValue;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetEventParameterUnionSize)]
    internal struct BacnetEventParameterUnionCommandFailure
    {
        public CommandFailure stCmdFail;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetEventParameterUnionSize)]
    internal struct BacnetEventParameterUnionFloatingLimit
    {
        public FloatingLimit stFlotLimit;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetEventParameterUnionSize)]
    internal struct BacnetEventParameterUnionOutOfRange
    {
        public OutOfRange stOutRange;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetEventParameterUnionSize)]
    internal struct BacnetEventParameterUnionChangeOfLifeSafety
    {
        public ChangeOfLifeSafety stCngLifeSafety;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetEventParameterUnionSize)]
    internal struct BacnetEventParameterUnionExtended
    {
        public Extended stExtended;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetEventParameterUnionSize)]
    internal struct BacnetEventParameterUnionBufferReady
    {
        public BufferReady stBuffReady;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetEventParameterUnionSize)]
    internal struct BacnetEventParameterUnionUnsignedRange
    {
        public UnsignedRange stUnsiRange;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetEventParameterUnionSize)]
    internal struct BacnetEventParameterUnionAccessEvent
    {
        public AccessEvent stAccessEvent;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetEventParameterUnionSize)]
    internal struct BacnetEventParameterUnionDoubleOutOfRange
    {
        public DoubleOutOfRange stDoubleOutofRange;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetEventParameterUnionSize)]
    internal struct BacnetEventParameterUnionSignedOutOfRange
    {
        public SignedOutOfRange stSignedOutofRange;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetEventParameterUnionSize)]
    internal struct BacnetEventParameterUnionUnSignedOutOfRange
    {
        public UnSignedOutOfRange stUnSignedOutofRange;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetEventParameterUnionSize)]
    internal struct BacnetEventParameterUnionChangeOfCharString
    {
        public ChangeOfCharString stCngCharString;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetEventParameterUnionSize)]
    internal struct BacnetEventParameterUnionChangeOfStatusFlag
    {
        public ChangeOfStatusFlag stCngStatusFlag;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet event parameters.
    /// Name in Stack Application: Pr_BACnetEventParameter_t
    /// </summary>
    internal struct PrBacnetEventParameter
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        public int activeCOV;
        public BacnetEventParameter bacnetEventParam;
    }

    /// <summary>
    /// bacnet notification parameters - change of bitstring
    /// Name in Stack Application: ChangeOfBitstringNP_t
    ///</summary>
    internal struct ChangeOfBitstringNP
    {
        public BacnetBITStr stRefBitString;
        public BacnetBitStr stStatusFlag;
    }

    /// <summary>
    /// bacnet notification parameters - change of state
    /// Name in Stack Application: ChangeOfStateNP_t
    /// </summary>
    internal struct ChangeOfStateNP
    {
        public BacnetPropertyState bacnetPropertyStates;
        public BacnetBitStr stStatusFlag;
    }

    /// <summary>
    /// bacnet notification parameters - change of value
    /// Name in Stack Application: ChangeOfValueNP_t
    /// </summary>
    internal struct ChangeOfValueNP
    {
        public byte valueType;

        /// <summary>
        /// Union uNewValue in Stack has been declared as byte[] ChangeOfValueNPUnion here. This field may contain any one of below structures.
        /// ChangeOfValueNPUnionFloat, ChangeOfValueNPUnionBITStr
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.U1, SizeConst = BacDelPropertyDef.ChangeOfValueNPUnionSize)]
        public byte[] ChangeOfValueNPUnion;

        public BacnetBitStr stStatusFlag;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.ChangeOfValueNPUnionSize)]
    internal struct ChangeOfValueNPUnionFloat
    {
        public float changedValue;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.ChangeOfValueNPUnionSize)]
    internal struct ChangeOfValueNPUnionBITStr
    {
        public BacnetBITStr stChangedBits;
    }

    /// <summary>
    /// bacnet notification parameters - command failure
    /// Name in Stack Application: CommandFailureNP_t
    /// </summary>
    internal struct CommandFailureNP
    {
        ///<summary> to save application tag </summary>
        public byte Apptag;

        /// <summary>
        /// Union PropertyValue_u in Stack has been declared as byte[] stCommandValue here.
        /// This field may contain any one of below structures.
        /// PropertyValueUnionBool, PropertyValueUnionUint, PropertyValueUnionInt, PropertyValueUnionBinaryPV,
        /// PropertyValueUnionFloat, PropertyValueUnionDouble, PropertyValueUnionBITStr, PropertyValueUnionCharStr,
        /// PropertyValueUnionOctetStr, PropertyValueUnionTime, PropertyValueUnionDate, PropertyValueUnionDateTime
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.U1, SizeConst = BacDelPropertyDef.PropertyValueUnionSize)]
        public byte[] stCommandValue;

        /// <summary>
        /// Union PropertyValue_u in Stack has been declared as byte[] stFeedbackValue here.
        /// This field may contain any one of below structures.
        /// PropertyValueUnionBool, PropertyValueUnionUint, PropertyValueUnionInt, PropertyValueUnionBinaryPV,
        /// PropertyValueUnionFloat, PropertyValueUnionDouble, PropertyValueUnionBITStr, PropertyValueUnionCharStr,
        /// PropertyValueUnionOctetStr, PropertyValueUnionTime, PropertyValueUnionDate, PropertyValueUnionDateTime
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.U1, SizeConst = BacDelPropertyDef.PropertyValueUnionSize)]
        public byte[] stFeedbackValue;

        public BacnetBitStr stStatusFlag;
    }

    /// <summary>
    /// bacnet notification parameters - floating limit
    /// Name in Stack Application: FloatingLimitNP_t
    /// </summary>
    internal struct FloatingLimitNP
    {
        public float referenceValue; // controlled variable value
        public float setPointValue; // set point value
        public float errorLimit; // error limit
        public BacnetBitStr stStatusFlag; // status flag
    }

    /// <summary>
    /// bacnet notification parameters - out of range
    /// Name in Stack Application: OutOfRangeNP_t
    /// </summary>
    internal struct OutOfRangeNP
    {
        public float exceedingValue; // exceeding value is PV
        public float exceededLimit; // exceeded limit is high or low limit
        public float deadband; // deadband
        public BacnetBitStr stStatusFlag; // status flag
    }

    /// <summary>
    /// bacnet notification parameters - complex event type
    /// Name in Stack Application: ComplexEventTypeNP_t
    /// </summary>
    internal struct ComplexEventTypeNP
    {
        // this structure is not complete, refer manual
        public ushort val;
    }

    /// <summary>
    /// bacnet notification parameters - change of life safety
    /// Name in Stack Application: ChangeOfLifeSafetyNP_t
    /// </summary>
    internal struct ChangeOfLifeSafetyNP
    {
        public BacnetLifeSafetyMode newMode; // new mode
        public BacnetLifeSafetyOperation operationExpected; // operations expected
        public BacnetLifeSafetyState newState; // life safety states of PV
        public BacnetBitStr stStatusFlag; // status flag
    }

    /// <summary>
    /// bacnet notification parameters - extended
    /// Name in Stack Application: ExtendedNP_t
    /// </summary>
    internal struct ExtendedNP
    {
        public ushort vendorID; // vendor id
        public uint extendedEventType; // extended event type
        /* sequence of parameters */
        public IntPtr parameters;
    }

    /// <summary>
    /// bacnet notification parameters -  buffer ready
    /// Name in Stack Application: BufferReadyNP_t
    /// </summary>
    internal struct BufferReadyNP
    {
        public uint previousNotification; // last notifiy record
        public uint currentNotification; // total record count
        public BacnetDevObjPropRef stBufferProperty; // buffer property
    }

    /// <summary>
    /// bacnet notification parameters - unsigned range
    /// Name in Stack Application: UnsignedRangeNP_t
    /// </summary>
    internal struct UnsignedRangeNP
    {
        public uint exceedingValue; // pulse rate
        public uint exceededLimit; // low or high limit
        public BacnetBitStr stStatusFlag; // status flag
    }

    /// <summary>
    /// bacnet notification parameters - access event
    /// Name in Stack Application: AccessEventNP_t
    /// </summary>
    internal struct AccessEventNP
    {
        public BacnetAccessEvent accessEvent; // access event value
        public uint eventTag; // access event tag value
        public BacnetTimeStamp stEventTime; // access event time
        public BacnetDevObjRef accessCredential; // access credential value
        public BacnetBitStr stStatusFlag; // status flag
        /* For bool datatype we have to explicitly mention to allocate 1 byte to avoid packing issue.*/

        [MarshalAs(UnmanagedType.I1)]
        public bool factorPresent;

        public BacnetAuFactor stAuFactor;
    }

    /// <summary>
    /// bacnet notification parameters - double out of range
    /// Name in Stack Application: DoubleOutOfRangeNP_t
    /// </summary>
    internal struct DoubleOutOfRangeNP
    {
        public double exceedingValue; // exceeding value is PV
        public double exceededLimit; // exceeded limit is high or low limit
        public double deadband; // deadband
        public BacnetBitStr stStatusFlag; // status flag
    }

    /// <summary>
    /// bacnet notification parameters - signed out of range
    /// Name in Stack Application: SignedOutOfRangeNP_t
    /// </summary>
    internal struct SignedOutOfRangeNP
    {
        public int exceedingValue; // exceeding value is PV
        public int exceededLimit; // exceeded limit is high or low limit
        public uint deadband; // deadband
        public BacnetBitStr stStatusFlag; // status flag
    }

    /// <summary>
    /// bacnet notification parameters - unsigned out of range
    /// Name in Stack Application: UnSignedOutOfRangeNP_t
    /// </summary>
    internal struct UnSignedOutOfRangeNP
    {
        public uint exceedingValue; // exceeding value is PV
        public uint exceededLimit; // exceeded limit is high or low limit
        public uint deadband; // deadband
        public BacnetBitStr stStatusFlag; // status flag
    }

    /// <summary>
    /// bacnet notification parameters - change of character string
    /// Name in Stack Application: ChangeOfCharStringNP_t
    /// </summary>
    internal struct ChangeOfCharStringNP
    {
        public BacnetCharStr stChangedValue; // PV value
        public BacnetCharStr stAlarmValue; // corresponding alarm value
        public BacnetBitStr stStatusFlag; // status flag
    }

    /// <summary>
    /// bacnet notification parameters - change of status flags
    /// Name in Stack Application: ChangeOfStatusFlagsNP_t
    /// </summary>
    internal struct ChangeOfStatusFlagsNP
    {
        public byte appTag;

        /// <summary>
        /// Union PropertyValue_u in Stack has been declared as byte[] chngOfStatusFlagUnion here.
        /// This field may contain any one of below structures.
        /// PropertyValueUnionBool, PropertyValueUnionUint, PropertyValueUnionInt, PropertyValueUnionBinaryPV,
        /// PropertyValueUnionFloat, PropertyValueUnionDouble, PropertyValueUnionBITStr, PropertyValueUnionCharStr,
        /// PropertyValueUnionOctetStr, PropertyValueUnionTime, PropertyValueUnionDate, PropertyValueUnionDateTime
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.U1, SizeConst = BacDelPropertyDef.PropertyValueUnionSize)]
        public byte[] propertyValueUnion;

        public BacnetBitStr stStatusFlag;
    }

    /// <summary>
    /// bacnet notification parameters - change of reliability
    /// Name in Stack Application: ChangeOfReliabilityNP_t
    /// </summary>
    internal struct ChangeOfReliabilityNP
    {
        public BacnetReliability reliabilty; // reliability
        public BacnetBitStr statusFlag; // status flag
        public PropertyValue propertyValue; // prperty value
    }

    ///<summary>
    ///   This structure defines the property datatype
    ///   of type bacnet notification parameters.
    ///   Name in Stack Application: BACnetNotificationParameters_t
    ///</summary>
    internal struct BacnetNotificationParameters
    {
        /* event type */
        public BacnetEventType eventType;
        /** flag to specify if event values were decoded successfully */
        /** value is true if event values decoding fails */
        /* For bool datatype we have to explicitly mention to allocate 1 byte to avoid packing issue.*/

        [MarshalAs(UnmanagedType.I1)]
        public bool decodeFailure;

        /// <summary>
        /// Union BACnetNotificationParameters_u in Stack has been declared as byte[] bacnetNotifParametersUnion here. This field may contain any one of below structures.
        /// BacnetNotifiParamsUnionChngOfBitStrNP, BacnetNotifiParamsUnionChngOfStateNP, BacnetNotifiParamsUnionChngOfValueNP, BacnetNotifiParamsUnionCmdFailureNP,
        /// BacnetNotifiParamsUnionFloatingLimitNP, BacnetNotifiParamsUnionOutOfRangeNP, BacnetNotifiParamsUnionCmplxEvtTypeNP, BacnetNotifiParamsUnionChngOfLifeSafetyNP,
        /// BacnetNotifiParamsUnionExtendedNP, BacnetNotifiParamsUnionBufferReadyNP, BacnetNotifiParamsUnionUnsignedRangeNP, BacnetNotifiParamsUnionAccEventNP,
        /// BacnetNotifiParamsUnionDoubleOutOfRangeNP, BacnetNotifiParamsUnionSignedOutOfRangeNP, BacnetNotifiParamsUnionUnSignedOutOfRangeNP,
        /// BacnetNotifiParamsUnionChngOfCharStr, BacnetNotifiParamsUnionChngOfStatusFlagsNP, BacnetNotifiParamsUnionChngOfReliabilityNP
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.U1, SizeConst = BacDelPropertyDef.BacnetNotificationParametersUnionSize)]
        public byte[] bacnetNotifParametersUnion;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetNotificationParametersUnionSize)]
    internal struct BacnetNotifiParamsUnionChngOfBitStrNP
    {
        public ChangeOfBitstringNP stChangeBitstring;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetNotificationParametersUnionSize)]
    internal struct BacnetNotifiParamsUnionChngOfStateNP
    {
        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.U1, SizeConst = BacDelPropertyDef.BacnetNotifiParamsChngOfStateSize)]
        public byte[] stChangeState;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetNotificationParametersUnionSize)]
    internal struct BacnetNotifiParamsUnionChngOfValueNP
    {
        public ChangeOfValueNP stChangeValue;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetNotificationParametersUnionSize)]
    internal struct BacnetNotifiParamsUnionCmdFailureNP
    {
        public CommandFailureNP stCmdFailure;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetNotificationParametersUnionSize)]
    internal struct BacnetNotifiParamsUnionFloatingLimitNP
    {
        public FloatingLimitNP stFlotLimit;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetNotificationParametersUnionSize)]
    internal struct BacnetNotifiParamsUnionOutOfRangeNP
    {
        public OutOfRangeNP stOutOfRange;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetNotificationParametersUnionSize)]
    internal struct BacnetNotifiParamsUnionCmplxEvtTypeNP
    {
        public ComplexEventTypeNP stCmplxEventType;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetNotificationParametersUnionSize)]
    internal struct BacnetNotifiParamsUnionChngOfLifeSafetyNP
    {
        public ChangeOfLifeSafetyNP stChangeLifeSafety;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetNotificationParametersUnionSize)]
    internal struct BacnetNotifiParamsUnionExtendedNP
    {
        public ExtendedNP stExtended;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetNotificationParametersUnionSize)]
    internal struct BacnetNotifiParamsUnionBufferReadyNP
    {
        public BufferReadyNP stBufferReady;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetNotificationParametersUnionSize)]
    internal struct BacnetNotifiParamsUnionUnsignedRangeNP
    {
        public UnsignedRangeNP stUnsignedRange;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetNotificationParametersUnionSize)]
    internal struct BacnetNotifiParamsUnionAccEventNP
    {
        public AccessEventNP stAccessEvent;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetNotificationParametersUnionSize)]
    internal struct BacnetNotifiParamsUnionDoubleOutOfRangeNP
    {
        public DoubleOutOfRangeNP stDoubleOutOfRange;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetNotificationParametersUnionSize)]
    internal struct BacnetNotifiParamsUnionSignedOutOfRangeNP
    {
        public SignedOutOfRangeNP stSignedOutOfRange;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetNotificationParametersUnionSize)]
    internal struct BacnetNotifiParamsUnionUnSignedOutOfRangeNP
    {
        public UnSignedOutOfRangeNP stUnSgndOutOfRange;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetNotificationParametersUnionSize)]
    internal struct BacnetNotifiParamsUnionChngOfCharStr
    {
        public ChangeOfCharStringNP stChangeCharString;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetNotificationParametersUnionSize)]
    internal struct BacnetNotifiParamsUnionChngOfStatusFlagsNP
    {
        public ChangeOfStatusFlagsNP stChangeStatusFlags;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetNotificationParametersUnionSize)]
    internal struct BacnetNotifiParamsUnionChngOfReliabilityNP
    {
        public ChangeOfReliabilityNP stChangeReliability;
    }

    /// <summary>
    /// structure to save the message string for event notification
    /// Name in Stack Application: NotificationString_t
    /// </summary>
    internal struct NotificationString
    {
        /** String lenght */
        public uint strLen;
        /** For Encoding Format */
        public byte encoding;
        /** Value for character string */

        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.I1, SizeConst = BacDelStackConfig.MaxCharacterStringBytes)]
        public sbyte[] charStr;
    }

    /// <summary>
    /// structure to hold the event notification data
    /// Name in Stack Application: BacnetEnPropElem_t
    /// </summary>
    internal struct BacnetEnPropElem
    {
        /* The process Id of device to which notification is to be send */
        public uint processID;
        public uint deviceID;		/* Object Instance */
        public BacnetObjectType deviceType;	/* Type of Object */
        public BacnetObjectType objectType;	/* Type of Object */
        public uint objId;		/* Object Instance */
        /* Time Stamp */
        public BacnetTimeStamp stTimeStamp;
        /* Notification Class */
        public uint notifyClass;
        /* Priority - use BACNET_NO_PRIORITY if no priority */
        public byte priority;
        /* Event Type */
        public BacnetEventType eventType;
        /* Notify Type */
        public BacnetNotifyType notifyType;
        /* From State */
        public BacnetEventState fromState;
        /* To State */
        public BacnetEventState toState;
        /* Message Text (optional) */
        public BacnetCharStr charString;
        public BacnetNotificationParameters bacnetNotificationParams;
        /* For bool datatype we have to explicitly mention to allocate 1 byte to avoid packing issue.*/

        [MarshalAs(UnmanagedType.I1)]
        public bool ackReq;

        /* Status of service success or Error */
        /* For bool datatype we have to explicitly mention to allocate 1 byte to avoid packing issue.*/

        [MarshalAs(UnmanagedType.I1)]
        public bool errorStatus;
    }

    /// <summary>
    /// Structure to hold file stream data or list of ercords.
    /// Name in Stack Application: LIST_OF_FILE_DATA
    /// </summary>
    internal struct ListOfFileData
    {
        /* indicates the data size read from the file.
        - no of bytes read in case of stream access
        - no of bytes for the particular record in case of record access */
        public uint noOfBytes;
        /* buffer to save file data */

        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.U1, SizeConst = BacDelStackConfig.MaxFileOctetStringBytes)]
        public byte[] fileData;

        /* Self reference pointer to create linklist */
        public IntPtr next;
    }

    /// <summary>
    /// This structure defines the property datatype of type date range.
    /// Name in Stack Application: BACnetDateRange_t
    /// </summary>
    internal struct BacnetDateRange
    {
        /** start date*/
        public BacnetDate stStartDate;
        /** end date */
        public BacnetDate stEndDate;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet date range.
    /// Name in Stack Application: Pr_BACnetDateRange_t
    /// </summary>
    internal struct PrBacnetDateRange
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        public int activeCOV;
        /** date range value */
        public BacnetDateRange stDateRange;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet Week N Day.
    /// Name in Stack Application: WeekNDay_t
    /// </summary>
    internal struct WeekNDay
    {
        /** Month */
        public BacnetMonth month;
        /** Week */
        public BacnetWeekOfMonth weekOfMonth;
        /** Days */
        public BacnetWeekDay weekNDay;
    }

    /// <summary>
    /// calendar entry data type.
    /// Name in Stack Application: BACnetCalendarEntry_t
    /// </summary>
    internal struct BacnetCalendarEntry
    {
        /** type of calendar entry value */
        public CalendarEntryStatus statusCalendar;
        public CalendarUnion bacnetCalEntruUnion;
    }

    [StructLayout(LayoutKind.Explicit)]
    internal struct CalendarUnion
    {
        /*Date*/

        [FieldOffset(0)]
        public BacnetDate stDate;

        /*Date Range*/

        [FieldOffset(0)]
        public BacnetDateRange stDateRange;

        /*Week and Day*/

        [FieldOffset(0)]
        public WeekNDay stWeekNDay;
    }

    /// <summary>
    /// Linked list of calendar entry.
    /// Name in Stack Application: ListOfBACnetCalendarEntry_t
    /// </summary>
    internal struct ListOfBacnetCalendarEntry
    {
        /** type of calendar entry value */
        public CalendarEntryStatus statusCalendar;
        public CalendarUnion bacnetCalEntruUnion;
        public IntPtr next;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet calendar entry list/array.
    /// Name in Stack Application: Pr_ListOfBACnetCalendarEntry_t
    /// </summary>
    internal struct PrListOfBacnetCalendarEntry
    {
        /*Access type of prop*/
        public PropAccessType accessType;
        /** COV Status Specifier */
        public int activeCOV;
        /** count for no of nodes in list */
        public uint count;
        /** List of Calendar */
        public IntPtr listOfCalendar;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet time value.
    /// Name in Stack Application: BACnetTimeValue_t
    /// </summary>
    internal struct BacnetTimeValue
    {
        /* For bool datatype we have to explicitly mention to allocate 1 byte to avoid packing issue.*/

        [MarshalAs(UnmanagedType.I1)]
        public bool isUsed;

        /* Time for weekly schedule */
        public BacnetTime stTime;
        public BacnetPropertyValue bacnetPropVal;
        public IntPtr next;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet daily schedule.
    /// Name in Stack Application: Pr_BACnetDailySchedule_t
    /// </summary>
    internal struct PrBacnetDailySchedule
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        public BacnetTimeValue bacnetTimeVal;
    }

    /// <summary>
    /// DESCRIPTION
    ///This structure defines the property datatype
    ///of type bacnet daily schedule.
    ///Name in Stack Application: Pr_ListOfBACnetDailySchedule_t
    /// </summary>
    internal struct PrListOfBacnetDailySchedule
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        public int activeCOV;
        /* Time Value */

        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.LPStruct, SizeConst = BacDelPropertyDef.PrListOfBacnetDailyScheduleTimeValueSize)]
        public BacnetTimeValue[] timeValue;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet special event.
    /// Name in Stack Application: BACnetSpecialEvent_t
    /// </summary>
    internal struct BacnetSpecialEvent
    {
        public BacnetSpecialEventUnion bacnetSpecialEventUnion;
        /* type of special event value */
        public CalendarEntryStatus statusCalendar;
        /* list Of Time Values */
        public BacnetTimeValue stListOfTimeValues;
        /* Event Priority (1..16) */
        public uint eventPriority;
    }

    [StructLayout(LayoutKind.Explicit)]
    internal struct BacnetSpecialEventUnion
    {
        /* Date */

        [FieldOffset(0)]
        public BacnetDate stDate;

        /* Date Range */

        [FieldOffset(0)]
        public BacnetDateRange stDateRange;

        /* Week and Day*/

        [FieldOffset(0)]
        public WeekNDay stWeekNDay;

        /* Calendar Reference */

        public struct CalReff
        {
            /** Type of Object*/
            public BacnetObjectType objectType;
            /** Object Instance*/
            public uint objId;
        }

        [FieldOffset(0)]
        public CalReff calReff;
    }

    /// <summary>
    /// Linked list of special events.
    /// Name in Stack Application: ListOfSpecialEvent_t
    /// </summary>
    internal struct ListOfSpecialEvent
    {
        public BacnetSpecialEvent bacnetSpecialEvt;
        public IntPtr next;
    }

    /// <summary>
    /// This structure defines the property datatype of type list/array of bacnet special events.
    /// Name in Stack Application: Pr_ListOfBACnetSpecialEvent_t
    /// </summary>
    internal struct PrListOfBacnetSpecialEvent
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        public int activeCOV;
        /** count for no of items in list */
        public uint count;
        /** list / array member */
        public IntPtr splEvent;
    }

    /// <summary>
    /// This structure defines the property datatype of type log status.
    /// Name in Stack Application: BacnetLogStatus_t
    /// </summary>
    internal struct BacnetLogStatus
    {
        public byte unusedBits;
        public byte byteCnt;
        /*
           First three MSB Bits are used as log status bits
           Bit 7 -> log-disabled (0),
           Bit 6 -> buffer-purged (1),
           Bit 5 -> log-interrupted (2)
        */

        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.U1, SizeConst = BacDelStackConfig.MinBitstringBytes)]
        public byte[] transBits;
    }

    /// <summary>
    /// This structure defines the property datatype of type log datum.
    /// NOTE - log datum is same as BACnetLogData.
    /// Name in Stack Application: logDatum_t
    /// </summary>
    internal struct LogDatum
    {
        public uint tagType;

        /// <summary>
        /// Union bacnetlogDatum_U in Stack has been declared as byte[] LogDatumUnion here. This field may contain any one of below structures.
        /// LogDatumUnionLogStatus, LogDatumUnionBool, LogDatumUnionFloat, LogDatumUnionEnum, LogDatumUnionUint, LogDatumUnionInt, LogDatumUnionDouble,
        /// LogDatumUnionByteArray, LogDatumUnionBITStr, LogDatumUnionErrorResponse, LogDatumUnionFloatTimeChange, LogDatumUnionBacnetPropVal
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.U1, SizeConst = BacDelPropertyDef.LogDatumUnionSize)]
        public byte[] LogDatumUnion;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.LogDatumUnionSize)]
    internal struct LogDatumUnionLogStatus
    {
        public BacnetLogStatus stLogStatus;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.LogDatumUnionSize)]
    internal struct LogDatumUnionBool
    {
        /* For bool datatype we have to explicitly mention to allocate 1 byte to avoid packing issue.*/

        [MarshalAs(UnmanagedType.I1)]
        public bool booleanValue;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.LogDatumUnionSize)]
    internal struct LogDatumUnionFloat
    {
        public LogDatum logDatum;
        public float floatValue;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.LogDatumUnionSize)]
    internal struct LogDatumUnionEnum
    {
        public LogDatum logDatum;
        public uint enumValue;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.LogDatumUnionSize)]
    internal struct LogDatumUnionUint
    {
        public LogDatum logDatum;
        public uint unSignedInt;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.LogDatumUnionSize)]
    internal struct LogDatumUnionInt
    {
        public LogDatum logDatum;
        public int signedInt;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.LogDatumUnionSize)]
    internal struct LogDatumUnionDouble
    {
        public LogDatum logDatum;
        public double doublevalue;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.LogDatumUnionSize)]
    internal struct LogDatumUnionByteArray
    {
        public LogDatum logDatum;

        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.U1, SizeConst = BacDelStackConfig.MaxBitstringBytes)]
        public byte[] byteValues;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.LogDatumUnionSize)]
    internal struct LogDatumUnionBITStr
    {
        public LogDatum logDatum;
        public BacnetBITStr stBitString;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.LogDatumUnionSize)]
    internal struct LogDatumUnionErrorResponse
    {
        public LogDatum logDatum;
        public ErrorResponse stError;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.LogDatumUnionSize)]
    internal struct LogDatumUnionFloatTimeChange
    {
        public LogDatum logDatum;
        public float timechange;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.LogDatumUnionSize)]
    internal struct LogDatumUnionBacnetPropVal
    {
        public LogDatum logDatum;
        public BacnetPropertyValue bacnetProVal;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet log record.
    /// Name in Stack Application: BACnetLogRecord_t
    /// </summary>
    internal struct BacnetLogRecord
    {
        /** time stamp */
        public BacnetDateTime m_stTimeStamp;
        public short unusedBytes;
        public LogDatum logDatum;
        public BacnetBitStr stStatusFlag;
    }

    /// <summary>
    /// Linked list of bacnet log records.
    /// Name in Stack Application: ListOfBACnetLogRecord_t
    /// </summary>
    internal struct ListOfBacnetLogRecord
    {
        public BacnetLogRecord bacnetLogRecord;
        /** sequence no of log record */
        public uint sequenceNo;
        /** for creating linklist */
        public IntPtr next;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet log record array/list.
    /// Name in Stack Application: Pr_ListOfBACnetLogRecord_t
    /// </summary>
    internal struct PrListOfBacnetLogRecord
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /** count for no of nodes in list */
        public uint count;

        /// <summary>
        /// Union PropertyValue_u in Stack has been declared as byte[] propValueUnion here.
        /// This field may contain any one of below structures.
        /// PropertyValueUnionBool, PropertyValueUnionUint, PropertyValueUnionInt, PropertyValueUnionBinaryPV,
        /// PropertyValueUnionFloat, PropertyValueUnionDouble, PropertyValueUnionBITStr, PropertyValueUnionCharStr,
        /// PropertyValueUnionOctetStr, PropertyValueUnionTime, PropertyValueUnionDate, PropertyValueUnionDateTime
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.U1, SizeConst = BacDelPropertyDef.BacnetPropertyValueUnionSize)]
        public byte[] propValueUnion;

        public IntPtr logRecord;
    }

    /// <summary>
    /// This is structure of bacnet client cov increment.
    /// Name in Stack Application: BACnetClientCOV_t
    /// </summary>
    internal struct BacnetClientCOV
    {
        /** value if tag is real */
        public float value;
        /** variable to save data type - real or null */
        public BacnetApplicationTag appTagtype;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet client cov increment.
    /// Name in Stack Application: Pr_BACnetClientCOV_t
    /// </summary>
    internal struct PrBacnetClientCOV
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        public BacnetClientCOV stClientCOV;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet log data.
    /// Name in Stack Application: BACnetLogData_t
    /// </summary>
    internal struct BacnetLogData
    {
        /** type of datum value */
        public uint tagType;

        /// <summary>
        /// Union logMultipleData_u in Stack has been declared as byte[] bacnetLogDataUnion here. This field may contain any one of below structures.
        /// BacnetLogDataUnionLogStatus, BacnetLogDataUnionBool, BacnetLogDataUnionFloat, BacnetLogDataUnionEnum, BacnetLogDataUnionUint, BacnetLogDataUnionInt,
        /// BacnetLogDataUnionDouble, BacnetLogDataUnionByteArray, BacnetLogDataUnionBITStr, BacnetLogDataUnionErrorResponse, BacnetLogDataUnionFloatTimeChange,
        /// BacnetLogDataUnionPropVal
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.U1, SizeConst = BacDelPropertyDef.BacnetLogDataUnionSize)]
        public byte[] bacnetLogDataUnion;

        /* to create linklist */
        public IntPtr next;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetLogDataUnionSize)]
    internal struct BacnetLogDataUnionLogtatus
    {
        public BacnetLogStatus stlog_status;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetLogDataUnionSize)]
    internal struct BacnetLogDataUnionBool
    {
        /* For bool datatype we have to explicitly mention to allocate 1 byte to avoid packing issue.*/

        [MarshalAs(UnmanagedType.I1)]
        public bool value;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetLogDataUnionSize)]
    internal struct BacnetLogDataUnionFloat
    {
        public float value;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetLogDataUnionSize)]
    internal struct BacnetLogDataUnionEnum
    {
        public uint enumValue;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetLogDataUnionSize)]
    internal struct BacnetLogDataUnionUint
    {
        public uint value;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetLogDataUnionSize)]
    internal struct BacnetLogDataUnionInt
    {
        public int value;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetLogDataUnionSize)]
    internal struct BacnetLogDataUnionDouble
    {
        public double value;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetLogDataUnionSize)]
    internal struct BacnetLogDataUnionByteArray
    {
        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.U1, SizeConst = BacDelStackConfig.MaxBitstringBytes)]
        public byte[] value;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetLogDataUnionSize)]
    internal struct BacnetLogDataUnionBITStr
    {
        public BacnetBITStr stBitString;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetLogDataUnionSize)]
    internal struct BacnetLogDataUnionErrorResponse
    {
        public ErrorResponse stError;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetLogDataUnionSize)]
    internal struct BacnetLogDataUnionFloatTimeChange
    {
        public float timeChange;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetLogDataUnionSize)]
    internal struct BacnetLogDataUnionPropVal
    {
        public BacnetPropertyValue bacnetPropVal;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet log multiple record.
    /// Linked list of bacnet log multiple records.
    /// Name in Stack Application: ListOfBACnetLogMultipleRecord_t
    /// </summary>
    internal struct ListOfBacnetLogMultipleRecord
    {
        public BacnetDateTime stTimeStamp;
        public BacnetLogData bacnetLogData;
        /** sequence no of log record */
        public uint sequenceNo;
        /** for creating linklist */
        public IntPtr next;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet log multiple record list.
    /// Name in Stack Application: Pr_ListOfBACnetLogMultipleRecord_t
    /// </summary>
    internal struct PrListOfBacnetLogMultipleRecord
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /** count for no of nodes in list */
        public uint count;
        /** List of Log Records */
        public IntPtr logMultipleRecord;
    }

    /// <summary>
    /// This structure defines the property datatype of type event log data.
    /// Name in Stack Application: EventlogDatam_t
    /// </summary>
    internal struct EventlogDatam
    {
        /** type of datum value */
        public byte tagType;

        /// <summary>
        /// Union eventlogData_u in Stack has been declared as byte[] eventlogDatamUnion here. This field may contain any one of below structures.
        /// EventlogDatamUnionLogStatus, EventlogDatamUnionFloat, EventlogDatamUnionBacnetEnPropElem
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.U1, SizeConst = BacDelPropertyDef.EventlogDatamUnionSize)]
        public byte[] eventlogDatamUnion;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.EventlogDatamUnionSize)]
    internal struct EventlogDatamUnionLogStatus
    {
        public BacnetLogStatus bacnetLogStatus;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.EventlogDatamUnionSize)]
    internal struct EventlogDatamUnionFloat
    {
        public float timechange;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.EventlogDatamUnionSize)]
    internal struct EventlogDatamUnionBacnetEnPropElem
    {
        public BacnetEnPropElem bacnetEnPropElem;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet event log record.
    /// Linked list of bacnet event log records.
    /// Name in Stack Application: ListOfBACnetEventLogRecord_t
    /// </summary>
    internal struct ListOfBacnetEventLogRecord
    {
        /** single log record value */
        /** time stamp */
        public BacnetDateTime stTimeStamp;
        public EventlogDatam stEventLogDatum;
        /** sequence no of log record */
        public uint sequenceNo;
        /** for creating linklist */
        public IntPtr next;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet event log record list.
    /// Name in Stack Application: Pr_ListOfBACnetEventLogRecord_t
    /// </summary>
    internal struct PrListOfBacnetEventLogRecord
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /** count for no of nodes in list */
        public uint count;
        /** List of Event Log Records */
        public IntPtr eventLogRecord;
    }

    /// <summary>
    /// This is structure of bacnet set point reference.
    /// Name in Stack Application: BACnetSetpointRef_t
    /// </summary>
    internal struct BacnetSetpointRef
    {
        /* BACnet Object property reference data */
        public BacnetObjPropRef stObjPropRef;
        /** indicates set pt reff empty or not */
        /* For bool datatype we have to explicitly mention to allocate 1 byte to avoid packing issue.*/

        [MarshalAs(UnmanagedType.I1)]
        public bool dataFlag;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet set point reference.
    /// Name in Stack Application: Pr_BACnetSetpointRef_t
    /// </summary>
    internal struct PrBacnetSetpointRef
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        // public int activeCOV;
        /* set point ref data */
        public BacnetSetpointRef stSptRef;
    }

    /// <summary>
    /// structure for bacnet Scale.
    /// Name in Stack Application: BACnetScale_t
    /// </summary>
    internal struct BacnetScale
    {
        public BacnetScaleUnion bacnetScaleUnion;
        /* false == m_floatScale, true == m_i32integerScale */
        /* For bool datatype we have to explicitly mention to allocate 1 byte to avoid packing issue.*/

        [MarshalAs(UnmanagedType.I1)]
        public bool isIntegerScale;
    }

    [StructLayout(LayoutKind.Explicit)]
    internal struct BacnetScaleUnion
    {
        [FieldOffset(0)]
        public float floatScale;

        [FieldOffset(0)]
        public int integerScale;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet Scale.
    /// Name in Stack Application: Pr_BACnetScale_t
    /// </summary>
    internal struct PrBacnetScale
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        public BacnetScale bacnetScale;
    }

    /// <summary>
    /// structure for bacnet Prescale.
    /// Name in Stack Application: BACnetPrescale_t
    /// </summary>
    internal struct BacnetPrescale
    {
        public uint multiplier;
        public uint moduloDivide;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet PreScale.
    /// Name in Stack Application: Pr_BACnetPrescale_t
    /// </summary>
    internal struct PrBacnetPrescale
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /* prescale data */
        public BacnetPrescale stPrescale;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet accumulator record.
    /// Name in Stack Application: BACnetAccRecord_t
    /// </summary>
    internal struct BacnetAccRecord
    {
        /** time stamp */
        public BacnetDateTime stTimestamp;
        /** present value */
        public uint presentValue;
        /** accumulated value */
        public uint accumulatedValue;
        /** accumulator status */
        public BacnetAccStatus accStatus;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet accumulator record.
    /// Name in Stack Application: Pr_BACnetAccRecord_t
    /// </summary>
    internal struct PrBacnetAccRecord
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /** list of accumulator records */
        public BacnetAccRecord stRecord;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet action command.
    /// Name in Stack Application: ListOfBACnetActionCommand_t
    /// </summary>
    internal struct ListOfBacnetActionCommand
    {
        /** Type of Object*/
        public BacnetObjectType deviceType;
        /** Type of Object*/
        public BacnetObjectType objectType;
        /** Object Instance*/
        public uint devID;
        /** Object Instance*/
        public uint objID;
        /** Property Identifier*/
        public BacnetPropertyID propertyIdentifier;
        /** Array Index & its flag */
        public uint arrayIndex;
        /* For bool datatype we have to explicitly mention to allocate 1 byte to avoid packing issue.*/

        [MarshalAs(UnmanagedType.I1)]
        public bool arrIndxFlag;

        /** if true terminate the execution of the
        action list prematurely */
        /* For bool datatype we have to explicitly mention to allocate 1 byte to avoid packing issue.*/

        [MarshalAs(UnmanagedType.I1)]
        public bool quitOnFailure;

        /** Current action write Successful */
        /* For bool datatype we have to explicitly mention to allocate 1 byte to avoid packing issue.*/

        [MarshalAs(UnmanagedType.I1)]
        public bool writeSuccess;

        /** If the property being written is a commandable property,
        then a priority value shall be supplied */
        public uint priority;
        /* To identify whether priority present */
        /* For bool datatype we have to explicitly mention to allocate 1 byte to avoid packing issue.*/

        [MarshalAs(UnmanagedType.I1)]
        public bool priorityFlag;

        /** represent a delay in seconds */
        public uint postDelay;
        /* To identify whether PostDelay present */
        /* For bool datatype we have to explicitly mention to allocate 1 byte to avoid packing issue.*/

        [MarshalAs(UnmanagedType.I1)]
        public bool postDelayFlag;

        /** Property Value to command */
        public BacnetAbstractData stPropPointer;
        /** List Of Action command */
        public IntPtr next;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet action command list.
    /// Name in Stack Application: ListOfBACnetActionList_t
    /// </summary>
    internal struct ListOfBacnetActionList
    {
        /* list of action command */
        public IntPtr actionCommand;
        /* For bool datatype we have to explicitly mention to allocate 1 byte to avoid packing issue.*/

        [MarshalAs(UnmanagedType.I1)]
        public bool terminateList;

        public IntPtr next;
    }

    /// <summary>
    /// member element for array of bacnet action list.
    /// Name in Stack Application: ActionElement_t
    /// </summary>
    internal struct ActionElement
    {
        public int noListElement;
        public IntPtr arrayActCmd; // pointer to structure ListOfBACnetActionList_t
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet action list.
    /// Name in Stack Application: Pr_ListOfBACnetActionList_t
    /// </summary>
    internal struct PrListOfBacnetActionList
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /** array or list count */
        public uint count;
        /** array of action list */
        public IntPtr arrayActCmd;
    }

    /// <summary>
    /// list of property reference.
    /// Name in Stack Application: ListOfBACnetPropRef_t
    /// </summary>
    internal struct ListOfBacnetPropRef
    {
        /** Property Identifier*/
        public BacnetPropertyID propertyIdentifier;
        /** Property_Array_Index */
        public uint arrayIndex;
        /** check if array index present  */
        /* For bool datatype we have to explicitly mention to allocate 1 byte to avoid packing issue.*/

        [MarshalAs(UnmanagedType.I1)]
        public bool isArrayIndxPresent;

        /** to create list */
        public IntPtr next;
    }

    /// <summary>
    /// structure for read access specification list.
    /// Name in Stack Application: ListOfReadAccessSpecs_t
    /// </summary>
    internal struct ListOfReadAccessSpecs
    {
        /** Type of Object */
        public BacnetObjectType objectType;
        /** Object Instance */
        public uint objID;
        /** List Of Prop */
        public ListOfBacnetPropRef stlistOfPropRef;
        /** Next Object */
        public IntPtr next;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet Read Access Specification.
    /// Name in Stack Application: Pr_ListOfReadAccessSpecs_t
    /// </summary>
    internal struct PrListOfReadAccessSpecs
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /** count */
        public uint count;
        /** Group of obj n Prop */
        public IntPtr gpOfObj;
    }

    /// <summary>
    /// structure for list of read access results.
    /// Name in Stack Application: listOfResults_t
    /// </summary>
    internal struct ListOfResults
    {
        /** Property Identifier*/
        public BacnetPropertyID propertyIdentifier;
        /** Property_Array_Index */
        public uint arrayIndex;
        /** indicates if array index is present or not */
        /* For bool datatype we have to explicitly mention to allocate 1 byte to avoid packing issue.*/

        [MarshalAs(UnmanagedType.I1)]
        public bool arrIndxFlag;

        /** Indicate memeber in union is in use */
        /** tag == 4 is property value
            tag == 5 is property access error */
        public uint unionMember;
        public ListOfResultsUnion listOfResultUnion;
        /** to create list */
        public IntPtr next;
    }

    [StructLayout(LayoutKind.Explicit)]
    internal struct ListOfResultsUnion
    {
        /** Property Value  -- m_u32UniMember 1*/
        //BACNET_PROPERTY_VALUE m_stPropertyValue;
        /** Error -- m_u32UniMember 2 */

        [FieldOffset(0)]
        public ErrorResponse stError;

        /* Pointer to hold property add -- m_u32UniMember 3 */

        [FieldOffset(0)]
        public BacnetAbstractData stPropPointer;
    }

    /// <summary>
    /// Name in Stack Application: ListOfReadAccessResult_t
    /// </summary>
    internal struct ListOfReadAccessResult
    {
        /** Type of Object */
        public BacnetObjectType objectType;
        /** Object Instance */
        public uint objID;
        public ListOfResults listOfResults;
        public IntPtr next;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet Read Access Result.
    /// Name in Stack Application: Pr_ListOfReadAccessResult_t
    /// </summary>
    internal struct PrListOfReadAccessResult
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /** count */
        public uint count;
        /** list of values */
        public IntPtr gpObjResults;
    }

    /// <summary>
    /// This structure defines the property datatype of type array of Property Access Result.
    /// Name in Stack Application: ListOfBACnetPropAccessRslt_t
    /// </summary>
    internal struct ListOfBacnetPropAccessRslt
    {
        /** Type of Object */
        public BacnetObjectType objectType;
        /** Type of Device */
        public BacnetObjectType devType;
        /** Object Instance */
        public uint objID;
        /** Device Instance */
        public uint devID;
        /** Priority */
        public uint priority;
        /** */
        public BacnetPropertyID propertyIdentifier;
        /** Property_Array_Index */
        public uint arrayIndex;
        /** check if array index present  */
        /* For bool datatype we have to explicitly mention to allocate 1 byte to avoid packing issue.*/

        [MarshalAs(UnmanagedType.I1)]
        public bool isArrayIndxPresent;

        /** Indicate memeber in union is in use */
        /** tag == 4 is property value
            tag == 5 is property access error */
        public uint uniMember;
        public ListOfBacnetPropAccessRsltUnion listOfBacnetPropAccessRsltUnion;
        public bool dataTypeNotSupproted;
        /** to create list */
        public IntPtr next;
    }

    [StructLayout(LayoutKind.Explicit)]
    internal struct ListOfBacnetPropAccessRsltUnion
    {
        [FieldOffset(0)]
        public BacnetAbstractData stPropertyValue;

        [FieldOffset(0)]
        public ErrorResponse stError;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet Property Access Result.
    /// Name in Stack Application: Pr_ListOfBACnetPropAccessRslt_t
    /// </summary>
    internal struct PrListOfBacnetPropAccessRslt
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        // public int activeCOV;
        /** array size */
        public uint arraySize;
        /** array elements */
        public IntPtr propAccRslt;
    }

    /// <summary>
    /// Name in Stack Application: BACnetShedLevel_t
    /// </summary>
    internal struct BacnetShedLevel
    {
        /** type of shed level */
        public BacnetShedLevelType shedLevelType;
        public BacnetShedLevelUnion bacnetShelLevelUnion;
    }

    [StructLayout(LayoutKind.Explicit)]
    internal struct BacnetShedLevelUnion
    {
        /** percent value */

        [FieldOffset(0)]
        public uint percent;

        /** level value */

        [FieldOffset(0)]
        public uint level;

        /** amount value */

        [FieldOffset(0)]
        public float amount;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet shed level.
    /// Name in Stack Application: Pr_BACnetShedLevel_t
    /// </summary>
    internal struct PrBacnetShedLevel
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        public BacnetShedLevel bacnetShedLevel;
    }

    /// <summary>
    /// This structure defines the property datatype of type bacnet Authentication Factor.
    /// Name in Stack Application: Pr_BACnetAuFactor_t
    /// </summary>
    internal struct PrBacnetAuFactor
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        // public int activeCOV;
        /** Authentication Factor value */
        public BacnetAuFactor auFactor;
    }

    /// <summary>
    /// array of Authentication Factor format.
    /// Name in Stack Application: ListOfBACnetAuFactorFormat_t
    /// </summary>
    internal struct ListOfBacnetAuFactorFormat
    {
        /** type of authentication format */
        public BacnetAuthenticateFactorType formatType;
        /** vendor id */
        public ushort vendorID;
        /** vendor format */
        public ushort vendorFormat;
        /** flags for vendor id and vendor format */
        /* For bool datatype we have to explicitly mention to allocate 1 byte to avoid packing issue.*/

        [MarshalAs(UnmanagedType.I1)]
        public bool idFlag;

        /* For bool datatype we have to explicitly mention to allocate 1 byte to avoid packing issue.*/

        [MarshalAs(UnmanagedType.I1)]
        public bool formatFlag;

        /** to create array/list */
        public IntPtr next;
    }

    /// <summary>
    /// This structure defines the property datatype
    /// of type array of bacnet Authentication Factor format.
    /// Name in Stack Application: Pr_ListOfBACnetAuFactorFormat_t
    /// </summary>
    internal struct PrListOfBacnetAuFactorFormat
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /** array size count */
        public uint arraySize;
        /** elements of array */
        public IntPtr auFactFormatList;
    }

    /// <summary>
    /// array of Network Security Policy.
    /// Name in Stack Application: ListOfBACnetNwSecurityPolicy_t
    /// </summary>
    internal struct ListOfBacnetNwSecurityPolicy
    {
        /** port id */
        public byte portID;
        /** security policy */
        public BacnetSecurityPolicy securityPolicy;
        /** to create list */
        public IntPtr next;
    }

    /// <summary>
    /// This structure defines the property datatype
    /// of type array of bacnet network security policy.
    /// Name in Stack Application: Pr_ListOfBACnetNwSecurityPolicy_t
    /// </summary>
    internal struct PrListOfBacnetNwSecurityPolicy
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /** array size count */
        public uint arraySize;
        /** elements of array */
        public IntPtr securityPolicyArr;
    }

    /// <summary>
    /// struct for bacnet key identifier.
    /// Name in Stack Application: ListOfBACnetKeyId_t
    /// </summary>
    internal struct ListOfBacnetKeyId
    {
        /** algorithm value */
        public byte algorithm;
        /** key id value */
        public byte keyID;
        /** to create list */
        public IntPtr next;
    }

    /// <summary>
    /// This structure defines the property datatype
    /// of type bacnet key identifier.
    /// Name in Stack Application: Pr_ListOfBACnetKeyId_t
    /// </summary>
    internal struct PrListOfBacnetKeyId
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /** value of key identifier */
        public ListOfBacnetKeyId stKeyID;
    }

    /// <summary>
    /// structure for Security KeySet.
    /// Name in Stack Application: BACnetSecurityKeySet_t
    /// </summary>
    internal struct BacnetSecurityKeySet
    {
        /** port id */
        public byte keyRevision;
        /** activation time */
        public BacnetDateTime stActivationTime;
        /** expiration time */
        public BacnetDateTime stExpirationTime;
        /** key identifier */
        public ListOfBacnetKeyId stKeyIDs;
        /** to create list */
    }

    /// <summary>
    /// This structure defines the property datatype
    /// of type array of bacnet security keyset.
    /// Name in Stack Application: Pr_ListOfBACnetSecurityKeySet_t
    /// </summary>
    internal struct PrListOfBacnetSecurityKeySet
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /** array size count */
        public uint arraySize;
        /** elements of array */

        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.LPStruct, SizeConst = BacDelPropertyDef.PrListOfBacnetSecurityKeySetSize)]
        public BacnetSecurityKeySet[] stSecurityKetSet;
    }

    /// <summary>
    /// DESCRIPTION
    /// This structure defines the property datatype
    /// of type bacnet security keyset.
    /// Name in Stack Application: Pr_BACnetSecurityKeySet_t
    /// </summary>
    internal struct PrBacnetSecurityKeySet
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        // public int activeCOV;
        public BacnetSecurityKeySet securityKetSet;
    }

    /// <summary>
    /// struct for authentication policy.
    /// Name in Stack Application: Policy_t
    /// </summary>
    internal struct Policy
    {
        /** device object reference */
        public BacnetDevObjRef stCredentialDataInput;
        /** index value */
        public uint index;
        /** to create array/list */
        public IntPtr next;
    }

    /// <summary>
    /// array of Authentication Policy.
    /// Name in Stack Application: ListOfBACnetAuPolicy_t
    /// </summary>
    internal struct ListOfBacnetAuPolicy
    {
        /** timeout value */
        public uint timeout;
        /** order enforced */
        /* For bool datatype we have to explicitly mention to allocate 1 byte to avoid packing issue.*/

        [MarshalAs(UnmanagedType.I1)]
        public bool orderEnforced;

        /** policy */
        public Policy stPolicy;
        /** to create array/list */
        public IntPtr next;
    }

    /// <summary>
    /// This structure defines the property datatype
    /// of type array of bacnet Authentication Policy.
    /// Name in Stack Application: Pr_ListOfBACnetAuPolicy_t
    /// </summary>
    internal struct PrListOfBacnetAuPolicy
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /** array size */
        public uint arraySize;
        /** array/list elements */
        public IntPtr auPolicy;
    }

    /// <summary>
    /// struct for array/list of access rule.
    /// Name in Stack Application: ListOfBACnetAccessRule_t
    /// </summary>
    internal struct ListOfBacnetAccessRule
    {
        /** time range specifier */
        public BacnetTimeRangeSpecifier timeRangeType;
        /** location specifier */
        public BacnetLocationSpecifier locationType;
        /** time range - optional */
        public BacnetDevObjPropRef stTimeRange;
        /** location - optional */
        public BacnetDevObjRef stLocation;
        /** enable value */
        /* For bool datatype we have to explicitly mention to allocate 1 byte to avoid packing issue.*/

        [MarshalAs(UnmanagedType.I1)]
        public bool enable;

        /** to create list/array */
        public IntPtr next;
    }

    /// <summary>
    /// This structure defines the property datatype
    /// of type array of bacnet Access Rule.
    /// Name in Stack Application: Pr_ListOfBACnetAccessRule_t
    /// </summary>
    internal struct PrListOfBacnetAccessRule
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /** array size */
        public uint arraySize;
        /** array/list elements */
        public IntPtr accessRuleArray;
    }

    /// <summary>
    /// struct for array/list of Credential Authentication Factor.
    /// Name in Stack Application: ListOfBACnetCredAuFactor_t
    /// </summary>
    internal struct ListOfBacnetCredAuFactor
    {
        /** disable value */
        public BacnetAccessAuthenticationFactorDisable disable;
        /** authentication factor */
        public BacnetAuFactor stAuFactor;
        /** to create list/array */
        public IntPtr next;
    }

    /// <summary>
    /// This structure defines the property datatype
    /// of type array of bacnet Credential Authentication Factor.
    /// Name in Stack Application: Pr_ListOfBACnetCredAuFactor_t
    /// </summary>
    internal struct PrListOfBacnetCredAuFactor
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /** array size */
        public uint arraySize;
        /** array/list elements */
        public IntPtr credAuFactArray;
    }

    /// <summary>
    /// struct for array/list of Assigned Access Rights.
    /// Name in Stack Application: ListOfBACnetAssignedAccessRights_t
    /// </summary>
    internal struct ListOfBacnetAssignedAccessRights
    {
        /** assigned access rights value */
        public BacnetDevObjRef stAssAccessRights;
        /** enable value */
        /* For bool datatype we have to explicitly mention to allocate 1 byte to avoid packing issue.*/

        [MarshalAs(UnmanagedType.I1)]
        public bool enable;

        /** to create list/array */
        public IntPtr next;
    }

    /// <summary>
    /// This structure defines the property datatype
    /// of type array of bacnet Assigned Access Rights.
    /// Name in Stack Application: Pr_ListOfBACnetAssignedAccessRights_t
    /// </summary>
    internal struct PrListOfBacnetAssignedAccessRights
    {
        /** Access Sepcifier */
        public PropAccessType accessType;
        /** COV Status Specifier */
        //public int activeCOV;
        /** array size */
        public uint arraySize;
        /** array/list elements */
        public IntPtr asngdAccessArray;
    }

    /// <summary>
    /// Name in Stack Application: m_stObjId
    /// </summary>
    internal struct ObjID
    {
        ///<summary> Type of Object</summary>
        public BacnetObjectType ObjectType;

        ///<summary> Object Instance</summary>
        public uint ObjId;
    }


    /// <summary>
    /// Structure to save property values.
    /// Datatype as per standard is BACnetPropertyValue.
    /// Name in Stack Application: property_value_t
    /// </summary>
    internal struct PropertyValues
    {
        public BacnetPropertyID objectProperty;
        public int propertyArrayIndex;
        public bool isArrayIndxPresent;
        public BacnetDataType dataType;
        public byte priority;
        public IntPtr nextVal;
        public IntPtr propVal;
    }


    /// <summary>
    ///  This structure defines the property datatype of type channel value.
    /// Name in Stack Application: BACnetChannelValue_t
    /// </summary>
    internal struct BACnetChannelValue
    {
        /* data type */
        public BacnetDataType dataType;
        /* application tag type */
        public BacnetApplicationTag appTag;

        /// <summary>
        /// Union uValue in Stack has been declared as byte[] ChannelValueUnion here. This field may contain any one of below structures.
        /// BacnetPropertyValue, BACnetLightingCommand.
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.U1, SizeConst = BacDelPropertyDef.ChannelValueUnionSize)]
        public byte[] channelValueUnion;
    }

    /// <summary>
    ///  This structure defines the property datatype of type lighting commands.
    /// Name in Stack Application: BACnetLightingCommand_t
    /// </summary>
    internal struct BACnetLightingCommand
    {
        /* fade time */
        public uint fadeTime;
        /* target level */
        public float targetLevel;
        /* ramp rate */
        public float rampRate;
        /* step increment */
        public float stepIncrement;
        /* priority */
        public uint priority;
        /* flags for optional commands */
        public bool fadeTimeFlag;
        public bool priorityFlag;
        public bool targetLevelFlag;
        public bool rampRateFlag;
        public bool stepIncrementFlag;
        /* lighting operations */
        public BACnetLightingOperation operation;
    }

    /// <summary>
    /// This structure defines the property datatype of type channel value.
    /// Name in Stack Application: Pr_BACnetChannelValue_t
    /// </summary>
    internal struct PrBACnetChannelValue
    {
        //#ifdef USER_DEFINED_ACCESS_TYPE
        /** access sepcifier */
        public PropAccessType accessType;
        //#endif
        //#ifdef BACDEL_SER_DS_COV_B 
        /** cov status specifier */
        // int    m_i32ActiveCOV;
        //#endif
        /* channel value */
        public BACnetChannelValue channelValue;
    }
}