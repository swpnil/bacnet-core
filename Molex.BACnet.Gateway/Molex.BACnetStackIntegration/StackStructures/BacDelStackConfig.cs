﻿///-----------------------------------------------------------------
/// Project: Molex.BACnetStackIntegration 
/// Namespace: Molex.BACnetStackIntegration.Stack.Wrapper
/// Class: <ClassName>
/// Description: <Description>


/// 
/// Notes: <Notes>
///
///-----------------------------------------------------------------
using Molex.StackDataObjects.Constants;
using System;
using System.Runtime.InteropServices;

namespace Molex.BACnetStackIntegration.Stack.Wrapper
{
    internal class BacDelStackConfig
    {
        public const int MinBitstringBytes = 1;
        public const int MaxBitstringBytes = 10;
        public const int MaxOctetStringBytes = 50;
        public const int MaxCharacterStringBytes = 255;
        public const int MaxApplnPropValStr = 120;
        public const int MaxFileOctetStringBytes = 2048;
        public const int MaxPtSerData = 2048;
        public const int MaxIpLen = 4;   // IPv4
        public const int MaxPortLen = 2;
        public const int MaxMacLen = (MaxIpLen + MaxPortLen);
        public const int MaxPriorityArrayCount = 16;
        public const int InvokeIdRange = 255;
    }

    /// <summary>
    /// Name in Stack Application: INITIATE_SERVICE_STATE
    /// </summary>
    internal enum InitiateServiceState
    {
        StateIdle,
        StateFillRequest,
        StateRequestSendFailed,
        StateAwaitResponse,
        StateResponseReceived,
        StateSegmentedConfirmation,
        StateFixed,
        StateSendRequest = 253,
        StateTimeout = 254,
        StateError
    }

    /// <summary>
    /// Name in Stack Application: BACApp_MaxLimits_t
    /// </summary>
    internal struct BACAppMaxLimits
    {
        /* max initiate Q length */
        public uint InitiateQueNodes;
        /* max BBMD initiate Q length */
        public uint BBMDInitiateQueNodes;
        /* max count for BDT, FDT & register FD entries */
        public uint MaxBdtFdtEntries;
        /* max objects that can be created in single device */
        public uint MaxObject;
        /* max count for dynamic address binding nodes */
        public uint MaxDynamicAddrBind;
        /* max count for COV subscriptions which can be sent by stack  */
        public uint MaxCovSubsTx;
        /* max count for properties or data-types */
        public uint MaxStateText;
        public uint MaxAFValuesList;
        public uint MaxExSchdList;
        public uint MaxTimeValueList;
        public uint MaxDateList;
        public uint MaxDestinationList;
        public uint MaxDevObjPropRefList;
        public uint MaxEventParaList;
        public uint MaxObjIdList;
        public uint MaxActiveCovSubs;
        /* max count for static address binding nodes */
        public uint MaxStaticAddrBind;
        /* max count for network analyzability nodes */
        public uint MaxNwAnalyzability;
        /* max b-side callback notification Q length */
        public uint CbNotifyQueNodes;
        /* max count for fualt paremeters data-type */
        public uint MaxFaultParaList;
        /* max count for COVU subsription list */
        public uint MaxCovUSubs;
        /* max count for subordinate list property */
        public uint MaxSubordinateList;
    }

    ///<summary>
    ///Structure to save device IP address, port no, DNET and DADR
    ///Name in Stack Application: BACnetAddress_t
    ///</summary>
    internal struct BacnetAddress
    {
        /*u8mac_len = 0 if global address */
        public byte macLen;
        /*
        LEN = 0 denotes broadcast MAC ADR and ADR field is absent
        LEN > 0 specifies length of ADR field
        */
        public byte dLen;        /// length of MAC address
        /* DNET,DLEN,DADR or SNET,SLEN,SADR */
        /* the following are used if the device is behind a router */
        /* net = 0 indicates local */
        public ushort net;       // BACnet network number
        /* note: MAC for IP addresses uses 4 bytes for addr, 2 bytes for port */
        /* use de/Encode_Unsigned32/16 for re/storing the IP address */

        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.U1, SizeConst = BacDelStackConfig.MaxMacLen)]
        public byte[] ipAddrs;

        /* hwaddr (MAC) address */

        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.U1, SizeConst = BacDelStackConfig.MaxMacLen)]
        public byte[] dvDadr;
    }

    ///<summary>
    /// Name in Stack Application: BACnetCharStr_t
    ///  DESCRIPTION
    /// This structure defines the property datatype
    /// of type Character String.
    /// </summary>
    internal struct BacnetCharStr
    {
        /* String lenght. */
        public uint strLen;
        /* Code page value */
        public ushort codePage;
        /* Character Encoding Format */
        public byte encoding;
        /* Value for character string */

        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.I1, SizeConst = BacDelStackConfig.MaxCharacterStringBytes)]
        public byte[] charStr;
    }

    ///<summary>
    /// Name in Stack Application: BACnetOctetStr_t
    /// DESCRIPTION
    /// This structure defines the property datatype
    /// of type Octet String.
    ///</summary>
    internal struct BacnetOctetStr
    {
        /* String lenght. */
        public uint OctetCount;
        /* Value for octet string */

        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.U1, SizeConst = BacDelStackConfig.MaxOctetStringBytes)]
        public byte[] octetStr;
    }

    ///<summary> Name in Stack Application: BACnetBitStr_t
    /// DESCRIPTION
    /// This structure defines the property datatype
    /// of type Bit String.
    /// Contains single byte bit string.
    ///</summary>
    internal struct BacnetBitStr
    {
        /* Number of unused bits in last byte */
        public byte unusedBits;
        /* Number of used byte in array */
        public byte byteCnt;
        /* bit values */

        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.U1, SizeConst = BacDelStackConfig.MinBitstringBytes)]
        public byte[] transBits;
    }

    /// <summary>
    /// Name in Stack Application:BACnetBITStr_t
    /// </summary>
    internal struct BacnetBITStr
    {
        /* Number of unused bits in last byte1 */
        public byte unusedBits;
        /* Number of used byte in array */
        public byte byteCnt;
        /* bit values */

        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.U1, SizeConst = BacDelStackConfig.MaxBitstringBytes)]
        public byte[] transBits;
    }

    ///<summary>
    ///structure of Bit-String datatype
    ///Name in Stack Application: BACNET_BIT_STRING
    ///</summary>
    internal struct BacnetBitString
    {
        public byte bitsUsed;

        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.U1, SizeConst = BacDelStackConfig.MaxBitstringBytes)]
        public byte[] values;
    }

    ///<summary>
    ///date
    ///Name in Stack Application: BACnetDate_t
    ///</summary>
    internal struct BacnetDate
    {
        /* Year value */
        public ushort year; // AD
        /* Month value */
        public byte month;  // 1=Jan
        /* Day value */
        public byte day;    // 1..31
        /* Weekday value */
        public byte weekDay;   // 1=Monday-7=Sunday
    }

    ///<summary>
    ///time
    ///Name in Stack Application: BACnetTime_t
    ///</summary>
    internal struct BacnetTime
    {
        /* Hour value */
        public byte hour;
        /* Minutes value*/
        public byte min;
        /* Second value */
        public byte sec;
        /* Hundredths value */
        public byte hundredths;
    }

    ///<summary>
    ///note: with microprocessors having lots more code space than memory,
    ///it might be better to have a packed encoding with a library to
    ///easily access the data.
    ///Name in Stack Application: BACnetObjId_t
    ///</summary>
    internal struct BacnetObjID
    {
        /* Object type */
        public BacnetObjectType objType;
        /* Object instances number */
        public uint objInstance;
    }

    ///<summary>
    ///* Structure to access the data of Error Response
    ///Name in Stack Application: error_response_t
    ///</summary>
    internal struct ErrorResponse
    {
        /* Error class as per BACNET_ERROR_CLASS enums */
        public BacnetErrorClass errorClass;
        /* Error code as per BACNET_ERROR_CODE enums */
        public BacnetErrorCode errorCode;
        /* First failed element number */
        public int firstFailedNo;
    }

    ///<summary>
    /// Structure to access the data of Reject Response
    ///Name in Stack Application: reject_response_t
    ///</summary>
    internal struct RejectResponse
    {
        /* Reject reason as per BACNET_REJECT_REASON enums */
        public BacnetRejectReason rejectReason;
    }

    ///<summary>
    /// Structure to access the data of Abort Response
    ///Name in Stack Application: abort_response_t
    ///</summary>
    internal struct AbortResponse
    {
        /* Abort reason as per BACNET_ABORT_REASON enums */
        public BacnetAbortReason abortReason;
    }

    ///<summary>
    ///BACnet Property Value
    ///Name in Stack Application: BACNET_PROPERTY_VALUE
    ///</summary>
    internal struct BacnetPropertyValue
    {
        public byte contextSpecific;  // true if context specific data
        public byte startTag;         // only used for context specific data
        public byte contextTag;       // only used for context specific data
        public byte tagType;          // application tag data type

        /// <summary>
        /// Union uValue in Stack has been declared as byte[] BacnetPropertyValueUnion here. This field may contain any one of below structures.
        /// BacnetPropertyValueUnionSbyte, BacnetPropertyValueUnionUint, BacnetPropertyValueUnionInt, BacnetPropertyValueUnionFloat, BacnetPropertyValueUnionDouble,
        /// BacnetPropertyValueUnionOctetStr, BacnetPropertyValueUnionCharStr, BacnetPropertyValueUnionBitStr, BacnetPropertyValueUnionEnum, BacnetPropertyValueUnionDate,
        /// BacnetPropertyValueUnionTime, BacnetPropertyValueUnionObjID, BacnetPropertyValueUnionIntPtr.
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.U1, SizeConst = BacDelPropertyDef.BacnetPropertyValueUnionSize)]
        public byte[] BacnetPropertyValueUnion;

        public IntPtr nextPropVal;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetPropertyValueUnionSize)]
    internal struct BacnetPropertyValueUnionSbyte
    {
        public sbyte value;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetPropertyValueUnionSize)]
    internal struct BacnetPropertyValueUnionUint
    {
        public uint value;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetPropertyValueUnionSize)]
    internal struct BacnetPropertyValueUnionInt
    {
        public int value;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetPropertyValueUnionSize)]
    internal struct BacnetPropertyValueUnionFloat
    {
        public float value;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetPropertyValueUnionSize)]
    internal struct BacnetPropertyValueUnionDouble
    {
        public double value;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetPropertyValueUnionSize)]
    internal struct BacnetPropertyValueUnionOctetStr
    {
        public BacnetOctetStr octetString;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetPropertyValueUnionSize)]
    internal struct BacnetPropertyValueUnionCharStr
    {
        public BacnetCharStr charString;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetPropertyValueUnionSize)]
    internal struct BacnetPropertyValueUnionBitStr
    {
        public BacnetBitString bitString;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetPropertyValueUnionSize)]
    internal struct BacnetPropertyValueUnionEnum
    {
        public uint enumerated;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetPropertyValueUnionSize)]
    internal struct BacnetPropertyValueUnionDate
    {
        public BacnetDate stDate;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetPropertyValueUnionSize)]
    internal struct BacnetPropertyValueUnionTime
    {
        public BacnetTime stTime;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetPropertyValueUnionSize)]
    internal struct BacnetPropertyValueUnionObjID
    {
        public BacnetObjID stObjID;
    }

    [StructLayout(LayoutKind.Sequential, Size = BacDelPropertyDef.BacnetPropertyValueUnionSize)]
    internal struct BacnetPropertyValueUnionIntPtr
    {
        public IntPtr pvValue;
    }

    /// <summary>
    /// Name in Stack Application: BACnetAddrBinding_t
    /// </summary>
    internal struct BacnetAddrBinding
    {
        /** Stores Device Identifier */
        public uint objId;		/** Object Instance*/
        public BacnetObjectType objectType;   /** Type of Object*/
        /** Stores MAX APDU Length */
        public ushort maxAPDULenAccepted;
        /** Vendor Id */
        public ushort vendorId;
        /** Type of segmentation */
        public BacnetSegmentation segmentationSupport;
        /** BACnet address */
        public BacnetAddress stAddress;
        /** Last Invoke Id used */
        public ushort currentInvokeId;
        /** Array to search free Invoke ID */
        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.U1, SizeConst = BacDelStackConfig.InvokeIdRange)]
        public byte[] invokeId;
        /** Next node in list */
        public IntPtr next;
    }
}